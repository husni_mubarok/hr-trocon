<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\BonusRequest;
use App\Http\Controllers\Controller;
use App\Model\Payperiod;
use App\Model\Year;
use App\Model\Department;
use App\Model\Bonus; 
use App\Model\Bonusdetail; 
use App\Model\Month;
use Validator;
use Response;
use App\Post;
use View;

class BonusController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'periodid' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::where('status', '0')->get()->pluck('description', 'id');
    $year_list = Year::all()->pluck('yearname', 'yearname');
    $month_list = Month::all()->pluck('monthname', 'id');
    $bonuss = Bonus::all();
    return view ('editor.bonus.index', compact('bonuss','payperiod_list', 'department_list', 'year_list', 'month_list'));
  }


  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
                bonus.id,
                bonus.periodid,
                bonus.departmentid,
                bonus.year,
                DATE_FORMAT(bonus.bonusdate,
                  "%d-%m-%Y"
                ) AS bonusdate,
                DATE_FORMAT(bonus.paymentdate,
                  "%d-%m-%Y"
                ) AS paymentdate,
                department.departmentcode,
                department.departmentname,
                payperiod.description,
                bonus.status
              FROM
                bonus
              LEFT JOIN department ON bonus.departmentid = department.id
              LEFT JOIN payperiod ON bonus.periodid = payperiod.id
              WHERE bonus.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->description."'".')"> Hapus</a>';
      })

      ->addColumn('actiondetail', function ($itemdata) {
        return '<a href="bonus/'.$itemdata->id.'/'.$itemdata->departmentid.'/editdetail" title="Detail" class="btn btn-primary btn-xs btn-flat"><i class="fa fa-folder"></i> Detail</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Aktif </span>';
        }else{
         return '<span class="label label-danger"> Tidak Aktif </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

 public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Bonus(); 
    $post->periodid = $request->periodid; 
    $post->departmentid = $request->departmentid; 
    $post->year = $request->yearform; 
    $post->bonusdate = $request->bonusdate; 
    $post->paymentdate = $request->paymentdate; 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    $bonus = Bonus::Find($id);
    echo json_encode($bonus); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Bonus::Find($id); 
    $post->periodid = $request->periodid; 
    $post->departmentid = $request->departmentid; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function editdetail($id, $id2)
  {
    $bonus = Bonus::Find($id);
    $sql = 'SELECT
              bonusdet.id AS id,
              employee.employeename,
              DATE_FORMAT(employee.joindate,
                  "%d-%m-%Y"
                ) AS joindate,
              employee.npwp,
              employee.taxstatus,
              employee.basic,
              bonusdet.grade,
              bonusdet.`value`,
              bonusdet.bonus,
              bonusdet.tarif,
              FORMAT(bonusdet.adjustbonus,0) AS adjustbonus,
              bonusdet.tbonus,
              bonusdet.pph21,
              FORMAT(bonusdet.potpinjaman,0) AS potpinjaman,
              bonusdet.netto
            FROM
              bonusdet
            INNER JOIN employee ON bonusdet.employeeid = employee.id
            WHERE bonusdet.transid = '.$id.' AND employee.departmentid = '.$id2.'';
    $bonus_detail = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

    return view ('editor.bonus.form', compact('bonus', 'bonus_detail'));
  }

  public function updatedetail($id, $id2, Request $request)
  {
     foreach($request->input('detail') as $key => $detail_data)
    {
      $bonus_detail = Bonusdetail::Find($key); 
      $bonus_detail->value = $detail_data['value']; 
      $bonus_detail->grade = $detail_data['grade']; 
      $bonus_detail->adjustbonus = str_replace(",", "", $detail_data['adjustbonus']);
      $bonus_detail->pph21 = str_replace(",", "", $detail_data['pph21']);
      $bonus_detail->potpinjaman = str_replace(",", "", $detail_data['potpinjaman']);
      $bonus_detail->save();
    } 

    // return redirect()->action('Editor\BonusController@index'); 
    return back();
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Bonus::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function generate($id)
  {  
    //dd("generate");
    //return response()->json($post); 
     DB::insert("INSERT INTO bonusdet (transid, periodid, employeeid) SELECT
                    bonus.id,
                    bonus.periodid,
                    employee.id
                  FROM
                    bonus
                  LEFT OUTER JOIN bonusdet ON bonus.id = bonusdet.transid
                  CROSS JOIN employee
                  WHERE
                    (bonusdet.employeeid IS NULL)
                  AND bonus.periodid = (
                    SELECT
                      `user`.periodid
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )
                  AND employee.`status` = 0
                  AND bonus.departmentid = employee.departmentid
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


      DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.dateemp = employee.joindate, bonusdet.biayajabmember = employee.biayajabmember, bonusdet.periodid = bonus.periodid
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");



      DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.basic = employee.basic,
                   bonusdet.bonus = ROUND(
                    CASE
                    WHEN TIMESTAMPDIFF(
                      MONTH,
                      bonusdet.dateemp,
                      bonus.bonusdate
                    ) < 12 THEN
                      (IFNULL(employee.basic, 0)) * (
                        TIMESTAMPDIFF(
                          MONTH,
                          bonusdet.dateemp,
                          bonus.bonusdate
                        )
                      ) / 12
                    ELSE
                      IFNULL(employee.basic, 0)
                    END,
                    - 2
                  )
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


      DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.basic =  CASE WHEN (TIMESTAMPDIFF(
                      MONTH,bonusdet.dateemp, bonus.bonusdate)) < 3 THEN 0 ELSE employee.basic END
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


      DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.basic =  CASE WHEN (TIMESTAMPDIFF(
                      MONTH,bonusdet.dateemp, bonus.bonusdate)) < 3 THEN 0 ELSE employee.basic END
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


      DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.bonus =  ROUND(CASE WHEN (TIMESTAMPDIFF(
                  MONTH,bonusdet.dateemp, bonus.bonusdate)) < 12 THEN (IFNULL(bonusdet.basic, 0)) * (TIMESTAMPDIFF(
                    MONTH,bonusdet.dateemp, bonus.bonusdate)) / 12 ELSE IFNULL(bonusdet.basic, 0) END, -2)
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


      DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.bonus = CASE
                  WHEN (
                    TIMESTAMPDIFF(
                    MONTH,
                      bonusdet.dateemp,
                      bonus.bonusdate
                    ) 
                  ) < 3 THEN
                    0
                  ELSE
                    bonusdet.bonus
                  END
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       DB::update("UPDATE bonusdet
                    INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                    INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                    SET bonusdet.taxstatus =  employee.taxstatus, bonusdet.npwp =  employee.npwp
                    WHERE
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                    AND bonus.`year` = (
                      SELECT
                        `user`.`year`
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )");

       // Untuk mengembalikan nilai menjadi 0
       DB::update("UPDATE bonusdet
                    INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                    INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                    SET bonusdet.kumulatif1 =  0, bonusdet.lastbruto1 =  0, bonusdet.bonusmonth1 =  0, bonusdet.biayajab1 =  0, bonusdet.nettoptkp1 =  0, bonusdet.ptkp1 =  0, bonusdet.pkp1 =  0, bonusdet.pph211 =  0, bonusdet.kumulatif2 =  0, bonusdet.lastbruto2 =  0, bonusdet.bonusmonth2 =  0, bonusdet.tlastbruto2 =  0, bonusdet.biayajab2 =  0, bonusdet.nettoptkp2 =  0, bonusdet.ptkp2 =  0, bonusdet.pkp2 =  0, bonusdet.pph212 =  0, bonusdet.pph21 =  0, bonusdet.netto =  0
                    WHERE
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                    AND bonus.`year` = (
                      SELECT
                        `user`.`year`
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )");


        /* DB::update("UPDATE UNTUK PINJAMAN KARYAWAN */
          DB::update("UPDATE bonusdet a
          INNER JOIN (
            SELECT
              loan.employeeid,
              loandet.`month`,
              payperiod.id AS periodid,
              sum(loandet.bonus) AS amount
            FROM
              loan
            INNER JOIN loandet ON loan.id = loandet.transid
            INNER JOIN payperiod ON loandet.`year` = payperiod.`year`
            AND loandet.`month` = payperiod.`month`
            WHERE  loan.loantypeid = 1  
            GROUP BY
              payperiod.begindate,
              payperiod.enddate,
              loan.employeeid,
              loandet.`month`,
              loandet.`year`
          ) u
          SET a.potpinjaman = round(ifnull(u.amount, 0) + 0.1, 0)
          WHERE
            a.employeeid = u.employeeid AND a.periodid = u.periodid
          AND a.periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )");



       // Mengambil nilai kumulatif tanpa Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  INNER JOIN (SELECT
                    payroll.employeeid,
                    SUM(payroll.bruto) AS bruto
                  FROM
                    payroll
                  INNER JOIN payperiod ON payroll.periodid = payperiod.id
                  WHERE
                    (
                      payperiod.`month` BETWEEN (
                        SELECT
                          `user`.bmonth
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                      AND (
                        SELECT
                          `user`.emonth
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                    )
                  AND (
                    payperiod.`year` = (
                      SELECT
                        `user`.`year`
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  )
                  GROUP BY
                    payroll.employeeid) AS payroll ON bonusdet.employeeid = payroll.employeeid
                  SET bonusdet.kumulatif1 = payroll.bruto
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");



        // Mengambil nilai kumulatif dengan Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  INNER JOIN (SELECT
                    payroll.employeeid,
                    SUM(payroll.bruto) AS bruto
                  FROM
                    payroll
                  INNER JOIN payperiod ON payroll.periodid = payperiod.id
                  WHERE
                    (
                      payperiod.`month` BETWEEN (
                        SELECT
                          `user`.lastbrutothrmonth
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                      AND (
                        SELECT
                          `user`.emonth
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                    )
                  AND (
                    payperiod.`year` = (
                      SELECT
                        `user`.`year`
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  )
                  GROUP BY
                    payroll.employeeid) AS payroll ON bonusdet.employeeid = payroll.employeeid
                  SET bonusdet.kumulatif2 = payroll.bruto
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // Mengambil nilai bruto terakhir tanpa Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  INNER JOIN (SELECT
                    payroll.employeeid,
                    SUM(payroll.bruto) AS bruto
                  FROM
                    payroll
                  INNER JOIN payperiod ON payroll.periodid = payperiod.id
                  WHERE
                    (
                      payperiod.`month` = (
                        SELECT
                          `user`.lastbrutothrmonth
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      ) 
                    )
                  AND (
                    payperiod.`year` = (
                      SELECT
                        `user`.lastbrutothryear
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  )
                  GROUP BY
                    payroll.employeeid) AS payroll ON bonusdet.employeeid = payroll.employeeid
                  SET bonusdet.lastbruto1 = payroll.bruto
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // Mengambil nilai bruto terakhir dengan Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  INNER JOIN (SELECT
                    payroll.employeeid,
                    SUM(payroll.bruto) AS bruto
                  FROM
                    payroll
                  INNER JOIN payperiod ON payroll.periodid = payperiod.id
                  WHERE
                    (
                      payperiod.`month` = (
                        SELECT
                          `user`.lastbrutothrmonth
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      ) 
                    )
                  AND (
                    payperiod.`year` = (
                      SELECT
                        `user`.lastbrutothryear
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  )
                  GROUP BY
                    payroll.employeeid) AS payroll ON bonusdet.employeeid = payroll.employeeid
                  SET bonusdet.lastbruto2 = payroll.bruto
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // Mengambil bulan terakhir tanpa Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.bonusmonth1 = (SELECT `user`.lastbrutothrmonth FROM `user` WHERE `user`.id = 1)
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // Mengambil bulan terakhir dengan Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.bonusmonth2 = (SELECT `user`.lastbrutothrmonth FROM `user` WHERE `user`.id = 1)
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // Mengambil bulan terakhir dengan Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.bonusmonth2 = (SELECT `user`.lastbrutothrmonth FROM `user` WHERE `user`.id = 1)
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // Tanpa bonus
       DB::update("UPDATE bonusdet
                    INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                    INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                    SET bonusdet.bonusmonth1 = CASE WHEN bonusdet.bonusmonth1 = 12 THEN 12 ELSE CASE WHEN bonusdet.bonusmonth1 <> 12 THEN 12 - bonusdet.bonusmonth1 END END
                    WHERE
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                    AND bonus.`year` = (
                      SELECT
                        `user`.`year`
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )");


        // Dengan bonus
       DB::update("UPDATE bonusdet
                    INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                    INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                    SET bonusdet.bonusmonth2 = CASE WHEN bonusdet.bonusmonth2 = 12 THEN 12 ELSE CASE WHEN bonusdet.bonusmonth2 <> 12 THEN 12 - bonusdet.bonusmonth2 END END
                    WHERE
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                    AND bonus.`year` = (
                      SELECT
                        `user`.`year`
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )");


       // Total Bruto tanpa Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.tlastbruto1 = (Ifnull(bonusdet.lastbruto1,0) * ifnull(bonusdet.bonusmonth1,0)) + ifnull(bonusdet.kumulatif1,0)
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // Total Bruto dengan Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.tlastbruto2 = (ifnull(bonusdet.lastbruto2,0) * ifnull(bonusdet.bonusmonth2,0)) + ifnull(bonusdet.kumulatif2,0) + ifnull(bonusdet.tbonus,0)
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // Biaya jabatan tanpa Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.biayajab1 = ROUND(CASE WHEN bonusdet.tlastbruto1 * 5 / 100 > 6000000 THEN 6000000 ELSE bonusdet.tlastbruto1 * 5 / 100 END,0) * bonusdet.biayajabmember
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // Biaya jabatan dengan Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.biayajab2 = ROUND(CASE WHEN bonusdet.tlastbruto2 * 5 / 100 > 6000000 THEN 6000000 ELSE bonusdet.tlastbruto2 * 5 / 100 END,0) * bonusdet.biayajabmember
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");



       // NettoPTKP tanpa Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.nettoptkp1 = IFNULL(bonusdet.tlastbruto1,0) - IFNULL(bonusdet.biayajab1,0)
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // NettoPTKP tanpa Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.nettoptkp1 = IFNULL(bonusdet.tlastbruto1,0) - IFNULL(bonusdet.biayajab1,0)
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // PTKP tanpa Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  INNER JOIN (SELECT * FROM ptkp) AS ptkp ON bonusdet.taxstatus = ptkp.taxstatus
                  SET bonusdet.ptkp1 = IFNULL(ptkp.ptkp,0) * 12
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // PTKP dengan Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  INNER JOIN (SELECT * FROM ptkp) AS ptkp ON bonusdet.taxstatus = ptkp.taxstatus
                  SET bonusdet.ptkp2 = IFNULL(ptkp.ptkp,0) * 12
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");

       // PKP tanpa THR
       DB::update("UPDATE bonusdet
                    INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                    INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id 
                    SET bonusdet.pkp1 = ifnull(bonusdet.nettoptkp1,0) - ifnull(bonusdet.ptkp1,0)
                    WHERE bonusdet.nettoptkp1 > bonusdet.ptkp1 AND
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                    AND bonus.`year` = (
                      SELECT
                        `user`.`year`
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )");

        // PKP dengan THR
       DB::update("UPDATE bonusdet
                    INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                    INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id 
                    SET bonusdet.pkp2 = ifnull(bonusdet.nettoptkp2,0) - ifnull(bonusdet.ptkp2,0)
                    WHERE bonusdet.nettoptkp2 > bonusdet.ptkp2 AND
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                    AND bonus.`year` = (
                      SELECT
                        `user`.`year`
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )");


      // PPh21 tanpa Bonus
       DB::update("UPDATE bonusdet
                    INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                    INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                    INNER JOIN (SELECT
                      bonusdet.employeeid,
                      sum(
                        CASE
                        WHEN pkp1 < tarif.`from` THEN
                          0
                        ELSE
                          CASE
                        WHEN pkp1 > tarif.`to` THEN
                          tarif.`to` - tarif.`from`
                        ELSE
                          pkp1 - tarif.`from`
                        END
                        END * tarif.tarif / 100
                      ) AS pph
                    FROM
                      bonusdet
                    INNER JOIN bonus ON bonusdet.transid = bonus.id
                    CROSS JOIN tarif
                    WHERE
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = @UserID
                      )
                    GROUP BY
                      bonusdet.employeeid) AS tarif ON bonusdet.employeeid = tarif.employeeid
                    SET bonusdet.pph211 = ROUND(tarif.pph,0)
                    WHERE 
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                    AND bonus.`year` = (
                      SELECT
                        `user`.`year`
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )");


       // PPh21 tanpa Bonus
       DB::update("UPDATE bonusdet
                    INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                    INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                    INNER JOIN (SELECT
                      bonusdet.employeeid,
                      sum(
                        CASE
                        WHEN pkp1 < tarif.`from` THEN
                          0
                        ELSE
                          CASE
                        WHEN pkp1 > tarif.`to` THEN
                          tarif.`to` - tarif.`from`
                        ELSE
                          pkp1 - tarif.`from`
                        END
                        END * tarif.tarif / 100
                      ) AS pph
                    FROM
                      bonusdet
                    INNER JOIN bonus ON bonusdet.transid = bonus.id
                    CROSS JOIN tarif
                    WHERE
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = @UserID
                      )
                    GROUP BY
                      bonusdet.employeeid) AS tarif ON bonusdet.employeeid = tarif.employeeid
                    SET bonusdet.pph211 = ROUND(tarif.pph,0)
                    WHERE 
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                    AND bonus.`year` = (
                      SELECT
                        `user`.`year`
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )");


       // PPh21 dengan Bonus
       DB::update("UPDATE bonusdet
                    INNER JOIN (SELECT * FROM employee) AS employee ON bonusdet.employeeid = employee.id
                    INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                    INNER JOIN (SELECT
                      bonusdet.employeeid,
                      sum(
                        CASE
                        WHEN pkp2 < tarif.`from` THEN
                          0
                        ELSE
                          CASE
                        WHEN pkp1 > tarif.`to` THEN
                          tarif.`to` - tarif.`from`
                        ELSE
                          pkp1 - tarif.`from`
                        END
                        END * tarif.tarif / 100
                      ) AS pph
                    FROM
                      bonusdet
                    INNER JOIN bonus ON bonusdet.transid = bonus.id
                    CROSS JOIN tarif
                    WHERE
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = @UserID
                      )
                    GROUP BY
                      bonusdet.employeeid) AS tarif ON bonusdet.employeeid = tarif.employeeid
                    SET bonusdet.pph212 = ROUND(tarif.pph,0)
                    WHERE 
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                    AND bonus.`year` = (
                      SELECT
                        `user`.`year`
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )");


       // PPh21 tanpa Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.PPh211 = CASE WHEN LENGTH(RTRIM(bonusdet.npwp)) > 0 THEN bonusdet.pph211  ELSE bonusdet.pph211 * 1.2 END
                  WHERE 
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");

       // PPh21 dengan Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.PPh212 = CASE WHEN LENGTH(RTRIM(bonusdet.npwp)) > 0 THEN bonusdet.pph212 ELSE bonusdet.pph212 * 1.2 END
                  WHERE 
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // Hitung selisih PPh21 dengan Bonus dan tanpa Bonus
       DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.pph21 = IFNULL(bonusdet.pph212,0) - IFNULL(bonusdet.pph211,0)
                  WHERE 
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       DB::update("UPDATE bonusdet
                    INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                    SET bonusdet.pph21 = CASE WHEN taxstatus IS NULL THEN 0 ELSE pph21 End
                    WHERE 
                      bonus.periodid = (
                        SELECT
                          `user`.periodid
                        FROM
                          `user`
                        WHERE
                          `user`.id = 1
                      )
                    AND bonus.`year` = (
                      SELECT
                        `user`.`year`
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )");

       DB::update("UPDATE bonusdet INNER JOIN (SELECT
        payroll_payslips.payroll_period_id,
        payroll_payslips.employee_id,
        payroll_payslips.component_regular_tax
      FROM
        payroll_payslips) AS tax
      SET bonusdet.pph21 =  tax.component_regular_tax
      WHERE bonusdet.periodid = tax.payroll_period_id AND bonusdet.employeeid = tax.employee_id AND
        (
          bonusdet.periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");


      // T Bonus
      DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.tbonus = ROUND(
                    (
                      (IFNULL(bonusdet.bonus, 0) * IFNULL(bonusdet.value, 0))  + IFNULL(bonusdet.adjustbonus, 0)
                    ),
                    0
                  )
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");


       // Netto
      DB::update("UPDATE bonusdet
                  INNER JOIN (SELECT * FROM bonus) AS bonus ON bonusdet.transid = bonus.id
                  SET bonusdet.netto = ROUND(
                    (
                      IFNULL(bonusdet.tbonus, 0) - IFNULL(bonusdet.pph21, 0)
                    ) - IFNULL(bonusdet.potpinjaman, 0),
                    0
                  )
                  WHERE
                    bonus.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  AND bonus.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )");

     
  }
}
