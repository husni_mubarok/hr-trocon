<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Controllers\Controller;
use App\Model\Item; 
use App\Model\Itembrand; 
use App\Model\Itemcategory; 
use App\Model\Branch;
use App\Model\User;
use App\Model\Purchaseorderdetail;
use App\Model\Purchaseorderdetaillog;
use App\Model\Currency;
use App\Model\Branchallocimport;
use App\Model\Container;
use App\Model\Week;
use Validator;
use Response;
use App\Post;
use View;
use Excel;

class BranchallocController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'capitalgood_name' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  { 

    $userid = Auth::id();

    $branch = Branch::orderBy('seq_no', 'ASC')->get();
 
    $capitalgood = Item::first();

    $currency_list = Currency::all();

    $week_list = Week::all();

    // $item_category = Itemcategory::all();
    $item_category = Itemcategory::orderBy('item_category_name', 'ASC')->get();
    $container = Container::orderBy('container_name', 'ASC')->get();

    $sql = 'SELECT
              item_category.item_category_name,
              purchase_order_detail.id,
              purchase_order.po_number,
              purchase_order.container_id,
              DATE_FORMAT(purchase_order.doc_date, "%d-%m-%Y") AS doc_date,
              DATE_FORMAT(purchase_order.gr_date, "%d-%m-%Y") AS gr_date,
              item.item_name,
              item.description,
              branch.add_modal,
              branch.branch_name AS plant,
              store_location.store_location_name,
              purchase_order.mat_doc,
              sum_qty_po.order_qty,
              purchase_order_detail.order_qty AS order_qty_po,
	            purchase_order_detail.nett_weight AS nett_weight_po,
              purchase_order_detail.gr_qty,
              purchase_order_detail.totalmodal,
              sum_qty_po.nett_weight AS nett_weight,
              purchase_order_detail.kurs,
              CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END AS ls,
              CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.plugin END AS plugin,
              CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.emkl END AS emkl_master,
              CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END AS kalog,
              FORMAT(IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0),0) AS rate, 
              (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal) AS rateidr, 
              pib.cost_pib,
              purchase_order_detail.currency_id,
              currency.currency_name,
              purchase_order_detail.idr,
              IFNULL(emkl_cost.emkl,0) AS modal_ctn, 

              purchase_order_detail.tarik, ';
       foreach ($branch as $key => $branchs) {
          $sql .= "IFNULL(purchase_order_detail." . $branchs->field_name . ",0) AS " . $branchs->field_name . ", ";
        };

      $sql .= "IFNULL(purchase_order_detail.order_qty,0) - (";
      foreach ($branch as $key => $branchs) {
          $sql .= "IFNULL(purchase_order_detail." . $branchs->field_name . ",0) + ";
        };
      $sql .= " 0) AS dif_qty, ";

      $sql .= 'container.container_no, container.container_name, container.mix, container.prefix
            FROM
              purchase_order
            INNER JOIN purchase_order_detail ON purchase_order_detail.purchase_order_id = purchase_order.id
            LEFT JOIN currency ON purchase_order_detail.currency_id = currency.id
            LEFT JOIN daily_rate ON CASE WHEN purchase_order_detail.currency_id IS NULL OR purchase_order_detail.currency_id = 0 THEN 1 ELSE purchase_order_detail.currency_id END = daily_rate.currency_id AND purchase_order.doc_date = daily_rate.date
            INNER JOIN item ON purchase_order_detail.item_id = item.id
            INNER JOIN item_category ON item.item_category_id = item_category.id
            INNER JOIN container ON purchase_order.container_id = container.id
            INNER JOIN branch ON purchase_order.plant = branch.id
            LEFT JOIN store_location ON purchase_order.store_location_id = store_location.id
            LEFT JOIN pib ON purchase_order.container_id = pib.container_id
        
            LEFT JOIN (SELECT
                SUM( purchase_order_detail.order_qty ) AS order_qty,
                SUM( purchase_order_detail.order_qty * CASE WHEN IFNULL(purchase_order_detail.nett_weight,0) = 0 THEN 1 ELSE purchase_order_detail.nett_weight END) AS nett_weight,
                purchase_order.container_id,
                purchase_order.doc_date 
              FROM
                purchase_order
                JOIN purchase_order_detail ON purchase_order.id = purchase_order_detail.purchase_order_id
                JOIN item ON purchase_order_detail.item_id = item.id 
              GROUP BY
                purchase_order.container_id,
                purchase_order.doc_date) AS sum_qty_po ON purchase_order.container_id = sum_qty_po.container_id AND purchase_order.doc_date = sum_qty_po.doc_date

            , user, emkl_cost
          WHERE CASE WHEN user.check_filter = 1 THEN (YEARWEEK(purchase_order.doc_date) = CONCAT(YEAR(NOW()),user.week)) ELSE (purchase_order.doc_date BETWEEN user.grfrom AND user.grto) END AND (purchase_order.doc_date BETWEEN emkl_cost.datefrom AND emkl_cost.dateto) AND (item_category.id = CASE WHEN user.item_category_id = 0 THEN item_category.id ELSE user.item_category_id END) AND (container.id = CASE WHEN user.container_id = 0 THEN container.id ELSE user.container_id END) AND (user.id = '.$userid.')  AND (pib.deleted_at IS NULL)  AND (purchase_order.deleted_at IS NULL) AND (purchase_order_detail.deleted_at IS NULL)';

    $capitalgood_detail = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('container_no', 'ASC')->orderBy('po_number', 'ASC')->orderBy('item_name', 'ASC')->get();

    $branch = Branch::orderBy('seq_no', 'ASC')->get();

    // $datafilter = User::where('id', Auth::id())->first();
    $sql1 = 'SELECT
              item_category.item_category_name,
              `user`.id,
              `user`.grfrom,
              `user`.grto,
              `user`.search_filter,
              `user`.item_category_id,
              `user`.container_id,
              `user`.check_filter,
              `user`.week,
              container.container_name
              FROM
              `user`
              LEFT JOIN item_category 
              ON `user`.item_category_id = item_category.id
              LEFT JOIN container
              ON `user`.container_id = container.id';

    $datafilter = DB::table(DB::raw("($sql1) as rs_sql"))->where('id', Auth::id())->first(); 



    return view ('editor.branchalloc.index', compact('capitalgood', 'capitalgood_detail', 'branch', 'datafilter', 'currency_list', 'week_list', 'item_category', 'container'));
  }

  public function needsave($id)
  { 
    $post = Purchaseorderdetail::Find($id); 
    $post->need_save_ba = 1;  
    $post->save();

    return response()->json($post); 
  }

  public function needsavecount()
  { 
    $post = Purchaseorderdetail::where('need_save_ba', '1')->first();  

    // return response()->json($post); 
    $details[] = $post;
    return $details;
  }

  public function resetneedsave()
  { 
    $post = DB::insert("UPDATE purchase_order_detail SET need_save_ba = NULL");
     return response()->json($post);
  }
   
  public function edit($id)
  {
    // $capitalgood = Item::Find($id);
     $sql = 'SELECT
                capitalgood_category.capitalgood_category_name,
                capitalgood_brand.capitalgood_brand_name,
                capitalgood.capitalgood_category_id,
                capitalgood.capitalgood_brand_id,
                capitalgood.id,
                capitalgood.capitalgood_code,
                capitalgood.capitalgood_name,
                capitalgood.description,
                capitalgood.status
              FROM
                capitalgood
              LEFT JOIN capitalgood_brand ON capitalgood.capitalgood_brand_id = capitalgood_brand.id
              LEFT JOIN capitalgood_category ON capitalgood.capitalgood_category_id = capitalgood_category.id';
      $capitalgood = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id)->first(); 
    echo json_encode($capitalgood); 
  }

  public function update(Request $request)
  {

      $branch = Branch::orderBy('seq_no', 'ASC')->get();

      foreach($request->input('detail') as $key => $detail_data)
      { 

        $old_data = Purchaseorderdetail::where('id', $key)->where('need_save_ba', '1')->first();

        if(isset($old_data)){
          $capitalgood_details = New Purchaseorderdetaillog; 
          $capitalgood_details->totalmodal = $detail_data['totalmodal']; 
          $capitalgood_details->currency_id = $detail_data['currency_id']; 
          $capitalgood_details->currency_id_to = $old_data->currency_id; 
          $capitalgood_details->totalmodal_to = $old_data->totalmodal; 
          $capitalgood_details->purchase_order_detail_id = $key;

          $capitalgood_details->updated_by = Auth::id();

          $capitalgood_details->save();
        }

        $capitalgood_details = Purchaseorderdetail::Find($key); 
        $capitalgood_details->currency_id = $detail_data['currency_id'];
        $capitalgood_details->totalmodal = $detail_data['totalmodal']; 
        $capitalgood_details->save();
    } 

    $post = DB::insert("UPDATE user SET read_notif = 1");
    
    return redirect()->back(); 

  }

  public function delete($id)
  {
    //dd($id);
    $post =  Item::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   
 
   foreach($idkey as $key => $id)
   {
    // $post =  Item::where('id', $id["1"])->get();
    $post = Item::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}

 public function storeexport(Request $request, $type)
  {

    // $data =  \DB::table('item') 
    // ->select('item_code', 'item_name')
    // ->get();
    $userid = Auth::id();

    $branch = Branch::orderBy('seq_no', 'ASC')->get();
 
    $capitalgood = Item::first();

    $currency_list = Currency::all();

    // $item_category = Itemcategory::all();
    $item_category = Itemcategory::orderBy('item_category_name', 'ASC')->get();
    

    $sql = 'SELECT
              item_category.item_category_name,
              purchase_order_detail.id,
              purchase_order.po_number,
              DATE_FORMAT(purchase_order.doc_date, "%d-%m-%Y") AS doc_date,
              DATE_FORMAT(purchase_order.gr_date, "%d-%m-%Y") AS gr_date,
              item.item_name,
              item.description,
              branch.branch_name AS plant,
              store_location.store_location_name,
              purchase_order.mat_doc,
              sum_qty_po.order_qty,
              purchase_order_detail.order_qty AS order_qty_po,
              purchase_order_detail.nett_weight AS nett_weight_po,
              purchase_order_detail.gr_qty,
              purchase_order_detail.totalmodal,
              sum_qty_po.nett_weight AS nett_weight,
              purchase_order_detail.kurs,
              CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END AS ls,
              CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.plugin END AS plugin,
              CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.emkl END AS emkl_master,
              CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END AS kalog,
              FORMAT(IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0),0) AS rate, 
              (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal) AS rateidr, 
              pib.cost_pib,
              purchase_order_detail.currency_id,
              currency.currency_name,
              purchase_order_detail.idr,
              IFNULL(emkl_cost.emkl,0) AS modal_ctn,

              (((IFNULL(pib.cost_pib,0) + CASE WHEN container.prefix = "AWB" THEN 0 ELSE   IFNULL(emkl_cost.emkl,0) + IFNULL(emkl_cost.plugin,0) + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) end + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END)  +
              (IFNULL(pib.cost_pib,0) + (CASE WHEN container.prefix = "AWB" THEN 0 ELSE   IFNULL(emkl_cost.emkl,0) + IFNULL(emkl_cost.plugin,0) + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) end + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END) * 0.001)) /(sum_qty_po.order_qty)) + (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal) AS total,

              ((((IFNULL(pib.cost_pib,0) + CASE WHEN container.prefix = "AWB" THEN 0 ELSE   IFNULL(emkl_cost.emkl,0) + IFNULL(emkl_cost.plugin,0) + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) end + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END)  +
              (IFNULL(pib.cost_pib,0) + (CASE WHEN container.prefix = "AWB" THEN 0 ELSE   IFNULL(emkl_cost.emkl,0) + IFNULL(emkl_cost.plugin,0) end + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END) * 0.001)) /(sum_qty_po.order_qty)) + (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal)) * 0.001  AS fee,

              (CASE WHEN container.prefix = "AWB" THEN 0 ELSE IFNULL(emkl_cost.emkl,0) END + CASE WHEN container.prefix = "AWB" THEN 0 ELSE IFNULL(emkl_cost.plugin,0) END + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END)  
               /(sum_qty_po.order_qty)  AS biaya_emkl,

              purchase_order_detail.tarik, ';
       foreach ($branch as $key => $branchs) {
          $sql .= "IFNULL(purchase_order_detail." . $branchs->field_name . ",0) AS " . $branchs->field_name . ", ";
        };

      $sql .= "IFNULL(purchase_order_detail.order_qty,0) - (";
      foreach ($branch as $key => $branchs) {
          $sql .= "IFNULL(purchase_order_detail." . $branchs->field_name . ",0) + ";
        };
      $sql .= " 0) AS dif_qty, ";

      $sql .= 'container.container_no
            FROM
              purchase_order
            INNER JOIN purchase_order_detail ON purchase_order_detail.purchase_order_id = purchase_order.id
            LEFT JOIN currency ON purchase_order_detail.currency_id = currency.id
            LEFT JOIN daily_rate ON CASE WHEN purchase_order_detail.currency_id IS NULL OR purchase_order_detail.currency_id = 0 THEN 1 ELSE purchase_order_detail.currency_id END = daily_rate.currency_id AND purchase_order.doc_date = daily_rate.date
            INNER JOIN item ON purchase_order_detail.item_id = item.id
            INNER JOIN item_category ON item.item_category_id = item_category.id
            INNER JOIN container ON purchase_order.container_id = container.id
            INNER JOIN branch ON purchase_order.plant = branch.id
            LEFT JOIN store_location ON purchase_order.store_location_id = store_location.id
            LEFT JOIN pib ON purchase_order.container_id = pib.container_id
        
            LEFT JOIN (SELECT
                SUM( purchase_order_detail.order_qty ) AS order_qty,
                SUM( purchase_order_detail.order_qty * CASE WHEN IFNULL(purchase_order_detail.nett_weight,0) = 0 THEN 1 ELSE purchase_order_detail.nett_weight END) AS nett_weight,
                purchase_order.container_id,
                purchase_order.doc_date 
              FROM
                purchase_order
                JOIN purchase_order_detail ON purchase_order.id = purchase_order_detail.purchase_order_id
                JOIN item ON purchase_order_detail.item_id = item.id 
              GROUP BY
                purchase_order.container_id,
                purchase_order.doc_date) AS sum_qty_po ON purchase_order.container_id = sum_qty_po.container_id AND purchase_order.doc_date = sum_qty_po.doc_date


            , user, emkl_cost
          WHERE (purchase_order.doc_date BETWEEN user.grfrom AND user.grto) AND (purchase_order.doc_date BETWEEN emkl_cost.datefrom AND emkl_cost.dateto) AND (item_category.id = CASE WHEN user.item_category_id = 0 THEN item_category.id ELSE user.item_category_id END) AND (user.id = '.$userid.')  AND pib.deleted_at IS NULL';

    $data = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('container_no', 'ASC')->orderBy('po_number', 'ASC')->orderBy('item_name', 'ASC')->get();

    // dd($data);
    $data = collect($data)->map(function($x){ return (array) $x; })->toArray();
    return Excel::create('lmu_branch_allocation_template', function($excel) use ($data) {
      $excel->sheet('mySheet', function($sheet) use ($data)
      {
        $sheet->fromArray($data);
      });
    })->download($type);
  }

  public function truncateimport()
  {
    Bra::query()->truncate();
    return redirect('editor/capitalgood/index');
  }

  public function storeimport(Request $request)
  { 
    Branchallocimport::query()->truncate();


    if($request->hasFile('import_file')){
      $path = $request->file('import_file')->getRealPath();
      $data = Excel::load($path, function($reader) {})->get();

      if(!empty($data) && $data->count()){
        foreach ($data->toArray() as $key => $value) {
          if(!empty($value)){
            foreach ($value as $v) {

              // $branch = Branch::orderBy('seq_no', 'ASC')->get();

              $insert[] = ['purchase_order_detail_id' => $v['id'], 'lmu_jakarta' => $v['lmu_jakarta'], 'lmu_surabaya' => $v['lmu_surabaya'], 'lmu_semarang' => $v['lmu_semarang'], 'lmu_palembang' => $v['lmu_palembang'], 'lmu_makassar' => $v['lmu_makassar'], 'lmu_manado' => $v['lmu_manado'], 'lmu_bali' => $v['lmu_bali'], 'lmu_samarinda' => $v['lmu_samarinda'], 'lmu_jakartaspm' => $v['lmu_jakartaspm'], 'category_name' => $v['item_category_name'], 'lmu_kupang' => $v['lmu_kupang'] ];
            }
          }
        } 

        if(!empty($insert)){
          Branchallocimport::insert($insert);

         DB::insert("UPDATE purchase_order_detail
                      INNER JOIN (
                        SELECT
                          branch_alloc_import.id,
                          branch_alloc_import.purchase_order_detail_id,
                          branch_alloc_import.lmu_jakarta,
                          branch_alloc_import.lmu_surabaya,
                          branch_alloc_import.lmu_semarang,
                          branch_alloc_import.lmu_palembang,
                          branch_alloc_import.lmu_makassar,
                          branch_alloc_import.lmu_manado,
                          branch_alloc_import.lmu_bali,
                          branch_alloc_import.lmu_samarinda,
                          branch_alloc_import.lmu_kupang,
                          branch_alloc_import.need_save_ba,
                          branch_alloc_import.need_save,
                          branch_alloc_import.lmu_jakartaspm
                        FROM
                          branch_alloc_import
                      ) AS branch_alloc_import
                      SET purchase_order_detail.lmu_jakarta = branch_alloc_import.lmu_jakarta,
                       purchase_order_detail.lmu_surabaya = branch_alloc_import.lmu_surabaya,
                       purchase_order_detail.lmu_semarang = branch_alloc_import.lmu_semarang,
                       purchase_order_detail.lmu_palembang = branch_alloc_import.lmu_palembang,
                       purchase_order_detail.lmu_makassar = branch_alloc_import.lmu_makassar,
                       purchase_order_detail.lmu_manado = branch_alloc_import.lmu_manado,
                       purchase_order_detail.lmu_bali = branch_alloc_import.lmu_bali,
                       purchase_order_detail.lmu_samarinda = branch_alloc_import.lmu_samarinda,
                       purchase_order_detail.lmu_kupang = branch_alloc_import.lmu_kupang,
                       purchase_order_detail.lmu_jakartaspm = branch_alloc_import.lmu_jakartaspm
                      WHERE
                        purchase_order_detail.id = branch_alloc_import.purchase_order_detail_id");
         
          return back()->with('success','Insert Record successfully.');
        }
      }
    }
    return back()->with('error','Please Check your file, Something is wrong there.');
  }

}
