<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Controllers\Controller;
use App\Model\Item; 
use App\Model\Itembrand; 
use App\Model\Itemcategory; 
use App\Model\Branch;
use App\Model\User;
use App\Model\Purchaseorderdetail;
use App\Model\Currency;
use App\Model\Week;
use Validator;
use Response;
use App\Post;
use View;
use Jenssegers\Agent\Agent as Agent;


class BranchallocationreportController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'capitalgood_name' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  { 

    $userid = Auth::id();

    // $branch = Branch::all();
 
    $capitalgood = Item::first();

    $currency_list = Currency::all();

    $week_list = Week::all();

    // $item_category = Itemcategory::all();
    $item_category = Itemcategory::orderBy('item_category_name', 'ASC')->get();
    

    $sql_branch_cb = 'SELECT
                        user_branch.user_id,
                        branch.id,
                        branch.branch_name,
                        branch.description,
                        branch.field_name,
                        branch.seq_no 
                      FROM
                        branch
                      LEFT JOIN user_branch ON branch.id = user_branch.branch_id
                      WHERE user_branch.user_id = '.Auth::id().' ORDER BY branch.seq_no ASC';
    $branch_cb = DB::table(DB::raw("($sql_branch_cb) as rs_sql"))->orderBy('seq_no', 'ASC')->get(); 

    $sql_branch_filter = 'SELECT
                        user_branch.user_id,
                        branch.id,
                        branch.branch_name,
                        branch.description,
                        branch.field_name,
                        branch.seq_no 
                      FROM
                        branch
                      LEFT JOIN user_branch ON branch.id = user_branch.branch_id 
                      INNER JOIN user ON user_branch.user_id = user.id AND branch.id = CASE WHEN user.branch_id = 0 THEN branch.id ELSE user.branch_id END
                      WHERE `user`.id = '.Auth::id().' ORDER BY branch.seq_no ASC';
    $branch = DB::table(DB::raw("($sql_branch_filter) as rs_sql"))->orderBy('seq_no', 'ASC')->get(); 

    // $datafilter = User::where('id', Auth::id())->first();
    $sql1 = 'SELECT
              item_category.item_category_name,
              branch.branch_name,
              `user`.id,
              `user`.grfrom,
              `user`.grto,
              `user`.branch_id,
              `user`.search,
              `user`.check_filter,
              `user`.week,
              `user`.item_category_id
              FROM
              `user`
              LEFT JOIN item_category
              ON `user`.item_category_id = item_category.id
              LEFT JOIN branch
              ON `user`.branch_id = branch.id';
    $datafilter = DB::table(DB::raw("($sql1) as rs_sql"))->where('id', Auth::id())->first(); 


    if (Input::has('page'))
       {
         $page = Input::get('page');    
       }
    else
       {
         $page = 1;
       }
    $no = 50*$page-49; 


    $sql = 'SELECT
              item_category.item_category_name,
              purchase_order_detail.id,
              purchase_order.po_number,
              DATE_FORMAT(purchase_order.doc_date, "%d-%m-%Y") AS doc_date,
              DATE_FORMAT(purchase_order.gr_date, "%d-%m-%Y") AS gr_date,
              item.item_code,
              item.item_name,
              IFNULL(item.description,"") AS description,
              branch.branch_name AS plant,
              branch.add_modal,
              store_location.store_location_name,
              purchase_order.mat_doc,
              sum_qty_po.order_qty,
              purchase_order_detail.gr_qty,
              purchase_order_detail.totalmodal,
              purchase_order_detail.kurs,
              CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END AS ls,
              CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.plugin END AS plugin,
              CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.emkl END AS emkl_master,
              CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END AS kalog,
              FORMAT(IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0),0) AS rate, 
              (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal) AS rateidr, 
              pib.cost_pib,
              pib.eta,
              purchase_order_detail.currency_id,
              currency.currency_name,
              purchase_order_detail.idr,
              pib.emkl,
              
              IFNULL(emkl_cost.emkl,0) AS modal_ctn,

              (((IFNULL(pib.cost_pib,0) + CASE WHEN container.prefix = "AWB" THEN 0 ELSE   IFNULL(emkl_cost.emkl,0) + IFNULL(emkl_cost.plugin,0) + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) end + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END)  +
              (IFNULL(pib.cost_pib,0) + (CASE WHEN container.prefix = "AWB" THEN 0 ELSE   IFNULL(emkl_cost.emkl,0) + IFNULL(emkl_cost.plugin,0) + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) end + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END) * 0.001)) /(sum_qty_po.order_qty)) + (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal) AS total,

              ((((IFNULL(pib.cost_pib,0) + CASE WHEN container.prefix = "AWB" THEN 0 ELSE   IFNULL(emkl_cost.emkl,0) + IFNULL(emkl_cost.plugin,0) + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) end + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END)  +
              (IFNULL(pib.cost_pib,0) + (CASE WHEN container.prefix = "AWB" THEN 0 ELSE   IFNULL(emkl_cost.emkl,0) + IFNULL(emkl_cost.plugin,0) end + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END) * 0.001)) /(sum_qty_po.order_qty)) + (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal)) * 0.001  AS fee,

              (CASE WHEN container.prefix = "AWB" THEN 0 ELSE IFNULL(emkl_cost.emkl,0) END + CASE WHEN container.prefix = "AWB" THEN 0 ELSE IFNULL(emkl_cost.plugin,0) END + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END)  
               /(sum_qty_po.order_qty)  AS biaya_emkl,

              purchase_order_detail.tarik, ';
       foreach ($branch as $key => $branchs) {
          $sql .= "IFNULL(purchase_order_detail." . $branchs->field_name . ",0) AS " . $branchs->field_name . ", ";
        };

      $sql .= "IFNULL(purchase_order_detail.order_qty,0) - (";
      foreach ($branch as $key => $branchs) {
          $sql .= "IFNULL(purchase_order_detail." . $branchs->field_name . ",0) + ";
        };
      $sql .= " 0) AS dif_qty, ";

      $sql .= 'IFNULL(container.container_no,"") AS container_no, IFNULL(container.prefix,"") AS prefix, CONCAT(IFNULL(container.prefix,""), IFNULL(container.container_no,"")) AS containerfull
            FROM
              purchase_order
            INNER JOIN purchase_order_detail ON purchase_order_detail.purchase_order_id = purchase_order.id
            LEFT JOIN currency ON purchase_order_detail.currency_id = currency.id
            LEFT JOIN daily_rate ON purchase_order_detail.currency_id = daily_rate.currency_id AND purchase_order.doc_date = daily_rate.date
            INNER JOIN item ON purchase_order_detail.item_id = item.id
            INNER JOIN item_category ON item.item_category_id = item_category.id
            INNER JOIN container ON purchase_order.container_id = container.id
            INNER JOIN branch ON purchase_order.plant = branch.id
            LEFT JOIN store_location ON purchase_order.store_location_id = store_location.id
            LEFT JOIN pib ON purchase_order.container_id = pib.container_id
        
            LEFT JOIN (SELECT
                SUM( purchase_order_detail.order_qty ) AS order_qty,
                purchase_order.container_id,
                purchase_order.doc_date 
              FROM
                purchase_order
                JOIN purchase_order_detail ON purchase_order.id = purchase_order_detail.purchase_order_id
                JOIN item ON purchase_order_detail.item_id = item.id 
              GROUP BY
                purchase_order.container_id,
                purchase_order.doc_date) AS sum_qty_po ON purchase_order.container_id = sum_qty_po.container_id AND purchase_order.doc_date = sum_qty_po.doc_date


            , user, emkl_cost
          WHERE  CASE WHEN user.check_filter = 1 THEN (YEARWEEK(purchase_order.doc_date) = CONCAT(YEAR(NOW()),user.week)) ELSE (pib.eta BETWEEN user.grfrom AND user.grto) END AND  CASE WHEN user.check_filter = 1 THEN (WEEK(purchase_order.doc_date) = user.week) ELSE (purchase_order.doc_date BETWEEN emkl_cost.datefrom AND emkl_cost.dateto) END AND (item_category.id = CASE WHEN user.item_category_id = 0 THEN item_category.id ELSE user.item_category_id END) AND (user.id = '.$userid.') AND (pib.deleted_at IS NULL)  AND (purchase_order.deleted_at IS NULL) AND (purchase_order_detail.deleted_at IS NULL)';

    $capitalgood_detail = DB::table(DB::raw("($sql) as rs_sql"))->where('po_number', 'LIKE', '%'.$datafilter->search.'%')->orwhere('containerfull', 'LIKE', '%'.$datafilter->search.'%')->orwhere('plant', 'LIKE', '%'.$datafilter->search.'%')->orwhere('description', 'LIKE', '%'.escape_like($datafilter->search).'%')->orderBy('container_no', 'ASC')->orderBy('po_number', 'ASC')->orderBy('item_name', 'ASC')->get();

    // $branch = Branch::all();

    $Agent = new Agent();
    if ($Agent->isMobile()) {
        return view ('editor.branchallocationreport.indexmobile', compact('capitalgood', 'capitalgood_detail', 'branch', 'datafilter', 'currency_list', 'item_category', 'branch_cb', 'week_list'));
    } else {
        return view ('editor.branchallocationreport.index', compact('capitalgood', 'capitalgood_detail', 'branch', 'datafilter', 'currency_list', 'item_category', 'branch_cb', 'week_list'));
    };
  }
   
}
