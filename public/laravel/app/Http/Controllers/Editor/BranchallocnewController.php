<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Controllers\Controller;
use App\Model\Item; 
use App\Model\Itembrand; 
use App\Model\Itemcategory; 
use App\Model\Branch;
use App\Model\Purchaseorderdetail;
use App\Model\Purchaseorderdetaillog;
use App\Model\User;
use App\Model\Container;
use App\Model\Week;
use Validator;
use Response;
use App\Post;
use View;
use Excel;

class BranchallocnewController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'capitalgood_name' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  { 

    $userid = Auth::id();

    $branch = Branch::orderBy('seq_no', 'ASC')->get();
    // dd($branch);

    $item_category = Itemcategory::orderBy('item_category_name', 'ASC')->get();
    $container = Container::orderBy('container_name', 'ASC')->get();

    $week_list = Week::all();
 
    $capitalgood = Item::first();

    $sql1 = 'SELECT
      `user`.id,
      `user`.search
    FROM
      `user`';
    $datafilter = DB::table(DB::raw("($sql1) as rs_sql"))->where('id', Auth::id())->first(); 

     $sql = 'SELECT
              item_category.item_category_name,
              purchase_order_detail.id,
              purchase_order.po_number,
              purchase_order.container_id,
              DATE_FORMAT(purchase_order.doc_date, "%d-%m-%Y") AS doc_date,
              DATE_FORMAT(purchase_order.gr_date, "%d-%m-%Y") AS gr_date,
              item.item_name,
              item.description,
              branch.branch_name AS plant,
              branch.add_modal,
              store_location.store_location_name,
              purchase_order.mat_doc,
              sum_qty_po.order_qty,
              purchase_order_detail.order_qty AS order_qty_po,
              purchase_order_detail.gr_qty,
              purchase_order_detail.totalmodal,
              purchase_order_detail.kurs,
              CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END AS ls,
              CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.plugin END AS plugin,
              CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.emkl END AS emkl_master,
              CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END AS kalog,
              FORMAT(IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0),0) AS rate, 
              (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal) AS rateidr, 
              pib.cost_pib,
              pib.eta,
              purchase_order_detail.currency_id,
              currency.currency_name,
              purchase_order_detail.idr,
              IFNULL(emkl_cost.emkl,0) AS modal_ctn,

              (((IFNULL(pib.cost_pib,0) + CASE WHEN container.prefix = "AWB" THEN 0 ELSE   IFNULL(emkl_cost.emkl,0) + IFNULL(emkl_cost.plugin,0) + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) end + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END)  +
              (IFNULL(pib.cost_pib,0) + (CASE WHEN container.prefix = "AWB" THEN 0 ELSE   IFNULL(emkl_cost.emkl,0) + IFNULL(emkl_cost.plugin,0) + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) end + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END) * 0.001)) /(sum_qty_po.order_qty)) + (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal) AS total,

              ((((IFNULL(pib.cost_pib,0) + CASE WHEN container.prefix = "AWB" THEN 0 ELSE   IFNULL(emkl_cost.emkl,0) + IFNULL(emkl_cost.plugin,0) + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) end + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END)  +
              (IFNULL(pib.cost_pib,0) + (CASE WHEN container.prefix = "AWB" THEN 0 ELSE   IFNULL(emkl_cost.emkl,0) + IFNULL(emkl_cost.plugin,0) end + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END) * 0.001)) /(sum_qty_po.order_qty)) + (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal)) * 0.001  AS fee,

              (CASE WHEN container.prefix = "AWB" THEN 0 ELSE IFNULL(emkl_cost.emkl,0) END + CASE WHEN container.prefix = "AWB" THEN 0 ELSE IFNULL(emkl_cost.plugin,0) END + IFNULL(CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END,0) + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END)  
               /(sum_qty_po.order_qty)  AS biaya_emkl,

              purchase_order_detail.tarik, ';
       foreach ($branch as $key => $branchs) {
          $sql .= "IFNULL(purchase_order_detail." . $branchs->field_name . ",0) AS " . $branchs->field_name . ", ";
        };

      $sql .= "CASE WHEN purchase_order_detail.gr_qty = 0 THEN purchase_order_detail.order_qty ELSE purchase_order_detail.gr_qty END - (";
      foreach ($branch as $key => $branchs) {
          $sql .= "IFNULL(purchase_order_detail." . $branchs->field_name . ",0) + ";
        };
      $sql .= " 0) AS dif_qty, ";

      $sql .= 'container.container_no, container.container_name, container.prefix
            FROM
              purchase_order
            INNER JOIN purchase_order_detail ON purchase_order_detail.purchase_order_id = purchase_order.id
            LEFT JOIN currency ON purchase_order_detail.currency_id = currency.id
            LEFT JOIN daily_rate ON purchase_order_detail.currency_id = daily_rate.currency_id AND purchase_order.doc_date = daily_rate.date
            INNER JOIN item ON purchase_order_detail.item_id = item.id
            INNER JOIN item_category ON item.item_category_id = item_category.id
            INNER JOIN container ON purchase_order.container_id = container.id
            INNER JOIN branch ON purchase_order.plant = branch.id
            LEFT JOIN store_location ON purchase_order.store_location_id = store_location.id
            LEFT JOIN pib ON purchase_order.container_id = pib.container_id
        
            LEFT JOIN (SELECT
                SUM( purchase_order_detail.order_qty ) AS order_qty,
                purchase_order.container_id,
                purchase_order.doc_date 
              FROM
                purchase_order
                JOIN purchase_order_detail ON purchase_order.id = purchase_order_detail.purchase_order_id
                JOIN item ON purchase_order_detail.item_id = item.id 
              GROUP BY
                purchase_order.container_id,
                purchase_order.doc_date) AS sum_qty_po ON purchase_order.container_id = sum_qty_po.container_id AND purchase_order.doc_date = sum_qty_po.doc_date


            , user, emkl_cost
          WHERE CASE WHEN user.check_filter = 1 THEN (YEARWEEK(purchase_order.doc_date) = CONCAT(YEAR(NOW()),user.week)) ELSE  (purchase_order.doc_date BETWEEN user.grfrom AND user.grto) END AND (purchase_order.doc_date BETWEEN emkl_cost.datefrom AND emkl_cost.dateto) AND (item_category.id = CASE WHEN user.item_category_id = 0 THEN item_category.id ELSE user.item_category_id END) AND (container.id = CASE WHEN user.container_id = 0 THEN container.id ELSE user.container_id END) AND (user.id = '.$userid.')  AND (pib.deleted_at IS NULL)  AND (purchase_order.deleted_at IS NULL) AND (purchase_order_detail.deleted_at IS NULL) AND (item_category.item_category_name LIKE "%'.$datafilter->search.'%" OR purchase_order.po_number LIKE "%'.$datafilter->search.'%" OR item.item_name LIKE "%'.$datafilter->search.'%" OR currency.currency_name LIKE "%'.$datafilter->search.'%" OR container.container_no LIKE "%'.$datafilter->search.'%")';

    $capitalgood_detail = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('container_no', 'ASC')->orderBy('po_number', 'ASC')->orderBy('item_name', 'ASC')->paginate(10);
    $branch = Branch::orderBy('seq_no', 'ASC')->get();

    // $datafilter = User::where('id', Auth::id())->first();

     $sql1 = 'SELECT
              item_category.item_category_name,
              `user`.id,
              `user`.grfrom,
              `user`.grto,
              `user`.search_filter,
              `user`.search,
              `user`.item_category_id,
              `user`.container_id,
              `user`.check_filter,
              `user`.week,
              container.container_name
              FROM
              `user`
              LEFT JOIN item_category 
              ON `user`.item_category_id = item_category.id
              LEFT JOIN container
              ON `user`.container_id = container.id';

    $datafilter = DB::table(DB::raw("($sql1) as rs_sql"))->where('id', Auth::id())->first(); 


    return view ('editor.branchallocnew.index', compact('capitalgood', 'capitalgood_detail', 'branch', 'datafilter', 'item_category', 'container', 'week_list'));
  }
  
  public function needsave($id)
  { 
    $post = Purchaseorderdetail::Find($id); 
    $post->need_save = 1;  
    $post->save();

    return response()->json($post); 
  }

  public function needsavecount()
  { 
    $post = Purchaseorderdetail::where('need_save', '1')->first();  

    // return response()->json($post); 
    $details[] = $post;
    return $details;
  }

  public function resetneedsave()
  { 
    $post = DB::insert("UPDATE purchase_order_detail SET need_save = NULL");
     return response()->json($post);
  }
  

  public function update(Request $request)
  {
    ini_set('max_input_vars', 10000);

    $branch = Branch::orderBy('seq_no', 'ASC')->get();

    foreach($request->input('detail') as $key => $detail_data)
    { 
      $old_data = Purchaseorderdetail::where('id', $key)->where('need_save', '1')->first();

      if(isset($old_data)){
        $capitalgood_details = New Purchaseorderdetaillog; 
        $capitalgood_details->tarik = $detail_data['tarik']; 
        $capitalgood_details->purchase_order_detail_id = $key;
        foreach ($branch as $key => $branchs) {
          $field = $branchs->field_name;
          $fieldto = $branchs->field_name."_to";
          $capitalgood_details->$field = $old_data->$field; 
          $capitalgood_details->$fieldto = $detail_data[$branchs->field_name]; 
        };

        $capitalgood_details->updated_by = Auth::id();

        $capitalgood_details->save();
      }
    } 

    foreach($request->input('detail') as $key => $detail_data)
    { 
      $capitalgood_details = Purchaseorderdetail::Find($key); 
      $capitalgood_details->tarik = $detail_data['tarik']; 
      foreach ($branch as $key => $branchs) {
        $field = $branchs->field_name;
        $capitalgood_details->$field = $detail_data[$branchs->field_name]; 
      };

      $capitalgood_details->save();
    } 

    $post = DB::insert("UPDATE user SET read_notif = 1");


    return redirect()->back(); 

  }

  public function editx($id)
  {
    $capitalgoodnew = Purchaseorderdetail::Find($id);
    echo json_encode($capitalgoodnew); 
  }

  public function updatex(Request $request)
  {

    $old_data = Purchaseorderdetail::where('id', $request->pk)->first();


    $name = $request->name;

    $to = $name.'_to';

    $branchallocnews = New Purchaseorderdetaillog; 
    $branchallocnews->purchase_order_detail_id = $request->pk;
    $branchallocnews->$name = $old_data->$name; 
    $branchallocnews->$to = $request->value; 
    $branchallocnews->updated_by = Auth::id();
    $branchallocnews->save();

    $branchallocnew = Purchaseorderdetail::Find($request->pk);
    $branchallocnew->$name = $request->value; 
    $branchallocnew->need_save = 1; 
    $branchallocnew->save();

    $post = DB::insert("UPDATE user SET read_notif = 1");

  }

  public function delete($id)
  {
    //dd($id);
    $post =  Item::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {
     $idkey = $request->idkey;   
   
     foreach($idkey as $key => $id)
     {
      // $post =  Item::where('id', $id["1"])->get();
      $post = Item::Find($id["1"]);
      $post->delete(); 
    }
    echo json_encode(array("status" => TRUE));
  }
   
}
