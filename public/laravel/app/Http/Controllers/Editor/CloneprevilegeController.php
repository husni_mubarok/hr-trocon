<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Controllers\Controller;
use App\Model\Item; 
use App\Model\Itembrand; 
use App\Model\Itemcategory; 
use App\Model\Branch;
use App\Model\User;
use App\Model\Purchaseorderdetail;
use App\Model\Purchaseorderdetaillog;
use App\Model\Currency;
use App\Model\Branchallocimport;
use App\Model\Container;
use Validator;
use Response;
use App\Post;
use View;
use Excel;

class CloneprevilegeController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'item_name' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $userid = Auth::id();

    $user_list = User::all(); 

    return view ('editor.cloneprevilege.index', compact('user_list'));
  }



   public function store(Request $request)
  { 
      
     DB::insert("DELETE FROM privilege where user_id = ".$request->input('user_list_id_to')."");
     DB::insert("INSERT INTO privilege (user_id, module_id, action_id) SELECT ".$request->input('user_list_id_to').", module_id, action_id FROM privilege where user_id = ".$request->input('user_list_id_from')."");
     
      return back()->with('success','Insert Record successfully.');
      } 
     
    }
