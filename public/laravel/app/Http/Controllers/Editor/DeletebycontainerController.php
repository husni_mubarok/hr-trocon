<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ContainerRequest;
use App\Http\Controllers\Controller;
use App\Model\Container; 
use App\Model\Userlog; 
use Carbon\Carbon;
use Validator;
use Response;
use App\Post;
use View;
// use App\Notifications\ToDb;
use Illuminate\Notifications\Notifiable;


class DeletebycontainerController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'containername' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    return view ('editor.deletebycontainer.index');
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 

      $sql = 'SELECT
                purchase_order.container_id,
                container.prefix,
                container.container_no,
                container.container_name,
                container.description,
                container.`status`,
                container.deleted_at 
              FROM
                purchase_order
                JOIN container ON purchase_order.container_id = container.id 
              WHERE
                purchase_order.deleted_at IS NULL 
              GROUP BY
                purchase_order.container_id';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->container_no."', ".$itemdata->container_id."".')"> Delete</a>';
      })
 
      ->make(true);
    } else {
      exit("No data available");
    }
  }
 

  public function delete($id)
  { 

    $user_id = Auth::id();

    DB::update("UPDATE purchase_order SET deleted_at = NOW(), deleted_by = $user_id WHERE container_id = $id");

  }
   
}
