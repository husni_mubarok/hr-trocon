<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ContainerRequest;
use App\Http\Controllers\Controller;
use App\Model\Container; 
use App\Model\Userlog; 
use Carbon\Carbon;
use Validator;
use Response;
use App\Post;
use View;
// use App\Notifications\ToDb;
use Illuminate\Notifications\Notifiable;


class DeletebyitemController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'containername' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    return view ('editor.deletebyitem.index');
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 

      $sql = 'SELECT
                item.id,
                item.item_code,
                item.item_name 
              FROM
                purchase_order
                JOIN purchase_order_detail ON purchase_order.id = purchase_order_detail.purchase_order_id
                JOIN item ON purchase_order_detail.item_id = item.id 
              WHERE
                purchase_order.deleted_at IS NULL AND purchase_order_detail.deleted_at IS NULL';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->item_name."', ".$itemdata->id."".')"> Delete</a>';
      })
 
      ->make(true);
    } else {
      exit("No data available");
    }
  }
 

  public function delete($id)
  { 
    $user_id = Auth::id();

    DB::update("UPDATE purchase_order_detail SET deleted_at = NOW(), deleted_by = $user_id WHERE item_id = $id");

  }
   
}
