<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ContainerRequest;
use App\Http\Controllers\Controller;
use App\Model\Container; 
use App\Model\Userlog; 
use Carbon\Carbon;
use Validator;
use Response;
use App\Post;
use View;
// use App\Notifications\ToDb;
use Illuminate\Notifications\Notifiable;


class DeletebypoController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'containername' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    return view ('editor.deletebypo.index');
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 

      $sql = 'SELECT
                purchase_order.po_number 
              FROM
                purchase_order 
              WHERE
                purchase_order.deleted_at IS NULL 
              GROUP BY
                purchase_order.po_number';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->po_number."'".')"> Delete</a>';
      })
 
      ->make(true);
    } else {
      exit("No data available");
    }
  }
 

  public function delete($id)
  {
    // $post =  Container::Find($id);
    // $post->delete(); 

    $user_id = Auth::id();

    DB::update("UPDATE purchase_order SET deleted_at = NOW(), deleted_by = $user_id WHERE po_number = $id");

    // return response()->json($post); 
  }
   
}
