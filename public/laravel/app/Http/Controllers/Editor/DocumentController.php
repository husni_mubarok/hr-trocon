<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\DocumentRequest;
use App\Http\Controllers\Controller;
use App\Model\Document; 
use Validator;
use Response;
use App\Post;
use View;

class DocumentController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'documentno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'documentname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $documents = Document::all();
      return view ('editor.document.index', compact('documents'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 
        $itemdata = Document::orderBy('documentname', 'ASC')->get();

        return Datatables::of($itemdata) 

        ->addColumn('action', function ($itemdata) {
          return '<a href="document/'.$itemdata->id.'/edit" title="Edit")"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->documentname."'".')"> Hapus</a>';
        })

        ->addColumn('attachment', function ($itemdata) {
          if ($itemdata->attachment == null) {
            return '';
          }else{
           return '<a href="../uploads/document/'.$itemdata->attachment.'" target="_blank"/><i class="fa fa-download"></i> Download</a>';
         };  
       })
        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> Aktif </span>';
          }else{
           return '<span class="label label-danger"> Tidak Aktif </span>';
         };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function create()
    { 
      return view ('editor.document.form');
    }

    public function store(Request $request)
    { 
      $validator = Validator::make(Input::all(), $this->rules);
      if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
      } else {
       $document = new Document; 
       $document->documentno = $request->input('documentno');
       $document->documentname = $request->input('documentname');
       $document->documentdate = $request->input('documentdate');
       $document->expireddate = $request->input('expireddate');
       $document->dayprocess = $request->input('dayprocess');
       $document->indate = $request->input('indate'); 
       $document->year = $request->input('year'); 
       $document->amount = $request->input('amount'); 
       $document->created_by = Auth::id();
       $document->save();

       if($request->attachment)
       {
        $document = Document::FindOrFail($document->id);
        $original_directory = "uploads/document/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $document->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $document->attachment);
          $document->save(); 
        } 
        return redirect('editor/document'); 
      }
    }

    public function edit($id)
    {
      $document = Document::Where('id', $id)->first();    
      return view ('editor.document.form', compact('document'));
    }

    public function update($id, Request $request)
    {
      $document = Document::Find($id);
      $document->documentno = $request->input('documentno');
      $document->documentname = $request->input('documentname');
      $document->documentdate = $request->input('documentdate');
      $document->expireddate = $request->input('expireddate');
      $document->dayprocess = $request->input('dayprocess');
      $document->indate = $request->input('indate'); 
      $document->year = $request->input('year'); 
      $document->amount = $request->input('amount'); 
      $document->created_by = Auth::id();
      $document->save();

      if($request->attachment)
      {
        $document = Document::FindOrFail($document->id);
        $original_directory = "uploads/document/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $document->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $document->attachment);
          $document->save(); 
        } 
        return redirect('editor/document');  
      }

      public function delete($id)
      {
    //dd($id);
        $post =  Document::Find($id);
        $post->delete(); 

        return response()->json($post); 
      }

      public function deletebulk(Request $request)
      {

       $idkey = $request->idkey;   

  //$count = count($idkey);

//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

       foreach($idkey as $key => $id)
       {
    // $post =  Document::where('id', $id["1"])->get();
        $post = Document::Find($id["1"]);
        $post->delete(); 
      }

      echo json_encode(array("status" => TRUE));

    }
  }
