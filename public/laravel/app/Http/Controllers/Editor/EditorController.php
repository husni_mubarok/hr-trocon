<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Payroll;
use App\Model\Popup;
use DB;

class EditorController extends Controller
{
    public function index()
    {
	 $birthday = \DB::select(\DB::raw("
            SELECT
				employee.employeename,
				employee.sex,
				department.departmentname,
				employee.image, 
				DATE_FORMAT(employee.datebirth, '%d %M') AS datebirth
			FROM
				employee
			LEFT JOIN department ON employee.departmentid = department.id
			WHERE
				MONTH (datebirth) = MONTH (NOW())
        "));

	 $new_hire = \DB::select(\DB::raw("
				           SELECT
					employee.employeename,
					employee.sex,
					department.departmentname,
					DATE_FORMAT(employee.termdate, '%d %M') AS termdate,
					DATE_FORMAT(
						employee.joindate,
						'%d %M %Y'
					) AS joindate,
					employee.image
				FROM
					employee
				LEFT JOIN department ON employee.departmentid = department.id
				ORDER BY
					employee.joindate DESC
				LIMIT 4"));

	 $leave = \DB::select(\DB::raw("
            SELECT
				employee.employeename,
				DATE_FORMAT(
					leaving.leavingfrom,
					'%d-%m-%Y'
				) AS fromdate,
				DATE_FORMAT(
					leaving.leavingto,
					'%d-%m-%Y'
				) AS todate,
				department.departmentname
			FROM
				leaving 
			LEFT JOIN employee ON leaving.employeeid = employee.id
			LEFT JOIN department ON employee.departmentid = employee.employeename
			LIMIT 3
        "));

	  $contract = \DB::select(\DB::raw("
            SELECT
				employee.employeename,
				employee.sex,
				department.departmentname,
				DATE_FORMAT(employee.termdate, '%d-%m-%Y') AS termdate,
				DATE_FORMAT(
					employee.joindate, '%d-%m-%Y'
				) AS joindate,
				employee.image,
				DATEDIFF(
					employee.termdate,
					employee.joindate
				) AS days
			FROM
				employee
			LEFT JOIN department ON employee.departmentid = department.id
			WHERE
				DATEDIFF(
					employee.termdate,
					employee.joindate
				) IS NOT NULL
			ORDER BY
				DATEDIFF(
					employee.termdate,
					employee.joindate
				) ASC
			LIMIT 4
        "));

	  $news = \DB::select(\DB::raw("
            SELECT
				news.id,
				news.title,
				news.content,
				DATE_FORMAT(news.date, '%d-%m-%Y') AS date,
				news.time,
				news.attachment,
				news.`status`
			FROM
				news
			LIMIT 4
        "));

	 $sql = 'SELECT
				popup.id,
				popup.popup_name,
				popup.description,
				popup.date_popup, 
				DATE_FORMAT(NOW(), "%Y-%m-%d") AS date_now 
			FROM
				popup';
	$popup = DB::table(DB::raw("($sql) as rs_sql"))->first(); 

    	return view ('editor.index', compact('birthday', 'leave', 'new_hire', 'contract', 'news', 'popup'));
    }

    
}
