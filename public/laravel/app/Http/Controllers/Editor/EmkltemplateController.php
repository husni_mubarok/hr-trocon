<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Controllers\Controller;
use App\Model\Item; 
use App\Model\Itembrand; 
use App\Model\Itemcategory; 
use Validator;
use Response;
use App\Post;
use View;
use Jenssegers\Agent\Agent as Agent;


class EmkltemplateController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'item_name' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $sql1 = 'SELECT
              item_category.item_category_name,
              `user`.id,
              `user`.grfrom,
              `user`.grto,
              `user`.item_category_id
              FROM
              `user`
              LEFT JOIN item_category
              ON `user`.item_category_id = item_category.id';

    $datafilter = DB::table(DB::raw("($sql1) as rs_sql"))->where('id', Auth::id())->first(); 

    $Agent = new Agent();
    if ($Agent->isMobile()) {
        return view ('editor.emkltemplate.indexmobile', compact('datafilter'));
    } else {
        return view ('editor.emkltemplate.index', compact('datafilter'));
    };
    
  }

  public function data(Request $request)
  {   
    $userid = Auth::id();
    if($request->ajax()){ 
       $sql = 'SELECT
              item_category.item_category_name,
              item_category.ls_status,
              purchase_order_detail.id,
              purchase_order.po_number,
              DATE_FORMAT(purchase_order.doc_date, "%d-%m-%Y") AS doc_date,
              DATE_FORMAT(purchase_order.gr_date, "%d-%m-%Y") AS gr_date,
              item.item_name,
              item.description,
              branch.branch_name AS plant,
              store_location.store_location_name,
              purchase_order.mat_doc,
              sum_qty_po.order_qty,
              sum_qty_po.nett_weight,
              purchase_order_detail.gr_qty,
              purchase_order_detail.totalmodal,
              purchase_order_detail.kurs,
              FORMAT(IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0),0) AS rate, 
              FORMAT((IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal),0) AS rateidr, 
              purchase_order_detail.currency_id,
              currency.currency_name,
              purchase_order_detail.idr,
              CASE WHEN container.prefix = "AWB"  OR item_category.ls_status IS NULL THEN 0 ELSE emkl_cost.ls END AS ls,
              CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.plugin END AS plugin,
              CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.emkl END AS emkl_master,
              CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END AS kalog,
              pib.no_pib,
              pib.cost_pib,
              pib.emkl AS emkl_name,
              -- IFNULL(emkl_cost.emkl,0) AS modal_ctn,

              (((IFNULL(pib.cost_pib,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.emkl END,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.plugin END,0) + IFNULL(emkl_cost.ls,0) + IFNULL(emkl_cost.kalog,0))  +
              (IFNULL(pib.cost_pib,0) + (IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.emkl END,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.plugin END,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.ls END,0) + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END) * 0.001)) /(sum_qty_po.order_qty)) + (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal) AS total,

              ((((IFNULL(pib.cost_pib,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.emkl END,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.plugin END,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.ls END,0) + IFNULL(emkl_cost.kalog,0))  +
              (IFNULL(pib.cost_pib,0) + (IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.emkl END,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.plugin END,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.ls END,0) + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END) * 0.001)) /(sum_qty_po.order_qty)) + (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal)) * 0.001  AS fee,

              ((IFNULL(pib.cost_pib,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.emkl END,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.plugin END,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.ls END,0) + CASE WHEN purchase_order_detail.tarik = 1 THEN emkl_cost.kalog ELSE 0 END)  +
              (IFNULL(pib.cost_pib,0) + (IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.emkl END,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.plugin END,0) + IFNULL(CASE WHEN container.prefix = "AWB" THEN 0 ELSE emkl_cost.ls END,0) + IFNULL(emkl_cost.kalog,0)) * 0.001)) /(sum_qty_po.order_qty)  AS emkl,
              purchase_order_detail.tarik, ';

      $sql .= 'container.container_no, container.prefix, CONCAT(container.prefix, " ", container.container_no) AS prefix_container
            FROM
              purchase_order
            INNER JOIN purchase_order_detail ON purchase_order_detail.purchase_order_id = purchase_order.id
            LEFT JOIN currency ON purchase_order_detail.currency_id = currency.id
            LEFT JOIN daily_rate ON purchase_order_detail.currency_id = daily_rate.currency_id AND purchase_order.doc_date = daily_rate.date
            INNER JOIN item ON purchase_order_detail.item_id = item.id
            INNER JOIN item_category ON item.item_category_id = item_category.id
            INNER JOIN container ON purchase_order.container_id = container.id
            INNER JOIN branch ON purchase_order.plant = branch.id
            LEFT JOIN store_location ON purchase_order.store_location_id = store_location.id
            LEFT JOIN pib ON purchase_order.container_id = pib.container_id
        
            LEFT JOIN (SELECT
                SUM( purchase_order_detail.order_qty ) AS order_qty,
                SUM( purchase_order_detail.order_qty * CASE WHEN IFNULL(purchase_order_detail.nett_weight,0) = 0 THEN 1 ELSE purchase_order_detail.nett_weight END) AS nett_weight,
                purchase_order.container_id,
                purchase_order.doc_date 
              FROM
                purchase_order
                JOIN purchase_order_detail ON purchase_order.id = purchase_order_detail.purchase_order_id
                JOIN item ON purchase_order_detail.item_id = item.id 
              GROUP BY
                purchase_order.container_id,
                purchase_order.doc_date) AS sum_qty_po ON purchase_order.container_id = sum_qty_po.container_id AND purchase_order.doc_date = sum_qty_po.doc_date
            , user, emkl_cost
          WHERE (purchase_order.doc_date BETWEEN user.grfrom AND user.grto) AND (purchase_order.doc_date BETWEEN emkl_cost.datefrom AND emkl_cost.dateto) AND (user.id = '.$userid.') AND (pib.deleted_at IS NULL)  AND (purchase_order.deleted_at IS NULL) AND (purchase_order_detail.deleted_at IS NULL)
          GROUP BY CONCAT(container.prefix, " ", container.container_no), item_category.item_category_name
          ';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('emklctn', function ($itemdata) {
        // return ''.$itemdata->total + $itemdata->fee.'';
        return ''.($itemdata->cost_pib + $itemdata->emkl_master + $itemdata->plugin + $itemdata->ls + $itemdata->kalog) / $itemdata->order_qty.'';
      })

      ->addColumn('emklkg', function ($itemdata) {
        // return ''.$itemdata->total + $itemdata->fee.'';
        return ''.($itemdata->cost_pib + $itemdata->emkl_master + $itemdata->plugin + $itemdata->ls + $itemdata->kalog) / $itemdata->nett_weight.'';
      })

        ->addColumn('prefix_container2', function ($itemdata) {
          return ''.$itemdata->prefix.' '.$itemdata->container_no.'';
         
        })

      ->addColumn('total_modal', function ($itemdata) {
          return ''.$itemdata->cost_pib + $itemdata->emkl_master + $itemdata->plugin + $itemdata->ls + $itemdata->kalog.'';
         
        })

      ->addColumn('tarik_notarik', function ($itemdata) {
        if ($itemdata->tarik == 1) {
          return '<span class="label label-success"> Tarik </span>';
        }else{
         return '<span class="label label-danger"> No Tarik </span>';
       };
        })

       ->addColumn('ls_status', function ($itemdata) {
        if ($itemdata->ls == 0) { 
	  return '<span class="label label-danger"> No </span>';
        }else{
          return '<span class="label label-success"> Yes </span>';
       };
        })
 
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
    $post = new Item(); 
    $post->item_category_id = $request->item_category_id; 
    $post->item_brand_id = $request->item_brand_id; 
    $post->item_code = $request->item_code;  
    $post->item_name = $request->item_name;  
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    // $item = Item::Find($id);
     $sql = 'SELECT
                item_category.item_category_name,
                item_brand.item_brand_name,
                item.item_category_id,
                item.item_brand_id,
                item.id,
                item.item_code,
                item.item_name,
                item.description,
                item.status
              FROM
                item
              LEFT JOIN item_brand ON item.item_brand_id = item_brand.id
              LEFT JOIN item_category ON item.item_category_id = item_category.id';
      $item = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id)->first(); 
    echo json_encode($item); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Item::Find($id); 
    $post->item_category_id = $request->item_category_id; 
    $post->item_brand_id = $request->item_brand_id; 
    $post->item_code = $request->item_code;  
    $post->item_name = $request->item_name;  
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Item::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   
 
   foreach($idkey as $key => $id)
   {
    // $post =  Item::where('id', $id["1"])->get();
    $post = Item::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
