<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Session;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\EmployeeRequest;
use App\Http\Controllers\Controller;
use App\Model\Employee; 
use App\Model\Position;
use App\Model\Department;
use App\Model\City;
use App\Model\Sex;
use App\Model\Taxstatus;
use App\Model\Golongan;
use App\Model\Payrolltype;
use App\Model\Religion; 
use App\Model\Educationlevel;
use App\Model\Educationmajor;
use App\Model\Location;
use App\Model\Tunjangankesehatan;
use Validator;
use Response;
use App\Post;
use View;
use Intervention\Image\Facades\Image;

class EmployeeController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
    'employeename' => 'required|min:2'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $employees = Employee::all();
      return view ('editor.employee.index', compact('employees'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
        employee.id,
        employee.nik,
        employee.identityno,
        employee.employeename,
        employee.nickname,
        employee.placebirth,
        employee.datebirth,
        employee.address,
        employee.positionid,
        position.positionname,
        position.positionlevel,
        employee.departmentid,
        employee.image,
        employee.email,
        employee.`status`,
        department.departmentname,
        city.cityname,
        employee.npwp,
        employee.taxstatus,
        employee.educationlevelid,
        educationlevel.educationlevelname,
        employee.joindate,
        employee.termdate,
        payrolltype.payrolltypename,
        employee.bankaccount,
        employee.bankname,
        employee.bankbranch,
        employee.bankan,
        employee.jamsostekmember,
        employee.gol,
        employee.tunjangankesehatan,
        CASE
        WHEN employee.sex = 1 THEN
        "Laki-laki"
        ELSE
        "Perempuan"
        END AS gender
        FROM
        employee
        LEFT JOIN department ON employee.departmentid = department.id
        LEFT JOIN position ON employee.positionid = position.id
        LEFT JOIN city ON employee.cityid = city.id
        LEFT JOIN educationlevel ON employee.educationlevelid = educationlevel.id
        LEFT JOIN payrolltype ON employee.paytypeid = payrolltype.id
        WHERE
        employee.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('id', 'asc')->get(); 

        return Datatables::of($itemdata) 

        ->addColumn('action', function ($itemdata) {
          return '<a href="employee/'.$itemdata->id.'/edit" title="Edit")"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->employeename."'".')"> Hapus</a>';
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

         ->addColumn('image', function ($itemdata) {
          if ($itemdata->image == null) {
            return '<a class="fancybox" rel="group" href="//assets/img/thumbnail.png"><img sytyle="heigh:10px" src="../assets/img/thumbnail.jpg" class="img-thumbnail img-responsive"/></a>';
          }else{
           return '<a class="fancybox" rel="group" href="../uploads/employee/'.$itemdata->image.'"><img src="../uploads/employee/thumbnail/'.$itemdata->image.'" class="img-thumbnail img-responsive" /></a>';
         };
       })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> Aktif </span>';
          }else{
           return '<span class="label label-danger"> Tidak Aktif </span>';
         };
         })

        ->addColumn('jamsostekm', function ($itemdata) {
          if ($itemdata->jamsostekmember == 1) {
            return '<span class="label label-success"> Yes </span>';
          }else{
           return '<span class="label label-danger"> No </span>';
       };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function create()
    { 

      $department_list = Department::all()->pluck('departmentname', 'id');
      $position_list = Position::all()->pluck('positionname', 'id'); 
      $city_list = City::all()->pluck('cityname', 'id');
      $sex_list = Sex::all()->pluck('sexname', 'id');
      $taxstatus_list = Taxstatus::all()->pluck('taxstatus', 'taxstatus');
      $golongan_list = Golongan::all()->pluck('golonganname', 'golonganname');
      $payrolltype_list = Payrolltype::all()->pluck('payrolltypename', 'id');
      $religion_list = Religion::all()->pluck('religionname', 'id');
      $city_list = City::all()->pluck('cityname', 'id');
      $educationlevel_list = Educationlevel::all()->pluck('educationlevelname', 'id');
      $educationmajor_list = Educationmajor::all()->pluck('educationmajorname', 'id');
      $location_list = Location::all()->pluck('locationname', 'id');
      $tunjangankesehatan_list = Tunjangankesehatan::all()->pluck('tunjangankesehatanname', 'tunjangankesehatanname');

      return view ('editor.employee.form', compact('department_list', 'position_list', 'city_list', 'sex_list', 'taxstatus_list', 'golongan_list','payrolltype_list','religion_list','city_list', 'educationlevel_list', 'educationmajor_list', 'location_list', 'tunjangankesehatan_list'));
    }


    public function store(Request $request)
    { 

      $employee = new Employee; 
      $employee->identityno = $request->input('identityno');
      $employee->nik = $request->input('nik');
      $employee->employeename = $request->input('employeename');
      $employee->nickname = $request->input('nickname'); 
      $employee->datebirth = $request->input('datebirth'); 
      $employee->joindate = $request->input('joindate'); 
      $employee->insuranceno = $request->input('insuranceno'); 
      $employee->npwp = $request->input('npwp'); 
      $employee->address = $request->input('address'); 
      $employee->hp = $request->input('hp'); 
      $employee->telp1 = $request->input('telp1'); 
      $employee->telp2 = $request->input('telp2'); 
      $employee->bankan = $request->input('bankan'); 
      $employee->bankaccount = $request->input('bankaccount'); 
      $employee->bankname = $request->input('bankname'); 
      $employee->bankbranch = $request->input('bankbranch'); 
      $employee->cityid = $request->input('cityid'); 
      $employee->sex = $request->input('sex'); 
      $employee->taxstatus = $request->input('taxstatus'); 
      $employee->departmentid = $request->input('departmentid'); 
      $employee->positionid = $request->input('positionid'); 
      $employee->gol = $request->input('gol'); 
      $employee->paytypeid = $request->input('paytypeid'); 
      $employee->religionid = $request->input('religionid'); 
      $employee->placebirth = $request->input('placebirth'); 
      $employee->educationlevelid = $request->input('educationlevelid'); 
      $employee->locationid = $request->input('locationid');  
      $employee->status = $request->input('status');  
      $employee->jamsostekmember = $request->input('jamsostekmember');  
      $employee->termdate = $request->input('termdate');  
      $employee->gol = $request->input('gol');  

      $employee->biayajabatan = $request->has('biayajabatan');  
      $employee->tdkdibayarmingguan = $request->has('tdkdibayarmingguan');  
      $employee->statusistimewa = $request->has('statusistimewa');  
      $employee->dibayarbulanandikantor = $request->has('dibayarbulanandikantor');  
      $employee->statuspotpensiun = $request->has('statuspotpensiun');  
      $employee->email = $request->input('email');  
      $employee->tunjangankesehatan = $request->input('tunjangankesehatan');  
      $employee->created_by = Auth::id();
      $employee->save();

      if($request->image)
      {
        $employee = Employee::FindOrFail($employee->id);

        $original_directory = "uploads/employee/";

        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          }

      //$file_extension = $request->image->getClientOriginalExtension();
          $employee->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
          $request->image->move($original_directory, $employee->image);

          $thumbnail_directory = $original_directory."thumbnail/";
          if(!File::exists($thumbnail_directory))
            {
             File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
           }
           $thumbnail = Image::make($original_directory.$employee->image);
           $thumbnail->fit(10,10)->save($thumbnail_directory.$employee->image);

           $employee->save(); 
         }

         return redirect('editor/employee'); 

       }

       public function edit($id)
       { 
        $employee = Employee::Find($id);  

        $department_list = Department::all()->pluck('departmentname', 'id');
        $position_list = Position::all()->pluck('positionname', 'id'); 
        $city_list = City::all()->pluck('cityname', 'id');
        $sex_list = Sex::all()->pluck('sexname', 'id');
        $taxstatus_list = Taxstatus::all()->pluck('taxstatus', 'taxstatus');
        $golongan_list = Golongan::all()->pluck('golonganname', 'golonganname');
        $payrolltype_list = Payrolltype::all()->pluck('payrolltypename', 'id');
        $religion_list = Religion::all()->pluck('religionname', 'id');
        $city_list = City::all()->pluck('cityname', 'id');
        $educationlevel_list = Educationlevel::all()->pluck('educationlevelname', 'id');
        $educationmajor_list = Educationmajor::all()->pluck('educationmajorname', 'id');
        $location_list = Location::all()->pluck('locationname', 'id');
        $tunjangankesehatan_list = Tunjangankesehatan::all()->pluck('tunjangankesehatanname', 'tunjangankesehatanname');


        return view ('editor.employee.form', compact('employee', 'department_list', 'position_list', 'city_list', 'sex_list', 'taxstatus_list', 'golongan_list','payrolltype_list','religion_list','city_list', 'educationlevel_list', 'educationmajor_list', 'location_list', 'tunjangankesehatan_list'));
      }

      public function update($id, Request $request)
      { 

        // dd($request->input('gol'));  


        $employee = Employee::Find($id);
        $employee->identityno = $request->input('identityno');
        $employee->nik = $request->input('nik');
        $employee->employeename = $request->input('employeename');
        $employee->nickname = $request->input('nickname'); 
        $employee->datebirth = $request->input('datebirth'); 
        $employee->joindate = $request->input('joindate'); 
        $employee->insuranceno = $request->input('insuranceno'); 
        $employee->npwp = $request->input('npwp'); 
        $employee->address = $request->input('address'); 
        $employee->hp = $request->input('hp'); 
        $employee->telp1 = $request->input('telp1'); 
        $employee->telp2 = $request->input('telp2'); 
        $employee->bankan = $request->input('bankan'); 
        $employee->bankaccount = $request->input('bankaccount'); 
        $employee->bankname = $request->input('bankname'); 
        $employee->bankbranch = $request->input('bankbranch'); 
        $employee->cityid = $request->input('cityid'); 
        $employee->sex = $request->input('sex'); 
        $employee->taxstatus = $request->input('taxstatus'); 
        $employee->departmentid = $request->input('departmentid'); 
        $employee->positionid = $request->input('positionid'); 
        $employee->gol = $request->input('gol'); 
        $employee->paytypeid = $request->input('paytypeid'); 
        $employee->religionid = $request->input('religionid'); 
        $employee->placebirth = $request->input('placebirth'); 
        $employee->educationlevelid = $request->input('educationlevelid'); 
        $employee->locationid = $request->input('locationid');
        $employee->status = $request->input('status');  
        $employee->termdate = $request->input('termdate');  
        $employee->gol = $request->input('gol');  


        $employee->updated_by = Auth::id();

        $employee->biayajabatan = $request->has('biayajabatan');  
        $employee->jamsostekmember = $request->input('jamsostekmember');  
        $employee->tdkdibayarmingguan = $request->has('tdkdibayarmingguan');  
        $employee->statusistimewa = $request->has('statusistimewa');  
        $employee->dibayarbulanandikantor = $request->has('dibayarbulanandikantor');  
        $employee->statuspotpensiun = $request->has('statuspotpensiun');  
        $employee->email = $request->input('email');  
        $employee->tunjangankesehatan = $request->input('tunjangankesehatan');  
        $employee->save(); 


        if($request->image)
        {
          $employee = Employee::FindOrFail($employee->id);

          $original_directory = "uploads/employee/";

          if(!File::exists($original_directory))
            {
              File::makeDirectory($original_directory, $mode = 0777, true, true);
            }

      // $file_extension = $request->image->getClientOriginalExtension();
            $employee->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
            $request->image->move($original_directory, $employee->image);

            $thumbnail_directory = $original_directory."thumbnail/";
            if(!File::exists($thumbnail_directory))
              {
               File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
             }
             $thumbnail = Image::make($original_directory.$employee->image);
             $thumbnail->fit(10,10)->save($thumbnail_directory.$employee->image);

             $employee->save(); 
           } 

           return redirect('editor/employee'); 
         }  

         public function delete($id)
         {
    //dd($id);
          $post =  Employee::Find($id);
          $post->delete(); 

          return response()->json($post); 
        }

        public function deletebulk(Request $request)
        {
         $idkey = $request->idkey;   
         foreach($idkey as $key => $id)
         { 
          $post = Employee::Find($id["1"]);
          $post->delete(); 
        }

        echo json_encode(array("status" => TRUE));

      }
    }
