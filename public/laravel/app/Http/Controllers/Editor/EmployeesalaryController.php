<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\CityRequest;
use App\Http\Controllers\Controller;
use App\Model\Employee; 
use App\Model\City;
use Validator;
use Response;
use App\Post;
use View;

class EmployeesalaryController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'basic' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $employeesalarys = City::all();
    return view ('editor.employeesalary.index', compact('employeesalarys'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
                employee.id,
                employee.nik,
                employee.identityno,
                employee.employeename, 
                position.positionname,
                employee.basic, 
                employee.basicbegin, 
                employee.postall, 
                employee.mealtransall, 
                employee.overtimeall, 
                employee.extrapudding, 
                employee.medicalall, 
                employee.rateovertime, 
                employee.mealtransrate, 
                employee.ratetunjanganmalam, 
                employee.ratemealincity, 
                employee.ratemealoutcity,
                employee.status 
                FROM
                employee
                LEFT JOIN department ON employee.departmentcode = department.departmentcode
                LEFT JOIN position ON employee.positionid = position.id
                LEFT JOIN city ON employee.cityid = city.id
                LEFT JOIN educationlevel ON employee.educationlevelid = educationlevel.id
                LEFT JOIN payrolltype ON employee.paytypeid = payrolltype.id
                WHERE
                employee.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Set Gaji" class="btn btn-primary btn-xs" onclick="edit('."'".$itemdata->id."'".')"><i class="fa fa-credit-card"></i> Set Gaji</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Aktif </span>';
        }else{
         return '<span class="label label-danger"> Tidak Aktif </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  
  public function edit($id)
  {
    // $employeesalary = Employee::Find($id);
    $sql = 'SELECT
              employee.id,
              employee.nik,
              employee.identityno,
              employee.employeename, 
              position.positionname,
              FORMAT(employee.basic,0) AS basic, 
              FORMAT(employee.basicbegin,0) AS basicbegin, 
              FORMAT(employee.postall,0) AS postall, 
              FORMAT(employee.mealtransall,0) AS mealtransall, 
              FORMAT(employee.overtimeall,0) AS overtimeall, 
              FORMAT(employee.extrapudding,0) AS extrapudding, 
              FORMAT(employee.medicalall,0) AS medicalall, 
              FORMAT(employee.rateovertime,0) AS rateovertime, 
              FORMAT(employee.mealtransrate,0) AS mealtransrate, 
              FORMAT(employee.ratetunjanganmalam,0) AS ratetunjanganmalam, 
              FORMAT(employee.ratemealincity,0) AS ratemealincity, 
              FORMAT(employee.ratemealoutcity,0) AS ratemealoutcity,
              employee.status 
            FROM
              employee
            LEFT JOIN department ON employee.departmentcode = department.departmentcode
            LEFT JOIN position ON employee.positionid = position.id
            LEFT JOIN city ON employee.cityid = city.id
            LEFT JOIN educationlevel ON employee.educationlevelid = educationlevel.id
            LEFT JOIN payrolltype ON employee.paytypeid = payrolltype.id
            WHERE
            employee.deleted_at IS NULL';
    $employeesalary = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id)->first(); 
    echo json_encode($employeesalary); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
      $post = Employee::Find($id); 
      $post->basic = str_replace(",","",$request->basic);
      $post->basicbegin = str_replace(",","",$request->basicbegin);
      $post->postall = str_replace(",","",$request->postall);
      $post->mealtransall = str_replace(",","",$request->mealtransall);
      $post->overtimeall = str_replace(",","",$request->overtimeall);
      $post->extrapudding = str_replace(",","",$request->extrapudding);
      $post->medicalall = str_replace(",","",$request->medicalall);
      $post->rateovertime = str_replace(",","",$request->rateovertime);
      $post->mealtransrate = str_replace(",","",$request->mealtransrate);
      $post->ratetunjanganmalam = str_replace(",","",$request->ratetunjanganmalam);
      $post->ratemealincity = str_replace(",","",$request->ratemealincity);
      $post->ratemealoutcity = str_replace(",","",$request->ratemealoutcity);
      $post->status = $request->status;
      $post->updated_by = Auth::id();
      $post->save();

      return response()->json($post); 
    }
  } 
}
