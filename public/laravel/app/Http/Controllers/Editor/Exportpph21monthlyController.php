<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class Exportpph21monthlyController extends Controller
{
  
  public function index()
  {   
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    return view ('editor.exportpph21monthly.index', compact('department_list','payperiod_list','datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                MONTH(payperiod.dateperiod) AS masa_pajak,
                YEAR(payperiod.dateperiod) AS tahun_pajak,
                0 AS pembetulan,
                REPLACE (
                  REPLACE (
                    ifnull(
                      employee.npwp,
              "0000000000000000"
                    ),
                    "-",
                    ""
                  ),
                  ".",
                  ""
                ) AS npwp,
                employee.employeename AS employee,
                "21-100-01" AS kode_pajak,
                ifnull(payroll.bruto, 0) AS jumlah_bruto,
                ifnull(payroll.pph21, 0) AS jumlah_pph
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN USER ON payroll.periodid = USER .periodid
              AND payroll.departmentid = ifnull(
                USER .departmentid,
                payroll.departmentid
              )
              INNER JOIN payperiod ON payroll.periodid = payperiod.id
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              WHERE
                (user.id = 1)
              ORDER BY
                employee.employeename';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->addColumn('data', function ($itemdata) {
        return ''.$itemdata->masa_pajak.';'.$itemdata->tahun_pajak.';0;'.$itemdata->npwp.';'.$itemdata->employee.';'.$itemdata->kode_pajak.';'.$itemdata->jumlah_bruto.';'.$itemdata->jumlah_pph.';';
      })

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
