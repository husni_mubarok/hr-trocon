<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Controllers\Controller;
use App\Model\Item; 
use App\Model\Itembrand; 
use App\Model\Itemcategory; 
use App\Model\Branch;
use App\Model\User;
use App\Model\Purchaseorderdetail;
use App\Model\Purchaseorderdetaillog;
use App\Model\Currency;
use App\Model\Branchallocimport;
use App\Model\Container;
use Validator;
use Response;
use App\Post;
use View;
use Excel;

class ImportbranchallocController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'item_name' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $userid = Auth::id();

    $branch = Branch::orderBy('seq_no', 'ASC')->get();
 
    $capitalgood = Item::first();

    $currency_list = Currency::all();

    $item_category = Itemcategory::orderBy('item_category_name', 'ASC')->get();

    $container = Container::orderBy('container_name', 'ASC')->get();
     
    $branch = Branch::orderBy('seq_no', 'ASC')->get();

    $sql1 = 'SELECT
              item_category.item_category_name,
              `user`.id,
              `user`.grfrom,
              `user`.grto,
              `user`.search_filter,
              `user`.item_category_id,
              `user`.container_id,
              container.container_name
              FROM
              `user`
              LEFT JOIN item_category 
              ON `user`.item_category_id = item_category.id
              LEFT JOIN container
              ON `user`.container_id = container.id';

    $datafilter = DB::table(DB::raw("($sql1) as rs_sql"))->where('id', Auth::id())->first(); 

    return view ('editor.importbranchalloc.index', compact('capitalgood', 'capitalgood_detail', 'branch', 'datafilter', 'currency_list', 'item_category', 'container'));
  }

     
}
