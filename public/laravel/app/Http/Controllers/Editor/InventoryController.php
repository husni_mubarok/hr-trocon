<?php

namespace App\Http\Controllers\Editor;

use Auth;
use File;
use Session;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\InventoryRequest;
use App\Http\Controllers\Controller;
use App\Model\Inventory; 
use App\Model\Inventorybrand;
use App\Model\Inventorygroup;
use App\Model\Inventorytype;
use App\Model\Inventorysize;
use App\Model\Inventorycolor;
use App\Model\Unit;
use Validator;
use Response;
use App\Post;
use View;
use Intervention\Image\Facades\Image;

class InventoryController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
  'inventoryname' => 'required|min:2'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $inventorys = Inventory::all();
      return view ('editor.inventory.index', compact('inventorys'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
        inventory.id,
        inventory.inventorycode,
        inventory.inventoryname,
        inventory.spec,
        inventory.size,
        inventory.inventorygroupid,
        inventorygroup.inventorygroupname,
        inventory.stockunit,
        inventory.purchaseunit,
        inventory.inventorytypeid,
        inventorytype.inventorytypename,
        inventory.inventorybrandid,
        inventorybrand.inventorybrandname,
        inventory.inventorycolorid,
        inventorycolor.inventorycolorname,
        inventory.inventorysizeid,
        inventorysize.inventorysizename,
        inventory.image,
        inventory.status
        FROM
        inventory
        LEFT JOIN inventorygroup ON inventory.inventorygroupid = inventorygroup.id
        LEFT JOIN inventorytype ON inventory.inventorytypeid = inventorytype.id
        LEFT JOIN inventorybrand ON inventory.inventorybrandid = inventorybrand.id
        LEFT JOIN inventorycolor ON inventory.inventorycolorid = inventorycolor.id
        LEFT JOIN inventorysize ON inventory.inventorysizeid = inventorysize.id
        WHERE inventory.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata) 

        ->addColumn('action', function ($itemdata) {
          return '<a href="inventory/'.$itemdata->id.'/edit" title="Edit")"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->inventoryname."'".')"> Hapus</a>';
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('image', function ($itemdata) {
          if ($itemdata->image == null) {
            return '<a class="fancybox" rel="group" href="uploads/inventory/placeholder.png"><img src="../uploads/inventory/thumbnail/placeholder.png" class="img-thumbnail img-responsive" /></a>';
          }else{
           return '<a class="fancybox" rel="group" href="../uploads/inventory/'.$itemdata->image.'"><img src="../uploads/inventory/thumbnail/'.$itemdata->image.'" class="img-thumbnail img-responsive" /></a>';
         };
       })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> Aktif </span>';
          }else{
           return '<span class="label label-danger"> Tidak Aktif </span>';
         };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function datalookup(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
        inventory.id,
        inventory.inventorycode,
        inventory.inventoryname,
        inventory.spec,
        inventory.size,
        inventory.inventorygroupid,
        inventorygroup.inventorygroupname,
        inventory.stockunit,
        inventory.purchaseunit,
        inventory.inventorytypeid,
        inventorytype.inventorytypename,
        inventory.inventorybrandid,
        inventorybrand.inventorybrandname,
        inventory.inventorycolorid,
        inventorycolor.inventorycolorname,
        inventory.inventorysizeid,
        inventorysize.inventorysizename,
        inventory.image,
        inventory.status
        FROM
        inventory
        LEFT JOIN inventorygroup ON inventory.inventorygroupid = inventorygroup.id
        LEFT JOIN inventorytype ON inventory.inventorytypeid = inventorytype.id
        LEFT JOIN inventorybrand ON inventory.inventorybrandid = inventorybrand.id
        LEFT JOIN inventorycolor ON inventory.inventorycolorid = inventorycolor.id
        LEFT JOIN inventorysize ON inventory.inventorysizeid = inventorysize.id
        WHERE inventory.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata) 

        ->addColumn('action', function ($itemdata) {
          return '<a href="javascript:void(0)" id="btnstore" onclick="addValue(this, '.$itemdata->id.')" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-hand-up"></i> Choose</a>';
        })

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('image', function ($itemdata) {
          if ($itemdata->image == null) {
            return '<a class="fancybox" rel="group" href="uploads/inventory/placeholder.png"><img src="../uploads/inventory/thumbnail/placeholder.png" class="img-thumbnail img-responsive" /></a>';
          }else{
           return '<a class="fancybox" rel="group" href="../uploads/inventory/'.$itemdata->image.'"><img src="../uploads/inventory/thumbnail/'.$itemdata->image.'" class="img-thumbnail img-responsive" /></a>';
         };
       })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> Aktif </span>';
          }else{
           return '<span class="label label-danger"> Tidak Aktif </span>';
         };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function create()
    { 

      $unit_list = Unit::all()->pluck('unitname', 'unitname');
      $type_list = Inventorytype::all()->pluck('inventorytypename', 'id'); 
      $group_list = Inventorygroup::all()->pluck('inventorygroupname', 'id');
      $brand_list = Inventorybrand::all()->pluck('inventorybrandname', 'id');
      $color_list = Inventorycolor::all()->pluck('inventorycolorname', 'id');
      $size_list = Inventorysize::all()->pluck('inventorysizename', 'id');

      return view ('editor.inventory.form', compact('unit_list', 'type_list', 'group_list', 'brand_list', 'color_list', 'size_list'));
    }


    public function store(InventoryRequest $request)
    { 

      $inventory = new Inventory; 
      $inventory->inventorycode = $request->input('inventorycode');
      $inventory->inventoryname = $request->input('inventoryname');
      $inventory->stockunit = $request->input('stockunit');
      $inventory->inventorytypeid = $request->input('inventorytypeid');
      $inventory->inventorygroupid = $request->input('inventorygroupid');
      $inventory->inventorybrandid = $request->input('inventorybrandid'); 
      $inventory->inventorycolorid = $request->input('inventorycolorid'); 
      $inventory->inventorysizeid = $request->input('inventorysizeid'); 
      $inventory->created_by = Auth::id();
      $inventory->save();

      if($request->image)
      {
        $inventory = Inventory::FindOrFail($inventory->id);

        $original_directory = "uploads/inventory/";

        if(!File::exists($original_directory))
        {
          File::makeDirectory($original_directory, $mode = 0777, true, true);
        }

      //$file_extension = $request->image->getClientOriginalExtension();
        $inventory->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
        $request->image->move($original_directory, $inventory->image);

        $thumbnail_directory = $original_directory."thumbnail/";
        if(!File::exists($thumbnail_directory))
        {
         File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
       }
       $thumbnail = Image::make($original_directory.$inventory->image);
       $thumbnail->fit(10,10)->save($thumbnail_directory.$inventory->image);

       $inventory->save(); 
     }

     return redirect('editor/inventory'); 

   }

   public function edit($id)
   { 
    $inventory = Inventory::Find($id); 

    //dd($inventory);

    $unit_list = Unit::all()->pluck('unitname', 'unitname');
    $type_list = Inventorytype::all()->pluck('inventorytypename', 'id'); 
    $group_list = Inventorygroup::all()->pluck('inventorygroupname', 'id');
    $brand_list = Inventorybrand::all()->pluck('inventorybrandname', 'id');
    $color_list = Inventorycolor::all()->pluck('inventorycolorname', 'id');
    $size_list = Inventorysize::all()->pluck('inventorysizename', 'id');

    return view ('editor.inventory.form', compact('inventory','unit_list', 'type_list', 'group_list', 'brand_list', 'color_list', 'size_list'));
  }

  public function update($id, Request $request)
  { 

    $inventory = Inventory::Find($id);
    $inventory->inventorycode = $request->input('inventorycode');
    $inventory->inventoryname = $request->input('inventoryname');
    $inventory->stockunit = $request->input('stockunit');
    $inventory->inventorytypeid = $request->input('inventorytypeid');
    $inventory->inventorygroupid = $request->input('inventorygroupid');
    $inventory->inventorybrandid = $request->input('inventorybrandid'); 
    $inventory->inventorycolorid = $request->input('inventorycolorid'); 
    $inventory->inventorysizeid = $request->input('inventorysizeid'); 
    $inventory->created_by = Auth::id(); 
    $inventory->save();

    if($request->image)
    {
      $inventory = Inventory::FindOrFail($inventory->id);

      $original_directory = "uploads/inventory/";

      if(!File::exists($original_directory))
      {
        File::makeDirectory($original_directory, $mode = 0777, true, true);
      }

      // $file_extension = $request->image->getClientOriginalExtension();
      $inventory->image = Carbon::now()->format("d-m-Y h-i-s").$request->image->getClientOriginalName();
      $request->image->move($original_directory, $inventory->image);

      $thumbnail_directory = $original_directory."thumbnail/";
      if(!File::exists($thumbnail_directory))
      {
       File::makeDirectory($thumbnail_directory, $mode = 0777, true, true);
     }
     $thumbnail = Image::make($original_directory.$inventory->image);
     $thumbnail->fit(10,10)->save($thumbnail_directory.$inventory->image);

     $inventory->save(); 
   } 

   return redirect('editor/inventory'); 
 }  

 public function delete($id)
 {
    //dd($id);
  $post =  Inventory::Find($id);
  $post->delete(); 

  return response()->json($post); 
}

public function deletebulk(Request $request)
{

 $idkey = $request->idkey;   

  //$count = count($idkey);

//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

 foreach($idkey as $key => $id)
 {
    // $post =  Inventory::where('id', $id["1"])->get();
  $post = Inventory::Find($id["1"]);
  $post->delete(); 
}

echo json_encode(array("status" => TRUE));

}
}
