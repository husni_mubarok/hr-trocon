<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Controllers\Controller;
use App\Model\Item; 
use App\Model\Itembrand; 
use App\Model\Itemcategory; 
use Validator;
use Response;
use App\Post;
use View;

class ItemController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'item_name' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $item_brand_list = Itembrand::all();
    $item_category_list = Itemcategory::all();
    $sql = 'SELECT
              item_category.item_category_name,
              item_brand.item_brand_name,
              item.id,
              item.item_code,
              item.item_name,
              item.description
            FROM
              item
            LEFT JOIN item_brand ON item.item_brand_id = item_brand.id
            LEFT JOIN item_category ON item.item_category_id = item_category.id
            WHERE
            item.deleted_at IS NULL';
    $items = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

    return view ('editor.item.index', compact('items', 'item_brand_list', 'item_category_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
       $sql = 'SELECT
                item_category.item_category_name,
                item_brand.item_brand_name,
                item.id,
                item.item_code,
                item.item_name,
                item.description,
                item.status
              FROM
                item
              LEFT JOIN item_brand ON item.item_brand_id = item_brand.id
              LEFT JOIN item_category ON item.item_category_id = item_category.id
              WHERE
              item.deleted_at IS NULL';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->item_code."', '".$itemdata->item_category_name."'".')"> Delete</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
    $post = new Item(); 
    $post->item_category_id = $request->item_category_id; 
    $post->item_brand_id = $request->item_brand_id; 
    $post->item_code = $request->item_code;  
    $post->item_name = $request->item_name;  
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    // $item = Item::Find($id);
     $sql = 'SELECT
                item_category.item_category_name,
                item_brand.item_brand_name,
                item.item_category_id,
                item.item_brand_id,
                item.id,
                item.item_code,
                item.item_name,
                item.description,
                item.status
              FROM
                item
              LEFT JOIN item_brand ON item.item_brand_id = item_brand.id
              LEFT JOIN item_category ON item.item_category_id = item_category.id';
      $item = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id)->first(); 
    echo json_encode($item); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Item::Find($id); 
    $post->item_category_id = $request->item_category_id; 
    $post->item_brand_id = $request->item_brand_id; 
    $post->item_code = $request->item_code;  
    $post->item_name = $request->item_name;  
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Item::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   
 
   foreach($idkey as $key => $id)
   {
    // $post =  Item::where('id', $id["1"])->get();
    $post = Item::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
