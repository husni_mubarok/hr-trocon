<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\LeavingRequest;
use App\Http\Controllers\Controller;
use App\Model\Leaving; 
use App\Model\Employee; 
use App\Model\Absencetype;
use Validator;
use Response;
use App\Post;
use View;

class LeavingController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'leavingno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'leavingname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $leavings = Leaving::all();
      return view ('editor.leaving.index', compact('leavings'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT 
                  leaving.id,
                  leaving.codetrans,
                  leaving.notrans,
                  leaving.datetrans,
                  leaving.employeeid,
                  employee.employeename,
                  leaving.leavingfrom,
                  leaving.leavingto,
                  DATEDIFF(leaving.leavingto, leaving.leavingfrom) AS days,
                  -- leaving.days,
                  leaving.used,
                  leaving.plafond,
                  leaving.absencetypeid,
                  absencetype.absencetypename,
                  leaving.attachment,
                  leaving.remark,
                  leaving.`status` 
                FROM
                  leaving
                  LEFT JOIN employee ON leaving.employeeid = employee.id
                  LEFT JOIN absencetype ON leaving.absencetypeid = absencetype.id
                WHERE
                  leaving.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 


        ->addColumn('action', function ($itemdata) {
          return '<a href="leaving/'.$itemdata->id.'/edit" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
        })

        ->addColumn('approval', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Approval" class="btn btn-success btn-xs" onclick="approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-check"></i> Approve</a> <a  href="javascript:void(0)" title="Approval" class="btn btn-warning btn-xs" onclick="not_approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-close"></i> Not Approve</a>';
      })

      ->addColumn('btn_attachment', function ($itemdata) {
          if ($itemdata->attachment == "") {
            return 'No Attachment';
          }else{
           return '<a href="../uploads/leaving/'.$itemdata->attachment.'" target="_blank"><i class="fa fa-download"></i> Download </a>';
        };
       })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
            return '<span class="label label-success"> <i class="fa fa-check"></i> Active </span>';
          }else if ($itemdata->status == 9){
           return '<span class="label label-danger"> <i class="fa fa-minus-square"></i> Cancel </span>';
          }else if ($itemdata->status == 2){
           return '<span class="label label-warning"> <i class="fa fa-minus-square"></i> Not Approve </span>';
          }else if ($itemdata->status == 1){
           return '<span class="label label-success"> <i class="fa fa-check"></i> Approve </span>';
        }; 
       })

        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function datahistory(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  leaving.id,
                  leaving.codetrans,
                  leaving.notrans,
                  leaving.datetrans,
                  leaving.employeeid,
                  DATE_FORMAT(leaving.leavingfrom, "%d-%m-%Y") AS leavingfrom,
                  DATE_FORMAT(leaving.leavingto, "%d-%m-%Y") AS leavingto,
                  absencetype.absencetypename,
                  employee.employeename
                FROM
                  leaving
                INNER JOIN absencetype ON leaving.absencetypeid = absencetype.id
                INNER JOIN employee ON leaving.employeeid = employee.id,
                 `user`
                WHERE
                  `user`.id = '.Auth::id().' AND leaving.employeeid = user.employeeid
                AND
                  leaving.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata) 

        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function create()
    {  
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      $absencetype_list = Absencetype::all()->pluck('absencetypename', 'id'); 

      return view ('editor.leaving.form', compact('employee_list', 'absencetype_list'));
    }

    public function store(Request $request)
    { 
        DB::insert("INSERT INTO leaving (codetrans, notrans, datetrans)
                    SELECT 'MUTA',
                    IFNULL(CONCAT('PUNS','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(leaving.notrans),3))+1001,3)), CONCAT('PUNS','-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW())  
                    FROM
                    leaving
                    WHERE codetrans='PUNS'");

       $lastInsertedID = DB::table('leaving')->max('id');  

       // dd($lastInsertedID);

       $leaving = Leaving::where('id', $lastInsertedID)->first(); 
       $leaving->employeeid = $request->input('employeeid'); 
       $leaving->absencetypeid = $request->input('absencetypeid'); 
       $leaving->leavingfrom = $request->input('leavingfrom'); 
       $leaving->leavingto = $request->input('leavingto');   
       $leaving->created_by = Auth::id();
       $leaving->save();

       if($request->attachment)
       {
        $leaving = Leaving::FindOrFail($leaving->id);
        $original_directory = "uploads/leaving/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $leaving->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $leaving->attachment);
          $leaving->save(); 
        } 
        return redirect('editor/leaving'); 
     
    }

    public function edit($id)
    {
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      $absencetype_list = Absencetype::all()->pluck('absencetypename', 'id'); 
      $leaving = Leaving::Where('id', $id)->first();   

      // dd($leaving); 
      return view ('editor.leaving.form', compact('leaving','absencetype_list', 'employee_list'));
    }

    public function update($id, Request $request)
    {
      
       $leaving = Leaving::FindOrFail($id); 
       $leaving->employeeid = $request->input('employeeid'); 
       $leaving->absencetypeid = $request->input('absencetypeid'); 
       $leaving->leavingfrom = $request->input('leavingfrom'); 
       $leaving->leavingto = $request->input('leavingto');  
       $leaving->created_by = Auth::id();
       $leaving->save();

      if($request->attachment)
      {
        $leaving = Leaving::FindOrFail($leaving->id);
        $original_directory = "uploads/leaving/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $leaving->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $leaving->attachment);
          $leaving->save(); 
        } 
        return redirect('editor/leaving');  
      }

      public function delete($id)
      {
    //dd($id);
        $post =  Leaving::Find($id);
        $post->delete(); 

        return response()->json($post); 
      }

      public function deletebulk(Request $request)
      {

       $idkey = $request->idkey;   

       foreach($idkey as $key => $id)
       {
    // $post =  Document::where('id', $id["1"])->get();
        $post = Document::Find($id["1"]);
        $post->delete(); 
      }

      echo json_encode(array("status" => TRUE));

    }
  }
