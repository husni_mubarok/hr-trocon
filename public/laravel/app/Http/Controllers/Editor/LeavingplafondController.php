<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\LeavingRequest;
use App\Http\Controllers\Controller;
use App\Model\Leaving; 
use App\Model\Employee; 
use App\Model\Absencetype;
use Validator;
use Response;
use App\Post;
use View;

class LeavingplafondController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'leavingno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'leavingname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $leavings = Leaving::all();
      return view ('editor.leavingplafond.index', compact('leavings'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  employee.id,
                  employee.nik,
                  employee.employeename,
                  SUM(
                    datediff(
                      leaving.leavingto,
                      leaving.leavingfrom
                    )
                  ) AS days,
                  TIMESTAMPDIFF(
                    MONTH,
                    employee.joindate,
                    NOW()
                  ) AS plafond
                FROM
                  employee
                LEFT JOIN (SELECT * FROM leaving WHERE status =1) AS leaving ON leaving.employeeid = employee.id
                LEFT JOIN absencetype ON leaving.absencetypeid = absencetype.id
                WHERE
                  leaving.deleted_at IS NULL
                GROUP BY
                  leaving.employeeid,
                  employee.employeename';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata) 

        ->addColumn('rs_plafond', function ($itemdata) {
          if ($itemdata->plafond > 12) {
              return '12';
            }else{
             return ''.$itemdata->plafond.'';
          };
        })

        ->addColumn('rs_remain', function ($itemdata) {

          //total days
          if ($itemdata->plafond > 12) {
              $totaldays = '12';
          };

          //day used
          if($itemdata->days == ''){
            $days = '0';
          }else{
            $days = $itemdata->days;
          };

          if ($itemdata->plafond > 12) {
              return ''.$totaldays - $days.'';
            }else{
             return ''.$itemdata->plafond - $days.'';
          };
        })


        //rs days
        ->addColumn('rs_days', function ($itemdata) {

          //day used
          if($itemdata->days == ''){
            return '0';
          }else{
            return $itemdata->days;
          }; 
        })
 
        ->make(true);
      } else {
        exit("No data available");
      }
    } 

     public function approve($id, Request $request)
    { 
        $post = Leaving::Find($id); 
        $post->status = 1;  
        $post->save();

        return response()->json($post);  
    }

    public function notapprove($id, Request $request)
    { 
        $post = Leaving::Find($id); 
        $post->status = 2;  
        $post->save();

        return response()->json($post);  
    }

  }
