<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\MaterialusedRequest;
use App\Http\Controllers\Controller;
use App\Model\Materialused; 
use App\Model\Materialuseddet;
use App\Model\Materialusedtype;
use App\Model\Employee;
use App\Model\Building;
use App\Model\Unit;
use App\Model\Floor;
use App\Model\Room;
use Validator;
use Response;
use App\Post;
use View;

class MaterialusedController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
  'materialusedname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $materialuseds = Materialused::all();
      return view ('editor.materialused.index', compact('materialuseds'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
        materialused.id,
        materialused.codetrans,
        materialused.notrans,
        DATE_FORMAT(
        materialused.datetrans,
        "%d/%m/%Y"
        ) AS datetrans,
        materialused.matusedtypeid,
        materialusedtype.materialusedtypename,
        materialused.employeeid,
        materialused.remark,
        materialused.`status`,
        employee.employeename
        FROM
        materialused
        LEFT JOIN materialusedtype ON materialused.matusedtypeid = materialusedtype.id
        LEFT JOIN employee ON materialused.employeeid = employee.id
        WHERE
        materialused.deleted_at IS NULL AND materialused.status <> 1';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata) 

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('action', function ($itemdata) {
          return '<a href="materialused/detail/'.$itemdata->id.'" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> Open </span>';
          }else{
           return '<span class="label label-danger"> Cancel </span>';
         };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }
 
  public function detail($id)
    {
      $used_list = MaterialusedType::all()->pluck('materialusedtypename', 'id');
      $employee_list = Employee::all()->pluck('employeename', 'id');
      $building_list = Building::all()->pluck('buildingname', 'id');
      $unit_list = Unit::all()->pluck('unitname', 'id');
      $floor_list = Floor::all()->pluck('floorname', 'id');
      $room_list = Room::all()->pluck('roomname', 'id');

      //dd($used_list);
      $materialused = Materialused::Find($id); 
      return view ('editor.materialused.form', compact('used_list', 'employee_list', 'building_list', 'unit_list', 'floor_list', 'room_list','materialused'));
    }

    public function store(Request $request)
    { 

      $userid= Auth::id();
      $codetrans = $request->input('codetrans'); 

       DB::insert("INSERT INTO materialused (codetrans, notrans, datetrans, created_by, created_at)
      SELECT '".$codetrans."',
      IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(materialused.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
      FROM
      materialused
      WHERE codetrans= '".$codetrans."'");

      $lastInsertedID = DB::table('materialused')->max('id');
      //return redirect()->action('Editor\MaterialusedController@edit', $lastInsertedID->id);
      return redirect('editor/materialused/detail/'.$lastInsertedID.''); 
    }

    public function saveheader($id, Request $request)
    { 
        $post = Materialused::Find($id); 
        $post->matusedtypeid = $request->matusedtypeid; 
        $post->buildingid = $request->buildingid; 
        $post->employeeid = $request->employeeid; 
        $post->remark = $request->remark; 
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post); 
      
    }

     public function savedetail($id, Request $request)
    { 
        $post = new Materialuseddet;
        $post->transid = $id;  
        $post->inventoryid = $request->inventoryid; 
        $post->unit = $request->unit; 
        $post->floorid = $request->floorid; 
        $post->roomid = $request->roomid; 
        $post->quantity = $request->quantity; 
        $post->save();

        return response()->json($post); 
      
    }

    public function deletedet($id)
    {
    //dd($id);
      $post =  Materialuseddet::Find($id);
      $post->delete(); 

      return response()->json($post); 
    }

    public function deletebulk(Request $request)
    {

     $idkey = $request->idkey;   

  //$count = count($idkey);

//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

     foreach($idkey as $key => $id)
     {
    // $post =  Materialused::where('id', $id["1"])->get();
      $post = Materialused::Find($id["1"]);
      $post->delete(); 
    }

    echo json_encode(array("status" => TRUE));

  }
  public function datadetail(Request $request, $id)
    {   
     
    if($request->ajax()){ 

        $sql = 'SELECT
                  materialuseddet.id,
                  materialuseddet.transid,
                  materialuseddet.inventoryid,
                  inventory.inventorycode,
                  inventory.inventoryname,
                  materialuseddet.unit,
                  FORMAT(materialuseddet.quantity, 0) AS quantity
                FROM
                  materialuseddet
                LEFT JOIN inventory ON materialuseddet.inventoryid = inventory.id
                WHERE materialuseddet.deleted_at IS NULL AND materialuseddet.transid = '.$id.'';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

       return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Delete" class="btn btn-danger btn-xs" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->inventoryname."'".')"><i class="fa fa-trash"></i></a>';
      })
   
      ->make(true);
    } else {
      exit("No data available");
    }
    }
}
