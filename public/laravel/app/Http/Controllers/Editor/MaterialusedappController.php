<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\MaterialusedRequest;
use App\Http\Controllers\Controller;
use App\Model\Materialused; 
use App\Model\Materialuseddet;
use App\Model\MaterialusedType;
use App\Model\Employee;
use App\Model\Building;
use App\Model\Unit;
use App\Model\Floor;
use App\Model\Room;
use Validator;
use Response;
use App\Post;
use View;

class MaterialusedappController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
  'materialusedname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $materialuseds = Materialused::all();
      return view ('editor.materialusedapp.index', compact('materialuseds'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
        materialused.id,
        materialused.codetrans,
        materialused.notrans,
        DATE_FORMAT(
        materialused.datetrans,
        "%d/%m/%Y"
        ) AS datetrans,
        materialused.matusedtypeid,
        materialusedtype.materialusedtypename,
        materialused.employeeid,
        materialused.remark,
        materialused.`status`,
        employee.employeename
        FROM
        materialused
        LEFT JOIN materialusedtype ON materialused.matusedtypeid = materialusedtype.id
        LEFT JOIN employee ON materialused.employeeid = employee.id
        WHERE
        materialused.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata) 

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('action', function ($itemdata) {
          return '<a href="materialusedapp/detail/'.$itemdata->id.'" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
        })

        ->addColumn('approval', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Approval" class="btn btn-success btn-xs" onclick="approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-check"></i> Approve</a> <a  href="javascript:void(0)" title="Approval" class="btn btn-warning btn-xs" onclick="not_approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-close"></i> Not Approve</a>';
      })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-warning"> Open </span>';
          }else if ($itemdata->status == 2){
           return '<span class="label label-danger"> Not Approve </span>';
        }else if ($itemdata->status == 1){
           return '<span class="label label-success"> Approve </span>';
         };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }
 
  public function detail($id)
    {
      $used_list = MaterialusedType::all()->pluck('materialusedtypename', 'id');
      $employee_list = Employee::all()->pluck('employeename', 'id');
      $building_list = Building::all()->pluck('buildingname', 'id');
      $unit_list = Unit::all()->pluck('unitname', 'id');
      $floor_list = Floor::all()->pluck('floorname', 'id');
      $room_list = Room::all()->pluck('roomname', 'id');

      //dd($used_list);
      $materialused = Materialused::Find($id); 
      return view ('editor.materialusedapp.form', compact('used_list', 'employee_list', 'building_list', 'unit_list', 'floor_list', 'room_list','materialused'));
    }

  
    public function approve($id, Request $request)
    { 
        $post = Materialused::Find($id); 
        $post->status = 1;  
        $post->save();

        return response()->json($post); 
      
    }

    public function notapprove($id, Request $request)
    { 
        $post = Materialused::Find($id); 
        $post->status = 2;  
        $post->save();

        return response()->json($post); 
      
    }

     
  public function datadetail(Request $request, $id)
    {   
     
    if($request->ajax()){ 

        $sql = 'SELECT
                  materialuseddet.id,
                  materialuseddet.transid,
                  materialuseddet.inventoryid,
                  inventory.inventorycode,
                  inventory.inventoryname,
                  materialuseddet.unit,
                  FORMAT(materialuseddet.quantity, 0) AS quantity
                FROM
                  materialuseddet
                LEFT JOIN inventory ON materialuseddet.inventoryid = inventory.id
                WHERE Materialuseddet.deleted_at IS NULL AND materialuseddet.transid = '.$id.'';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

       return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Delete" class="btn btn-danger btn-xs" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->inventoryname."'".')"><i class="fa fa-trash"></i></a>';
      })
   
      ->make(true);
    } else {
      exit("No data available");
    }
    }
}
