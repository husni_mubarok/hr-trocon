<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\MealtranRequest;
use App\Http\Controllers\Controller;
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Mealtran; 
use App\Model\Mealtrandet; 
use App\Model\Logum;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;

class MealtranController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'periodid' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::orderBy('id','desc')->where('status', '0')->get();
    $mealtrans = Mealtran::all();
    $sql1 = 'SELECT
              `user`.id,
              payperiod.id AS periodid,
              payperiod.description
            FROM
              payperiod
            INNER JOIN `user` ON payperiod.id = `user`.periodid';

    $datafilter = DB::table(DB::raw("($sql1) as rs_sql"))->where('id', Auth::id())->first();
    $payperiod_list2 = Payperiod::orderBy('id','desc')->get()->pluck('description', 'id');

    return view ('editor.mealtran.index', compact('mealtrans','payperiod_list','payperiod_list2', 'department_list', 'datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                mealtran.id,
                mealtran.periodid,
                payperiod.description,
                payperiod.begindate AS begindate_short,
                DATE_FORMAT(
                  payperiod.begindate,
                  "%d-%m-%Y"
                ) AS begindate,
                DATE_FORMAT(
                  payperiod.enddate,
                  "%d-%m-%Y"
                ) AS enddate,
                mealtran.`status`
              FROM
                mealtran
              LEFT JOIN payperiod ON mealtran.periodid = payperiod.id
              WHERE
                mealtran.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('id','desc')->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->description."'".')"> Hapus</a>';
      })

      ->addColumn('actiondetail', function ($itemdata) {
        return '<a href="mealtran/'.$itemdata->id.'/editdetail" title="Detail" class="btn btn-primary btn-xs btn-flat"><i class="fa fa-folder"></i> Detail</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Aktif </span>';
        }else{
         return '<span class="label label-danger"> Tidak Aktif </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
      $post = new Mealtran(); 
      $post->periodid = $request->periodid; 
      $post->departmentid = $request->departmentid; 
      $post->status = $request->status;
      $post->created_by = Auth::id();
      $post->save();

      return response()->json($post); 
    }
  }

  public function edit($id)
  {
    $mealtran = Mealtran::Find($id);
    echo json_encode($mealtran); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
      $post = Mealtran::Find($id); 
      $post->periodid = $request->periodid; 
      $post->departmentid = $request->departmentid; 
      $post->status = $request->status;
      $post->updated_by = Auth::id();
      $post->save();

      return response()->json($post); 
    }
  }

  public function editdetail($id)
  {

    $sqlm = 'SELECT
              mealtran.id,
              mealtran.codetrans,
              mealtran.notrans,
              mealtran.datetrans,
              mealtran.departmentcode,
              mealtran.departmentid,
              mealtran.periodid,
              mealtran.remark,
              payperiod.description
            FROM
              mealtran
            LEFT JOIN payperiod ON mealtran.periodid = payperiod.id
            WHERE mealtran.id = '.$id.'';
    $mealtran = DB::table(DB::raw("($sqlm) as rs_sql"))->first();


    $sql = 'SELECT
              employee.nik,
              employee.employeename,
              employee.gol,
              location.locationname,
              department.departmentname,
              mealtrandet.id,
              mealtrandet.transid,
              mealtrandet.checklistum,
              mealtrandet.employeeid,
              mealtrandet.checklist,
              mealtrandet.project,
              mealtrandet.pottelat1,
              mealtrandet.pottelat2,
              mealtrandet.pottelat3,
              mealtrandet.pottelat4,
              mealtrandet.pottelat5,
              FORMAT(mealtrandet.um1,0) AS um1,
              FORMAT(mealtrandet.um2,0) AS um2,
              FORMAT(mealtrandet.um3,0) AS um3,
              FORMAT(mealtrandet.um4,0) AS um4,
              FORMAT(mealtrandet.um5,0) AS um5,  
              FORMAT(IFNULL(mealtrandet.um1,0) + IFNULL(mealtrandet.um2,0) + IFNULL(mealtrandet.um3,0) + IFNULL(mealtrandet.um4,0) + IFNULL(mealtrandet.um5,0) + IFNULL(mealtrandet.addmeal,0) ,0) AS tum, 
              FORMAT(CASE WHEN mealtrandet.insentif1 < 0 THEN 0 ELSE mealtrandet.insentif1 - IFNULL(mealtrandet.pottelat1,0) END,0) AS insentif1,
              FORMAT(CASE WHEN mealtrandet.insentif2 < 0 THEN 0 ELSE mealtrandet.insentif2 - IFNULL(mealtrandet.pottelat2,0) END,0) AS insentif2,
              FORMAT(CASE WHEN mealtrandet.insentif3 < 0 THEN 0 ELSE mealtrandet.insentif3 - IFNULL(mealtrandet.pottelat3,0) END,0) AS insentif3,
              FORMAT(CASE WHEN mealtrandet.insentif4 < 0 THEN 0 ELSE mealtrandet.insentif4 - IFNULL(mealtrandet.pottelat4,0) END,0) AS insentif4,
              FORMAT(CASE WHEN mealtrandet.insentif5 < 0 THEN 0 ELSE mealtrandet.insentif5 - IFNULL(mealtrandet.pottelat5,0) END,0) AS insentif5,
              FORMAT(mealtrandet.insentif,0) AS insentif,  
              FORMAT(IFNULL(mealtrandet.pottelat1,0) + IFNULL(mealtrandet.pottelat2,0) + IFNULL(mealtrandet.pottelat3,0) + IFNULL(mealtrandet.pottelat4,0) + IFNULL(mealtrandet.pottelat5,0),0) AS pottelat,  
              FORMAT(mealtrandet.potabsence,0) AS potabsence,  
              FORMAT(mealtrandet.correction,0) AS correction,
              FORMAT(mealtrandet.perday,0) AS perday,
              mealtrandet.addmeal AS addmeal,
              mealtrandet.addinsentif AS addinsentif,
              FORMAT(mealtrandet.amount,0) AS amount,
              FORMAT(mealtrandet.amountpajak,0) AS amountpajak 
            FROM
              mealtrandet
            LEFT JOIN employee ON mealtrandet.employeeid = employee.id
            LEFT JOIN location ON employee.locationid = location.idlocation
            LEFT JOIN department ON employee.departmentid = department.id
            WHERE mealtrandet.transid = '.$id.' AND employee.status =0 AND employee.mealtransall =0';
    $mealtran_detail = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('departmentname', 'asc')->orderBy('gol', 'asc')->get(); 


    $sql1 = 'SELECT 
              FORMAT(SUM(mealtrandet.um1),0) AS um1,
              FORMAT(SUM(mealtrandet.um2),0) AS um2,
              FORMAT(SUM(mealtrandet.um3),0) AS um3,
              FORMAT(SUM(mealtrandet.um4),0) AS um4,
              FORMAT(SUM(mealtrandet.um5),0) AS um5,
              SUM(mealtrandet.pottelat1) AS pottelat1,
              SUM(mealtrandet.pottelat2) AS pottelat2,
              SUM(mealtrandet.pottelat3) AS pottelat3,
              SUM(mealtrandet.pottelat4) AS pottelat4,
              SUM(mealtrandet.pottelat5) AS pottelat5,
              FORMAT(SUM(IFNULL(mealtrandet.um1,0) + IFNULL(mealtrandet.um2,0) + IFNULL(mealtrandet.um3,0) + IFNULL(mealtrandet.um4,0) + IFNULL(mealtrandet.um5,0) + IFNULL(mealtrandet.addmeal,0)) ,0) AS tum, 
              FORMAT(SUM(mealtrandet.insentif1),0) AS insentif1,
              FORMAT(SUM(mealtrandet.insentif2),0) AS insentif2,
              FORMAT(SUM(mealtrandet.insentif3),0) AS insentif3,
              FORMAT(SUM(mealtrandet.insentif4),0) AS insentif4,
              FORMAT(SUM(mealtrandet.insentif5),0) AS insentif5,
              FORMAT(SUM(mealtrandet.correction),0) AS correction,
              FORMAT(SUM(mealtrandet.perday),0) AS perday,
              FORMAT(SUM(mealtrandet.addmeal),0) AS addmeal,
              FORMAT(SUM(mealtrandet.addinsentif),0) AS addinsentif,
              FORMAT(SUM(mealtrandet.amount),0) AS amount,
              FORMAT(SUM(mealtrandet.potabsence),0) AS potabsence,
              FORMAT(SUM(mealtrandet.amountpajak),0) AS amountpajak
            FROM
              mealtrandet
            LEFT JOIN employee ON mealtrandet.employeeid = employee.id
            LEFT JOIN location ON employee.locationid = location.idlocation
            WHERE mealtrandet.transid = '.$id.'';
    $mealtran_total = DB::table(DB::raw("($sql1) as rs_sql"))->first(); 

    return view ('editor.mealtran.form', compact('mealtran', 'mealtran_detail', 'mealtran_total', 'period'));
  }

  public function updatedetail($id, Request $request)
  {
     foreach($request->input('detail') as $key => $detail_data)
     {
      if( isset($detail_data['checklist'])){
        $checklist = 1;
      }else{
        $checklist = 0;
      };

      $mealtran_detail = Mealtrandet::Find($key); 
      $mealtran_detail->um1 = str_replace(",","",$detail_data['um1']); 
      $mealtran_detail->um2 = str_replace(",","",$detail_data['um2']);
      $mealtran_detail->um3 = str_replace(",","",$detail_data['um3']);
      $mealtran_detail->um4 = str_replace(",","",$detail_data['um4']);
      $mealtran_detail->um5 = str_replace(",","",$detail_data['um5']);
      $mealtran_detail->addmeal = str_replace(",","",$detail_data['addmeal']);
      $mealtran_detail->addinsentif = str_replace(",","",$detail_data['addinsentif']);
      $mealtran_detail->insentif1 = str_replace(",","",$detail_data['insentif1']); 
      $mealtran_detail->insentif2 = str_replace(",","",$detail_data['insentif2']);
      $mealtran_detail->insentif3 = str_replace(",","",$detail_data['insentif3']);
      $mealtran_detail->insentif4 = str_replace(",","",$detail_data['insentif4']);
      $mealtran_detail->insentif5 = str_replace(",","",$detail_data['insentif5']);
      $mealtran_detail->checklist = $checklist; 

      $mealtran_detail->save();
    } 

    return back();
    // return redirect()->action('Editor\MealtranController@index'); 
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Mealtran::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function generate($id)
  {  
    $sql_begindate = 'SELECT
                    begindate
                  FROM
                    payperiod
                  WHERE
                    id = '.$id.''; 
    $begindate = DB::table(DB::raw("($sql_begindate) as rs_sql"))->first(); 

    $sql_enddate = 'SELECT
                      enddate
                    FROM
                      payperiod
                    WHERE
                      id = '.$id.''; 
    $enddate = DB::table(DB::raw("($sql_enddate) as rs_sql"))->first(); 

    //dd("generate");
    //return response()->json($post); 
     DB::insert("INSERT INTO mealtrandet (employeeid, transid, periodid) SELECT
                  employee.id,
                  mealtran.id,
                  ".$id."
                FROM
                  mealtran
                LEFT OUTER JOIN mealtrandet ON mealtran.id = mealtrandet.transid
                CROSS JOIN employee
                WHERE
                  (
                    mealtrandet.employeeid IS NULL
                  )
                AND mealtran.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )
                AND (employee.`status` = 0 OR employee.status IS NULL)");

      DB::update("UPDATE mealtrandet,
                   mealtran,
                   potabsence,
                   employee
                  SET  
                   mealtrandet.perday = employee.mealtransrate,
                   mealtrandet.um1 = 0,
                   mealtrandet.um2 = 0,
                   mealtrandet.um3 = 0,
                   mealtrandet.um4 = 0,
                   mealtrandet.um5 = 0,
                   mealtrandet.insentif1 = 0,
                   mealtrandet.insentif2 = 0,
                   mealtrandet.insentif3 = 0,
                   mealtrandet.insentif4 = 0,
                   mealtrandet.insentif5 = 0,
                   mealtrandet.employeestatus = employee.`status`
                  WHERE
                    employee.id = mealtrandet.employeeid
                  AND mealtran.id = mealtrandet.transid
                  AND mealtran.periodid = (
                    SELECT
                      `user`.periodid
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )
                  AND employee.`status` = 0 AND (mealtrandet.checklist = 0 OR mealtrandet.checklist IS NULL)");

     
    $rate_lpk = DB::connection('mysql2')->select('SELECT
                                                    employee.nik,
                                                    employee.tunjluarkota,
                                                    employee.tunjmalam,
                                                    employee.tunjlembur,
                                                    employee.uangmakan,
                                                    employee.pottelat
                                                  FROM
                                                    employee'); 

    foreach ($rate_lpk as $key => $rate_lpks) {
        $ratelpk = Employee::where('nik', $rate_lpks->nik)->first();
        if(isset($ratelpk)){
          $ratelpk->ratemealoutcity = $rate_lpks->tunjluarkota;
          $ratelpk->ratetunjanganmalam = $rate_lpks->tunjmalam;
          $ratelpk->rateovertime = $rate_lpks->tunjlembur;
          $ratelpk->mealtransrate = $rate_lpks->uangmakan; 
          $ratelpk->save();   
        }     
    };

     //delete active period
    // DB::connection('mysql')->select('DELETE FROM logum WHERE enddate BETWEEN "'.$begindate->begindate.'" AND "'.$enddate->enddate.'"'); 
    DB::connection('mysql')->delete('TRUNCATE TABLE logum'); 

     //post overtime to log
    $log_um = DB::connection('mysql2')->select('SELECT
                                                  payroll.dayjob,
                                                  payperiod.begindate,
                                                  payperiod.enddate,
                                                  project.projectname,
                                                  payroll.upah,
                                                  payroll.totalallowance,
                                                  payroll.dayjob,
                                                  payroll.totalmealamount AS stotalmealamount,
                                                  payroll.totalmealamount - (
                                                    payroll.deductionlate * payroll.e_pottelat
                                                  ) AS totalmealamount,
                                                  payroll.deductionlate * payroll.e_pottelat AS pottelat,
                                                  employee.employeename,
                                                  employee.nik
                                                FROM
                                                  payroll
                                                INNER JOIN payperiod ON payroll.periodid = payperiod.idpayperiod
                                                INNER JOIN project ON payroll.projectid = project.idproject
                                                INNER JOIN employee ON payroll.employeeid = employee.idemployee
                                                WHERE payperiod.enddate BETWEEN "'.$begindate->begindate.'" AND "'.$enddate->enddate.'"'); 


    foreach ($log_um as $key => $log_ums) {
        $logum = new Logum();
        $logum->nik = $log_ums->nik;
        $logum->employeename = $log_ums->employeename;
        $logum->begindate = $log_ums->begindate;
        $logum->enddate = $log_ums->enddate;
        $logum->insentif = $log_ums->totalallowance; 
        $logum->uangmakan = $log_ums->totalmealamount; 
        $logum->pottelat = $log_ums->pottelat; 
        $logum->project = $log_ums->projectname;
        $logum->dayjob = $log_ums->dayjob;
        $logum->save();        
    };


    DB::connection('mysql')->select('SET @row_number = 0'); 

    DB::connection('mysql')->select('UPDATE logum
                                    INNER JOIN (
                                      SELECT
                                        (@row_number :=@row_number + 1) AS num,
                                        logum.enddate
                                      FROM
                                        (
                                          SELECT
                                            logum.enddate
                                          FROM
                                            logum
                                          GROUP BY
                                            logum.enddate
                                        ) AS logum
                                      WHERE
                                        logum.enddate BETWEEN "'.$begindate->begindate.'" AND "'.$enddate->enddate.'"
                                    ) AS seq_no ON logum.enddate = seq_no.enddate
                                    SET logum.seqno = seq_no.num'); 


        //update project
        DB::update("UPDATE mealtrandet
                    INNER JOIN (
                      SELECT
                        employee.id AS employeeid,
                        logum.nik,
                        GROUP_CONCAT(
                          logum.project SEPARATOR ' dan '
                        ) AS project
                      FROM
                        (
                          SELECT
                            nik,
                            project
                          FROM
                            logum
                          GROUP BY
                            nik, 
                            project
                        ) AS logum
                      INNER JOIN employee ON logum.nik = employee.nik
                      GROUP BY
                        logum.nik,
                        employee.id
                    ) AS logum ON mealtrandet.employeeid = logum.employeeid
                    SET mealtrandet.project = logum.project
                    WHERE mealtrandet.periodid = ".$id."");


      //1
      DB::update("UPDATE mealtrandet
                  INNER JOIN (
                    SELECT
                      logum.nik,
                      logum.employeename,
                      logum.begindate,
                      logum.enddate,
                      employee.id AS employeeid,
                      GROUP_CONCAT(logum.project SEPARATOR ' dan ') AS project,
                      SUM(logum.uangmakan) AS uangmakan,
                      SUM(logum.pottelat) AS pottelat,
                      SUM(logum.insentif) AS insentif,
                      logum.seqno
                    FROM
                      logum
                    INNER JOIN employee ON logum.nik = employee.nik 
                    WHERE
                      logum.seqno = 1
                    GROUP BY
                      logum.nik,
                      logum.employeename,
                      logum.begindate,
                      logum.enddate,
                      employee.id,
                      logum.seqno
                  ) AS logum ON mealtrandet.employeeid = logum.employeeid
                  SET mealtrandet.um1 = logum.uangmakan,  mealtrandet.insentif1 = logum.insentif 
                  WHERE mealtrandet.periodid = ".$id."");

      //2
      DB::update("UPDATE mealtrandet
                  INNER JOIN (
                    SELECT
                      logum.nik,
                      logum.employeename,
                      logum.begindate,
                      logum.enddate,
                      employee.id AS employeeid,
                      GROUP_CONCAT(logum.project SEPARATOR ' dan ') AS project,
                      SUM(logum.uangmakan) AS uangmakan,
                      SUM(logum.pottelat) AS pottelat,
                      SUM(logum.insentif) AS insentif,
                      logum.seqno
                    FROM
                      logum 
                    INNER JOIN employee ON logum.nik = employee.nik
                    WHERE
                      logum.seqno = 2
                    GROUP BY
                      logum.nik,
                      logum.employeename,
                      logum.begindate,
                      logum.enddate,
                      employee.id,
                      logum.seqno
                  ) AS logum ON mealtrandet.employeeid = logum.employeeid
                  SET mealtrandet.um2 = logum.uangmakan, mealtrandet.insentif2 = logum.insentif, mealtrandet.pottelat2 = logum.pottelat
                  WHERE mealtrandet.periodid = ".$id."");


      //3
      DB::update("UPDATE mealtrandet
                  INNER JOIN (
                    SELECT
                      logum.nik,
                      logum.employeename,
                      logum.begindate,
                      logum.enddate,
                      employee.id AS employeeid,
                      GROUP_CONCAT(logum.project SEPARATOR ' dan ') AS project,
                      SUM(logum.uangmakan) AS uangmakan,
                      SUM(logum.insentif) AS insentif,
                      SUM(logum.pottelat) AS pottelat,
                      logum.seqno
                    FROM
                      logum
                    INNER JOIN employee ON logum.nik = employee.nik
                    WHERE
                      logum.seqno = 3
                    GROUP BY
                      logum.nik,
                      logum.employeename,
                      logum.begindate,
                      logum.enddate,
                      employee.id,
                      logum.seqno
                  ) AS logum ON mealtrandet.employeeid = logum.employeeid
                  SET mealtrandet.um3 = logum.uangmakan, mealtrandet.insentif3 = logum.insentif, mealtrandet.pottelat3 = logum.pottelat
                  WHERE mealtrandet.periodid = ".$id."");


      //4
      DB::update("UPDATE mealtrandet
                  INNER JOIN (
                    SELECT
                      logum.nik,
                      logum.employeename,
                      logum.begindate,
                      logum.enddate,
                      employee.id AS employeeid,
                      GROUP_CONCAT(logum.project SEPARATOR ' dan ') AS project,
                      SUM(logum.uangmakan) AS uangmakan,
                      SUM(logum.insentif) AS insentif,
                      SUM(logum.pottelat) AS pottelat,
                      logum.seqno
                    FROM
                      logum
                    INNER JOIN employee ON logum.nik = employee.nik 
                    WHERE
                      logum.seqno = 4
                    GROUP BY
                      logum.nik,
                      logum.employeename,
                      logum.begindate,
                      logum.enddate,
                      employee.id,
                      logum.seqno
                  ) AS logum ON mealtrandet.employeeid = logum.employeeid
                  SET mealtrandet.um4 = logum.uangmakan, mealtrandet.insentif4 = logum.insentif
                  WHERE mealtrandet.periodid = ".$id."");


      //5
      DB::update("UPDATE mealtrandet
                  INNER JOIN (
                    SELECT
                      logum.nik,
                      logum.employeename,
                      logum.begindate,
                      logum.enddate,
                      employee.id AS employeeid,
                      GROUP_CONCAT(logum.project SEPARATOR ' dan ') AS project,
                      SUM(logum.uangmakan) AS uangmakan,
                      SUM(logum.insentif) AS insentif,
                      SUM(logum.pottelat) AS pottelat,
                      logum.seqno
                    FROM
                      logum
                    INNER JOIN employee ON logum.nik = employee.nik 
                    WHERE
                      logum.seqno = 5
                    GROUP BY
                      logum.nik,
                      logum.employeename,
                      logum.begindate,
                      logum.enddate,
                      employee.id,
                      logum.seqno
                  ) AS logum ON mealtrandet.employeeid = logum.employeeid
                  SET mealtrandet.um5 = logum.uangmakan, mealtrandet.insentif5 = logum.insentif, mealtrandet.pottelat5 = logum.pottelat
                  WHERE mealtrandet.periodid = ".$id.""); 
    
      }
}
