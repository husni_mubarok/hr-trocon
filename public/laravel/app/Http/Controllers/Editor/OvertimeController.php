<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\OvertimeRequest;
use App\Http\Controllers\Controller;
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Overtime; 
use App\Model\Overtimedetail; 
use App\Model\Logovertime;
use Validator;
use Response;
use App\Post;
use View;

class OvertimeController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'periodid' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::orderBy('id', 'desc')->where('status', '0')->get();
    // $sql = 'SELECT
    //           payperiod.id,
    //           payperiod.description
    //         FROM
    //           payperiod
    //         INNER JOIN overtimerequest ON payperiod.id = overtimerequest.periodid
    //         GROUP BY
    //           payperiod.id,
    //           payperiod.description';
    // $payperiod_list = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('id', 'desc')->get();
    
    $payperiod_list2 = Payperiod::orderBy('id','desc')->get()->pluck('description', 'id');
    $overtimes = Overtime::all();

    $sql1 = 'SELECT
              `user`.id,
              payperiod.id AS periodid,
              payperiod.description
            FROM
              payperiod
            INNER JOIN `user` ON payperiod.id = `user`.periodid';

    $datafilter = DB::table(DB::raw("($sql1) as rs_sql"))->where('id', Auth::id())->first(); 

    return view ('editor.overtime.index', compact('overtimes','payperiod_list', 'department_list', 'datafilter', 'payperiod_list2'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                overtimerequest.id,
                overtimerequest.periodid,  
                DATE_FORMAT(
                  payperiod.begindate,
                  "%d-%m-%Y"
                ) AS begindate,
                DATE_FORMAT(
                  payperiod.enddate,
                  "%d-%m-%Y"
                ) AS enddate,
                payperiod.begindate AS begindateshort,
                payperiod.description,
                overtimerequest.status
              FROM
                overtimerequest
              LEFT JOIN payperiod ON overtimerequest.periodid = payperiod.id
              WHERE overtimerequest.deleted_at IS NULL
              ORDER BY overtimerequest.id DESC ';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('id', 'desc')->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->description."'".')"> Hapus</a>';
      })

      ->addColumn('actiondetail', function ($itemdata) {
        return '<a href="overtime/'.$itemdata->id.'/editdetail" title="Detail" class="btn btn-primary btn-xs btn-flat"><i class="fa fa-folder"></i> Detail</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Aktif </span>';
        }else{
         return '<span class="label label-danger"> Tidak Aktif </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
      $post = new Overtime(); 
      $post->periodid = $request->periodid; 
      $post->departmentid = $request->departmentid; 
      $post->status = $request->status;
      $post->created_by = Auth::id();
      $post->save();

      return response()->json($post); 
    }
  }

  public function edit($id)
  {
    $overtime = Overtime::Find($id);
    echo json_encode($overtime); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
      $post = Overtime::Find($id); 
      $post->periodid = $request->periodid; 
      $post->departmentid = $request->departmentid; 
      $post->status = $request->status;
      $post->updated_by = Auth::id();
      $post->save();

      return response()->json($post); 
    }
  }

  public function editdetail($id)
  {
    
    // $overtime = Overtime::Find($id); 

    $sqlm = 'SELECT
              overtimerequest.id,
              overtimerequest.codetrans,
              overtimerequest.notrans,
              overtimerequest.datetrans,
              overtimerequest.departmentcode,
              overtimerequest.departmentid,
              overtimerequest.periodid,
              overtimerequest.remark,
              payperiod.description
            FROM
              overtimerequest
            LEFT JOIN payperiod ON overtimerequest.periodid = payperiod.id
            WHERE overtimerequest.id = '.$id.'';
    $overtime = DB::table(DB::raw("($sqlm) as rs_sql"))->first();

    $sql = 'SELECT
          employee.identityno,
          employee.employeename,
          employee.nik,
          employee.departmentid,
          employee.gol,
          location.locationname,
          department.departmentname,
          overtimerequestdet.id,
          overtimerequestdet.project,
          overtimerequestdet.transid,
          overtimerequestdet.checklist,
          overtimerequestdet.employeeid,
          overtimerequestdet.employeestatus,
          FORMAT(overtimerequestdet.rateovertime,0) AS rateovertime,
          FORMAT(overtimerequestdet.ot1,1) AS ot1,
          FORMAT(overtimerequestdet.ot2,1) AS ot2,
          FORMAT(overtimerequestdet.ot3,1) AS ot3,
          FORMAT(overtimerequestdet.ot4,1) AS ot4,
          FORMAT(overtimerequestdet.ot5,1) AS ot5,
          FORMAT(overtimerequestdet.otcorrection,1) AS otcorrection,
          FORMAT(IFNULL(overtimerequestdet.ot1,0) + IFNULL(overtimerequestdet.ot2,0) + IFNULL(overtimerequestdet.ot3,0) + IFNULL(overtimerequestdet.ot4,0) + IFNULL(overtimerequestdet.ot5,0) + IFNULL(overtimerequestdet.otcorrection,0),1) AS tovertime,
          FORMAT(IFNULL(overtimerequestdet.ot1,0) + IFNULL(overtimerequestdet.ot2,0) + IFNULL(overtimerequestdet.ot3,0) + IFNULL(overtimerequestdet.ot4,0) + IFNULL(overtimerequestdet.ot5,0) + IFNULL(overtimerequestdet.addovertime,0),0) AS grandtotal,
          FORMAT(overtimerequestdet.overtime,0) AS overtime,
          FORMAT(overtimerequestdet.otin,0) AS otin,
          FORMAT(overtimerequestdet.otout,0) AS otout,
          FORMAT(overtimerequestdet.amount,0) AS amount,
          overtimerequestdet.addovertime AS addovertime,
          overtimerequestdet.description,
          overtimerequestdet.locationid
        FROM
          overtimerequestdet
        INNER JOIN employee ON overtimerequestdet.employeeid = employee.id
        LEFT JOIN location ON employee.locationid = location.idlocation
        LEFT JOIN department ON employee.departmentid = department.id
        WHERE overtimerequestdet.transid = '.$id.' AND employee.status = 0 AND (employee.overtimeall = 0 OR employee.overtimeall IS NULL)';
    $overtime_detail = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('departmentname', 'asc')->orderBy('id', 'asc')->get(); 


    $sql = 'SELECT 
              FORMAT(SUM(overtimerequestdet.rateovertime),0) AS rateovertime,
              FORMAT(SUM(overtimerequestdet.ot1),1) AS ot1,
              FORMAT(SUM(overtimerequestdet.ot2),1) AS ot2,
              FORMAT(SUM(overtimerequestdet.ot3),1) AS ot3,
              FORMAT(SUM(overtimerequestdet.ot4),1) AS ot4,
              FORMAT(SUM(overtimerequestdet.ot5),1) AS ot5,
              FORMAT(SUM(overtimerequestdet.otcorrection),1) AS otcorrection,
              FORMAT(SUM(overtimerequestdet.overtime),0) AS overtime,
              FORMAT(SUM(overtimerequestdet.otin),0) AS otin,
              FORMAT(SUM(overtimerequestdet.otout),0) AS otout,
              FORMAT(SUM(overtimerequestdet.amount),0) AS amount,
              FORMAT(SUM(overtimerequestdet.addovertime),0) AS addovertime ,
              FORMAT(SUM(IFNULL(overtimerequestdet.ot1,0) + IFNULL(overtimerequestdet.ot2,0) + IFNULL(overtimerequestdet.ot3,0) + IFNULL(overtimerequestdet.ot4,0) + IFNULL(overtimerequestdet.ot5,0) + IFNULL(overtimerequestdet.otcorrection,0)),0) AS tovertime
            FROM
              overtimerequestdet
            INNER JOIN employee ON overtimerequestdet.employeeid = employee.id
            LEFT JOIN location ON employee.locationid = location.idlocation
            WHERE overtimerequestdet.transid = '.$id.' AND employee.status = 0 AND employee.overtimeall = 0';
    $overtime_total = DB::table(DB::raw("($sql) as rs_sql"))->first(); 

    return view ('editor.overtime.form', compact('overtime', 'overtime_detail', 'overtime_total'));
  }

  public function updatedetail($id, Request $request)
  {
     foreach($request->input('detail') as $key => $detail_data)
    {
      if( isset($detail_data['checklist'])){
        $checklist = 1;
      }else{
        $checklist = 0;
      };

      $overtime_detail = Overtimedetail::Find($key); 
      $overtime_detail->ot1 = str_replace(",","",$detail_data['ot1']); 
      $overtime_detail->ot2 = str_replace(",","",$detail_data['ot2']);
      $overtime_detail->ot3 = str_replace(",","",$detail_data['ot3']);
      $overtime_detail->ot4 = str_replace(",","",$detail_data['ot4']);
      $overtime_detail->ot5 = str_replace(",","",$detail_data['ot5']);
      $overtime_detail->otcorrection = str_replace(",","",$detail_data['otcorrection']);
      $overtime_detail->addovertime = str_replace(",","",$detail_data['addovertime']);
      $overtime_detail->rateovertime = str_replace(",","",$detail_data['rateovertime']);
      $overtime_detail->checklist = $checklist;

      $overtime_detail->save();
    } 

    return back();
    // return redirect()->action('Editor\OvertimeController@index'); 
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Overtime::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function generate($id)
  {  

    $sql_begindate = 'SELECT
                        begindate
                      FROM
                        payperiod
                      WHERE
                        id = '.$id.''; 
    $begindate = DB::table(DB::raw("($sql_begindate) as rs_sql"))->first(); 

    $sql_enddate = 'SELECT
                      enddate
                    FROM
                      payperiod
                    WHERE
                      id = '.$id.''; 
    $enddate = DB::table(DB::raw("($sql_enddate) as rs_sql"))->first(); 
 
    
    //delete active period
    // DB::connection('mysql')->select('DELETE FROM logovertime WHERE enddate BETWEEN "'.$begindate->begindate.'" AND "'.$enddate->enddate.'"'); 

    DB::connection('mysql')->select('TRUNCATE TABLE logovertime'); 

    //post overtime to log
    $log_overtime = DB::connection('mysql2')->select('SELECT
                                                        payroll.employeeid,
                                                        employee.nik,
                                                        employee.employeename,
                                                        employee.tunjlembur,
                                                        payperiod.dateperiod,
                                                        payperiod.begindate,
                                                        payperiod.enddate,
                                                        payroll.thp,
                                                        project.projectname,
                                                        payroll.e_uangmakan,
                                                        ROUND(absence.t_workhour) AS t_workhour
                                                      FROM
                                                        payroll
                                                      INNER JOIN payperiod ON payroll.periodid = payperiod.idpayperiod
                                                      INNER JOIN employee ON payroll.employeeid = employee.idemployee
                                                      INNER JOIN project ON project.idproject = payroll.projectid
                                                      INNER JOIN (
                                                        SELECT
                                                          absence.employeeid,
                                                          absence.periodid,
                                                          SUM(
                                                            absence.workhour + IFNULL(absence.koreksijamlembur, 0)
                                                          ) AS t_workhour
                                                        FROM
                                                          absence
                                                        GROUP BY
                                                          absence.employeeid,
                                                          absence.periodid
                                                      ) AS absence ON payroll.employeeid = absence.employeeid
                                                      AND payroll.periodid = absence.periodid WHERE payperiod.enddate BETWEEN "'.$begindate->begindate.'" AND "'.$enddate->enddate.'"
                                                      GROUP BY
                                                        payroll.employeeid,
                                                        employee.nik,
                                                        employee.employeename,
                                                        payperiod.dateperiod,
                                                        payperiod.begindate,
                                                        payperiod.enddate,
                                                        payroll.thp'); 

    

    foreach ($log_overtime as $key => $log_overtimes) {
        $logovertime = new Logovertime();
        $logovertime->nik = $log_overtimes->nik;
        $logovertime->employeename = $log_overtimes->employeename;
        $logovertime->begindate = $log_overtimes->begindate;
        $logovertime->enddate = $log_overtimes->enddate;
        $logovertime->overtime = $log_overtimes->thp;
        $logovertime->project = $log_overtimes->projectname;
        $logovertime->overtimehour = $log_overtimes->t_workhour;
        $logovertime->rateovertime = $log_overtimes->tunjlembur;
        $logovertime->save();        
    };

     DB::connection('mysql')->select('SET @row_number = 0'); 

     DB::connection('mysql')->select('UPDATE logovertime
                                      INNER JOIN (
                                        SELECT
                                          (@row_number :=@row_number + 1) AS num,
                                          logovertime.enddate
                                        FROM
                                          (
                                            SELECT
                                              logovertime.enddate
                                            FROM
                                              logovertime
                                            GROUP BY
                                              logovertime.enddate
                                          ) AS logovertime
                                        WHERE
                                          logovertime.enddate BETWEEN "'.$begindate->begindate.'" AND "'.$enddate->enddate.'"
                                      ) AS seq_no ON logovertime.enddate = seq_no.enddate
                                      SET logovertime.seqno = seq_no.num'); 

    //dd("generate");
    //return response()->json($post); 
     DB::insert("INSERT INTO overtimerequestdet (
                    employeeid,
                    transid,
                    rateovertime,
                    periodid
                  ) SELECT
                    employee.id AS employeeid,
                    overtimerequest.id AS transid,
                    employee.rateovertime,
                    overtimerequest.periodid AS periodid
                  FROM
                    overtimerequest
                  LEFT OUTER JOIN overtimerequestdet ON overtimerequest.id = overtimerequestdet.transid
                  CROSS JOIN employee
                  WHERE
                    (
                      overtimerequestdet.employeeid IS NULL
                    )
                  AND overtimerequest.periodid = (
                    SELECT
                      `user`.periodid
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )
                  AND employee.`status` = 0");


     DB::update("UPDATE overtimerequestdet,
                   employee
                  SET  
                   overtimerequestdet.ot1 = 0,
                   overtimerequestdet.ot2 = 0,
                   overtimerequestdet.ot3 = 0,
                   overtimerequestdet.ot4 = 0,
                   overtimerequestdet.ot5 = 0 
                  WHERE  overtimerequestdet.periodid = (
                    SELECT
                      `user`.periodid
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )
                  AND employee.`status` = 0 AND (overtimerequestdet.checklist = 0 OR overtimerequestdet.checklist IS NULL) AND overtimerequestdet.periodid = ".$id."");

        DB::update("UPDATE overtimerequestdet,
                 employee
                SET  
                 overtimerequestdet.otcorrection = 0 
                WHERE  overtimerequestdet.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )
                AND employee.`status` = 0 AND overtimerequestdet.otcorrection = '' AND overtimerequestdet.periodid = ".$id."");

      DB::update("UPDATE overtimerequest
                  INNER JOIN overtimerequestdet
                  INNER JOIN employee
                  SET overtimerequestdet.rateovertime = employee.rateovertime,
                   overtimerequestdet.employeestatus = employee.`status`
                  WHERE
                    employee.id = overtimerequestdet.employeeid
                  AND overtimerequest.id = overtimerequestdet.transid
                  AND overtimerequest.periodid = (
                    SELECT
                      `user`.periodid
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )
                  AND overtimerequestdet.periodid = ".$id."");

      //ot1
      DB::update("UPDATE overtimerequestdet
                  INNER JOIN (
                    SELECT
                      logovertime.id,
                      logovertime.nik,
                      logovertime.employeename,
                      logovertime.begindate,
                      logovertime.enddate,
                      logovertime.overtime,
                      logovertime.overtimehour,
                      logovertime.rateovertime,
                      employee.id AS employeeid,
                      logovertime.project
                    FROM
                      logovertime
                    INNER JOIN employee ON logovertime.nik = employee.nik
                    WHERE
                      logovertime.seqno = 1
                  ) AS logovertime ON overtimerequestdet.employeeid = logovertime.employeeid
                  SET overtimerequestdet.ot1 = logovertime.overtimehour, overtimerequestdet.project = logovertime.project, overtimerequestdet.rateovertime = logovertime.rateovertime
                  WHERE overtimerequestdet.checklist = 0 OR overtimerequestdet.checklist IS NULL  AND overtimerequestdet.periodid = ".$id."");

      //ot2
      DB::update("UPDATE overtimerequestdet
                  INNER JOIN (
                    SELECT
                      logovertime.id,
                      logovertime.nik,
                      logovertime.employeename,
                      logovertime.begindate,
                      logovertime.enddate,
                      logovertime.overtime,
                      logovertime.overtimehour,
                      employee.id AS employeeid
                    FROM
                      logovertime
                    INNER JOIN employee ON logovertime.nik = employee.nik
                    WHERE
                      logovertime.seqno = 2
                  ) AS logovertime ON overtimerequestdet.employeeid = logovertime.employeeid
                  SET overtimerequestdet.ot2 = logovertime.overtimehour
                  WHERE overtimerequestdet.checklist = 0 OR overtimerequestdet.checklist IS NULL AND overtimerequestdet.periodid = ".$id."");

      //ot3
      DB::update("UPDATE overtimerequestdet
                  INNER JOIN (
                    SELECT
                      logovertime.id,
                      logovertime.nik,
                      logovertime.employeename,
                      logovertime.begindate,
                      logovertime.enddate,
                      logovertime.overtime,
                      logovertime.overtimehour, 
                      employee.id AS employeeid
                    FROM
                      logovertime
                    INNER JOIN employee ON logovertime.nik = employee.nik
                    WHERE
                      logovertime.seqno = 3
                  ) AS logovertime ON overtimerequestdet.employeeid = logovertime.employeeid
                  SET overtimerequestdet.ot3 = logovertime.overtimehour
                  WHERE overtimerequestdet.checklist = 0 OR overtimerequestdet.checklist IS NULL AND overtimerequestdet.periodid = ".$id."");

      //ot4
      DB::update("UPDATE overtimerequestdet
                  INNER JOIN (
                    SELECT
                      logovertime.id,
                      logovertime.nik,
                      logovertime.employeename,
                      logovertime.begindate,
                      logovertime.enddate,
                      logovertime.overtime,
                      logovertime.overtimehour, 
                      employee.id AS employeeid
                    FROM
                      logovertime
                    INNER JOIN employee ON logovertime.nik = employee.nik
                    WHERE
                      logovertime.seqno = 4
                  ) AS logovertime ON overtimerequestdet.employeeid = logovertime.employeeid
                  SET overtimerequestdet.ot4 = logovertime.overtimehour
                  WHERE overtimerequestdet.checklist = 0 OR overtimerequestdet.checklist IS NULL AND overtimerequestdet.periodid = ".$id."");

      //ot5
      DB::update("UPDATE overtimerequestdet
                  INNER JOIN (
                    SELECT
                      logovertime.id,
                      logovertime.nik,
                      logovertime.employeename,
                      logovertime.begindate,
                      logovertime.enddate,
                      logovertime.overtime,
                      logovertime.overtimehour, 
                      employee.id AS employeeid
                    FROM
                      logovertime
                    INNER JOIN employee ON logovertime.nik = employee.nik
                    WHERE
                      logovertime.seqno = 5
                  ) AS logovertime ON overtimerequestdet.employeeid = logovertime.employeeid
                  SET overtimerequestdet.ot5 = logovertime.overtimehour
                  WHERE overtimerequestdet.checklist = 0 OR overtimerequestdet.checklist IS NULL AND overtimerequestdet.periodid = ".$id."");


      //update project
      DB::update("UPDATE overtimerequestdet
                  INNER JOIN (
                    SELECT
                      employee.id AS employeeid,
                      logovertime.nik,
                      GROUP_CONCAT(
                        logovertime.project SEPARATOR ' dan '
                      ) AS project
                    FROM
                      (
                        SELECT
                          nik,
                          project
                        FROM
                          logovertime
                        GROUP BY
                          nik, 
                          project
                      ) AS logovertime
                    INNER JOIN employee ON logovertime.nik = employee.nik
                    GROUP BY
                      logovertime.nik,
                      employee.id
                  ) AS logum ON overtimerequestdet.employeeid = logum.employeeid
                  SET overtimerequestdet.project = logum.project
                  WHERE overtimerequestdet.periodid = ".$id."");

  }
}
