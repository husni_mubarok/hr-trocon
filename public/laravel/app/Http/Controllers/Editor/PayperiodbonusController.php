<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayperiodbonusRequest;
use App\Http\Controllers\Controller;
use App\Model\Payperiodbonus; 
use App\Model\Year;
use App\Model\Payrolltype;
use Validator;
use Response;
use App\Post;
use View;

class PayperiodbonusController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'description' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  { 
    $payperiods = Payperiodbonus::all();
    $year_list = Year::all()->pluck('yearname', 'yearname');
    $payrolltype_list = Payrolltype::all()->pluck('payrolltypename', 'id');

    return view ('editor.payperiodbonus.index', compact('payperiods', 'year_list', 'payrolltype_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
              payperiodbonus.id,
              payperiodbonus.description,
              payperiodbonus.dateperiod,
              payperiodbonus.begindate,
              payperiodbonus.enddate,
              payperiodbonus.paydate,
              payperiodbonus.`status`,
              payperiodbonus.`month`,
              payperiodbonus.`year`,
              payrolltype.payrolltypename
            FROM
              payperiodbonus
            LEFT JOIN payrolltype ON payperiodbonus.payrolltypeid = payrolltype.id
            WHERE payperiodbonus.deleted_at IS NULL';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->description."'".')"> Hapus</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Aktif </span>';
        }else{
         return '<span class="label label-danger"> Tidak Aktif </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $post = new Payperiodbonus(); 
            $post->description = $request->description; 
            $post->dateperiod = $request->dateperiod; 
            $post->begindate = $request->begindate; 
            $post->enddate = $request->enddate; 
            $post->paydate = $request->paydate;  
            $post->status = $request->status;
            $post->created_by = Auth::id();
            $post->save();

            return response()->json($post); 
          }
  }

  public function edit($id)
  {
    $payperiod = Payperiodbonus::Find($id);
    echo json_encode($payperiod); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
      $post = Payperiodbonus::Find($id);  
      $post->description = $request->description; 
      $post->dateperiod = $request->dateperiod; 
      $post->begindate = $request->begindate; 
      $post->enddate = $request->enddate; 
      $post->paydate = $request->paydate;  
      $post->status = $request->status;
      $post->updated_by = Auth::id();
      $post->save();

      return response()->json($post); 
    }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Payperiodbonus::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Payperiodbonus::where('id', $id["1"])->get();
    $post = Payperiodbonus::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
