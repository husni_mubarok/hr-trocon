<?php

namespace App\Http\Controllers\Editor\Payroll;

use Auth;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Bonusdetail;
use App\Model\Bonus;

class BonusCalculationController extends Controller
{
    public function preview()
    {
    	$datafilter = User::where('id', Auth::id())->first();
    	$department_list = Department::orderBy('id', 'ASC')->get();
        $bonus_list = Bonus::orderBy('id', 'desc')->where('status', '0')->groupBy('description')->get();
        
        return view ('editor.payroll.bonus_preview.index', [
            'department_list' => $department_list,
            'bonus_list' => $bonus_list,
            'datafilter' => $datafilter
        ]);
    }

    public function getDataAjax(Request $request)
    {
        if (!$request->input('bonus_date')) {
            $department = Department::orderBy('id', 'ASC')->first();
            $departmentId = $department->id;
            
            $result = Bonus::where('departmentid', $departmentId)->orderBy('bonus.id', 'DESC')->first();
            $result = $result->bonusDets;
            foreach ($result as $record) {
                if (!$record->employee || $record->employee->deleted_at) continue;
                $record['position'] = ($record->employee->positionid) ? $record->employee->jobtitle->positionname : null;
                $record['department'] = ($record->employee->departmentid) ? $record->employee->department->departmentname : null;
                $record['employeename'] = ($record->employee) ? $record->employee->employeename : null;
            }
        } else {
            $bonusDate = $request->input('bonus_date');
            $departmentId = $request->input('department_id');
            $result = Bonusdetail::join('bonus', 'bonus.id', '=', 'bonusdet.transid')
                        ->join('employee', 'bonusdet.employeeid', '=', 'employee.id')
                        ->whereNull('employee.deleted_at')
                        ->where('bonus.departmentid', $departmentId)
                        ->where('bonus.datetrans', $bonusDate)
                        ->get();
            foreach ($result as $record) {
                if (!$record->employee || $record->employee->deleted_at) continue;
                $record['position'] = ($record->employee->positionid) ? $record->employee->jobtitle->positionname : null;
                $record['department'] = ($record->employee->departmentid) ? $record->employee->department->departmentname : null;
                $record['employeename'] = ($record->employee) ? $record->employee->employeename : null;
            }
        }
    	
        return response()->json([
            'draw' => 0,
            'recordsTotal' => count($result),
            'recordsFiltered' => count($result),
            'data' => $result,
            'input' => []
        ]);  
    }
}
