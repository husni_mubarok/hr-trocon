<?php

namespace App\Http\Controllers\Editor\Payroll;

use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Dompdf\Dompdf;
use Dompdf\Options;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Mail\ReservationEmail;
use App\Model\Employee;
use App\Model\Position;
use App\Model\Payperiod;
use App\Model\Payroll;
use App\Model\Umr; 
use App\Model\Bonus;
use App\Model\Payroll\Bonus as BonusPayslips;
use App\Model\Bonusdetail;
use App\Model\Payroll\Payslip;
use App\Model\Payroll\PayslipItem;
use App\Model\Payroll\Correction;
use App\Model\Payroll\SettingBracket;
use App\Model\Payroll\SettingAssurance;
use App\Model\Payroll\MasterPtkp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BonusController extends Controller
{
    protected $months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ini_set('max_execution_time', 0);

        if (!$request->has('bonus_date')) {
            return response()->json('There is not bonus date', 500);
        }
        
        $bonus = Bonus::where('datetrans', $request->get('bonus_date'))->get();
        if (empty($bonus)) {
            return response()->json(['message' => 'Periode tidak ditemukan'], 500);   
        }

        $datePeriod = Carbon::parse($bonus[0]->datetrans);
        $datePeriod = Carbon::parse($datePeriod->format('Y-m-01'));
        
        $payslips = BonusPayslips::where('date', $datePeriod)->get();
        if ($payslips->count() > 0) {
            return response()->json(['message' => 'Bonus ' . $this->months[$datePeriod->format('m') - 1] . ' ' . $datePeriod->format('Y') . ' sudah dihitung'], 500);      
        }

        $defaultValue = $this->defaultComponentPayroll();
        $this->calculation($datePeriod, $defaultValue, $bonus);

        return response()->json([
            'success' => true,
            'message' => 'Calculation completed'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payslips = BonusPayslips::where('payroll_period_id', $id)->get();
        foreach ($payslips as $payslip) {
            $payslip->delete();
        }

        return response()->json([
            'success' => true,
            'message' => 'Delete completed'
        ], 200);
    }

    public function showResult(Request $request)
    {
        return view ('editor.payroll.bonus_result.index');
    }

    public function showResultData()
    {
        $payslips = BonusPayslips::groupBy('payroll_period_id')->orderBy('id', 'DESC')->get();
        foreach ($payslips as $payslip) {
            $payslip['date_format'] = Carbon::parse($payslip->date)->format('F Y');
            $payslip['action'] = '<a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$payslip->payroll_period_id."', '".$payslip['date_format']."'".')"> Hapus</a> | <a  href="result/detail/'.$payslip->payroll_period_id.'" title="Detail"> Buka</a> | <a target="_blank" href="result/'.$payslip->payroll_period_id.'" title="Download"> Download</a>';
        }
       
        return response()->json([
            'draw' => 0,
            'recordsTotal' => count($payslips),
            'recordsFiltered' => count($payslips),
            'data' => $payslips,
            'input' => []
        ]);  
    }

    public function showResultDetail($id)
    {
        return view ('editor.payroll.bonus_result.index_detail', [
            'periodId' => $id
        ]);
    }

    public function showResultDetailData(Request $request)
    {
        $payslips = BonusPayslips::where('payroll_period_id', $request->get('period_id'))->orderBy('id', 'ASC')->get();
        foreach ($payslips as $payslip) {
            $month = Carbon::parse($payslip->date)->format('m');
            $year  = Carbon::parse($payslip->date)->format('Y');
            $employee = $payslip->employee;

            $payslip['action'] = '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" onclick="detail_payslip('."'".$payslip->id."'".')">Buka Payslip</button>';
            $payslip['date_format'] = Carbon::parse($payslip->date)->format('F Y');
            $payslip['employee_name'] = $payslip->employee->employeename;
            $payslip['bonus'] = number_format((int) $payslip->component_bonus);
            $payslip['pph_21'] = number_format((int) $payslip->component_irregular_tax);
            $payslip['potongan'] = number_format((int) $payslip->component_deduction);
            $payslip['bonus_final'] = number_format((int) $payslip->component_bonus_final);
        }

        return response()->json([
            'draw' => 0,
            'recordsTotal' => count($payslips),
            'recordsFiltered' => count($payslips),
            'data' => $payslips,
            'input' => []
        ]);  
    }

    public function showPayslip($payslipId)
    {
        $payslip  = BonusPayslips::find($payslipId);
        $periodId = $payslip->payroll_period_id;
        $employeeId = $payslip->employee->id;
        $payroll  = Payroll::where('periodid', $periodId)->where('employeeid', $employeeId)->first();
        $content = $this->createContentModalPayslip($payslip, $payroll);

        return response()->json($content);
    }

    public function downloadPayslips($periodId)
    {
        $payslips = BonusPayslips::where('payroll_period_id', $periodId)->get();
        
        $resultCalculation = [];
        foreach ($payslips as $index => $payslip) {
            $periodId = $payslip->payroll_period_id;
            $employeeId = $payslip->employee->id;
            $payroll  = Payroll::where('periodid', $periodId)->where('employeeid', $employeeId)->first();
            $otherInformations = $this->getPayslipInformation($payslip);

            $hutang = 0;
            if ($payroll) $hutang = (int) $payroll->remaininsuranceloan + (int) $payroll->remainemployeeloan;

            $resultCalculation[$index]['other_information'] = $otherInformations;
            $resultCalculation[$index]['value'] = $payslip->component_value;
            $resultCalculation[$index]['bonus'] = (int) $payslip->component_bonus;
            $resultCalculation[$index]['tax'] = (int) $payslip->component_irregular_tax;
            $resultCalculation[$index]['potongan'] = (int) $payslip->component_deduction;
            $resultCalculation[$index]['bonus_final'] = (int) $payslip->component_bonus_final;
            $resultCalculation[$index]['hutang'] = (int) $hutang;
        }

        return view ('editor.payroll.bonus_result.print_payslip', ['resultCalculation' => $resultCalculation]);
    }

    public function downloadPayslip($payslipId)
    {
        $payslip  = BonusPayslips::find($payslipId);
        $periodId = $payslip->payroll_period_id;
        $employeeId = $payslip->employee->id;
        $payroll  = Payroll::where('periodid', $periodId)->where('employeeid', $employeeId)->first();
            
        $pdf = $this->createContentEmailPayslip($payslip, $payroll);
        $pdf = $this->createPdf($pdf);
        $url = $this->savePdf($pdf);

        return response()->json([
            'url' => $url
        ]);
    }

    public function sendPayslip($payslipId)
    {
        $payslips = BonusPayslips::where('payroll_period_id', $payslipId)->get();
        
        $resultCalculation = [];
        foreach ($payslips as $index => $payslip) {
            $periodId = $payslip->payroll_period_id;
            $employeeId = $payslip->employee->id;
            $payroll  = Payroll::where('periodid', $periodId)->where('employeeid', $employeeId)->first();

            $pdf = $this->createContentEmailPayslip($payslip, $payroll);
            $pdf = $this->createPdf($pdf);
            $folderPath = $this->savePdf($pdf);
            $email = $this->sendEmail($payslip, $folderPath);
        }

        return response()->json(['result' => true]);
    }

    public function download()
    {
        $validation = \Validator::make(request()->input(), ['filename' => 'required']);

        if ($validation->fails()) {
            return response()->json([
                'errors' => $validation->errors()
            ]);
        }

        $filename = base64_decode(request()->get('filename'));

        return response()->download(storage_path($filename));
    }

    public function downloadReport($id)
    {
        $payslips = BonusPayslips::where('payroll_period_id', $id)->get();
        $header = ['-', 'Nama Karyawan', 'Periode', 'Value', 'Bonus', 'Pajak', 'Potongan', 'Bonus Final'];
        $filename = 'Bonus-Trocon';

        Excel::create($filename, function ($excel) use ($header, $payslips) {
            $excel->sheet('Sheet1', function ($sheet) use ($header, $payslips) {
                $sheet->row(1, $header);
                
                $i = 0;
                foreach ($payslips as $i => $payslip) {
                    $sheet->prependRow(2 + $i,
                        [
                            $i + 1,
                            $payslip->employee->employeename,
                            $payslip->date,
                            $payslip->component_value,
                            $payslip->component_bonus,
                            $payslip->component_irregular_tax,
                            $payslip->component_deduction,
                            $payslip->component_bonus_final
                        ]
                    );

                    $i++;
                }
            });
        })->download('xls');
    }

    /* ============================== PRIVATE ============================== */

    private function generatePayrollPeriod($period)
    {
        return Carbon::parse($period->dateperiod);
    }

    private function defaultComponentPayroll()
    {
        //BPJS
        $bpjsSetting = SettingAssurance::all();
        $bpjsRate = [];
        foreach ($bpjsSetting as $value) {
            $bpjsRate[$value['code']] = [
                'kode'     => $value['code'],
                'name'     => $value['name'],
                'employee' => $value['prorate_employee'],
                'company'  => $value['prorate_company'],
                'max'      => $value['max']
            ];
        }

        //BRACKET
        $bracketSetting = SettingBracket::all();
        $bracketRate = [];
        foreach ($bracketSetting as $value) {
            $bracketRate[$value['code']] = [
                'kode'    => $value['code'],
                'name'    => $value['name'],
                'min'     => $value['min'],
                'max'     => $value['max'],
                'prorate' => $value['prorate'],
                'class'   => $value['class'],
            ];
        }

        return [
            'bpjs_rate'    => $bpjsRate,
            'bracket_rate' => $bracketRate
        ];
    }

    private function calculation($datePeriod, $defaultValue, $bonuses)
    {
        $period = Carbon::parse($datePeriod);
        $period = PayPeriod::whereMonth('dateperiod', $period->format('m'))
                    ->whereYear('dateperiod', $period->format('Y'))
                    ->first();

        foreach ($bonuses as $bonus) {
            $bonusDets = $bonus->bonusDets;
            foreach ($bonusDets as $bonusRecord) {
                $employee = $this->findEmployee($bonusRecord->employeeid);
                if (!$employee) continue; // [CASE] jika tidak ada employee
                
                $payroll = Payroll::where('employeeid', $employee->id)
                            ->where('periodid', $period->id)
                            ->first();
                if (!$payroll) continue; // [CASE] jika tidak ada di list payroll

                $payrollData = $this->collectPayrollDataForCalculation($employee, $period, $bonusRecord);
                $result = $this->calculate($payrollData, $defaultValue, $period, $employee);
                
                try {
                    $payslip = $this->createPayslip($payrollData, $result, $period, $employee, $bonusRecord);
                } catch (\Exception $e) {
                    continue;
                }
            }
        }

        return true;
    }

    private function findEmployee($id)
    {
        return Employee::find($id);
    }

    private function collectPayrollDataForCalculation($employee, $period, $bonusRecord)
    {
        $paidComponent = $this->collectPaidComponent($employee, $period);
        $salaryComponent = $this->collectSalaryComponent($employee, $period, $bonusRecord);
        $ptkpAmount = $this->collectPtkpAmount($employee, $period);
        $date = $this->collectPayrollPeriod($employee, $period);
        
        return [
            'paid_component' => $paidComponent,
            'salary_component' => $salaryComponent,
            'ptkp_amount' => $ptkpAmount,
            'ptkp_status' => $employee->taxstatus,
            'date' => $date,
            'npwp' => $employee->npwp,
            'value' => $bonusRecord->value
        ];
    }

    private function calculate($payrollData, $defaultValue, $period, $employee)
    {
        $assurance = $this->calculateAssurance($payrollData['salary_component'], $defaultValue, $employee);
        
        $tax = $this->calculateTaxAndParse(
                    $payrollData['paid_component'], 
                    $payrollData['salary_component'], 
                    $payrollData['ptkp_amount'],
                    $payrollData['date'],
                    $payrollData['npwp'],
                    $assurance, 
                    $defaultValue
        );
        
        return [
            'assurance' => $assurance,
            'tax' => $tax
        ];
    }

    private function createPayslip($payslipData, $result, $period, $employee, $bonusRecord)
    {
        $payslip = new BonusPayslips();
        $payslip->payroll_period_id = $period->id;
        $payslip->employee_id = $employee->id;
        $payslip->bonus_id = $bonusRecord->transid;
        $payslip->bonus_det_id = $bonusRecord->id;
        $payslip->date = $period->dateperiod;
        $payslip->tax_number = $payslipData['npwp'];
        $payslip->ptkp_status = $payslipData['ptkp_status'];
        $payslip->component_bonus = $result['tax']['irregularIncome'];
        $payslip->component_value = $payslipData['value'];
        $payslip->component_ptkp = $result['tax']['ptkp'];
        $payslip->component_irregular_tax = $result['tax']['irregularTax'];
        $payslip->component_deduction = $result['tax']['insentiveDeduction'];
        $payslip->component_bonus_final = $result['tax']['irregularIncome'] - $result['tax']['irregularTax'] - $result['tax']['insentiveDeduction'];
        $payslip->save();

        return $payslip;
    }

    private function collectPaidComponent($employee, $period)
    {
        $date = Carbon::parse($period->dateperiod);
        $payslips = Payslip::where('date', '<', $date)
                        ->where('employee_id', $employee->id)
                        ->whereYear('date', $date)
                        ->get();

        $correction = Correction::whereYear('date', $date)
                        ->where('employee_id', $employee->id)
                        ->first();

        $brutoRegularIncome = 0;
        $brutoNonRegularIncome = 0;
        $taxAllowanceDeduct = 0;
        $regularTax = 0;
        $irregularTax = 0;
        $yearlyNettoIncome = 0;
        $lastPaidTax = 0;

        if ($correction) {
            $yearlyNettoIncome = (int) $correction->exist_yearly_netto_income_total;
            $lastPaidTax = (int) $correction->exist_tax_paid_total;
        }

        foreach ($payslips as $payslip) {
            $regularTax += (int) $payslip->component_regular_tax;
            $irregularTax += (int) $payslip->component_irregular_tax;
            $brutoRegularIncome += ((int) $payslip->component_basic_salary + 
                                        (int) $payslip->component_tax_allowance_regular +
                                        (int) $payslip->component_allowance +
                                        (int) $payslip->component_tax_allowance_earn);
            $brutoNonRegularIncome += ((int) $payslip->component_non_regular_income +
                                           (int) $payslip->component_tax_allowance_irregular);
            $taxAllowanceDeduct += (int) $payslip->component_tax_allowance_deduct;
            $yearlyNettoIncome += (int) $payslip->component_last_netto_income;
        }

        return [
            'brutoRegularIncome'    => $brutoRegularIncome,
            'brutoNonRegularIncome' => $brutoNonRegularIncome,
            'taxAllowanceDeduct'    => $taxAllowanceDeduct,
            'regularTax'    => $regularTax,
            'nonRegularTax' => $irregularTax,
            'yearlyNettoIncome' => $yearlyNettoIncome,
            'lastPaidTax' => $lastPaidTax
        ];
    }

    private function collectSalaryComponent($employee, $period, $bonusRecord)
    {
        $payroll = Payroll::where('employeeid', $employee->id)
                        ->where('periodid', $period->id)
                        ->first();
        
        $regularIncome = $this->collectRegularIncomeComponent($payroll);
        $nonRegularIncome = $this->collectNonRegularIncomeComponent($payroll, $bonusRecord);
        $deduction = $this->collectDeduction($payroll, $bonusRecord);

        return [
            'regularIncome' => $regularIncome['data'],
            'regularIncomeTotal' => $regularIncome['total'],
            'nonRegularIncome' => $nonRegularIncome['data'],
            'nonRegularIncomeTotal' => $nonRegularIncome['total'],
            'basicSalaryTotal' => $regularIncome['basic_salary_total'],
            'allowanceTotal' => $regularIncome['allowance_total'],
            'nonRegularIncome' => $nonRegularIncome['data'],
            'nonRegularIncomeTotal' => $nonRegularIncome['total'],
            'deduction' => $deduction['data']
        ];
    }

    private function collectRegularIncomeComponent($payroll)
    {
        return [
            'data' => [
                [
                    'name'  => 'Gaji Pokok',
                    'type'  => 'basic_salary',
                    'value' => (int) $payroll->basic,
                    'taxable' => true
                ],
                [
                    'name'  => 'Tunjangan Makan Tetap',
                    'type'  => 'allowance',
                    'value' => (int) $payroll->mealtransall,
                    'taxable' => true
                ],
                [
                    'name'  => 'Tunjangan Makan Proyek',
                    'type'  => 'allowance',
                    'value' => (int) $payroll->mealtransproject,
                    'taxable' => true
                ],
                [
                    'name'  => 'Uang Lembur',
                    'type'  => 'allowance',
                    'value' => (int) $payroll->overtimeall,
                    'taxable' => true
                ],
                [
                    'name'  => 'Uang Lembur Proyek',
                    'type'  => 'allowance',
                    'value' => (int) $payroll->otproject,
                    'taxable' => true
                ],
                [
                    'name'  => 'Tunjangan Keahlian',
                    'type'  => 'allowance',
                    'value' => (int) $payroll->postall,
                    'taxable' => true
                ],
                [
                    'name'  => 'Tunjangan Kesehatan',
                    'type'  => 'allowance',
                    'value' => (int) $payroll->tunjangankesehatan,
                    'taxable' => true
                ],
                [
                    'name'  => 'Klaim',
                    'type'  => 'allowance',
                    'value' => (int) $payroll->medicalclaim,
                    'taxable' => true
                ],
                [
                    'name'  => 'TM, TLK, Isnst',
                    'type'  => 'allowance',
                    'value' => (int) $payroll->tunjmalam + (int) $payroll->tunluarkota + (int) $payroll->insentif,
                    'taxable' => true
                ],
                [
                    'name'  => 'Tunjangan Lain',
                    'type'  => 'allowance',
                    'value' => (int) $payroll->otherall,
                    'taxable' => true
                ]
            ],
            'total' => (int) $payroll->basic + (int) $payroll->mealtransall + (int) $payroll->mealtransproject + (int) $payroll->overtimeall + (int) $payroll->otproject + (int) $payroll->postall + (int) $payroll->tunjangankesehatan + (int) $payroll->medicalclaim + (int) $payroll->tunjmalam + (int) $payroll->tunluarkota + (int) $payroll->insentif + (int) $payroll->otherall,
            'basic_salary_total' => (int) $payroll->basic,
            'allowance_total' => (int) $payroll->mealtransall + (int) $payroll->mealtransproject + (int) $payroll->overtimeall + (int) $payroll->otproject + (int) $payroll->postall + (int) $payroll->tunjangankesehatan + (int) $payroll->medicalclaim + (int) $payroll->tunjmalam + (int) $payroll->tunluarkota + (int) $payroll->insentif + (int) $payroll->otherall
        ];
    }

    private function collectNonRegularIncomeComponent($payroll, $bonusRecord)
    {
        return [
            'data' => [
                'name'  => 'Bonus',
                'type'  => 'non_regular_income',
                'value' => (int) $bonusRecord->tbonus + $bonusRecord->adjustbonus,
                'taxable' => true
            ],
            'total' => (int) $bonusRecord->tbonus + $bonusRecord->adjustbonus
        ];
    }

    private function collectDeduction($payroll, $bonusRecord)
    {
        if ($payroll->employee->statusistimewa || $payroll->employee->statusistimewa === 1) {
            $potongan = $payroll->medicalclaim + $payroll->tunjangankesehatan;
        } else {
            $potongan = $payroll->potmealtransproject + $payroll->tunjmalam + $payroll->tunluarkota + $payroll->insentif + $payroll->medicalclaim + $payroll->tunjangankesehatan;
        }

        return [
            'data' => [
                [
                    'name' => 'Gaji Dibayar di Muka',
                    'value' => (int) $potongan,
                    'taxable' => false,
                    'scope' => 'SALARY'
                ],
                [
                    'name' => 'Pinjaman Karyawan',
                    'value' => (int) $payroll->employeeloan,
                    'taxable' => false,
                    'scope' => 'LOAN'
                ],
                [
                    'name' => 'Pinjaman Insurance',
                    'value' => (int) $payroll->insuranceloan,
                    'taxable' => false,
                    'scope' => 'LOAN'
                ],
                [
                    'name' => 'Potongan Lainnya',
                    'value' => (int) $payroll->otherded,
                    'taxable' => false,
                    'scope' => 'OTHER_DEDUCTION'
                ],
                [
                    'name' => 'Potongan Bonus',
                    'value' => (int) $bonusRecord->potpinjaman,
                    'taxable' => false,
                    'scope' => 'INSENTIVE_DEDUCTION'
                ]
            ]
        ];
    }

    private function collectPtkpAmount($employee, $period)
    {
        $payroll = Payroll::where('employeeid', $employee->id)
                        ->where('periodid', $period->id)
                        ->first(); 

        $ptkp = MasterPtkp::where('name', $payroll->taxstatus)->first();
        return ($ptkp) ? (int) $ptkp->amount : 0; // [CASE] jika tidak punya ptkp status
    }

    private function collectPayrollPeriod($employee, $period)
    {
        $date = Carbon::parse($period->dateperiod);

        $payslip = Payslip::where('employee_id', $employee->id)
                    ->whereYear('date', '=', $date->format('Y'))
                    ->orderBy('id', 'ASC')
                    ->first();

        return [
            'current_period' => ($payslip) ? ($date->format('m') + 1) - Carbon::parse($payslip->date)->format('m') : 1,
            'total_period'   => ($payslip) ? 13 - Carbon::parse($payslip->date)->format('m') : 13 - $date->format('m')
        ];
    }

    private function calculateAssurance($salaryComponents, $defaultValue, $employee)
    {
        $jht_e = $jht_c = $jkk_e = $jkk_c = $jkm_e = $jkm_c = $jp_e  = $jp_c  = 0;
        $setting = $defaultValue;
        $bpjsRate = $setting['bpjs_rate'];

        $umr = Umr::first();
        $umrAmount = ($umr) ? (int) $umr->umr : 0;

        $component = $salaryComponents['basicSalaryTotal'];
        if ($umrAmount > 0 && $umrAmount > $salaryComponents['basicSalaryTotal']) {
            $component = $umrAmount;
        }

        if ($employee->jamsostekmember) {
            $jht_e = $component * $bpjsRate['jht']['employee'];
            $jht_c = $component * $bpjsRate['jht']['company'];
            
            $jkkLevel = 5; // standard
            $jkk_e = $component * $bpjsRate["jkk-$jkkLevel"]['employee'];
            $jkk_c = $component * $bpjsRate["jkk-$jkkLevel"]['company'];

            $jkm_e = $component * $bpjsRate['jkm']['employee'];
            $jkm_c = $component * $bpjsRate['jkm']['company'];
        }

        if ($employee->statuspotpensiun) {
            $jpProrate = $bpjsRate['jp']['employee'] + $bpjsRate['jp']['company'];
            $jpMax = $bpjsRate['jp']['max'] * $jpProrate;
            $jp_e = min([$component * $bpjsRate['jp']['employee'], $jpMax * ($bpjsRate['jp']['employee'] / $jpProrate)]);
            $jp_c = min([$component * $bpjsRate['jp']['company'], $jpMax * ($bpjsRate['jp']['company'] / $jpProrate)]);
        }

        $return = $this->arrayOfBpjs($jht_e, $jht_c, $jkk_e, $jkk_c, $jkm_e, $jkm_c, $jp_e, $jp_c);
        
        return $return;
    }

    private function arrayOfBpjs($jht_e, $jht_c, $jkk_e, $jkk_c, $jkm_e, $jkm_c, $jp_e, $jp_c)
    {
        return [
            "jht" => ["employee" => $jht_e, "company" => $jht_c, "total" => $jht_e + $jht_c],
            "jkk" => ["employee" => $jkk_e, "company" => $jkk_c, "total" => $jkk_e + $jkk_c],
            "jkm" => ["employee" => $jkm_e, "company" => $jkm_c, "total" => $jkm_e + $jkm_c],
            "jp"  => ["employee" => $jp_e, "company" => $jp_c, "total" => $jp_e + $jp_c]
        ];
    }

    private function calculateTaxAndParse($paidPayrolls, $salaryComponents, $ptkp, $date, $npwp, $assurance, $defaultValue)
    {
        $data = $this->calculateTax($paidPayrolls, $salaryComponents, $ptkp, $date, $npwp, $assurance, $defaultValue);

        $tax = [
            'basicSalary' => $salaryComponents['basicSalaryTotal'],
            'allowance'   => $salaryComponents['allowanceTotal'],
            'taxAllowanceRegular'   => $data['brutoIncome']['taxAllowanceRegular'],
            'taxAllowanceIrregular' => $data['brutoIncome']['taxAllowanceIrregular'],
            'taxAllowanceEarn' => $data['brutoIncome']['taxAllowanceEarn'],
            'irregularIncome'  => $salaryComponents['nonRegularIncomeTotal'],
            'brutoIncomeRegular' => $data['brutoIncome']['regularIncome'] + $data['brutoIncome']['taxAllowanceEarn'] + $data['brutoIncome']['taxAllowanceRegular'] + $data['brutoIncome']['irregularIncome'] + $data['brutoIncome']['taxAllowanceIrregular'],
            'taxAllowanceDeduct' => $data['deduct']['taxAllowanceDeduct'],
            'yearlyTotalBrutoIncome' => $data['brutoIncome']['brutoYearlyTotalIncome'],
            'yearlyOccupationFee'    => $data['biayaJabatan']['bjTotal'],
            'yearlyTaxAllowanceDeduct' => $data['deduct']['yearlyTaxAllowanceDeduct'],
            'totalDeduct' => $data['deduct']['totalDeduct'],
            'yearlyTotalNettoIncome' => $data['nettoIncome']['nettoYearlyTotalIncome'],
            'lastTotalNettoIncome'   => 0,
            'finalYearlyTotalNettoIncome' => $data['nettoIncome']['nettoYearlyTotalIncome'] + 0,
            'ptkp' => $data['ptkp'],
            'nettoIncomeAfterPtkp' => (($data['nettoIncome']['nettoYearlyTotalIncome'] + 0) > $data['ptkp']) ? ($data['nettoIncome']['nettoYearlyTotalIncome'] + 0) - $data['ptkp'] : 0,
            'yearlyTax'    => $data['ptTotalSum'],
            'regularTax'   => $data['tax']['taxRegular'],
            'irregularTax' => $data['tax']['taxIrregular'],
            'takeHomePayFirst'  => $data['takeHomePayFirst'],
            'absenceDeduction'  => $data['absenceDeduction'],
            'preSalary'  => $data['preSalary'],
            'takeHomePaySecond' => $data['takeHomePaySecond'],
            'loan'  => $data['loan'],
            'takeHomePayThird'  => $data['takeHomePayThird'],
            'otherDeduction'  => $data['otherDeduction'],
            'insentiveDeduction'  => $data['insentiveDeduction'],
            'takeHomePayFourth' => $data['takeHomePayFourth'],
            'healthAssurance' => $data['healthAssurance']
        ];

        return $tax;
    }
    private function calculateTax($paidPayrolls, $salaryComponents, $ptkp, $date, $npwp, $assurance, $defaultValue)
    {
        /* ============================== SETTING ============================== */
        $currentPeriod = $date['current_period'];
        $totalPeriod   = $date['total_period'];

        $bracket = $defaultValue['bracket_rate'];
        for ($i=0; $i<4; $i++) {
            $temp = [
                $bracket[$i+1]['prorate'],
                $bracket[$i+1]['min'],
                $bracket[$i+1]['max'],
                $bracket[$i+1]['max'] - $bracket[$i+1]['min']
            ];

            if ($i === 0) $tr1 = $temp;
            else if ($i === 1) $tr2 = $temp;
            else if ($i === 2) $tr3 = $temp; 
            else if ($i === 3) $tr4 = $temp;
        }

        $asuransi = $assurance;
        $jht_e = $asuransi["jht"]["employee"];
        $jht_c = $asuransi["jht"]["company"];
        $jkk_e = $asuransi["jkk"]["employee"];
        $jkk_c = $asuransi["jkk"]["company"];
        $jkm_e = $asuransi["jkm"]["employee"];
        $jkm_c = $asuransi["jkm"]["company"];
        $jp_e = $asuransi["jp"]["employee"];
        $jp_c = $asuransi["jp"]["company"];
        /* ============================== SETTING ============================== */

        /* ============================== CALCULATION ============================== */
        $paidRegularIncome   = (int) $paidPayrolls['brutoRegularIncome'];
        $paidIrregularIncome = (int) $paidPayrolls['brutoNonRegularIncome'];
        $paidTaxAllowanceDeduct = (int) $paidPayrolls['taxAllowanceDeduct'];
        $paidRegularTax   = (int) $paidPayrolls['regularTax'];
        $paidIrregularTax = (int) $paidPayrolls['nonRegularTax'];
        $paidYearlyNettoIncome = (int) $paidPayrolls['yearlyNettoIncome'];
        $paidTax = (int) $paidPayrolls['lastPaidTax'];
        
        $regularIncome   = $salaryComponents['regularIncomeTotal'];
        $irregularIncome = $salaryComponents['nonRegularIncomeTotal'];
        $taxAllowanceRegular   = 0;
        $taxAllowanceIrregular = 0;

        $regularIncome += $taxAllowanceRegular;
        $thp = $regularIncome;
        $regularIncome += $paidRegularIncome;
        
        $irregularIncome += $taxAllowanceIrregular;
        $thp += $irregularIncome;
        $irregularIncome += $paidIrregularIncome;

        $taxAllowanceEarn   = ($jkk_c + $jkm_c);
        $taxAllowanceDeduct = ($jkk_e + $jkm_e) + ($jht_e + $jp_e);
        
        $brutoYearlyRegularIncome = (($regularIncome + $taxAllowanceEarn) * $totalPeriod) / $currentPeriod;
        $brutoYearlyTotalIncome   = ((($regularIncome + $taxAllowanceEarn) * $totalPeriod) / $currentPeriod) + $irregularIncome;

        $bjRegular = min([$totalPeriod * 500000, (5 / 100) * $brutoYearlyRegularIncome]);
        $bjTotal   = min([$totalPeriod * 500000, (5 / 100) * $brutoYearlyTotalIncome]);
        $bjIrregular = $bjTotal - $bjRegular;
        if (((5 / 100) * ($irregularIncome - $taxAllowanceIrregular)) < 0) {
            $bjIrregular = (5 / 100) * ($irregularIncome - $taxAllowanceIrregular);
            $bjTotal = $bjRegular + $bjIrregular;
        } 
        
        $yearlyTaxAllowanceDeduct = round((($taxAllowanceDeduct + $paidTaxAllowanceDeduct) * $totalPeriod) / $currentPeriod);
        
        $regularDeduct = $bjRegular + $yearlyTaxAllowanceDeduct;
        $totalDeduct   = $bjTotal + $yearlyTaxAllowanceDeduct;

        $nettoYearlyRegularIncome = $brutoYearlyRegularIncome - $regularDeduct + $paidYearlyNettoIncome;
        $nettoYearlyTotalIncome   = $brutoYearlyTotalIncome - $totalDeduct + $paidYearlyNettoIncome;
        
        if ($ptkp === 0) {
            $pkpRegular = $pkpTotal = 0;
        } else {
            $pkpRegular = round($nettoYearlyRegularIncome - $ptkp);
            $pkpTotal   = round($nettoYearlyTotalIncome - $ptkp);
        }
        
        $pkpRegular -= $pkpRegular % 1000;
        $pkpTotal   -= $pkpTotal % 1000;

        $tarifRegular = 0;
        if ($pkpRegular < $tr1[2]) {
            $tarifRegular = 1;
        } elseif ($pkpRegular < $tr2[2]) {
            $tarifRegular = 2;
        } elseif ($pkpRegular < $tr3[2]) {
            $tarifRegular = 3;
        } elseif ($pkpRegular < $tr4[2]) {
            $tarifRegular = 4;
        }

        $tarifTotal = 0;
        if ($pkpTotal < $tr1[2]) {
            $tarifTotal = 1;
        } elseif ($pkpTotal < $tr2[2]) {
            $tarifTotal = 2;
        } elseif ($pkpTotal < $tr3[2]) {
            $tarifTotal = 3;
        } elseif ($pkpTotal < $tr4[2]) {
            $tarifTotal = 4;
        }

        $ptRegular1 = 0;
        $ptRegular2 = 0;
        $ptRegular3 = 0;
        $ptRegular4 = 0;
        if ($tarifRegular >= 1) {
            $taxTr1 = ($pkpRegular - $tr1[1]) * $tr1[0];
            $taxTr1 = min([$taxTr1, $tr1[3] * $tr1[0]]);
            $taxTr1 = max([0, $taxTr1]);
            $ptRegular1 = $taxTr1;
        }
        if ($tarifRegular >= 2) {
            $taxTr2 = ($pkpRegular - $tr2[1]) * $tr2[0];
            $taxTr2 = min([$taxTr2, $tr2[3] * $tr2[0]]);
            $taxTr2 = max([0, $taxTr2]);
            $ptRegular2 = $taxTr2;
        }
        if ($tarifRegular >= 3) {
            $taxTr3 = ($pkpRegular - $tr3[1]) * $tr3[0];
            $taxTr3 = min([$taxTr3, $tr3[3] * $tr3[0]]);
            $taxTr3 = max([0, $taxTr3]);
            $ptRegular3 = $taxTr3;
        }
        if ($tarifRegular >= 4) {
            $taxTr4 = ($pkpRegular - $tr4[1]) * $tr4[0];
            $taxTr4 = min([$taxTr4, $tr4[3] * $tr4[0]]);
            $taxTr4 = max([0, $taxTr4]);
            $ptRegular4 = $taxTr4;
        }
        $ptRegularSum = round($ptRegular1 + $ptRegular2 + $ptRegular3 + $ptRegular4);

        $ptTotal1 = 0;
        $ptTotal2 = 0;
        $ptTotal3 = 0;
        $ptTotal4 = 0;
        if ($tarifTotal >= 1) {
            $taxTr1 = ($pkpTotal - $tr1[1]) * $tr1[0];
            $taxTr1 = min([$taxTr1, $tr1[3] * $tr1[0]]);
            $taxTr1 = max([0, $taxTr1]);
            $ptTotal1 = round($taxTr1);
        }

        if ($tarifTotal >= 2) {
            $taxTr2 = ($pkpTotal - $tr2[1]) * $tr2[0];
            $taxTr2 = min([$taxTr2, $tr2[3] * $tr2[0]]);
            $taxTr2 = max([0, $taxTr2]);
            $ptTotal2 = round($taxTr2);
        }

        if ($tarifTotal >= 3) {
            $taxTr3 = ($pkpTotal - $tr3[1]) * $tr3[0];
            $taxTr3 = min([$taxTr3, $tr3[3] * $tr3[0]]);
            $taxTr3 = max([0, $taxTr3]);
            $ptTotal3 = round($taxTr3);
        }

        if ($tarifTotal >= 4) {
            $taxTr4 = ($pkpTotal - $tr4[1]) * $tr4[0];
            $taxTr4 = min([$taxTr4, $tr4[3] * $tr4[0]]);
            $taxTr4 = max([0, $taxTr4]);
            $ptTotal4 = round($taxTr4);
        }
        $ptTotalSum = round($ptTotal1 + $ptTotal2 + $ptTotal3 + $ptTotal4);
        
        if (!$npwp) {
            $ptTotalSum   = $ptTotalSum * 1.2;
            $ptRegularSum = $ptRegularSum * 1.2;
        }
    
        $taxRegular   = round(((($ptRegularSum - $paidTax) / $totalPeriod) * $currentPeriod) - $paidRegularTax);
        $taxIrregular = ($ptTotalSum - $ptRegularSum) - $paidIrregularTax;
        
        $taxPenalty = 0;
        if (!$npwp) {
            $taxPenalty = ($taxRegular + $taxIrregular) - (($taxRegular + $taxIrregular) / 1.2);
        }
        
        // hardcode
        $preSalary = (int) $salaryComponents['deduction'][0]['value'];
        $loan = (int) $salaryComponents['deduction'][1]['value'] + (int) $salaryComponents['deduction'][2]['value'];
        $otherDeduction = (int) $salaryComponents['deduction'][3]['value'];
        $insentiveDeduction = (int) $salaryComponents['deduction'][4]['value'];

        $taxTotal = $taxRegular + $taxIrregular;
        $takeHomePayFirst = $thp - $taxAllowanceDeduct - $taxTotal; 
        $takeHomePaySecond = $takeHomePayFirst - $preSalary; 
        $takeHomePayThird = $takeHomePaySecond - $loan;
        $takeHomePayFourth = $takeHomePayThird - $otherDeduction;

        $result = [
            'initial' => [
                'currentPeriod' => $currentPeriod,
                'totalPeriod'   => $totalPeriod
            ],
            'paid' => [
                'paidBrutoRegularIncome'   => $paidRegularIncome,
                'paidBrutoIrregularIncome' => $paidIrregularIncome,
                'paidYearlyNettoIncome'  => $paidYearlyNettoIncome,
                'paidTaxAllowanceDeduct' => $paidTaxAllowanceDeduct,
                'paidRegularTax'   => $paidRegularTax + $paidTax,
                'paidIrregularTax' => $paidIrregularTax
            ],
            'brutoIncome' => [
                'regularIncome'    => $regularIncome - $paidRegularIncome - $taxAllowanceRegular,
                'taxAllowanceEarn' => $taxAllowanceEarn,
                'taxAllowanceRegular'   => $taxAllowanceRegular,
                'irregularIncome'  => $irregularIncome - $paidIrregularIncome - $taxAllowanceIrregular,
                'taxAllowanceIrregular' => $taxAllowanceIrregular,
                'brutoYearlyRegularIncome' => $brutoYearlyRegularIncome,
                'brutoYearlyTotalIncome'   => $brutoYearlyTotalIncome
            ],
            'biayaJabatan' => [
                'bjRegular'   => $bjRegular,
                'bjIrregular' => $bjIrregular,
                'bjTotal'     => $bjTotal
            ],
            'deduct' => [
                'taxAllowanceDeduct' => $taxAllowanceDeduct,
                'yearlyTaxAllowanceDeduct' => $yearlyTaxAllowanceDeduct,
                'regularDeduct' => $regularDeduct,
                'totalDeduct'   => $totalDeduct
            ],
            'nettoIncome' => [
                'nettoYearlyRegularIncome' => $nettoYearlyRegularIncome,
                'nettoYearlyTotalIncome'   => $nettoYearlyTotalIncome
            ],
            'ptkp' => $ptkp,
            'ptRegularSum' => $ptRegularSum,
            'ptTotalSum'   => $ptTotalSum,
            'tax'  => [
                'taxRegular'   => $taxRegular, 
                'taxIrregular' => $taxIrregular,
                'taxTotal'     => $taxTotal,
                'taxPenalty'   => $taxPenalty
            ],
            'healthAssurance'  => 0,
            'takeHomePayFirst'  => $takeHomePayFirst,
            'absenceDeduction'  => 0,
            'preSalary'         => $preSalary,
            'takeHomePaySecond' => $takeHomePaySecond,
            'loan'              => $loan,
            'takeHomePayThird'  => $takeHomePayThird,
            'otherDeduction'    => $otherDeduction,
            'insentiveDeduction' => $insentiveDeduction,
            'takeHomePayFourth' => $takeHomePayFourth
        ];

        return $result;
    }

    private function slugify($text) 
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '_', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '_');

        // remove duplicate -
        $text = preg_replace('~-+~', '_', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n_a';
        }

        return $text;
    }

    private function createContentModalPayslip($payslip, $payroll)
    {
        $otherInformations = $this->getPayslipInformation($payslip);
        $title = $otherInformations['title'];
        $employeeName = $otherInformations['employee_name'];
        $grade = $otherInformations['grade'];
        $taxStatus = $otherInformations['tax_status'];
        $jobtitle = $otherInformations['jobtitle'];

        $hutang = 0;
        if ($payroll) $hutang = (int) $payroll->remaininsuranceloan + (int) $payroll->remainemployeeloan;

        $content = "
            <table style='width:550px' class='payslip-table'>
                <tr>
                    <td style='width:70%'>Nama</td>
                    <td style='width:30%'>".$employeeName."</td>
                </tr>
                <tr>
                    <td>Jabatan</td>
                    <td>".$jobtitle."</td>
                </tr>
                <tr>
                    <td>Grade</td>
                    <td>".$grade."</td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>".$taxStatus."</td>
                </tr>
                <tr class='text-bold border-top-bottom text-center'>
                    <td colspan='2'>PENGHASILAN</td>
                </tr>
                <tr>
                    <td>Bonus</td>
                    <td>Rp ".number_format(@($payslip->component_bonus / $payslip->component_value))."</td>
                </tr>
                <tr>
                    <td>Value</td>
                    <td>".$payslip->component_value."</td>
                </tr>
                <tr class='text-bold border-top-bottom text-center'>
                    <td colspan='2'>POTONGAN</td>
                </tr>
                <tr>
                    <td>PPh Pasal 21</td>
                    <td>Rp ".number_format($payslip->component_irregular_tax)."</td>
                </tr>
                <tr>
                    <td>Potongan Pinjaman</td>
                    <td>Rp ".number_format($payslip->component_deduction)."</td>
                </tr>
                <tr class='text-bold border-top-bottom'>
                    <td>Bonus Netto</td>
                    <td>Rp ".number_format($payslip->component_bonus_final)."</td>
                </tr>
                <tr>
                    <td>Sisa Pinjaman Karyawan</td>
                    <td>Rp ".number_format($hutang)."</td>
                </tr>
            </table>
        ";

        return [
            'title' => $title,
            'content' => $content
        ];
    }

    private function getPayslipInformation($payslip)
    {
        $payslipDate = Carbon::parse($payslip->date);
        $monthName = $this->months[$payslipDate->format('m') - 1];
        $title = 'Payslip Bonus ' . $monthName . ' ' . $payslipDate->format('Y');

        $employee = $payslip->employee;
        $employeeName  = $employee->employeename;
        $grade = ($employee->gol) ? $employee->gol : '-';
        $taxStatus = ($employee->taxstatus) ? $employee->taxstatus : '-';
        $jobtitle = ($employee->jobtitle) ? $employee->jobtitle->positionname : '-';

        return [
            'title' => $title,
            'employee_name' => $employeeName,
            'grade' => $grade,
            'tax_status' => $taxStatus,
            'jobtitle' => $jobtitle
        ];
    }

    private function createContentEmailPayslip($payslip, $payroll)
    {
        $otherInformations = $this->getPayslipInformation($payslip);
        $title = $otherInformations['title'];
        $employeeName = $otherInformations['employee_name'];
        $grade = $otherInformations['grade'];
        $taxStatus = $otherInformations['tax_status'];
        $jobtitle = $otherInformations['jobtitle'];

        $hutang = 0;
        if ($payroll) $hutang = (int) $payroll->remaininsuranceloan + (int) $payroll->remainemployeeloan;

        $content = "
            <html lang='en'>
            <head>
                <meta http-equiv='Content-Type' content='text/html' charset='utf-8'>
                <title>Payslip</title>
                <style type='text/css'>
                    .my_class {
                        background-color: white;
                    }
                    table.payslip-table td {
                        padding: 5px;
                    }
                    .text-bold {
                        font-weight: bold;
                    }
                    .text-center {
                        text-align: center;
                    }
                    .border-top-bottom {
                        border-top: 1px solid black; 
                        border-bottom: 1px solid black;
                    }
                    .font-size-8 {
                        font-size: 8px;
                    }
                </style>
            </head>
            <body>

            <table style='width:600px' class='payslip-table font-size-8'>
                <tr>
                    <td colspan='3'>".$title."</td>
                </tr>
                <tr>
                    <td style='width:50%'>Nama</td>
                    <td style='width:1%'>:</td>
                    <td style='width:49%'>".$employeeName."</td>
                </tr>
                <tr>
                    <td>Jabatan</td>
                    <td>:</td>
                    <td>".$jobtitle."</td>
                </tr>
                <tr>
                    <td>Grade</td>
                    <td>:</td>
                    <td>".$grade."</td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>:</td>
                    <td>".$taxStatus."</td>
                </tr>
                <tr class='text-bold border-top-bottom text-center'>
                    <td colspan='3'>PENGHASILAN</td>
                </tr>
                <tr>
                    <td>Bonus</td>
                    <td>:</td>
                    <td>Rp ".number_format(@($payslip->component_bonus / $payslip->component_value))."</td>
                </tr>
                <tr>
                    <td>Value</td>
                    <td>:</td>
                    <td>".$payslip->component_value."</td>
                </tr>
                <tr class='text-bold border-top-bottom'>
                    <td>BONUS BRUTO</td>
                    <td>:</td>
                    <td>Rp ".number_format((int) $payslip->component_bonus)."</td>
                </tr>
                <tr class='text-bold border-top-bottom text-center'>
                    <td colspan='3'>POTONGAN</td>
                </tr>
                <tr>
                    <td>PPh Pasal 21</td>
                    <td>:</td>
                    <td>Rp ".number_format((int) $payslip->component_irregular_tax)."</td>
                </tr>
                <tr>
                    <td>Potongan Pinjaman</td>
                    <td>:</td>
                    <td>Rp ".number_format((int) $payslip->component_deduction)."</td>
                </tr>
                <tr class='text-bold border-top-bottom'>
                    <td>JUMLAH POTONGAN</td>
                    <td>:</td>
                    <td>Rp ".number_format((int) $payslip->component_irregular_tax + $payslip->component_deduction)."</td>
                </tr>
                <tr class='text-bold border-top-bottom'>
                    <td>Bonus Netto</td>
                    <td>:</td>
                    <td>Rp ".number_format((int) $payslip->component_bonus_final)."</td>
                </tr>
                <tr>
                    <td>Sisa Pinjaman Karyawan</td>
                    <td>:</td>
                    <td>Rp ".number_format($hutang)."</td>
                </tr>
                <tr>
                    <td colspan='3'>________________________________________________________________________________________________________________________</td>
                </tr>
                <tr>
                    <td class='text-center'><img src='http://webapp.lmu.co.id/trocon/laravel/storage/payroll-report/mgr_finance.jpg' width='100px' alt='Image'></td>
                    <td style='padding-left:20px'><img src='http://webapp.lmu.co.id/trocon/laravel/storage/payroll-report/finance.jpg' width='100px' alt='Image'></td>
                    <td><img src='http://webapp.lmu.co.id/trocon/laravel/storage/payroll-report/hrd.jpg' width='100px' alt='Image'></td>
                </tr>
                <tr>
                    <td class='text-center'>Mgr Finance</td>
                    <td style='padding-left:20px'>Finance</td>
                    <td style='padding-left:20px'>HRD</td>
                </tr>
            </table>

            </body>
            </html>
        ";

        return [
            'title' => $title,
            'content' => $content
        ];
    }

    private function createPdf($pdf)
    {
        $options = new Options();
        $options->setIsRemoteEnabled(true);
        $domPdf = new Dompdf($options);
        $domPdf->loadHtml($pdf['content']);
        $domPdf->render();

        return $domPdf->output();
    }

    private function savePdf($pdf)
    {
        $folderPath = 'payroll-report/';
        $name = time().str_random(6).".pdf";
        $path = $folderPath.$name;

        Storage::makeDirectory($folderPath);        
        Storage::put($path, $pdf);
        
        $url = \URL::route('payroll.payslip.download', [
            'filename' => base64_encode('app/' . $path)
        ]);
        
        return $url;
    }

    private function sendEmail($payslip, $path)
    {
        $date = Carbon::parse($payslip->date);
        $monthName = $this->months[$date->format('m')-1];
        $year = $date->format('Y');
        $employee = $payslip->employee;
        
        $message = new Client();
        try {
            \Mail::send('email_send_payslip', [], function($message) use ($employee, $monthName, $year, $path) {
                $message->from('trocon.payroll@gmail.com', 'HR Trocon');
                $message->subject('Payslip Bonus ' . $monthName . ' ' . $year);
                $message->to($employee->email);
                $message->attach($path, [
                    'as' => 'payslip_bonus.pdf',
                    'mime' => 'application/pdf',
                ]);
            });
        } catch (\Exception $e) {
            return true;
        }
        
        return true;
    }
}