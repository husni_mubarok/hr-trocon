<?php

namespace App\Http\Controllers\Editor\Payroll;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Payroll\Correction;
use Illuminate\Http\Request;

class CorrectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = $this->collectEmployeeCorrection();

        return view('editor.payroll.correction.index', [
            'employees' => $employees
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        return view('editor.payroll.correction.form', [
            'employee' => $employee
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($id, $request) {
                $employee = Employee::find($id);
                
                Correction::create([
                    'employee_id' => $employee->id,
                    'date' => Carbon::now(),
                    'exist_yearly_netto_income_total' => $request->input('netto_income'),
                    'exist_tax_paid_total' => $request->input('tax')
                ]);
            });

            $alert = 'alert-success';
            $message = 'Berhasil ubah data!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Gagal ubah data!';
        }

        $request->session()->flash($alert, $message);
        return redirect('editor/payroll/correction');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $corrections = $employee->corrections;

        if ($corrections) {
            $correction = $corrections[0];
            $correction->delete();
        }

        return response()->json([
            'success' => true,
            'message' => 'Deleted successfull'
        ]);
    }

    public function destroyAll(Request $request)
    {
        $employeeIds = $request->input('ids');   
        foreach($employeeIds as $id) {
            $post = Correction::where('employee_id', $id)->first();
            if ($post) $post->delete(); 
        }

        return response()->json([
            'status' => TRUE
        ]);
    }

    public function getDataAjax()
    {
        $employees = $this->collectEmployeeCorrection();

        foreach ($employees as $employee) {
            if ($employee->payslip) {
                $employee['action'] = 'Tidak dapat mengubah karena sudah ada perhitungan payroll untuk staff ini.';
            } else {
                if ($employee->correction) {
                    $employee['action'] = '<a href="correction/'.$employee->id.'/edit" title="Edit")"> Edit</a> | <a href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$employee->id."', '".$employee->employeename."'".')"> Hapus</a>';
                } else {
                    $employee['action'] = '<a href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$employee->id."', '".$employee->employeename."'".')"> Hapus</a>';
                }
            }

            $employee['check']  = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'.$employee->id.'"> <div class="control__indicator"></div> </label>';
        }
       
        return response()->json([
            'draw' => 0,
            'recordsTotal' => count($employees),
            'recordsFiltered' => count($employees),
            'data' => $employees,
            'input' => []
        ]);  
    }

    private function collectEmployeeCorrection()
    {
        $employees = Employee::all();

        foreach ($employees as $employee) {
            $corrections = $employee->corrections;
            $payslips = $employee->payslips;

            $employee->exist_yearly_netto_income_total = 0;
            $employee->exist_tax_paid_total = 0;
            $employee->correction = true;
            $employee->payslip = false;
            $employee->date = '-';

            if ($corrections->count() > 0 || $payslips->count() > 0) {
                if ($payslips->count() > 0) {
                    $employee->payslip = true;
                }

                if (isset($corrections[0])) {
                    $employee->exist_yearly_netto_income_total = $corrections[0]->exist_yearly_netto_income_total;
                    $employee->exist_tax_paid_total = $corrections[0]->exist_tax_paid_total;
                    $employee->date = Carbon::parse($corrections[0]->date)->format('Y-m-d');
                }

                $employee->correction = false;
            }
        }
        
        return $employees;   
    }
}
