<?php

namespace App\Http\Controllers\Editor\Payroll;

use App\Http\Controllers\Controller;
use App\Model\Payroll\MasterPtkp;
use Illuminate\Http\Request;

class MasterPtkpController extends Controller
{
    private $masterPtkps = ['TK0', 'TK1', 'TK2',  'TK3', 'K0', 'K1', 'K2', 'K3'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = MasterPtkp::all();

        return view('editor.payroll.master_ptkp.index', [
            'masterPtkps' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $masterPtkps = [];
        foreach ($this->masterPtkps as $index => $ptkp) {
            $record = MasterPtkp::where('name', $ptkp)->first();
            if (!$record) {
                $masterPtkps[$ptkp] = $ptkp;
            }
        }

        return view('editor.payroll.master_ptkp.form', [
            'masterPtkps' => $masterPtkps,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            \DB::transaction(function () use ($request) {
                MasterPtkp::create([
                    'name' => $request->input('name'),
                    'amount' => $request->input('amount')
                ]);
            });

            $alert = 'alert-success';
            $message = 'Berhasil tambah data!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Gagal tambah data!';
        }

        $request->session()->flash($alert, $message);
        return redirect('editor/payroll/setting/master-ptkp');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masterPtkp = MasterPtkp::where('id', $id)->first();
        if (!$masterPtkp) {
            return redirect('editor/payroll/setting/master-ptkp');
        }

        $masterPtkps = [];
        foreach ($this->masterPtkps as $index => $ptkp) {
            $record = MasterPtkp::where('name', $ptkp)->first();
            if (!$record) {
                $masterPtkps[$ptkp] = $ptkp;
            }
        }

        return view('editor.payroll.master_ptkp.form', [
            'masterPtkps' => $masterPtkps,
            'masterPtkp'  => $masterPtkp
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($id, $request) {
                $record = MasterPtkp::where('id', $id)->first();
                
                $record->updated([
                    'name' => $request->input('name'),
                    'amount' => $request->input('amount')
                ]);
            });

            $alert = 'alert-success';
            $message = 'Berhasil ubah data!';
        } catch (\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Gagal ubah data!';
        }

        $request->session()->flash($alert, $message);
        return redirect('editor/payroll/setting/master-ptkp');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ptkp = MasterPtkp::find($id);
        $ptkp->delete(); 

        return response()->json($ptkp); 
    }

    public function destroyAll(Request $request)
    {
        $ids = $request->ids;   
        foreach($ids as $key => $id) {
            $post = MasterPtkp::Find($id[1]);
            if ($post) $post->delete(); 
        }

        return response()->json([
            'status' => TRUE
        ]); 
    }

    public function getDataAjax()
    {
        $result = MasterPtkp::all();
        foreach ($result as $record) {
            $record['action'] = '<a href="master-ptkp/'.$record->id.'/edit" title="Edit")"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$record->id."', '".$record->name."'".')"> Hapus</a>';
            $record['check']  = '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$record->id."'".'"> <div class="control__indicator"></div> </label>';
        }
       
        return response()->json([
            'draw' => 0,
            'recordsTotal' => count($result),
            'recordsFiltered' => count($result),
            'data' => $result,
            'input' => []
        ]);  
    }
}
