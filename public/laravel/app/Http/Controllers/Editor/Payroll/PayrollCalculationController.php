<?php

namespace App\Http\Controllers\Editor\Payroll;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class PayrollCalculationController extends Controller
{
    public function preview()
    {
    	$datafilter = User::where('id', Auth::id())->first();
    	$department_list = Department::all()->pluck('departmentname', 'id');
    	$payperiod_list = Payperiod::orderBy('id', 'desc')->where('status', '0')->pluck('description', 'id');

    	return view ('editor.payroll.calculation_preview.index', compact('department_list','payperiod_list','datafilter'));
    }

    public function getDataAjax()
    {
    	$userid = Auth::id();
      	$sql = 'SELECT
                employee.id AS employeeid,
                employee.employeename AS employee,
                position.positionname AS position,
                ifnull(payroll.basic, 0) AS basic,
                ifnull(payroll.totalmealtrans, 0) as mealtransall,
                ifnull(payroll.mealtransproject, 0) AS uang_makan,
                ifnull(payroll.totalovertime, 0) as overtimeall,
                ifnull(payroll.otproject, 0) AS lembur_proyek,
                ifnull(payroll.postall, 0) AS tunj_keahlian,
                ifnull(payroll.medicalclaim, 0) AS tunj_kesehatan,
                ifnull(payroll.tunjmalam, 0) + ifnull(payroll.tunluarkota, 0) + ifnull(payroll.insentif, 0) AS tm_tdk_tlk_instpd,
                department.departmentname
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN user ON payroll.periodid = user.periodid
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              LEFT OUTER JOIN position ON employee.positionid = position.id
              WHERE
                (user.id = '.$userid.')
              AND (employee.status = 0)
              AND payroll.departmentid = ifnull(
                user.departmentid,
                payroll.departmentid
              )
              ORDER BY
                employee.id desc';
      	$itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('employeeid', 'desc')->get(); 

      	return Datatables::of($itemdata)->make(true);
    }
}
