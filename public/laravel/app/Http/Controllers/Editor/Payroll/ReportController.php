<?php

namespace App\Http\Controllers\Editor\Payroll;

use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Model\Payperiod;
use App\Model\Payroll;
use App\Model\Department;
use App\Model\Employee;
use App\Model\Sex;
use App\Model\Payroll\Payslip;
use App\Model\Payroll\PayslipItem;
use App\Model\Payroll\SettingBracket;
use App\Model\Payroll\Correction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use setasign\Fpdi;

class ReportController extends Controller
{
    protected $payrollBracket = [];

	/**
     * PayrollReportService constructor.
     *
     */
    public function __construct() {
        $this->payrollBracket  = $this->getDefaultPayrollBracket();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('editor.payroll.report.index');
    }

    public function data()
    {
        $payslips = Payslip::groupBy('payroll_period_id')->get();
        $departments = Department::all();
        
        $action = null;
        foreach ($payslips as $payslip) {
            $payslip['date_format'] = Carbon::parse($payslip->date)->format('F Y');

            foreach ($departments as $department) {
                $action .= 
                '<a  href="javascript:void(0)" title="PPh21 Bulanan PDF" onclick="download_file('."'pph21-bulanan-pdf'".', '."'".$payslip->payroll_period_id."'".', '."'".$department->id."'".')"> PPh21 Bulanan PDF '.$department->departmentname.'</a> ||
                <a  href="javascript:void(0)" title="PPh21 Bulanan CSV" onclick="download_file('."'pph21-bulanan-csv'".', '."'".$payslip->payroll_period_id."'".', '."'".$department->id."'".')"> PPh21 Bulanan CSV '.$department->departmentname.'</a> ||
                <a  href="javascript:void(0)" title="BPJS Ketenagakerjaan" onclick="download_file('."'employment-insurance'".', '."'".$payslip->payroll_period_id."'".', '."'".$department->id."'".')"> BPJS Ketenaga Kerjaan '.$department->departmentname.'</a> || 
                <a  href="javascript:void(0)" title="BPJS Ketenagakerjaan Template Upah" onclick="download_file('."'employment-insurance-template'".', '."'".$payslip->payroll_period_id."'".', '."'".$department->id."'".')"> BPJS Ketenaga Kerjaan Template Upah '.$department->departmentname.'</a> || 
                <a  href="javascript:void(0)" title="Format PPh21" onclick="download_file('."'format-pph21'".', '."'".$payslip->payroll_period_id."'".', '."'".$department->id."'".')"> Format PPh21 '.$department->departmentname.'</a> || 
                <a  href="javascript:void(0)" title="Form A1" onclick="download_file('."'form-a1'".', '."'".$payslip->payroll_period_id."'".', '."'".$department->id."'".')"> Form A1 '.$department->departmentname.'</a><br>';
            }
            $payslip['action'] = $action;
            $action = null;
        }
       
        return response()->json([
            'draw' => 0,
            'recordsTotal' => count($payslips),
            'recordsFiltered' => count($payslips),
            'data' => $payslips,
            'input' => []
        ]);
    }

    public function downloadPph21Pdf(Request $request)
    {
        $periodId = $request->input('period_id');
        $departmentId = $request->input('department_id');

        $payslips = Payslip::select('payroll_payslips.*')
                        ->join('employee', 'employee.id', '=', 'payroll_payslips.employee_id')
                        ->where('payroll_payslips.payroll_period_id', $periodId)
                        ->where('employee.departmentid', $departmentId)
                        ->get();
        $datas = $this->collectDataForPph21Pdf($payslips);
        $pdf = $this->createPph21Pdf($datas);
        $url = $this->createPdfFile($pdf);

        return response()->json([
            'url' => $url
        ]);
    }

    public function downloadPph21Csv(Request $request)
    {
        $periodId = $request->input('period_id');
        $departmentId = $request->input('department_id');

        $payslips = Payslip::select('payroll_payslips.*')
                        ->join('employee', 'employee.id', '=', 'payroll_payslips.employee_id')
                        ->where('payroll_payslips.payroll_period_id', $periodId)
                        ->where('employee.departmentid', $departmentId)
                        ->get();
        
        $date = $this->collectTaxPeriod($payslips[0])['full'];
        $brutoIncome = $tax = 0;
        $data = 'Masa Pajak;Tahun Pajak;Pembetulan;NPWP;Nama;Kode Pajak;Jumlah Bruto;Jumlah PPh;Kode Negara' . "\n";
        foreach ($payslips as $payslip) {
            $brutoIncome = $this->collectBrutoIncomeAmount($payslip);
            $tax = $this->collectTaxAmount($payslip);
            $employee = $payslip->employee;
            $npwp = ($employee->npwp) ? $employee->npwp : '000000000000000';

            $data .= Carbon::parse($date)->format('n').';'.Carbon::parse($date)->format('Y').';0;'.$npwp.';'.$employee->employeename.';21-100-01;'.$brutoIncome.';'.$tax.';'."\n";
        }

        $fileName = '1721_bulanan.csv';
        $folderPath = 'payroll-report/';

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        Storage::makeDirectory($folderPath);
        Storage::put($folderPath .'/'. $fileName, $data);

        $url = \URL::route('payroll.payslip.download', [
            'filename' => base64_encode('app/' . $folderPath . $fileName),
        ]);

        return response()->json([
            'url' => $url
        ]);
    }

    public function downloadEmploymentInsurance(Request $request)
    {
        $periodId = $request->input('period_id');
        $departmentId = $request->input('department_id');

        $payslips = Payslip::select('payroll_payslips.*')
                        ->join('employee', 'employee.id', '=', 'payroll_payslips.employee_id')
                        ->where('payroll_payslips.payroll_period_id', $periodId)
                        ->where('employee.departmentid', $departmentId)
                        ->get();
        
        $date = $this->collectTaxPeriod($payslips[0])['full'];
        $records = [];
        $header = ['No', 'NIK', 'KPJ', 'Nama', 'Kawin', 'Tanggal Lahir', 'Periode Kepesertaan', 'Upah', 'Rapel', 'JHT', 'JKK', 'JKM', 'JP', 'Iuran'];
        $filename = 'BPJS-Ketenagakerjaan-' . date('Y-m-d');

        Excel::create($filename, function ($excel) use ($header, $payslips) {
            $excel->sheet('Sheet1', function ($sheet) use ($header, $payslips) {
                $sheet->row(1, $header);
                foreach ($payslips as $i => $payslip) {
                    $payslipItems = $payslip->payslipItems;
                    $employee = $payslip->employee;
                    $ptkpStatus = ($employee->taxstatus) ? $employee->taxstatus : 'TK0';

                    $jht = $jkk = $jkm = $jp = $brutoIncome = 0;
                    foreach ($payslipItems as $payslipItem) {
                        if ($payslipItem->type === PayslipItem::TYPE_EARN && 
                            $payslipItem->category !== PayslipItem::IS_ALLOWANCE_NON_REGULAR
                        ) {
                            $brutoIncome += (int) $payslipItem->amount;
                        }

                        if ($payslipItem->category == PayslipItem::IS_ASSURANCE || $payslipItem->category == PayslipItem::IS_ASSURANCE_ALLOWANCE) {
                            if ($payslipItem->assurance_type == PayslipItem::JHT) {
                                $jht += $payslipItem->amount;
                            } else if ($payslipItem->assurance_type == PayslipItem::JKK) {
                                $jkk += $payslipItem->amount;
                            } else if ($payslipItem->assurance_type == PayslipItem::JKM) {
                                $jkm += $payslipItem->amount;
                            } else if ($payslipItem->assurance_type == PayslipItem::JP) {
                                $jp += $payslipItem->amount;
                            }
                        }
                    }

                    $sheet->prependRow(2 + $i,
                        [
                            $i + 1,
                            (string) $employee->nik,
                            '-',
                            $employee->employeename,
                            '-',
                            Carbon::parse($employee->datebirth)->format('d-m-Y'),
                            '',
                            $brutoIncome,
                            '',
                            $jht,
                            $jkk,
                            $jkm,
                            $jp,
                            $jht + $jkk + $jkm + $jp
                        ]
                    );

                    $i++;
                }
            });
        })->store('xls', storage_path('app/payroll-report/'));
        $filename = $filename . '.xls';
        $folderPath = 'payroll-report/';

        $url = \URL::route('payroll.payslip.download', [
            'filename' => base64_encode('app/' . $folderPath . $filename)
        ]);

        return response()->json([
            'url' => $url
        ]);
    }

    public function downloadEmploymentInsuranceTemplate(Request $request)
    {
        $periodId = $request->input('period_id');
        $departmentId = $request->input('department_id');

        $payslips = Payslip::select('payroll_payslips.*')
                        ->join('employee', 'employee.id', '=', 'payroll_payslips.employee_id')
                        ->where('payroll_payslips.payroll_period_id', $periodId)
                        ->where('employee.departmentid', $departmentId)
                        ->get();
        
        $date = Carbon::parse($this->collectTaxPeriod($payslips[0])['full']);
        $day   = $date->format('d');
        $month = $date->format('m');
        $year  = $date->format('Y');
        $date  = $date->format('Y-m-d');

        $header = ['ID_PEGAWAI', 'KPJ', 'NAMA_LENGKAP', 'TANGGAL_LAHIR dd-mm-yyyy', 'NPP', 'BLTH* ddmmyyyy', 'UPAH', 'RAPEL'];
        $filename = 'template_upah_000_' . $month . $year;

        Excel::create($filename, function ($excel) use ($header, $payslips, $month, $year, $day) {
            $excel->sheet('Sheet1', function ($sheet) use ($header, $payslips, $month, $year, $day) {
                $sheet->row(1, $header);

                $i = 0;
                foreach ($payslips as $payslip) {
                    $employee = $payslip->employee;
                    $bpjsTkNumber = '00000000000';

                    $brutoIncome = 0;
                    $payslipItems = $payslip->payslipItems;
                    foreach ($payslipItems as $payslipItem) {
                        if ($payslipItem->type === PayslipItem::TYPE_EARN && 
                            $payslipItem->category !== PayslipItem::IS_ALLOWANCE_NON_REGULAR
                        ) {
                            $brutoIncome += (int) $payslipItem->amount;
                        }
                    }

                    $sheet->prependRow(2 + $i,
                        [
                            (string) $employee->nik,
                            (string) $bpjsTkNumber,
                            $employee->employeename,
                            Carbon::parse($employee->datebirth)->format('d-m-Y'),
                            '00000000000',
                            $day . $month . $year,
                            $brutoIncome,
                            0
                        ]
                    );

                    $i++;
                }
            });
        })->store('xls', storage_path('app/payroll-report/'));
        $filename = $filename . '.xls';
        $folderPath = 'payroll-report/';

        $url = \URL::route('payroll.payslip.download', [
            'filename' => base64_encode('app/' . $folderPath . $filename)
        ]);

        return response()->json([
            'url' => $url
        ]);
    }

    public function downloadFormatPph21(Request $request)
    {
        $periodId = $request->input('period_id');
        $departmentId = $request->input('department_id');

        $payslips = Payslip::select('payroll_payslips.*')
                        ->join('employee', 'employee.id', '=', 'payroll_payslips.employee_id')
                        ->where('payroll_payslips.payroll_period_id', $periodId)
                        ->where('employee.departmentid', $departmentId)
                        ->get();

        $fileExcelFolder = storage_path('payroll_report');
        $filename = 'format-program-pph21';
        
        Excel::load('storage/payroll-report/' . $filename . '.xlsx', function ($excel) use ($payslips) {
            $firstRow = 4;
            $excel->sheet('Sheet1', function ($sheet) use ($firstRow, $payslips) {
                foreach ($payslips as $index => $payslip) {
                    $employee = $payslip->employee;
                    $payslipItems =$payslip->payslipItems;

                    $component = [
                        'basic_salary' => 0,
                        'regular_allowance' => 0,
                        'assurance_allowance' => 0,
                        'irregular_income' => 0,
                        'occupation_fee' => min([500000, (5 / 100) * $payslip->component_total_bruto_income]),
                        'assurance_allowance_deduct' => 0
                    ];
                    foreach ($payslipItems as $payslipItem) {
                        if ($payslipItem->category === PayslipItem::IS_BASIC_SALARY) $component['basic_salary'] += $payslipItem->amount;

                        if ($payslipItem->category === PayslipItem::IS_ALLOWANCE_REGULAR) $component['regular_allowance'] += $payslipItem->amount;

                        if ($payslipItem->category === PayslipItem::IS_ASSURANCE_ALLOWANCE && in_array($payslipItem->assurance_type, [PayslipItem::JKK, PayslipItem::JKM])) 
                            $component['assurance_allowance'] += $payslipItem->amount;

                        if ($payslipItem->category === PayslipItem::IS_ASSURANCE) $component['assurance_allowance_deduct'] += $payslipItem->amount;
                    }

                    $sheet->prependRow($firstRow,
                        [
                            '',
                            $index + 1,
                            $employee->employeename,
                            ($employee->jobtitle) ? $employee->jobtitle->positionname : '-',
                            $payslip->employee->npwp,
                            (int) $component['basic_salary'],
                            (int) $component['regular_allowance'],
                            (int) $component['assurance_allowance'],
                            0,
                            (int) $payslip->component_total_bruto_income,
                            (int) $component['occupation_fee'],
                            (int) $component['assurance_allowance_deduct'],
                            (int) $payslip->component_total_bruto_income - ((int) $component['occupation_fee'] + (int) $component['assurance_allowance_deduct']),
                            (int) $payslip->component_yearly_total_netto_income,
                            (int) $payslip->component_ptkp,
                            (int) $payslip->component_netto_income_after_ptkp,
                            (int) $payslip->component_regular_tax + (int) $payslip->component_irregular_tax,
                            (int) $payslip->component_regular_tax + (int) $payslip->component_irregular_tax,
                            ($payslip->component_regular_tax + $payslip->component_irregular_tax < 0) ? (int) $payslip->component_regular_tax + (int) $payslip->component_irregular_tax : 0
                        ]
                    );

                    $firstRow++;
                }
            });
        })->store('xls', storage_path('app/payroll-report/'));
        $filename = $filename . '.xls';
        $folderPath = 'payroll-report/';

        $url = \URL::route('payroll.payslip.download', [
            'filename' => base64_encode('app/' . $folderPath . $filename)
        ]);

        return response()->json([
            'url' => $url
        ]);
    }

    public function downloadFormA1(Request $request)
    {
        $departmentId = $request->input('department_id');
        $employees = Employee::where('departmentid', $departmentId)->get();
        
        $periodId = $request->input('period_id');
        $period = Payperiod::find($periodId);
        $selectedDate = Carbon::parse($period->dateperiod);

        $report = $this->createFormA1Pdf($employees, $selectedDate);
        $url = $this->createPdfFile($report);

        return response()->json([
            'url' => $url
        ]);
    }

    // ------------------------------------------ PRIVATE ------------------------------------------ //
    private function collectDataForPph21Pdf($payslips)
    {
        $result['tax_period'] = $this->collectTaxPeriod($payslips[0]);
        $result['employees'] = $this->collectEmployees($payslips);

        return $result;
    }

    private function collectTaxPeriod($payslip) 
    {
        $date = Carbon::parse($payslip->date);
        return [
            'full'  => $date->format('Y-m-d'),
            'month' => $date->format('m'),
            'year'  => $date->format('Y')
        ];
    }

    private function collectEmployees($payslips)
    {
        $result = $array = [];
        $result['list'] = [];
        $brutoIncomeAmount = $taxAmount = 0;
        $brutoIncomeAmountTotalWithoutTax = $brutoIncomeAmountTotalWithTax = $taxAmountTotal = 0;
        $countOfResult = $countOfArray = $countOfPayslipWithoutTax = 0;

        foreach ($payslips as $payslip) {
            if ($payslip->component_regular_tax + $payslip->component_irregular_tax === 0) {
                $countOfPayslipWithoutTax++;
                $brutoIncomeAmountTotalWithoutTax += $this->collectBrutoIncomeAmount($payslip);
                continue;
            } else {
                $brutoIncomeAmount = $this->collectBrutoIncomeAmount($payslip);
                $taxAmount = $this->collectTaxAmount($payslip);

                $brutoIncomeAmountTotalWithTax += $brutoIncomeAmount;
                $taxAmountTotal += $taxAmount;

                $array[] = [
                    'index'        => $countOfArray + 1,
                    'tax_number'   => $payslip->employee->npwp,
                    'name'         => $payslip->employee->employeename,
                    'tax_code'     => '21-100-01',
                    'bruto_income_amount' => $this->formatCurrency($brutoIncomeAmount),
                    'tax_amount'   => $this->formatCurrency($taxAmount)
                ];
                
                $result['list'][$countOfResult] = $array;
                $countOfArray++;
            }

            if ($countOfArray % 20 === 0) {
                $countOfResult++;
                $array = [];
            }
        }

        $result['count_of_payslip_without_tax'] = $this->formatCurrency($countOfPayslipWithoutTax);
        $result['total_bruto_income_amount_of_payslip_without_tax'] = $this->formatCurrency($brutoIncomeAmountTotalWithoutTax);
        $result['total_bruto_income_amount_of_payslip_with_tax'] = $this->formatCurrency($brutoIncomeAmountTotalWithTax);
        $result['total_tax_amount'] = $this->formatCurrency($taxAmountTotal); 
        $result['total_all_bruto_income'] = $this->formatCurrency($brutoIncomeAmountTotalWithoutTax + $brutoIncomeAmountTotalWithTax); 

        return $result;
    }

    private function collectBrutoIncomeAmount($payslip)
    {
        $payslipItems = $payslip->payslipItems;
        $brutoIncome = 0;

        foreach ($payslipItems as $payslipItem) {
            if ($this->isBrutoIncome($payslipItem) && $payslipItem->taxable) {
                $brutoIncome += (int) $payslipItem->amount;
            }

            if ($payslipItem->category == PayslipItem::IS_ALLOWANCE_NON_REGULAR && $payslipItem->name == 'Tunjangan Pajak Rutin') {
                $brutoIncome += (int) $payslipItem->amount;
            }

            if ($payslipItem->category == PayslipItem::IS_ALLOWANCE_NON_REGULAR && $payslipItem->name == 'Tunjangan Pajak Tidak Rutin') {
                $brutoIncome += (int) $payslipItem->amount;
            }
        }

        return $brutoIncome;
    }

    private function isBrutoIncome($payslipItem)
    {
        $regularIncomeBpjs = [PayslipItem::JKK, PayslipItem::JKM];
        
        if (
            $payslipItem->category == PayslipItem::IS_BASIC_SALARY || 
            $payslipItem->category == PayslipItem::IS_ALLOWANCE_REGULAR || 
            (
                $payslipItem->category == PayslipItem::IS_ASSURANCE_ALLOWANCE && 
                $payslipItem->type == PayslipItem::TYPE_PAID_COMPANY && 
                in_array($payslipItem->assurance_type, $regularIncomeBpjs)
            )
        ) {
            return true;
        }

        return false;
    }

    private function collectTaxAmount($payslip)
    {
        return $payslip->component_regular_tax + $payslip->component_irregular_tax;
    }

    private function formatCurrency($value)
    {
        return number_format($value);
    }

    private function createPph21Pdf($params)
    {
        $pdf = new Fpdi\Fpdi('L', 'mm', array(225,330));

        if (count($params['employees']['list']) === 0) {
            $pdf = $this->createContentPph21Pdf($pdf, $params);
        } else {
            foreach ($params['employees']['list'] as $index => $list) {        
                $pdf = $this->createContentPph21Pdf($pdf, $params, $index);
            }
        }

        return $pdf;
    }

    private function createContentPph21Pdf($pdf, $params, $index = 0)
    {
        $pdf->AddPage('L');
        $fileExcelFolder = storage_path("payroll-report");
        $pdf->setSourceFile($fileExcelFolder . "/form1721-a1-masa-pajak.pdf");
        $tplId = $pdf->importPage(1);
        $pdf->useTemplate($tplId);
         
        $pdf->SetFont('Arial', 'B', 8);
        
        $pdf->SetXY(89, 33);
        $pdf->Write(0, $params['tax_period']['month']);
        $pdf->SetXY(103, 33);
        $pdf->Write(0, $params['tax_period']['year']);

        $pdf->SetXY(125, 29);
        $pdf->Write(0, 'X');

        if (count($params['employees']['list']) > 0) {
            $y = 68;
            foreach ($params['employees']['list'][$index] as $value) {
                $pdf->SetXY(25, $y);
                $pdf->Write(0, $value['tax_number']);
                $pdf->SetXY(68, $y);
                $pdf->Write(0, $value['name']);
                $pdf->SetXY(198, $y);
                $pdf->Write(0, $value['tax_code']);
                $pdf->SetXY(230, $y);
                $pdf->Cell(20, 0, $value['bruto_income_amount'], 0, 0, 'R');
                $pdf->SetXY(268, $y);
                $pdf->Cell(18, 0, $value['tax_amount'], 0, 0, 'R');
                $y += 6; 
            }
        }

        $pdf->SetXY(230, 188);
        $pdf->Cell(20, 0, $params['employees']['total_bruto_income_amount_of_payslip_with_tax'], 0, 0, 'R');
        $pdf->SetXY(268, 188);
        $pdf->Cell(18, 0, $params['employees']['total_tax_amount'], 0, 0, 'R');
        $pdf->SetXY(196, 195);
        $pdf->Write(0, $params['employees']['count_of_payslip_without_tax']);
        $pdf->SetXY(230, 195.5);
        $pdf->Cell(20, 0, $params['employees']['total_bruto_income_amount_of_payslip_without_tax'], 0, 0, 'R');
        $pdf->SetXY(268, 202.5);
        $pdf->Cell(18, 0, $params['employees']['total_tax_amount'], 0, 0, 'R');
        $pdf->SetXY(230, 202.5);
        $pdf->Cell(20, 0, $params['employees']['total_all_bruto_income'], 0, 0, 'R');

        return $pdf;
    }

    private function createPdfFile($file) 
    {
        $folderPath = 'payroll-report/';

        $name = time().str_random(6).".pdf";
        $path = $folderPath.$name;

        Storage::makeDirectory($folderPath);        
        Storage::put($path, $file->output('S'));
        
        $url = \URL::route('payroll.payslip.download', [
            'filename' => base64_encode('app/' . $folderPath . $name),
        ]);
        
        return $url;
    }





    private function createFormA1Pdf($employees, $selectedDate)
    {
        $year = $selectedDate->format('Y');                        
    	$pdf = new Fpdi\Fpdi();

    	$count = 0;
    	foreach ($employees as $employee) {
            $year    = Carbon::parse($selectedDate)->format('Y');
            $payslips = $this->getPayslipFormA1($employee, $year);
    		if ($payslips) {
    			$count++;
    			$pdf = $this->generateReportFormA1PDF($pdf, $payslips, $year, $count);
    		}
    	}

    	return $pdf;
    }

    private function getPayslipFormA1($employee, $year)
    {
        $payslips = Payslip::select('payroll_payslips.*')
                        ->where('payroll_payslips.date', '>=', $year.'-01-01')
                        ->where('payroll_payslips.date', '<=', $year.'-12-31')
                        ->where('payroll_payslips.employee_id', $employee->id)
                        ->get();

        if (count($payslips) > 0) {
            return $this->formA1($payslips, $employee);
        }

        return null;
    }

    private function formA1($payslips, $employee)
    {
        // if ($payrollSetting && $payrollSetting->withholder_tax_number) {
    	//     $withHolderTaxNumber = Helper::npwpRegex($payrollSetting->withholder_tax_number);
    	//     $withHolderTaxNumber = explode('-', $withHolderTaxNumber);
        //     $partWithHolderTaxNumber = explode('.', $withHolderTaxNumber[1]);
        // }

        // if ($payrollSetting && $payrollSetting->npwp) {
        //     $companyTaxNumber = Helper::npwpRegex($payrollSetting->npwp);
        //     $companyTaxNumber = explode('-', $companyTaxNumber);
        //     $partCompanyTaxNumber = explode('.', $companyTaxNumber[1]);
        // }

        $company = 'PT. Trocon Indah Perkasa';
    	// $user = $employee->user;
        
        $taxNumber = $this->npwpRegex($employee->npwp);
        $taxNumber = ($taxNumber) ? explode('-', $taxNumber) : null;
        $partTaxNumber = ($taxNumber) ? explode('.', $taxNumber[1]) : null;
        
        $taxStatus = null;
        for ($index=0; $index<3; $index++) {
            $taxStatus .= (isset($employee->taxstatus[$index])) ? $employee->taxstatus[$index] : null;
            if (isset($employee->taxstatus[$index]) && $employee->taxstatus[$index] === 'K') {
                $temp = $employee->taxstatus[$index+1];
                $taxStatus .= '/';
                $taxStatus .= $temp;
                break;
            }
        }
        $ptkpStatus = (!$taxStatus) ? 'TK/0' : $taxStatus;
        $ptkpStatus = explode('/', $ptkpStatus);

        $address0 = trim(substr($employee->address, 0, 39));
        $address1 = trim(substr($employee->address, 39, 39));

        $gender = Sex::find($employee->sex)->sexname;

        $lastPaidTax = 0;
        $firstPayslip = $payslips[0];
        $lastPayslip  = $payslips[count($payslips)-1];
        $correction = Correction::where('employee_id', $employee->id)->first();
        if ($correction) {
            $lastPaidTax = $correction->exist_tax_paid_total;
        }

        $basicSalary  = $payslips->sum('component_basic_salary');
        $taxAllowance = $payslips->sum('component_tax_allowance_regular') + $payslips->sum('component_tax_allowance_irregular');
        $allowance = $payslips->sum('component_allowance');
        $bpjsEarn  = $payslips->sum('component_tax_allowance_earn');
        $nri = $payslips->sum('component_non_regular_income');
        $occupationFee = (count($payslips) == 0) ? 0 : $payslips[count($payslips)-1]->component_yearly_occupation_fee;
        $bpjsDeduct = $payslips->sum('component_tax_allowance_deduct');
        $lastNettoIncome = ($correction) ? $correction->exist_yearly_netto_income_total : 0;
        
        $ptkp = (count($payslips) == 0) ? 0 : $payslips[count($payslips)-1]->component_ptkp;
        if (!$ptkp) {
            if ($ptkpStatus[0] == 'K') {
                $ptkp = 58500000 + (4500000 * $ptkpStatus[1]);
            } else if ($ptkpStatus[0] == 'TK') {
                $ptkp = 54000000;
            }
        }

        $pkp  = ($basicSalary + $taxAllowance + $allowance + $bpjsEarn + $nri) - ($occupationFee + $bpjsDeduct) + $lastNettoIncome - $ptkp;
        $pkp -= $pkp % 1000;    
        
        $payrollBracket = $this->payrollBracket;
        $tr1 = $payrollBracket['tr1'];
        $tr2 = $payrollBracket['tr2'];
        $tr3 = $payrollBracket['tr3'];
        $tr4 = $payrollBracket['tr4'];
        
        $tarifTotal = 0;
        if ($pkp < $tr1[2]) {
            $tarifTotal = 1;
        } elseif ($pkp < $tr2[2]) {
            $tarifTotal = 2;
        } elseif ($pkp < $tr3[2]) {
            $tarifTotal = 3;
        } elseif ($pkp < $tr4[2]) {
            $tarifTotal = 4;
        }

        $ptTotal1 = 0;
        $ptTotal2 = 0;
        $ptTotal3 = 0;
        $ptTotal4 = 0;
        if ($tarifTotal >= 1) {
            $taxTr1 = ($pkp - $tr1[1]) * $tr1[0];
            $taxTr1 = min([$taxTr1, $tr1[3] * $tr1[0]]);
            $taxTr1 = max([0, $taxTr1]);
            $ptTotal1 = round($taxTr1);
        }
        if ($tarifTotal >= 2) {
            $taxTr2 = ($pkp - $tr2[1]) * $tr2[0];
            $taxTr2 = min([$taxTr2, $tr2[3] * $tr2[0]]);
            $taxTr2 = max([0, $taxTr2]);
            $ptTotal2 = round($taxTr2);
        }
        if ($tarifTotal >= 3) {
            $taxTr3 = ($pkp - $tr3[1]) * $tr3[0];
            $taxTr3 = min([$taxTr3, $tr3[3] * $tr3[0]]);
            $taxTr3 = max([0, $taxTr3]);
            $ptTotal3 = round($taxTr3);
        }
        if ($tarifTotal >= 4) {
            $taxTr4 = ($pkp - $tr4[1]) * $tr4[0];
            $taxTr4 = min([$taxTr4, $tr4[3] * $tr4[0]]);
            $taxTr4 = max([0, $taxTr4]);
            $ptTotal4 = round($taxTr4);
        }

        $ptTotalSum = round($ptTotal1 + $ptTotal2 + $ptTotal3 + $ptTotal4);
        if (!$taxNumber) {
            $ptTotalSum = $ptTotalSum * 1.2;
        }

        $data = [
            'company' => [
                'name' => $company,
                // 'company_tax_number_0' => isset($companyTaxNumber) ? $companyTaxNumber[0] : null,
                // 'company_tax_number_1' => isset($partCompanyTaxNumber) ? $partCompanyTaxNumber[0] : null,
                // 'company_tax_number_2' => isset($partCompanyTaxNumber) ? $partCompanyTaxNumber[1] : null
            ],
        	'withholder' => [
        		// 'name' => ($payrollSetting) ? $payrollSetting->withholder_name : null,
        		// 'jobtitle' => ($payrollSetting) ? $payrollSetting->withholder_jobtitle : null,
        		// 'withholder_tax_number_0' => isset($withHolderTaxNumber) ? $withHolderTaxNumber[0] : null,
                // 'withholder_tax_number_1' => isset($partWithHolderTaxNumber) ? $partWithHolderTaxNumber[0] : null,
                // 'withholder_tax_number_2' => isset($partWithHolderTaxNumber) ? $partWithHolderTaxNumber[1] : null,
        	],
            'employee' => [
                'employee_name'   => $employee->employeename,
                'jobtitle'		  => ($employee->jobtitle) ? trim(substr($employee->jobtitle->positionname, 0, 20)) : null,
                'tax_number_0'    => $taxNumber[0],
                'tax_number_1'    => ($partTaxNumber) ? $partTaxNumber[0] : null,
                'tax_number_2'    => ($partTaxNumber) ? $partTaxNumber[1] : null,
                'identity_number' => $employee->identityno,
                'address_0'       => $address0,
                'address_1'       => $address1,
                'gender'          => $gender,
                'ptkp_status_0'   => $ptkpStatus[0],
                'ptkp_status_1'   => $ptkpStatus[1],
                'first_payslip'   => ($firstPayslip) ? Carbon::parse($firstPayslip->date)->format('m') : 00,
                'last_payslip'    => ($lastPayslip) ? Carbon::parse($lastPayslip->date)->format('m') : 00
            ],
            'payslip' => [
                'basic_salary'  => $basicSalary,
                'tax_allowance' => $taxAllowance,
                'allowance'     => $allowance,
                'bpjs_earn'     => $bpjsEarn,
                'non_regular_income' => $nri,
                'total_bruto_income' => $basicSalary + $taxAllowance + $allowance + $bpjsEarn + $nri,
                'occupation_fee' => $occupationFee,
                'bpjs_deduct'   => $bpjsDeduct,
                'total_deduct'  => $occupationFee + $bpjsDeduct,
                'yearly_netto_income' => ($basicSalary + $taxAllowance + $allowance + $bpjsEarn + $nri) - ($occupationFee + $bpjsDeduct),
                'last_netto_income'   => $lastNettoIncome,
                'final_yearly_netto_income' => ($basicSalary + $taxAllowance + $allowance + $bpjsEarn + $nri) - ($occupationFee + $bpjsDeduct) + $lastNettoIncome,
                'ptkp'          => $ptkp,
                'netto_income_after_ptkp'   => ((int) $pkp > 0) ? (int) round($pkp) : 0,
                'yearly_tax'    => (int) round($ptTotalSum),
                'paid_tax'      => $payslips->sum('component_regular_tax') + $payslips->sum('component_irregular_tax'),
                'last_paid_tax' => $lastPaidTax,
                'take_home_pay' => $payslips->sum('component_take_home_pay_first')
            ]
        ];

        return $data;
    }

    private function getDefaultPayrollBracket()
    {
        $bracketSetting = SettingBracket::get()->toArray();;
        $bracketRate = $tr1 = $tr2 = $tr3 = $tr4 = [];

        for ($i=0; $i<4; $i++) {
            $temp = [
                $bracketSetting[$i]['prorate'],
                $bracketSetting[$i]['min'],
                $bracketSetting[$i]['max'],
                $bracketSetting[$i]['max'] - $bracketSetting[$i]['min']
            ];

            if ($i === 0) $tr1 = $temp;
            else if ($i === 1) $tr2 = $temp;
            else if ($i === 2) $tr3 = $temp; 
            else if ($i === 3) $tr4 = $temp;
        }

        return [
            'tr1' => $tr1,
            'tr2' => $tr2,
            'tr3' => $tr3,
            'tr4' => $tr4
        ];
    }

    private function generateReportFormA1PDF($pdf, $data, $year, $count)
    {       
		$pdf->AddPage('P', 'Legal');
		$fileExcelFolder = storage_path("payroll-report");
		$pdf->setSourceFile($fileExcelFolder . "/form1721-a1-1.pdf");
		$tplId = $pdf->importPage(1);
		$pdf->useTemplate($tplId, 10, 10, 200);

		$pdf->SetFont('Arial', 'B', 8);
		        
		$pdf->SetXY(107, 42);
		$pdf->Write(0, '12');
		$pdf->SetXY(119.5, 42);
		$pdf->Write(0, substr($year, 2));
		$pdf->SetXY(131, 42);
		$pdf->Write(0, str_pad($count, 7, '0', STR_PAD_LEFT));

		$pdf->SetXY(182.5, 42.8);
		$pdf->Write(0, $data['employee']['first_payslip']);
		$pdf->SetXY(195, 42.8);
		$pdf->Write(0, $data['employee']['last_payslip']);

		// $pdf->SetXY(50, 51);
		// $pdf->Write(0, $data['company']['company_tax_number_0']);
		// $pdf->SetXY(97, 51);
		// $pdf->Write(0, $data['company']['company_tax_number_1']);
		// $pdf->SetXY(113, 51);
		// $pdf->Write(0, $data['company']['company_tax_number_2']);

		$pdf->SetXY(50, 58.5);
		$pdf->Write(0, strtoupper($data['company']['name']));

		$pdf->SetXY(42, 77);
		$pdf->Write(0, $data['employee']['tax_number_0']);
		$pdf->SetXY(89, 77);
		$pdf->Write(0, $data['employee']['tax_number_1']);
		$pdf->SetXY(105, 77);
		$pdf->Write(0, $data['employee']['tax_number_2']);

		$pdf->SetXY(42, 85);
		$pdf->Write(0, $data['employee']['identity_number']);

		if ($data['employee']['ptkp_status_0'] === 'K') {
			$pdf->SetXY(130, 84);
			$pdf->Write(0, $data['employee']['ptkp_status_1']);	
		} else if ($data['employee']['ptkp_status_0'] === 'TK') {
			$pdf->SetXY(153, 84);
			$pdf->Write(0, $data['employee']['ptkp_status_1']);	
		}

		$pdf->SetXY(42, 92);
		$pdf->Write(0, strtoupper($data['employee']['employee_name']));

		$pdf->SetXY(155, 91);
		$pdf->Write(0, strtoupper($data['employee']['jobtitle']));

		$pdf->SetXY(42, 98);
		$pdf->Write(0, strtoupper($data['employee']['address_0']));
		$pdf->SetXY(42, 104.5);
		$pdf->Write(0, strtoupper($data['employee']['address_1']));

		if ($data['employee']['gender'] === 'Laki-Laki') {
			$pdf->SetXY(44, 112.5);
		    $pdf->Write(0, 'X');
		} else if ($data['employee']['gender'] === 'Perempuan') {
			$pdf->SetXY(71.5, 112.5);
		    $pdf->Write(0, 'X');
		}

        $pdf->SetXY(41.3, 132);
        $pdf->Write(0, 'X');

		$pdf->SetXY(165, 137.5);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['basic_salary']), 0, 0, 'R');
        $pdf->SetXY(165, 143);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['tax_allowance']), 0, 0, 'R');
        $pdf->SetXY(165, 148.5);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['allowance']), 0, 0, 'R');
        $pdf->SetXY(165, 154.3);
        $pdf->Cell(0, 10, 0, 0, 0, 'R');
        $pdf->SetXY(165, 159.3);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['bpjs_earn']), 0, 0, 'R');
        $pdf->SetXY(165, 165.2);
        $pdf->Cell(0, 10, 0, 0, 0, 'R');
        $pdf->SetXY(165, 170.7);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['non_regular_income']), 0, 0, 'R');
        $pdf->SetXY(165, 176);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['total_bruto_income']), 0, 0, 'R');

        $pdf->SetXY(165, 187);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['occupation_fee']), 0, 0, 'R');
        $pdf->SetXY(165, 192);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['bpjs_deduct']), 0, 0, 'R');
        $pdf->SetXY(165, 197.8);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['total_deduct']), 0, 0, 'R');

        $pdf->SetXY(165, 208.8);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['yearly_netto_income']), 0, 0, 'R');
        $pdf->SetXY(165, 214);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['last_netto_income']), 0, 0, 'R');
        $pdf->SetXY(165, 219.5);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['final_yearly_netto_income']), 0, 0, 'R');
        $pdf->SetXY(165, 225);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['ptkp']), 0, 0, 'R');
        $pdf->SetXY(165, 230.7);
        $pdf->Cell(0, 10, number_format((int) substr($data['payslip']['netto_income_after_ptkp'], 0, -3) . '000'), 0, 0, 'R');
        $pdf->SetXY(165, 236.3);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['yearly_tax']), 0, 0, 'R');
        $pdf->SetXY(165, 241.1);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['last_paid_tax']), 0, 0, 'R');
        $pdf->SetXY(165, 247.1);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['paid_tax']), 0, 0, 'R');
        $pdf->SetXY(165, 252.5);
        $pdf->Cell(0, 10, number_format((int) $data['payslip']['paid_tax']), 0, 0, 'R');

        // $pdf->SetXY(42.2, 272);
        // $pdf->Write(0, $data['withholder']['withholder_tax_number_0']);
        // $pdf->SetXY(89.2, 272);       
        // $pdf->Write(0, $data['withholder']['withholder_tax_number_1']);
        // $pdf->SetXY(105.2, 272);
        // $pdf->Write(0, $data['withholder']['withholder_tax_number_2']);

        // $pdf->SetXY(42.2, 281);
        // $pdf->Write(0, strtoupper($data['withholder']['name']));
        $pdf->SetXY(125.5, 281);
        $pdf->Write(0, '31');
        $pdf->SetXY(137.5, 281);
        $pdf->Write(0, '12');
        $pdf->SetXY(149.2, 281);
        $pdf->Write(0, $year);

	   return $pdf;
    }

    private function npwpRegex($string)
    {
        $split = str_split(preg_replace('~[^0-9]+~', '', $string));
        $string = ($split[0] === '' || $split[0] === null) ? [] : $split;

        $result = null;
        for ($i = 0; $i < 15; $i++) {
            if ($i === 2 || $i === 5 || $i === 8 || $i === 12) {
                $result .= '.';
            } elseif ($i === 9) {
                $result .= '-';
            }

            if (count($string) > 0) {
                if (isset($string[$i])) {
                    $result .= $string[$i];
                } else {
                    $result .= 'x';
                }
            } else {
                $result .= '0';
            }
        }

        return $result;
    }
}
