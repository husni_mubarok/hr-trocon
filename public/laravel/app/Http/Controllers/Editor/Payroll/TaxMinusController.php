<?php

namespace App\Http\Controllers\Editor\Payroll;

use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use App\Model\Payroll\Payslip;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TaxMinusController extends Controller
{
    public function showResult()
    {
        $currentYear = Carbon::now()->format('Y');
        $yearFilter = ['2018', '2019', '2020', '2021', '2022', '2023'];

        return view ('editor.payroll.tax_minus.index', compact('currentYear', 'yearFilter'));
    }

    public function showResultData(Request $request)
    {
        $year = ($request->input('year')) ? $request->input('year') : Carbon::now()->format('Y');

        $payslips = Payslip::select(\DB::raw("SUM(component_regular_tax) as total"), 'payroll_payslips.*')
                        ->whereYear('date', '=', $year)
                        ->where('component_regular_tax', '<', 0)
                        ->groupBy('employee_id')
                        ->orderBy('id', 'DESC')
                        ->get();
        
        foreach ($payslips as $payslip) {
            $payslip['employee_name'] = $payslip->employee->employeename;
            $payslip['periode'] = $this->getPeriod($payslip);
            $payslip['total'] = number_format($payslip['total']);
        }

        return response()->json([
            'draw' => 0,
            'recordsTotal' => count($payslips),
            'recordsFiltered' => count($payslips),
            'data' => $payslips,
            'input' => []
        ]);  
    }

    public function download(Request $request)
    {
        $year = ($request->input('year')) ? $request->input('year') : Carbon::now()->format('Y');

        $payslips = Payslip::select(\DB::raw("SUM(component_regular_tax) as total"), 'payroll_payslips.*')
                        ->whereYear('date', '=', $year)
                        ->where('component_regular_tax', '<', 0)
                        ->groupBy('employee_id')
                        ->orderBy('id', 'DESC')
                        ->get();

        $filename = 'Pengembalian pajak';
        $header = ['No', 'Nama', 'Periode', 'Jumlah yang Harus Dikembalikan'];
        Excel::create($filename, function ($excel) use ($header, $payslips) {
            $excel->sheet('Sheet1', function ($sheet) use ($header, $payslips) {
                $sheet->row(1, $header);

                foreach ($payslips as $i => $payslip) {
                    $sheet->prependRow(2 + $i,
                        [
                            $i + 1,
                            $payslip->employee->employeename,
                            $this->getPeriod($payslip),
                            number_format($payslip['total'])
                        ]
                    );

                    $i++;
                }
            });
        })->store('xls', storage_path('app/payroll-report/'));
        $filename = 'Pengembalian pajak.xls';
        $folderPath = 'payroll-report/';

        $url = \URL::route('payroll.payslip.download', [
            'filename' => base64_encode('app/' . $folderPath . $filename)
        ]);

        return response()->json([
            'url' => $url
        ]);
    }

    private function getPeriod($payslip)
    {
        $result = Payslip::where('employee_id', $payslip->employee_id)
                        ->where('component_regular_tax', '<', 0)
                        ->get();

        $period = null;
        foreach ($result as $record) {
            if (!is_null($period)) {
                $period .= ', ' . Carbon::parse($record->date)->format('F Y');
            } else {
                $period .= Carbon::parse($record->date)->format('F Y');
            }
        }

        return $period;
    }
}
