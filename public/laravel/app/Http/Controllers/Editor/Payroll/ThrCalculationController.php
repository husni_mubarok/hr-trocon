<?php

namespace App\Http\Controllers\Editor\Payroll;

use Auth;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Thrdet;
use App\Model\Thr;

class ThrCalculationController extends Controller
{
    public function preview()
    {
    	$datafilter = User::where('id', Auth::id())->first();
    	$department_list = Department::orderBy('id', 'ASC')->get();
        $thr_list = Thr::orderBy('id', 'desc')->where('status', '0')->groupBy('description')->get();
        
        return view ('editor.payroll.thr_preview.index', [
            'department_list' => $department_list,
            'thr_list' => $thr_list,
            'datafilter' => $datafilter
        ]);
    }

    public function getDataAjax(Request $request)
    {
        if (!$request->input('thr_date')) {
            $department = Department::orderBy('id', 'ASC')->first();
            $departmentId = $department->id;
            
            $result = Thr::where('departmentid', $departmentId)->orderBy('thr.id', 'DESC')->first();
            $result = $result->thrDets;
            foreach ($result as $record) {
                if (!$record->employee || $record->employee->deleted_at) continue;
                $record['position'] = ($record->employee->positionid) ? $record->employee->jobtitle->positionname : null;
                $record['department'] = ($record->employee->departmentid) ? $record->employee->department->departmentname : null;
            }
        } else {
            $thrDate = $request->input('thr_date');
            $departmentId = $request->input('department_id');

            $result = ThrDet::join('thr', 'thr.id', '=', 'thrdet.transid')
                        ->join('employee', 'thrdet.employeeid', '=', 'employee.id')
                        ->whereNull('employee.deleted_at')
                        ->where('thr.departmentid', $departmentId)
                        ->where('thr.datetrans', $thrDate)
                        ->get();
            foreach ($result as $record) {
                if (!$record->employee || $record->employee->deleted_at) continue;
                $record['position'] = ($record->employee->positionid) ? $record->employee->jobtitle->positionname : null;
                $record['department'] = ($record->employee->departmentid) ? $record->employee->department->departmentname : null;
            }
        }
    	
        return response()->json([
            'draw' => 0,
            'recordsTotal' => count($result),
            'recordsFiltered' => count($result),
            'data' => $result,
            'input' => []
        ]);  
    }
}
