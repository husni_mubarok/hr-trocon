<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;

class PayrollController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'payrollname' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $payrolls = Payroll::all();
    // $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $payperiod_list = Payperiod::orderBy('id', 'DESC')->where('status', '0')->get();
    $department_list = Department::all()->pluck('departmentname', 'departmentcode');

     $sql1 = 'SELECT
              `user`.id,
              payperiod.id AS periodid,
              payperiod.description
            FROM
              payperiod
            INNER JOIN `user` ON payperiod.id = `user`.periodid';

    $datafilter = DB::table(DB::raw("($sql1) as rs_sql"))->where('id', Auth::id())->first(); 

    return view ('editor.payroll.index', compact('payrolls','payperiod_list','department_list', 'datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
              payperiod.id,
              payperiod.description,
              payperiod.dateperiod,
              payperiod.begindate,
              payperiod.enddate,
              payperiod.paydate,
              payperiod.`status`,
              payperiod.`month`,
              payperiod.`year`,
              payrolltype.payrolltypename
            FROM
              payperiod
            LEFT JOIN payrolltype ON payperiod.payrolltypeid = payrolltype.id, user
            WHERE user.id = '.$userid.' AND payperiod.id = user.periodid
            AND payperiod.deleted_at IS NULL';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="payroll/'.$itemdata->id.'/edit" title="Detail" class="btn btn-primary btn-xs btn-flat" onclick="edit('."'".$itemdata->id."'".')"><i class="fa fa-folder"></i> Detail</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Aktif </span>';
        }else{
         return '<span class="label label-danger"> Tidak Aktif </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  } 

  public function store($id)
  {  
    DB::insert("insert into payroll
                      (employeeid, periodid, departmentid)
                select     derivedtbl.employeeid, derivedtbl.periodid, derivedtbl.departmentid
                from         (select     employee.id AS employeeid, employee.status, user.periodid, employee.departmentid
                                       from          employee cross join
                                                              user
                                       where      employee.status = 0 and user.id = 1) derivedtbl left outer join
                                      payroll on derivedtbl.employeeid = payroll.employeeid and derivedtbl.periodid = payroll.periodid
                where     (payroll.periodid is null)");

    //return response()->json($post); 
  }

  public function generate($id)
  {  

  DB::insert("insert into payroll
                (employeeid, periodid, departmentid)
          select     derivedtbl.employeeid, derivedtbl.periodid, derivedtbl.departmentid
          from         (select     employee.id AS employeeid, employee.status, user.periodid, employee.departmentid
                                 from          employee cross join
                                                        user
                                 where      employee.status =0 and user.id = 1) derivedtbl left outer join
                                payroll on derivedtbl.employeeid = payroll.employeeid and derivedtbl.periodid = payroll.periodid
          where     (payroll.periodid is null)");

      $userid = Auth::id();

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          payperiod.`month`,
                          payperiod.`year`
                        FROM
                          `user`
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 
  

    /* Untuk meng Update Gaji, Tunjangan, Uang Makan & Tetap, Overtime & Tetap mengjadi 0 */
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT * FROM employee) AS u
                SET a.basic = 0,
                 a.postall = 0,
                 a.mealtrans = 0,
                 a.overtime = 0,
                 a.mealtransall = 0,
                 a.overtimeall = 0,
                 a.medicalclaim = 0,
                 a.tht = 0, 
                 a.otproject = 0,
                 a.employeeloan = 0,
                 a.remainemployeeloan = 0,
                 a.insuranceloan = 0,
                 a.remaininsuranceloan = 0,
                 a.iuranpensiunkaryawan = 0, 
                 a.tunjangankesehatan = 0, 
                 a.jamsostekmember = 0, 
                 a.ptkp = 0,
                 a.pph21 = 0
                WHERE
                  a.employeeid = u.id
                AND u.`status` = 0
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");


        DB::update("UPDATE  employee
                SET termdate = NULL
                WHERE
                  termdate = '0000-00-00'");

    /* Untuk mengambil Gaji dari master Employee */
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT * FROM employee) u
                SET a.basic =  
                u.basic
                WHERE
                  a.employeeid = u.id
                AND u.`status` = 0
                AND a.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = 1
                )");


    /* Update day work*/
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT
                  employee.id,
                  TIMESTAMPDIFF(
                    DAY,
                    employee.joindate,
                    period.paydate
                  ) AS daywork,
                  employee.employeename
                FROM
                  employee,
                  (
                    SELECT
                      payperiod.paydate
                    FROM
                      `user`
                    INNER JOIN payperiod ON `user`.periodid = payperiod.id
                    WHERE
                      `user`.id = 1
                  ) AS period) u
                SET a.daywork =  u.daywork
                WHERE
                  a.employeeid = u.id
                AND a.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = 1
                )"); 

    DB::update("UPDATE payroll a
                INNER JOIN (SELECT
                  employee.id,
                  TIMESTAMPDIFF(
                    DAY,
                    period.begindate,
                    employee.termdate
                  ) AS daywork,
                  employee.employeename,
                  employee.termdate
                FROM
                  employee,
                  (
                    SELECT
                      payperiod.begindate
                    FROM
                      `user`
                    INNER JOIN payperiod ON `user`.periodid = payperiod.id
                    WHERE
                      `user`.id = 1
                  ) AS period) u
                SET a.daywork =  u.daywork
                WHERE
                  a.employeeid = u.id AND (u.termdate IS NOT NULL OR u.termdate <> '')
                AND a.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = 1
                )"); 
    
    /* Untuk mengambil Gaji dari master Employee */
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT * FROM employee) u
                SET a.basic = u.basic
                WHERE
                  a.employeeid = u.id
                AND u.`status` = 0
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");


    /* Porpotional basic */
    // by join date
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT * FROM employee) u
                SET a.basic = CASE
                WHEN u.locationid = 1  THEN
                  (a.daywork / 21) * u.basic ELSE (a.daywork / 25) * u.basic END,

                  a.mealtransall = CASE
                WHEN u.locationid = 1  THEN
                  (a.daywork / 21) * u.mealtransall ELSE (a.daywork / 25) * u.mealtransall END,

                  a.overtimeall = CASE
                WHEN u.locationid = 1  THEN
                  (a.daywork / 21) * u.overtimeall ELSE (a.daywork / 25) * u.overtimeall END,

                  a.postall = CASE
                WHEN u.locationid = 1  THEN
                  (a.daywork / 21) * u.postall ELSE (a.daywork / 25) * u.postall END
                WHERE
                  a.employeeid = u.id
                AND u.`status` = 0
                AND a.daywork < 30
                AND a.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = 1
                )");

     /* Untuk mengambil status karyawan yang uang makannya di bayar di kantor dari master Employee */
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT * FROM employee) u
                SET a.statusbayarkantor = u.statusdibayarkantor
                WHERE
                  a.employeeid = u.id
                AND u.`status` = 0
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");

    /* Untuk mengambil status pembayaran yang uang makannya di bayar perbulan dari master Employee */
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT * FROM employee) u
                SET a.statusistimewa = u.statusistimewa
                WHERE
                  a.employeeid = u.id
                AND u.`status` = 0
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");

    /* Untuk mengambil status pembayaran yang uang makannya di bayar perbulan dari master Employee */
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT * FROM employee) u
                SET a.mealtransall = ifnull(u.mealtransall, 0)
                WHERE
                  a.employeeid = u.id
                AND u.`status` = 0
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");

    /* Untuk mengambil tunjangan uang makan tetap dari master Employee */
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT * FROM employee) u
                SET a.mealtransall = ifnull(u.mealtransall, 0)
                WHERE
                  a.employeeid = u.id
                AND u.`status` = 0
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");

    /*  Update uang makan project dari transaksi Uang Makan Dan Transport */
    DB::update("UPDATE payroll a
                INNER JOIN (
                  SELECT
                    mealtran.periodid,
                    mealtrandet.employeeid AS empid,
                    mealtrandet.um1,
                    mealtrandet.um2,
                    mealtrandet.um3,
                    mealtrandet.um4,
                    mealtrandet.um5,
                    mealtrandet.amount,
                    mealtrandet.insentif1,
                    mealtrandet.insentif2,
                    mealtrandet.insentif3,
                    mealtrandet.insentif4,
                    mealtrandet.insentif5,
                    mealtrandet.insentif,
                    mealtrandet.addmeal,
                    mealtrandet.amountpajak,
                    mealtrandet.potabsence,
                    IFNULL(mealtrandet.pottelat1,0) + IFNULL(mealtrandet.pottelat2,0) + IFNULL(mealtrandet.pottelat3,0) + IFNULL(mealtrandet.pottelat4,0) + IFNULL(mealtrandet.pottelat5,0) AS pottelat, 
                    ifnull(mealtrandet.um1, 0) + ifnull(mealtrandet.um2, 0) + ifnull(mealtrandet.um3, 0) + ifnull(mealtrandet.um4, 0) + ifnull(mealtrandet.um5, 0) + ifnull(mealtrandet.addmeal, 0) AS totalmealtrans,
                    ifnull(mealtrandet.insentif1, 0) + ifnull(mealtrandet.insentif2, 0) + ifnull(mealtrandet.insentif3, 0) + ifnull(mealtrandet.insentif4, 0) + ifnull(mealtrandet.insentif5, 0) + ifnull(mealtrandet.addinsentif, 0) AS totalinsentif
                  FROM
                    mealtran
                  RIGHT OUTER JOIN mealtrandet ON mealtran.id = mealtrandet.transid
                  WHERE mealtran.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )
                ) u
                SET a.mealtransproject = u.totalmealtrans + u.pottelat, a.insentif = u.totalinsentif + u.pottelat, a.potterlambat = CASE WHEN a.departmentid = 2 THEN 0 ELSE u.pottelat END,
                 a.amountpajak = ifnull(u.amountpajak, 0) 
                WHERE
                  a.employeeid = u.empid
                AND a.periodid = u.periodid
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");

    DB::update("UPDATE payroll a
                INNER JOIN (
                  SELECT
                    mealtran.periodid,
                    mealtrandet.employeeid AS empid,
                    mealtrandet.um1,
                    mealtrandet.um2,
                    mealtrandet.um3,
                    mealtrandet.um4,
                    mealtrandet.um5,
                    mealtrandet.amount,
                    mealtrandet.insentif1,
                    mealtrandet.insentif2,
                    mealtrandet.insentif3,
                    mealtrandet.insentif4,
                    mealtrandet.insentif5,
                    mealtrandet.insentif,
                    mealtrandet.addmeal,
                    mealtrandet.amountpajak,
                    ifnull(mealtrandet.um1, 0) + ifnull(mealtrandet.um2, 0) + ifnull(mealtrandet.um3, 0) + ifnull(mealtrandet.um4, 0) + ifnull(mealtrandet.um5, 0) + ifnull(mealtrandet.insentif1, 0) + ifnull(mealtrandet.insentif2, 0) + ifnull(mealtrandet.insentif3, 0) + ifnull(mealtrandet.insentif4, 0) + ifnull(mealtrandet.insentif5, 0) + ifnull(mealtrandet.addmeal, 0) + ifnull(mealtrandet.addinsentif, 0) AS totalmealtrans
                  FROM
                    mealtran
                  RIGHT OUTER JOIN mealtrandet ON mealtran.id = mealtrandet.transid
                  WHERE mealtran.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )
                ) u
                SET a.totalmealtrans = u.totalmealtrans,
                 a.amountpajak = ifnull(u.amountpajak, 0) 
                WHERE
                  a.employeeid = u.empid
                AND a.periodid = u.periodid
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");

     /*  Update uang makan project dari transaksi Uang Makan Dan Transport */
    DB::update("UPDATE payroll a
                INNER JOIN employee u
                SET a.overtimeall = ifnull(u.overtimeall, 0)
                WHERE
                  a.employeeid = u.id
                AND u.`status` = 0
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");


    //update pot pensiun
     DB::update("UPDATE payroll
                  INNER JOIN (
                    SELECT
                      employee.id,
                      CASE
                    WHEN employee.basic <= umr.umr THEN
                      umr.umr * 0.01
                    ELSE
                      CASE
                    WHEN employee.basic > umr.umr
                    AND employee.basic < iuranpensiun.iuranpensiun THEN
                      employee.basic * 0.01
                    ELSE
                      iuranpensiun.iuranpensiun * 0.01
                    END
                    END AS potpensiun
                    FROM
                      employee,
                      umr,
                      iuranpensiun
                    WHERE employee.statuspotpensiun = 1
                  ) AS pensuin ON payroll.employeeid = pensuin.id
                  SET payroll.iuranpensiunkaryawan = pensuin.potpensiun
                WHERE payroll.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");

    /* Untuk mengambil tunjangan uang lembut tetap dari master Employee */
    DB::update("UPDATE payroll a
                INNER JOIN employee u
                SET a.overtimeall = ifnull(u.overtimeall, 0)
                WHERE
                  a.employeeid = u.id
                AND u.`status` = 0
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");


    /*  Update uang Lembur Project dari transaksi Lembur. Uang lembur di bayar perbulan, tapi Extra Puding di bayar perminggu. maka total uang makan di kurangi total extra puding */
    DB::update("UPDATE payroll a
                INNER JOIN (
                  SELECT
                    overtimerequest.periodid,
                    overtimerequestdet.employeeid,
                    overtimerequestdet.amount,
                    overtimerequestdet.ot1,
                    overtimerequestdet.ot2,
                    overtimerequestdet.ot3,
                    overtimerequestdet.ot4,
                    overtimerequestdet.ot5,
                    CASE
                    WHEN overtimerequestdet.otcorrection IS NULL THEN
                      0
                    ELSE
                      overtimerequestdet.otcorrection
                    END AS otcorrection,
                    overtimerequestdet.rateovertime,
                    overtimerequestdet.addovertime
                  FROM
                    overtimerequest
                  RIGHT OUTER JOIN overtimerequestdet ON overtimerequest.id = overtimerequestdet.transid
                  WHERE
                    overtimerequest.periodid = (
                      SELECT
                        `user`.periodid
                      FROM
                        `user`
                      WHERE
                        `user`.id = 1
                    )
                  ORDER BY
                    overtimerequestdet.employeeid ASC
                ) u
                -- SET a.otproject = ((u.ot1 + u.ot2 + u.ot3 + u.ot4 + u.ot5 + u.otcorrection) * u.rateovertime)
                SET a.otproject = (FORMAT((u.ot1 + u.ot2 + u.ot3 + u.ot4 + u.ot5 + u.otcorrection),1) * u.rateovertime) + u.addovertime
                WHERE
                  a.employeeid = u.employeeid
                AND a.periodid = u.periodid
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");

    /* Untuk mengambil tunjangan jabatan dari master Employee */
    DB::update("UPDATE payroll a
                INNER JOIN employee u
                SET a.postall = ifnull(u.postall, 0)
                WHERE
                  a.employeeid = u.id
                AND a.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )");
     
      /* Untuk mengambil status Jamsostek dari master Employee */
      DB::update("UPDATE payroll a
      INNER JOIN employee u
      SET a.jamsostekmember = u.jamsostekmember
      WHERE
        a.employeeid = u.id
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");

      /* Update perhitungan Jamsostek */
      DB::update("UPDATE payroll a
      INNER JOIN (SELECT umr FROM umr) u
      SET a.jamsostek = CASE
      WHEN basic < u.umr THEN
        u.umr * 2 / 100
      ELSE
        basic * 2 / 100
      END * jamsostekmember
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");

      /* Porpotional basic */
    // by join date
    DB::update("UPDATE payroll a
                INNER JOIN (SELECT * FROM employee) u
                SET a.basic = CASE
                WHEN u.locationid = 1  THEN
                  (a.daywork / 21) * u.basic ELSE (a.daywork / 25) * u.basic END,

                  a.mealtransall = CASE
                WHEN u.locationid = 1  THEN
                  (a.daywork / 21) * u.mealtransall ELSE (a.daywork / 25) * u.mealtransall END,

                  a.overtimeall = CASE
                WHEN u.locationid = 1  THEN
                  (a.daywork / 21) * u.overtimeall ELSE (a.daywork / 25) * u.overtimeall END,

                  a.postall = CASE
                WHEN u.locationid = 1  THEN
                  (a.daywork / 21) * u.postall ELSE (a.daywork / 25) * u.postall END
                WHERE
                  a.employeeid = u.id
                AND u.`status` = 0
                AND a.daywork < 30
                AND a.periodid = (
                  SELECT
                    user.periodid
                  FROM
                    `user`
                  WHERE
                    user.id = 1
                )");


      /*  Update Amount Medical Claim dari Menu Transaction */
      // DB::update("UPDATE payroll a
      // INNER JOIN (
      //   SELECT
      //     medicalrequest.periodid,
      //     medicalrequest.employeeid,
      //     sum(
      //       medicalrequestdet.claimamount
      //     ) AS claimamount
      //   FROM
      //     medicalrequest
      //   RIGHT OUTER JOIN medicalrequestdet ON medicalrequest.id = medicalrequestdet.transid
      //   WHERE
      //     (medicalrequest.status = 1)
      //   GROUP BY
      //     medicalrequest.employeeid,
      //     medicalrequest.periodid
      // ) u
      // SET a.medicalclaim = u.claimamount
      // WHERE
      //   a.employeeid = u.employeeid
      // AND a.periodid = u.periodid
      // AND a.periodid = (
      //   SELECT
      //     `user`.periodid
      //   FROM
      //     `user`
      //   WHERE
      //     `user`.id = 1
      // )");


      DB::update("UPDATE payroll a
      INNER JOIN (
        SELECT
          reimburse.periodid,
          reimburse.employeeid,
          sum(
            reimbursedet.amount
          ) AS claimamount
        FROM
          reimburse
        RIGHT OUTER JOIN reimbursedet ON reimburse.id = reimbursedet.transid
        WHERE reimbursedet.deleted_at IS NULL
        GROUP BY
          reimburse.employeeid,
          reimburse.periodid
      ) u
      SET a.medicalclaim = u.claimamount
      WHERE
        a.employeeid = u.employeeid
      AND a.periodid = u.periodid
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");


      DB::update("UPDATE payroll a
      INNER JOIN (
        SELECT
          employee.id,
          employee.tunjangankesehatan,
          employee.basicbegin / 12 AS basic
        FROM
          employee
        WHERE employee.tunjangankesehatan = 'Asuransi'
      ) u
      SET a.tunjangankesehatan = u.basic
      WHERE
        a.employeeid = u.id
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");


       DB::update("UPDATE payroll a
      INNER JOIN (
        SELECT
          employee.id,
          employee.tunjangankesehatan,
          -- umr.umr / 12 AS basic
          umr.umr * 0.05 AS basic
        FROM
          employee, umr
        WHERE employee.tunjangankesehatan = 'BPJS'
      ) u
      SET a.tunjangankesehatan = u.basic
      WHERE
        a.employeeid = u.id
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");



      /* Update untuk menghasilkan perhitungan Bruto = Gapok + Tunj Uang Makan + Tunj Lembur + Lembur Project + Tunj Jabatan + Tunj Jamsostek + Tunj Kesehatan + Uang Makan Project + Extra Pudding */
      DB::update("UPDATE payroll
      SET bruto = ifnull(basic, 0) + ifnull(mealtransall, 0) + ifnull(overtimeall, 0) + ifnull(postall, 0) + ifnull(jamsostek, 0) + ifnull(medicalclaim, 0) + ifnull(tunjangankesehatan, 0) + ifnull(amountpajak, 0) + ifnull(otproject, 0) + ifnull(extrapuddingproject, 0) + ifnull(tunjmalam, 0) + ifnull(tunluarkota, 0) + ifnull(insentif, 0) + ifnull(otherall, 0) + ifnull(mealtransproject, 0)
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");


      /* Untuk mengambil status Biaya Jabatan dari master Employee */
      DB::update("UPDATE payroll a
      INNER JOIN employee u
      SET a.biayajabmember = u.biayajabatan
      WHERE
        a.employeeid = u.id
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");


      /* Untuk Menghasilkan Biaya Jabatan = Jika Gaji Bruto di x 0.5 % lebih besar dari 500rb makan 500rb, selain itu Gaji Bruto di x 0.5% */
      DB::update("UPDATE payroll
      SET biayajab = round(
        CASE
        WHEN bruto * 5 / 100 > 500000 THEN
          500000
        ELSE
          bruto * 5 / 100
        END,
        0
      ) * biayajabmember
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");


      /* Untuk potongan THT = Jika Gapok di bawah UMR maka UMR di x 0.2 %, selain itu Gapok di x 0.2%  */
      DB::update("UPDATE payroll a
      INNER JOIN (SELECT umr FROM umr) u
      SET a.tht = CASE
      WHEN basic < u.umr THEN
        u.umr * 2 / 100
      ELSE
        basic * 2 / 100
      END * jamsostekmember
      WHERE
        jamsostekmember = 1
      AND (
        periodid = (
          SELECT
            `user`.periodid
          FROM
            `user`
          WHERE
            `user`.id = 1
        )
      )");


      /* Untuk menghasilkan Netto Pajak = Bruto - Pot Biaya Jabatan - Pot THT  */
      DB::update("UPDATE payroll
      SET netto = round(
        ifnull(bruto, 0) - ifnull(biayajab, 0) - ifnull(tht, 0),
        0
      )
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");


      /* Untuk mengambil status pajak dari master Employee */
      DB::update("UPDATE payroll a
      INNER JOIN employee u
      SET a.taxstatus = u.taxstatus
      WHERE
        a.employeeid = u.id
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");


      /* Untuk menghasilkan Potongan Tidak Kena Pajak dari Master Table PTKP */
      DB::update("UPDATE payroll a
      INNER JOIN ptkp u
      SET a.ptkp = ifnull(u.ptkp, 0)
      WHERE
        a.taxstatus = u.taxstatus
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");


      /* Untuk menghasilkan Potongan Kena Pajak = Netto Pajak - Potongan Tidak Kena Pajak dimana Netto Pajak lebih besar dari PTKP */
      DB::update("UPDATE payroll
      SET pkp = ifnull(netto, 0) - ifnull(ptkp, 0)
      WHERE
        netto > ptkp
      AND (
        periodid = (
          SELECT
            `user`.periodid
          FROM
            `user`
          WHERE
            `user`.id = 1
        )
      )");


      /* Untuk mengambil Master NPWP dari Employee */
      DB::update("UPDATE payroll a
      INNER JOIN employee u
      SET a.npwp = u.npwp
      WHERE
        a.employeeid = u.id
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");


      /* Untuk menghasilkan PPh21 = Jika NPWP ada maka perhitungan sesuai tarif, jika NPWP tidak ada maka di kalikan dengan 6% */
      DB::update("UPDATE payroll a
      INNER JOIN (
        SELECT
          payroll.employeeid,
          sum(
            CASE
            WHEN pkp * 12 < tarif.`from` THEN
              0
            ELSE
              CASE
            WHEN pkp * 12 > tarif.`to` THEN
              tarif.`to` - tarif.`from`
            ELSE
              pkp * 12 - tarif.`from`
            END
            END * tarif.tarif / 100
          ) AS pph
        FROM
          payroll
        CROSS JOIN tarif
        WHERE
          payroll.periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        GROUP BY
          payroll.employeeid
      ) u
      SET a.pph21 = u.pph / 12
      WHERE
        a.employeeid = u.employeeid
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");

      DB::update("UPDATE payroll
      SET pph21 = CASE
      WHEN length(rtrim(npwp)) > 0 THEN
        pph21
      ELSE
        pph21 * 1.2
      END
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");

      DB::update("UPDATE payroll
      SET pph21 = CASE
      WHEN taxstatus IS NULL THEN
        0
      ELSE
        pph21
      END
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");


      DB::update("UPDATE payroll INNER JOIN (SELECT
        payroll_payslips.payroll_period_id,
        payroll_payslips.employee_id,
        payroll_payslips.component_regular_tax
      FROM
        payroll_payslips) AS tax
      SET payroll.pph21 =  tax.component_regular_tax
      WHERE payroll.periodid = tax.payroll_period_id AND payroll.employeeid = tax.employee_id AND
        (
          payroll.periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");


      /* Untuk menghasilkan Potongan Jamsostek = Jika gaji di bawah UMR maka UMR di x 4.04/100, selain itu gaji di x 4.04/100 */
      DB::update("UPDATE payroll a
      INNER JOIN (SELECT umr FROM umr) u
      SET a.potjamsostek = CASE
      WHEN basic < u.umr THEN
        u.umr * 4.04 / 100
      ELSE
        basic * 4.04 / 100
      END * jamsostekmember
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");


      /* ISNULL(dbo.Payroll.Basic"); 0) * 0,037 */
      DB::update("UPDATE payroll a
      INNER JOIN (SELECT umr FROM umr) u
      SET a.jhtperusahaan = CASE
      WHEN basic <= u.umr THEN
        u.umr * 0.037
      ELSE
        basic * 0.037
      END
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )
      AND a.jamsostekmember = 1");



      DB::update("UPDATE payroll a
      INNER JOIN (SELECT umr FROM umr) u
      SET a.jhtperusahaan = 0
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )
      AND a.jamsostekmember = 0");


      /* Untuk menghasilkan Potongan Iuran Pensiun Karyawan = Jika gaji di bawah UMR maka UMR di x 0.01, selain itu 7335300 di x 0.01 */
      DB::update("UPDATE payroll a
      INNER JOIN (SELECT umr FROM umr) u
      INNER JOIN (
        SELECT
          iuranpensiun
        FROM
          iuranpensiun
      ) b
      SET a.iuranpensiunkaryawan = CASE
      WHEN basic <= u.umr THEN
        u.umr * 0.01
      ELSE
        CASE
      WHEN basic > u.umr
      AND basic < b.iuranpensiun THEN
        basic * 0.01
      ELSE
        b.iuranpensiun * 0.01
      END
      END
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )
      AND a.potpensiunstatus = 1");



      /* Untuk menghasilkan Potongan Iuran Pensiun Perusahaan = Jika gaji di bawah UMR maka UMR di x 0.02, selain itu 7335300 di x 0.02 */
      DB::update("UPDATE payroll a
      INNER JOIN (SELECT umr FROM umr) u
      INNER JOIN (
        SELECT
          iuranpensiun
        FROM
          iuranpensiun
      ) b
      SET a.iuranpensiunperusahaan = CASE
      WHEN basic <= u.umr THEN
        u.umr * 0.02
      ELSE
        CASE
      WHEN basic > u.umr
      AND basic < b.iuranpensiun THEN
        basic * 0.02
      ELSE
        b.iuranpensiun * 0.02
      END
      END
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )
      AND potpensiunstatus = 1");



      DB::update("UPDATE payroll a
      SET a.iuranpensiunperusahaan = 0
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )
      AND potpensiunstatus = 0");


      DB::update("UPDATE payroll a
      SET a.pph21 = 0
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )
      AND pph21 < 0");


      /* Untuk menghasilkan gaji Bersih = Bruto - PPh21 - Potongan Jamsostek */
      DB::update("UPDATE payroll
      SET totalnetto = round(
        ifnull(bruto, 0) - ifnull(pph21, 0) - ifnull(potjamsostek, 0) - ifnull(tpotabsence, 0) - ifnull(iuranpensiunkaryawan, 0) - ifnull(potterlambat, 0),
        0
      )
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");


      /* DB::update("UPDATE UNTUK PINJAMAN KARYAWAN */
      DB::update("UPDATE payroll a
      INNER JOIN (
        SELECT
          loan.employeeid,
          loandet.`month`,
          payperiod.id AS periodid,
          sum(loandet.amount) AS amount
        FROM
          loan
        INNER JOIN loandet ON loan.id = loandet.transid
        INNER JOIN payperiod ON loandet.`year` = payperiod.`year`
        AND loandet.`month` = payperiod.`month`
        WHERE  loan.loantypeid = 1   AND loandet.deleted_at IS NULL
        GROUP BY
          payperiod.begindate,
          payperiod.enddate,
          loan.employeeid,
          loandet.`month`,
          loandet.`year`
      ) u
      SET a.employeeloan = round(ifnull(u.amount, 0) + 0.1, 0)
      WHERE
        a.employeeid = u.employeeid AND a.periodid = u.periodid
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");


      /* DB::update("UPDATE UNTUK PINJAMAN KARYAWAN */
      DB::update("UPDATE payroll a
      INNER JOIN (
        SELECT
          loan.employeeid,
          loandet.`month`,
          payperiod.id AS periodid,
          sum(loandet.bonus) AS amount
        FROM
          loan
        INNER JOIN loandet ON loan.id = loandet.transid
        INNER JOIN payperiod ON loandet.`year` = payperiod.`year`
        AND loandet.`month` = payperiod.`month`
        WHERE  loan.loantypeid = 1   AND loandet.deleted_at IS NULL
        GROUP BY
          payperiod.begindate,
          payperiod.enddate,
          loan.employeeid,
          loandet.`month`,
          loandet.`year`
      ) u
      SET a.employeeloanbonus = round(ifnull(u.amount, 0) + 0.1, 0)
      WHERE
        a.employeeid = u.employeeid AND a.periodid = u.periodid
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");


      /* DB::update("UPDATE UNTUK PINJAMAN KARYAWAN */
      DB::update("UPDATE payroll a
      INNER JOIN (
        SELECT
          loan.employeeid,
          loandet.`month`,
          loandet.`year`,
          payperiod.id AS periodid, 
          sum(loandet.amount) AS amount
        FROM
          loan
        INNER JOIN loandet ON loan.id = loandet.transid
        INNER JOIN payperiod ON loandet.`year` = payperiod.`year`
        AND loandet.`month` = payperiod.`month`
        WHERE  loan.loantypeid = 4 AND loandet.deleted_at IS NULL
        GROUP BY
          payperiod.begindate,
          payperiod.enddate,
          loan.employeeid,
          loandet.`month`,
          loandet.`year`
      ) u
      SET a.insuranceloan = round(ifnull(u.amount, 0) + 0.1, 0)
      WHERE
        a.employeeid = u.employeeid AND a.periodid = u.periodid
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");


      DB::update("UPDATE loandet a
      INNER JOIN (
        SELECT
          loan.remain,
          loandet.id,
          payperiod.`month`,
          payperiod.`year`,
          payroll.employeeloan 
        FROM
          `user`
        INNER JOIN payroll ON `user`.periodid = payroll.periodid
        INNER JOIN payperiod ON payroll.periodid = payperiod.id
        INNER JOIN loan ON payroll.employeeid = loan.employeeid
        INNER JOIN loandet ON loan.id = loandet.transid AND payperiod.`month` = loandet.`month`
        AND payperiod.`year` = loandet.`year`
        WHERE
          `user`.id = 1 AND loan.loantypeid = 1 AND loandet.deleted_at IS NULL
      ) u
      SET a.paid = u.employeeloan
      WHERE
        a.`year` = u.`year`
      AND a.`month` = u.`month`
      AND a.id = u.id");


      DB::update("UPDATE loandet a
      INNER JOIN (
        SELECT
          loan.remain,
          loandet.id,
          payperiod.`month`,
          payperiod.`year`,
          payroll.insuranceloan 
        FROM
          `user`
        INNER JOIN payroll ON `user`.periodid = payroll.periodid
        INNER JOIN payperiod ON payroll.periodid = payperiod.id
        INNER JOIN loan ON payroll.employeeid = loan.employeeid
        INNER JOIN loandet ON loan.id = loandet.transid AND payperiod.`month` = loandet.`month`
        AND payperiod.`year` = loandet.`year`
        WHERE
          `user`.id = 1  AND loan.loantypeid = 4 AND loandet.deleted_at IS NULL
      ) u
      SET a.paid = u.insuranceloan
      WHERE
        a.`year` = u.`year`
      AND a.`month` = u.`month`
      AND a.id = u.id");



      DB::update("UPDATE payroll a
      INNER JOIN (
        SELECT
          derivedtbl.employeeid,
          sum(derivedtbl.remain) AS remain
        FROM
          (
            SELECT
              loan.employeeid,
              sum(loandet.amount + loandet.bonus) AS remain
            FROM
              loan
            INNER JOIN loandet ON loan.id = loandet.transid
            CROSS JOIN `user`
            WHERE
              (`user`.id = 1)
            AND loan.loantypeid = 1
            AND loandet.`month` > ".$datafilter->month."  AND loandet.`year` = ".$datafilter->year." 
            AND loandet.deleted_at IS NULL
            GROUP BY
              loan.employeeid
            UNION ALL
              SELECT
                loan.employeeid,
                sum(loandet.amount + loandet.bonus) AS remain
              FROM
                loan
              INNER JOIN loandet ON loan.id = loandet.transid
              CROSS JOIN `user`
              WHERE
                (`user`.id = 1)
              AND (loan.loantypeid = 1 OR loan.loantypeid = 4)
              AND loandet.`year` > ".$datafilter->year." 
              AND loandet.deleted_at IS NULL
              GROUP BY
                loan.employeeid
          ) AS derivedtbl
          GROUP BY derivedtbl.employeeid
      ) u
      SET a.remainemployeeloan = u.remain
      WHERE
        a.employeeid = u.employeeid
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");


      DB::update("UPDATE payroll a
      INNER JOIN (
        SELECT
          derivedtbl.employeeid,
          sum(derivedtbl.remain) as remain
        FROM
          (
            SELECT
              loan.employeeid,
              sum(loandet.amount + loandet.bonus) AS remain
            FROM
              loan
            INNER JOIN loandet ON loan.id = loandet.transid
            CROSS JOIN `user`
            WHERE
              (`user`.id = 1)
            AND loan.loantypeid = 4
            AND loandet.`month` > ".$datafilter->month."  AND loandet.`year` = ".$datafilter->year." 
            AND loandet.deleted_at IS NULL
            GROUP BY
              loan.employeeid
            UNION ALL
              SELECT
                loan.employeeid,
                sum(loandet.amount + loandet.bonus) AS remain
              FROM
                loan
              INNER JOIN loandet ON loan.id = loandet.transid
              CROSS JOIN `user`
              WHERE
                (`user`.id = 1)
              AND loan.loantypeid = 4
              AND loandet.`year` > ".$datafilter->year." 
              AND loandet.deleted_at IS NULL
              GROUP BY
                loan.employeeid
          ) AS derivedtbl
          GROUP BY derivedtbl.employeeid
      ) u
      SET a.remaininsuranceloan = u.remain
      WHERE
        a.employeeid = u.employeeid
      AND a.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");


      /* Untuk menghasilkan Total Gaji Bruto untuk di Slip = Gapok + Tunj Jabatan + UM Project + UM Tetap + Extra Pudding + Lembur Project + Lembur Tetap + Medical + Jamsostek */
      DB::update("UPDATE payroll
      SET totalbruto = ifnull(basic, 0) + ifnull(postall, 0) + ifnull(amountpajak, 0) + ifnull(mealtransall, 0) + ifnull(overtimeall, 0) + ifnull(otproject, 0) + ifnull(insentif, 0) + ifnull(medicalclaim, 0) + ifnull(jamsostek, 0) + ifnull(otherall, 0) + ifnull(mealtransproject, 0)
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");


      DB::update("UPDATE payroll
      SET potmealtransproject = CASE
      WHEN statusistimewa = 0 THEN
        mealtransproject
      ELSE
        0
      END
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");


      /* Untuk menghasilkan Total Potongan = Pinj Karyawan + Pinj Kendaraan + Pinj Rumah + Pinj Asuransi + Pot Uang Makan Mingguan + Extra Pudding + Medical + PPh21 + Pot Jamsostek */
      DB::update("UPDATE payroll
      SET totalpot = ifnull(employeeloan, 0) + ifnull(potmealtransproject, 0) + ifnull(insentif, 0) + ifnull(medicalclaim, 0) + ifnull(pph21, 0) + ifnull(potjamsostek, 0) + ifnull(insuranceloan, 0) + ifnull(vehicleloan, 0) + ifnull(homeloan, 0) + ifnull(tpotabsence, 0) + ifnull(iuranpensiunkaryawan, 0) + ifnull(otherded, 0)  + ifnull(potterlambat, 0)
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )"); 


      /* Untuk menghasilkan Total Gaji Bersih Slip = Total Bruto di kurangi Total Potongan */
      DB::update("UPDATE payroll
      SET totalnetto2 = round(
        ifnull(totalbruto, 0) - ifnull(totalpot, 0),
        0
      )
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");


      /* Untuk menghasilkan Setelah gaji bersih di potong dengan uang makan dan extra puding yg keluar perminggu. jika status istimewa 1 maka uang makan dan extra puding akan masuk di akhir bulan / penghasilan */
      DB::update("UPDATE payroll
      SET gajiymhbayar = CASE
      WHEN statusistimewa = 0 THEN
        ifnull(totalnetto, 0) - ifnull(potmealtransproject, 0) - ifnull(extrapuddingproject, 0) - ifnull(tunjmalam, 0) - ifnull(tunluarkota, 0) - ifnull(insentif, 0) - ifnull(medicalclaim, 0)
      ELSE
        CASE
      WHEN statusistimewa = 1 THEN
        ifnull(totalnetto, 0) - ifnull(medicalclaim, 0)
      END
      END
      WHERE
        (
          periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )");
  }


public function edit($id)
  {

    $sql = 'SELECT
              payperiod.id,
              payperiod.description,
              payperiod.dateperiod,
              payperiod.begindate,
              payperiod.enddate,
              payperiod.paydate,
              payperiod.`month`,
              payperiod.`year`
            FROM
              payperiod
            WHERE payperiod.id = '.$id.''; 
    $payroll = DB::table(DB::raw("($sql) as rs_sql"))->first();
    
    $sql_detail = 'SELECT
                  employee.employeename,
                  department.departmentname,
                  FORMAT(payroll.otherded,0) AS otherded,
                  FORMAT(payroll.otherall,0) AS otherall,
                  FORMAT(payroll.vehicleloan,0) AS vehicleloan,
                  FORMAT(payroll.homeloan,0) AS homeloan,
                  payroll.id
                FROM
                  payroll
                INNER JOIN employee ON payroll.employeeid = employee.id
                INNER JOIN department ON payroll.departmentid = department.id
                WHERE payroll.periodid = '.$id.''; 
    $payroll_detail = DB::table(DB::raw("($sql_detail) as rs_sql_detail"))->get(); 

    return view ('editor.payroll.form', compact('payroll', 'payroll_detail'));
  }


  public function update($id, Request $request)
  {
     
    foreach($request->input('detail') as $key => $detail_data)
    {
      $payroll_detail = Payroll::Find($key); 
      $payroll_detail->otherded = str_replace(",","",$detail_data['otherded']); 
      $payroll_detail->otherall = str_replace(",","",$detail_data['otherall']);
      // $payroll_detail->vehicleloan = str_replace(",","",$detail_data['vehicleloan']);
      // $payroll_detail->homeloan = str_replace(",","",$detail_data['homeloan']);
      $payroll_detail->save();
    } 

    return back();

    // return redirect()->action('Editor\PayrollController@index'); 
  }  


 
}
