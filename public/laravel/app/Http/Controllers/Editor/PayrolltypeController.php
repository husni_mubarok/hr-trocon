<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrolltypeRequest;
use App\Http\Controllers\Controller;
use App\Model\Payrolltype; 
use Validator;
use Response;
use App\Post;
use View;

class PayrolltypeController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'payrolltypename' => 'required',
        'dayin' => 'required',
        'dayout' => 'required',
        'signin' => 'required',
        'signout' => 'required',
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $payrolltypes = Payrolltype::all();
    return view ('editor.payrolltype.index', compact('payrolltypes'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $itemdata = Payrolltype::orderBy('payrolltypename', 'ASC')->get();

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->payrolltypename."'".')"> Hapus</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Aktif </span>';
        }else{
         return '<span class="label label-danger"> Tidak Aktif </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Payrolltype(); 
    $post->payrolltypename = $request->payrolltypename; 
    $post->dayin = $request->dayin; 
    $post->dayout = $request->dayout; 
    $post->signin = $request->signin;  
    $post->signout = $request->signout; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    $payrolltype = Payrolltype::Find($id);
    echo json_encode($payrolltype); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Payrolltype::Find($id); 
    $post->payrolltypename = $request->payrolltypename; 
    $post->dayin = $request->dayin; 
    $post->dayout = $request->dayout; 
    $post->signin = $request->signin;  
    $post->signout = $request->signout; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Payrolltype::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;    
   foreach($idkey as $key => $id)
   {
    $post = Payrolltype::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
