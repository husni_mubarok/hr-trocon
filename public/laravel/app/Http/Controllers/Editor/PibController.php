<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PibRequest;
use App\Http\Controllers\Controller;
use App\Model\Pib; 
use App\Model\Container;  
use App\Model\Userlog; 
use Carbon\Carbon;
use Validator;
use Response;
use App\Post;
use View;

class PibController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'no_pib' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $container_list = Container::all();
    $sql = 'SELECT
              pib.id,
              pib.container_id,
              pib.no_pib,
              FORMAT(pib.cost_pib,0) AS cost_pib,
              pib.description,
              pib.`status`,
              pib.created_by,
              pib.updated_by,
              pib.deleted_by,
              pib.created_at,
              pib.updated_at,
              pib.deleted_at,
              CONCAT(container.prefix, container.container_no) AS containerno
            FROM
              pib
            LEFT JOIN container ON pib.container_id = container.id
            WHERE
            pib.deleted_at IS NULL';
    $pibs = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

    return view ('editor.pib.index', compact('pibs', 'container_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
       $sql = 'SELECT
                pib.id,
                pib.container_id,
                pib.no_pib,
                FORMAT(pib.cost_pib, 0) AS cost_pib,
                pib.emkl,
                pib.description,
                DATE_FORMAT(pib.eta, "%d-%m-%Y") AS eta,
                DATE_FORMAT(pib.ata, "%d-%m-%Y") AS ata,
                DATE_FORMAT(
                  pib.original_doc,
                  "%d-%m-%Y"
                ) AS original_doc,
                DATE_FORMAT(pib.pajak_pib, "%d-%m-%Y") AS pajak_pib,
                DATE_FORMAT(pib.kt2, "%d-%m-%Y") AS kt2,
                DATE_FORMAT(pib.inspect_kt9, "%d-%m-%Y") AS inspect_kt9,
                DATE_FORMAT(pib.tlg_respon, "%d-%m-%Y") AS tlg_respon,
                pib.status_respon,
                DATE_FORMAT(
                  pib.delivery_date,
                  "%d-%m-%Y"
                ) AS delivery_date,
                pib.delivery_time,
                pib.shipping_line,
                pib.remarks,
                pib.`status`,
                pib.created_by,
                pib.updated_by,
                pib.deleted_by,
                pib.created_at,
                pib.updated_at,
                pib.deleted_at,
                CONCAT(
                  container.prefix,
                  container.container_no
                ) AS container_no,
                purchase_order.plant,
                branch.branch_name,
                store_location.store_location_name
              FROM
                pib
              LEFT JOIN container ON pib.container_id = container.id
              LEFT JOIN purchase_order ON purchase_order.container_id = container.id
              LEFT JOIN branch ON purchase_order.plant = branch.id
              LEFT JOIN store_location ON purchase_order.store_location_id = store_location.id
              WHERE
                pib.deleted_at IS NULL
              GROUP BY
                pib.id,
                pib.container_id,
                branch.branch_name,
                store_location.store_location_name';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->no_pib."', '".$itemdata->cost_pib."'".')"> Delete</a>';
      })
 
      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {

    // //log
    // $container = Container::where('id', $request->container_id)->first();

    // $post = new Userlog(); 
    // $post->description = '<b>Created PIB Data</b><br> Container: '. $container->container_name . ', <br>No PIB: ' . $request->no_pib . ', <br>Cost PIB: ' . $request->cost_pib . ', <br>EMKL: ' . $request->emkl . ', <br>Description: ' . $request->description . ', <br>ATA: ' . $request->ata . ', <br>Original Doc: ' . $request->original_doc . ', <br>Pajak PIB: ' . $request->pajak_pib . ', <br>KT2: ' . $request->kt2 . ', <br>Inspect KT9: ' . $request->inspect_kt9 . ', <br>Tlg Respon: ' . $request->tlg_respon . ', <br>Status Respon: ' . $request->status_respon . ', <br>Delivery Date: ' . $request->delivery_date . ', <br>Delivery Time: ' . $request->delivery_time;  
    // $post->user_id = Auth::id();
    // $post->date_time = Carbon::now();
    // $post->save();


    $post = new Pib(); 
    $post->container_id = $request->container_id; 
    $post->no_pib = $request->no_pib; 
    $post->cost_pib = str_replace(",", "", $request->cost_pib); 
    $post->emkl = $request->emkl; 
    $post->shipping_line = $request->shipping_line; 
    $post->description = $request->description; 
$post->eta = $request->eta;
    $post->ata = $request->ata;
    $post->original_doc = $request->original_doc;
    $post->pajak_pib = $request->pajak_pib;
    $post->kt2 = $request->kt2;
    $post->inspect_kt9 = $request->inspect_kt9;
    $post->tlg_respon = $request->tlg_respon;
    $post->status_respon = $request->status_respon;
    $post->delivery_date = $request->delivery_date;
    $post->delivery_time = $request->delivery_time;
    $post->remarks = $request->remarks;
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();



    $post = DB::insert("UPDATE user SET read_notif = 1");

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    // $pib = Pib::Find($id);
     $sql = 'SELECT
                  pib.id,
                  pib.container_id,
                  pib.no_pib,
                  FORMAT(pib.cost_pib,0) AS cost_pib,
                  pib.description,
                  pib.emkl,
                  pib.shipping_line,
pib.eta,
                  pib.ata,
                  pib.original_doc,
                  pib.pajak_pib,
                  pib.kt2,
                  pib.inspect_kt9,
                  pib.tlg_respon,
                  pib.status_respon,
                  pib.delivery_date,
                  pib.delivery_time,
                  pib.remarks, 
                  pib.`status`,
                  pib.created_by,
                  pib.updated_by,
                  pib.deleted_by,
                  pib.created_at,
                  pib.updated_at,
                  pib.deleted_at,
                  container.container_no
                FROM
                  pib
                LEFT JOIN container ON pib.container_id = container.id
                WHERE
                container.deleted_at IS NULL';
      $pib = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id)->first(); 
    echo json_encode($pib); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {


    //log

    $container = Container::where('id', $request->container_id)->first();
    $olddata = Pib::where('id', $id)->first(); 

    $post = new Userlog(); 
    $post->description = '<b>Update PIB Data</b><br> Container: '. $olddata->container_name . ' to: '. $olddata->container_name . ', <br>No PIB: ' . $olddata->no_pib . ' to: ' . $request->no_pib . ', <br>Cost PIB: ' . $olddata->cost_pib. ' to: ' . $request->cost_pib . ', <br>EMKL: ' . $olddata->emkl. ' to: ' . $request->emkl . ', <br>Description: ' . $olddata->description. ' to: ' . $request->description . ', <br>ATA: ' . $olddata->ata. ' to: ' . $request->ata . ', <br>Original Doc: ' . $olddata->original_doc. ' to: ' . $request->original_doc . ', <br>Pajak PIB: ' . $olddata->pajak_pib. ' to: ' . $request->pajak_pib . ', <br>KT2: ' . $olddata->kt2. ' to: ' . $request->kt2 . ', <br>Inspect KT9: ' . $olddata->inspect_kt9. ' to: ' . $request->inspect_kt9 . ', <br>Tgl Respon: ' . $olddata->tlg_respon. ' to: ' . $request->tlg_respon . ', <br>Status Respon: ' . $olddata->status_respon. ' to: ' . $request->status_respon . ', <br>Delivery Date: ' . $olddata->delivery_date. ' to: ' . $request->delivery_date . ', <br>Delivery Time: ' . $olddata->delivery_time. ' to: ' . $request->delivery_time . ', <br>Remark: ' . $olddata->remarks. ' to: ' . $request->remarks;  
    $post->user_id = Auth::id();
    $post->date_time = Carbon::now();
    $post->type = 'PIB';
    $post->save();

    $post = DB::insert("UPDATE user SET read_notif = 1");

    $post = Pib::Find($id); 
    $post->container_id = $request->container_id; 
    $post->no_pib = $request->no_pib; 
    $post->cost_pib = str_replace(",", "", $request->cost_pib); 
    $post->emkl = $request->emkl; 
    $post->shipping_line = $request->shipping_line; 
    $post->description = $request->description; 
$post->eta = $request->eta;
    $post->ata = $request->ata;
    $post->original_doc = $request->original_doc;
    $post->pajak_pib = $request->pajak_pib;
    $post->kt2 = $request->kt2;
    $post->inspect_kt9 = $request->inspect_kt9;
    $post->tlg_respon = $request->tlg_respon;
    $post->status_respon = $request->status_respon;
    $post->delivery_date = $request->delivery_date;
    $post->delivery_time = $request->delivery_time;
    $post->remarks = $request->remarks;
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Pib::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Pib::where('id', $id["1"])->get();
    $post = Pib::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}

public function generate(Request $request)
  {
     $user_id = Auth::id();
     $post = DB::insert("INSERT INTO pib (container_id) SELECT
                            container.id 
                          FROM
                            container 
                          WHERE
                            container.id NOT IN ( SELECT container_id FROM pib WHERE deleted_at IS NULL )");
     return response()->json($post);
  }

}
