<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Controllers\Controller;
use App\Model\Item; 
use App\Model\Itembrand; 
use App\Model\Itemcategory; 
use Validator;
use Response;
use App\Post;
use View;
use Jenssegers\Agent\Agent as Agent;


class PibreportController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'item_name' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $item_brand_list = Itembrand::all();
    $item_category_list = Itemcategory::all();

     $sql1 = 'SELECT
              item_category.item_category_name,
              `user`.id,
              `user`.grfrom,
              `user`.grto,
              `user`.item_category_id
              FROM
              `user`
              LEFT JOIN item_category
              ON `user`.item_category_id = item_category.id';

    $datafilter = DB::table(DB::raw("($sql1) as rs_sql"))->where('id', Auth::id())->first(); 

    $Agent = new Agent();
    if ($Agent->isMobile()) {
        return view ('editor.pibreport.indexmobile', compact('datafilter'));
    } else {
        return view ('editor.pibreport.index', compact('datafilter'));
    };
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
       $sql = 'SELECT
                pib.id,
                pib.container_id,
                pib.no_pib,
                FORMAT(pib.cost_pib, 0) AS cost_pib,
                pib.emkl,
                pib.description,
                DATE_FORMAT(pib.eta, "%d-%m-%Y") AS eta,
                DATE_FORMAT(pib.ata, "%d-%m-%Y") AS ata,
                DATE_FORMAT(
                  pib.original_doc,
                  "%d-%m-%Y"
                ) AS original_doc,
                DATE_FORMAT(pib.pajak_pib, "%d-%m-%Y") AS pajak_pib,
                DATE_FORMAT(pib.kt2, "%d-%m-%Y") AS kt2,
                DATE_FORMAT(pib.inspect_kt9, "%d-%m-%Y") AS inspect_kt9,
                DATE_FORMAT(pib.tlg_respon, "%d-%m-%Y") AS tlg_respon,
                pib.status_respon,
                DATE_FORMAT(
                  pib.delivery_date,
                  "%d-%m-%Y"
                ) AS delivery_date,
                pib.delivery_time,
                pib.shipping_line,
                pib.remarks,
                pib.`status`,
                pib.created_by,
                pib.updated_by,
                pib.deleted_by,
                pib.created_at,
                pib.updated_at,
                pib.deleted_at,
                CONCAT(
                  container.prefix,
                  container.container_no
                ) AS container_no,
                purchase_order.plant,
                branch.branch_name,
                store_location.store_location_name
              FROM
                pib
              LEFT JOIN container ON pib.container_id = container.id
              LEFT JOIN purchase_order ON purchase_order.container_id = container.id
              LEFT JOIN branch ON purchase_order.plant = branch.id
              LEFT JOIN store_location ON purchase_order.store_location_id = store_location.id
              WHERE
                pib.deleted_at IS NULL AND purchase_order.deleted_at IS NULL 
              GROUP BY
                pib.id,
                pib.container_id,
                branch.branch_name,
                store_location.store_location_name';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 
 
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
    $post = new Item(); 
    $post->item_category_id = $request->item_category_id; 
    $post->item_brand_id = $request->item_brand_id; 
    $post->item_code = $request->item_code;  
    $post->item_name = $request->item_name;  
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    // $item = Item::Find($id);
     $sql = 'SELECT
                item_category.item_category_name,
                item_brand.item_brand_name,
                item.item_category_id,
                item.item_brand_id,
                item.id,
                item.item_code,
                item.item_name,
                item.description,
                item.status
              FROM
                item
              LEFT JOIN item_brand ON item.item_brand_id = item_brand.id
              LEFT JOIN item_category ON item.item_category_id = item_category.id';
      $item = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id)->first(); 
    echo json_encode($item); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Item::Find($id); 
    $post->item_category_id = $request->item_category_id; 
    $post->item_brand_id = $request->item_brand_id; 
    $post->item_code = $request->item_code;  
    $post->item_name = $request->item_name;  
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Item::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   
 
   foreach($idkey as $key => $id)
   {
    // $post =  Item::where('id', $id["1"])->get();
    $post = Item::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
