<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PunishmentRequest;
use App\Http\Controllers\Controller;
use App\Model\Punishment; 
use App\Model\Employee; 
use App\Model\Sktype;
use Validator;
use Response;
use App\Post;
use View;

class PunishmentController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'punishmentno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'punishmentname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $punishments = Punishment::all();
      return view ('editor.punishment.index', compact('punishments'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  punishment.id,
                  punishment.notrans,
                  DATE_FORMAT(punishment.datetrans, "%d-%m-%Y") AS datetrans,
                  punishment.employeeid,
                  employee.employeename,
                  DATE_FORMAT(punishment.datefrom, "%d-%m-%Y") AS datefrom,
                  DATE_FORMAT(punishment.dateto, "%d-%m-%Y") AS dateto,
                  punishment.sktypeid,
                  punishment.status, 
                  punishment.description,
                  punishment.attachment 
                FROM
                  punishment
                  LEFT JOIN employee ON punishment.employeeid = employee.id
                WHERE
                  punishment.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 


        ->addColumn('action', function ($itemdata) {
          return '<a href="punishment/'.$itemdata->id.'/edit" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
        })

        ->addColumn('approval', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Approval" class="btn btn-success btn-xs" onclick="approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-check"></i> Approve</a> <a  href="javascript:void(0)" title="Approval" class="btn btn-warning btn-xs" onclick="not_approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-close"></i> Not Approve</a>';
      })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> <i class="fa fa-check"></i> Active </span>';
          }else if ($itemdata->status == 9){
           return '<span class="label label-danger"> <i class="fa fa-minus-square"></i> Cancel </span>';
        }; 

       })

        ->addColumn('attachment', function ($itemdata) {
          if ($itemdata->attachment == null) {
            return '';
          }else{
           return '<a href="../uploads/punishment/'.$itemdata->attachment.'" target="_blank"/><i class="fa fa-download"></i> Download</a>';
         };  
        })
        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function datahistory(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  punishment.id,
                  punishment.notrans,
                  punishment.datetrans,
                  employee.nik,
                  employee.employeename,
                  punishment.datefrom,
                  punishment.dateto,
                  sktype.sktypename
                FROM
                  punishment
                INNER JOIN employee ON punishment.employeeid = employee.id
                INNER JOIN sktype ON punishment.sktypeid = sktype.id,
                 `user`
                WHERE
                  `user`.id = '.Auth::id().' AND punishment.employeeid = user.employeeid
                AND
                  punishment.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 

        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function create()
    {  
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      $sktype_list = Sktype::all()->pluck('sktypename', 'id'); 

      return view ('editor.punishment.form', compact('employee_list', 'sktype_list'));
    }

    public function store(Request $request)
    { 
        DB::insert("INSERT INTO punishment (codetrans, notrans, datetrans)
                    SELECT 'MUTA',
                    IFNULL(CONCAT('PUNS','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(punishment.notrans),3))+1001,3)), CONCAT('PUNS','-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW())  
                    FROM
                    punishment
                    WHERE codetrans='PUNS'");

       $lastInsertedID = DB::table('punishment')->max('id');  

       // dd($lastInsertedID);

       $punishment = Punishment::where('id', $lastInsertedID)->first(); 
       $punishment->employeeid = $request->input('employeeid'); 
       $punishment->sktypeid = $request->input('sktypeid'); 
       $punishment->datefrom = $request->input('datefrom'); 
       $punishment->dateto = $request->input('dateto');  
       $punishment->status = 0;  
       $punishment->created_by = Auth::id();
       $punishment->save();

       if($request->attachment)
       {
        $punishment = Punishment::FindOrFail($punishment->id);
        $original_directory = "uploads/punishment/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $punishment->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $punishment->attachment);
          $punishment->save(); 
        } 
        return redirect('editor/punishment'); 
     
    }

    public function edit($id)
    {
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      $sktype_list = Sktype::all()->pluck('sktypename', 'id'); 
      $punishment = Punishment::Where('id', $id)->first();   

      // dd($punishment); 
      return view ('editor.punishment.form', compact('punishment','sktype_list', 'employee_list'));
    }

    public function update($id, Request $request)
    {
      
        $punishment = Punishment::FindOrFail($id); 
       $punishment->employeeid = $request->input('employeeid'); 
       $punishment->sktypeid = $request->input('sktypeid'); 
       $punishment->datefrom = $request->input('datefrom'); 
       $punishment->dateto = $request->input('dateto');  
       $punishment->status = 0;  
       $punishment->created_by = Auth::id();
       $punishment->save();

      if($request->attachment)
      {
        $punishment = Punishment::FindOrFail($punishment->id);
        $original_directory = "uploads/punishment/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $punishment->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $punishment->attachment);
          $punishment->save(); 
        } 
        return redirect('editor/punishment');  
      }

      public function cancel($id, Request $request)
      {
        $mutation = Punishment::Find($id); 
        $mutation->status = 9; 
        $mutation->created_by = Auth::id();
        $mutation->save(); 
      
        return response()->json($mutation); 

      }

      public function delete($id)
      {
    //dd($id);
        $post =  Punishment::Find($id);
        $post->delete(); 

        return response()->json($post); 
      }

      public function deletebulk(Request $request)
      {

       $idkey = $request->idkey;   

  //$count = count($idkey);

//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

       foreach($idkey as $key => $id)
       {
    // $post =  Document::where('id', $id["1"])->get();
        $post = Document::Find($id["1"]);
        $post->delete(); 
      }

      echo json_encode(array("status" => TRUE));

    }
  }
