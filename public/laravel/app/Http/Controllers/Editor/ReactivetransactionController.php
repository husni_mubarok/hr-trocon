<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ContainerRequest;
use App\Http\Controllers\Controller;
use App\Model\Container; 
use App\Model\Userlog; 
use Carbon\Carbon;
use Validator;
use Response;
use App\Post;
use View;
// use App\Notifications\ToDb;
use Illuminate\Notifications\Notifiable;


class ReactivetransactionController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'containername' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    return view ('editor.reactivetransaction.index');
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 

      $sql = 'SELECT
              item_category.item_category_name,
              purchase_order_detail.id,
              purchase_order.po_number,
              purchase_order.container_id,
              DATE_FORMAT(purchase_order.doc_date, "%d-%m-%Y") AS doc_date,
              DATE_FORMAT(purchase_order.gr_date, "%d-%m-%Y") AS gr_date,
              item.item_name,
              item.description,
              branch.add_modal,
              branch.branch_name AS plant,
              store_location.store_location_name,
              purchase_order.mat_doc, 
              purchase_order_detail.order_qty AS order_qty_po,
              purchase_order_detail.nett_weight AS nett_weight_po,
              purchase_order_detail.gr_qty,
              purchase_order_detail.totalmodal, 
              purchase_order_detail.kurs, 
              FORMAT(IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0),0) AS rate, 
              (IFNULL(daily_rate.rate,0) + IFNULL(daily_rate.plusrate,0)) * (purchase_order_detail.totalmodal) AS rateidr, 
              pib.cost_pib,
              purchase_order_detail.currency_id,
              currency.currency_name,
              purchase_order_detail.idr, 
              container.container_no, 
              container.container_name, 
              container.mix, 
              container.prefix
            FROM
              purchase_order
            INNER JOIN purchase_order_detail ON purchase_order_detail.purchase_order_id = purchase_order.id
            LEFT JOIN currency ON purchase_order_detail.currency_id = currency.id
            LEFT JOIN daily_rate ON CASE WHEN purchase_order_detail.currency_id IS NULL OR purchase_order_detail.currency_id = 0 THEN 1 ELSE purchase_order_detail.currency_id END = daily_rate.currency_id AND purchase_order.doc_date = daily_rate.date
            INNER JOIN item ON purchase_order_detail.item_id = item.id
            INNER JOIN item_category ON item.item_category_id = item_category.id
            INNER JOIN container ON purchase_order.container_id = container.id
            INNER JOIN branch ON purchase_order.plant = branch.id
            LEFT JOIN store_location ON purchase_order.store_location_id = store_location.id
            LEFT JOIN pib ON purchase_order.container_id = pib.container_id  
          WHERE   (pib.deleted_at IS NULL)  AND (purchase_order.deleted_at IS NULL) AND (purchase_order_detail.deleted_at IS NOT NULL)';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Reactive" onclick="delete_id('."'".$itemdata->id."'".')"> Reactive</a>';
      })
 
      ->make(true);
    } else {
      exit("No data available");
    }
  }
 

  public function delete($id)
  {
    // $post =  Container::Find($id);
    // $post->delete(); 

    $user_id = Auth::id();

    DB::update("UPDATE purchase_order_detail SET deleted_at = NULL, updated_by = $user_id WHERE id = $id");

    // return response()->json($post); 
  }
   
}
