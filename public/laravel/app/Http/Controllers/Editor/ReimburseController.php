<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ReimburseRequest;
use App\Http\Controllers\Controller;
use App\Model\Reimburse; 
use App\Model\Reimbursedetail;
use App\Model\Medicaltype;
use App\Model\Department;
use App\Model\Employee;
use App\Model\Year;
use App\Model\Month;
use App\Model\Payperiod;
use Validator;
use Response;
use App\Post;
use View;

class ReimburseController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [ 
  'transdate' => 'required'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $reimburses = Reimburse::all();
      return view ('editor.reimburse.index', compact('reimburses'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  reimburse.id,
                  reimburse.codetrans,
                  reimburse.notrans,
                  reimburse.datetrans,
                  reimburse.employeeid,
                  employee.nik,
                  employee.employeename,
                  reimburse.departmentid,
                  department.departmentname,
                  reimburse.medicaltypeid,
                  medicaltype.medicaltypename,
                  reimburse.periodid,
                  payperiod.description,
                  reimburse.patientname,
                  FORMAT(reimburse.plafond,0) AS plafond,
                  FORMAT(reimburse.used,0) AS used,
                  FORMAT(reimburse.remain,0) AS remain,
                  reimburse.remark,
                  reimburse.status
                FROM
                  reimburse
                LEFT JOIN employee ON reimburse.employeeid = employee.id
                LEFT JOIN department ON reimburse.departmentid = department.id
                LEFT JOIN medicaltype ON reimburse.medicaltypeid = medicaltype.id
                LEFT JOIN payperiod ON reimburse.periodid = payperiod.id';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata) 

        ->addColumn('check', function ($itemdata) {
          return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
        })

        ->addColumn('action', function ($itemdata) {
          return '<a href="reimburse/detail/'.$itemdata->id.'" title="'."'".$itemdata->notrans."'".'" class="btn btn-default btn-xs" onclick="edit('."'".$itemdata->id."'".')"><i class="fa fa-pencil"></i> Edit</a>';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
            return '<span class="label label-success"> Open </span>';
          }else{
           return '<span class="label label-danger"> Cancel </span>';
         };

       })
        ->make(true);
      } else {
        exit("No data available");
      }
    }
 
  public function detail($id)
    {
      $employee_list = Employee::where('tunjangankesehatan', 'Reimburse')->get()->pluck('employeename', 'id'); 
      $reimbursetype_list = Medicaltype::all()->pluck('medicaltypename', 'id'); 
      $month_list = Month::all()->pluck('monthname', 'id'); 
      $year_list = Year::all()->pluck('yearname', 'yearname'); 
      $department_list = Department::all()->pluck('departmentname', 'id'); 
      $period_list = Payperiod::orderBy('id', 'DESC')->get()->pluck('description', 'id'); 

      $sql_day_work = 'SELECT
                          DATEDIFF(
                            reimburse.datetrans,
                            employee.joindate
                          ) AS day_work
                        FROM
                          employee
                        INNER JOIN reimburse ON employee.id = reimburse.employeeid
                        WHERE
                          reimburse.id = '.$id.'';  
      $day_work = DB::table(DB::raw("($sql_day_work) as rs_sql"))->first();  

      // $reimburse = Reimburse::Find($id); 
      $sql = 'SELECT
                reimburse.id,
                reimburse.codetrans,
                reimburse.notrans,
                reimburse.datetrans,
                reimburse.employeeid,
                reimburse.departmentid,
                reimburse.departmentcode,
                reimburse.medicaltypeid,
                reimburse.periodid,
                reimburse.patientname,
                FORMAT(reimburse.plafond,0) AS plafond,
                FORMAT(reimburse.used,0) AS used,
                FORMAT(reimburse.remain,0) AS remain,
                FORMAT(reimburse.claimamount,0) AS claimamount,
                reimburse.remain AS remain_val,
                reimburse.remark,
                reimburse.`status`
              FROM
                reimburse
              WHERE reimburse.id ='.$id.'';
      $reimburse = DB::table(DB::raw("($sql) as rs_sql"))->first(); 
      return view ('editor.reimburse.form', compact('employee_list', 'reimburse', 'reimbursetype_list', 'month_list', 'year_list', 'department_list', 'period_list', 'day_work'));
    }

    public function slip($id)
    {
       $reimburse = \DB::select(\DB::raw("
           SELECT
              reimburse.id,
              reimburse.codetrans,
              reimburse.notrans,
              reimburse.datetrans,
              reimburse.departmentid,
              reimburse.departmentcode,
              reimburse.datefrom,
              reimburse.dateto,
              employee.employeename,
              department.departmentname,
              reimburse.timefrom,
              reimburse.timeto,
              reimburse.reimbursein,
              reimburse.reimburseout,
              reimburse.periodid,
              reimburse.remark,
              reimburse.planwork,
              reimburse.actualwork,
              reimburse.location
            FROM
              reimburse
            INNER JOIN reimbursedet ON reimburse.id = reimbursedet.transid
            INNER JOIN employee ON reimbursedet.employeeid = employee.id
            INNER JOIN department ON employee.departmentid = department.id WHERE reimburse.id = ".$id."

        "));


      return view ('editor.reimburse.slip', compact('reimburse'));
    }



    public function store(Request $request)
    { 

      $userid= Auth::id();
      $codetrans = $request->input('codetrans'); 

       DB::insert("INSERT INTO reimburse (codetrans, notrans, datetrans, created_by, created_at)
      SELECT '".$codetrans."',
      IFNULL(CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(RIGHT(reimburse.notrans,5)),5))+100001,5)), CONCAT('".$codetrans."','-',DATE_FORMAT(NOW(), '%d%m%y'),'00001')), DATE(NOW()), '".$userid."', DATE(NOW()) 
      FROM
      reimburse
      WHERE codetrans= '".$codetrans."'");

      $lastInsertedID = DB::table('reimburse')->max('id');
      //return redirect()->action('Editor\ReimburseController@edit', $lastInsertedID->id);
      return redirect('editor/reimburse/detail/'.$lastInsertedID.''); 
    }

    public function saveheader($id, Request $request)
    { 
        $post = Reimburse::Find($id); 
        $post->medicaltypeid = $request->medicaltypeid; 
        $post->employeeid = $request->employeeid; 
        $post->departmentid = $request->departmentid; 
        $post->patientname = $request->patientname; 
        $post->plafond = $request->plafond; 
        $post->used = $request->used; 
        $post->remain = $request->remain; 
        $post->periodid = $request->periodid; 
        $post->remark = $request->remark;  
        $post->updated_by = Auth::id();
        $post->save();

        return response()->json($post); 
      
    }


    public function generate($id, Request $request)
    { 

      $post = Reimburse::Find($id); 
      $post->medicaltypeid = $request->medicaltypeid; 
      $post->employeeid = $request->employeeid; 
      $post->departmentid = $request->departmentid; 
      $post->patientname = $request->patientname; 
      $post->plafond = $request->plafond; 
      $post->used = $request->used; 
      $post->remain = $request->remain; 
      $post->periodid = $request->periodid; 
      $post->remark = $request->remark;  
      $post->updated_by = Auth::id();
      $post->save();

      DB::update("UPDATE reimburse
                  INNER JOIN (
                    SELECT
                      sum(reimbursedet.amount) AS amount
                    FROM
                      reimbursedet
                    WHERE
                      reimbursedet.transid = ".$id."
                  ) AS reimbursedet
                  SET reimburse.claimamount = reimbursedet.amount
                  WHERE
                    reimburse.id = ".$id."");

      $sql_year_work = 'SELECT
                    YEAR (reimburse.datetrans) - YEAR (employee.joindate) AS year_work
                  FROM
                    employee
                  INNER JOIN reimburse ON employee.id = reimburse.employeeid
                  WHERE
                    reimburse.id = '.$id.'';  
      $year_work = DB::table(DB::raw("($sql_year_work) as rs_sql"))->first();  


      $sql_plafond = 'SELECT
                        employee.id,
                        employee.basic,
                        MONTH (employee.joindate) AS dateemp
                      FROM
                        employee
                      INNER JOIN reimburse ON employee.id = reimburse.employeeid
            WHERE
              reimburse.id = '.$id.'';  
      $plafond = DB::table(DB::raw("($sql_plafond) as rs_sql"))->first(); 


      if($year_work->year_work >= 183 && $year_work->year_work <= 136){
        $plafond_val = $plafond->basic * (12 - ($plafond->dateemp) + 1) / 12;
      }else{
        $plafond_val = $plafond->basic;
      };

 
      DB::update("UPDATE reimburse
                  INNER JOIN (
                    SELECT
                      YEAR (reimburse.datetrans) AS `year`,
                      sum(reimbursedet.amount) AS used,
                      reimburse.employeeid
                    FROM
                      reimburse
                    INNER JOIN reimbursedet ON reimburse.id = reimbursedet.transid
                    INNER JOIN employee ON reimburse.employeeid = employee.id
                    WHERE reimbursedet.deleted_at IS NULL
                    GROUP BY
                      YEAR (reimburse.datetrans),
                      reimburse.employeeid
                  ) AS reimbursedet
                  SET reimburse.plafond = ".$plafond_val.", reimburse.used = reimbursedet.used, reimburse.remain = ".$plafond_val." - reimbursedet.used
                  WHERE
                    reimburse.id = ".$id." AND reimburse.employeeid = reimbursedet.employeeid AND YEAR(reimburse.datetrans) = reimbursedet.year");

    }

    public function update($id, Request $request)
    { 
        $post = Reimburse::Find($id); 
        $post->medicaltypeid = $request->input('medicaltypeid'); 
        $post->employeeid = $request->input('employeeid'); 
        $post->patientname = $request->input('patientname'); 
        $post->plafond = str_replace(",", "", $request->input('plafond')); 
        $post->used = str_replace(",", "", $request->input('used')); 
        $post->remain = str_replace(",", "", $request->input('remain')); 
        $post->periodid = str_replace(",", "", $request->input('periodid')); 
        $post->remark = $request->input('remark');   
        $post->updated_by = Auth::id();
        $post->save();

        return redirect('editor/reimburse');   
      
    }


     public function savedetail($id, Request $request)
    { 
        $post = new Reimbursedetail;
        $post->transid = $id;  
        $post->description = $request->description;   
        $post->dateclaim = $request->dateclaim;   
        $post->noref = $request->noref;   
        $post->amount = $request->amount;   
        $post->save();

        return response()->json($post); 
      
    }

    public function deletedet($id)
    {
    //dd($id);
      $post =  Reimbursedetail::where('idtransdet', $id);
      $post->delete(); 

      return response()->json($post); 
    }

    public function deletebulk(Request $request)
    {

     $idkey = $request->idkey;   

     foreach($idkey as $key => $id)
     {
        // $post =  Reimburse::where('id', $id["1"])->get();
        $post = Reimburse::Find($id["1"]);
        $post->delete(); 
      }

    echo json_encode(array("status" => TRUE));

  }
  public function datadetail(Request $request, $id)
    {   
     
    if($request->ajax()){ 

        $sql = 'SELECT
                  reimbursedet.idtransdet AS id,
                  reimbursedet.idtrans,
                  reimbursedet.transid,
                  reimbursedet.description,
                  reimbursedet.dateclaim,
                  FORMAT(reimbursedet.amount,0) AS amount,
                  reimbursedet.noref
                FROM
                  reimbursedet
                WHERE reimbursedet.transid = '.$id.' AND reimbursedet.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

       return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a  href="javascript:void(0)" title="Delete" class="btn btn-danger btn-xs" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->description."'".')"><i class="fa fa-trash"></i></a>';
      })
   
      ->make(true);
    } else {
      exit("No data available");
    }
    }
}
