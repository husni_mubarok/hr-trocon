<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportabsencedeductionController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    return view ('editor.reportabsencededuction.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                payperiod.description AS period,
                department.departmentname,
                employee.employeename AS employee,
                CONCAT(
                  mealtrandet.potabsence1,
                  " = ",
                  mealtrandet.potabsence1 * mealtrandet.ratepotabsence
                ) AS pot_absence1,
                CONCAT(
                  mealtrandet.potabsence2,
                  " = ",
                  mealtrandet.potabsence2 * mealtrandet.ratepotabsence
                ) AS pot_absence2,
                CONCAT(
                  mealtrandet.potabsence3,
                  " = ",
                  mealtrandet.potabsence3 * mealtrandet.ratepotabsence
                ) AS pot_absence3,
                CONCAT(
                  mealtrandet.potabsence4,
                  " = ",
                  mealtrandet.potabsence4 * mealtrandet.ratepotabsence
                ) AS pot_absence4,
                CONCAT(
                  mealtrandet.potabsence5,
                  " = ",
                  mealtrandet.potabsence5 * mealtrandet.ratepotabsence
                ) AS pot_absence5,
                mealtrandet.potabsence AS pot_absence,
                mealtrandet.ratepotabsence AS rate,
                mealtrandet.tpotabsence AS total
              FROM
                USER
              INNER JOIN mealtran ON ifnull(
                user.periodid,
                mealtran.periodid
              ) = mealtran.periodid
              AND ifnull(
                user.departmentid,
                mealtran.departmentid
              ) = mealtran.departmentid
              INNER JOIN mealtrandet ON mealtran.id = mealtrandet.transid
              AND ifnull(
                user.employeeid,
                mealtrandet.employeeid
              ) = mealtrandet.employeeid
              INNER JOIN payperiod ON mealtran.periodid = payperiod.id
              INNER JOIN department ON mealtran.departmentid = department.id
              INNER JOIN employee ON mealtrandet.employeeid = employee.id
              WHERE
                (user.id ='.$userid.')';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
