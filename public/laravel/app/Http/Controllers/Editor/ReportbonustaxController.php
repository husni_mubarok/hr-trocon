<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportbonustaxController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::orderBy('id', 'DESC')->get()->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    return view ('editor.reportbonustax.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function printreport()
  { 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.employeename AS employee,
                position.positionname AS position,
                payperiod.description AS periodegaji,
                bonus.description AS periodebonus,
                bonusdet.taxstatus AS taxstatus,
                bonusdet.basic AS gajipokok,
                bonusdet.`value` AS perkalian,
                bonusdet.tbonus AS totalbonus,
                bonusdet.kumulatif1 AS kumulatif,
                bonusdet.lastbruto1 AS lastbruto,
                bonusdet.tlastbruto2 AS tbrutodgnbonus,
                bonusdet.tlastbruto1 AS tbrutotnpbonus,
                bonusdet.biayajab2 AS biayajabdgnbonus,
                bonusdet.biayajab1 AS biayajabtnpbonus,
                bonusdet.nettoptkp2 AS nettoptkpdgnbonus,
                bonusdet.nettoptkp1 AS nettoptkptnpbonus,
                bonusdet.ptkp2 AS ptkpdgnbonus,
                bonusdet.ptkp1 AS ptkptnpbonus,
                bonusdet.pph212 AS pph21dgnbonus,
                bonusdet.pph211 AS pph21tanpabonus,
                bonusdet.pph21 AS pph21,
                bonusdet.potpinjaman AS potpinjaman,
                bonusdet.netto AS netto
              FROM
                bonus
              INNER JOIN `user` ON bonus.periodid = ifnull(
                `user`.periodid,
                bonus.periodid
              )
              AND bonus.departmentid = ifnull(
                `user`.departmentid,
                bonus.departmentid
              )
              INNER JOIN bonusdet ON ifnull(
                `user`.employeeid,
                bonusdet.employeeid
              ) = bonusdet.employeeid
              AND bonus.id = bonusdet.transid
              INNER JOIN employee ON bonusdet.employeeid = employee.id
              INNER JOIN position ON employee.positionid = position.id
              INNER JOIN payperiod ON bonus.periodid = payperiod.id
              WHERE
                (`user`.id = 1)
              ORDER BY
                position.positionlevel';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportbonusthrtax.printreport', compact('datafilter', 'itemdata'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.employeename AS employee,
                position.positionname AS position,
                payperiod.description AS periodegaji,
                bonus.description AS periodebonus,
                bonusdet.taxstatus AS taxstatus,
                bonusdet.basic AS gajipokok,
                bonusdet.`value` AS perkalian,
                bonusdet.tbonus AS totalbonus,
                bonusdet.kumulatif1 AS kumulatif,
                bonusdet.lastbruto1 AS lastbruto,
                bonusdet.tlastbruto2 AS tbrutodgnbonus,
                bonusdet.tlastbruto1 AS tbrutotnpbonus,
                bonusdet.biayajab2 AS biayajabdgnbonus,
                bonusdet.biayajab1 AS biayajabtnpbonus,
                bonusdet.nettoptkp2 AS nettoptkpdgnbonus,
                bonusdet.nettoptkp1 AS nettoptkptnpbonus,
                bonusdet.ptkp2 AS ptkpdgnbonus,
                bonusdet.ptkp1 AS ptkptnpbonus,
                bonusdet.pph212 AS pph21dgnbonus,
                bonusdet.pph211 AS pph21tanpabonus,
                bonusdet.pph21,
                bonusdet.potpinjaman AS potpinjaman,
                bonusdet.netto
              FROM
                bonus
              INNER JOIN `user` ON bonus.periodid = ifnull(
                `user`.periodid,
                bonus.periodid
              )
              AND bonus.departmentid = ifnull(
                `user`.departmentid,
                bonus.departmentid
              )
              INNER JOIN bonusdet ON ifnull(
                `user`.employeeid,
                bonusdet.employeeid
              ) = bonusdet.employeeid
              AND bonus.id = bonusdet.transid
              INNER JOIN employee ON bonusdet.employeeid = employee.id
              INNER JOIN position ON employee.positionid = position.id
              INNER JOIN payperiod ON bonus.periodid = payperiod.id
              WHERE
                (`user`.id = 1)
              ORDER BY
                position.positionlevel';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
