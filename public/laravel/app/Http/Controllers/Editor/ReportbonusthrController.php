<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportbonusthrController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    // $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $payperiod_list = Payperiod::orderBy('id', 'desc')->where('status', '0')->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    return view ('editor.reportbonusthr.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function printreport()
  { 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.employeename AS employee,
                position.positionname AS position,
                payperiod.description AS periodegaji,
                thr.description AS periodethr,
                thrdet.taxstatus AS taxstatus,
                thrdet.basic AS gajipokok,
                thrdet.`value` AS perkalian,
                thrdet.tthr AS totalthr,
                thrdet.pph21,
                thrdet.netto
              FROM
                thr
              INNER JOIN `user` ON thr.periodid = ifnull(
                `user`.periodid,
                thr.periodid
              )
              AND thr.departmentid = ifnull(
                `user`.departmentid,
                thr.departmentid
              )
              INNER JOIN thrdet ON ifnull(
                `user`.employeeid,
                thrdet.employeeid
              ) = thrdet.employeeid
              AND thr.id = thrdet.transid
              INNER JOIN employee ON thrdet.employeeid = employee.id
              INNER JOIN position ON employee.positionid = position.id
              INNER JOIN payperiod ON thr.periodid = payperiod.id
              WHERE
                (`user`.id = 1)
              ORDER BY
                position.positionlevel';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportbonusthr.printreport', compact('datafilter', 'itemdata'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.employeename AS employee,
                position.positionname AS position,
                payperiod.description AS periodegaji,
                thr.description AS periodethr,
                thrdet.taxstatus AS taxstatus,
                thrdet.basic AS gajipokok,
                thrdet.`value` AS perkalian,
                thrdet.tthr AS totalthr,
                thrdet.pph21,
                thrdet.netto
              FROM
                thr
              INNER JOIN `user` ON thr.periodid = ifnull(
                `user`.periodid,
                thr.periodid
              )
              AND thr.departmentid = ifnull(
                `user`.departmentid,
                thr.departmentid
              )
              INNER JOIN thrdet ON ifnull(
                `user`.employeeid,
                thrdet.employeeid
              ) = thrdet.employeeid
              AND thr.id = thrdet.transid
              INNER JOIN employee ON thrdet.employeeid = employee.id
              INNER JOIN position ON employee.positionid = position.id
              INNER JOIN payperiod ON thr.periodid = payperiod.id
              WHERE
                (`user`.id = 1)
              ORDER BY
                position.positionlevel';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
