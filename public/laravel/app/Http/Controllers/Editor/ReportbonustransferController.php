<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportbonustransferController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    return view ('editor.reportbonustransfer.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function printreport()
  { 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.employeename AS employeename,
                employee.bankan AS accountname,
                employee.bankname AS bankname,
                employee.bankaccount AS norekeningbca,
                bonusdet.netto
              FROM
                bonus
              INNER JOIN `user` ON bonus.periodid = ifnull(
                `user`.periodid,
                bonus.periodid
              )
              INNER JOIN bonusdet ON bonus.id = bonusdet.transid
              INNER JOIN employee ON bonusdet.employeeid = employee.id
              INNER JOIN position ON employee.positionid = position.id
              INNER JOIN payperiod ON bonus.periodid = payperiod.id
              INNER JOIN department ON employee.departmentid = department.id
              WHERE
                (`user`.id = 1)
              AND (
                employee.bankaccount IS NOT NULL
              )
              ORDER BY
                position.positionlevel';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportbonustransfer.printreport', compact('datafilter', 'itemdata'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.employeename AS employeename,
                employee.bankan AS accountname,
                employee.bankname AS bankname,
                employee.bankaccount AS norekeningbca,
                FORMAT(bonusdet.netto,0) AS netto
              FROM
                bonus
              INNER JOIN `user` ON bonus.periodid = CASE WHEN 
                `user`.periodid IS NULL OR `user`.periodid = "" THEN bonus.periodid ELSE
                `user`.periodid END
              INNER JOIN bonusdet ON bonus.id = bonusdet.transid
              INNER JOIN employee ON bonusdet.employeeid = employee.id
              INNER JOIN position ON employee.positionid = position.id
              INNER JOIN payperiod ON bonus.periodid = payperiod.id
              INNER JOIN department ON employee.departmentid = department.id
              WHERE
                (`user`.id = 1)
              AND (
                employee.bankaccount IS NOT NULL
              )
              ORDER BY
                position.positionlevel';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
