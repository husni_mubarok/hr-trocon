<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportemployeemasterController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::orderBy('id', 'DESC')->get()->pluck('description', 'id');
    return view ('editor.reportemployeemaster.index', compact('department_list','payperiod_list','datafilter'));
  }

  public function printreport()
  { 
      $userid = Auth::id();
      $sql = 'SELECT
                department.departmentname,
                employee.employeename,
                employee.joindate,
                position.positionname,
                position.positionlevel,
                payroll.basic,
                payroll.mealtransall,
                payroll.overtimeall,
                payroll.jamsostek,
                payroll.postall AS tunjkeahlian,
                payroll.medicalall,
                payroll.postall,
                payroll.netto
              FROM
                payroll
              INNER JOIN employee ON payroll.employeeid = employee.id
              INNER JOIN department ON payroll.departmentid = department.id
              LEFT JOIN position ON employee.positionid = position.id
              INNER JOIN user ON payroll.periodid = user.periodid
              AND payroll.departmentid = ifnull(
                user.departmentid,
                payroll.departmentid
              )
              LEFT OUTER JOIN department AS dpt  ON payroll.departmentid = dpt.id
              WHERE
                (user.id = '.$userid.')';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('positionlevel', 'asc')->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportemployeemaster.printreport', compact('datafilter', 'itemdata'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                department.departmentname,
                employee.employeename,
                employee.joindate,
                position.positionname,
                payroll.basic,
                ifnull(payroll.postall, 0) AS tunj_keahlian,
                payroll.mealtransall,
                payroll.overtimeall,
                payroll.jamsostek,
                payroll.tunjkeahlian,
                payroll.medicalall,
                payroll.netto
              FROM
                payroll
              INNER JOIN employee ON payroll.employeeid = employee.id
              INNER JOIN department ON payroll.departmentid = department.id
              LEFT JOIN position ON employee.positionid = position.id
              INNER JOIN user ON payroll.periodid = user.periodid
              AND payroll.departmentid = ifnull(
                user.departmentid,
                payroll.departmentid
              )
              LEFT OUTER JOIN department AS dpt  ON payroll.departmentid = dpt.id
              WHERE
                (user.id = '.$userid.')';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
