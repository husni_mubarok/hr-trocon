<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportinsentiveController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    return view ('editor.reportinsentive.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function printreport()
  { 
      $userid = Auth::id();
      $sql = 'SELECT
                payperiod.description AS period,
                department.departmentname,
                employee.employeename AS employee,
                position.positionlevel,
                mealtrandet.insentif1 AS day1,
                mealtrandet.insentif2 AS day2,
                mealtrandet.insentif3 AS day3,
                mealtrandet.insentif4 AS day4,
                mealtrandet.insentif5 AS day5,
                mealtrandet.insentif1 + mealtrandet.insentif2 + mealtrandet.insentif3 + mealtrandet.insentif4 + mealtrandet.insentif5 AS tday,
                mealtrandet.perday AS rate,
                mealtrandet.potabsence AS tpotabsence,
                  (
                    mealtrandet.insentif1 + mealtrandet.insentif2 + mealtrandet.insentif3 + mealtrandet.insentif4 + mealtrandet.insentif5
                  ) - mealtrandet.potabsence AS amount
              FROM
                mealtran
              INNER JOIN mealtrandet ON mealtran.id = mealtrandet.transid
              INNER JOIN employee ON mealtrandet.employeeid = employee.id
              LEFT JOIN position ON employee.positionid = position.id
              INNER JOIN department ON employee.departmentid = department.id
              INNER JOIN `user` ON CASE
              WHEN `user`.periodid = 0
              OR `user`.periodid = "" THEN
                mealtran.periodid
              ELSE
                `user`.periodid
              END = mealtran.periodid
              AND CASE
              WHEN `user`.departmentid = 0
              OR `user`.departmentid = "" THEN
                employee.departmentid
              ELSE
                `user`.departmentid
              END = employee.departmentid 
              INNER JOIN payperiod ON mealtran.periodid = payperiod.id
              WHERE
                (`user`.id = '.$userid.') AND employee.status = 0';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('positionlevel', 'asc')->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportinsentive.printreport', compact('datafilter', 'itemdata'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                payperiod.description AS period,
                department.departmentname,
                employee.employeename AS employee,
                FORMAT(mealtrandet.insentif1, 0) AS day1,
                FORMAT(mealtrandet.insentif2, 0) AS day2,
                FORMAT(mealtrandet.insentif3, 0) AS day3,
                FORMAT(mealtrandet.insentif4, 0) AS day4,
                FORMAT(mealtrandet.insentif5, 0) AS day5,
                FORMAT(mealtrandet.addinsentif, 0) AS addinsentif,
                FORMAT(
                  mealtrandet.insentif1 + mealtrandet.insentif2 + mealtrandet.insentif3 + mealtrandet.insentif4 + mealtrandet.insentif5,
                  0
                ) AS tday,
                FORMAT(mealtrandet.perday, 0) AS rate,
                FORMAT(mealtrandet.potabsence,0) AS tpotabsence,
                FORMAT( 
                  (
                    mealtrandet.insentif1 + mealtrandet.insentif2 + mealtrandet.insentif3 + mealtrandet.insentif4 + mealtrandet.insentif5 + mealtrandet.addinsentif
                  ) - mealtrandet.potabsence,
                  0
                ) AS amount
              FROM
                mealtran
              INNER JOIN mealtrandet ON mealtran.id = mealtrandet.transid
              INNER JOIN employee ON mealtrandet.employeeid = employee.id
              INNER JOIN department ON employee.departmentid = department.id
              INNER JOIN `user` ON CASE
              WHEN `user`.periodid = 0
              OR `user`.periodid = "" THEN
                mealtran.periodid
              ELSE
                `user`.periodid
              END = mealtran.periodid
              AND CASE
              WHEN `user`.departmentid = 0
              OR `user`.departmentid = "" THEN
                employee.departmentid
              ELSE
                `user`.departmentid
              END = employee.departmentid 
              INNER JOIN payperiod ON mealtran.periodid = payperiod.id
              WHERE
                (`user`.id = '.$userid.') AND employee.status = 0';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
