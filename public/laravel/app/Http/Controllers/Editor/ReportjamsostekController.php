<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportjamsostekController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    return view ('editor.reportjamsostek.index', compact('department_list','payperiod_list','datafilter'));
  }

  public function printreport()
  { 
      $userid = Auth::id();
      $sql = 'SELECT * FROM (SELECT
                employee.id AS employeeid,
                department.departmentname,
                employee.employeename AS employee,
                position.positionlevel,
                DATE_FORMAT(employee.dateemployee, "%d-%m-%Y") AS joinemployee,
                payroll.jamsostek AS jamsostek
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN user ON payroll.periodid = user.periodid
              AND payroll.departmentid = ifnull(
                user.departmentid,
                payroll.departmentid
              )
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              LEFT OUTER JOIN position ON employee.positionid = position.id
              WHERE employee.status = 0 OR employee.status IS NULL AND
                (user.id = '.$userid.')) AS derived';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('positionlevel', 'asc')->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportjamsostek.printreport', compact('datafilter', 'itemdata'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT * FROM (SELECT
                employee.id AS employeeid,
                department.departmentname,
                employee.employeename AS employee,
                DATE_FORMAT(employee.dateemployee, "%d-%m-%Y") AS joinemployee,
                FORMAT(payroll.jamsostek,0) AS jamsostek
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN user ON payroll.periodid = user.periodid
              AND payroll.departmentid = ifnull(
                user.departmentid,
                payroll.departmentid
              )
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              LEFT OUTER JOIN position ON employee.positionid = position.id
              WHERE employee.status = 0 OR employee.status IS NULL AND 
                (user.id = '.$userid.')) AS derived';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('employeeid', 'asc')->get(); 

      // dd($itemdata);

      return Datatables::of($itemdata)  



      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
