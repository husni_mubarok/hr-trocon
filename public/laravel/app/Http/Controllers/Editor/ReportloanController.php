<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportloanController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::where('status', '0')->orderBy('id', 'desc')->get()->pluck('description', 'id');
    return view ('editor.reportloan.index', compact('department_list','payperiod_list','datafilter'));
  }

  public function printreport()
  { 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.id AS employeeid,
                department.departmentname,
                employee.employeename,
                position.positionname AS jabatan,
                position.positionlevel AS positionlevel,
                ifnull(payroll.insuranceloan, 0) AS insuranceloan,
                ifnull(payroll.employeeloan, 0) AS employeeloan,
                ifnull(payroll.employeeloanbonus, 0) AS employeeloanbonus,
                ifnull(payroll.vehicleloan, 0) AS vehicleloan,
                ifnull(payroll.homeloan, 0) AS pinjaman_rumah,
                ifnull(payroll.insuranceloan, 0) + ifnull(payroll.employeeloan, 0) AS total_pinjaman,
                ifnull(
                  payroll.remainemployeeloan,
                  0
                ) AS sisa_pinjaman_personal,
                ifnull(
                  payroll.remainvehicleloan,
                  0
                ) AS sisa_pinjaman_kendaraan,
                ifnull(
                  payroll.remaininsuranceloan,
                  0
                ) AS sisa_pinjaman_asuransi,
                ifnull(payroll.remainhomeloan, 0) AS sisa_pinjaman_rumah
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN user ON payroll.periodid = user.periodid
              AND payroll.departmentid = ifnull(
                user.departmentid,
                payroll.departmentid
              )
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              LEFT OUTER JOIN position ON employee.positionid = position.id 
              WHERE ifnull(payroll.insuranceloan, 0) + ifnull(payroll.employeeloan, 0) > 0 AND
              -- WHERE ifnull(payroll.remainemployeeloan, 0) > 0 AND
                (user.id = '.$userid.')
              ORDER BY
                position.positionlevel';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('positionlevel', 'asc')->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportloan.printreport', compact('datafilter', 'itemdata'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.id AS employeeid,
                department.departmentname,
                employee.employeename,
                position.positionname AS jabatan,
                FORMAT(ifnull(payroll.insuranceloan, 0), 0) AS insuranceloan,
                FORMAT(ifnull(payroll.employeeloan, 0), 0) AS employeeloan,
                FORMAT(ifnull(payroll.employeeloanbonus, 0), 0) AS employeeloanbonus,
                FORMAT(ifnull(payroll.vehicleloan, 0), 0) AS vehicleloan,
                FORMAT(ifnull(payroll.homeloan, 0), 0) AS pinjaman_rumah,
                FORMAT(ifnull(payroll.insuranceloan, 0) + ifnull(payroll.employeeloan, 0),0) AS total_pinjaman,
                FORMAT(ifnull(
                  payroll.remainemployeeloan,
                  0
                ), 0) AS sisa_pinjaman_personal,
                FORMAT(ifnull(
                  payroll.remainvehicleloan,
                  0
                ), 0) AS sisa_pinjaman_kendaraan,
                FORMAT(ifnull(
                  payroll.remaininsuranceloan,
                  0
                ), 0) AS sisa_pinjaman_asuransi,
                FORMAT(ifnull(payroll.remainhomeloan, 0), 0) AS sisa_pinjaman_rumah
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN user ON payroll.periodid = user.periodid
              AND payroll.departmentid = ifnull(
                user.departmentid,
                payroll.departmentid
              )
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              LEFT OUTER JOIN position ON employee.positionid = position.id
              WHERE ifnull(payroll.insuranceloan, 0) + ifnull(payroll.employeeloan, 0) > 0 AND
              -- WHERE ifnull(payroll.remainemployeeloan, 0) > 0 AND
                (user.id = '.$userid.')
              ORDER BY
                position.positionlevel';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
