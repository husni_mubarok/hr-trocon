<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportmealController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::where('status', '0')->get()->pluck('description', 'id');
    // $employee_list = Employee::all()->pluck('employeename', 'id');
    $sql = 'SELECT "" AS id, "All Employee" AS employeename
        UNION ALL
        SELECT
          id,
          employeename
        FROM employee';
    $employee_list = DB::table(DB::raw("($sql) as rs_sql"))->get()->pluck('employeename', 'id'); 
    return view ('editor.reportmeal.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function printreport()
  { 
      $userid = Auth::id();
      $sql = 'SELECT
                payperiod.description AS period,
                department.departmentname,
                employee.employeename AS employee,
                position.positionlevel,
                mealtrandet.um1 AS day1,
                mealtrandet.um2 AS day2,
                mealtrandet.um3 AS day3,
                mealtrandet.um4 AS day4,
                mealtrandet.um5 AS day5,
                mealtrandet.addmeal AS addmeal,
                
                  mealtrandet.um1 + mealtrandet.um2 + mealtrandet.um3 + mealtrandet.um4 + mealtrandet.um5 AS tday,
                mealtrandet.perday AS rate,
                mealtrandet.potabsence AS tpotabsence,
                 
                  (
                    mealtrandet.um1 + mealtrandet.um2 + mealtrandet.um3 + mealtrandet.um4 + mealtrandet.um5 + mealtrandet.addmeal
                  ) - mealtrandet.potabsence AS amount
              FROM
                mealtran
              INNER JOIN mealtrandet ON mealtran.id = mealtrandet.transid
              INNER JOIN employee ON mealtrandet.employeeid = employee.id
              INNER JOIN position ON employee.positionid = position.id
              INNER JOIN department ON employee.departmentid = department.id
              INNER JOIN `user` ON CASE
              WHEN `user`.periodid = 0
              OR `user`.periodid = "" THEN
                mealtran.periodid
              ELSE
                `user`.periodid
              END = mealtran.periodid
              AND CASE
              WHEN `user`.departmentid = 0
              OR `user`.departmentid = "" THEN
                employee.departmentid
              ELSE
                `user`.departmentid
              END = employee.departmentid
              AND CASE
              WHEN `user`.employeeid = 0
              OR `user`.employeeid = "" THEN
                employee.id
              ELSE
                `user`.employeeid
              END = mealtrandet.employeeid
              INNER JOIN payperiod ON mealtran.periodid = payperiod.id
              WHERE
                (`user`.id = '.$userid.')';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('positionlevel', 'asc')->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportmeal.printreport', compact('datafilter', 'itemdata'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                payperiod.description AS period,
                department.departmentname,
                employee.employeename AS employee,
                FORMAT(mealtrandet.um1, 0) AS day1,
                FORMAT(mealtrandet.um2, 0) AS day2,
                FORMAT(mealtrandet.um3, 0) AS day3,
                FORMAT(mealtrandet.um4, 0) AS day4,
                FORMAT(mealtrandet.um5, 0) AS day5,
                FORMAT(mealtrandet.addmeal, 0) AS addmeal,
                FORMAT(
                  mealtrandet.um1 + mealtrandet.um2 + mealtrandet.um3 + mealtrandet.um4 + mealtrandet.um5,
                  0
                ) AS tday,
                FORMAT(mealtrandet.perday, 0) AS rate,
                FORMAT(mealtrandet.potabsence,0) AS tpotabsence,
                FORMAT( 
                  (
                    mealtrandet.um1 + mealtrandet.um2 + mealtrandet.um3 + mealtrandet.um4 + mealtrandet.um5
                  ) - mealtrandet.potabsence,
                  0
                ) AS amount
              FROM
                mealtran
              INNER JOIN mealtrandet ON mealtran.id = mealtrandet.transid
              INNER JOIN employee ON mealtrandet.employeeid = employee.id
              INNER JOIN department ON employee.departmentid = department.id
              INNER JOIN `user` ON CASE
              WHEN `user`.periodid = 0
              OR `user`.periodid = "" THEN
                mealtran.periodid
              ELSE
                `user`.periodid
              END = mealtran.periodid
              AND CASE
              WHEN `user`.departmentid = 0
              OR `user`.departmentid = "" THEN
                employee.departmentid
              ELSE
                `user`.departmentid
              END = employee.departmentid
              AND CASE
              WHEN `user`.employeeid = 0
              OR `user`.employeeid = "" THEN
                employee.id
              ELSE
                `user`.employeeid
              END = mealtrandet.employeeid
              INNER JOIN payperiod ON mealtran.periodid = payperiod.id
              WHERE
                (`user`.id = '.$userid.')';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
