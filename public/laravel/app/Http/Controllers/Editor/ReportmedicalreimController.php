<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportoutcityallowanceController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    return view ('editor.reportoutcityallowance.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                payperiod.description AS period,
                department.departmentname,
                employee.employeename AS employee,
                CONCAT(
                  mealtrandet.dic1,
                  " = ",
                  mealtrandet.dic1 * mealtrandet.ratemealoutcity
                ) AS dk1,
                CONCAT(
                  mealtrandet.dic2,
                  " = ",
                  mealtrandet.dic2 * mealtrandet.ratemealoutcity
                ) AS dk2,
                CONCAT(
                  mealtrandet.dic3,
                  " = ",
                  mealtrandet.dic3 * mealtrandet.ratemealoutcity
                ) AS dk3,
                CONCAT(
                  mealtrandet.dic4,
                  " = ",
                  mealtrandet.dic4 * mealtrandet.ratemealoutcity
                ) AS dk4,
                CONCAT(
                  mealtrandet.dic5,
                  " = ",
                  mealtrandet.dic5 * mealtrandet.ratemealoutcity
                ) AS dk5,
                mealtrandet.dayoutcity AS t_dk,
                mealtrandet.ratemealoutcity AS rate,
                mealtrandet.tundalamkota AS total
              FROM
                USER
              INNER JOIN mealtran ON ifnull(
                USER .periodid,
                mealtran.periodid
              ) = mealtran.periodid
              AND ifnull(
                USER .departmentid,
                mealtran.departmentid
              ) = mealtran.departmentid
              INNER JOIN mealtrandet ON mealtran.id = mealtrandet.transid
              AND ifnull(
                USER .employeeid,
                mealtrandet.employeeid
              ) = mealtrandet.employeeid
              INNER JOIN payperiod ON mealtran.periodid = payperiod.id
              INNER JOIN department ON mealtran.departmentid = department.id
              INNER JOIN employee ON mealtrandet.employeeid = employee.id
              WHERE
                (USER .id = '.$userid.')';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
