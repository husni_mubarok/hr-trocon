<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportovertimeController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::orderBy('id', 'desc')->pluck('description', 'id');

    $sql = 'SELECT "" AS id, "All Employee" AS employeename
            UNION ALL
            SELECT
              id,
              employeename
            FROM employee';
    $employee_list = DB::table(DB::raw("($sql) as rs_sql"))->get()->pluck('employeename', 'id'); 

    // $employee_list = Employee::all()->pluck('employeename', 'id');
    return view ('editor.reportovertime.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function printreport()
  { 
      $userid = Auth::id();
      $sql = 'SELECT
              payperiod.description AS period,
              department.departmentname,
              position.positionlevel,
              employee.employeename AS employee,
              FORMAT(overtimerequestdet.ot1,0) AS ot1, 
              FORMAT(overtimerequestdet.ot2,0) AS ot2, 
              FORMAT(overtimerequestdet.ot3,0) AS ot3,  
              FORMAT(overtimerequestdet.ot4,0) AS ot4,
              FORMAT(overtimerequestdet.ot5,0) AS ot5,
              overtimerequestdet.rateovertime AS rate,
              overtimerequestdet.ot1 + overtimerequestdet.ot2 + overtimerequestdet.ot3 + overtimerequestdet.ot4 + overtimerequestdet.ot5 +overtimerequestdet.otcorrection AS overtime,
              (overtimerequestdet.ot1 + overtimerequestdet.ot2 + overtimerequestdet.ot3 + overtimerequestdet.ot4 + overtimerequestdet.ot5 + overtimerequestdet.otcorrection) * overtimerequestdet.rateovertime AS amount
            FROM
              overtimerequestdet
            INNER JOIN overtimerequest ON overtimerequestdet.transid = overtimerequest.id
            INNER JOIN employee ON overtimerequestdet.employeeid = employee.id
            INNER JOIN position ON employee.positionid = position.id
            INNER JOIN department ON employee.departmentid = department.id
            INNER JOIN `user` ON overtimerequest.periodid = CASE
            WHEN `user`.periodid = 0
            OR `user`.periodid = "" THEN
              overtimerequest.periodid
            ELSE
              `user`.periodid
            END
            AND employee.departmentid = CASE
            WHEN `user`.departmentid = 0
            OR `user`.departmentid = "" THEN
              employee.departmentid
            ELSE
              `user`.departmentid
            END
            AND overtimerequestdet.employeeid = CASE
            WHEN `user`.employeeid = 0
            OR `user`.employeeid = "" THEN
              overtimerequestdet.employeeid
            ELSE
              `user`.employeeid
            END
            INNER JOIN payperiod ON overtimerequest.periodid = payperiod.id
            WHERE
              (`user`.id = '.$userid.')';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('positionlevel', 'asc')->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportovertime.printreport', compact('datafilter', 'itemdata'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
              payperiod.description AS period,
              department.departmentname,
              employee.employeename AS employee, 
                FORMAT(overtimerequestdet.ot1,0) AS ot1, 
                FORMAT(overtimerequestdet.ot2,0) AS ot2, 
                FORMAT(overtimerequestdet.ot3,0) AS ot3,  
                FORMAT(overtimerequestdet.ot4,0) AS ot4,
                FORMAT(overtimerequestdet.ot5,0) AS ot5,
                FORMAT(overtimerequestdet.otcorrection,0) AS otcorrection,
                FORMAT(overtimerequestdet.addovertime,0) AS addovertime,
              FORMAT(overtimerequestdet.rateovertime,0) AS rate,
              FORMAT(overtimerequestdet.ot1 + overtimerequestdet.ot2 + overtimerequestdet.ot3 + overtimerequestdet.ot4 + overtimerequestdet.ot5+ overtimerequestdet.otcorrection,2) AS overtime,
              FORMAT(((overtimerequestdet.ot1 + overtimerequestdet.ot2 + overtimerequestdet.ot3 + overtimerequestdet.ot4 + overtimerequestdet.ot5 + overtimerequestdet.otcorrection) * overtimerequestdet.rateovertime) + overtimerequestdet.addovertime,0) AS amount
            FROM
              overtimerequestdet
            INNER JOIN overtimerequest ON overtimerequestdet.transid = overtimerequest.id
            INNER JOIN employee ON overtimerequestdet.employeeid = employee.id
            INNER JOIN department ON employee.departmentid = department.id
            INNER JOIN `user` ON overtimerequest.periodid = CASE
            WHEN `user`.periodid = 0
            OR `user`.periodid = "" THEN
              overtimerequest.periodid
            ELSE
              `user`.periodid
            END
            AND employee.departmentid = CASE
            WHEN `user`.departmentid = 0
            OR `user`.departmentid = "" THEN
              employee.departmentid
            ELSE
              `user`.departmentid
            END
            AND overtimerequestdet.employeeid = CASE
            WHEN `user`.employeeid = 0
            OR `user`.employeeid = "" THEN
              overtimerequestdet.employeeid
            ELSE
              `user`.employeeid
            END
            INNER JOIN payperiod ON overtimerequest.periodid = payperiod.id
            WHERE
              (`user`.id = '.$userid.')';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
