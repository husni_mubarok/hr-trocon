<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportpayrollController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::orderBy('id', 'desc')->where('status', '0')->pluck('description', 'id');
    return view ('editor.reportpayroll.index', compact('department_list','payperiod_list','datafilter'));
  }

  public function printreport()
  { 
    $userid = Auth::id();
    $sql = 'SELECT
                employee.id AS employeeid,
                employee.employeename AS employee,
                position.positionname AS position,
                position.positionlevel AS positionlevel,
                payroll.basic,
                payroll.mealtransall,
                payroll.overtimeall,
                ifnull(payroll.postall, 0) AS tunj_keahlian,
                payroll.jamsostek AS tunj_jamsostek,
                ifnull(payroll.medicalclaim, 0) AS medicalclaim,
                payroll.mealtransproject AS uang_makan,
                payroll.otproject AS lembur_proyek,
                ifnull(
                  payroll.extrapuddingproject,
                  0
                ) + ifnull(payroll.tunjmalam, 0) + ifnull(payroll.tundalamkota, 0) + ifnull(payroll.tunluarkota, 0) + ifnull(payroll.insentif, 0) AS tm_tdk_tlk_instpd,
                ifnull(payroll.otherall, 0) AS otherall,
                payroll.bruto AS gaji_bruto,
                ifnull(payroll.biayajab, 0) AS biaya_jabatan,
                ifnull(payroll.tht, 0) AS tht,
                payroll.netto AS netto,
                ifnull(payroll.ptkp, 0) AS ptkp,
                ifnull(payroll.pkp, 0) AS pkp,
                round(ifnull(payroll.pph21, 0), 0) AS pph21,
                ifnull(payroll.potjamsostek, 0) AS pot_jamsostek,
                ifnull(payroll.otherded, 0) AS otherded,
                ifnull(payroll.tunjangankesehatan, 0) AS tunjangankesehatan,
                ifnull(
                  payroll.iuranpensiunkaryawan,
                  0
                ) AS pot_pensiun,
                payroll.totalnetto AS gaji_bersih,
                payroll.gajiymhbayar AS mh_dibayar,
                ifnull(payroll.employeeloan, 0) + ifnull(payroll.insuranceloan, 0) AS loan,
                payroll.totalnetto2,
                department.departmentname
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN user ON payroll.periodid = user.periodid
              AND payroll.departmentid = ifnull(
                user.departmentid,
                payroll.departmentid
              )
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              LEFT OUTER JOIN position ON employee.positionid = position.id
              WHERE
                (user.id = '.$userid.')
              AND (employee.status = 0)
              ORDER BY
                position.positionlevel asc';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('positionlevel', 'asc')->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportpayroll.printreport', compact('itemdata','datafilter'));
  }

  public function printslip()
  { 
    $userid = Auth::id();
    $sql = 'SELECT
                employee.id AS employeeid,
                employee.employeename AS employee,
                position.positionname AS position,
                position.positionlevel AS positionlevel,
                payroll.basic,
                payroll.mealtransall,
                payroll.overtimeall,
                ifnull(payroll.postall, 0) AS tunj_keahlian,
                payroll.jamsostek AS tunj_jamsostek,
                ifnull(payroll.medicalclaim, 0) AS tunj_kesehatan,
                payroll.mealtransproject AS uang_makan,
                payroll.otproject AS lembur_proyek,
                ifnull(
                  payroll.extrapuddingproject,
                  0
                ) + ifnull(payroll.tunjmalam, 0) + ifnull(payroll.tundalamkota, 0) + ifnull(payroll.tunluarkota, 0) + ifnull(payroll.insentif, 0) AS tm_tdk_tlk_instpd,
                payroll.bruto AS gaji_bruto,
                ifnull(payroll.biayajab, 0) AS biaya_jabatan,
                ifnull(payroll.tht, 0) AS tht,
                payroll.netto AS netto,
                ifnull(payroll.ptkp, 0) AS ptkp,
                ifnull(payroll.pkp, 0) AS pkp,
                round(ifnull(payroll.pph21, 0), 0) AS pph21,
                ifnull(payroll.tunjangankesehatan, 0) AS tunjangankesehatan,
                ifnull(
                  payroll.iuranpensiunkaryawan,
                  0
                ) AS pot_pensiun,
                payroll.totalnetto AS gaji_bersih,
                payroll.gajiymhbayar AS mh_dibayar,
                ifnull(payroll.employeeloan, 0) + ifnull(payroll.insuranceloan, 0) AS loan,
                payroll.totalnetto2,
                department.departmentname
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN user ON payroll.periodid = user.periodid
              AND payroll.departmentid = ifnull(
                user.departmentid,
                payroll.departmentid
              )
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              LEFT OUTER JOIN position ON employee.positionid = position.id
              WHERE
                (user.id = '.$userid.')
              AND (employee.status = 0)
              ORDER BY
                employee.id asc';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('employeeid', 'asc')->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportpayroll.printslip', compact('itemdata','datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.id AS employeeid,
                employee.employeename AS employee,
                position.positionname AS position,
                position.positionlevel AS positionlevel,
                payroll.basic,
                payroll.mealtransall,
                payroll.overtimeall,
                ifnull(payroll.postall, 0) AS tunj_keahlian,
                payroll.jamsostek AS tunj_jamsostek,
                ifnull(payroll.medicalclaim, 0) AS medicalclaim,
                ifnull(payroll.tunjangankesehatan, 0) AS tunjangankesehatan,
                payroll.mealtransproject AS uang_makan,
                payroll.otproject AS lembur_proyek,
                ifnull(
                  payroll.extrapuddingproject,
                  0
                ) + ifnull(payroll.tunjmalam, 0) + ifnull(payroll.tundalamkota, 0) + ifnull(payroll.tunluarkota, 0) + ifnull(payroll.insentif, 0) AS tm_tdk_tlk_instpd,
                ifnull(payroll.otherall, 0) AS otherall,
                payroll.bruto AS gaji_bruto,
                ifnull(payroll.biayajab, 0) AS biaya_jabatan,
                ifnull(payroll.tht, 0) AS tht,
                payroll.netto AS netto,
                ifnull(payroll.ptkp, 0) AS ptkp,
                ifnull(payroll.pkp, 0) AS pkp,
                round(ifnull(payroll.pph21, 0), 0) AS pph21,
                ifnull(payroll.potjamsostek, 0) AS pot_jamsostek,
                ifnull(payroll.otherded, 0) AS otherded,
                ifnull(payroll.potterlambat, 0) AS potterlambat,
                ifnull(
                  payroll.iuranpensiunkaryawan,
                  0
                ) AS pot_pensiun,
                payroll.totalnetto AS gaji_bersih,
                payroll.gajiymhbayar AS mh_dibayar,
                ifnull(payroll.employeeloan, 0) AS employeeloan,
                ifnull(payroll.insuranceloan, 0) AS insuranceloan, 
                ifnull(payroll.employeeloan, 0) + ifnull(payroll.insuranceloan, 0) AS loan,
                payroll.totalnetto2,
                department.departmentname
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN user ON payroll.periodid = user.periodid
              AND payroll.departmentid = ifnull(
                user.departmentid,
                payroll.departmentid
              )
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              LEFT OUTER JOIN position ON employee.positionid = position.id
              WHERE
                (user.id = '.$userid.')
              AND (employee.status = 0)
              ORDER BY
                employee.id desc';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('employeeid', 'desc')->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 

}
