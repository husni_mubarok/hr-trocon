<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportpayrollslipController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::orderBy('id', 'desc')->where('status', '0')->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    return view ('editor.reportpayrollslip.index', compact('department_list','payperiod_list','employee_list','datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.id AS employeeid,
                employee.employeename,
                position.positionname AS jabatan,
                payperiod.description AS periode,
                payperiod.paydate AS pembayaran,
                employee.gol AS grade,
                payroll.taxstatus AS `status`,
                NULL AS expr6,
                NULL AS expr9,
                NULL AS penghasilan,
                payroll.basic AS gaji_pokok,
                payroll.postall AS tunjangan_jabatan,
                ifnull(payroll.amountpajak, 0) + ifnull(payroll.mealtransall, 0) AS uang_makan_transport,
                ifnull(payroll.overtimeall, 0) + ifnull(payroll.otproject, 0) AS lembur,
                ifnull(
                  payroll.extrapuddingproject,
                  0
                ) + ifnull(payroll.tunjmalam, 0) + ifnull(payroll.tunluarkota, 0) + ifnull(payroll.insentif, 0) AS tlm_tlk,
                ifnull(payroll.medicalclaim, 0) AS pengobatan,
                payroll.jamsostek AS tunj_jamsostek,
                payroll.totalbruto AS gaji_bruto,
                NULL AS potongan,
                ifnull(payroll.employeeloan, 0) AS pinjaman_karyawan,
                ifnull(payroll.vehicleloan, 0) AS kendaraan,
                ifnull(payroll.tpotabsence, 0) AS pot_absence,
                ifnull(
                  payroll.potmealtransproject,
                  0
                ) AS pot_uang_makan,
                ifnull(
                  payroll.extrapuddingproject,
                  0
                ) + ifnull(payroll.tunjmalam, 0) + ifnull(payroll.tundalamkota, 0) + ifnull(payroll.tunluarkota, 0) + ifnull(payroll.insentif, 0) AS pot_t_malam,
                ifnull(payroll.medicalclaim, 0) AS pot_pengobatan,
                round(ifnull(payroll.pph21, 0), 0) AS pph_21,
                round(
                  ifnull(payroll.potjamsostek, 0),
                  0
                ) AS pot_jamsostek,
                round(
                  ifnull(
                    payroll.iuranpensiunkaryawan,
                    0
                  ),
                  0
                ) AS pot_pensiun,
                ifnull(payroll.otherded, 0) AS pot_lainnya,
                round(payroll.totalpot, 0) AS jum_potongan,
                round(payroll.totalnetto2, 0) AS gaji_bersih,
                ifnull(
                  payroll.remainemployeeloan,
                  0
                ) AS sisa_hutang_pinjaman,
                ifnull(payroll.jhtperusahaan, 0) AS tht,
                round(
                  ifnull(
                    payroll.iuranpensiunperusahaan,
                    0
                  ),
                  0
                ) AS tunj_pensiun
              FROM
                payroll
              INNER JOIN employee ON payroll.employeeid = employee.id
              INNER JOIN `user` ON payroll.periodid = `user`.periodid
              AND CASE
              WHEN `user`.departmentid = 0
              OR `user`.departmentid = "" THEN
                employee.departmentid
              ELSE
                `user`.departmentid
              END = employee.departmentid
              AND CASE
              WHEN `user`.employeeid = 0
              OR `user`.employeeid = "" THEN
                employee.id
              ELSE
                `user`.employeeid
              END = employee.id
              INNER JOIN payperiod ON payroll.periodid = payperiod.id
              LEFT OUTER JOIN position ON employee.positionid = position.id
              WHERE
                (`user`.id = '.$userid.')
              AND (employee.`status` = 0)
              ORDER BY
                position.positionlevel';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
