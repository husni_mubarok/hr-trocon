<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Year;
use App\Model\Department;
use App\Model\Employee;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportreimburseController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    $year_list = Year::all()->pluck('yearname', 'yearname');
    return view ('editor.reportreimburse.index', compact('department_list','payperiod_list','employee_list','datafilter', 'year_list'));
  }

  public function printreport()
  { 
      $userid = Auth::id();
      $sql = 'SELECT
                reimburse.id,
                reimburse.notrans,
                DATE_FORMAT(reimburse.datetrans, "%d-%m-%Y") AS datetrans,
                payperiod.description AS period,
                employee.employeename,
                department.departmentname,
                DATE_FORMAT(reimbursedet.dateclaim, "%d-%m-%Y") AS dateref,
                medicaltype.medicaltypename,
                FORMAT(reimburse.claimamount,0) AS claimamount,
                reimbursedet.description
              FROM
                reimburse
              INNER JOIN payperiod ON reimburse.periodid = payperiod.id
              INNER JOIN employee ON reimburse.employeeid = employee.id
              INNER JOIN department ON employee.departmentid = department.id
              INNER JOIN reimbursedet ON reimbursedet.idtrans = reimburse.id
              LEFT JOIN medicaltype ON reimburse.medicaltypeid = medicaltype.id
              INNER JOIN `user` ON CASE
              WHEN `user`.year = 0
              OR `user`.year = "" THEN
                DATE_FORMAT(reimburse.datetrans, "%Y")
              ELSE
                `user`.year
              END = DATE_FORMAT(reimburse.datetrans, "%Y")
              AND CASE
              WHEN `user`.departmentid = 0
              OR `user`.departmentid = "" THEN
                employee.departmentid
              ELSE
                `user`.departmentid
              END = employee.departmentid
              AND CASE
              WHEN `user`.employeeid = 0
              OR `user`.employeeid = "" THEN
                employee.id
              ELSE
                `user`.employeeid
              END = employee.id
              WHERE
                (`user`.id = '.$userid.') AND reimbursedet.deleted_at IS NULL';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname,
                          user.year
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportreimburse.printreport', compact('datafilter', 'itemdata'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                reimburse.id,
                reimburse.notrans,
                DATE_FORMAT(reimburse.datetrans, "%d-%m-%Y") AS datetrans,
                payperiod.description AS period,
                employee.employeename,
                department.departmentname,
                DATE_FORMAT(reimbursedet.dateclaim, "%d-%m-%Y") AS dateref,
                medicaltype.medicaltypename,
                FORMAT(reimburse.claimamount,0) AS claimamount,
                reimbursedet.description
              FROM
                reimburse
              INNER JOIN payperiod ON reimburse.periodid = payperiod.id
              INNER JOIN employee ON reimburse.employeeid = employee.id
              INNER JOIN department ON employee.departmentid = department.id
              INNER JOIN reimbursedet ON reimbursedet.idtrans = reimburse.id
              LEFT JOIN medicaltype ON reimburse.medicaltypeid = medicaltype.id
              INNER JOIN `user` ON CASE
              WHEN `user`.year = 0
              OR `user`.year = "" THEN
                DATE_FORMAT(reimburse.datetrans, "%Y")
              ELSE
                `user`.year
              END = DATE_FORMAT(reimburse.datetrans, "%Y")
              AND CASE
              WHEN `user`.departmentid = 0
              OR `user`.departmentid = "" THEN
                employee.departmentid
              ELSE
                `user`.departmentid
              END = employee.departmentid
              AND CASE
              WHEN `user`.employeeid = 0
              OR `user`.employeeid = "" THEN
                employee.id
              ELSE
                `user`.employeeid
              END = employee.id
              WHERE
                (`user`.id = '.$userid.') AND reimbursedet.deleted_at IS NULL';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
