<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Employee;
use App\Model\Year;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReportreimburseremainController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    $employee_list = Employee::all()->pluck('employeename', 'id');
    $year_list = Year::all()->pluck('yearname', 'yearname');
    return view ('editor.reportreimburseremain.index', compact('department_list','payperiod_list','employee_list','datafilter','year_list'));
  }

  public function printreport()
  { 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.id,
                employee.employeename,
                employee.joindate,
                CASE
              WHEN YEAR (employee.joindate) = `user`.`year` THEN
                0
              ELSE
                CASE
              WHEN YEAR (employee.joindate) + 1 < `user`.`year` THEN
                employee.basic
              ELSE
                (
                  12 - MONTH (employee.joindate)
                ) / 12 * employee.basic
              END
              END AS plafond,
               IFNULL(DERIVEDTBL.claimamount, 0) AS claimamount,
               CASE
              WHEN YEAR (employee.joindate) = `user`.`year` THEN
                0
              ELSE
                CASE
              WHEN YEAR (employee.joindate) + 1 < `user`.`year` THEN
                employee.basic
              ELSE
                (
                  12 - MONTH (employee.joindate)
                ) / 12 * employee.basic
              END
              END - IFNULL(DERIVEDTBL.claimamount, 0) AS remain
              FROM
                (
                  SELECT
                    reimburse.employeeid,
                    SUM(reimbursedet.amount) AS claimamount,
                    payperiod.`year`
                  FROM
                    reimburse
                  INNER JOIN reimbursedet ON reimburse.id = reimbursedet.transid
                  INNER JOIN payperiod ON reimburse.periodid = payperiod.id
                  WHERE reimbursedet.deleted_at IS NULL
                  GROUP BY
                    reimburse.employeeid,
                    payperiod.`year`
                ) DERIVEDTBL
              INNER JOIN employee ON employee.id = DERIVEDTBL.employeeid 
              RIGHT JOIN `user` ON CASE
              WHEN `user`.departmentid = 0
              OR `user`.departmentid = "" THEN
                employee.departmentid
              ELSE
                `user`.departmentid
              END = employee.departmentid
              AND
              CASE
              WHEN `user`.year = 0
              OR `user`.year = "" THEN
                DERIVEDTBL.year
              ELSE
                `user`.year
              END = DERIVEDTBL.year
              WHERE
                (`user`.id = 1)';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname,
                          user.year
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reportreimburseremain.printreport', compact('datafilter', 'itemdata'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.id,
                employee.employeename,
                employee.joindate,
                CASE
              WHEN YEAR (employee.joindate) = `user`.`year` THEN
                0
              ELSE
                CASE
              WHEN YEAR (employee.joindate) + 1 < `user`.`year` THEN
                employee.basic
              ELSE
                (
                  12 - MONTH (employee.joindate)
                ) / 12 * employee.basic
              END
              END AS plafond,
               IFNULL(DERIVEDTBL.claimamount, 0) AS claimamount,
               CASE
              WHEN YEAR (employee.joindate) = `user`.`year` THEN
                0
              ELSE
                CASE
              WHEN YEAR (employee.joindate) + 1 < `user`.`year` THEN
                employee.basic
              ELSE
                (
                  12 - MONTH (employee.joindate)
                ) / 12 * employee.basic
              END
              END - IFNULL(DERIVEDTBL.claimamount, 0) AS remain
              FROM
                (
                  SELECT
                    reimburse.employeeid,
                    SUM(reimbursedet.amount) AS claimamount,
                    payperiod.`year`
                  FROM
                    reimburse
                  INNER JOIN reimbursedet ON reimburse.id = reimbursedet.transid
                  INNER JOIN payperiod ON reimburse.periodid = payperiod.id
                  WHERE reimbursedet.deleted_at IS NULL
                  GROUP BY
                    reimburse.employeeid,
                    payperiod.`year`
                ) DERIVEDTBL
              INNER JOIN employee ON employee.id = DERIVEDTBL.employeeid 
              RIGHT JOIN `user` ON CASE
              WHEN `user`.departmentid = 0
              OR `user`.departmentid = "" THEN
                employee.departmentid
              ELSE
                `user`.departmentid
              END = employee.departmentid
              AND
              CASE
              WHEN `user`.year = 0
              OR `user`.year = "" THEN
                DERIVEDTBL.year
              ELSE
                `user`.year
              END = DERIVEDTBL.year
              WHERE
                (`user`.id = 1)';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('id', 'asc')->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
