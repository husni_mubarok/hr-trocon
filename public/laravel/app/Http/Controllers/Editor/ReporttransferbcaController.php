<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\PayrollRequest;
use App\Http\Controllers\Controller;
use App\Model\Payroll; 
use App\Model\Payperiod;
use App\Model\Department;
use Validator;
use Response;
use App\Post;
use View;
use App\User;

class ReporttransferbcaController extends Controller
{
  
  public function index()
  { 
    $datafilter = User::where('id', Auth::id())->first();
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all()->pluck('description', 'id');
    return view ('editor.reporttransferbca.index', compact('department_list','payperiod_list','datafilter'));
  }

   public function printreport()
  { 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.id AS employeeid,
                employee.employeename AS employee_name,
                employee.bankan AS account_name,
                employee.bankname AS bank_name,
                employee.bankaccount AS rek_no_bca,
                round(payroll.totalnetto2, 0) AS gaji_bersih,
                position.positionlevel
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN user ON payroll.periodid = user.periodid
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              LEFT OUTER JOIN position ON employee.positionid = position.id
              WHERE
                (user.id = '.$userid.')
              AND (
                employee.bankaccount IS NOT NULL
              ) AND  employee.bankaccount != ""
              AND employee.status = 0
              ORDER BY
                position.positionlevel';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->orderBy('positionlevel', 'asc')->get(); 

      $sqldatafilter = 'SELECT
                          payperiod.description AS period,
                          department.departmentname
                        FROM
                          `user`
                        LEFT JOIN department ON `user`.departmentid = department.id
                        LEFT JOIN payperiod ON `user`.periodid = payperiod.id
                        WHERE user.id = '.$userid.'';
      $datafilter = DB::table(DB::raw("($sqldatafilter) as rs_sql"))->first(); 

    return view ('editor.reporttransferbca.printreport', compact('datafilter', 'itemdata'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                employee.id AS employeeid,
                employee.employeename AS employee_name,
                employee.bankan AS account_name,
                employee.bankname AS bank_name,
                employee.bankaccount AS rek_no_bca,
                round(payroll.totalnetto2, 0) AS gaji_bersih
              FROM
                employee
              INNER JOIN payroll ON employee.id = payroll.employeeid
              INNER JOIN user ON payroll.periodid = user.periodid
              LEFT OUTER JOIN department ON payroll.departmentid = department.id
              LEFT OUTER JOIN position ON employee.positionid = position.id
              WHERE
                (user.id = '.$userid.')
              AND (
                employee.bankaccount IS NOT NULL
              ) AND  employee.bankaccount != ""
              AND employee.status = 0
              ORDER BY
                position.positionlevel';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata)  

      ->make(true);
    } else {
      exit("No data available");
    }
  } 
}
