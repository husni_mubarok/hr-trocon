<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\TransferrequestRequest;
use App\Http\Controllers\Controller;
use App\Model\Transferrequest; 
use Validator;
use Response;
use App\Post;
use View;

class ReporttransferrequestController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'perioddesc' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $transferrequests = Transferrequest::all();
    return view ('editor.reporttransferrequest.index', compact('transferrequests'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $itemdata = Transferrequest::orderBy('id', 'DESC')->get();

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->transferrequestname."'".')"> Hapus</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('actionprint', function ($itemdata) {
        return '<a href="reporttransferrequest/'.$itemdata->id.'/print" title="Detail" target="_blank" class="btn btn-primary btn-xs btn-flat"><i class="fa fa-print"></i> Print</a>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Aktif </span>';
        }else{
         return '<span class="label label-danger"> Tidak Aktif </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Transferrequest(); 
    $post->cekno = $request->cekno; 
    $post->rekno = $request->rekno; 
    $post->perioddesc = $request->perioddesc; 
    $post->remark = $request->remark; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    $transferrequest = Transferrequest::Find($id);
    echo json_encode($transferrequest); 
  }

  public function reportprint($id)
  {
    $transferrequest = Transferrequest::Find($id);

    return view ('editor.reporttransferrequest.print', compact('transferrequest'));
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Transferrequest::Find($id); 
    $post->cekno = $request->cekno; 
    $post->rekno = $request->rekno; 
    $post->perioddesc = $request->perioddesc; 
    $post->remark = $request->remark; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Transferrequest::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Reporttransferrequest::where('id', $id["1"])->get();
    $post = Transferrequest::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
