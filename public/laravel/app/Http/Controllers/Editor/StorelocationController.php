<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\StorelocationRequest;
use App\Http\Controllers\Controller;
use App\Model\Storelocation; 
use App\Model\Userlog; 
use Carbon\Carbon;
use Validator;
use Response;
use App\Post;
use View;

class StorelocationController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'storelocationname' => 'required|min:2'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $storelocations = Storelocation::all();
    return view ('editor.storelocation.index', compact('storelocations'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $itemdata = Storelocation::orderBy('store_location_name', 'ASC')->get();

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->store_location_name."'".')"> Delete</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };
       })

       ->addColumn('lsstatus', function ($itemdata) {
        if ($itemdata->ls_status == 1) {
          return '<span class="label label-success"><i class="fa fa-check"></i> </span>';
        }else{
         return '<span class="label label-danger"><i class="fa fa-close"></i> </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Storelocation(); 
    $post->store_location_name = $request->storelocationname; 
    $post->store_location_code = $request->storelocationcode; 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->ls_status = $request->ls_status;
    $post->created_by = Auth::id();
    $post->save();

    $post = new Userlog(); 
    $post->description = '<b>Created Store Location Data</b><br> Name: '. $request->storelocationname . ', <br>Description: ' . $request->description;  
    $post->user_id = Auth::id();
    $post->date_time = Carbon::now();
    $post->type = 'Store Location';
    $post->save();

    $post = DB::insert("UPDATE user SET read_notif = 1");

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    $storelocation = Storelocation::Find($id);
    echo json_encode($storelocation); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {

    //log
    $olddata = Storelocation::where('id', $id)->first(); 

    $post = new Userlog(); 
    $post->description = '<b>Update Store Location Data</b><br> Name: '. $olddata->store_location_name . ' to: '. $request->storelocationname . ', <br>Description: ' . $olddata->description. ' to: ' . $request->description;  
    $post->user_id = Auth::id();
    $post->date_time = Carbon::now();
    $post->type = 'Store Location';
    $post->save();

    $post = Storelocation::Find($id); 
    $post->store_location_name = $request->storelocationname; 
    $post->store_location_code = $request->storelocationcode; 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->ls_status = $request->ls_status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Storelocation::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Storelocation::where('id', $id["1"])->get();
    $post = Storelocation::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
