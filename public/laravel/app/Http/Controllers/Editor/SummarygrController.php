<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Controllers\Controller;
use App\Model\Item; 
use App\Model\Itembrand; 
use App\Model\Itemcategory; 
use App\Model\Week;
use Validator;
use Response;
use App\Post;
use View;
use Jenssegers\Agent\Agent as Agent;


class SummarygrController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'item_name' => 'required'
    ]; 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {

    $item_brand_list = Itembrand::all();
    $item_category_list = Itemcategory::all();
    $week_list = Week::all();


     $sql1 = 'SELECT
              item_category.item_category_name,
              `user`.id,
              `user`.grfrom,
              `user`.grto,
              `user`.item_category_id,
              `user`.check_filter,
              `user`.week
              FROM
              `user`
              LEFT JOIN item_category
              ON `user`.item_category_id = item_category.id';

    $datafilter = DB::table(DB::raw("($sql1) as rs_sql"))->where('id', Auth::id())->first(); 

    $Agent = new Agent();
    if ($Agent->isMobile()) {
        return view ('editor.summarygr.indexmobile', compact('datafilter', 'week_list'));
    } else {
        return view ('editor.summarygr.index', compact('datafilter', 'week_list'));
    };
     
    
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
       $sql = 'SELECT
                CONCAT(container.prefix, container.container_no) AS container_no,
                pib.no_pib,
                FORMAT(pib.cost_pib,0) AS cost_pib,
                pib.emkl,
                pib.shipping_line,
                pib.description,
                DATE_FORMAT(pib.eta, "%d-%m-%Y") AS eta,
                DATE_FORMAT(pib.ata, "%d-%m-%Y") AS ata,
                DATE_FORMAT(pib.original_doc, "%d-%m-%Y") AS original_doc,
                DATE_FORMAT(pib.pajak_pib, "%d-%m-%Y") AS pajak_pib,
                DATE_FORMAT(pib.kt2, "%d-%m-%Y") AS kt2,
                DATE_FORMAT(pib.inspect_kt9, "%d-%m-%Y") AS inspect_kt9,
                DATE_FORMAT(pib.tlg_respon, "%d-%m-%Y") AS tlg_respon,
                pib.status_respon,
                DATE_FORMAT(pib.delivery_date, "%d-%m-%Y") AS delivery_date,
                pib.delivery_time,
                pib.remarks, 
                FORMAT(
                  SUM(
                    purchase_order_detail.order_qty
                  ),
                  0
                ) AS po_qty, 
                FORMAT(
                  SUM(
                    purchase_order_detail.gr_qty
                  ),
                  0
                ) AS gr_qty 
              FROM
                purchase_order
              INNER JOIN purchase_order_detail ON purchase_order_detail.purchase_order_id = purchase_order.id
              INNER JOIN container ON purchase_order.container_id = container.id
              INNER JOIN pib ON purchase_order.container_id = pib.container_id, user
              WHERE (CASE WHEN user.check_filter = 1 THEN (YEARWEEK(purchase_order.doc_date) = CONCAT(YEAR(NOW()),user.week)) ELSE (purchase_order.doc_date BETWEEN user.grfrom AND user.grto) END ) AND (user.id = '.Auth::id().') AND (pib.deleted_at IS NULL) AND (purchase_order.deleted_at IS NULL) AND (purchase_order_detail.deleted_at IS NULL)
              GROUP BY
                container.container_no
              HAVING SUM(
                    purchase_order_detail.gr_qty
                  ) >0';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" class="btn btn-xs" title="Edit"  onclick="edit('."'".$itemdata->container_no."'".')"> Edit</a>';
      })
 
      ->make(true);
    } else {
      exit("No data available");
    }
  }


 

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
    $post = new Item(); 
    $post->item_category_id = $request->item_category_id; 
    $post->item_brand_id = $request->item_brand_id; 
    $post->item_code = $request->item_code;  
    $post->item_name = $request->item_name;  
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    // $item = Item::Find($id);
     $sql = 'SELECT
                item_category.item_category_name,
                item_brand.item_brand_name,
                item.item_category_id,
                item.item_brand_id,
                item.id,
                item.item_code,
                item.item_name,
                item.description,
                item.status
              FROM
                item
              LEFT JOIN item_brand ON item.item_brand_id = item_brand.id
              LEFT JOIN item_category ON item.item_category_id = item_category.id';
      $item = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id)->first(); 
    echo json_encode($item); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Item::Find($id); 
    $post->item_category_id = $request->item_category_id; 
    $post->item_brand_id = $request->item_brand_id; 
    $post->item_code = $request->item_code;  
    $post->item_name = $request->item_name;  
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Item::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   
 
   foreach($idkey as $key => $id)
   {
    // $post =  Item::where('id', $id["1"])->get();
    $post = Item::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
