<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\ThrRequest;
use App\Http\Controllers\Controller;
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Thr; 
use App\Model\Thrdet; 
use App\Model\Year;
use App\Model\Month;
use Validator;
use Response;
use App\Post;
use View;

class ThrController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'periodid' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::where('status', '0')->get()->pluck('description', 'id');
    $year_list = Year::all()->pluck('yearname', 'yearname');
    $month_list = Month::all()->pluck('monthname', 'id');
    $thrs = Thr::all();
    return view ('editor.thr.index', compact('thrs','payperiod_list', 'department_list', 'year_list', 'month_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $sql = 'SELECT
                thr.id,
                thr.departmentid,
                thr.periodid,
                thr.year, 
                DATE_FORMAT(thr.paymentdate,
                  "%d-%m-%Y"
                ) AS paymentdate,
                DATE_FORMAT(thr.idulfitridate,
                  "%d-%m-%Y"
                ) AS idulfitridate,
                department.departmentname,
                payperiod.description, 
                thr.status
              FROM
                thr
              LEFT JOIN department ON thr.departmentid = department.id
              LEFT JOIN payperiod ON thr.periodid = payperiod.id
              WHERE thr.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->description."'".')"> Hapus</a>';
      })

      ->addColumn('actiondetail', function ($itemdata) {
        return '<a href="thr/'.$itemdata->id.'/'.$itemdata->departmentid.'/editdetail" title="Detail" class="btn btn-primary btn-xs btn-flat"><i class="fa fa-folder"></i> Detail</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Aktif </span>';
        }else{
         return '<span class="label label-danger"> Tidak Aktif </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Thr(); 
    $post->periodid = $request->periodid; 
    $post->departmentid = $request->departmentid; 
    $post->year = $request->yearform; 
    $post->idulfitridate = $request->idulfitridate; 
    $post->paymentdate = $request->paymentdate; 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    $thr = Thr::Find($id);
    echo json_encode($thr); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Thr::Find($id); 
    $post->periodid = $request->periodid; 
    $post->departmentid = $request->departmentid; 
    $post->year = $request->yearform; 
    $post->idulfitridate = $request->idulfitridate; 
    $post->paymentdate = $request->paymentdate; 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function editdetail($id, $id2)
  {
    $thr = Thr::Find($id);
    $sql = 'SELECT
              employee.identityno,
              employee.employeename,
              employee.departmentid,
              employee.joindate,
              location.locationname,
              thrdet.transid,
              thrdet.employeeid,
              thrdet.dateemp,
              thrdet.basic,
              thrdet.`value` AS `value`,
              FORMAT(thrdet.thr, 0) AS thr,
              FORMAT(thrdet.adjthr, 0) AS adjthr,
              FORMAT(thrdet.tthr, 0) AS tthr,
              thrdet.id,
              thrdet.taxstatus,
              thrdet.npwp,
              thrdet.biayajabmember,
              thrdet.kumulatif1,
              thrdet.lastbruto1,
              thrdet.thrmonth1,
              thrdet.tlastbruto1,
              thrdet.biayajab1,
              thrdet.nettoptkp1,
              thrdet.ptkp1,
              thrdet.pkp1,
              thrdet.pph211,
              thrdet.kumulatif2,
              thrdet.lastbruto2,
              thrdet.thrmonth2,
              thrdet.tlastbruto2,
              thrdet.biayajab2,
              thrdet.nettoptkp2,
              thrdet.ptkp2,
              thrdet.pkp2,
              thrdet.pph212,
              thrdet.tarif,
              thrdet.pph21,
              thrdet.netto
            FROM
              thrdet
            INNER JOIN employee ON thrdet.employeeid = employee.id
            LEFT JOIN location ON employee.locationid = location.idlocation
            WHERE thrdet.transid = '.$id.' AND employee.departmentid = '.$id2.' AND employee.status =0';
    $thr_detail = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

    return view ('editor.thr.form', compact('thr', 'thr_detail'));
  }

  public function updatedetail($id, $id2, Request $request)
  {
     foreach($request->input('detail') as $key => $detail_data)
    {
      $thr_detail = Thrdet::Find($key); 
      $thr_detail->value = str_replace(",", "", $detail_data['value']);  
      $thr_detail->thr = str_replace(",", "", $detail_data['thr']);  
      $thr_detail->adjthr = str_replace(",", "", $detail_data['adjthr']);  
      $thr_detail->tthr = str_replace(",", "", $detail_data['tthr']);  
      // $thr_detail->pph21 = str_replace(",", "", $detail_data['pph21']);  
      $thr_detail->save();
    } 

    return back();
    // return redirect()->action('Editor\ThrController@index'); 
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Thr::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function generate($id)
  {  
    //dd("generate");
    //return response()->json($post); 
     DB::insert("INSERT INTO thrdet (transid, employeeid) 
                 SELECT
                  thr.id,
                  employee.id
                FROM
                  thr
                LEFT OUTER JOIN thrdet ON thr.id = thrdet.transid
                CROSS JOIN employee
                WHERE
                  (thrdet.employeeid IS NULL)
                AND thr.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )
                AND employee.`status` = 0
                AND thr.departmentid = employee.departmentid
                AND thr.`year` = (
                  SELECT
                    `user`.`year`
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )
                AND thr.`status` = 0");

      DB::update("UPDATE thrdet
                  INNER JOIN thr
                  SET thrdet.dateemp = (
                    SELECT
                      joindate
                    FROM
                      employee
                    WHERE
                      id = thrdet.employeeid
                  )
                  WHERE
                    thr.id = thrdet.transid
                  AND thr.periodid = (
                    SELECT
                      `user`.periodid
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )
                  AND thr.`year` = (
                    SELECT
                      `user`.`year`
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )
                  AND thr.`status` = 0");


      DB::update("UPDATE thrdet
      INNER JOIN thr
      SET biayajabmember = (
        SELECT
          biayajabmember
        FROM
          employee
        WHERE
          id = thrdet.employeeid
      )
      WHERE
        thr.id = thrdet.transid
      AND thr.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND thr.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND thr.`status` = 0");



      DB::update("UPDATE thrdet
      INNER JOIN thr
      SET basic = (
        SELECT
          basic
        FROM
          employee
        WHERE
          id = thrdet.employeeid
      ),
       thr = round(
        CASE
        WHEN TIMESTAMPDIFF(
          MONTH,
          dateemp,
          thr.idulfitridate
        ) < 12 THEN
          (ifnull(thrdet.basic, 0)) * TIMESTAMPDIFF(
            MONTH,
            dateemp,
            thr.idulfitridate
          ) / 12
        ELSE
          ifnull(thrdet.basic, 0)
        END,
        - 2
      )
      WHERE
        thr.id = thrdet.transid
      AND thr.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND thr.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND thr.`status` = 0");


      DB::update("UPDATE thrdet
      INNER JOIN thr
      SET thr = round(
        CASE
        WHEN timestampdiff(
          MONTH,
          dateemp,
          thr.idulfitridate
        ) < 12 THEN
          (ifnull(thrdet.basic, 0)) * timestampdiff(
            MONTH,
            dateemp,
            thr.idulfitridate
          ) / 12
        ELSE
          ifnull(thrdet.basic, 0)
        END,
        - 2
      )
      WHERE
        thr.id = thrdet.transid
      AND thr.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND thr.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND thr.status = 0");

      DB::update("UPDATE thrdet
      INNER JOIN thr ON thr.id = thrdet.transid
      INNER JOIN employee ON thrdet.employeeid = employee.id
      SET thrdet.taxstatus =  employee.taxstatus
      WHERE
        thr.id = thrdet.transid
      AND thr.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND thr.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )");



      DB::update("UPDATE thrdet
      INNER JOIN thr
      SET npwp = (
        SELECT
          npwp
        FROM
          employee
        WHERE
          id = thrdet.employeeid
      )
      WHERE
        thr.id = thrdet.transid
      AND thr.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND thr.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND thr.`status` = 0");


       // -- untuk mengembalikan nilai menjadi 0
      DB::update("UPDATE thrdet
      INNER JOIN thr
      SET kumulatif1 = 0,
       lastbruto1 = 0,
       thrmonth1 = 0,
       tlastbruto1 = 0,
       biayajab1 = 0,
       nettoptkp1 = 0,
       ptkp1 = 0,
       pkp1 = 0,
       pph211 = 0,
       kumulatif2 = 0,
       lastbruto2 = 0,
       thrmonth2 = 0,
       tlastbruto2 = 0,
       biayajab2 = 0,
       nettoptkp2 = 0,
       ptkp2 = 0,
       pkp2 = 0,
       pph212 = 0,
       pph21 = 0,
       netto = 0
      WHERE
        thr.id = thrdet.transid
      AND thr.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND thr.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND thr.`status` = 0");


      // -- mengambil nilai kumulatif tanpa thr
      DB::update("UPDATE thrdet td
      INNER JOIN thr t,
       (
        SELECT
          payroll.employeeid,
          sum(payroll.bruto) AS bruto
        FROM
          payroll
        INNER JOIN payperiod ON payroll.periodid = payperiod.id
        WHERE
          (
            payperiod. MONTH BETWEEN (
              SELECT
                `user`.bmonth
              FROM
                `user`
              WHERE
                `user`.id = 1
            )
            AND (
              SELECT
                `user`.emonth
              FROM
                `user`
              WHERE
                `user`.id = 1
            )
          )
        AND (
          payperiod.`year` = (
            SELECT
              `user`.`year`
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )
        GROUP BY
          payroll.employeeid
      ) k
      SET td.kumulatif1 = k.bruto
      WHERE
        t.id = td.id
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND k.employeeid = td.employeeid
      AND t.`status` = 0");

      // -- mengambil nilai kumulatif dengan thr
      DB::update("UPDATE thrdet td
      INNER JOIN thr t,
       (
        SELECT
          payroll.employeeid,
          sum(payroll.bruto) AS bruto
        FROM
          payroll
        INNER JOIN payperiod ON payroll.periodid = payperiod.id
        WHERE
          (
            payperiod. MONTH BETWEEN (
              SELECT
                `user`.bmonth
              FROM
                `user`
              WHERE
                `user`.id = 1
            )
            AND (
              SELECT
                `user`.emonth
              FROM
                `user`
              WHERE
                `user`.id = 1
            )
          )
        AND (
          payperiod.`year` = (
            SELECT
              `user`.`year`
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )
        GROUP BY
          payroll.employeeid
      ) k
      SET td.kumulatif2 = k.bruto
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND k.employeeid = td.employeeid
      AND t.`status` = 0");


      // -- mengambil nilai bruto terakhir tanpa thr
      DB::update("UPDATE thrdet td
      INNER JOIN thr t,
       (
        SELECT
          payroll.employeeid,
          sum(payroll.bruto) AS bruto
        FROM
          payroll
        INNER JOIN payperiod ON payroll.periodid = payperiod.id
        WHERE
          (
            payperiod. MONTH = (
              SELECT
                `user`.lastbrutothrmonth
              FROM
                `user`
              WHERE
                `user`.id = 1
            )
          )
        AND (
          payperiod.`year` = (
            SELECT
              `user`.lastbrutothr `year`
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )
        GROUP BY
          payroll.employeeid
      ) k
      SET td.lastbruto1 = k.bruto
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND k.employeeid = td.employeeid
      AND t.`status` = 0");

      // -- mengambil nilai bruto terakhir dengan thr
      DB::update("UPDATE thrdet td
      INNER JOIN thr t,
       (
        SELECT
          payroll.employeeid,
          sum(payroll.bruto) AS bruto
        FROM
          payroll
        INNER JOIN payperiod ON payroll.periodid = payperiod.id
        WHERE
          (
            payperiod. MONTH = (
              SELECT
                `user`.lastbrutothrmonth
              FROM
                `user`
              WHERE
                `user`.id = 1
            )
          )
        AND (
          payperiod.`year` = (
            SELECT
              `user`.lastbrutothr `year`
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        )
        GROUP BY
          payroll.employeeid
      ) k
      SET td.lastbruto2 = k.bruto
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND k.employeeid = td.employeeid
      AND t.`status` = 0");



      // // -- mengambil bulan terakhir tanpa thr
      // DB::update("UPDATE thr t,
      //  thrdet td
      // SET td.thrmonth1 = (
      //   SELECT
      //     `user`.lastbrutothrmonth
      //   FROM
      //     `user`
      //   WHERE
      //     `user`.id = 1
      // )
      // WHERE
      //   t.id = td.transid
      // AND t.periodid = (
      //   SELECT
      //     `user`.periodid
      //   FROM
      //     `user`
      //   WHERE
      //     `user`.id = 1
      // )
      // AND t.`year` = (
      //   SELECT
      //     `user`.`year`
      //   FROM
      //     `user`
      //   WHERE
      //     `user`.id = 1
      // )
      // AND t.`status` = 0");


      // // -- mengambil bulan terakhir dengan thr
      // DB::update("UPDATE thr t,
      //  thrdet td
      // SET td.thrmonth2 = (
      //   SELECT
      //     `user`.lastbrutothrmonth
      //   FROM
      //     `user`
      //   WHERE
      //     `user`.id = 1
      // )
      // WHERE
      //   t.id = td.transid
      // AND t.periodid = (
      //   SELECT
      //     `user`.periodid
      //   FROM
      //     `user`
      //   WHERE
      //     `user`.id = 1
      // )
      // AND t.`year` = (
      //   SELECT
      //     `user`.`year`
      //   FROM
      //     `user`
      //   WHERE
      //     `user`.id = 1
      // )
      // AND t.`status` = 0");

      // -- tanpa thr
      DB::update("UPDATE thr t,
       thrdet td
      SET td.thrmonth1 = CASE
      WHEN td.thrmonth1 = 12 THEN
        12
      ELSE
        CASE
      WHEN td.thrmonth1 <> 12 THEN
        12 - td.thrmonth1
      END
      END
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");

      // -- dengan thr
      DB::update("UPDATE thr t,
       thrdet td
      SET td.thrmonth2 = CASE
      WHEN td.thrmonth2 = 12 THEN
        12
      ELSE
        CASE
      WHEN td.thrmonth2 <> 12 THEN
        12 - td.thrmonth2
      END
      END
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");

      // -- total bruto tanpa thr
      DB::update("UPDATE thr t,
        thrdet td
      SET td.tlastbruto1 = (
        ifnull(td.lastbruto1, 0) * ifnull(td.thrmonth1, 0)
      ) + ifnull(td.kumulatif1, 0) 
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");

      // -- total bruto dengan thr
      DB::update("UPDATE thrdet a
                  INNER JOIN thrdet u
                  SET a.tthr = ifnull(u.`value`, 0) * ifnull(u.thr, 0) + ifnull(u.adjthr, 0)
                  WHERE
                    a.id = u.id");

      // -- total bruto dengan thr
      DB::update("UPDATE thr t,
       thrdet td
      SET td.tlastbruto2 = (
        ifnull(td.lastbruto2, 0) * ifnull(td.thrmonth2, 0)
      ) + ifnull(td.kumulatif2, 0) + ifnull(td.tthr, 0)
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");


      // -- biaya jabatan tanpa thr
      DB::update("UPDATE thr t,
       thrdet td
      SET td.biayajab1 = round(
        CASE
        WHEN td.tlastbruto1 * 5 / 100 > 6000000 THEN
          6000000
        ELSE
          td.tlastbruto1 * 5 / 100
        END,
        0
      ) * td.biayajabmember
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");

      // -- biaya jabatan dengan thr
      DB::update("UPDATE thr t,
       thrdet td
      SET td.biayajab2 = round(
        CASE
        WHEN td.tlastbruto2 * 5 / 100 > 6000000 THEN
          6000000
        ELSE
          td.tlastbruto2 * 5 / 100
        END,
        0
      ) * td.biayajabmember
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");


      // -- nettoptkp tanpa thr
      DB::update("UPDATE thr t,
       thrdet td
      SET td.nettoptkp1 = ifnull(td.tlastbruto1, 0) - ifnull(td.biayajab1, 0)
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");


      // -- nettoptkp dengan thr
      DB::update("UPDATE thr t,
       thrdet td
      SET td.nettoptkp2 = ifnull(td.tlastbruto2, 0) - ifnull(td.biayajab2, 0)
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");


      // -- ptkp tanpa thr
      DB::update("UPDATE thr t,
       thrdet td,
       ptkp p
      SET td.ptkp1 = ifnull(p.ptkp, 0) * 12
      WHERE
        t.id = td.transid
      AND td.taxstatus = p.taxstatus
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");

      // -- ptkp dengan thr
      DB::update("UPDATE thr t,
       thrdet td,
       ptkp p
      SET td.ptkp2 = ifnull(p.ptkp, 0) * 12
      WHERE
        t.id = td.transid
      AND td.taxstatus = p.taxstatus
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");


      // -- pkp tanpa thr
      DB::update("UPDATE thr t,
       thrdet td
      SET td.pkp1 = ifnull(td.nettoptkp1, 0) - ifnull(td.ptkp1, 0)
      WHERE
        td.nettoptkp1 > td.ptkp1
      AND t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");


      // -- pkp dengan thr
      DB::update("UPDATE thr t,
       thrdet td
      SET td.pkp2 = ifnull(td.nettoptkp2, 0) - ifnull(td.ptkp2, 0)
      WHERE
        td.nettoptkp2 > td.ptkp2
      AND t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");




      // -- pph21 tanpa thr
      DB::update("UPDATE   
        thr t,
        thrdet td,
        (
          SELECT
            thrdet.employeeid,
            sum(
              CASE
              WHEN pkp1 < tarif.`from`
                 THEN
                  0
                ELSE
                  CASE
                WHEN pkp1 > tarif.`to` THEN
                  tarif.`to` - tarif.`from`
                ELSE
                  pkp1 - tarif.`from`
                END
                END * tarif.tarif / 100
            ) AS pph
          FROM
            thrdet
          INNER JOIN thr ON thrdet.transid = thr.id
          CROSS JOIN tarif
          WHERE
            thr.periodid = (
              SELECT
                `user`.periodid
              FROM
                `user`
              WHERE
                `user`.id = 1
            )
          GROUP BY
            thrdet.employeeid
        ) u 
      SET td.pph211 = round(u.pph, 0)
      WHERE
        t.id = td.transid
      AND td.employeeid = u.employeeid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");



      // -- pph21 dengan thr
      DB::update("UPDATE thr t,
       thrdet td,
       (
        SELECT
          thrdet.employeeid,
          sum(
            CASE
            WHEN pkp2 < tarif.`from` THEN
              0
            ELSE
              CASE
            WHEN pkp2 > tarif.`to` THEN
              tarif.`to` - tarif.`from`
            ELSE
              pkp2 - tarif.`from`
            END
            END * tarif.tarif / 100
          ) AS pph
        FROM
          thrdet
        INNER JOIN thr ON thrdet.transid = thr.id
        CROSS JOIN tarif
        WHERE
          thr.periodid = (
            SELECT
              `user`.periodid
            FROM
              `user`
            WHERE
              `user`.id = 1
          )
        GROUP BY
          thrdet.employeeid
      ) u
      SET td.pph212 = round(u.pph, 0)
      WHERE
        t.id = td.transid
      AND td.employeeid = u.employeeid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");


      // -- pph21 tanpa thr
      DB::update("UPDATE thr t,
       thrdet td
      SET td.pph211 = CASE
      WHEN LENGTH(rtrim(npwp)) > 0 THEN
        td.pph211
      ELSE
        td.pph211 * 1.2
      END
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");


      // -- pph21 dengan thr
      DB::update("UPDATE thr t,
       thrdet td
      SET td.pph212 = CASE
      WHEN length(rtrim(npwp)) > 0 THEN
        td.pph212
      ELSE
        td.pph212 * 1.2
      END
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");


      // -- hitung selisih pph21 dengan thr dan tanpa thr
      DB::update("UPDATE thr t,
       thrdet td
      SET td.pph21 = ifnull(pph212, 0) - ifnull(pph211, 0)
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");


      DB::update("UPDATE thr t,
       thrdet td
      SET td.pph21 = CASE
      WHEN td.taxstatus IS NULL THEN
        0
      ELSE
        td.pph21
      END
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0");


      DB::update("UPDATE thrdet
                  INNER JOIN (
                    SELECT
                      thrdet.id,
                      thrdet.employeeid,
                      CASE
                    WHEN (thrdet.tthr * 12) - thrdet.ptkp1 BETWEEN tarif.`from`
                    AND `to` THEN
                      tarif.tarif
                    END AS tarif,
                    thrdet.tthr * CASE
                  WHEN (thrdet.tthr * 12) - thrdet.ptkp1 BETWEEN tarif.`from`
                  AND `to` THEN
                    tarif.tarif
                  END / 100 AS pph21,
                   employee.employeename,
                   thrdet.tthr, (thrdet.tthr * 12) - thrdet.ptkp1 AS pkp1
                  FROM
                    thrdet
                  INNER JOIN employee ON thrdet.employeeid = employee.id
                  CROSS JOIN tarif
                  WHERE
                    (
                      CASE
                      WHEN (thrdet.tthr * 12) - thrdet.ptkp1 BETWEEN tarif.`from`
                      AND `to` THEN
                        tarif.tarif
                      END IS NOT NULL
                    )
                  ) AS thrdet1 ON thrdet.id = thrdet1.id
                  SET thrdet.pph21 = IFNULL(thrdet1.pph21,0), thrdet.tarif = IFNULL(thrdet1.tarif,0), thrdet.pkp1 = IFNULL(thrdet1.pkp1,0)
                  WHERE thrdet.tthr >0");



      DB::update("UPDATE thr t,
       thrdet td
      SET td.netto = round(
        ifnull(td.tthr, 0) - ifnull(td.pph21, 0),
        0
      )
      WHERE
        t.id = td.transid
      AND t.periodid = (
        SELECT
          `user`.periodid
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`year` = (
        SELECT
          `user`.`year`
        FROM
          `user`
        WHERE
          `user`.id = 1
      )
      AND t.`status` = 0"); 
  }
}
