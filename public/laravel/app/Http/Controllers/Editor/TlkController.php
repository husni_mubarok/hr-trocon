<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\MealtranRequest;
use App\Http\Controllers\Controller;
use App\Model\Payperiod;
use App\Model\Department;
use App\Model\Mealtran; 
use App\Model\Mealtrandet; 
use Validator;
use Response;
use App\Post;
use View;

class TlkController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'periodid' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $department_list = Department::all()->pluck('departmentname', 'id');
    $payperiod_list = Payperiod::all();
    $mealtrans = Mealtran::all();
    $sql1 = 'SELECT
              `user`.id,
              payperiod.id AS periodid,
              payperiod.description
            FROM
              payperiod
            INNER JOIN `user` ON payperiod.id = `user`.periodid';

    $datafilter = DB::table(DB::raw("($sql1) as rs_sql"))->where('id', Auth::id())->first(); 

    return view ('editor.tlk.index', compact('mealtrans','payperiod_list', 'department_list', 'datafilter'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $userid = Auth::id();
      $sql = 'SELECT
                mealtran.id,
                mealtran.departmentid,
                mealtran.periodid,
                department.departmentname,
                payperiod.description, 
                mealtran.status
              FROM
                mealtran
              LEFT JOIN department ON mealtran.departmentid = department.id
              LEFT JOIN payperiod ON mealtran.periodid = payperiod.id, user
              WHERE user.id = '.$userid.' AND mealtran.periodid = user.periodid
              AND mealtran.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->description."'".')"> Hapus</a>';
      })

      ->addColumn('actiondetail', function ($itemdata) {
        return '<a href="tlk/'.$itemdata->id.'/'.$itemdata->departmentid.'/editdetail" title="Detail" class="btn btn-primary btn-xs btn-flat"><i class="fa fa-folder"></i> Detail</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Aktif </span>';
        }else{
         return '<span class="label label-danger"> Tidak Aktif </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
    if ($validator->fails()) {
        return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
    } else {
      $post = new Mealtran(); 
      $post->periodid = $request->periodid; 
      $post->departmentid = $request->departmentid; 
      $post->status = $request->status;
      $post->created_by = Auth::id();
      $post->save();

      return response()->json($post); 
    }
  }

  public function edit($id)
  {
    $mealtran = Mealtran::Find($id);
    echo json_encode($mealtran); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Mealtran::Find($id); 
    $post->periodid = $request->periodid; 
    $post->departmentid = $request->departmentid; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function editdetail($id, $id2)
  {
    $mealtran = Mealtran::Find($id); 

        $sql = 'SELECT
                  employee.identityno,
                  employee.employeename,
                  employee.departmentid,
                  location.locationname,
                  mealtrandet.id,
                  mealtrandet.transid,
                  mealtrandet.checklist,
                  mealtrandet.employeeid,
                  FORMAT(mealtrandet.doc1,0) AS doc1,
                  FORMAT(mealtrandet.doc2,0) AS doc2,
                  FORMAT(mealtrandet.doc3,0) AS doc3,
                  FORMAT(mealtrandet.doc4,0) AS doc4,
                  FORMAT(mealtrandet.doc5,0) AS doc5,
                  FORMAT(mealtrandet.dayoutcity,0) AS dayoutcity,
                  FORMAT(mealtrandet.ratemealoutcity,0) AS ratemealoutcity,
                  FORMAT(mealtrandet.tunluarkota,0) AS tunluarkota,
                  FORMAT(mealtrandet.perday,0) AS perday,
                  FORMAT(mealtrandet.amount,0) AS amount,
                  FORMAT(mealtrandet.amountpajak,0) AS amountpajak,
                  FORMAT(mealtrandet.addmeal,0) AS addmeal,
                  FORMAT(mealtrandet.insentif1,0) AS insentif1,
                  FORMAT(mealtrandet.insentif2,0) AS insentif2,
                  FORMAT(mealtrandet.insentif3,0) AS insentif3,
                  FORMAT(mealtrandet.insentif4,0) AS insentif4,
                  FORMAT(mealtrandet.insentif5,0) AS insentif5,
                  FORMAT(mealtrandet.insentif,0) AS insentif,
                  FORMAT(mealtrandet.dtunjmalam1,0) AS dtunjmalam1,
                  FORMAT(mealtrandet.dtunjmalam2,0) AS dtunjmalam2,
                  FORMAT(mealtrandet.dtunjmalam3,0) AS dtunjmalam3,
                  FORMAT(mealtrandet.dtunjmalam4,0) AS dtunjmalam4,
                  FORMAT(mealtrandet.dtunjmalam5,0) AS dtunjmalam5,
                  FORMAT(mealtrandet.dtunjmalam,0) AS dtunjmalam,
                  FORMAT(mealtrandet.ratetunjmalam,0) AS ratetunjmalam,
                  FORMAT(mealtrandet.tunjmalam,0) AS tunjmalam,
                  FORMAT(mealtrandet.potabsence1,0) AS potabsence1,
                  FORMAT(mealtrandet.potabsence2,0) AS potabsence2,
                  FORMAT(mealtrandet.potabsence3,0) AS potabsence3,
                  FORMAT(mealtrandet.potabsence4,0) AS potabsence4,
                  FORMAT(mealtrandet.potabsence5,0) AS potabsence5,
                  FORMAT(mealtrandet.potabsence,0) AS potabsence,
                  FORMAT(mealtrandet.ratepotabsence,0) AS ratepotabsence,
                  FORMAT(mealtrandet.tpotabsence,0) AS tpotabsence,
                  FORMAT(mealtrandet.correction,0) AS correction,
                  FORMAT(IFNULL(insentif,0) + IFNULL(tunluarkota,0) + IFNULL(tunjmalam,0) + IFNULL(tpotabsence,0),0) AS total,
                  FORMAT((IFNULL(insentif,0) + IFNULL(tunluarkota,0) + IFNULL(tunjmalam,0) + IFNULL(tpotabsence,0)) + IFNULL(mealtrandet.correction,0),0) AS grandtotal
                FROM
                  mealtrandet
                LEFT JOIN employee ON mealtrandet.employeeid = employee.id
                LEFT JOIN location ON employee.locationid = location.idlocation
            WHERE mealtrandet.transid = '.$id.'';
    $mealtran_detail = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


    $sql = 'SELECT 
                  FORMAT(SUM(mealtrandet.doc1),0) AS doc1,
                  FORMAT(SUM(mealtrandet.doc2),0) AS doc2,
                  FORMAT(SUM(mealtrandet.doc3),0) AS doc3,
                  FORMAT(SUM(mealtrandet.doc4),0) AS doc4,
                  FORMAT(SUM(mealtrandet.doc5),0) AS doc5,
                  FORMAT(SUM(mealtrandet.dayoutcity),0) AS dayoutcity,
                  FORMAT(SUM(mealtrandet.ratemealoutcity),0) AS ratemealoutcity,
                  FORMAT(SUM(mealtrandet.tunluarkota),0) AS tunluarkota,
                  FORMAT(SUM(mealtrandet.perday),0) AS perday,
                  FORMAT(SUM(mealtrandet.amount),0) AS amount,
                  FORMAT(SUM(mealtrandet.amountpajak),0) AS amountpajak,
                  FORMAT(SUM(mealtrandet.addmeal),0) AS addmeal,
                  FORMAT(SUM(mealtrandet.insentif1),0) AS insentif1,
                  FORMAT(SUM(mealtrandet.insentif2),0) AS insentif2,
                  FORMAT(SUM(mealtrandet.insentif3),0) AS insentif3,
                  FORMAT(SUM(mealtrandet.insentif4),0) AS insentif4,
                  FORMAT(SUM(mealtrandet.insentif5),0) AS insentif5,
                  FORMAT(SUM(mealtrandet.insentif),0) AS insentif,
                  FORMAT(SUM(mealtrandet.dtunjmalam1),0) AS dtunjmalam1,
                  FORMAT(SUM(mealtrandet.dtunjmalam2),0) AS dtunjmalam2,
                  FORMAT(SUM(mealtrandet.dtunjmalam3),0) AS dtunjmalam3,
                  FORMAT(SUM(mealtrandet.dtunjmalam4),0) AS dtunjmalam4,
                  FORMAT(SUM(mealtrandet.dtunjmalam5),0) AS dtunjmalam5,
                  FORMAT(SUM(mealtrandet.dtunjmalam),0) AS dtunjmalam,
                  FORMAT(SUM(mealtrandet.ratetunjmalam),0) AS ratetunjmalam,
                  FORMAT(SUM(mealtrandet.tunjmalam),0) AS tunjmalam,
                  FORMAT(SUM(mealtrandet.potabsence1),0) AS potabsence1,
                  FORMAT(SUM(mealtrandet.potabsence2),0) AS potabsence2,
                  FORMAT(SUM(mealtrandet.potabsence3),0) AS potabsence3,
                  FORMAT(SUM(mealtrandet.potabsence4),0) AS potabsence4,
                  FORMAT(SUM(mealtrandet.potabsence5),0) AS potabsence5,
                  FORMAT(SUM(mealtrandet.potabsence),0) AS potabsence,
                  FORMAT(SUM(mealtrandet.ratepotabsence),0) AS ratepotabsence,
                  FORMAT(SUM(mealtrandet.tpotabsence),0) AS tpotabsence,
                  FORMAT(SUM(mealtrandet.correction),0) AS correction,
                  FORMAT(SUM(IFNULL(insentif,0) + IFNULL(tunluarkota,0) + IFNULL(tunjmalam,0) + IFNULL(tpotabsence,0)),0) AS total,
                  FORMAT(SUM((IFNULL(insentif,0) + IFNULL(tunluarkota,0) + IFNULL(tunjmalam,0) + IFNULL(tpotabsence,0)) + IFNULL(mealtrandet.correction,0)),0) AS grandtotal
                FROM
                  mealtrandet
                LEFT JOIN employee ON mealtrandet.employeeid = employee.id
                LEFT JOIN location ON employee.locationid = location.idlocation
            WHERE mealtrandet.transid = '.$id.'';
    $mealtran_total = DB::table(DB::raw("($sql) as rs_sql"))->first(); 

    return view ('editor.tlk.form', compact('mealtran', 'mealtran_detail', 'mealtran_total'));
  }

  public function updatedetail($id, $id2, Request $request)
  {
     foreach($request->input('detail') as $key => $detail_data)
    {
      if( isset($detail_data['checklist'])){
        $checklist = 1;
      }else{
        $checklist = 0;
      };

      $mealtran_detail = Mealtrandet::Find($key); 
      $mealtran_detail->insentif1 = str_replace(",","",$detail_data['insentif1']); 
      $mealtran_detail->insentif2 = str_replace(",","",$detail_data['insentif2']);
      $mealtran_detail->insentif3 = str_replace(",","",$detail_data['insentif3']);
      $mealtran_detail->insentif4 = str_replace(",","",$detail_data['insentif4']);
      $mealtran_detail->insentif5 = str_replace(",","",$detail_data['insentif5']);
      $mealtran_detail->insentif = str_replace(",","",$detail_data['insentif']);
      $mealtran_detail->doc1 = str_replace(",","",$detail_data['doc1']); 
      $mealtran_detail->doc2 = str_replace(",","",$detail_data['doc2']);
      $mealtran_detail->doc3 = str_replace(",","",$detail_data['doc3']);
      $mealtran_detail->doc4 = str_replace(",","",$detail_data['doc4']);
      $mealtran_detail->doc5 = str_replace(",","",$detail_data['doc5']);
      $mealtran_detail->dayoutcity = str_replace(",","",$detail_data['dayoutcity']);
      $mealtran_detail->ratemealoutcity = str_replace(",","",$detail_data['ratemealoutcity']);
      $mealtran_detail->tunluarkota = str_replace(",","",$detail_data['tunluarkota']); 
      $mealtran_detail->dtunjmalam1 = str_replace(",","",$detail_data['dtunjmalam1']); 
      $mealtran_detail->dtunjmalam2 = str_replace(",","",$detail_data['dtunjmalam2']);
      $mealtran_detail->dtunjmalam3 = str_replace(",","",$detail_data['dtunjmalam3']);
      $mealtran_detail->dtunjmalam4 = str_replace(",","",$detail_data['dtunjmalam4']);
      $mealtran_detail->dtunjmalam5 = str_replace(",","",$detail_data['dtunjmalam5']);
      $mealtran_detail->dtunjmalam = str_replace(",","",$detail_data['dtunjmalam']);
      $mealtran_detail->ratetunjmalam = str_replace(",","",$detail_data['ratetunjmalam']);
      $mealtran_detail->tunjmalam = str_replace(",","",$detail_data['tunjmalam']);
      $mealtran_detail->correction = str_replace(",","",$detail_data['correction']);
      $mealtran_detail->checklist = $checklist;
      $mealtran_detail->save();
    } 

    return back();
    // return redirect()->action('Editor\TlkController@index'); 
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Mealtran::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function generate($id)
  {  
    //dd("generate");
    //return response()->json($post); 
     DB::insert("INSERT INTO mealtrandet (employeeid, transid) SELECT
                  employee.id,
                  mealtran.id
                FROM
                  mealtran
                LEFT OUTER JOIN mealtrandet ON mealtran.id = mealtrandet.transid
                CROSS JOIN employee
                WHERE
                  (
                    mealtrandet.employeeid IS NULL
                  )
                AND mealtran.periodid = (
                  SELECT
                    `user`.periodid
                  FROM
                    `user`
                  WHERE
                    `user`.id = 1
                )
                AND employee.`status` = 0
                AND mealtran.departmentid = employee.departmentid");

      DB::update("UPDATE mealtrandet,
                   mealtran,
                   potabsence,
                   employee
                  SET mealtrandet.rateextrapudding = employee.extrapudding,
                   mealtrandet.perday = employee.mealtransrate,
                   mealtrandet.ratemealincity = employee.ratemealincity,
                   mealtrandet.ratemealoutcity = employee.ratemealoutcity,
                   mealtrandet.ratetunjmalam = ifnull(employee.ratetunjanganmalam, 0),
                   mealtrandet.employeestatus = employee.`status`,
                   mealtrandet.ratepotabsence = potabsence.potabsence
                  WHERE
                    employee.id = mealtrandet.employeeid
                  AND mealtran.id = mealtrandet.transid
                  AND mealtran.periodid = (
                    SELECT
                      `user`.periodid
                    FROM
                      `user`
                    WHERE
                      `user`.id = 1
                  )
                  AND employee.`status` = 0");
  }
}
