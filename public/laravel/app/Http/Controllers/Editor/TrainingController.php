<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\TrainingRequest;
use App\Http\Controllers\Controller;
use App\Model\Training; 
use App\Model\Employee; 
use App\Model\Educationtype;
use Validator;
use Response;
use App\Post;
use View;

class TrainingController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'trainingno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'trainingname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $trainings = Training::all();
      return view ('editor.training.index', compact('trainings'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  training.id,
                  training.notrans,
                  DATE_FORMAT(training.datetrans, "%d-%m-%Y") AS datetrans,
                  training.employeeid,
                  employee.employeename,
                  DATE_FORMAT(training.trainingfrom, "%d-%m-%Y") AS trainingfrom,
                  DATE_FORMAT(training.trainingto, "%d-%m-%Y") AS trainingto,
                  training.educationtypeid,
                  educationtype.educationtypename,
                  training.fasilitator,
                  training.certified,
                  training.days,
                  training.status,
                  training.attachment,
                  FORMAT( training.cost, 0 ) AS cost 
                FROM
                  training
                  LEFT JOIN employee ON training.employeeid = employee.id
                  LEFT JOIN educationtype ON training.educationtypeid = educationtype.id
                WHERE
                  training.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

        return Datatables::of($itemdata)  
        ->addColumn('action', function ($itemdata) { 
            if ($itemdata->status == 1 || $itemdata->status == 2) {
                return ''.$itemdata->notrans.'';
              }else{
                return '<a href="training/'.$itemdata->id.'/edit" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
            };  
         })
         
        ->addColumn('approval', function ($itemdata) {
          return '<a  href="javascript:void(0)" title="Approval" class="btn btn-success btn-xs" onclick="approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-check"></i> Approve</a> <a  href="javascript:void(0)" title="Approval" class="btn btn-warning btn-xs" onclick="not_approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-close"></i> Not Approve</a>';
        })

        ->addColumn('mstatus', function ($itemdata) {
          if ($itemdata->status == 0) {
              return '<span class="label label-success"> <i class="fa fa-check"></i> Active </span>';
            }else if ($itemdata->status == 9){
             return '<span class="label label-danger"> <i class="fa fa-minus-square"></i> Cancel </span>';
            }else if ($itemdata->status == 2){
             return '<span class="label label-warning"> <i class="fa fa-minus-square"></i> Not Approve </span>';
            }else if ($itemdata->status == 1){
             return '<span class="label label-success"> <i class="fa fa-check"></i> Approve </span>';
          }; 
         })

        ->addColumn('attachment', function ($itemdata) {
          if ($itemdata->attachment == null) {
            return '';
            }else{
             return '<a href="../uploads/training/'.$itemdata->attachment.'" target="_blank"/><i class="fa fa-download"></i> Download</a>';
           };  
          })
        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function datahistory(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  training.id,
                  training.notrans,
                  training.datetrans,
                  employee.employeename,
                  training.educationtypeid,
                  educationtype.educationtypename
                FROM
                  training
                INNER JOIN employee ON training.employeeid = employee.id
                INNER JOIN educationtype ON training.educationtypeid = educationtype.id,
                 `user`
                WHERE
                  `user`.id = '.Auth::id().' AND training.employeeid = user.employeeid
                AND
                  training.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 

        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function create()
    {  
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      $educationtype_list = Educationtype::all()->pluck('educationtypename', 'id'); 

      return view ('editor.training.form', compact('employee_list', 'educationtype_list'));
    }

    public function store(Request $request)
    { 
        DB::insert("INSERT INTO training (codetrans, notrans, datetrans)
                    SELECT 'MUTA',
                    IFNULL(CONCAT('PUNS','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(training.notrans),3))+1001,3)), CONCAT('PUNS','-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW())  
                    FROM
                    training
                    WHERE codetrans='PUNS'");

       $lastInsertedID = DB::table('training')->max('id');  

       // dd($lastInsertedID);

       $training = Training::where('id', $lastInsertedID)->first(); 
       $training->employeeid = $request->input('employeeid'); 
       $training->educationtypeid = $request->input('educationtypeid'); 
       $training->trainingfrom = $request->input('trainingfrom'); 
       $training->trainingto = $request->input('trainingto');  
       $training->cost = str_replace(",","",$request->input('cost'));  
       $training->days = $request->input('days');  
       $training->certified = $request->input('certified');  
       $training->fasilitator = $request->input('fasilitator');  
       $training->created_by = Auth::id();
       $training->save();

       if($request->attachment)
       {
        $training = Training::FindOrFail($training->id);
        $original_directory = "uploads/training/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $training->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $training->attachment);
          $training->save(); 
        } 
        return redirect('editor/training'); 
     
    }

    public function edit($id)
    {
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      $educationtype_list = Educationtype::all()->pluck('educationtypename', 'id'); 
      $training = Training::Where('id', $id)->first();   

      // dd($training); 
      return view ('editor.training.form', compact('training','educationtype_list', 'employee_list'));
    }

    public function update($id, Request $request)
    {
      
       $training = Training::FindOrFail($id); 
       $training->employeeid = $request->input('employeeid'); 
       $training->educationtypeid = $request->input('educationtypeid'); 
       $training->trainingfrom = $request->input('trainingfrom'); 
       $training->trainingto = $request->input('trainingto');  
       $training->cost = str_replace(",","",$request->input('cost'));   
       $training->days = $request->input('days');  
       $training->certified = $request->input('certified');  
       $training->fasilitator = $request->input('fasilitator');  
       $training->status = 0;  
       $training->created_by = Auth::id();
       $training->save();

      if($request->attachment)
      {
        $training = Training::FindOrFail($training->id);
        $original_directory = "uploads/training/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $training->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $training->attachment);
          $training->save(); 
        } 
        return redirect('editor/training');  
      }

      public function cancel($id, Request $request)
      {
        $post = Training::Find($id); 
        $post->status = 9; 
        $post->created_by = Auth::id();
        $post->save(); 
      
        return response()->json($post); 

      }

      public function delete($id)
      {
    //dd($id);
        $post =  Training::Find($id);
        $post->delete(); 

        return response()->json($post); 
      }

      public function deletebulk(Request $request)
      {

       $idkey = $request->idkey;   

       foreach($idkey as $key => $id)
       {
    // $post =  Document::where('id', $id["1"])->get();
        $post = Document::Find($id["1"]);
        $post->delete(); 
      }

      echo json_encode(array("status" => TRUE));

    }
  }
