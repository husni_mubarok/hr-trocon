<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\TravellingRequest;
use App\Http\Controllers\Controller;
use App\Model\Travelling; 
use App\Model\Employee; 
use App\Model\Travellingtype;
use App\Model\City;
use Validator;
use Response;
use App\Post;
use View;

class TravellingController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'travellingno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'travellingname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $travellings = Travelling::all();
      return view ('editor.travelling.index', compact('travellings'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  travelling.id,
                  travelling.codetrans,
                  travelling.notrans,
                  DATE_FORMAT( travelling.datetrans, "%d/%m/%Y" ) AS datetrans,
                  travelling.employeeid,
                  employee.employeename,
                  travelling.travellingfrom,
                  travelingtype.travelingtypename,
                  travelling.travellingtypeid,
                  travelling.cityid,
                  city.cityname,
                  travelling.travellingto,
                  travelling.actualin,
                  travelling.approveddate,
                  travelling.used,
                  travelling.status,
                  travelling.attachment,
                  FORMAT( travelling.amount, 0 ) AS amount,
                  travelling.remark 
                FROM
                  travelling
                  LEFT JOIN employee ON travelling.employeeid = employee.id
                  LEFT JOIN travelingtype ON travelling.travellingtypeid = travelingtype.id
                  LEFT JOIN city ON travelling.cityid = city.id
                WHERE
                  travelling.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 

        ->addColumn('action', function ($itemdata) { 
            if ($itemdata->status == 1 || $itemdata->status == 2) {
                return ''.$itemdata->notrans.'';
              }else{
                return '<a href="travelling/'.$itemdata->id.'/edit" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
            };  
         })

        ->addColumn('approval', function ($itemdata) {
          return '<a  href="javascript:void(0)" title="Approval" class="btn btn-success btn-xs" onclick="approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-check"></i> Approve</a> <a  href="javascript:void(0)" title="Approval" class="btn btn-warning btn-xs" onclick="not_approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-close"></i> Not Approve</a>';
        })

        ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
            return '<span class="label label-success"> <i class="fa fa-check"></i> Active </span>';
          }else if ($itemdata->status == 9){
           return '<span class="label label-danger"> <i class="fa fa-minus-square"></i> Cancel </span>';
          }else if ($itemdata->status == 2){
           return '<span class="label label-warning"> <i class="fa fa-minus-square"></i> Not Approve </span>';
          }else if ($itemdata->status == 1){
           return '<span class="label label-success"> <i class="fa fa-check"></i> Approve </span>';
        }; 
       })

        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function datahistory(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  travelling.notrans,
                  travelling.datetrans,
                  travelingtype.travelingtypename,
                  city.cityname
                FROM
                  travelling
                LEFT JOIN travelingtype ON travelling.travellingtypeid = travelingtype.id
                LEFT JOIN city ON travelling.cityid = city.id,
                 `user`
                WHERE
                  `user`.id = '.Auth::id().' AND travelling.employeeid = user.employeeid
                AND
                  travelling.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 

        ->make(true);
      } else {
        exit("No data available");
      }
    }

    public function create()
    {  
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      $travellingtype_list = Travellingtype::all()->pluck('travelingtypename', 'id'); 
      $city_list = City::all()->pluck('cityname', 'id'); 

      return view ('editor.travelling.form', compact('employee_list', 'city_list','travellingtype_list'));
    }

    public function store(Request $request)
    { 
        DB::insert("INSERT INTO travelling (codetrans, notrans, datetrans)
                    SELECT 'MUTA',
                    IFNULL(CONCAT('PUNS','-',DATE_FORMAT(NOW(), '%d%m%y'),RIGHT((RIGHT(MAX(travelling.notrans),3))+1001,3)), CONCAT('PUNS','-',DATE_FORMAT(NOW(), '%d%m%y'),'001')), DATE(NOW())  
                    FROM
                    travelling
                    WHERE codetrans='PUNS'");

       $lastInsertedID = DB::table('travelling')->max('id');  

       // dd($lastInsertedID);

       $travelling = Travelling::where('id', $lastInsertedID)->first(); 
       $travelling->employeeid = $request->input('employeeid'); 
       $travelling->travellingtypeid = $request->input('travellingtypeid'); 
       $travelling->travellingfrom = $request->input('travellingfrom'); 
       $travelling->travellingto = $request->input('travellingto');  
       $travelling->cityid = $request->input('cityid');  
       $travelling->actualin = $request->input('actualin');  
       $travelling->amount = str_replace(",","",$request->input('amount'));  
       $travelling->created_by = Auth::id();
       $travelling->save();

       if($request->attachment)
       {
        $travelling = Travelling::FindOrFail($travelling->id);
        $original_directory = "uploads/travelling/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $travelling->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $travelling->attachment);
          $travelling->save(); 
        } 
        return redirect('editor/travelling'); 
     
    }

    public function edit($id)
    {
      $employee_list = Employee::all()->pluck('employeename', 'id'); 
      $travellingtype_list = Travellingtype::all()->pluck('travelingtypename', 'id'); 
      $city_list = City::all()->pluck('cityname', 'id'); 
      $travelling = Travelling::Where('id', $id)->first();   

      // dd($travelling); 
      return view ('editor.travelling.form', compact('travelling','travellingtype_list', 'employee_list','city_list'));
    }

    public function update($id, Request $request)
    {
      
       $travelling = Travelling::FindOrFail($id); 
       $travelling->employeeid = $request->input('employeeid'); 
       $travelling->travellingtypeid = $request->input('travellingtypeid'); 
       $travelling->travellingfrom = $request->input('travellingfrom'); 
       $travelling->travellingto = $request->input('travellingto');  
       $travelling->cityid = $request->input('cityid');  
       $travelling->actualin = $request->input('actualin');  
       $travelling->amount = str_replace(",","",$request->input('amount'));  
       $travelling->status = 0;  
       $travelling->created_by = Auth::id();
       $travelling->save();
 
       $travelling->created_by = Auth::id();
       $travelling->save();

      if($request->attachment)
      {
        $travelling = Travelling::FindOrFail($travelling->id);
        $original_directory = "uploads/travelling/";
        if(!File::exists($original_directory))
          {
            File::makeDirectory($original_directory, $mode = 0777, true, true);
          } 
          $travelling->attachment = Carbon::now()->format("d-m-Y h-i-s").$request->attachment->getClientOriginalName();
          $request->attachment->move($original_directory, $travelling->attachment);
          $travelling->save(); 
        } 
        return redirect('editor/travelling');  
      }

       public function cancel($id, Request $request)
      {
        $post = Travelling::Find($id); 
        $post->status = 9; 
        $post->created_by = Auth::id();
        $post->save(); 
      
        return response()->json($post); 

      }

      public function delete($id)
      {
    //dd($id);
        $post =  Travelling::Find($id);
        $post->delete(); 

        return response()->json($post); 
      }

      public function deletebulk(Request $request)
      {

       $idkey = $request->idkey;   

       foreach($idkey as $key => $id)
       {
    // $post =  Document::where('id', $id["1"])->get();
        $post = Document::Find($id["1"]);
        $post->delete(); 
      }

      echo json_encode(array("status" => TRUE));

    }
  }
