<?php

namespace App\Http\Controllers\Editor;

use File;
use Auth;
use Carbon\Carbon;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\TravellingRequest;
use App\Http\Controllers\Controller;
use App\Model\Travelling; 
use App\Model\Employee; 
use App\Model\Travellingtype;
use App\Model\City;
use Validator;
use Response;
use App\Post;
use View;

class TravellingappController extends Controller
{
  /**
    * @var array
    */
  protected $rules =
  [
    'travellingno' => 'required|min:2|max:32|regex:/^[a-z ,.\'-]+$/i',
    'travellingname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
  ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
      $travellings = Travelling::all();
      return view ('editor.travellingapp.index', compact('travellings'));
    }

    public function data(Request $request)
    {   
      if($request->ajax()){ 

        $sql = 'SELECT
                  travelling.id,
                  travelling.codetrans,
                  travelling.notrans,
                  DATE_FORMAT( travelling.datetrans, "%d/%m/%Y" ) AS datetrans,
                  travelling.employeeid,
                  employee.employeename,
                  travelling.travellingfrom,
                  travelingtype.travelingtypename,
                  travelling.travellingtypeid,
                  travelling.cityid,
                  city.cityname,
                  travelling.travellingto,
                  travelling.actualin,
                  travelling.approveddate,
                  travelling.used,
                  travelling.status,
                  travelling.attachment,
                  FORMAT( travelling.amount, 0 ) AS amount,
                  travelling.remark 
                FROM
                  travelling
                  LEFT JOIN employee ON travelling.employeeid = employee.id
                  LEFT JOIN travelingtype ON travelling.travellingtypeid = travelingtype.id
                  LEFT JOIN city ON travelling.cityid = city.id
                WHERE
                  travelling.deleted_at IS NULL';
        $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 


        return Datatables::of($itemdata) 

        ->addColumn('action', function ($itemdata) { 
            if ($itemdata->status == 1 || $itemdata->status == 2) {
                return ''.$itemdata->notrans.'';
              }else{
                return '<a href="travelling/'.$itemdata->id.'/edit" title="'."'".$itemdata->notrans."'".'"  onclick="edit('."'".$itemdata->id."'".')"> '.$itemdata->notrans.'</a>';
            };  
         })

         ->addColumn('approval', function ($itemdata) {
            return '<a  href="javascript:void(0)" title="Approval" class="btn btn-success btn-xs" onclick="approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-check"></i> Approve</a> <a  href="javascript:void(0)" title="Approval" class="btn btn-warning btn-xs" onclick="not_approve_id('."'".$itemdata->id."', '".$itemdata->notrans."'".')"><i class="fa fa-close"></i> Not Approve</a>';
          })

        ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
            return '<span class="label label-success"> <i class="fa fa-check"></i> Active </span>';
          }else if ($itemdata->status == 9){
           return '<span class="label label-danger"> <i class="fa fa-minus-square"></i> Cancel </span>';
          }else if ($itemdata->status == 2){
           return '<span class="label label-warning"> <i class="fa fa-minus-square"></i> Not Approve </span>';
          }else if ($itemdata->status == 1){
           return '<span class="label label-success"> <i class="fa fa-check"></i> Approve </span>';
        }; 
       })

        ->make(true);
      } else {
        exit("No data available");
      }
    } 


    public function approve($id, Request $request)
    { 
        $post = Travelling::Find($id); 
        $post->status = 1;  
        $post->save();

        return response()->json($post);  
    }

    public function notapprove($id, Request $request)
    { 
        $post = Travelling::Find($id); 
        $post->status = 2;  
        $post->save();

        return response()->json($post);  
    }
  }
