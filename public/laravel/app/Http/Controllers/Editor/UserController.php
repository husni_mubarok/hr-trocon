<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Controllers\Controller;
use App\Model\Employee;
use App\Model\Period;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        //$users = User::paginate(15);
        $users = DB::table('user') 
            ->select('user.id',
                'user.employee_id',
                'user.username',
                'user.email',
                'user.first_name',
                'user.last_name', 
                'user.created_at',
                'user.updated_at') 
            ->whereNull('user.deleted_at')
            ->paginate(15);

            //dd($users);

        return view ('editor.user.index', compact('users'))->with('number',$no);
    }

    public function create()
    {

    	return view ('editor.user.form');
    }

    public function store(Request $request)
    {
        $user = new User;
        $user->username = $request->input('username'); 
        $user->password = bcrypt($request->input('password'));
        $user->save();

        return redirect()->action('Editor\UserController@index');
    }

    public function show($id)
    {
    	$user = User::find($id);
    	return view ('editor.user.detail', compact('user'));
    }

    public function edit($id)
    {
        $user = User::find($id); 
        return view ('editor.user.form', compact('user'));
    }

    public function update($id, PasswordRequest $request)
    {
        $user = User::find($id); 
        $user->password = bcrypt($request->input('password'));
        $user->save();

        return redirect()->action('Editor\UserController@index');
    }

    public function delete($id)
    {
        User::find($id)->delete();
        return redirect()->action('Editor\UserController@index');
    }

    public function periodfilter(Request $request)
    {
      $post = User::where('id', Auth::id())->first();
      $post->departmentid = $request->departmentid;
      $post->periodid = $request->periodid; 
      $post->save();

      $period = Period::where('id', $post->periodid)->first();

      $post1 = User::where('id', Auth::id())->first();
      $post1->begindate = $period->begindate;
      $post1->enddate = $period->paydate; 
      $post1->save();

      return response()->json($period);
  } 

  public function periodfilterthr(Request $request)
    {
      $post = User::where('id', Auth::id())->first(); 
      $post->periodid = $request->periodidfilter; 
      $post->year = $request->year; 
      $post->bmonth = $request->bmonth; 
      $post->emonth = $request->emonth; 
      $post->lastbrutothrmonth = $request->lastbrutothrmonth; 
      $post->lastbrutothr = $request->lastbrutothr;  
      $post->save();
      return response()->json($post);
  } 

  public function periodfilteronly(Request $request)
    {
      $post = User::where('id', Auth::id())->first(); 
      $post->periodid = $request->periodid; 
      $post->departmentid = $request->departmentid; 
      $post->save();
      return response()->json($post);
  } 

  public function periodfilteremp(Request $request)
    {
      $post = User::where('id', Auth::id())->first();
      $post->departmentid = $request->departmentid;
      $post->periodid = $request->periodid; 
      $post->employeeid = $request->employeeid; 
      $post->save();
      return response()->json($post);
  }

   public function periodfilterempyear(Request $request)
    {
      $post = User::where('id', Auth::id())->first();
      $post->departmentid = $request->departmentid;
      $post->year = $request->year; 
      $post->employeeid = $request->employeeid; 
      $post->save();
      return response()->json($post);
  }
}
