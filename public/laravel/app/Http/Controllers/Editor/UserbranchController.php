<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests; 
use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Model\Userbranch;
use App\User;

class UserbranchController extends Controller
{
    public function index()
    {
        if (Input::has('page'))
           {
             $page = Input::get('page');    
           }
        else
           {
             $page = 1;
           }
        $no = 15*$page-14; 
        //$users = User::paginate(15);
        $users = DB::table('user') 
            ->select('user.id',
                'user.employee_id',
                'user.username',
                'user.email',
                'user.first_name',
                'user.last_name', 
                'user.created_at',
                'user.updated_at') 
            ->whereNull('user.deleted_at')
            ->get();

            //dd($users);

        return view ('editor.userbranch.index', compact('users'))->with('number',$no);
    }

    public function create()
    {

    	return view ('editor.user.form');
    }

    public function show($id)
    {
    	$user = User::find($id);
    	return view ('editor.userbranch.detail', compact('user'));
    }

    public function edit($id)
    {
        $user = User::find($id); 
        $branch_list = Branch::pluck('branch_name', 'id');
        $sql = 'SELECT
                    branch.branch_name,
                    user_branch.id,
                    user_branch.user_id
                FROM
                    user_branch
                INNER JOIN branch ON user_branch.branch_id = branch.id';
        $branch_user = DB::table(DB::raw("($sql) as rs_sql"))->where('user_id', $id)->get();

        return view ('editor.userbranch.form', compact('user', 'branch_list', 'branch_user'));
    }

    public function update($id, Request $request)
    {
        $user = new Userbranch;
        $user->branch_id = $request->input('branch_id'); 
        $user->user_id = $id;
        $user->save();

        $user->save();

        return redirect()->action('Editor\UserbranchController@edit', $id);
    }

    public function delete($id)
    {
        Userbranch::find($id)->delete();
        return redirect()->back();
        
    }
}
