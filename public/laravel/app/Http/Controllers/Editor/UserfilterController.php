<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Model\User; 
use App\Model\Userlog;  
use App\Model\Container;
use App\Model\Itembrand; 
use App\Model\Itemcategory; 
use Carbon\Carbon;
use Validator;
use Response;
use App\Post;
use View;
// use App\Notifications\ToDb;
use Illuminate\Notifications\Notifiable;


class UserfilterController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'userfiltername' => ''
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $userfilters = User::all();
    $item_category = Itemcategory::orderBy('item_category_name', 'ASC')->get();
    $container = Container::orderBy('container_name', 'ASC')->get();

    return view ('editor.userfilter.index', compact('userfilters', 'item_category', 'container'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      // $itemdata = User::orderBy('userfilter_name', 'ASC')->get();
      $sql = 'SELECT
                `user`.id,
                `user`.username,
                `user`.grfrom,
                `user`.grto,
                `user`.item_category_id,
                item_category.item_category_name,
                `user`.container_id,
                container.container_no,
                `user`.search 
              FROM
                `user`
                LEFT JOIN item_category ON `user`.item_category_id = item_category.id
                LEFT JOIN container ON `user`.container_id = container.id';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a>';
      })
  
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function edit($id)
  {
    $userfilter = User::Find($id);
    echo json_encode($userfilter); 
  }

  public function update($id, Request $request)
  { 
    //data
    $post = User::Find($id); 
    $post->grfrom = $request->grfrom; 
    $post->grto = $request->grto; 
    $post->item_category_id = $request->item_category_id; 
    $post->container_id = $request->container_id; 
    $post->search = $request->search; 
    $post->updated_by = Auth::id();
    $post->save(); 

    return response()->json($post); 
  }
 
}
