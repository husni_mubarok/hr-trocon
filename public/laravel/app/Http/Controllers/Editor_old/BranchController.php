<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\BranchRequest;
use App\Http\Controllers\Controller;
use App\Model\Branch; 
use Validator;
use Response;
use App\Post;
use View;

class BranchController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'branchname' => 'required|min:2|max:128|regex:/^[a-z ,.\'-]+$/i'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $branchs = Branch::all();
    return view ('editor.branch.index', compact('branchs'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
      $itemdata = Branch::orderBy('branch_name', 'ASC')->get();

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->branch_name."', '".$itemdata->field_name."'".')"> Delete</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Branch(); 
    $post->branch_name = $request->branchname; 
    $post->description = $request->description; 
    $post->field_name = $request->field_name; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    DB::insert("ALTER TABLE purchase_order_detail
                ADD COLUMN ".$request->field_name." DOUBLE;");

    DB::insert("ALTER TABLE purchase_order_detail_log
                ADD COLUMN ".$request->field_name." DOUBLE;");

    DB::insert("ALTER TABLE purchase_order_detail_log
                ADD COLUMN ".$request->field_name."_to DOUBLE;");

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    $branch = Branch::Find($id);
    echo json_encode($branch); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Branch::Find($id); 
    $post->branch_name = $request->branchname; 
    $post->description = $request->description; 
    // $post->field_name = $request->field_name; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id, Request $request)
  {
    //dd($id);
    DB::insert("ALTER TABLE purchase_order_detail
            DROP COLUMN ".$request->field_name.";");

    $post =  Branch::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Branch::where('id', $id["1"])->get();
    $post = Branch::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
