<?php

namespace App\Http\Controllers\Editor;

use Auth;
use Datatables;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\DailyrateRequest;
use App\Http\Controllers\Controller;
use App\Model\Dailyrate; 
use App\Model\Currency; 
use Validator;
use Response;
use App\Post;
use View;

class DailyrateController extends Controller
{
  /**
    * @var array
    */
    protected $rules =
    [ 
        'rate' => 'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
  public function index()
  {
    $currency_list = Currency::all();
    $sql = 'SELECT
              daily_rate.id,
              daily_rate.currency_id,
              daily_rate.date,
              FORMAT(daily_rate.rate,0) AS rate,
              FORMAT(daily_rate.plusrate,0) AS plusrate,
              daily_rate.description,
              daily_rate.`status`,
              daily_rate.created_by,
              daily_rate.updated_by,
              daily_rate.deleted_by,
              daily_rate.created_at,
              daily_rate.updated_at,
              currency.currency_name
            FROM
              daily_rate
            LEFT JOIN currency ON daily_rate.currency_id = currency.id
            WHERE
            daily_rate.deleted_at IS NULL';
    $dailyrates = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

    return view ('editor.dailyrate.index', compact('dailyrates', 'currency_list'));
  }

  public function data(Request $request)
  {   
    if($request->ajax()){ 
       $sql = 'SELECT
              daily_rate.id,
              DATE_FORMAT(daily_rate.date, "%d-%m-%Y") AS date,
              FORMAT(daily_rate.rate,0) AS rate,
              FORMAT(daily_rate.plusrate,0) AS plusrate,
              daily_rate.description,
              daily_rate.`status`,
              daily_rate.created_by,
              daily_rate.updated_by,
              daily_rate.deleted_by,
              daily_rate.created_at,
              daily_rate.updated_at,
              currency.currency_name
            FROM
              daily_rate
            LEFT JOIN currency ON daily_rate.currency_id = currency.id
            WHERE
            daily_rate.deleted_at IS NULL';
      $itemdata = DB::table(DB::raw("($sql) as rs_sql"))->get(); 

      return Datatables::of($itemdata) 

      ->addColumn('action', function ($itemdata) {
        return '<a href="javascript:void(0)" title="Edit"  onclick="edit('."'".$itemdata->id."'".')"> Edit</a> | <a  href="javascript:void(0)" title="Delete" onclick="delete_id('."'".$itemdata->id."', '".$itemdata->currency_name."', '".$itemdata->date."'".')"> Delete</a>';
      })

      ->addColumn('check', function ($itemdata) {
        return '<label class="control control--checkbox"> <input type="checkbox" class="data-check" value="'."'".$itemdata->id."'".'"> <div class="control__indicator"></div> </label>';
      })

      ->addColumn('mstatus', function ($itemdata) {
        if ($itemdata->status == 0) {
          return '<span class="label label-success"> Active </span>';
        }else{
         return '<span class="label label-danger"> Not Active </span>';
       };

     })
      ->make(true);
    } else {
      exit("No data available");
    }
  }

  public function store(Request $request)
  { 
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = new Dailyrate(); 
    $post->currency_id = $request->currency_id; 
    $post->date = $request->date; 
    $post->rate = str_replace(",", "", $request->rate); 
    $post->plusrate = str_replace(",", "", $request->plusrate); 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->created_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function edit($id)
  {
    // $dailyrate = Dailyrate::Find($id);
     $sql = 'SELECT
              daily_rate.id,
              daily_rate.date,
              daily_rate.currency_id,
              FORMAT(daily_rate.rate,0) AS rate,
              FORMAT(daily_rate.plusrate,0) AS plusrate,
              daily_rate.description,
              daily_rate.`status`,
              daily_rate.created_by,
              daily_rate.updated_by,
              daily_rate.deleted_by,
              daily_rate.created_at,
              daily_rate.updated_at,
              currency.currency_name
            FROM
              daily_rate
            LEFT JOIN currency ON daily_rate.currency_id = currency.id
            WHERE
            daily_rate.deleted_at IS NULL';
      $dailyrate = DB::table(DB::raw("($sql) as rs_sql"))->where('id', $id)->first(); 
    echo json_encode($dailyrate); 
  }

  public function update($id, Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
    $post = Dailyrate::Find($id); 
    $post->currency_id = $request->currency_id; 
    $post->date = $request->date; 
    $post->rate = str_replace(",", "", $request->rate); 
    $post->plusrate = str_replace(",", "", $request->plusrate); 
    $post->description = $request->description; 
    $post->status = $request->status;
    $post->updated_by = Auth::id();
    $post->save();

    return response()->json($post); 
  }
  }

  public function delete($id)
  {
    //dd($id);
    $post =  Dailyrate::Find($id);
    $post->delete(); 

    return response()->json($post); 
  }

  public function deletebulk(Request $request)
  {

   $idkey = $request->idkey;   

  //$count = count($idkey);
   
//    $i = 0;
// dd($idkey[$i]);

//    $idkey = (object) $idkey;
// dd($idkey);

   foreach($idkey as $key => $id)
   {
    // $post =  Dailyrate::where('id', $id["1"])->get();
    $post = Dailyrate::Find($id["1"]);
    $post->delete(); 
  }

  echo json_encode(array("status" => TRUE));

}
}
