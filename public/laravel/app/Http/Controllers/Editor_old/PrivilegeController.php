<?php

namespace App\Http\Controllers\Editor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Privilege;
use App\User;
use App\Model\Module;
use App\Model\Action;

class PrivilegeController extends Controller
{
    public function index()
    {
    	$users = User::has('privilege')->paginate(15);
    	return view ('editor.privilege.index', compact('users'));
    }

    public function create()
    {
    	$username_list = User::doesntHave('privilege')->pluck('username', 'id');
    	$module_list = Module::pluck('name', 'id');
    	$action_list = Action::pluck('name', 'id');
    	return view ('editor.privilege.form', compact('username_list', 'module_list', 'action_list'));
    }

    public function store(Request $request)
    {
    	if($request->input('privilege'))
    	{
    		foreach($request->input('privilege') as $module_id => $action_list)
    		{
    			foreach($action_list as $action_id => $value)
    			{
	    			$privilege = new Privilege;
	    			$privilege->user_id = $request->input('user_id');
	    			$privilege->module_id = $module_id;
	    			$privilege->action_id = $action_id;
	    			$privilege->save();
    			}
    		}
    	}

    	return redirect()->action('Editor\PrivilegeController@index');
    }

    public function edit($id)
    {
        $user = User::find($id);
        $module_list = Module::pluck('name', 'id');
        $action_list = Action::pluck('name', 'id');
        return view ('editor.privilege.form', compact('user', 'module_list', 'action_list'));
    }

    public function update($id, Request $request)
    {
        if($request->input('privilege'))
        {
            Privilege::where('user_id', $id)->delete();
            foreach($request->input('privilege') as $module_id => $action_list)
            {
                foreach($action_list as $action_id => $value)
                {
                    $privilege = new Privilege;
                    $privilege->user_id = $id;
                    $privilege->module_id = $module_id;
                    $privilege->action_id = $action_id;
                    $privilege->save();
                }
            }
        }

        return redirect()->action('Editor\PrivilegeController@index');
    }

    public function delete($id)
    {
        Privilege::where('user_id', $id)->delete();
        return redirect()->action('Editor\PrivilegeController@index');
    }
}
