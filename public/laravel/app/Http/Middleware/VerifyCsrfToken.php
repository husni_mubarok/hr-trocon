<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'editor/payroll/calculation/payslip/*',
        'editor/payroll/calculation/report/*',
        'editor/payroll/thr/payslip/*',
        'editor/payroll/thr/report/*',
        'editor/payroll/bonus/payslip/*',
        'editor/payroll/bonus/report/*'
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->isReading($request) || $this->tokensMatch($request)) {
            return $this->addCookieToResponse($request, $next($request));
        }
        
        return redirect("/")->with("alert", "error message to user interface");
        #throw new TokenMismatchException;
    }
    
}