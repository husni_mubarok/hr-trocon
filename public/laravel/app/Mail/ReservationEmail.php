<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue; 

class ReservationEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $month;
    public $year;
    public $path;

    public function __construct($month, $year, $path)
    {
        $this->month = $month;
        $this->year = $year;
        $this->path = $path;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'trocon.payroll@gmail.com';
        $name = 'HR Trocon';
        $subject = 'Payslip ' . $this->month . ' ' . $this->year;

        return $this->view('email_send_payslip')
                    ->from($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject)
                    ->attach($this->path, [
                        'as' => 'payslip.xls',
                        'mime' => 'application/xls'
                    ]);
    }
}
