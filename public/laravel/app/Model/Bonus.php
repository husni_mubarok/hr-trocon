<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bonus extends Model
{
	use SoftDeletes;
	protected $table = 'bonus';
	protected $dates = ['deleted_at'];  

	public function bonusDets()
  	{
  		return $this->hasMany('App\Model\Bonusdetail', 'transid');
	}
}
