<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bonusdetail extends Model
{
	use SoftDeletes;
	protected $table = 'bonusdet';
	protected $dates = ['deleted_at'];  

	public function bonus()
  	{
  		return $this->belongsTo('App\Model\Bonus', 'transid');
	}
	
	public function employee()
  	{
  		return $this->belongsTo('App\Model\Employee', 'employeeid')->withTrashed();
	}
}
