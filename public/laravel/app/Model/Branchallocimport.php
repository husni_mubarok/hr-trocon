<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branchallocimport extends Model
{
	use SoftDeletes;
	protected $table = 'branch_alloc_import';
	protected $dates = ['deleted_at'];  

}
