<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Container extends Model
{
	use SoftDeletes;
	protected $table = 'container';
	protected $dates = ['deleted_at'];  

}
