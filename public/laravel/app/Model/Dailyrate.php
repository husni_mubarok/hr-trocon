<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Dailyrate extends Model
{
	use SoftDeletes;
	protected $table = 'daily_rate';
	protected $dates = ['deleted_at'];  

}
