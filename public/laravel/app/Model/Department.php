<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
	use SoftDeletes;
	protected $table = 'department';
	protected $dates = ['deleted_at'];  

	public function employees()
    {
    	return $this->belongsTo('App\Models\Employee')->withTrashed();	
    }
}
