<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Emklcost extends Model
{
	use SoftDeletes;
	protected $table = 'emkl_cost';
	protected $dates = ['deleted_at'];  

}
