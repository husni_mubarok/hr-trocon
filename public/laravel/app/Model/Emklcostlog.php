<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Emklcostlog extends Model
{
	use SoftDeletes;
	protected $table = 'emkl_cost_log';
	protected $dates = ['deleted_at'];  

}
