<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Employee extends Model
{
	use SoftDeletes;
	protected $table = 'employee';
	protected $dates = ['deleted_at'];  

	public function corrections()
	{
		return $this->hasMany('App\Model\Payroll\Correction');
  	}

  	public function payrolls()
	{
	    return $this->hasMany('App\Model\Payroll');
	}

  	public function payslips()
  	{
  		return $this->hasMany('App\Model\Payroll\Payslip');
	}

	public function thrPayslips()
  	{
  		return $this->hasMany('App\Model\Payroll\Thr');
	}

	public function bonusPayslips()
  	{
  		return $this->hasMany('App\Model\Payroll\Bonus');
	}

	public function thrDets()
  	{
  		return $this->hasMany('App\Model\Thrdet');
	}

	public function bonusDets()
  	{
  		return $this->hasMany('App\Model\Bonusdetail');
	}
	  
	public function jobtitle()
	{
		return $this->belongsTo('App\Model\Position', 'positionid')->withTrashed();
	}

	public function department()
	{
		return $this->belongsTo('App\Model\Department', 'departmentid')->withTrashed();
	}
}
