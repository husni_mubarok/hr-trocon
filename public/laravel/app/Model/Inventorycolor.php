<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventorycolor extends Model
{
	use SoftDeletes;
	protected $table = 'inventorycolor';
	protected $dates = ['deleted_at'];  

}
