<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Itembrand extends Model
{
	use SoftDeletes;
	protected $table = 'item_brand';
	protected $dates = ['deleted_at'];  

}
