<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Itemcategory extends Model
{
	use SoftDeletes;
	protected $table = 'item_category';
	protected $dates = ['deleted_at'];  

}
