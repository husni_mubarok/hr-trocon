<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Itemunit extends Model
{
	use SoftDeletes;
	protected $table = 'item_unit';
	protected $dates = ['deleted_at'];  

}
