<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Leaving extends Model
{
	use SoftDeletes;
	protected $table = 'leaving';
	protected $dates = ['deleted_at'];  

}
