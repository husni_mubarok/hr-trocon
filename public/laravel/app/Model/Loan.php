<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Loan extends Model
{
	use SoftDeletes;
	protected $table = 'loan';
	protected $dates = ['deleted_at'];  

}
