<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Loandetail extends Model
{
	use SoftDeletes;
	protected $table = 'loandet';
	protected $dates = ['deleted_at'];  

}
