<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Logovertime extends Model
{
	use SoftDeletes;
	protected $table = 'logovertime';
	protected $dates = ['deleted_at'];  

}
