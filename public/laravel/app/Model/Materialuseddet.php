<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Materialuseddet extends Model
{
	use SoftDeletes;
	protected $table = 'materialuseddet';
	protected $dates = ['deleted_at'];  

}
