<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mutation extends Model
{
	use SoftDeletes;
	protected $table = 'mutation';
	protected $dates = ['deleted_at'];  

}
