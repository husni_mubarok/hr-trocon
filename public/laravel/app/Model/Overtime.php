<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Overtime extends Model
{
	use SoftDeletes;
	protected $table = 'overtimerequest';
	protected $dates = ['deleted_at'];  

}
