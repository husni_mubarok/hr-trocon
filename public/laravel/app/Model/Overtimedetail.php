<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Overtimedetail extends Model
{
	use SoftDeletes;
	protected $table = 'overtimerequestdet';
	protected $dates = ['deleted_at'];  

}
