<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payperiod extends Model
{
	use SoftDeletes;
	protected $table = 'payperiod';
	protected $dates = ['deleted_at'];  

	public function payslips()
  	{
  		return $this->hasMany('App\Model\Payroll\Payslip');
  	}

  	public function payrolls()
  	{
  		return $this->hasMany('App\Model\Payroll', 'periodid', 'id');
  	}
}
