<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payperiodbonus extends Model
{
	use SoftDeletes;
protected $table = 'payperiodbonus';
	protected $dates = ['deleted_at'];  

}
