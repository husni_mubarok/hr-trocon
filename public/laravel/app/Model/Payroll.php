<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payroll extends Model
{
	use SoftDeletes;
	protected $table = 'payroll';
	protected $dates = ['deleted_at'];  

	public function period()
  	{
  		return $this->belongsTo('App\Model\Payperiod', 'periodid');
  	}

  	public function employee()
  	{
  		return $this->belongsTo('App\Model\Employee', 'employeeid')->withTrashed();
  	}
}
