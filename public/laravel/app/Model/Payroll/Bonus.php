<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    protected $table = 'bonus_payslips';
    protected $guarded = [];
    
    public $timestamps = true;

    public function employee()
    {
    	return $this->belongsTo('App\Model\Employee')->withTrashed();
    }
}
