<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class Correction extends Model
{
	protected $table = 'payroll_corrections';
	protected $guarded = [];
    
	public function employee()
	{
		return $this->belongsTo('App\Model\Employee')->withTrashed();
	}
}
