<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class MasterPtkp extends Model
{
    protected $table = 'payroll_master_ptkps';
    protected $guarded = [];
    
    public $timestamps = true;
}
