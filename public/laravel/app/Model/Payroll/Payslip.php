<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class Payslip extends Model
{
    protected $table = 'payroll_payslips';
    protected $guarded = [];
    
    public $timestamps = true;

    public function employee()
    {
    	return $this->belongsTo('App\Model\Employee')->withTrashed();
    }

    public function payrollPeriod()
    {
    	return $this->belongsTo('App\Model\Payperiod');
    }

    public function payslipItems()
    {
        return $this->hasMany('App\Model\Payroll\PayslipItem');
    }
}
