<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class PayslipItem extends Model
{
    const IS_BASIC_SALARY = 'BASIC_SALARY';
    const IS_ALLOWANCE_REGULAR = 'REGULAR_ALLOWANCE';
    const IS_ALLOWANCE_NON_REGULAR = 'non_regular_allowance';
    const IS_CLAIM = 'CLAIM';
    const IS_LOAN = 'LOAN';
    const IS_TAX = 'TAX';
    const IS_ASSURANCE = 'ASSURANCE';
    const IS_ASSURANCE_ALLOWANCE = 'ASSURANCE_ALLOWANCE';

    const JHT = 'JHT';
    const JP = 'JP';
    const JKK = 'JKK';
    const JKM = 'JKM';

    const TYPE_EARN = 'EARN';
    const TYPE_DEDUCT = 'DEDUCT';
    const TYPE_PAID_COMPANY = 'PAID_COMPANY';

    protected $table = 'payroll_payslip_items';
    protected $guarded = [];
    
    public $timestamps = true;

    public function payslip()
    {
    	return $this->belongsTo('App\Model\Payroll\Payslip');
    }
}
