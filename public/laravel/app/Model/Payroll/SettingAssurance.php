<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class SettingAssurance extends Model
{
    protected $table = 'payroll_setting_assurances';
	  protected $guarded = [];
	    
	  public $timestamps = true;
}
