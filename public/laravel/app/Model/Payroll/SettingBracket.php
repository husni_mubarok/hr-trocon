<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class SettingBracket extends Model
{
    protected $table = 'payroll_setting_brackets';
	protected $guarded = [];
	    
	public $timestamps = true;
}
