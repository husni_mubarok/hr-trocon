<?php

namespace App\Model\Payroll;

use Illuminate\Database\Eloquent\Model;

class Thr extends Model
{
    protected $table = 'thr_payslips';
    protected $guarded = [];
    
    public $timestamps = true;

    public function employee()
    {
    	return $this->belongsTo('App\Model\Employee')->withTrashed();
    }
}
