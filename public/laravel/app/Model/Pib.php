<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Pib extends Model
{
	use SoftDeletes;
	protected $table = 'pib';
	protected $dates = ['deleted_at'];  

}
