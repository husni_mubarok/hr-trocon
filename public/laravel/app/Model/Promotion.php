<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model
{
	use SoftDeletes;
	protected $table = 'promotion';
	protected $dates = ['deleted_at'];  

}
