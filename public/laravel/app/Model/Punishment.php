<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Punishment extends Model
{
	use SoftDeletes;
	protected $table = 'punishment';
	protected $dates = ['deleted_at'];  

}
