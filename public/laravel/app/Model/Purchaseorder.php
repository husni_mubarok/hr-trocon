<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Purchaseorder extends Model
{
	use SoftDeletes;
	protected $table = 'purchase_order';
	protected $dates = ['deleted_at'];  

}
