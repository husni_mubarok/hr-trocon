<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Purchaseorderdetaillog extends Model
{
	use SoftDeletes;
	protected $table = 'purchase_order_detail_log';
	protected $dates = ['deleted_at'];  

}
