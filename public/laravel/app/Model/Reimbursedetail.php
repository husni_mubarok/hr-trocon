<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class  Reimbursedetail extends Model
{
	use SoftDeletes;
	protected $table = 'reimbursedet';
	protected $dates = ['deleted_at'];  

}
