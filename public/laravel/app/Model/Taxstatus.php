<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Taxstatus extends Model
{
	use SoftDeletes;
	protected $table = 'ptkp';
	protected $dates = ['deleted_at'];  

}
