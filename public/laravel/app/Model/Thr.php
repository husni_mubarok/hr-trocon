<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Thr extends Model
{
	use SoftDeletes;
	protected $table = 'thr';
	protected $dates = ['deleted_at'];  

	public function thrDets()
  	{
  		return $this->hasMany('App\Model\Thrdet', 'transid');
	}
}
