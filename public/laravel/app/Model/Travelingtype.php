<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Travelingtype extends Model
{
	use SoftDeletes;
	protected $table = 'travelingtype';
	protected $dates = ['deleted_at'];  

}
