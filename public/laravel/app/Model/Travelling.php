<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Travelling extends Model
{
	use SoftDeletes;
	protected $table = 'travelling';
	protected $dates = ['deleted_at'];  

}
