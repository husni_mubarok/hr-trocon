<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Week extends Model
{
    protected $table = 'week';
    protected $dates = ['deleted_at']; 
}
