<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Year extends Model
{
	use SoftDeletes;
	protected $table = 'year';
	protected $dates = ['deleted_at'];  

}
