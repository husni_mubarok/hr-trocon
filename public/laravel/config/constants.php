<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Directory Paths
    |--------------------------------------------------------------------------
    |
    | This value determines the directory path for the assets you use such as
    | CSS, js, plug-ins, etc.
    | 
    */

    'path' => [
        'uploads' => '/trocon2/hr-trocon/public/uploads',
        'bootstrap' => '/trocon2/hr-trocon/public/laravel/bootstrap',
        'css' => '/trocon2/hr-trocon/public/assets/css',
        'scss' => '/trocon2/hr-trocon/public/assets/lte_sass/build/scss',
        'img' => '/trocon2/hr-trocon/public/assets/img',
        'js' => '/trocon2/hr-trocon/public/assets/js',
        'plugin' => '/trocon2/hr-trocon/public/assets/plugins',
        'swf' => '/trocon2/hr-trocon/public/assets/swf',
        '404' => '/trocon2/hr-trocon/public/assets/404'
    ],

];