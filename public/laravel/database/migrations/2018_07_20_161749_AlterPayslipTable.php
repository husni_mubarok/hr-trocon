<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPayslipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payroll_payslips', function (Blueprint $table) {
            $table->dropColumn('component_take_home_pay');
            $table->text('component_take_home_pay_first')->nullable();
            $table->text('component_absence_deduction')->nullable();
            $table->text('component_take_home_pay_second')->nullable();
            $table->text('component_pre_salary')->nullable();
            $table->text('component_take_home_pay_third')->nullable();
            $table->text('component_loan')->nullable();
            $table->text('component_take_home_pay_fourth')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payroll_payslips', function ($table) {
            $table->dropColumn(['component_take_home_pay_first', 'component_absence_deduction', 'component_take_home_pay_second', 'component_pre_salary', 'component_take_home_pay_third', 'component_loan', 'component_take_home_pay_fourth']);
        });
    }
}
