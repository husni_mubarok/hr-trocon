<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUseJamsostekRetireFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payroll_payslips', function (Blueprint $table) {
            $table->boolean('use_jamsostek')->nullable();
            $table->boolean('use_iuran_pensiun')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payroll_payslips', function (Blueprint $table) {
            $table->dropColumn('use_jamsostek');
            $table->dropColumn('use_iuran_pensiun');
        });
    }
}
