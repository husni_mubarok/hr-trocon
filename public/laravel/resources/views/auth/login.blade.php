@extends('layouts.auth.template')

@section('content')
<div class="login-logo">
    

     <!-- <span class="logo-lg"><img src="{{Config::get('constants.path.img')}}/logo.png" alt="HRIS"></span> -->
</div>
<!-- /.login-logo -->
<div class="login-box-body">
    <div class="login-logo">
     <span class="logo-lg"><img src="{{Config::get('constants.path.img')}}/logo.png" alt="Payroll - PT. Golden Harvest Cocoa Indonesia"></span>
    </div>
    <p> Welcome to the HRIS System. To continue, please login using your username and password below.</p><br>
            <h4 class="login-box-msg"><b>Press login to continue</b></h4>

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
    {{ csrf_field() }}
        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            <input id="username" type="username" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username" required autofocus>

            @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">   
            {{ Form::hidden('branch_id', $list_branch, old('branch_id'), array('class' => 'form-control', 'required' => 'true')) }}
        </div>                    

        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i>&nbsp;Sign In</button>
            </div>
        <!-- /.col -->
        </div>
    </form> 
    <hr>
          <a class="btn btn-warning btn-flat btn-xs">2.2</a> <a href="#">Built in  08/09/2017 02:37 pm</a><br>
    <hr>
    
</div>
<!-- /.login-box-body -->
@stop
