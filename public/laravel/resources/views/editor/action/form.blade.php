@extends('layouts.editor.template')
@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-user"></i> Action
    <small>Auth</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{url('/')}}/editor"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Auth</a></li>
    <li class="active">Action</li>
  </ol>
</section>
@actionStart('action', 'create|update')
<section class="content">
	<div class="col-md-6 col-sm-6 col-xs-6"> 
		<section class="content box box-solid">
			<div class="row">
			    <div class="col-md-12 col-sm-12 col-xs-12">
			    	<div class="col-md-1"></div>
			    	<div class="col-md-12">
				        <div class="x_panel">
			                <h2>
			                	@if(isset($action))
			                	<i class="fa fa-pencil"></i>
			                	@else
			                	<i class="fa fa-plus"></i>
			                	@endif
			                	&nbsp;Action
		                	</h2>
			                <hr>
				            <div class="x_content">
				                @include('errors.error')

				                @if(isset($action))
				                {!! Form::model($action, array('route' => ['editor.action.update', $action->id], 'method' => 'PUT', 'files' => 'true'))!!}
			                    @else
			                    {!! Form::open(array('route' => 'editor.action.store', 'files' => 'true'))!!}
			                    @endif
			                    {{ csrf_field() }}
			                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
			                    	{{ Form::label('name', 'Name') }}
			                    	@if(isset($action))
			                    	{{ Form::text('name', old('name'), ['class' => 'form-control', 'disabled' => 'true']) }}
			                    	@else
			                    	{{ Form::text('name', old('name'), ['class' => 'form-control']) }}
			                    	@endif
			                    	<br>

			                    	{{ Form::label('description', 'Description') }}
			                    	{{ Form::text('description', old('description'), ['class' => 'form-control']) }}
			                    	<br>

		                            <button type="submit" class="btn btn-primary-ghci btn-flat pull-right"><i class="fa fa-check"></i> Save</button>
		                    	</div>
		                        {!! Form::close() !!}
				            </div>
				        </div>
			        </div>
			    </div>
			</div>
		</section>
	</div>
</section>
@actionEnd
@stop
