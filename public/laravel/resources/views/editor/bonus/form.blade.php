 @extends('layouts.editor.template')
 @section('title', 'Bonus')
 @section('content')

 <style type="text/css">
 	.input-smform {
  height: 22px;
  padding: 1px 3px;
  font-size: 12px;
  line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
  border-radius: 0px;
  width: 95% !important;
}

th,td  {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
 </style>
 <!-- Content Header (Page header) -->
 <section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
 	<h4>
 		Bonus
 	</h4>
 	<ol class="breadcrumb">
 		<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
 		<li><a href="#">Master Data</a></li>
 		<li class="active">Bonus</li>
 	</ol>
 </section>

 <section class="content">
 	<section class="content box box-danger">
 		<div class="row">
			<div class="col-md-12"> 
				{!! Form::model($bonus, array('route' => ['editor.bonus.updatedetail', $bonus->id, $bonus->departmentid], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_bonus'))!!}
					<div class="box-body"> 
						<table class="table table-bordered table-hover stripe" id="bonusTable">
							<thead>
								<tr>
									<th>Karyawan</th>
									<th>Tanggal Masuk </th> 
									<th>NPWP</th> 
									<th>Status Pajak</th>
									<th>Gaji</th>
									<th>Grade</th>
									<th>Nilai</th>
									<th>Bonus</th>
									<th>Koreksi Bonus</th>
									<th>Total Bonus</th>
									<th>Tarif</th>
									<th>PPH 21</th>
									<th>Pot Pinjaman</th>
									<th>Netto</th>
								</tr>
							</thead>
							@php
								$tbonus = 0;
								$tadjustbonus = 0;
								$ttbonus = 0;
								$tpph21 = 0;
								$tpotpinjaman = 0;
								$tnetto = 0;
							@endphp
							<tbody> 
								@foreach($bonus_detail as $key => $bonus_details)
								<tr>
									<td class="col-sm-1 col-md-1">
										{{$bonus_details->employeename}} 
									</td>
									<td class="col-sm-1 col-md-1">
										{{$bonus_details->joindate}} 
									</td>
									<td class="col-sm-1 col-md-1">
										{{$bonus_details->npwp}} 
									</td>  
									<td class="col-sm-1 col-md-1">  
										{{$bonus_details->taxstatus}} 
									</td> 
									<td class="col-sm-1 col-md-1">  
										{{number_format($bonus_details->basic,0)}} 
									</td> 
									<td class="col-sm-1 col-md-1">  
										{{ Form::text('detail['.$bonus_details->id.'][grade]', old($bonus_details->id.'[grade]', $bonus_details->grade), ['class' => 'form-control input-smform', 'id' => 'grade'.$bonus_details->id, 'min' => '0', 'oninput' => 'cal_sparator_thr('.$bonus_details->id.');']) }} 
									</td> 
									<td class="col-sm-1">  
										{{ Form::number('detail['.$bonus_details->id.'][value]', old($bonus_details->id.'[value]', $bonus_details->value), ['class' => 'form-control input-smform', 'id' => 'value'.$bonus_details->id, 'min' => '0', 'oninput' => 'cal_sparator_thr('.$bonus_details->id.');']) }} 
									</td> 
									<td class="col-sm-1">   
										{{number_format($bonus_details->bonus,0)}} 
									</td> 
									<td class="col-sm-1">  
										{{ Form::text('detail['.$bonus_details->id.'][adjustbonus]', old($bonus_details->id.'[adjustbonus]', $bonus_details->adjustbonus), ['class' => 'form-control input-smform', 'id' => 'adjustbonus'.$bonus_details->id, 'placeholder' => 'Koreksi Bonus*', 'min' => '0', 'oninput' => 'cal_sparator_adjustbonus('.$bonus_details->id.');']) }} 
									</td> 
									<td class="col-sm-1">  
										{{number_format($bonus_details->tbonus,0)}} 
									</td> 
									<td class="col-sm-1 col-md-1">  
										{{$bonus_details->tarif}} 
									</td> 
									<td class="col-sm-1">  
										{{ Form::text('detail['.$bonus_details->id.'][pph21]', old($bonus_details->id.'[pph21]', $bonus_details->pph21), ['class' => 'form-control input-smform', 'id' => 'pph21'.$bonus_details->id, 'placeholder' => 'Pph21', 'min' => '0', 'oninput' => 'cal_sparator_pph21('.$bonus_details->id.');']) }} 
									</td>   
									<td class="col-sm-1">  
										{{ Form::text('detail['.$bonus_details->id.'][potpinjaman]', old($bonus_details->id.'[potpinjaman]', $bonus_details->potpinjaman), ['class' => 'form-control input-smform', 'id' => 'potpinjaman'.$bonus_details->id, 'placeholder' => 'Pt Pinjaman', 'min' => '0', 'oninput' => 'cal_sparator_potpinjaman('.$bonus_details->id.');']) }} 
									</td>   
									<td class="col-sm-1">  
										{{number_format($bonus_details->netto,0)}} 
									</td>
								</td>  
							</tr>  
							@php  

								$tbonus += str_replace(",","",$bonus_details->bonus);
								$tadjustbonus += str_replace(",","",$bonus_details->adjustbonus);
								$ttbonus += str_replace(",","",$bonus_details->tbonus);
								$tpph21 += str_replace(",","",$bonus_details->pph21);
								$tpotpinjaman += str_replace(",","",$bonus_details->potpinjaman);
								$tnetto += str_replace(",","",$bonus_details->netto);
							@endphp
							@endforeach

						</tbody>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th align="right">Total</th>
								<th>@php echo number_format($tbonus,0); @endphp</th>
								<th>@php echo number_format($tadjustbonus,0); @endphp</th>
								<th>@php echo number_format($ttbonus,0); @endphp</th>
								<th>@php echo number_format($tpph21,0); @endphp</th> 
								<th>@php echo number_format($tpotpinjaman,0); @endphp</th>
								<th>@php echo number_format($tnetto,0); @endphp</th>
							</tr>
						</tfoot>
					</table>
				</div> 
				<!-- /.box-body -->
				<a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Submit</a>
				<a href="{{ URL::route('editor.bonus.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
			{!! Form::close() !!} 
 			</div>
 		</div>
 	</div>
 </div>
</section>
</section>

@stop 

@section('scripts')

<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#bonusTable").DataTable({
			"sScrollX": true,
	         "scrollY": "350px",
	         "bPaginate": false,
	         "autoWidth": false,
	         "ordering": false
		});
	});
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
</script>  
<script type="text/javascript"> 
	$('#btn_submit').on('click', function()
	{ 
		$.confirm({
			title: 'Confirm!',
			content: 'Are you sure to create data?',
			type: 'green',
			typeAnimated: true,
			buttons: {
				cancel: {
					action: function () { 
					}
				},
				confirm: {
					text: 'CREATE',
					btnClass: 'btn-green',
					action: function () {  
						$('#form_bonus').submit(); 
					}
				},

			}
		});
	});

	function cal_sparator_adjustbonus(id) {  
		//ot 1
	    var adjustbonus = document.getElementById('adjustbonus' + id).value;
	    var result = document.getElementById('adjustbonus' + id);
	    var rsamount = (adjustbonus);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('adjustbonus' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='adjustbonus' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}


  	function cal_sparator_pph21(id) {  
		//ot 1
	    var pph21 = document.getElementById('pph21' + id).value;
	    var result = document.getElementById('pph21' + id);
	    var rsamount = (pph21);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('pph21' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='pph21' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}

  	function cal_sparator_potpinjaman(id) {  
		//ot 1
	    var potpinjaman = document.getElementById('potpinjaman' + id).value;
	    var result = document.getElementById('potpinjaman' + id);
	    var rsamount = (potpinjaman);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('potpinjaman' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='potpinjaman' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}
</script>
@stop

