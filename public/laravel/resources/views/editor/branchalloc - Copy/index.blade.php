 @extends('layouts.editor.template')
 @section('content')
 <style type="text/css">
.toolbar {
    float: left;
}
th,td  {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
</style>

<section class="content-header" style="margin-top: -10px; margin-bottom: -10px">
  <h1>
    <i class="fa fa-truck"></i> Capital Goods
    <small>Transaction</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Transaction</a></li>
    <li class="active">Capital Goods</li>
  </ol>
</section>

 
 <!-- Main content -->
<section class="content">
    <div class="row"> 
        <div class="col-sm-12">
            <div class="box box-danger"> 
              {!! Form::model($capitalgood, array('route' => ['editor.branchalloc.update'], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_branchalloc'))!!}
                <input type="hidden" name="search_filter" id="search_filter">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="box-body"> 
                            <table id="table_capitalgood" class="table table-bordered table-hover stripe"> 
                              <thead style="font-size: 12px">
                                <tr>
                                  <th rowspan="2" width="1%">No Cont</th>
                                  <th rowspan="2" width="1%">Mix/Not</th>
                                  <th rowspan="2" width="1%">PO No</th>
                                  <th rowspan="2" width="1%">Doc Date</th> 
                                  <th rowspan="2" width="1%">GR Date</th>
                                  <th rowspan="2" width="5%">Item</th>
                                  {{-- <th rowspan="2" width="5%">Description</th> --}}
                                  <th rowspan="2" width="1%">Plant</th>
                                  <th rowspan="2" width="1%">Port</th>
                                  <th rowspan="2" width="1%">Mat Doc</th>
                                  <th rowspan="2" width="1%">Qty Order</th>
                                  <th rowspan="2" width="1%">Nett Weight <br> Item</th>
                                  <th rowspan="2" width="1%">Qty Dif</th>
                                  <th rowspan="2" width="1%">Modal</th>
                                  <th colspan="3"><center>MODAL</center></th>
                                  <th rowspan="2" width="1%">EMKL/KG</th>
                                  <th rowspan="2" width="1%">Biaya EMKL</th>
                                  <th rowspan="2" width="1%">Total</th>
                                  <th rowspan="2" width="1%">Fee 1%</th>
                                  <th rowspan="2" width="1%">Modal/Ctn</th>
                                  <th rowspan="2" width="1%">Tarik ?</th>
                                  <th colspan="
                                  @php
                                    echo count($branch) + 1;
                                  @endphp
                                  " width="2%"><center>PEMBAGIAN CABANG</center></th>
                                </tr>
                                <tr>
                                  <th width="1%">Currency</th>
                                  <th width="1%">Kurs</th>
                                  <th width="1%">IDR</th>
                                  @foreach($branch as $branchs)
                                  <th width="1%">{{ $branchs->branch_name }}</th>
                                  @endforeach
                                </tr>
                               </thead> 
                                <tbody> 
                                  @foreach($capitalgood_detail as $capitalgood_details)
                                  <tr style="background-color: #fff !important">
                                    <td>{{ $capitalgood_details->prefix }} {{ $capitalgood_details->container_no }}</td>
                                    @if($capitalgood_details->mix == 1)
                                    <td>Mix</td>
                                    @else
                                    <td>Not Mix</td>
                                    @endif
                                    <td>{{ $capitalgood_details->po_number }}</td>
                                    <td>{{ $capitalgood_details->doc_date }}</td>
                                    <td>{{ $capitalgood_details->gr_date }}</td>
                                    <td>{{ $capitalgood_details->item_name }}</td>
                                    {{-- <td>{{ $capitalgood_details->description }}</td> --}}
                                    <td>{{ $capitalgood_details->plant }}</td>
                                    <td>{{ $capitalgood_details->store_location_name }}</td>
                                    <td>{{ $capitalgood_details->mat_doc }}</td>
                                    <td>{{ number_format($capitalgood_details->order_qty_po,0) }}</td>
                                    <td>{{ number_format($capitalgood_details->nett_weight_po,0) }}</td>
                                    <td @if($capitalgood_details->dif_qty == 0) style="background-color: #b3ecb3" @else style="background-color: #ecb3c0" @endif>{{ number_format($capitalgood_details->dif_qty,0) }}</td>
                                    <td style="width: 10px !important">
                                        @php 
                                          echo '<input  style="width: 70px" class="form-control input-sm" placeholder="" id="totalmodal'.$capitalgood_details->id.'" name="detail['.$capitalgood_details->id.'][totalmodal]" type="number" value="'.$capitalgood_details->totalmodal.'" onchange=needsave('.$capitalgood_details->id.');>';
                                         @endphp
                                    </td>
                                    <td>
                                      <select onchange="needsave({{$capitalgood_details->id}})" name="detail[{{ $capitalgood_details->id }}][currency_id]"> 
                                        <option value="{{ $capitalgood_details->currency_id}}"> {{$capitalgood_details->currency_name}}</option>
                                        @foreach($currency_list as $currency)
                                        <option value="{{$currency->id}}">{{$currency->currency_name}}</option>
                                        @endforeach
                                      </select>
                                    </td>
                                    <td>{{ $capitalgood_details->rate }}</td>
                                    <td>{{ number_format($capitalgood_details->rateidr,0) }}</td>

                                    
                                     @php
                                      $total = ($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty);

                                      $total_perkg = ($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->nett_weight);
                                    @endphp

                                    @if($capitalgood_details->mix ==1)
                                      <td>
                                      {{ number_format((($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->nett_weight)) + ($total_perkg * 0.01),0) }}</td>
                                    @else
                                      <td>{{ number_format(($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty,0) }}</td>
                                    @endif

                                    @if($capitalgood_details->mix ==1)
                                      @php
                                        if($capitalgood_details->nett_weight_po == 0){
                                          $nett_weight_dev = 1;
                                        }else{
                                          $nett_weight_dev = $capitalgood_details->nett_weight_po;
                                        };
                                      @endphp

                                      <td>{{ number_format(($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / ($capitalgood_details->order_qty * $nett_weight_dev)) }}</td>
                                    @else
                                      <td>{{ number_format(($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty,0) }}</td>
                                    @endif                                    

                                    <td>{{ number_format( ($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty),0) }}</td>

                                   
                                    <td>{{ number_format($total * 0.01,0) }}</td>
                                    <td>{{ number_format((($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty)) + ($total * 0.01),0) }}</td>
                                    <td>
                                       @if($capitalgood_details->tarik == 1)
                                        <span class="label label-success"> <i class="fa fa-check"></i> Yes</span>
                                       @else
                                        <span class="label label-danger"> <i class="fa fa-close"></i> No</span>
                                      @endif
                                    </td>
                                    @foreach($branch as $branchs)
                                    <td style="width: 10px !important">
                                        @php 
                                          $branch_name = $branchs->field_name;
                                          echo $capitalgood_details->$branch_name;
                                        @endphp
                                    </td>
                                    @endforeach
                                  </tr>
                                  @endforeach
                                </tbody>
                          </table> 
                          <!-- /.box-body -->
                         <a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-save"></i> Save</a> 
                        <a href="" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a> 
                   </div>
                <input type="hidden" id="has_change">
                {!! Form::close() !!}     
          </div>
      </div> 
    </div>  
</section><!-- /.content -->
       

@stop 

@section('scripts')
  
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 

    var grfrom = $("#grfrom").val();
    var grto = $("#grto").val();

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to update <i>tarik</i> data from period: <b>' + grfrom + '</b> to period: <b>' + grto + '</b>?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_branchalloc').submit(); 
          }
        },

      }
    });
  });

  $(document).ready(function() {  
        $("#table_capitalgood").dataTable( {
            "sScrollX": true,
             "scrollY": "330px",
             "bPaginate": false,
             "dom": '<"toolbar">frtip',
             "autoWidth": false,
             "ordering": false,
             "oSearch": {"sSearch": "{{$datafilter->search_filter}}"},
             fixedColumns:   {
              leftColumns: 7
             },
        });

        $("div.toolbar").html('<div class="col-sm-3" data-toggle="tooltip" data-placement="top" title="Date From" style="height: 10px"><div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>{{ Form::text("grfrom", old("grfrom", $datafilter->grfrom), array("class" => "form-control", "placeholder" => "GR From*", "required" => "true", "id" => "grfrom", "onchange" => "filter();")) }}</div><!-- /.input group --></div><div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Date To"><div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>{{ Form::text("grto", old("grto", $datafilter->grto), array("class" => "form-control", "placeholder" => "GR To*", "required" => "true", "id" => "grto", "onchange" => "filter();")) }}</div><!-- /.input group --></div><div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Category"><select name="item_category_id" id="item_category_id" class="form-control select2" id="select2" onchange="filter();">@if($datafilter->item_category_id == 0)<option value="0">All Category</option> @else <option value="{{$datafilter->item_category_id}}">{{$datafilter->item_category_name}}</option> @endif <option value="0">All Category</option> @foreach($item_category as $item_categorys)<option value="{{$item_categorys->id}}">{{$item_categorys->item_category_name}}</option>@endforeach</select></div><div class="col-sm-2" style="margin-left: -10px" data-toggle="tooltip" data-placement="top" title="Container"><select name="container_id" id="container_id" class="form-control select2" onchange="filter();">@if($datafilter->container_id == 0)<option value="0">All Container</option> @else <option value="{{$datafilter->container_id}}">{{$datafilter->container_name}}</option> @endif <option value="0">All Container</option> @foreach($container as $containers)<option value="{{$containers->id}}">{{$containers->container_name}}</option>@endforeach</select></div><button onClick="filter(); history.go(0);" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button><div class="box-tools pull-right"><div class="tableTools-container"></div></div>');

       var table = $('#table_capitalgood').DataTable();
 
      table.on( 'search.dt', function () {
          $('#search_filter').val(table.search());
          filter();
      });
    });

  function filter()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.datefilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'grfrom': $('#grfrom').val(),    
            'grto': $('#grto').val(),
            'item_category_id': $('#item_category_id').val(),   
            'container_id': $('#container_id').val(),   
            'search_filter': $('#search_filter').val(),   
            'branch_id': $('#branch_id').val(),   
          }, 
          success: function(data) { 
            // reload_table();
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Filter GR date has been change!', 'Success Alert', options);
          }
        }) 
      }; 

      function needsave(id)
      { 
        console.log(id);
        $("#has_change").val(1);
        //Ajax Load data from ajax
        $.ajax({
          url: 'branchalloc/needsave/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val()
          },
        })
      };

      window.onload= function(){ 
        // console.log(id);
        //Ajax Load data from ajax
        $.ajax({
          url: 'branchalloc/resetneedsave',
          type: "POST",
          data: {
            '_token': $('input[name=_token]').val()
          },
        })
      };

</script>

<script>
var submitted = false;

$(document).ready(function () {
    window.onbeforeunload = function (e) {
        var has_change = $("#has_change").val();
        if (has_change == 1 && !submitted) {
            var message = "You have not saved your changes.", e = e || window.event;
            if (e) {
                e.returnValue = message;
            }
            return message;
        }
    }
     $("form_branchalloc").submit(function() {
         // alert('test');
         submitted = true;
     });
});
</script>
@stop

