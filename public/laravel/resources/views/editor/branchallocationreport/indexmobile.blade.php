@extends('layouts.editor.templatemobile')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top: -18%; margin-bottom: -5%">
  <h4>
    <i class="fa fa-file"></i> Branch Allocation Report
  </h4>
</section><br>

 
 <!-- Main content -->
<section class="content">
    <div class="row"> 
        <div class="col-sm-12"> 
            <div class="box box-danger">
              {!! Form::model($capitalgood, array('route' => ['editor.branchalloc.update'], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_branchalloc'))!!}
                <div class="box-header with-border">
                    <div class="row">
                        <div class="box-body"> 
                          <div class="row">
                            <div class="col-sm-1 col-xs-6" data-toggle="tooltip" data-placement="top" title="Filter By">
                              <select name="check_filter" id="check_filter" class="form-control" onchange="filter(); filter_week();">
                                @if($datafilter->check_filter > 0) <option value="1">By Week</option> @else <option value="0">By Date</option>  @endif
                                <option value="0">By Date</option> 
                                <option value="1">By Week</option> 
                              </select>
                            </div>
                            <div class="col-sm-2 col-xs-6" id="divgrfrom" data-toggle="tooltip" data-placement="top" style="margin-left: -10px" title="Date From" style="height: 10px">
                              <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                  {{ Form::text("grfrom", old("grfrom", $datafilter->grfrom), array("class" => "form-control", "placeholder" => "GR From*", "required" => "true", "id" => "grfrom", "onchange" => "filter();")) }}
                              </div><!-- /.input group -->
                            </div>
                            <div class="col-sm-2 col-xs-6" id="divgrto" data-toggle="tooltip" data-placement="top" title="Date To">
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                {{ Form::text("grto", old("grto", $datafilter->grto), array("class" => "form-control", "placeholder" => "GR To*", "required" => "true", "id" => "grto", "onchange" => "filter();")) }}
                              </div><!-- /.input group -->
                            </div>

                            <div class="col-sm-2 col-xs-6" id="divweek" data-toggle="tooltip" data-placement="top" title="Week"> 
                              <select name="week" id="week" class="form-control select2" onchange="filter();">
                              @if($datafilter->week > 0) <option value="{{$datafilter->week}}">Week {{$datafilter->week}} Year <?php echo date("Y"); ?></option> @endif
                              @foreach($week_list as $week_lists)<option value="{{$week_lists->week_name}}">Week {{$week_lists->week_name}} Year <?php echo date("Y"); ?></option>@endforeach
                            </select>
                            </div>
                            <div class="col-sm-2 col-xs-6" data-toggle="tooltip" data-placement="top" title="Category">
                              <select name="item_category_id" id="item_category_id" class="form-control select2" onchange="filter();">       
                                @if($datafilter->item_category_id == 0)
                                  <option value="0">All Category</option> 
                                  @else <option value="{{$datafilter->item_category_id}}">{{$datafilter->item_category_name}}</option> 
                                @endif <option value="0">All Category</option> 
                                @foreach($item_category as $item_categorys)
                                  <option value="{{$item_categorys->id}}">{{$item_categorys->item_category_name}}</option>
                                @endforeach
                              </select></div>
                              <div class="col-sm-2 col-xs-6" data-toggle="tooltip" data-placement="top" title="Branch">
                                <select name="branch_id" id="branch_id" class="form-control select2" onchange="filter();">
                                  @if($datafilter->branch_id == 0)
                                    <option value="0">All Branch</option> 
                                    @else 
                                    <option value="{{$datafilter->branch_id}}">{{$datafilter->branch_name}}</option> 
                                  @endif 
                                    <option value="0">All Branch</option> 
                                  @foreach($branch_cb as $branchs)
                                    <option value="{{$branchs->id}}">{{$branchs->branch_name}}</option>
                                  @endforeach
                                </select>
                              </div>
                              <div class="col-sm-2 col-xs-6" data-toggle="tooltip" data-placement="top" title="Search">
                                 <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    {{ Form::text("search", old("search", $datafilter->search), array("class" => "form-control", "placeholder" => "Search...", "required" => "true", "id" => "search", "onchange" => "filter();")) }}
                                </div>
                              </div>
                              <div class="col-sm-2 col-xs-6" data-toggle="tooltip" data-placement="top" title="Search">
                                <button onClick="filter(); history.go(0);" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button> 
                              </div>
                            </div>
                            <table id="dtTable" class="table table-bordered table-hover stripe"> 
                              <thead style="font-size: 12px">
                                <tr>
                                  <th rowspan="2" width="0.1%">No Con</th>
                                  <th rowspan="2" width="0.5%">Doc Date</th> 
                                  <th rowspan="2" width="0.5%">ETA</th> 
                                  <th rowspan="2" width="0.5%">GR Date</th> 
                                  <th rowspan="2" width="0.1%">PO No</th>
                                  <th rowspan="2" width="0.1%">Plant</th>
                                  <th rowspan="2" width="0.1%">Store</th>
                                  <th rowspan="2" width="3%">SKU</th>
                                  <th rowspan="2" width="10%">Material</th>
                                  <th rowspan="2" width="0.1%">Harga/Ctn</th>
                                  <th colspan="
                                  @php
                                    echo count($branch);
                                  @endphp
                                  "><center>PEMBAGIAN CABANG</center></th>
                                  <th rowspan="2" width="0.1%">Total</th>
                                </tr>
                                <tr>
                                  @foreach($branch as $branchs)
                                  <th width="1%">{{ $branchs->branch_name }}</th>
                                  @endforeach
                                </tr>
                               </thead> 
                                <tbody> 
                                  @foreach($capitalgood_detail as $capitalgood_details)

                                  @php
                                    $totalf = 0;
                                  @endphp
                                  @foreach($branch as $branchs)
                                      @php 
                                        $branch_name = $branchs->field_name;
                                        $totalf += $capitalgood_details->$branch_name;
                                      @endphp
                                  @endforeach
                                  @if($totalf>0)
                                  <tr style="background-color: #fff !important">
                                    <td>{{ $capitalgood_details->prefix }}{{ $capitalgood_details->container_no }}</td>
                                    <td>{{ $capitalgood_details->doc_date }}</td>
                                    <td>{{ $capitalgood_details->eta }}</td>
                                    <td>{{ $capitalgood_details->gr_date }}</td> 
                                    <td>{{ $capitalgood_details->po_number }}</td>
                                    <td>{{ $capitalgood_details->plant }}</td>
                                    <td>{{ $capitalgood_details->store_location_name }}</td>
                                    <td>{{ $capitalgood_details->item_code }}</td>
                                    <td>{{ $capitalgood_details->item_name }}</td>

                                    @php
                                      $total = ($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty);
                                    @endphp

                                    <td>{{ number_format(($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty) + ($total * 0.01) + $capitalgood_details->add_modal,0)}}</td>

                                    @php
                                      $total = 0;
                                    @endphp
                                    @foreach($branch as $branchs)
                                    <td style="width: 10px !important">
                                        @php 
                                          $branch_name = $branchs->field_name;
                                          echo $capitalgood_details->$branch_name;
                                          $total += $capitalgood_details->$branch_name;
                                        @endphp
                                    </td>
                                    @endforeach
                                    <td>@php echo $total; @endphp</td>
                                  </tr>
                                  @endif
                                  @endforeach
                                </tbody>
                          </table> 
                          <!-- /.box-body -->
                         {{-- <a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-save"></i> Save</a>  --}}
                   </div>
                        <!-- <a href="" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>  -->

                {!! Form::close() !!}     
          </div>
      </div> 
    </div>  
</section><!-- /.content -->
       

@stop 

@section('scripts')
  
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 

    var grfrom = $("#grfrom").val();
    var grto = $("#grto").val();

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to update <i>tarik</i> data from period: <b>' + grfrom + '</b> to period: <b>' + grto + '</b>?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_branchalloc').submit(); 
          }
        },

      }
    });
  });

  function filter_week()
  {
    var filter_week = $("#check_filter").val();
     if(filter_week == 1){
      $("#divgrfrom").hide(500);
      $("#divgrto").hide(500);
      $("#divweek").show(500); 
     }else{
      $("#divgrfrom").show(500);
      $("#divgrto").show(500);
      $("#divweek").hide(500); 
     };
  };

  $(document).ready(function() {   
      @if($datafilter->check_filter == 1)
          $("#divgrfrom").hide(500);
          $("#divgrto").hide(500);
      @else
          $("#divweek").hide(500); 
      @endif
  });


  $(document).ready(function() { 
        $("#dtTable").dataTable( {
             "bPaginate": true,
             "scrollX": true,
             "autowidth": false,
             "sScrollXInner": "500%",
             "searching": false,
             "responsive": false,
             "dom": '<"toolbar">frtip' 
        });

        $("div.toolbar").html('');
    });

  function filter()
       { 
        console.log($('#branch_id').val());
        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.datefilterbranch') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'grfrom': $('#grfrom').val(),    
            'grto': $('#grto').val(),
            'item_category_id': $('#item_category_id').val(),   
            'branch_id': $('#branch_id').val(),   
            'search': $('#search').val(),   
            'week': $('#week').val(),   
            'check_filter': $('#check_filter').val(),   
          }, 
          success: function(data) { 
            // reload_table();
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Filter GR date has been change!', 'Success Alert', options);
          }
        }) 
      }; 
</script>
@stop

