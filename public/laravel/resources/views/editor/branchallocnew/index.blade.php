@extends('layouts.editor.templatxeditable')
@section('title', 'Capital Good')
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Latest compiled and minified CSS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

<style>
  /* Center the loader */
  #loader {
    position: absolute;
    left: 50%;
    top: 50%;
    z-index: 1;
    width: 150px;
    height: 150px;
    margin: -75px 0 0 -75px;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
  }

  @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }

  /* Add animation to "page content" */
  .animate-bottom {
    position: relative;
    -webkit-animation-name: animatebottom;
    -webkit-animation-duration: 1s;
    animation-name: animatebottom;
    animation-duration: 1s
  }

  @-webkit-keyframes animatebottom {
    from { bottom:-100px; opacity:0 } 
    to { bottom:0px; opacity:1 }
  }

  @keyframes animatebottom { 
    from{ bottom:-100px; opacity:0 } 
    to{ bottom:0; opacity:1 }
  }

  #myDiv {
    display: none;
    text-align: center;
  }

  thead th {
    /*font-size: 0.9em;*/
    padding: 1px !important;
    height: 10px;
  }
</style>  

<section class="content-header">
  <h1>
    <i class="fa fa-truck"></i> Branch Allocation
    <small>Transaction</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Transaction</a></li>
    <li class="active">Branch Allocation</li>
  </ol>
</section>
 
 <!-- Main content -->
<section class="content">
    <div class="row"> 
        <div class="col-sm-12">
            <div class="box box-danger">  
              @include('errors.error')
              {!! Form::model($capitalgood, array('route' => ['editor.capitalgood.update'], 'method' => 'PUT', 'class'=>'updateold', 'id'=>'form_capitalgood'))!!}
              <input type="hidden" name="search_filter" id="search_filter">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="box-body"> 
                          <div class="row">
                          <div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Filter By">
                              <select name="check_filter" id="check_filter" class="form-control" onchange="filter(); filter_week();">
                              @if($datafilter->check_filter > 0) <option value="1">By Week</option> @else <option value="0">By Date</option>  @endif
                              <option value="0">By Date</option> 
                              <option value="1">By Week</option> 
                            </select>
                          </div>
                          <div class="col-sm-2" style="margin-left: -20px" id="divgrfrom" data-toggle="tooltip" data-placement="top" title="Date From">
                            <div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>{{ Form::text("grfrom", old("grfrom", $datafilter->grfrom), array("class" => "form-control", "placeholder" => "GR From*", "required" => "true", "id" => "grfrom", "onchange" => "filter();")) }}
                            </div><!-- /.input group -->
                          </div>
                          <div class="col-sm-2" id="divgrto" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Date To">
                            <div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>{{ Form::text("grto", old("grto", $datafilter->grto), array("class" => "form-control", "placeholder" => "GR To*", "required" => "true", "id" => "grto", "onchange" => "filter();")) }}
                            </div><!-- /.input group -->
                          </div>
                          <div class="col-sm-2" id="divweek" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Week"> 
                            <select name="week" id="week" class="form-control select2" onchange="filter();">
                              @if($datafilter->week > 0) <option value="{{$datafilter->week}}">Week {{$datafilter->week}} Year <?php echo date("Y"); ?></option> @endif
                              @foreach($week_list as $week_lists)<option value="{{$week_lists->week_name}}">Week {{$week_lists->week_name}} Year <?php echo date("Y"); ?></option>@endforeach
                            </select>
                          </div>
                          <div class="col-sm-2" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Category">
                            <select name="item_category_id" id="item_category_id" class="form-control select2" onchange="filter();">@if($datafilter->item_category_id == 0)<option value="0">All Category</option> @else <option value="{{$datafilter->item_category_id}}">{{$datafilter->item_category_name}}</option> @endif <option value="0">All Category</option> @foreach($item_category as $item_categorys)<option value="{{$item_categorys->id}}">{{$item_categorys->item_category_name}}</option>@endforeach</select>
                          </div>
                          <div class="col-sm-2" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Container"><select name="container_id" id="container_id" class="form-control select2" onchange="filter();">@if($datafilter->container_id == 0)<option value="0">All Container</option> @else <option value="{{$datafilter->container_id}}">{{$datafilter->container_name}}</option> @endif <option value="0">All Container</option> @foreach($container as $containers)<option value="{{$containers->id}}">{{$containers->container_name}}</option>@endforeach</select>
                          </div>
                           <div class="col-sm-2" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Search">
                            {{ Form::text("search", old("search", $datafilter->search), array("class" => "form-control pull-right", "placeholder" => "Search...", "required" => "true", "id" => "search", "onchange" => "filter();")) }}
                          </div>
                          <button onClick="filter(); history.go(0);" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
                          <div class="box-tools pull-right">
                            <div class="tableTools-container"></div>
                          </div>
                        </div>
                            <table id="table_capitalgood" class="table table-bordered table-hover stripe" style="margin-top: -20px"> 
                              <thead style="font-size: 12px">
                                <tr>
                                  <th rowspan="2" width="1%">No Cont</th>
                                  <th rowspan="2" width="1%">PO No</th>
                                  <th rowspan="2" width="1%">ETA</th> 
                                  <th rowspan="2" width="1%">GR Date</th>
                                  <th rowspan="2" width="5%">Item</th>
                                  {{-- <th rowspan="2" width="5%">Description</th> --}}
                                  <th rowspan="2" width="0.1%">Plant</th>
                                  <th rowspan="2" width="0.1%">Port</th>
                                  <th rowspan="2" width="1%">Qty GR</th>
                                  <th rowspan="2" width="1%">Qty Order</th>
                                  <th rowspan="2" width="1%">Qty Dif</th>
                                  {{-- <th rowspan="2" width="1%">Modal</th> --}}
                                  {{-- <th colspan="3"><center>MODAL</center></th> --}}
                                  {{-- <th rowspan="2" width="3%">Biaya EMKL</th> --}}
                                  {{-- <th rowspan="2" width="2%">Total</th> --}}
                                  {{-- <th rowspan="2" width="2%">Fee 1%</th> --}}
                                  <th rowspan="2" width="2%">Modal/Ctn</th>
                                  <th width="1%" rowspan="2">Tarik ?</th>
                                  <th colspan="
                                  @php
                                    echo count($branch) + 1;
                                  @endphp
                                  "><center>PEMBAGIAN CABANG</center></th>
                                </tr>
                                <tr>
                                  {{-- <th width="2%">Currency</th>
                                  <th width="2%">Kurs</th>
                                  <th width="2%">IDR</th> --}}
                                  
                                  @foreach($branch as $branchs)
                                  <th width="2%">{{ $branchs->branch_name }}</th>
                                  @endforeach
                                </tr>
                               </thead> 
                                <tbody style="height: 10px"> 
                                  @foreach($capitalgood_detail as $capitalgood_details)
                                  <tr style="background-color: #fff !important">
                                    <td>{{ $capitalgood_details->prefix }}{{ $capitalgood_details->container_no }}</td>
                                    <td>{{ $capitalgood_details->po_number }}</td>
                                    <td>{{ $capitalgood_details->eta }}</td>
                                    <td>{{ $capitalgood_details->gr_date }}</td>
                                    <td>{{ $capitalgood_details->item_name }}</td>
                                    <td>{{ $capitalgood_details->plant }}</td>
                                    <td>{{ $capitalgood_details->store_location_name }}</td>
                                    <td>{{ $capitalgood_details->gr_qty }}</td>
                                    <td>{{ number_format($capitalgood_details->order_qty_po,0) }}</td>
                                    <td @if($capitalgood_details->dif_qty == 0) style="background-color: #b3ecb3" @else style="background-color: #ecb3c0" @endif>{{ number_format($capitalgood_details->dif_qty,0) }}</td>
                                    @php
                                      $total = ($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty);
                                    @endphp
                                    <td>{{ number_format(((($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty)) + ($total * 0.01)) + $capitalgood_details->add_modal,0) }}</td>
                                    <td @if($capitalgood_details->tarik == 1) style="background-color: #b3ecb3" @else style="background-color: #ecb3c0" @endif > 
                                       <a href="" class="update" data-name="tarik" data-title="Tarik" data-pk="{{ $capitalgood_details->id }}" data-type="select" data-source='[{value:"1","text":"Yes"},{value:"0","text":"No"}]'>@if($capitalgood_details->tarik == 1) Yes @else No @endif
                                    </td>

                                    @foreach($branch as $branchs)
                                    <td style="width: 10px !important">

                                        @php 
                                          $branch_name = $branchs->field_name;
                                          echo '<a href="" class="update" data-name="'.$branchs->field_name.'" data-pk="'.$capitalgood_details->id.'" data-type="text" data-title="'.$capitalgood_details->container_no.': '.$branchs->field_name.'" data-emptytext="Null">'.$capitalgood_details->$branch_name.'</a>';
                                         @endphp
                                    </td>
                                    @endforeach
                                  </tr>
                                  @endforeach
                                </tbody>
                          </table> 
                          <div class="pull-right">{{ $capitalgood_detail->links() }}</div> 
                          {!! Form::close() !!}  
                          <input type="hidden" id="has_change">
                          <div id="loader"></div>  

                          {!! Form::open(array('route' => 'editor.branchalloc.storeimport', 'class'=>'create', 'files' => 'true'))!!}
                          {{ csrf_field() }}<br/> 
                          {{ Form::label('import_file', 'Import Item from Excel File (.ods)') }}<br>
                          Max 5MB
                          {{ Form::file('import_file') }} 
                          <br/>                         
                          <button type="submit" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Import</button>
                          {!! Form::close() !!}  
                          <br>
                         <a href="{{ url('editor/branchalloc/storeexport/xls') }}" class="btn btn-success"><i class="fa fa-download"></i> Download Template .xls </a>
                        
                   </div>
                {!! Form::close() !!}     
          </div>
      </div> 
    </div>  
    {{-- <a href="#" onclick="test();">test</a> --}}
</section><!-- /.content -->
       

@stop 

@section('scripts')
  
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 
    var grfrom = $("#grfrom").val();
    var grto = $("#grto").val();

    // alert(grfrom);

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to update <i>allocation</i> data from period: <b>' + grfrom + '</b> to period: <b>' + grto + '</b>?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_capitalgood').submit(); 
          }
        },

      }
    });
  });

  function filter_week()
  {
    var filter_week = $("#check_filter").val();
     if(filter_week == 1){
      $("#divgrfrom").hide(500);
      $("#divgrto").hide(500);
      $("#divweek").show(500); 
     }else{
      $("#divgrfrom").show(500);
      $("#divgrto").show(500);
      $("#divweek").hide(500); 
     };
  };

  $(document).ready(function() {   
      @if($datafilter->check_filter == 1)
          $("#divgrfrom").hide(500);
          $("#divgrto").hide(500);
      @else
          $("#divweek").hide(500); 
      @endif
  });

   $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $('.update').editable({
         url: 'branchallocnew/edit',
         type: 'text',
         pk: 1,
         name: 'name',
         title: 'Enter name'
  });

  $(document).ready(function() { 
      document.getElementById("loader").style.display = "none";
      resetneedsave();
      $("#table_capitalgood").dataTable( {
         "scrollX": true,
         "autowidth": true,
         "sScrollXInner": "250%",
         "scrollY": "330px",
         "bPaginate": false,
         "autoWidth": true,
         "ordering": false,
         "dom": '<"toolbar">frtip',
         "oSearch": {"sSearch": "{{$datafilter->search_filter}}"},
         fixedColumns:   {
          leftColumns: 5
         },
         "bInfo" : false,
         "paging": false,
         "searching": false,
         'columnDefs' : [ 
              { 
                  'searchable'    : false, 
                  'targets'       : [12,13,14,15,16,17,18,19] 
              },
          ]
      });
       $("div.toolbar").html('');

      var table = $('#table_capitalgood').DataTable();
 
      table.on( 'search.dt', function () {
          $('#search_filter').val(table.search());
          filter();
      });
    });

    function filter()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.datefilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'grfrom': $('#grfrom').val(),    
            'grto': $('#grto').val(),   
            'item_category_id': $('#item_category_id').val(), 
            'container_id': $('#container_id').val(),   
            'search': $('#search').val(),   
            'week': $('#week').val(),   
            'check_filter': $('#check_filter').val(),   
          }, 
          success: function(data) { 
            // var options = { 
            //   "positionClass": "toast-bottom-right", 
            //   "timeOut": 1000, 
            // };
            // toastr.success('Filter GR date has been change!', 'Success Alert', options);
          }
        }) 
      }; 

      function difference(id)
      {
        var orderqty = $("#orderqty").val();
        var allocation = $("#allocation").val();
                                          
        $("#difference").val = orderqty - allocation;
        
      }

      function needsave(id)
      { 
        console.log(id);
        $("#has_change").val(1);
        //Ajax Load data from ajax
        $.ajax({
          url: 'capitalgood/needsave/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val()
          },
        })
      };

      function resetneedsave(){ 
        //Ajax Load data from ajax
        $.ajax({
          url: 'capitalgood/resetneedsave',
          type: "POST",
          data: {
            '_token': $('input[name=_token]').val()
          },
        })
      };
</script>

<script> 
var submitted = false;

$(document).ready(function () {
    window.onbeforeunload = function (e) {
        var has_change = $("#has_change").val();
        if (has_change == 1 && !submitted) {
            var message = "You have not saved your changes.", e = e || window.event;
            if (e) {
                e.returnValue = message;
            }
            return message;
        }
    }
     $("form_capitalgood").submit(function() {
         // alert('test');
         submitted = true;
     });
});
</script>
@stop
