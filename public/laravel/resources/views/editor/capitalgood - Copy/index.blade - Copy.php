 @extends('layouts.editor.template')
 @section('content')
 <style type="text/css">
 .input-sm {
      height: 22px;
      padding: 1px 3px;
      font-size: 12px;
      line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
      border-radius: 0px;
      /*width: 90% !important;*/
}
.toolbar {
    float: left;
}
</style>

   
<section class="content-header" style="margin-top: -10px; margin-bottom: -10px">
  <h1>
    <i class="fa fa-truck"></i> Capital Goods
    <small>Transaction</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Transaction</a></li>
    <li class="active">Capital Goods</li>
  </ol>
</section>

 
 <!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-6">
    <div class="box box-danger">
      <div class="box-header with-border">
        <i class="fa fa-filter"></i><h3 class="box-title">Filter Good Receive</h3>
      </div>
      
      <div class="box-body">
        <div class="row">
          <div class="col-xs-5">
            <div class="form-group">
              <label for="real_name" class="col-sm-2 control-label">From</label>
                <div class="col-sm-10">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('grfrom', old('grfrom'), array('class' => 'form-control', 'placeholder' => 'GR From*', 'required' => 'true', 'id' => 'grfrom')) }}
                </div><!-- /.input group --> 
              </div>
            </div>
          </div>
          
          <div class="col-xs-5">
            <div class="form-group">
              <label for="real_name" class="col-sm-2 control-label">To</label>
                <div class="col-sm-10">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('grto', old('grto'), array('class' => 'form-control', 'placeholder' => 'GR To*', 'required' => 'true', 'id' => 'grto')) }}
                </div><!-- /.input group --> 
              </div>
            </div>
          </div>

          <div class="col-xs-2">
            <button type="submit" class="btn btn-success pull-right btn-flat"><i class="fa fa-filter"></i> Filter</button> 
          </div>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
    <div class="row"> 
        <div class="col-sm-12">
            <div class="box box-danger">  
              {!! Form::model($capitalgood, array('route' => ['editor.capitalgood.update'], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_capitalgood'))!!}
                <div class="box-header with-border">
                    <div class="row">
                        <div class="box-body"> 
                            <table id="table_capitalgood" class="table table-bordered table-hover stripe"> 
                              <thead>
                                <tr>
                                  <th rowspan="2">No Container</th>
                                  <th rowspan="2">PO Number</th>
                                  <th rowspan="2">Doc Date</th> 
                                  <th rowspan="2">GR Date</th>
                                  <th rowspan="2">Item</th>
                                  <th rowspan="2">Description</th>
                                  <th rowspan="2">Plant</th>
                                  <th rowspan="2">Mat Doc</th>
                                  <th rowspan="2">Qty Order</th>
                                  <th colspan="3"><center>MODAL</center></th>
                                  <th rowspan="2">Biaya EMKL</th>
                                  <th rowspan="2">Total</th>
                                  <th rowspan="2">Fee 1%</th>
                                  <th rowspan="2">Modal per Ctn</th>
                                  <th colspan="
                                  @php
                                    echo count($branch) + 1;
                                  @endphp
                                  "><center>PEMBAGIAN CABANG</center></th>
                                </tr>
                                <tr>
                                  <th>$</th>
                                  <th>Kurs</th>
                                  <th>IDR</th>
                                  <th>Tarik ?</th>
                                  @foreach($branch as $branchs)
                                  <th style="width: 10px !important">{{ $branchs->branch_name }}</th>
                                  @endforeach
                                </tr>
                               </thead> 
                                <tbody> 
                                  @foreach($capitalgood_detail as $capitalgood_details)
                                  <tr style="background-color: #fff !important">
                                    <td>{{ $capitalgood_details->container_no }}</td>
                                    <td>{{ $capitalgood_details->po_number }}</td>
                                    <td>{{ $capitalgood_details->doc_date }}</td>
                                    <td>{{ $capitalgood_details->gr_date }}</td>
                                    <td>{{ $capitalgood_details->item_name }}</td>
                                    <td>{{ $capitalgood_details->description }}</td>
                                    <td>{{ $capitalgood_details->plant }}</td>
                                    <td>{{ $capitalgood_details->mat_doc }}</td>
                                    <td>{{ number_format($capitalgood_details->order_qty,0) }}</td>

                                    <td>{{ $capitalgood_details->po_number }}</td>
                                    <td>{{ $capitalgood_details->po_number }}</td>
                                    <td>{{ $capitalgood_details->po_number }}</td>

                                    <td>{{ number_format($capitalgood_details->biaya_emkl,0) }}</td>
                                    <td>{{ number_format($capitalgood_details->total,0) }}</td>
                                    <td>{{ number_format($capitalgood_details->fee,0) }}</td>
                                    <td>{{ number_format($capitalgood_details->modal_ctn,0) }}</td>
                                    <td>
                                      @if($capitalgood_details->tarik == 1)
                                        <span class="label label-success"> <i class="fa fa-check"></i> Yes</span>
                                       @else
                                        <span class="label label-danger"> <i class="fa fa-close"></i> No</span>
                                      @endif
                                    </td>

                                    @foreach($branch as $branchs)
                                    <td style="width: 10px !important">
                                        @php 
                                          $branch_name = $branchs->field_name;
                                          echo '<input  style="width: 50px" class="form-control input-sm" placeholder="" id="'.$branchs->field_name.''.$capitalgood_details->id.'" name="detail['.$capitalgood_details->id.']['.$branchs->field_name.']" type="number" value="'.$capitalgood_details->$branch_name.'">';
                                         @endphp
                                    </td>
                                    @endforeach
                                  </tr>
                                  @endforeach
                                </tbody>
                          </table> 
                          <!-- /.box-body -->
                         <a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Submit</a> 
                        <a href="" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a> 
                   </div>
                {!! Form::close() !!}     
          </div>
      </div> 
    </div>  
</section><!-- /.content -->
       

@stop 

@section('scripts')
  
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to create data?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_capitalgood').submit(); 
          }
        },

      }
    });
  });

  $(document).ready(function() { 
        $("#table_capitalgood").dataTable( {
            "sScrollX": true,
             "scrollY": "380px",
             "sScrollXInner": "230%",
             "bPaginate": false,
             "dom": '<"toolbar">frtip',
             "rowReorder": true,
             fixedColumns:   {
              leftColumns: 7
             },
        });
    });
</script>
@stop

