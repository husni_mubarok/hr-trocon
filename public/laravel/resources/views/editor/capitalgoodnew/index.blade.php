@extends('layouts.editor.templatxeditable')
@section('title', 'Capital Good')
@section('content')
<!-- Content Wrapper. Contains page content -->
<!-- Latest compiled and minified CSS -->
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

<style>
  /* Center the loader */
  #loader {
    position: absolute;
    left: 50%;
    top: 50%;
    z-index: 1;
    width: 150px;
    height: 150px;
    margin: -75px 0 0 -75px;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
  }

  @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }

  /* Add animation to "page content" */
  .animate-bottom {
    position: relative;
    -webkit-animation-name: animatebottom;
    -webkit-animation-duration: 1s;
    animation-name: animatebottom;
    animation-duration: 1s
  }

  @-webkit-keyframes animatebottom {
    from { bottom:-100px; opacity:0 } 
    to { bottom:0px; opacity:1 }
  }

  @keyframes animatebottom { 
    from{ bottom:-100px; opacity:0 } 
    to{ bottom:0; opacity:1 }
  }

  #myDiv {
    display: none;
    text-align: center;
  }

  thead th {
    /*font-size: 0.9em;*/
    padding: 1px !important;
    height: 10px;
  }
</style>  

<section class="content-header">
  <h1>
    <i class="fa fa-truck"></i> Capital Goods
    <small>Transaction</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Transaction</a></li>
    <li class="active">Capital Goods</li>
  </ol>
</section>

 
 <!-- Main content -->
<section class="content">
    <div class="row"> 
        <div class="col-sm-12">
            <div class="box box-danger"> 
              {!! Form::model($capitalgood, array('route' => ['editor.branchalloc.update'], 'method' => 'PUT', 'class'=>'updateold', 'id'=>'form_branchalloc'))!!}
                <input type="hidden" name="search_filter" id="search_filter">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="box-body"> 
                          <div class="row">
                          <div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Filter By">
                              <select name="check_filter" id="check_filter" class="form-control" onchange="filter(); filter_week();">
                              @if($datafilter->check_filter > 0) <option value="1">By Week</option> @else <option value="0">By Date</option>  @endif
                              <option value="0">By Date</option> 
                              <option value="1">By Week</option> 
                            </select>
                          </div>
                          <div class="col-sm-2" style="margin-left: -20px" id="divgrfrom" data-toggle="tooltip" data-placement="top" title="Date From">
                            <div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>{{ Form::text("grfrom", old("grfrom", $datafilter->grfrom), array("class" => "form-control", "placeholder" => "GR From*", "required" => "true", "id" => "grfrom", "onchange" => "filter();")) }}
                            </div><!-- /.input group -->
                          </div>
                          <div class="col-sm-2" id="divgrto" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Date To">
                            <div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>{{ Form::text("grto", old("grto", $datafilter->grto), array("class" => "form-control", "placeholder" => "GR To*", "required" => "true", "id" => "grto", "onchange" => "filter();")) }}
                            </div><!-- /.input group -->
                          </div>
                          <div class="col-sm-2" id="divweek" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Week"> 
                            <select name="week" id="week" class="form-control select2" onchange="filter();">
                              @if($datafilter->week > 0) <option value="{{$datafilter->week}}">Week {{$datafilter->week}} Year <?php echo date("Y"); ?></option> @endif
                              @foreach($week_list as $week_lists)<option value="{{$week_lists->week_name}}">Week {{$week_lists->week_name}} Year <?php echo date("Y"); ?></option>@endforeach
                            </select>
                          </div>
                          <div class="col-sm-2" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Category">
                            <select name="item_category_id" id="item_category_id" class="form-control select2" onchange="filter();">@if($datafilter->item_category_id == 0)<option value="0">All Category</option> @else <option value="{{$datafilter->item_category_id}}">{{$datafilter->item_category_name}}</option> @endif <option value="0">All Category</option> @foreach($item_category as $item_categorys)<option value="{{$item_categorys->id}}">{{$item_categorys->item_category_name}}</option>@endforeach</select>
                          </div>
                          <div class="col-sm-2" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Search"><select name="container_id" id="container_id" class="form-control select2" onchange="filter();">@if($datafilter->container_id == 0)<option value="0">All Container</option> @else <option value="{{$datafilter->container_id}}">{{$datafilter->container_name}}</option> @endif <option value="0">All Container</option> @foreach($container as $containers)<option value="{{$containers->id}}">{{$containers->container_name}}</option>@endforeach</select>
                          </div>

                          <div class="col-sm-2" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Search">
                            {{ Form::text("search", old("search", $datafilter->search), array("class" => "form-control pull-right", "placeholder" => "Search...", "required" => "true", "id" => "search", "onchange" => "filter();")) }}
                          </div>

                          <button onClick="filter(); history.go(0);" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
                          <div class="box-tools pull-right">
                            <div class="tableTools-container"></div>
                          </div>
                        </div>

                            <table id="table_capitalgood" class="table table-bordered table-hover stripe"> 
                              <thead style="font-size: 12px">
                                <tr>
                                  <th rowspan="2" width="1%">No Cont</th>
                                  <th rowspan="2" width="1%">Mix/Not</th>
                                  <th rowspan="2" width="1%">PO No</th>
                                  <th rowspan="2" width="1%">Doc Date</th> 
                                  <th rowspan="2" width="1%">GR Date</th>
                                  <th rowspan="2" width="5%">Item</th>
                                  {{-- <th rowspan="2" width="5%">Description</th> --}}
                                  <th rowspan="2" width="1%">Plant</th>
                                  <th rowspan="2" width="1%">Port</th>
                                  <th rowspan="2" width="1%">Mat Doc</th>
                                  <th rowspan="2" width="1%">Qty Order</th>
                                  <th rowspan="2" width="1%">Nett Weight <br> Item</th>
                                  <th rowspan="2" width="1%">Qty Dif</th>
                                  <th rowspan="2" width="1%">Modal</th>
                                  <th colspan="3"><center>MODAL</center></th>
                                  <th rowspan="2" width="1%">EMKL/KG</th>
                                  <th rowspan="2" width="1%">Biaya EMKL</th>
                                  <th rowspan="2" width="1%">Total</th>
                                  <th rowspan="2" width="1%">Fee 1%</th>
                                  <th rowspan="2" width="1%">Modal/Ctn</th>
                                  <th rowspan="2" width="1%">Tarik ?</th>
                                  <th colspan="
                                  @php
                                    echo count($branch) + 1;
                                  @endphp
                                  " width="2%"><center>PEMBAGIAN CABANG</center></th>
                                </tr>
                                <tr>
                                  <th width="1%">Currency</th>
                                  <th width="1%">Kurs</th>
                                  <th width="1%">IDR</th>
                                  @foreach($branch as $branchs)
                                  <th width="1%">{{ $branchs->branch_name }}</th>
                                  @endforeach
                                </tr>
                               </thead> 
                                <tbody> 
                                  @foreach($capitalgood_detail as $capitalgood_details)
                                  <tr style="background-color: #fff !important">
                                    <td>{{ $capitalgood_details->prefix }} {{ $capitalgood_details->container_no }}</td>
                                    @if($capitalgood_details->mix == 1)
                                    <td>Mix</td>
                                    @else
                                    <td>Not Mix</td>
                                    @endif
                                    <td>{{ $capitalgood_details->po_number }}</td>
                                    <td>{{ $capitalgood_details->doc_date }}</td>
                                    <td>{{ $capitalgood_details->gr_date }}</td>
                                    <td>{{ $capitalgood_details->item_name }}</td>
                                    {{-- <td>{{ $capitalgood_details->description }}</td> --}}
                                    <td>{{ $capitalgood_details->plant }}</td>
                                    <td>{{ $capitalgood_details->store_location_name }}</td>
                                    <td>{{ $capitalgood_details->mat_doc }}</td>
                                    <td>{{ number_format($capitalgood_details->order_qty_po,0) }}</td>
                                    <td>{{ number_format($capitalgood_details->nett_weight_po,0) }}</td>
                                    <td @if($capitalgood_details->dif_qty == 0) style="background-color: #b3ecb3" @else style="background-color: #ecb3c0" @endif>{{ number_format($capitalgood_details->dif_qty,0) }}</td>
                                    <td style="width: 10px !important"> 
                                         <a href="" class="update" data-name="totalmodal" data-pk="{{ $capitalgood_details->id }}" data-type="text" data-title="Modal:" data-emptytext="Null">{{ $capitalgood_details->totalmodal }}</a>
                                    </td>
                                    <td>
                                      <a href="" class="update" data-name="currency_id" data-pk="{{ $capitalgood_details->id }}" data-type="select" data-source='{{ url('editor/capitalgoodnew/lookup') }}'>{{ $capitalgood_details->currency_name }}</a>

                                      {{-- <select onchange="needsave({{$capitalgood_details->id}})" name="detail[{{ $capitalgood_details->id }}][currency_id]"> 
                                        <option value="{{ $capitalgood_details->currency_id}}"> {{$capitalgood_details->currency_name}}</option>
                                        @foreach($currency_list as $currency)
                                        <option value="{{$currency->id}}">{{$currency->currency_name}}</option>
                                        @endforeach
                                      </select> --}}
                                    </td>
                                    <td>{{ $capitalgood_details->rate }}</td>
                                    <td>{{ number_format($capitalgood_details->rateidr,0) }}</td>

                                    
                                     @php
                                      $total = ($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty);

                                      $total_perkg = ($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->nett_weight);
                                    @endphp

                                    @if($capitalgood_details->mix ==1)
                                      <td>
                                      {{ number_format((($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->nett_weight)) + ($total_perkg * 0.01),0) }}</td>
                                    @else
                                      <td>{{ number_format(($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty,0) }}</td>
                                    @endif

                                    @if($capitalgood_details->mix ==1)
                                      @php
                                        if($capitalgood_details->nett_weight_po == 0){
                                          $nett_weight_dev = 1;
                                        }else{
                                          $nett_weight_dev = $capitalgood_details->nett_weight_po;
                                        };
                                      @endphp

                                      <td>{{ number_format(($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / ($capitalgood_details->order_qty * $nett_weight_dev)) }}</td>
                                    @else
                                      <td>{{ number_format(($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty,0) }}</td>
                                    @endif                                    

                                    <td>{{ number_format( ($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty),0) }}</td>

                                   
                                    <td>{{ number_format($total * 0.01,0) }}</td>
                                    <td>{{ number_format(((($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty)) + ($total * 0.01)) + $capitalgood_details->add_modal,0) }}</td>
                                    <td>
                                       @if($capitalgood_details->tarik == 1)
                                        <span class="label label-success"> <i class="fa fa-check"></i> Yes</span>
                                       @else
                                        <span class="label label-danger"> <i class="fa fa-close"></i> No</span>
                                      @endif
                                    </td>
                                    @foreach($branch as $branchs)
                                    <td style="width: 10px !important">
                                        @php 
                                          $branch_name = $branchs->field_name;
                                          echo $capitalgood_details->$branch_name;
                                        @endphp
                                    </td>
                                    @endforeach
                                  </tr>
                                  @endforeach
                                </tbody>
                          </table> 
                          <div class="pull-right">{{ $capitalgood_detail->links() }}</div> 
                          <!-- /.box-body --> 
                   </div>
                <input type="hidden" id="has_change">
                {!! Form::close() !!}   
                <div id="loader"></div>  
          </div>
      </div> 
    </div>  
</section><!-- /.content -->
@stop 

@section('scripts')
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 
    var grfrom = $("#grfrom").val();
    var grto = $("#grto").val();

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to update <i>tarik</i> data from period: <b>' + grfrom + '</b> to period: <b>' + grto + '</b>?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_branchalloc').submit(); 
          }
        },
      }
    });
  });

  function filter_week()
  {
    var filter_week = $("#check_filter").val();
     if(filter_week == 1){
      $("#divgrfrom").hide(500);
      $("#divgrto").hide(500);
      $("#divweek").show(500); 
     }else{
      $("#divgrfrom").show(500);
      $("#divgrto").show(500);
      $("#divweek").hide(500); 
     };
  };

  $(document).ready(function() {   
      @if($datafilter->check_filter == 1)
          $("#divgrfrom").hide(500);
          $("#divgrto").hide(500);
      @else
          $("#divweek").hide(500); 
      @endif
  });

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $('.update').editable({
         url: 'capitalgoodnew/edit',
         type: 'text',
         pk: 1,
         name: 'name',
         title: 'Enter name'
  });

  $(document).ready(function() {  
      document.getElementById("loader").style.display = "none";
      $("#table_capitalgood").dataTable( {
          "sScrollX": true,
           "scrollY": "330px",
           "scrollX": true,
           "autowidth": true,
           "sScrollXInner": "250%",
           "bPaginate": false,
           "dom": '<"toolbar">frtip',
           "autoWidth": false,
           "ordering": false,
           "bInfo" : false,
           "paging": false,
           "searching": false,
           "oSearch": {"sSearch": "{{$datafilter->search_filter}}"},
           fixedColumns:   {
            leftColumns: 7
           },
      });

      $("div.toolbar").html('');

       $.fn.editable.defaults.mode = 'inline';     
      
        //make username editable
        $('#username').editable({
        emptytext: 'Leer',
        });
  });

  webshims.setOptions('forms-ext', {
    replaceUI: 'auto',
    types: 'number'
  });
  webshims.polyfill('forms forms-ext');

  $('#c').on('update', function(e, editable) {
      alert('new value: ');
  });

  function filter()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.datefilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'grfrom': $('#grfrom').val(),    
            'grto': $('#grto').val(),
            'item_category_id': $('#item_category_id').val(),   
            'container_id': $('#container_id').val(),   
            'search': $('#search').val(),   
            'branch_id': $('#branch_id').val(),   
            'week': $('#week').val(),   
            'check_filter': $('#check_filter').val(),   
          }, 
          success: function(data) { 
            // reload_table();
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Filter GR date has been change!', 'Success Alert', options);
          }
        }) 
      }; 

      function needsave(id)
      { 
        console.log(id);
        $("#has_change").val(1);
        //Ajax Load data from ajax
        $.ajax({
          url: 'branchalloc/needsave/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val()
          },
        })
      };

      window.onload= function(){ 
        // console.log(id);
        //Ajax Load data from ajax
        $.ajax({
          url: 'branchalloc/resetneedsave',
          type: "POST",
          data: {
            '_token': $('input[name=_token]').val()
          },
        })
      };

</script>

<script>
var submitted = false;

$(document).ready(function () {
    window.onbeforeunload = function (e) {
        var has_change = $("#has_change").val();
        if (has_change == 1 && !submitted) {
            var message = "You have not saved your changes.", e = e || window.event;
            if (e) {
                e.returnValue = message;
            }
            return message;
        }
    }
     $("form_branchalloc").submit(function() {
         // alert('test');
         submitted = true;
     });
});
</script>
@stop

