@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo" style="font-size:23px !important">
    <i class="fa fa-sticky-note"></i> <a href="#">CLONE PRIVILEGE</a>
  </div>

  <div class="register-box-body">
      <div>
        {!! Form::open(array('route' => 'editor.cloneprevilege.store', 'class'=>'create', 'files' => 'true'))!!}
        {{ csrf_field() }} 
        <p>User From:</p>
        <select name="user_list_id_from" id="user_list_id_from" class="form-control" onchange="filter();" style="margin-top: 10px">       
          @foreach($user_list as $user_lists)
            <option value="{{$user_lists->id}}">{{$user_lists->username}}</option>
          @endforeach
        </select>
        <br>
        <br>
        <p>User To:</p>
        <select name="user_list_id_to" id="user_list_id_to" class="form-control" onchange="filter();" style="margin-top: 10px">       
          @foreach($user_list as $user_lists)
            <option value="{{$user_lists->id}}">{{$user_lists->username}}</option>
          @endforeach
        </select>
      <br/>                         
      <br/>                         
      <button type="submit" class="btn btn-success">START CLONE</button>
      {!! Form::close() !!}  
  </div><!-- /.form-box -->
</div><!-- /.register-box -->

 


@stop
@section('scripts')
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 
    var grfrom = $("#grfrom").val();
    var grto = $("#grto").val();

    // alert(grfrom);

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to update <i>allocation</i> data from period: <b>' + grfrom + '</b> to period: <b>' + grto + '</b>?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_capitalgood').submit(); 
          }
        },

      }
    });
  });

   
    function filter()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.datefilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'grfrom': $('#grfrom').val(),    
            'grto': $('#grto').val(),   
            'item_category_id': $('#item_category_id').val(), 
            'container_id': $('#container_id').val(),   
            'search_filter': $('#search_filter').val(),   
          }, 
          success: function(data) { 
            // var options = { 
            //   "positionClass": "toast-bottom-right", 
            //   "timeOut": 1000, 
            // };
            // toastr.success('Filter GR date has been change!', 'Success Alert', options);
          }
        }) 
      }; 

      function difference(id)
      {
        var orderqty = $("#orderqty").val();
        var allocation = $("#allocation").val();
                                          
        $("#difference").val = orderqty - allocation;
        
      }

      function needsave(id)
      { 
        console.log(id);
        $("#has_change").val(1);
        //Ajax Load data from ajax
        $.ajax({
          url: 'capitalgood/needsave/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val()
          },
        })
      };

      function resetneedsave(){ 
        //Ajax Load data from ajax
        $.ajax({
          url: 'capitalgood/resetneedsave',
          type: "POST",
          data: {
            '_token': $('input[name=_token]').val()
          },
        })
      };
</script>

<script> 
var submitted = false;

$(document).ready(function () {
    window.onbeforeunload = function (e) {
        var has_change = $("#has_change").val();
        if (has_change == 1 && !submitted) {
            var message = "You have not saved your changes.", e = e || window.event;
            if (e) {
                e.returnValue = message;
            }
            return message;
        }
    }
     $("form_capitalgood").submit(function() {
         // alert('test');
         submitted = true;
     });
});
</script>
  @stop
