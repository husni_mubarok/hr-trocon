@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    <i class="fa fa-sticky-note"></i> Email Payroll Slip
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Report</a></li>
    <li class="active">Email Payroll Slip</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12"> 
          <div class="box box-danger">
            <div class="box-header with-border">
              <div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Period">
                {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'RefreshData();')) }} 
              </div>  
              <a href="#" onclick="SendMail();" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-envelope-square"></i> Send Mail</a>
              <div class="box-tools pull-right">
                <div class="tableTools-container">
                </div>
              </div><!-- /.box-tools -->
            </div>
              <div class="box-body">
                  <table id="dtTable" class="table table-bordered table-hover">
              <thead>
                  <tr>
                    <th width="5%">#</th>
                    <th>Select</th>
                    <th>Employee Name</th> 
                    <th>Email</th> 
                </tr>
              </thead>
              <tbody>
              @foreach($employee as $key => $employees)
                <tr>
                    <td>{{$key+1}}</td>
                    <th><input type="checkbox" name="" checked="checked"></th>
                    <td>{{$employees->employeename}}</td> 
                    <td>{{$employees->email}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
      </div>
    </div>
</section>
@stop
@section('scripts')
<script>
$(document).ready(function () {
  $("#dtTable").DataTable( {
    "scrollY": "330px",
    "bPaginate": false,
    "ordering": false
  });
});

function SendMail()
{

  var periodid = $('#periodid').val();
  var perioddesc = $("#periodid option:selected").text();

  $.confirm({
    title: 'Confirm!',
    content: 'Are you sure to send mail <b><u>' + perioddesc + '</u></b> slip?',
    type: 'red',
    typeAnimated: true,
    buttons: {
      cancel: {
       action: function () { 
       }
     },
     confirm: {
      text: 'SEND',
      btnClass: 'btn-red',
      action: function () { 
       waitingDialog.show('Process...', {dialogSize: 'sm', progressType: 'warning'});
       $.ajax({
        url : 'emailpayrollslip/sendmail/' + periodid,
        type: "POST",
        data: {
          '_token': $('input[name=_token]').val() 
        },
        success: function(data)
        { 
          waitingDialog.hide();
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully genareted data!', 'Success Alert', options);
          reload_table();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          $.alert({
            type: 'red',
            icon: 'fa fa-danger', // glyphicon glyphicon-heart
            title: 'Warning',
            content: 'Error generate data!',
          });
        }
      });
     }
   },
 }
});
}

 $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

/**
 * Module for displaying "Waiting for..." dialog using Bootstrap
 *
 * @author Eugene Maslovich <ehpc@em42.ru>
 */

  var waitingDialog = waitingDialog || (function ($) {
      'use strict';

    // Creating modal dialog's DOM
    var $dialog = $(
      '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
      '<div class="modal-dialog modal-m">' +
      '<div class="modal-content">' +
        '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
        '<div class="modal-body">' +
          '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
        '</div>' +
      '</div></div></div>');

    return {
      /**
       * Opens our dialog
       * @param message Process...
       * @param options Custom options:
       *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
       *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
       */
      show: function (message, options) {
        // Assigning defaults
        if (typeof options === 'undefined') {
          options = {};
        }
        if (typeof message === 'undefined') {
          message = 'Loading';
        }
        var settings = $.extend({
          dialogSize: 'm',
          progressType: '',
          onHide: null // This callback runs after the dialog was hidden
        }, options);

        // Configuring dialog
        $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
        $dialog.find('.progress-bar').attr('class', 'progress-bar');
        if (settings.progressType) {
          $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
        }
        $dialog.find('h3').text(message);
        // Adding callbacks
        if (typeof settings.onHide === 'function') {
          $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
            settings.onHide.call($dialog);
          });
        }
        // Opening dialog
        $dialog.modal();
      },
      /**
       * Closes dialog
       */
      hide: function () {
        $dialog.modal('hide');
      }
    };
  })(jQuery);
</script> 
@stop
