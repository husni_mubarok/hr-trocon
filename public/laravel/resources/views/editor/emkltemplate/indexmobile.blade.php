@extends('layouts.editor.templatemobile')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top: -18%; margin-bottom: -5%">
  <h4>
    <i class="fa fa-file"></i> EMKL Template
  </h4>
</section><br>

<section class="content">
{{ csrf_field() }}
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border">
          <div class="row" style="margin-top: -5%">
          <div class="col-lg-5 col-md-5 col-xs-5 col-sm-5" data-toggle="tooltip" data-placement="top" title="Date From">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
              {{ Form::text('grfrom', old('grfrom', $datafilter->grfrom), array('class' => 'form-control', 'placeholder' => 'GR From*', 'required' => 'true', 'id' => 'grfrom', 'onchange' => 'filter();')) }}
            </div><!-- /.input group -->  
          </div>
          <div class="col-lg-5 col-md-5 col-xs-5 col-sm-5" data-toggle="tooltip" data-placement="top" title="Date To" style="margin-left: -5%">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
              {{ Form::text('grfrom', old('grfrom', $datafilter->grto), array('class' => 'form-control', 'placeholder' => 'GR To*', 'required' => 'true', 'id' => 'grto', 'onchange' => 'filter();')) }}
            </div><!-- /.input group --> 
          </div>  
          <div class="col-lg-2 col-md-2 col-xs-2 col-sm-2" data-toggle="tooltip" data-placement="top" title="Date To" style="margin-left: -5%">
            <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          </div>  
        </div> 
          <!-- <button class="btn btn-danger btn-flat" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Bulk Delete</button> -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th>No Container</th> 
                  <th>Group</th>
                  <th>Total Qty</th>
                  <th>Total KG</th>
                  <th>LS</th>
                  <th>Tarik/No Tarik</th>
                  <th>No PIB</th>
                  <th>Biaya PIB</th> 
                  <th>EMKL</th>
                  <th>Plug In</th>
                  <th>LS</th>
                  <th>KALOG</th>
                  <th>Total Modal</th>
                  <th>EMKL/Ctn</th>
                  <th>EMKL/KG</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  


@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 10,
         "sScrollX": true, 
         // "responsive": true,
         // language: { search: "", searchPlaceholder: "Search", sLengthMenu: "_MENU_" },
         "infoEmpty": "Showing 0 to 0 of 0",
         "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
         ajax: "{{ url('editor/emkltemplate/data') }}",
         columns: [  
         { data: 'prefix_container', name: 'prefix_container' },
         { data: 'item_category_name', name: 'item_category_name' },
         { data: 'order_qty', name: 'order_qty', render: $.fn.dataTable.render.number( ',', '.', 0) },
         { data: 'nett_weight', name: 'nett_weight', render: $.fn.dataTable.render.number( ',', '.', 0) },
         { data: 'ls_status', name: 'ls_status'},
         { data: 'tarik_notarik', name: 'tarik_notarik'},
         { data: 'no_pib', name: 'no_pib'},
         { data: 'cost_pib', name: 'cost_pib', render: $.fn.dataTable.render.number( ',', '.', 0) },
         { data: 'emkl_master', name: 'emkl_master', render: $.fn.dataTable.render.number( ',', '.', 0) },
         { data: 'plugin', name: 'plugin', render: $.fn.dataTable.render.number( ',', '.', 0) },
         { data: 'ls', name: 'ls', render: $.fn.dataTable.render.number( ',', '.', 0) },
         { data: 'kalog', name: 'kalog', render: $.fn.dataTable.render.number( ',', '.', 0) },
         { data: 'total_modal', name: 'total_modal', render: $.fn.dataTable.render.number( ',', '.', 0) },
         { data: 'emklctn', name: 'emklctn', render: $.fn.dataTable.render.number( ',', '.', 0) },
         { data: 'emklkg', name: 'emklkg', render: $.fn.dataTable.render.number( ',', '.', 0) }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }
 
       function add()
      {
        $("#btnSave").attr("onclick","save()");
        $("#btnSaveAdd").attr("onclick","saveadd()");
 
        $('.errorItemName').addClass('hidden');
        $('.errorItemNo').addClass('hidden');

        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Item'); // Set Title to Bootstrap modal title
      }

      function save()
      {   
        var url;
        url = "{{ URL::route('editor.item.store') }}";
        
        $.ajax({
          type: 'POST',
          url: url,
          data: {
            '_token': $('input[name=_token]').val(), 
            'item_category_id': $('#item_category_id').val(),
            'item_brand_id': $('#item_brand_id').val(),
            'item_code': $('#item_code').val(), 
            'item_name': $('#item_name').val(), 
            'description': $('#description').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
 
            $('.errorItemName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
              
              if (data.errors.itemname) {
                $('.errorItemName').removeClass('hidden');
                $('.errorItemName').text(data.errors.itemname);
              }
            } else {

              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully added data!', 'Success Alert', options);
              $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };

      function saveadd()
      {   
       $.ajax({
        type: 'POST',
        url: "{{ URL::route('editor.item.store') }}",
        data: {
          '_token': $('input[name=_token]').val(), 
          'item_category_id': $('#item_category_id').val(),
          'item_brand_id': $('#item_brand_id').val(),
          'item_code': $('#item_code').val(), 
          'item_name': $('#item_name').val(), 
          'description': $('#description').val(), 
          'status': $('#status').val()
        },
        success: function(data) {  
            $('.errorItemName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
            
              if (data.errors.rate) {
                $('.errorItemName').removeClass('hidden');
                $('.errorItemName').text(data.errors.rate);
              }
            } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully added data!', 'Success Alert', options);

            $('#form')[0].reset(); // reset form on modals
            reload_table(); 
          } 
        },
      })
     };

     function edit(id)
     { 

      $('.errorItemName').addClass('hidden');

      //alert("asdad");

      $("#btnSave").attr("onclick","update("+id+")");

      $("#btnSaveAdd").attr("onclick","updateadd("+id+")");

      save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : 'item/edit/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

            $('[name="id_key"]').val(data.id); 
            $('[name="item_category_id"]').val(data.item_category_id);
            $('[name="item_brand_id"]').val(data.item_brand_id);
            $('[name="item_code"]').val(data.item_code);
            $('[name="item_name"]').val(data.item_name);
            $('[name="description"]').val(data.description);
            $('[name="status"]').val(data.status);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Item'); // Set title to Bootstrap modal title
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
      }

      function update(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'item/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'item_category_id': $('#item_category_id').val(),
            'item_brand_id': $('#item_brand_id').val(),
            'item_code': $('#item_code').val(), 
            'item_name': $('#item_name').val(), 
            'description': $('#description').val(), 
            'status': $('#status').val()
          },
          success: function(data) {  
            $('.errorItemName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
             
              if (data.errors.itemname) {
                $('.errorItemName').removeClass('hidden');
                $('.errorItemName').text(data.errors.itemname);
              }
            } else {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
            $('#modal_form').modal('hide');
              $('#form')[0].reset(); // reset form on modals
              reload_table(); 
            } 
          },
        })
      };

      function updateadd(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'item/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'item_category_id': $('#item_category_id').val(),
            'item_brand_id': $('#item_brand_id').val(),
            'item_code': $('#item_code').val(), 
            'item_name': $('#item_name').val(), 
            'description': $('#description').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
            if ((data.errors)) {
             swal("Error!", "Gat data failed!", "error")
           } else { 
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
              $('#form')[0].reset(); // reset form on modals
              reload_table(); 
              $("#btnSave").attr("onclick","save()");
              $("#btnSaveAdd").attr("onclick","saveadd()");
            } 
          },
        })
      };

      
      function delete_id(id, itemname, date)
      {
        //var varnamre= $('#itemname').val();
        var itemname = itemname.bold();
        var date = date.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete ' + itemname + ' in ' + date + ' data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : 'item/delete/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error deleteing data!',
                });
              }
            });
           }
         },

       }
     });
      }

      window.onload= function(){ 

        n2= document.getElementById('rate');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        }

        n3= document.getElementById('plusrate');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }

      }

      function filter()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.datefilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'grfrom': $('#grfrom').val(),    
            'grto': $('#grto').val(),
            'item_category_id': $('#item_category_id').val(),   
          }, 
          success: function(data) { 
            reload_table();
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Filter GR date has been change!', 'Success Alert', options);
          }
        }) 
      }; 
   </script> 

   <!-- Add fancyBox -->
   <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
   <script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
   <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox").fancybox();
    });
  </script>
  @stop
