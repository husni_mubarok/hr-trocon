@extends('layouts.editor.template')
@section('title', 'Data Karyawan')
@section('content')
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- <div class="col-xs-2">
		</div> -->
		<div class="col-xs-12">
			<div class="box box-danger">
				@include('errors.error')
				@if(isset($employee))
				{!! Form::model($employee, array('route' => ['editor.employee.update', $employee->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_employee'))!!}
				@else
				{!! Form::open(array('route' => 'editor.employee.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_employee'))!!}
				@endif
				{{ csrf_field() }}
				<div class="box-header with-border">
					<section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
						<h4>
							@if(isset($module))
							<i class="fa fa-pencil"></i> Edit
							@else
							<i class="fa fa-plus"></i> 
							@endif
							Data Karyawan
						</h4>
					</section>
				</div>
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('identityno', 'No Identitas') }}
								{{ Form::text('identityno', old('identityno'), array('class' => 'form-control', 'placeholder' => 'No Identitas*', 'id' => 'identityno')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('nik', 'NIK') }}
								{{ Form::text('nik', old('nik'), array('class' => 'form-control', 'placeholder' => 'NIK*', 'required' => 'true', 'id' => 'nik')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('employeename', 'Nama Karyawan') }}
								{{ Form::text('employeename', old('employeename'), array('class' => 'form-control', 'placeholder' => 'Nama Karyawan*', 'required' => 'true', 'id' => 'employeename')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('nickname', 'Nama Panggilan') }}
								{{ Form::text('nickname', old('nickname'), array('class' => 'form-control', 'placeholder' => 'Nama Panggilan*', 'id' => 'nickname')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('datebirth', 'Tanggal Lahir') }}
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
									{{ Form::text('datebirth', old('datebirth'), array('class' => 'form-control', 'placeholder' => 'Tanggal Lahir*', 'id' => 'datebirth')) }}
								</div><!-- /.input group --> 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('joindate', 'Tanggal Masuk') }}
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
									{{ Form::text('joindate', old('joindate'), array('class' => 'form-control', 'placeholder' => 'Tanggal Masuk*', 'required' => 'true', 'id' => 'joindate')) }}
								</div><!-- /.input group --> 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('joindate', 'Tanggal Keluar') }}
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
									{{ Form::text('termdate', old('termdate'), array('class' => 'form-control', 'placeholder' => 'Tanggal Keluar*', 'id' => 'termdate')) }}
								</div><!-- /.input group --> 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('insuranceno', 'No Asuransi') }}
								{{ Form::text('insuranceno', old('insuranceno'), array('class' => 'form-control', 'placeholder' => 'No Asuransi*', 'id' => 'insuranceno')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('npwp', 'NPWP') }}
								{{ Form::text('npwp', old('npwp'), array('class' => 'form-control', 'placeholder' => 'NPWP*', 'id' => 'npwp')) }} 
							</div>
						</div> 
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('address', 'Alamat') }}
								{{ Form::text('address', old('address'), array('class' => 'form-control', 'placeholder' => 'Alamat*', 'id' => 'address')) }} 
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('hp', 'HP No') }}
								{{ Form::text('hp', old('hp'), array('class' => 'form-control', 'placeholder' => 'HP No*', 'id' => 'hp')) }} 
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('telp1', 'Telp 1') }}
								{{ Form::text('telp1', old('telp1'), array('class' => 'form-control', 'placeholder' => 'Telp 1*', 'id' => 'telp1')) }} 
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('telp2', 'Telp 2') }}
								{{ Form::text('telp2', old('telp2'), array('class' => 'form-control', 'placeholder' => 'Telp 2*', 'id' => 'telp2')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('bankan', 'Nama Akun Bank') }}
								{{ Form::text('bankan', old('bankan'), array('class' => 'form-control', 'placeholder' => 'Nama Akun Bank*', 'id' => 'bankan')) }}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('bankaccount', 'No Rekening') }}
								{{ Form::text('bankaccount', old('bankaccount'), array('class' => 'form-control', 'placeholder' => 'No Rekening*', 'id' => 'bankaccount')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('bankname', 'Nama Bank') }}
								{{ Form::text('bankname', old('bankname'), array('class' => 'form-control', 'placeholder' => 'Nama Bank*', 'id' => 'bankname')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('bankbranch', 'Cabang Bank') }}
								{{ Form::text('bankbranch', old('bankbranch'), array('class' => 'form-control', 'placeholder' => 'Cabang Bank*', 'id' => 'bankbranch')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('cityid', 'Kota') }}
								{{ Form::select('cityid', $city_list, old('cityid'), array('class' => 'form-control select2', 'placeholder' => 'Pilih Kota', 'id' => 'cityid')) }} 
							</div>
						</div> 
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('sex', 'Jenis Kelamin') }}
								{{ Form::select('sex', $sex_list, old('sex'), array('class' => 'form-control', 'placeholder' => 'Pilih Jenis Kelamin', 'id' => 'sex')) }} 
							</div>
						</div> 
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('taxstatus', 'Status Pajak') }}
								{{ Form::select('taxstatus', $taxstatus_list, old('taxstatus'), array('class' => 'form-control', 'placeholder' => 'Pilih Status Pajak', 'id' => 'taxstatus')) }} 
							</div>
						</div> 
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('departmentid', 'Departemen') }}
								{{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control', 'placeholder' => 'Pilih Departemen', 'id' => 'departmentid')) }} 
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('positionid', 'Posisi') }}
								{{ Form::select('positionid', $position_list, old('positionid'), array('class' => 'form-control select2', 'placeholder' => 'Pilih Posisi', 'id' => 'positionid')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('golongan', 'Golongan') }}
								{{ Form::select('gol', $golongan_list, old('golongan'), array('class' => 'form-control', 'placeholder' => 'Pilih Golongan', 'id' => 'golongan')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('paytypeid', 'Jenis Gaji') }}
								{{ Form::select('paytypeid', $payrolltype_list, old('paytypeid'), array('class' => 'form-control', 'required' => 'true', 'id' => 'paytypeid')) }}
							</div>
						</div> 
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('religionid', 'Agama') }}
								{{ Form::select('religionid', $religion_list, old('religionid'), array('class' => 'form-control', 'placeholder' => 'Pilih Agama', 'id' => 'religionid')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('placebirth', 'Tempat Lahir') }}
								{{ Form::select('placebirth', $city_list, old('placebirth'), array('class' => 'form-control select2', 'placeholder' => 'Pilih Tempat Lahir', 'id' => 'placebirth')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('educationlevelid', 'Level Pendidikan') }}
								{{ Form::select('educationlevelid', $educationlevel_list, old('educationlevelid'), array('class' => 'form-control', 'placeholder' => 'Select Level Pendidikan', 'id' => 'educationlevelid')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('educationmajorid', 'Jurusan') }}
								{{ Form::select('educationmajorid', $educationmajor_list, old('educationmajorid'), array('class' => 'form-control select2', 'placeholder' => 'Pilih Jurusan', 'id' => 'educationmajorid')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('locationid', 'Lokasi') }}
								{{ Form::select('locationid', $location_list, old('locationid'), array('class' => 'form-control', 'placeholder' => 'Pilih Lokasi', 'id' => 'locationid')) }}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('jamsostekmember', 'Jamsostek') }}
								<select class="form-control" style="width: 100%;" name="jamsostekmember"  id="jamsostekmember">
								   @if(isset($employee))
					               <option value="{{$employee->jamsostekmember}}">@if($employee->jamsostekmember == 1) Ya @else Tidak @endif</option>
					               @endif
					               <option value="1">Ya</option>
					               <option value="0">Tidak</option>
					             </select>
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('status', 'Status Karyawan') }}
								<select class="form-control" style="width: 100%;" name="status"  id="status">
								   @if(isset($employee))
					               <option value="{{$employee->status}}">@if($employee->status == 0) Aktif @else Tidak Aktif @endif</option>
					               @endif
					               <option value="0">Aktif</option>
					               <option value="1">Tidak Aktif</option>
					             </select>
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('tunjangankesehatan', 'Tunjangan Kesehatan') }}
								{{ Form::select('tunjangankesehatan', $tunjangankesehatan_list, old('tunjangankesehatan'), array('class' => 'form-control', 'placeholder' => 'Pilih Tunjangan Kesehatan', 'id' => 'tunjangankesehatan')) }}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('email', 'Email') }}
								{{ Form::email('email', old('email'), array('class' => 'form-control', 'placeholder' => 'Email*', 'id' => 'email')) }} 
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								{{ Form::label('image', 'Foto') }} 
								<span class="btn btn-default"><input type="file" name="image" /></span><br/>
							</div>
						</div>
					</div>

					<hr>
					<div class="col-md-12">
						<div class="col-md-12">
							<div class="form-group">
								<input class="form-check-input" type="checkbox" id="biayajabatan" name="biayajabatan[]" value="1" @if(isset($employee)) @if($employee->biayajabatan == 1) checked @endif @endif> <b>Biaya Jabatan </b>
							</div> 
							<div class="form-group">
								<input class="form-check-input" type="checkbox" id="statusistimewa" name="statusistimewa[]" value="1" @if(isset($employee)) @if($employee->statusistimewa == 1) checked @endif @endif> <b>Tidak Dibayar Mingguan </b>
							</div>
							<div class="form-group">
								<input class="form-check-input" type="checkbox" id="dibayarbulanandikantor" name="dibayarbulanandikantor[]" value="1" @if(isset($employee)) @if($employee->dibayarbulanandikantor == 1) checked @endif @endif> <b>Dibayar Bulanan di Kantor </b>
							</div>
							<div class="form-group">
								<input class="form-check-input" type="checkbox" id="statuspotpensiun" name="statuspotpensiun[]" value="1" @if(isset($employee)) @if($employee->statuspotpensiun == 1) checked @endif @endif> <b>Status Potongan Pensiun </b>
							</div>
						</div>
					</div>
				</div>


				<div class="box-header with-border">
					<div class="col-md-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
							<a href="{{ URL::route('editor.employee.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
						</div>
					</div>
				</div>
			</section><!-- /.content -->
		</div>
	</div>
</section>
@stop

