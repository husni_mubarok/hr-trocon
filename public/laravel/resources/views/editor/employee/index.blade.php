@extends('layouts.editor.template')
@section('title', 'Data Karyawan')
@section('content')
<!-- Content Wrapper. Contains page content -->
<style type="text/css">
.my_class {
  background-color: white;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    Data Karyawan
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Data Karyawan</li>
  </ol>
</section>

<section class="content">
{{ csrf_field() }}
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <a href="{{ URL::route('editor.employee.create') }}" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Add New</a>
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Kembali</button>
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <button class="btn btn-danger btn-flat" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Hapus Sekaligus</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th style="width:5px">
                    <label class="control control--checkbox">
                      <input type="checkbox" id="check-all"/>
                      <div class="control__indicator"></div>
                    </label> 
                  </th>
                  <th>Aksi</th> 
                  <th>NIK</th>
                  <th>Nama Karyawan</th> 
                  <th>Departemen</th>
                  <th>Posisi</th>
                  <th>Level Jabatan</th>
                  <th>Golongan</th>
                  <th>Pajak</th>
                  <th>Pendidikan</th> 
                  <th>Jenis Kelamin</th>
                  <th>Tanggal Masuk</th>
                  <th>Tanggal Selesai</th>
                  <th>Payroll</th>
                  <th>NPWP</th>
                  <th>Nama Bank</th>
                  <th>No Rekening</th> 
                  <th>Jamsostek</th>
                  <th>Grade</th> 
                  <th>Tun Kes</th> 
                  <th>Email</th> 
                  <th>Foto</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  



@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "scrollX": true,
         "sScrollXInner": "210%",
         "order": [[ 6, "asc" ]],
         "autoWidth": true,
         "rowReorder": true,
         fixedColumns:   {
          leftColumns: 4,
          rightColumns: 1
         },
          "aoColumnDefs": [
        { "sClass": "my_class", "aTargets": [ 0, 1, 2, 3, 22 ] }
        ],
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/employee/data') }}",
         columns: [  
         { data: 'check', name: 'check', orderable: false, searchable: false },
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'nik', name: 'nik' },
         { data: 'employeename', name: 'employeename' }, 
         { data: 'departmentname', name: 'departmentname' },
         { data: 'positionname', name: 'positionname' },
         { data: 'positionlevel', name: 'positionlevel' },
         { data: 'gol', name: 'gol' },
         { data: 'taxstatus', name: 'taxstatus' },
         { data: 'educationlevelname', name: 'educationlevelname' },
         { data: 'gender', name: 'gender' },
         { data: 'joindate', name: 'joindate' },
         { data: 'termdate', name: 'termdate' },
         { data: 'payrolltypename', name: 'payrolltypename' },
         { data: 'npwp', name: 'npwp' },
         { data: 'bankaccount', name: 'bankaccount' },
         { data: 'bankan', name: 'bankan' },  
         { data: 'jamsostekm', name: 'jamsostekm' }, 
         { data: 'gol', name: 'gol' }, 
         { data: 'tunjangankesehatan', name: 'tunjangankesehatan' }, 
         { data: 'email', name: 'email' }, 
         { data: 'image', name: 'image' },
         { data: 'mstatus', name: 'mstatus' }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
      function reload_table()
      {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function delete_id(id, employeename)
      {

        //var varnamre= $('#employeename').val();
        var employeename = employeename.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete ' + employeename + ' data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : 'employee/delete/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error deleteing data!',
                      });
              }
            });
           }
         },

       }
     });
      }

      function bulk_delete()
      {


        var list_id = [];
        $(".data-check:checked").each(function() {
          list_id.push(this.value);
        });
        if(list_id.length > 0)
        {


          $.confirm({
            title: 'Confirm!',
            content: 'Yakin akan menghapus data '+list_id.length+'?',
            type: 'red',
            typeAnimated: true,
            buttons: {
              cancel: {
               action: function () {

               }
             },
             confirm: {
              text: 'DELETE',
              btnClass: 'btn-red',
              action: function () {
               $.ajax({
                 data: {
                  '_token': $('input[name=_token]').val(),
                  'idkey': list_id,
                }, 
                url: "employee/deletebulk",
                type: "POST", 
                dataType: "JSON",
                success: function(data)
                {
                  if(data.status)
                  {


                    reload_table();
                  }
                  else
                  {
                    alert('Failed.');
                  }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                  $.alert({
                    type: 'red',
                                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                title: 'Warning',
                                content: 'Hapus data gagal!',
                              });
                }
              });
             }
           },

         }
       });
        }
        else
        {
         $.alert({
          type: 'orange',
                icon: 'fa fa-warning', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'No data selected!',
              });
       }
     }
   </script> 

   <!-- Add fancyBox -->
   <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
   <script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
   <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox").fancybox();
    });
  </script>
  @stop
