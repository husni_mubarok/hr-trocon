@extends('layouts.editor.template')
@section('title', 'Gaji Karyawan')
@section('content')
<!-- Content Wrapper. Contains page content -->
<style type="text/css">
.my_class {
  background-color: white;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    Gaji Karyawan
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Gaji Karyawan</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important"> 
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Kembali</button>
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <button class="btn btn-danger btn-flat" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Hapus Sekaligus</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th style="width:5%">
                    <label class="control control--checkbox">
                      <input type="checkbox" id="check-all"/>
                      <div class="control__indicator"></div>
                    </label>
                  </th>
                  <th>Aksi</th> 
                  <th>Nama Karyawan</th> 
                  <th>Posisi</th>
                  <th>Gaji</th>
                  <th>Gaji Porposional</th>
                  <th>Tunjangan Keahlian</th>
                  <th>Makan Transport Tetap</th>
                  <th>Lembur Tetap</th>
                  <th>Tunjangan Kesehatan</th>
                  <th>Rate Lembur</th>
                  <th>Rate Uang Makan</th>
                  <th>Rate Malam</th>
                  <th>Rate Luar Kota</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:50% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <input name="extrapudding" id="extrapudding" class="form-control" type="hidden">
        <input name="ratemealincity" id="ratemealincity" class="form-control" type="hidden">
        <div class="modal-header" style="height: 60px">
          <div class="form-group pull-right">
            <label for="real_name" class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8 pull-right">
              <select class="form-control" style="width: 100%;" name="status"  id="status">
               <option value="0">Aktif</option>
               <option value="1">Tidak Aktif</option>
             </select>
           </div>
         </div>
         <h3 class="modal-title">Form Gaji Karyawan</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row"> 
            <div class="form-group">
              <label class="control-label col-md-3">Nama Karyawan</label>
              <div class="col-md-8">
                <input name="employeename" id="employeename" class="form-control" type="text" readonly> 
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Gaji</label>
              <div class="col-md-8">
                <input name="basic" id="basic" class="form-control" type="text" oninput="cal_sparator_basic()">
                <small class="errorEducationlevelName hidden alert-danger"></small> 
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Gaji Porposional</label>
              <div class="col-md-8">
                <input name="basicbegin" id="basicbegin" class="form-control" type="text" oninput="cal_sparator_basicbegin()">
                <small class="errorEducationlevelName hidden alert-danger"></small> 
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Tunjangan Keahlian</label>
              <div class="col-md-8">
                <input name="postall" id="postall" class="form-control" type="text" oninput="cal_sparator_postall()">
                <small class="errorEducationlevelName hidden alert-danger"></small> 
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Makan Transport Tetap</label>
              <div class="col-md-8">
                <input name="mealtransall" id="mealtransall" class="form-control" type="text" oninput="cal_sparator_mealtransall()">
                <small class="errorEducationlevelName hidden alert-danger"></small> 
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Lembur Tetap</label>
              <div class="col-md-8">
                <input name="overtimeall" id="overtimeall" class="form-control" type="text" oninput="cal_sparator_overtimeall()">
                <small class="errorEducationlevelName hidden alert-danger"></small> 
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Tunjangan Kesehatan</label>
              <div class="col-md-8">
                <input name="medicalall" id="medicalall" class="form-control" type="text" oninput="cal_sparator_medicalall()">
                <small class="errorEducationlevelName hidden alert-danger"></small> 
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Rate Lembur</label>
              <div class="col-md-8">
                <input name="rateovertime" id="rateovertime" class="form-control" type="text" oninput="cal_sparator_rateovertime()">
                <small class="errorEducationlevelName hidden alert-danger"></small> 
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Rate Uang Makan</label>
              <div class="col-md-8">
                <input name="mealtransrate" id="mealtransrate" class="form-control" type="text" oninput="cal_sparator_mealtransrate()">
                <small class="errorEducationlevelName hidden alert-danger"></small> 
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Rate Malam</label>
              <div class="col-md-8">
                <input name="ratetunjanganmalam" id="ratetunjanganmalam" class="form-control" type="text" oninput="cal_sparator_ratetunjanganmalam()">
                <small class="errorEducationlevelName hidden alert-danger"></small> 
              </div>
            </div>  
            <div class="form-group">
              <label class="control-label col-md-3">Rate Luar Kota</label>
              <div class="col-md-8">
                <input name="ratemealoutcity" id="ratemealoutcity" class="form-control" type="text" oninput="cal_sparator_ratemealoutcity()">
                <small class="errorEducationlevelName hidden alert-danger"></small> 
              </div>
            </div>  
          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Tutup</button> 
      <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Simpan & Tutup</button>

    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "scrollX": true,
         "sScrollXInner": "150%",
         "autoWidth": true,
         "rowReorder": true,
         fixedColumns:   {
          leftColumns: 4,
          rightColumns: 1
         },
         "aoColumnDefs": [
        { "sClass": "my_class", "aTargets": [ 0, 1, 2, 3, 13 ] }
        ],
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/employeesalary/data') }}",
         columns: [  
         { data: 'check', name: 'check', orderable: false, searchable: false, "width": "2%" },
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'employeename', name: 'employeename' },
         { data: 'positionname', name: 'positionname' }, 
         { data: 'basic', name: 'basic', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },
         { data: 'basicbegin', name: 'basicbegin', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },
         { data: 'postall', name: 'postall', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },
         { data: 'mealtransall', name: 'mealtransall', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },
         { data: 'overtimeall', name: 'overtimeall', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },
         { data: 'medicalall', name: 'medicalall', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },
         { data: 'rateovertime', name: 'rateovertime', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },
         { data: 'mealtransrate', name: 'mealtransrate', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },
         { data: 'ratetunjanganmalam', name: 'ratetunjanganmalam', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },
         // { data: 'ratemealincity', name: 'ratemealincity', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },
         { data: 'ratemealoutcity', name: 'ratemealoutcity', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },
         { data: 'mstatus', name: 'mstatus' }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

        

     function edit(id)
     { 

      $('.errorCityName').addClass('hidden');

      //alert(id);

      $("#btnSave").attr("onclick","update("+id+")");

      $("#btnSaveAdd").attr("onclick","updateadd("+id+")");

      save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : 'employeesalary/edit/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          { 
            $('[name="id_key"]').val(data.id); 
            $('[name="employeename"]').val(data.employeename);
            $('[name="basic"]').val(data.basic);
            $('[name="basicbegin"]').val(data.basicbegin);
            $('[name="postall"]').val(data.postall);
            $('[name="mealtransall"]').val(data.mealtransall);
            $('[name="overtimeall"]').val(data.overtimeall);
            $('[name="extrapudding"]').val(data.extrapudding);
            $('[name="medicalall"]').val(data.medicalall);
            $('[name="rateovertime"]').val(data.rateovertime);
            $('[name="mealtransrate"]').val(data.mealtransrate);
            $('[name="ratetunjanganmalam"]').val(data.ratetunjanganmalam);
            $('[name="ratemealincity"]').val(data.ratemealincity);
            $('[name="ratemealoutcity"]').val(data.ratemealoutcity);
            $('[name="status"]').val(data.status);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Seting Gaji'); // Set title to Bootstrap modal title
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                alert('Error get data from ajax');
              }
            });
      }

      function update(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'employeesalary/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'basic': $('#basic').val(),  
            'basicbegin': $('#basicbegin').val(),  
            'postall': $('#postall').val(),
            'mealtransall': $('#mealtransall').val(),
            'overtimeall': $('#overtimeall').val(),
            'extrapudding': $('#extrapudding').val(),
            'medicalall': $('#medicalall').val(),
            'rateovertime': $('#rateovertime').val(),
            'mealtransrate': $('#mealtransrate').val(),
            'ratetunjanganmalam': $('#ratetunjanganmalam').val(),
            'ratemealincity': $('#ratemealincity').val(), 
            'ratemealoutcity': $('#ratemealoutcity').val(), 
            'status': $('#status').val()
          },
          success: function(data) {  
            $('.errorCityName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data wajib diisi!', 'Validasi Error', options);
             
              if (data.errors.employeesalaryname) {
                $('.errorCityName').removeClass('hidden');
                $('.errorCityName').text(data.errors.employeesalaryname);
              }
            } else {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
            $('#modal_form').modal('hide');
            $('#form')[0].reset(); // reset form on modals
            reload_table(); 
          } 
        },
      })
      }; 


      function cal_sparator_basic() {  

        var basic = document.getElementById('basic').value;
        var result = document.getElementById('basic');
        var rsamount = (basic);
        result.value = rsamount.replace(/,/g, "");  


        n2= document.getElementById('basic');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='basic')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }


      function cal_sparator_basicbegin() {  

        var basicbegin = document.getElementById('basicbegin').value;
        var result = document.getElementById('basicbegin');
        var rsamount = (basicbegin);
        result.value = rsamount.replace(/,/g, "");  


        n2= document.getElementById('basicbegin');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='basicbegin')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }
      

      function cal_sparator_postall() {  

        var postall = document.getElementById('postall').value;
        var result = document.getElementById('postall');
        var rsamount = (postall);
        result.value = rsamount.replace(/,/g, "");  


        n2= document.getElementById('postall');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='postall')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }


      function cal_sparator_medicalall() {  

        var medicalall = document.getElementById('medicalall').value;
        var result = document.getElementById('medicalall');
        var rsamount = (medicalall);
        result.value = rsamount.replace(/,/g, "");  


        n2= document.getElementById('medicalall');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='medicalall')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }



      function cal_sparator_mealtransall() {  

        var mealtransall = document.getElementById('mealtransall').value;
        var result = document.getElementById('mealtransall');
        var rsamount = (mealtransall);
        result.value = rsamount.replace(/,/g, "");  


        n2= document.getElementById('mealtransall');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='mealtransall')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }


      function cal_sparator_overtimeall() {  

        var overtimeall = document.getElementById('overtimeall').value;
        var result = document.getElementById('overtimeall');
        var rsamount = (overtimeall);
        result.value = rsamount.replace(/,/g, "");  


        n2= document.getElementById('overtimeall');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='overtimeall')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }


       function cal_sparator_rateovertime() {  

        var rateovertime = document.getElementById('rateovertime').value;
        var result = document.getElementById('rateovertime');
        var rsamount = (rateovertime);
        result.value = rsamount.replace(/,/g, "");  


        n2= document.getElementById('rateovertime');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='rateovertime')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }


      function cal_sparator_mealtransrate() {  

        var mealtransrate = document.getElementById('mealtransrate').value;
        var result = document.getElementById('mealtransrate');
        var rsamount = (mealtransrate);
        result.value = rsamount.replace(/,/g, "");  


        n2= document.getElementById('mealtransrate');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='mealtransrate')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }

      function cal_sparator_ratetunjanganmalam() {  

        var ratetunjanganmalam = document.getElementById('ratetunjanganmalam').value;
        var result = document.getElementById('ratetunjanganmalam');
        var rsamount = (ratetunjanganmalam);
        result.value = rsamount.replace(/,/g, "");  


        n2= document.getElementById('ratetunjanganmalam');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='ratetunjanganmalam')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }


      function cal_sparator_ratemealoutcity() {  

        var ratemealoutcity = document.getElementById('ratemealoutcity').value;
        var result = document.getElementById('ratemealoutcity');
        var rsamount = (ratemealoutcity);
        result.value = rsamount.replace(/,/g, "");  


        n2= document.getElementById('ratemealoutcity');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='ratemealoutcity')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }
 
   </script> 
   @stop
