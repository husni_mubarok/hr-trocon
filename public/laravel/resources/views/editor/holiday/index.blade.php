@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    Hari Libur
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Hari Libur</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <button onClick="add()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Tambah Baru</button>
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Kembali</button>
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <button class="btn btn-danger btn-flat" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Hapus Sekaligus</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th style="width:5%">
                    <label class="control control--checkbox">
                      <input type="checkbox" id="check-all"/>
                      <div class="control__indicator"></div>
                    </label> 
                  </th>
                  <th>Aksi</th> 
                  <th>Keterangan</th> 
                  <th>Tanggal Hari Libur</th>
                  <th>Wajib</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
          <div class="form-group pull-right">
            <label for="real_name" class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8 pull-right">
              <select class="form-control" style="width: 100%;" name="status"  id="status">
               <option value="0">Aktif</option>
               <option value="1">Tidak Aktif</option>
             </select>
           </div>
         </div>
         <h3 class="modal-title">Hari Libur Form</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row">
 
            <div class="form-group">
              <label class="control-label col-md-3">Keterangan</label>
              <div class="col-md-8">
                <input name="description" id="description" class="form-control" type="text">
                <small class="errorKeterangan hidden alert-danger"></small> 
              </div>
            </div> 

            <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Tanggal Hari Libur</label>
                <div class="col-sm-8">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('dateholiday', old('dateholiday'), array('class' => 'form-control', 'placeholder' => 'Tanggal Hari Libur*', 'required' => 'true', 'id' => 'dateholiday', 'onclick' => 'saveheader();')) }}
                </div><!-- /.input group --> 
              </div>
            </div> 

            <div class="form-group">
              <label class="control-label col-md-3">Wajib</label>
              <div class="col-md-8">
                <select name="description" id="description" class="form-control">
                  <option value="1">Ya</option>
                  <option value="0">Tidak</option>
                </select>
                <small class="errorWajib hidden alert-danger"></small> 
              </div>
            </div> 

          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Tutup</button>
      <button type="button" id="btnSaveAdd" onClick="saveadd()" class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-plus-square"></i> Simpan & Tambah</button>
      <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Simpan & Tutup</button>

    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/holiday/data') }}",
         columns: [  
         { data: 'check', name: 'check', orderable: false, searchable: false },
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'description', name: 'description' },
         { data: 'dateholiday', name: 'dateholiday', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'mandatory', name: 'mandatory', render: function(data, type, row){return data == '1' ? '<span style="color:#008000">Yes</span>' : '<span style="color:#696969">No</span>'} }, 
         { data: 'mstatus', name: 'mstatus' }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function add()
      {
        $("#btnSave").attr("onclick","save()");
        $("#btnSaveAdd").attr("onclick","saveadd()");
 
        $('.errorKeterangan').addClass('hidden');

        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Tambah Hari Libur'); // Set Title to Bootstrap modal title
      }

      function save()
      {   
        var url;
        url = "{{ URL::route('editor.holiday.store') }}";
        
        $.ajax({
          type: 'POST',
          url: url,
          data: {
            '_token': $('input[name=_token]').val(), 
            'description': $('#description').val(), 
            'dateholiday': $('#dateholiday').val(), 
            'mandatory': $('#mandatory').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
 
            $('.errorKeterangan').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data wajib diisi!', 'Validasi Error', options);
              
              if (data.errors.description) {
                $('.errorKeterangan').removeClass('hidden');
                $('.errorKeterangan').text(data.errors.description);
              }
            } else {

              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Tambah data berhasil!', 'Success Alert', options);
              $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };

      function saveadd()
      {   
       $.ajax({
        type: 'POST',
        url: "{{ URL::route('editor.holiday.store') }}",
        data: {
          '_token': $('input[name=_token]').val(), 
          'description': $('#description').val(), 
          'dateholiday': $('#dateholiday').val(), 
          'mandatory': $('#description').val(), 
          'status': $('#status').val()
        },
        success: function(data) {  
            $('.errorKeterangan').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data wajib diisi!', 'Validasi Error', options);
            
              if (data.errors.description) {
                $('.errorKeterangan').removeClass('hidden');
                $('.errorKeterangan').text(data.errors.description);
              }
            } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Tambah data berhasil!', 'Success Alert', options);

                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
     };

     function edit(id)
     { 

      $('.errorKeterangan').addClass('hidden');

      //alert("asdad");

      $("#btnSave").attr("onclick","update("+id+")");

      $("#btnSaveAdd").attr("onclick","updateadd("+id+")");

      save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : 'holiday/edit/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

            $('[name="id_key"]').val(data.id); 
            $('[name="description"]').val(data.description);
            $('[name="dateholiday"]').val(data.dateholiday);
            $('[name="mandatory"]').val(data.mandatory);
            $('[name="status"]').val(data.status);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Hari Libur'); // Set title to Bootstrap modal title
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                alert('Error get data from ajax');
              }
            });
      }

      function update(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'holiday/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'description': $('#description').val(), 
            'dateholiday': $('#dateholiday').val(), 
            'mandatory': $('#mandatory').val(), 
            'status': $('#status').val()
          },
          success: function(data) {  
            $('.errorKeterangan').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data wajib diisi!', 'Validasi Error', options);
             
              if (data.errors.description) {
                $('.errorKeterangan').removeClass('hidden');
                $('.errorKeterangan').text(data.errors.description);
              }
            } else {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
            $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };

      function updateadd(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'holiday/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'description': $('#description').val(), 
            'dateholiday': $('#dateholiday').val(), 
            'mandatory': $('#mandatory').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
            if ((data.errors)) {
             swal("Error!", "Gat data failed!", "error")
           } else { 
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                    $("#btnSave").attr("onclick","save()");
                    $("#btnSaveAdd").attr("onclick","saveadd()");
                  } 
                },
              })
      };

      function delete_id(id, description)
      {

        //var varnamre= $('#description').val();
        var description = description.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete ' + description + ' data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : 'holiday/delete/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error deleteing data!',
                      });
              }
            });
           }
         },

       }
     });
      }

      function bulk_delete()
      {


        var list_id = [];
        $(".data-check:checked").each(function() {
          list_id.push(this.value);
        });
        if(list_id.length > 0)
        {


          $.confirm({
            title: 'Confirm!',
            content: 'Yakin akan menghapus data '+list_id.length+'?',
            type: 'red',
            typeAnimated: true,
            buttons: {
              cancel: {
               action: function () {

               }
             },
             confirm: {
              text: 'DELETE',
              btnClass: 'btn-red',
              action: function () {
               $.ajax({
                 data: {
                  '_token': $('input[name=_token]').val(),
                  'idkey': list_id,
                }, 
                url: "holiday/deletebulk",
                type: "POST", 
                dataType: "JSON",
                success: function(data)
                {
                  if(data.status)
                  {


                    reload_table();
                  }
                  else
                  {
                    alert('Failed.');
                  }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                  $.alert({
                    type: 'red',
                                icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                title: 'Warning',
                                content: 'Hapus data gagal!',
                              });
                }
              });
             }
           },

         }
       });
        }
        else
        {
         $.alert({
          type: 'orange',
                icon: 'fa fa-warning', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'No data selected!',
              });
       }
     }
   </script> 
   @stop
