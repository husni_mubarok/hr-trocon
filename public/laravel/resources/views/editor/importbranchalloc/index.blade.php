@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo" style="font-size:23px !important">
    <i class="fa fa-sticky-note"></i> <a href="#">EXPORT/IMPORT BRANCH ALLOCATION</a>
  </div>

  <div class="register-box-body">
      <div>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            {{ Form::text("grfrom", old("grfrom", $datafilter->grfrom), array("class" => "form-control", "placeholder" => "GR From*", "required" => "true", "id" => "grfrom", "onchange" => "filter();")) }}
        </div><!-- /.input group -->
     
        <div class="input-group" style="margin-top: 5px">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          {{ Form::text("grto", old("grto", $datafilter->grto), array("class" => "form-control", "placeholder" => "GR To*", "required" => "true", "id" => "grto", "onchange" => "filter();")) }}
        </div><!-- /.input group --><br>
        
        <select name="item_category_id" id="item_category_id" class="form-control select2" onchange="filter();" style="margin-top: 10px">       
          @if($datafilter->item_category_id == 0)
            <option value="0">All Category</option> 
            @else <option value="{{$datafilter->item_category_id}}">{{$datafilter->item_category_name}}</option> 
          @endif <option value="0">All Category</option> 
          @foreach($item_category as $item_categorys)
            <option value="{{$item_categorys->id}}">{{$item_categorys->item_category_name}}</option>
          @endforeach
        </select>

        <button onClick="filter(); history.go(0);" type="button" class="btn btn-success btn-flat pull-right" style="margin-top: 5px"> <i class="fa fa-refresh"></i> Refresh</button>
      </div>
      <hr style="margin-top: 40px">
     {!! Form::open(array('route' => 'editor.branchalloc.storeimport', 'class'=>'create', 'files' => 'true'))!!}
      {{ csrf_field() }}<br/> 
      {{ Form::label('import_file', 'Import Item from Excel File (.ods)') }}<br>
      Max 5MB
      {{ Form::file('import_file') }} 
      <br/>                         
      <button type="submit" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Import</button>
      {!! Form::close() !!}  
      <br>
     <a href="{{ url('editor/branchalloc/storeexport/xls') }}" class="btn btn-success"><i class="fa fa-download"></i> Download Template .xls </a>
     <hr>
     <p>Note: To view result from Branch Allocation Import please check Branch Allocation Report</p>
    {!! Form::close() !!}     
  </div><!-- /.form-box -->
</div><!-- /.register-box -->

 


@stop
@section('scripts')
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 
    var grfrom = $("#grfrom").val();
    var grto = $("#grto").val();

    // alert(grfrom);

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to update <i>allocation</i> data from period: <b>' + grfrom + '</b> to period: <b>' + grto + '</b>?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_capitalgood').submit(); 
          }
        },

      }
    });
  });

   
    function filter()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.datefilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'grfrom': $('#grfrom').val(),    
            'grto': $('#grto').val(),   
            'item_category_id': $('#item_category_id').val(), 
            'container_id': $('#container_id').val(),   
            'search_filter': $('#search_filter').val(),   
          }, 
          success: function(data) { 
            // var options = { 
            //   "positionClass": "toast-bottom-right", 
            //   "timeOut": 1000, 
            // };
            // toastr.success('Filter GR date has been change!', 'Success Alert', options);
          }
        }) 
      }; 

      function difference(id)
      {
        var orderqty = $("#orderqty").val();
        var allocation = $("#allocation").val();
                                          
        $("#difference").val = orderqty - allocation;
        
      }

      function needsave(id)
      { 
        console.log(id);
        $("#has_change").val(1);
        //Ajax Load data from ajax
        $.ajax({
          url: 'capitalgood/needsave/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val()
          },
        })
      };

      function resetneedsave(){ 
        //Ajax Load data from ajax
        $.ajax({
          url: 'capitalgood/resetneedsave',
          type: "POST",
          data: {
            '_token': $('input[name=_token]').val()
          },
        })
      };
</script>

<script> 
var submitted = false;

$(document).ready(function () {
    window.onbeforeunload = function (e) {
        var has_change = $("#has_change").val();
        if (has_change == 1 && !submitted) {
            var message = "You have not saved your changes.", e = e || window.event;
            if (e) {
                e.returnValue = message;
            }
            return message;
        }
    }
     $("form_capitalgood").submit(function() {
         // alert('test');
         submitted = true;
     });
});
</script>
  @stop
