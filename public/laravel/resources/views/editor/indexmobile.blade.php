@extends('layouts.editor.templatemobile')
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header hidden-xs">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content" style="margin-top: -15%; padding: 5% 5%;">
    @actionStart('dashboarduser', 'read')
    <!-- Main content -->
    <section class="content">
     <h4>User</h4>
     <!-- Info boxes -->
      <div class="row">
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-download"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Incoming Today</span>
              <span class="info-box-number">{{ $incoming->count_incoming }}</span>
              <button onclick="show_modal_table_incoming();" type="button" class="btn btn-default btn-block">Detail</button>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-book"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Outstanding</span>
              <span class="info-box-number">{{ $outstanding->count_outstanding }}</span>
              <button type="button" onclick="show_modal_table_outstanding();" class="btn btn-default btn-block">Detail</button>
            </div>

            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-cart-plus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Receive</span>
              <span class="info-box-number">{{ number_format($receive->count_receive) }}</span>
              <button type="button" onclick="show_modal_table_receive();" class="btn btn-default btn-block">Detail</button>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-podcast"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Change</span>
              <span class="info-box-number">{{ $change->count_change }}</span>
              <button type="button" onclick="show_modal_table_change();" class="btn btn-default btn-block">Detail</button>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      @actionEnd

      
      <h4>Report</h4>

      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-list"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Summary GR by Container </span>
              <a href="{{ URL::route('editor.summarygr.index') }}" type="button" class="btn btn-success btn-block">Open Report</a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-list"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Monitoring Container</span>
              <a href="{{ URL::route('editor.monitoringcontainer.index') }}" type="button" class="btn btn-success btn-block">Open Report</a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-th-list"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">EMKL Template</span> 
              <a href="{{ URL::route('editor.emkltemplate.index') }}" type="button" class="btn btn-success btn-block">Open Report</a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-pencil-square-o "></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Branch Allocation Report</span>
              <a href="{{ URL::route('editor.branchallocationreport.index') }}" type="button" class="btn btn-success btn-block">Open Report</a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


       <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">KKPB Report</span>
              <a href="{{ URL::route('editor.kkbpreport.index') }}" type="button" class="btn btn-success btn-block">Open Report</a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-calendar-times-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PIB Report</span>
              <a href="{{ URL::route('editor.pibreport.index') }}" type="button" class="btn btn-success btn-block">Open Report</a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
      <!-- /.row -->

    @if($popup->date_popup == $popup->date_now)
      <div class="container">
          <!-- Modal -->
          <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h1 class="modal-title">{{$popup->popup_name}}</h1>
                </div>
                <div class="modal-body">
                  {!!$popup->description!!}
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
    @endif


    <div class="container">
          <!-- Modal -->
           <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="modal_table_incoming" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">INCOMING TODAY</h3>
              </div>
              <div class="modal-body"> 
                  <table id="dt_table_incoming" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Container No</th>
                <th>Container Name</th> 
                <th>Date Coming</th> 
              </tr>
              </thead>
            </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="container">
        <!-- Modal -->
        <div class="modal fade" id="modal_table_outstanding" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">OUTSTANDING</h3>
              </div>
              <div class="modal-body"> 
                  <table id="dt_table_outstanding" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Container No</th>
                <th>Container Name</th> 
                <th>Date PO</th> 
              </tr>
              </thead>
            </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>



      <div class="container">
       <!-- Modal -->
        <div class="modal fade" id="modal_table_receive" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">RECEIVE</h3>
              </div>
              <div class="modal-body"> 
                  <table id="dt_table_receive" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Container No</th>
                <th>Container Name</th> 
                <th>Date Receive</th> 
              </tr>
              </thead>
            </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="container">
       <!-- Modal -->
        <div class="modal fade" id="modal_table_change" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">CHANGE</h3>
              </div>
              <div class="modal-body"> 
                  <table id="dt_table_change" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>Type</th>
                <th>Date</th> 
                <th>Description</th> 
              </tr>
              </thead>
            </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->

 
<script type="text/javascript"> 
  var table_incoming;
  $(document).ready(function() {
    //datatables
    table_incoming = $('#dt_table_incoming').DataTable({ 
     processing: true,
     serverSide: true,
     autowidth: true,
     responsive: true,
     ajax: "{{ url('editor/incomingtoday') }}",
     columns: [  
       { data: 'container_no', name: 'container_no' }, 
       { data: 'container_name', name: 'container_name' },
       { data: 'gr_date', name: 'gr_date' }
       ]
    });
  });

  function show_modal_table_incoming()
  {
    $('#modal_table_incoming').modal('show');
    table_incoming.ajax.reload(null,false); 
  }; 



  var table_outstanding;
  $(document).ready(function() {
    //datatables
    table_outstanding = $('#dt_table_outstanding').DataTable({ 
     processing: true,
     serverSide: true,
     autowidth: true,
     responsive: true,
     ajax: "{{ url('editor/outstanding') }}",
     columns: [  
       { data: 'container_no', name: 'container_no' }, 
       { data: 'container_name', name: 'container_name' },
       { data: 'doc_date', name: 'doc_date' }
       ]
    });
  });

  function show_modal_table_outstanding()
  {
    $('#modal_table_outstanding').modal('show');
    table_outstanding.ajax.reload(null,false); 
  }; 


  var table_receive;
  $(document).ready(function() {
    //datatables
    table_receive = $('#dt_table_receive').DataTable({ 
     processing: true,
     serverSide: true,
     autowidth: true,
     responsive: true,
     ajax: "{{ url('editor/receive') }}",
     columns: [  
       { data: 'container_no', name: 'container_no' }, 
       { data: 'container_name', name: 'container_name' },
       { data: 'gr_date', name: 'gr_date' }
       ]
    });
  });

  function show_modal_table_receive()
  {
    $('#modal_table_receive').modal('show');
    table_receive.ajax.reload(null,false); 
  }; 


  var table_change;
  $(document).ready(function() {
    //datatables
    table_change = $('#dt_table_change').DataTable({ 
     processing: true,
     serverSide: true,
     autowidth: true,
     responsive: true,
     "order": [[ 0, "desc" ]],
     ajax: "{{ url('editor/change') }}",
     columns: [  
       { data: 'type', name: 'type' }, 
       { data: 'date_time', name: 'date_time' },
       { data: 'description', name: 'description' }
       ]
    });
  });

  function show_modal_table_change()
  {
    $('#modal_table_change').modal('show');
    table_change.ajax.reload(null,false); 
  }; 


  var table_eta;
  $(document).ready(function() {
    //datatables
    table_eta = $('#dt_table_eta').DataTable({ 
     processing: true,
     serverSide: true,
     autowidth: true,
     responsive: true,
     "order": [[ 0, "desc" ]],
     ajax: "{{ url('editor/eta') }}",
     columns: [  
       { data: 'container_name', name: 'container_name' }, 
       { data: 'no_pib', name: 'no_pib' },
       { data: 'emkl', name: 'emkl' },
       { data: 'shipping_line', name: 'shipping_line' },
       ]
    });
  });

  function show_modal_table_eta()
  {
    $('#modal_table_eta').modal('show');
    table_eta.ajax.reload(null,false); 
  }; 


  var table_geser;
  $(document).ready(function() {
    //datatables
    table_geser = $('#dt_table_geser').DataTable({ 
     processing: true,
     serverSide: true,
     autowidth: true,
     // responsive: true,
     "order": [[ 0, "desc" ]],
     ajax: "{{ url('editor/geser') }}",
     columns: [  
       { data: 'container_no', name: 'container_no' }, 
       { data: 'doc_date', name: 'doc_date' },
       { data: 'gr_date', name: 'gr_date' },
       { data: 'item_code', name: 'item_code' },
       { data: 'item_name', name: 'item_name' },
       { data: 'order_qty', name: 'order_qty' },
       { data: 'gr_qty', name: 'gr_qty' },
       ]
    });
  });

  function show_modal_table_geser()
  {
    $('#modal_table_geser').modal('show');
    table_geser.ajax.reload(null,false); 
  }; 
</script>
@stop