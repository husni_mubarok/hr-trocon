 
@extends('layouts.editor.template')
@section('content')
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- <div class="col-xs-2">
	</div> -->
	<div class="col-xs-8">
		<div class="box box-danger">
			@include('errors.error')
			@if(isset($inventory))
			{!! Form::model($inventory, array('route' => ['editor.inventory.update', $inventory->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_inventory'))!!}
			@else
			{!! Form::open(array('route' => 'editor.inventory.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_inventory'))!!}
			@endif
			{{ csrf_field() }}
			<div class="box-header with-border">
				<section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
					<h4>
						@if(isset($module))
						<i class="fa fa-pencil"></i> Edit
						@else
						<i class="fa fa-plus"></i> 
						@endif
						Inventory
					</h4>
				</section>
			</div>
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('inventorycode', 'Inventory Code') }}
							{{ Form::text('inventorycode', old('inventorycode'), array('class' => 'form-control', 'placeholder' => 'Inventory Code*', 'required' => 'true', 'id' => 'inventorycode')) }}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('inventoryname', 'Inventory Name') }}
							{{ Form::text('inventoryname', old('inventoryname'), array('class' => 'form-control', 'placeholder' => 'Inventory Name*', 'required' => 'true', 'id' => 'inventoryname')) }} 
						</div>
					</div> 
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('stockunit', 'Unit') }}
							{{ Form::select('stockunit', $unit_list, old('stockunit'), array('class' => 'form-control', 'placeholder' => 'Select Unit', 'required' => 'true', 'id' => 'stockunit')) }} 
						</div>
					</div> 
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('sex', 'Type') }}
							{{ Form::select('inventorytypeid', $type_list, old('inventorytypeid'), array('class' => 'form-control', 'placeholder' => 'Select Type', 'required' => 'true', 'id' => 'inventorytypeid')) }} 
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('sex', 'Group') }}
							{{ Form::select('inventorygroupid', $group_list, old('inventorygroupid'), array('class' => 'form-control', 'placeholder' => 'Select Group', 'required' => 'true', 'id' => 'inventorygroupid')) }}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('sex', 'Brand') }}
							{{ Form::select('inventorybrandid', $brand_list, old('inventorybrandid'), array('class' => 'form-control', 'placeholder' => 'Select Brand', 'required' => 'true', 'id' => 'inventorybrandid')) }}
						</div>
					</div>


					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('sex', 'Color') }}
							{{ Form::select('inventorycolorid', $color_list, old('inventorycolorid'), array('class' => 'form-control', 'placeholder' => 'Select Color', 'required' => 'true', 'id' => 'inventorycolorid')) }}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('sex', 'Size') }}
							{{ Form::select('inventorysizeid', $group_list, old('inventorysizeid'), array('class' => 'form-control', 'placeholder' => 'Select Size', 'required' => 'true', 'id' => 'inventorysizeid')) }}
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('image', 'Image') }}
							<span class="btn btn-default btn-file"><span>Choose image</span><input type="file" name="image" /></span><br/><br/>
						</div>
					</div>
				</div>
			</div>
			<div class="box-header with-border">
				<div class="col-md-12">
					<div class="form-group">
						<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
						<a href="{{ URL::route('editor.inventory.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
					</div>
				</div>
			</div>
		</section><!-- /.content -->
	</div>
</div>
</section>
@stop

