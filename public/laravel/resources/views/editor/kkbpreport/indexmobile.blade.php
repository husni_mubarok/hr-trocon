@extends('layouts.editor.templatemobile')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top: -18%; margin-bottom: -5%">
  <h4>
    <i class="fa fa-file"></i> KKBP Report
  </h4>
</section><br>

 
 <!-- Main content -->
<section class="content">
    <div class="row"> 
        <div class="col-sm-12">  
            <div class="box box-danger"> 
              {!! Form::model($capitalgood, array('route' => ['editor.branchalloc.update'], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_branchalloc'))!!}
                <div class="box-header with-border">
                    <div class="row">
                        <div class="box-body"> 
                          <div class="col-sm-2 col-xs-6" data-toggle="tooltip" data-placement="top" title="Date From">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                {{ Form::text("grfrom", old("grfrom", $datafilter->grfrom), array("class" => "form-control", "placeholder" => "GR From*", "required" => "true", "id" => "grfrom", "onchange" => "filter();")) }}
                            </div><!-- /.input group -->
                          </div>
                          <div class="col-sm-2 col-xs-6" data-toggle="tooltip" data-placement="top" title="Date To">
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                              {{ Form::text("grto", old("grto", $datafilter->grto), array("class" => "form-control", "placeholder" => "GR To*", "required" => "true", "id" => "grto", "onchange" => "filter();")) }}
                            </div><!-- /.input group -->
                          </div>
                          <div class="col-sm-2 col-xs-6" data-toggle="tooltip" data-placement="top" title="Category">
                            <select name="item_category_id" id="item_category_id" class="form-control select2" onchange="filter();">       
                              @if($datafilter->item_category_id == 0)
                                <option value="0">All Category</option> 
                                @else <option value="{{$datafilter->item_category_id}}">{{$datafilter->item_category_name}}</option> 
                              @endif <option value="0">All Category</option> 
                              @foreach($item_category as $item_categorys)
                                <option value="{{$item_categorys->id}}">{{$item_categorys->item_category_name}}</option>
                              @endforeach
                            </select></div>
                            <div class="col-sm-2 col-xs-6" data-toggle="tooltip" data-placement="top" title="Branch">
                              <select name="branch_id" id="branch_id" class="form-control select2" onchange="filter();">
                                @if($datafilter->branch_id == 0)
                                  <option value="0">All Branch</option> 
                                  @else 
                                  <option value="{{$datafilter->branch_id}}">{{$datafilter->branch_name}}</option> 
                                @endif 
                                  <option value="0">All Branch</option> 
                                @foreach($branch_cb as $branchs)
                                  <option value="{{$branchs->id}}">{{$branchs->branch_name}}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="col-sm-2 col-xs-6" data-toggle="tooltip" data-placement="top" title="Search">
                               <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                  {{ Form::text("search", old("search", $datafilter->search), array("class" => "form-control", "placeholder" => "Search...", "required" => "true", "id" => "search", "onchange" => "filter();")) }}
                              </div>
                            </div>
                            <div class="col-sm-2 col-xs-6" data-toggle="tooltip" data-placement="top" title="Search">
                                <button onClick="filter(); history.go(0);" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
                            </div>
                            <table id="dtTable" class="table table-bordered table-hover stripe"> 
                              <thead style="font-size: 12px">
                                <tr>
                                  <th rowspan="2" width="0.1%">No Con</th>
                                  <th rowspan="2" width="0.5%">Doc Date</th> 
                                  <th rowspan="2" width="0.5%">GR Date</th> 
                                  <th rowspan="2" width="0.1%">PO No</th>
                                  <th rowspan="2" width="0.1%">Plant</th>
                                  <th rowspan="2" width="0.1%">Store</th>
                                  <th rowspan="2" width="3%">Material</th>
                                  <th rowspan="2" width="0.1%">Order Qty</th>
                                  <th colspan="
                                  @php
                                    echo count($branch) + 1;
                                  @endphp
                                  "><center>PEMBAGIAN CABANG</center></th>
                                </tr>
                                <tr>
                                  @foreach($branch as $branchs)
                                  <th width="1%">{{ $branchs->branch_name }}</th>
                                  @endforeach
                                </tr>
                               </thead> 
                                <tbody> 
                                  @foreach($capitalgood_detail as $capitalgood_details)
                                  <tr style="background-color: #fff !important">
                                    <td>{{ $capitalgood_details->prefix }}{{ $capitalgood_details->container_no }}</td>
                                    <td>{{ $capitalgood_details->doc_date }}</td>
                                    <td>{{ $capitalgood_details->gr_date }}</td> 
                                    <td>{{ $capitalgood_details->po_number }}</td>
                                    <td>{{ $capitalgood_details->plant }}</td>
                                    <td>{{ $capitalgood_details->store_location_name }}</td>
                                    <td>{{ $capitalgood_details->item_name }}</td>

@php
                                      $total = ($capitalgood_details->rateidr) + (($capitalgood_details->cost_pib + $capitalgood_details->emkl_master + $capitalgood_details->plugin + $capitalgood_details->ls + $capitalgood_details->kalog) / $capitalgood_details->order_qty);
                                    @endphp

                                    <td>{{ number_format($capitalgood_details->order_qty,0) }}</td>
                                    @foreach($branch as $branchs)
                                    <td style="width: 10px !important">
                                        @php 
                                          $branch_name = $branchs->field_name;
                                          echo $capitalgood_details->$branch_name;
                                        @endphp
                                    </td>
                                    @endforeach
                                  </tr>
                                  @endforeach
                                </tbody>
                          </table> 
                          <!-- /.box-body -->
                         {{-- <a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-save"></i> Save</a>  --}}
                        <!-- <a href="" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>  -->
                          <div class="pull-right">{{ $capitalgood_detail->links() }}</div>  

                   </div>
                {!! Form::close() !!}     
          </div>
      </div> 
    </div>  
</section><!-- /.content -->
       

@stop 

@section('scripts')
  
<script type="text/javascript"> 
  $('#btn_submit').on('click', function()
  { 

    var grfrom = $("#grfrom").val();
    var grto = $("#grto").val();

    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to update <i>tarik</i> data from period: <b>' + grfrom + '</b> to period: <b>' + grto + '</b>?',
      type: 'green',
      typeAnimated: true,
      buttons: {
        cancel: {
          action: function () { 
          }
        },
        confirm: {
          text: 'CREATE',
          btnClass: 'btn-green',
          action: function () {  
            $('#form_branchalloc').submit(); 
          }
        },

      }
    });
  });

  $(document).ready(function() { 
        $("#dtTable").dataTable( {
            "sScrollX": true,
             "scrollY": "330px",
             "bPaginate": false,
             "searching": false,
             "dom": '<"toolbar">frtip', 
        });

        $("div.toolbar").html('');
    });

  function filter()
       { 
        console.log($('#branch_id').val());
        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.datefilterbranch') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'grfrom': $('#grfrom').val(),    
            'grto': $('#grto').val(),
            'item_category_id': $('#item_category_id').val(),   
            'branch_id': $('#branch_id').val(),   
          }, 
          success: function(data) { 
            // reload_table();
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Filter GR date has been change!', 'Success Alert', options);
          }
        }) 
      }; 
</script>
@stop

