@extends('layouts.editor.template')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-vcard"></i> Leaving Request
    <small>Leaving</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Leaving</a></li>
    <li class="active">Leaving Request</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row"> 
		<div class="col-xs-7 col-md-7">
			<div class="box box-danger">
				@include('errors.error')
				@if(isset($leaving))
				{!! Form::model($leaving, array('route' => ['editor.leaving.update', $leaving->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_leaving'))!!}
				@else
				{!! Form::open(array('route' => 'editor.leaving.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_leaving'))!!}
				@endif
				{{ csrf_field() }}
				<div class="box-header with-border">
					<section class="content-header">
						<h4>
							@if(isset($module))
							<i class="fa fa-pencil"></i> Edit
							@else
							<i class="fa fa-plus"></i> 
							@endif
							Leaving
						</h4>
					</section>
				</div>
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('notrans', 'No Trans') }}
								{{ Form::text('notrans', old('notrans'), array('class' => 'form-control', 'placeholder' => 'No Trans*', 'required' => 'true', 'id' => 'notrans', 'disabled' => 'disabled')) }}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('datetrans', 'Date Trans') }}
								<div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
			                        {{ Form::text('datetrans', old('datetrans'), array('class' => 'form-control', 'placeholder' => 'Date Trans*', 'required' => 'true')) }}
			                    </div><!-- /.input group --> 
							</div>
						</div>   
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('employeeid', 'Employee Name') }}
								{{ Form::select('employeeid', $employee_list, old('employeeid'), array('class' => 'form-control select2', 'placeholder' => 'Select Employee', 'id' => 'employeeid', 'onchange' => 'RefreshData();')) }} 
								  
							</div>
						</div>   
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('absencetypeid', 'Absence Type') }}
								{{ Form::select('absencetypeid', $absencetype_list, old('absencetypeid'), array('class' => 'form-control', 'placeholder' => 'Select Absence Type', 'id' => 'absencetypeid')) }} 
								  
							</div>
						</div>  

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('leavingfrom', 'Leaving From') }}
								<div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
			                        {{ Form::text('leavingfrom', old('leavingfrom'), array('class' => 'form-control', 'placeholder' => 'Leaving From*', 'required' => 'true', 'id' => 'leavingfrom')) }}
			                    </div><!-- /.input group --> 
							</div>
						</div>   

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('leavingto', 'Leaving To') }}
								<div class="input-group">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
			                        {{ Form::text('leavingto', old('leavingto'), array('class' => 'form-control', 'placeholder' => 'Leaving To*', 'required' => 'true', 'id' => 'leavingto')) }}
			                    </div><!-- /.input group --> 
							</div>
						</div>    
 
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('attachment', 'Attachment') }}<br>
								<span class="btn btn-default btn-file"><span>Choose file</span><input type="file" name="attachment" /></span>
								<br/>
							</div>
						</div>
					</div>
				</div>
				<div class="box-header with-border">
         		 	<div class="row">  
						<div class="col-md-12">
							<div class="form-group">
								@if(isset($leaving)) 
				                  @if($leaving->status == 9)
				                    <button type="submit" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save and Active</button>   
				                    <script type="text/javascript"> 
				                      $(document).ready(function(){
				                         hidebtnactive();
				                      });
				                    </script>
				                  @else
				                    <a href="#" onclick="cancel();" class="btn btn-danger btn-flat"><i class="fa fa-minus-square"></i> Cancel</a>  
				                  @endif
				                @endif
				                <button type="submit" id="btnsave" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save</button>
								<a href="{{ URL::route('editor.leaving.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
							</div>
						</div>
					</div>
				</div> 
			{!! Form::close() !!}
		</div>
	</div>

	<div class="col-xs-5 col-md-5">
	    <div class="box box-danger">
	      <div class="box-header with-border">
	          <section class="content-header">
	              <h4><i class="fa fa-sticky-note-o"></i> Leaving History</h4>
	          </section>
	      </div>
	      <div class="box-body">
	          <table id="dtTable" class="table table-bordered table-hover stripe">
	            <thead>
	              <tr> 
	                <th>No</th>
	                <th>Date</th>
	                <th>Leave From</th>
	                <th>Leave To</th>
	                <th>Absence Type</th>
	              </tr>
	            </thead>
	            <tbody>
	            </tbody>
	          </table>
	        </div>
	      </div>
	    </div>
	  </div>
</section>
@stop

@if(isset($leaving)) 
@section('scripts') 
<script type="text/javascript"> 
var table;
  $(document).ready(function() {
        RefreshData();
        //datatables
        table = $('#dtTable').DataTable({ 
         //dttables
         processing: true,
         serverSide: true,
         "pageLength": 25,  
         "scrollY": "100%", 
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],  
         ajax: "{{ url('editor/leaving/datahistory') }}",
         columns: [  
         { data: 'notrans', name: 'notrans', orderable: false, searchable: false }, 
         { data: 'datetrans', name: 'datetrans', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'leavingfrom', name: 'leavingfrom' }, 
         { data: 'leavingto', name: 'leavingto' },
         { data: 'absencetypename', name: 'absencetypename' },
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });

      });

  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax 
  }

  function RefreshData()
   { 

    $.ajax({
      type: 'POST',
      url: "{{ URL::route('editor.employeefilter') }}",
      data: {
        '_token': $('input[name=_token]').val(), 
        'employeeid': $('#employeeid').val()   
      }, 
      success: function(data) { 
        reload_table();
      }
    }) 
  };  

  function cancel()
  {   
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to cancel this data?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        cancel: {
         action: function () { 
         }
       },
       confirm: {
        text: 'CANCEL',
        btnClass: 'btn-red',
        action: function () {
         $.ajax({
          url : '../../leaving/cancel/' + {{$leaving->id}},
          type: "PUT", 
          data: {
            '_token': $('input[name=_token]').val() 
          }, 
          success: function(data) {  
            //var loc = 'ap_invoice';
            if ((data.errors)) { 
              alert("Cancel error!");
            } else{
              window.location.href = "{{ URL::route('editor.leaving.index') }}";
            }
          }, 
        }); 
       }
     },
    }
  });
  }  
  function hidebtnactive() {
      $('#btnsave').hide(100); 
  }
</script>
@stop  
@endif