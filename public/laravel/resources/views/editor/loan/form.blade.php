 @extends('layouts.editor.template')
 @section('title', 'Pinjaman')
 @section('content')
 <style type="text/css">


  #detailModals .modal-dialog
  {
    width: 60%;
  }
  th { font-size: 13px; }
  td { font-size: 12px; }
</style>
<!-- Content Wrapper. Contains page content --> 
<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    <i class="fa fa-adjust"></i> Pinjaman Karyawan
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Time & Payroll</a></li>
    <li class="active">Pinjaman Karyawan</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        {!! Form::model($loan, array('route' => ['editor.loan.update', $loan->id], 'method' => 'PUT', 'class'=>'form-horizontal', 'files' => 'true', 'id'=>'form_employee'))!!}
        {{ csrf_field() }}
        <!--  Hidden element -->
        <input type="hidden" value="" id="idtrans" name="idtrans">
        <input type="hidden" value="" id="status" name="status">
        <div class="box-header with-border">
          <div class="row">
            <!-- Coloumn 1-->
            <div class="col-md-4">
              <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Trans No</label>
                <div class="col-sm-9">  
                  {{ Form::text('notrans', old('notrans'), array('class' => 'form-control', 'placeholder' => 'No Trans*', 'required' => 'true', 'id' => 'notrans', 'disabled' => 'disabled')) }}
                </div>
              </div>
              <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Trans Date</label>
                <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('datetrans', old('datetrans'), array('class' => 'form-control', 'placeholder' => 'Date Trans*', 'required' => 'true', 'id' => 'datetrans', 'onchange' => 'saveheader();')) }}
                </div><!-- /.input group --> 
              </div>
            </div> 
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Jenis Pinjaman</label>
              <div class="col-sm-9"> 
                {{ Form::select('loantypeid', $loantype_list, old('loantypeid'), array('class' => 'form-control select2', 'placeholder' => 'Pilih Jenis Pinjaman', 'id' => 'loantypeid', 'onchange' => 'saveheader();', 'onchange' => 'RefreshData();')) }} 
              </div>
            </div>
          </div>
          <!-- Coloumn 2-->   
          <div class="col-md-4">
           <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Karyawan</label>
              <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>  
                    {{ Form::select('employeeid', $employee_list, old('employeeid'), array('class' => 'form-control select2', 'placeholder' => 'Pilih Karyawan', 'onchange' => 'saveheader();', 'id' => 'employeeid', 'onchange' => 'RefreshData();')) }}
                </div><!-- /.input group --> 
              </div>
          </div>  
           <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Diajukan</label>
              <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon">Rp.</span>  
                  {{ Form::text('requestamount', old('requestamount'), array('class' => 'form-control', 'placeholder' => 'Nominal Diajukan*', 'required' => 'true', 'id' => 'requestamount', 'onkeyup' => 'cal_sparator_requestamount();')) }}
                </div><!-- /.input group --> 
              </div>
          </div>
         <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Disetujui</label>
              <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon">Rp.</span>  
                  {{ Form::text('approvedamount', old('approvedamount'), array('class' => 'form-control', 'placeholder' => 'Nominal Disetujui*', 'required' => 'true', 'id' => 'approvedamount', 'onkeyup' => 'cal_sparator_approvedamount();')) }}
                </div><!-- /.input group --> 
              </div>
          </div>
          
        </div>
        <!-- Coloumn 3-->                                   
        <div class="col-md-4">  
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Periode Pinjaman</label>
              <div class="col-sm-9">
                {{ Form::text('period', old('period'), array('class' => 'form-control', 'placeholder' => 'Periode Pinjaman*', 'required' => 'true', 'id' => 'period', 'onchange' => 'saveheader();', 'onkeyup' => 'cal_sparator_period();')) }}
              </div>
            </div>
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Tanggal Potongan</label>
              <div class="col-sm-9">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('loanapproved', old('loanapproved'), array('class' => 'form-control', 'placeholder' => 'Date Trans*', 'required' => 'true', 'id' => 'loanapproved', 'onchange' => 'saveheader();')) }}
                </div><!-- /.input group --> 
              </div>
            </div>
      </div>
    </div><!-- /.box-header -->
{!! Form::close() !!}
    <hr>
               
    <div class="box-body">
      <div class="col-md-2" style="margin-right: 5px">
        <div class="form-group">
          <label>Tahun</label> 
            {{ Form::select('year', $year_list, old('yearid'), array('class' => 'form-control select1', 'placeholder' => 'Pilih Tahun', 'id' => 'year')) }} 
        </div> 
      </div> 

      <div class="col-md-2" style="margin-right: 5px">
        <div class="form-group">
          <label>Bulan</label>
          {{ Form::select('month', $month_list, old('monthid'), array('class' => 'form-control select1', 'placeholder' => 'Pilih Bulan', 'id' => 'month', 'onchange' => 'RefreshData();')) }}
        </div> 
      </div>

      <div class="col-md-1" style="margin-right: 5px">
        <div class="form-group">
          <label>Jumlah</label>
          {{ Form::text('amount', old('amount'), array('class' => 'form-control', 'placeholder' => 'Dibayar*', 'id' => 'amount', 'onclick' => 'saveheader();')) }}
        </div> 
      </div>

      <div class="col-md-1" style="margin-right: 5px">
        <div class="form-group">
          <label>Bonus</label>
          {{ Form::text('bonus', old('bonus'), array('class' => 'form-control', 'placeholder' => 'Bonus*', 'id' => 'bonus', 'onclick' => 'saveheader();')) }}
          <input type="hidden" name="idtransdet" id="idtransdet">
        </div> 
      </div>

     <!--  <div class="col-md-1" style="margin-right: 5px">
        <div class="form-group">
          <label>Dibayar</label>
          {{ Form::text('paid', old('paid'), array('class' => 'form-control', 'placeholder' => 'Dibayar*', 'id' => 'paid', 'onclick' => 'saveheader();')) }}
        </div> 
      </div>


      <div class="col-md-1" style="margin-right: 5px">
        <div class="form-group">
          <label>Sisa</label>
          {{ Form::text('remain', old('remain'), array('class' => 'form-control', 'placeholder' => 'Sisa*', 'id' => 'remain', 'onclick' => 'saveheader();')) }} 
        </div> 
      </div> -->
  
      <div class="col-md-2" style="margin-right: 5px">
        <div class="form-group">
         <label>Action</label><br/>
         <a href="#" onclick="update_detail();" id="btn_update_detail" type="button" class="btn btn-primary btn-flat" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-pencil"></i> Update</a>
         <a href="#" onclick="savedetail();" type="button" id="btn_add_detail" class="btn btn-primary btn-flat" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-plus"></i> Add</a>
       </div>   
     </div> 
   </div>  
   <div class="box-body">  
     <table id="dtTable" class="table table-bordered table-hover stripe">
      <thead>
       <tr>  
        <th>#</th>
        <th>Tahun</th> 
        <th>Bulan</th>  
        <th>Jumlah</th>
        <th>Bonus</th>
        <!-- <th>Dibayar</th> -->
        <!-- <th>Sisa</th> -->
        <th>Action</th> 
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table> 
</div>  
<hr style="margin-top: -10px">   
<div class="box-header with-border">
  <div class="col-md-6">
    <div class="form-group">
      <label for="real_name" class="col-sm-3 control-label">Remark</label>
      <div class="col-sm-9">
        <textarea style="height: 40px !important" class="form-control" id="remark" name="remark" onchange="saveheader();"> {{$loan->remark}}</textarea>
      </div>
    </div>
  </div>

  <!-- <button type="submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Save</button>   -->
  <a href="{{ URL::route('editor.loan.index') }}" onclick="saveheader();" type="button" class="btn btn-primary btn-flat pull-right" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-close"></i> Save</a> 
  <!-- <a href="#" class="btn btn-primary btn-flat pull-right" name="saveclose" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow'); saveheader();" data-toggle="modal" data-easein="swoopIn" data-target=".MyModals"><i class="fa fa-print"></i> Print</a> -->

  <a href="#" class="btn btn-danger btn-flat pull-right" name="saveclose" style="margin-left: 2px; margin-right: 2px" onclick="GenerateData();"><i class="fa fa-magic"></i> Generate</a>
  
  <!-- <a class="btn btn-warning btn-flat pull-right" name="cancel" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow'); canceltransaction();" ><i class="fa fa-minus-square"></i> Cancel</a> -->
  
  <iframe src='' height="0" width="0" frameborder='0' name="print_frame"></iframe>

</div>

</div>
</section><!-- /.content -->  

<!-- Modal Popup -->
<div class="modal fade MyModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog" style="width:400px !important;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Print Out</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="form-control" id="idperiod" name="idperiod">
        <input type="hidden" class="form-control" id="type" name="type">
        <select multiple class="form-control" id="slipid" class="slipid" value="{{$loan->id}}">
          <option ondblclick="showslip()" value="loan">SPK</option>
        </select>
      </div>
      <div class="modal-footer">
        <a href="#" onclick="showslip();" class="btn btn-primary btn-flat"> <i class="fa fa-search"></i> Preview</a> 
        <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Popup -->
<div class="modal fade detailModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" id="detailModals" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
       <h3> <i class="fa fa-user"></i> Employee Master</h3>
     </div>
     <div class="modal-body">
      <table id="detailTable" class="table table-bordered table-hover stripe">
        <thead>
         <tr>  
           <th>#</th>
           <th>NIK</th>
           <th>Employee Name</th>    
           <th>Position</th> 
           <th>Action</th>   
         </tr>
       </thead>
       <tbody>
       </tbody>
     </table> 
   </div>
   <div class="modal-footer">  
    <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
  </div>
</div>
</div>
</div>
@stop

@section('scripts')

<script type="text/javascript">

  var table;
  $(document).ready(function() {
        $("#btn_update_detail").hide();
        //datatables
        table = $('#dtTable').DataTable({ 
          processing: true,
          serverSide: true,
          "pageLength": 25,
          "scrollY": "120px",
          "rowReorder": true,
          "order": [[ 0, "asc" ]],
          "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
          ajax: "{{ url('editor/loan/datadetail') }}/{{$loan->id}}",
          
          columns: [   
          
          { data: 'id', name: 'id' },
          { data: 'year', name: 'year' },
          { data: 'monthname', name: 'monthname' }, 
          { data: 'amount', name: 'amount' }, 
          { data: 'bonus', name: 'bonus' }, 
          // { data: 'paid', name: 'paid' }, 
          // { data: 'remain', name: 'remain' }, 
          { data: 'action', name: 'action', orderable: false, searchable: false }
          ]
        });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        }); 
      });
      function reload_table()
      {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function add()
      {
       $("#btnSave").attr("onclick","save()");
       $("#btnSaveAdd").attr("onclick","saveadd()");

       $('.errorMaterial UsedName').addClass('hidden');

       save_method = 'add'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Asset Request'); // Set Title to Bootstrap modal title
      } 

      function reload_table_detail()
      {
        table_detail.ajax.reload(null,false); //reload datatable ajax 
      }

      function addValue(str, id){
        var empid = id;
        var employeeid = $(str).closest('tr').find('td:eq(0)').text();
        var nik = $(str).closest('tr').find('td:eq(1)').text(); 
        var employeename = $(str).closest('tr').find('td:eq(2)').text(); 

        $("#employeeid").val(employeeid);
        $("#nik").val(nik);
        $("#employeename").val(employeename); 

        console.log(id);
        $('#detailModals').modal('toggle');
      }


      function saveheader(id)
      {
        save_method = 'update';  

        //Ajax Load data from ajax
        $.ajax({
          url: '../../loan/saveheader/{{$loan->id}}' ,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'loantypeid': $('#loantypeid').val(),
            'employeeid': $('#employeeid').val(),
            'requestamount': $('#requestamount').val(),
            'approvedamount': $('#approvedamount').val(),
            'loanapproved': $('#loanapproved').val(),
            'period': $('#period').val(),
            'paid': $('#paid').val(),
            'bonus': $('#bonus').val(),
            'amount': $('#amount').val(),
            'remark': $('#remark').val()
          },
          success: function(data) {  
            if ((data.errors)) { 
              toastr.error('Data is required!', 'Error Validation', options);
            } 
          },
        })
      };

      function savedetail(id)
      {
        var employeeid = $("#employeeid").val();
        save_method = 'update';  

        if(employeeid == '')
        {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.error('Employee data is required!', 'Error Validation', options);
        }else{
            //Ajax Load data from ajax
            $.ajax({
              url: '../../loan/savedetail/{{$loan->id}}' ,
              type: "PUT",
              data: {
                '_token': $('input[name=_token]').val(), 
                'year': $('#year').val(),
                'month': $('#month').val(),
                'amount': $('#amount').val(),
                'bonus': $('#bonus').val(),
                'paid': $('#paid').val(),
                'remain': $('#remain').val(),
              },
              success: function(data) {

              
               var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully add detail data!', 'Success Alert', options);

              if ((data.errors)) { 
                toastr.error('Data is required!', 'Error Validation', options);
              } 
              reload_table();

              $('#year').val(''),
              $('#month').val(''),
              $('#paid').val(''),
              $('#bonus').val(''),
              $('#amount').val(''),
              $('#remain').val('')

            },
          
          })
        }
      };

      function delete_id(id, employeename)
      {
        //alert("asdasd");
        //var varnamre= $('#employeename').val();
        var employeename = employeename.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : '../../loan/deletedet/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error deleteing data!',
                });
              }
            });
           }
         },

       }
     });
      }

     function edit(id)
     { 
        $.ajax({
          url : '../../loan/editdetail/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
            // console.log(data.paid);
            $("#btn_update_detail").show();
            $("#btn_add_detail").hide();

            $('[name="amount"]').val(data.amount);
            $('[name="bonus"]').val(data.bonus);
            $('[name="paid"]').val(data.paid);
            $('[name="remain"]').val(data.remain);
            $('[name="month"]').val(data.month);
            $('[name="year"]').val(data.year); 
            $('[name="idtransdet"]').val(data.id); 
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
      }


      function update_detail(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: '../../loan/editdetail/' +  $('#idtransdet').val(),
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'paid': $('#paid').val(), 
            'amount': $('#amount').val(), 
            'bonus': $('#bonus').val(), 
            'remain': $('#remain').val(), 
            'month': $('#month').val(), 
            'year': $('#year').val(),  
          },
          success: function(data) {  
            reload_table(); 
            $("#btn_update_detail").hide();
            $("#btn_add_detail").show();

            $('#paid').val(''); 
            $('#amount').val(''); 
            $('#bonus').val(''); 
            $('#remain').val(''); 
            $('#month').val(''); 
            $('#year').val('');  
          } 
      })
        reload_table(); 
        $("#btn_update_detail").hide();
        $("#btn_add_detail").show();

        $('#paid').val(''); 
        $('#amount').val(''); 
        $('#bonus').val(''); 
        $('#remain').val(''); 
        $('#month').val(''); 
        $('#year').val('');  
      };
    </script>
    <script type="text/javascript">
      function GenerateData()
      {

        // var periodid = $('#periodidfilter').val();
        // var perioddesc = $("#periodidfilter option:selected").text();
        saveheader();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to generate data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'CREATE',
            btnClass: 'btn-red',
            action: function () { 
             $.ajax({
              url : '../../loan/generate/' + {{ $loan->id }},
              type: "POST",
              data: {
                '_token': $('input[name=_token]').val(), 
                'loantypeid': $('#loantypeid').val(),
                'employeeid': $('#employeeid').val(),
                'requestamount': $('#requestamount').val(),
                'approvedamount': $('#approvedamount').val(),
                'loanapproved': $('#loanapproved').val(),
                'period': $('#period').val(),
                'paid': $('#paid').val(),
                'remark': $('#remark').val()
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully genareted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error generate data!',
                });
              }
            });
           }
         },

       }
     });
      }
    </script>

    <script type="text/javascript">
      function showslip()
      {
         var slipid = $("#slipid").value;
         var url = "{{ URL::route('editor.loan.slip', $loan->id) }}";
         PopupCenter(url,'Popup_Window','700','650');
      }

      function PopupCenter(url, title, w, h) {  
        // Fixes dual-screen position                         Most browsers      Firefox  
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
        var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
        var top = ((height / 2) - (h / 2)) + dualScreenTop;  
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
        
        // Puts focus on the newWindow  
        if (window.focus) {  
          newWindow.focus();  
        }  
      } 

      
 
  function cal_sparator_requestamount() {  
    var requestamount = document.getElementById('requestamount').value;
    var result = document.getElementById('requestamount');
    var rsamount = (requestamount);
    result.value = rsamount.replace(/,/g, "");  


    n2= document.getElementById('requestamount');

    n2.onkeyup=n2.onchange= function(e){
      e=e|| window.event; 
      var who=e.target || e.srcElement,temp;
      if(who.id==='requestamount')  temp= validDigits(who.value,0); 
      else temp= validDigits(who.value);
      who.value= addCommas(temp);
    }   
    n2.onblur= function(){
      var 
      temp2=parseFloat(validDigits(n2.value));
      if(temp2)n2.value=addCommas(temp2.toFixed(0));
    } 
  }


  function cal_sparator_approvedamount() {  

    var approvedamount = document.getElementById('approvedamount').value;
    var result = document.getElementById('approvedamount');
    var rsamount = (approvedamount);
    result.value = rsamount.replace(/,/g, "");  


    n2= document.getElementById('approvedamount');

    n2.onkeyup=n2.onchange= function(e){
      e=e|| window.event; 
      var who=e.target || e.srcElement,temp;
      if(who.id==='approvedamount')  temp= validDigits(who.value,0); 
      else temp= validDigits(who.value);
      who.value= addCommas(temp);
    }   
    n2.onblur= function(){
      var 
      temp2=parseFloat(validDigits(n2.value));
      if(temp2)n2.value=addCommas(temp2.toFixed(0));
    } 
  }

</script>
@stop
