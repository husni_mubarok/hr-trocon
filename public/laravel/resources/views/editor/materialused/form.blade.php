 @extends('layouts.editor.template')
 @section('content')
 <style type="text/css">


  #detailModals .modal-dialog
  {
    width: 60%;
  }
  th { font-size: 13px; }
  td { font-size: 12px; }
</style>
<!-- Content Wrapper. Contains page content --> 
<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    Material Request
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Transaction</a></li>
    <li class="active">Material Request</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        {!! Form::model($materialused, array('route' => ['editor.materialused.update', $materialused->id], 'method' => 'PUT', 'class'=>'form-horizontal', 'files' => 'true', 'id'=>'form_employee'))!!}
        {{ csrf_field() }}
        <!--  Hidden element -->
        <input type="hidden" value="" id="idtrans" name="idtrans">
        <input type="hidden" value="" id="status" name="status">
        <div class="box-header with-border">
          <div class="row">
            <!-- Coloumn 1-->
            <div class="col-md-4">
              <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Trans No</label>
                <div class="col-sm-9">  
                  {{ Form::text('notrans', old('notrans'), array('class' => 'form-control', 'placeholder' => 'No Trans*', 'required' => 'true', 'id' => 'notrans', 'disabled' => 'disabled')) }}
                </div>
              </div>
              <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Trans Date</label>
                <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('datetrans', old('datetrans'), array('class' => 'form-control', 'placeholder' => 'Date Trans*', 'required' => 'true', 'id' => 'datetrans', 'onclick' => 'saveheader();')) }}
                </div><!-- /.input group --> 
              </div>
            </div> 
          </div>
          <!-- Coloumn 2-->   
          <div class="col-md-4"> 
           <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Used Type</label>
            <div class="col-sm-9"> 
              {{ Form::select('matusedtypeid', $used_list, old('matusedtypeid'), array('class' => 'form-control', 'placeholder' => 'Select Used Type', 'required' => 'true', 'id' => 'matusedtypeid', 'onclick' => 'saveheader();')) }} 
            </div>
          </div>
          <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Requested by</label>
            <div class="col-sm-9">

              {{ Form::select('employeeid', $employee_list, old('employeeid'), array('class' => 'form-control', 'placeholder' => 'Select Employee', 'required' => 'true', 'id' => 'employeeid', 'onclick' => 'saveheader();')) }} 
            </div>
          </div> 
        </div>
        <!-- Coloumn 3-->                                   
        <div class="col-md-4">  
          <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Building</label>
            <div class="col-sm-9">
              {{ Form::select('buildingid', $building_list, old('buildingid'), array('class' => 'form-control', 'placeholder' => 'Select Building', 'required' => 'true', 'id' => 'buildingid', 'onclick' => 'saveheader();')) }}
            </div>
          </div> 
        </div>
      </div>
    </div><!-- /.box-header -->

    <div class="box-body">
      <div class="col-md-2" style="margin-right: 5px">
        <div class="form-group">
          <label>Asset Code</label>
          <div class="input-group">
            <input type="text" class="form-control pull-right" id="inventorycode" name="inventorycode" data-toggle="tooltip" data-placement="inventorycode" value="">
            <span class="input-group-addon"><a href="#"  onclick="reload_table_detail();" data-toggle="modal" data-easein="swoopIn" data-target=".detailModals"><i class="fa fa-folder"></i></a></span>
          </div><!-- /.input group -->
        </div> 
      </div> 

      <div class="col-md-3" style="margin-right: 5px">
        <div class="form-group">
          <label>Asset Name</label>
          {{ Form::hidden('inventoryid', old('inventoryid'), array('id' => 'inventoryid')) }}
          <input type="text" value="" class="form-control" id="inventoryname" name="inventoryname">
        </div> 
      </div>
      <div class="col-md-1" style="margin-right: 5px">
        <div class="form-group">
          <label>Unit</label>
          {{ Form::select('unit', $unit_list, old('unit'), array('class' => 'form-control', 'placeholder' => 'Select Building', 'required' => 'true', 'id' => 'unit')) }} 
        </div> 
      </div>

      <div class="col-md-2" style="margin-right: 5px">
        <div class="form-group">
          <label>Floor</label> 
          {{ Form::select('floorid', $floor_list, old('floorid'), array('class' => 'form-control', 'placeholder' => 'Select Floor', 'required' => 'true', 'id' => 'floorid')) }} 
        </div> 
      </div>

      <div class="col-md-1" style="margin-right: 5px">
        <div class="form-group">
          <label>Room</label>
          {{ Form::select('roomid', $room_list, old('roomid'), array('class' => 'form-control', 'placeholder' => 'Select Room', 'required' => 'true', 'id' => 'roomid')) }} 
        </div> 
      </div>

      <div class="col-md-1" style="margin-right: 5px">
        <div class="form-group">
          <label>Quantity</label>
          <input type="number" value="" class="form-control" id="quantity" name="quantity">
        </div> 
      </div>

      <div class="col-md-1" style="margin-right: 5px">
        <div class="form-group">
         <label>Action</label><br/>
         <a href="#" onclick="savedetail();" type="button" class="btn btn-success btn-flat" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-plus"></i> Add</a>
       </div>   
     </div> 
   </div>  
   <div class="box-body">  
     <table id="dtTable" class="table table-bordered table-hover stripe">
      <thead>
       <tr>  
        <th>Asset Code</th> 
        <th>Asset Name</th> 
        <th>Unit</th>
        <th>Quantity</th>
        <th>Aksi</th> 
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table> 
</div>  
<hr style="margin-top: -10px">   
<div class="box-header with-border">
  <div class="col-md-6">
    <div class="form-group">
      <label for="real_name" class="col-sm-3 control-label">Remark</label>
      <div class="col-sm-9">
        <textarea style="height: 40px !important" class="form-control" id="remark" name="remark" onchange="saveheader();"></textarea>
      </div>
    </div>
  </div>

  <a href="#" class="btn btn-primary btn-flat pull-right" name="saveclose" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow'); saveprint();" data-toggle="modal" data-easein="swoopIn" data-target=".MyModals"><i class="fa fa-print"></i> Print</a>
  <a class="btn btn-warning btn-flat pull-right" name="cancel" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow'); canceltransaction();" ><i class="fa fa-minus-square"></i> Cancel</a>
  <a href="{{ URL::route('editor.materialused.index') }}" type="button" class="btn btn-default btn-flat pull-right" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-close"></i> Close</a>
  <iframe src='' height="0" width="0" frameborder='0' name="print_frame"></iframe>

</div>
{!! Form::close() !!}
</div>
</section><!-- /.content -->  

<!-- Modal Popup -->
<div class="modal fade MyModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog" style="width:400px !important;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Print Out</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="form-control" id="idperiod" name="idperiod">
        <input type="hidden" class="form-control" id="type" name="type">
        <select multiple class="form-control" id="slipid" class="slipid">
          <option ondblclick="javascript:framePrint('print_frame');" value="materialused">Material Used</option>
        </select>
      </div>
      <div class="modal-footer">
        <a href="javascript:framePrint('print_frame');" class="btn btn-primary btn-flat"> <i class="fa fa-search"></i> Preview</a> 
        <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Tutup</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Popup -->
<div class="modal fade detailModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" id="detailModals" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
       <h3> Asset Master</h3>
     </div>
     <div class="modal-body">
      <table id="detailTable" class="table table-bordered table-hover stripe">
        <thead>
         <tr>  
           <th>#</th>
           <th>Inventory Code</th>
           <th>Inventory Name</th>    
           <th>Group</th>
           <th>Stock Unit</th>
           <th>Type</th>
           <th>Brand</th>
           <th>Color</th>
           <th>Size</th>
           <th>Aksi</th>   
         </tr>
       </thead>
       <tbody>
       </tbody>
     </table> 
   </div>
   <div class="modal-footer">  
    <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Tutup</button>
  </div>
</div>
</div>
</div>
@stop

@section('scripts')

<script type="text/javascript">

  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
          processing: true,
          serverSide: true,
          "pageLength": 25,
          "scrollY": "170px",
          "rowReorder": true,
          "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
          ajax: "{{ url('editor/materialused/datadetail') }}/{{$materialused->id}}",
          
          columns: [   
          
          { data: 'inventorycode', name: 'inventorycode' },
          { data: 'inventoryname', name: 'inventoryname' },
          { data: 'unit', name: 'unit' },
          { data: 'quantity', name: 'quantity' },
          { data: 'action', name: 'action', orderable: false, searchable: false }
          ]
        });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        }); 
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function add()
      {
       $("#btnSave").attr("onclick","save()");
       $("#btnSaveAdd").attr("onclick","saveadd()");

       $('.errorMaterial UsedName').addClass('hidden');

       save_method = 'add'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Tambah Asset Request'); // Set Title to Bootstrap modal title
      } 

      function reload_table_detail()
      {
        table_detail.ajax.reload(null,false); //reload datatable ajax 
      }

      var table_detail;
      $(document).ready(function() {
        //datatables
        table_detail = $('#detailTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/inventory/datalookup') }}",
         columns: [  
         { data: 'id', name: 'id', orderable: false, searchable: false },
         { data: 'inventorycode', name: 'inventorycode' },
         { data: 'inventoryname', name: 'inventoryname' }, 
         { data: 'inventorygroupname', name: 'inventorygroupname' },
         { data: 'stockunit', name: 'stockunit' },
         { data: 'inventorytypename', name: 'inventorytypename' },
         { data: 'inventorybrandname', name: 'inventorybrandname' },
         { data: 'inventorycolorname', name: 'inventorycolorname' },
         { data: 'inventorysizename', name: 'inventorysizename' },
         { data: 'action', name: 'action', orderable: false, searchable: false }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });

      function addValue(str, id){
        var invid = id;
        var inventoryid = $(str).closest('tr').find('td:eq(0)').text();
        var inventorycode = $(str).closest('tr').find('td:eq(1)').text(); 
        var inventoryname = $(str).closest('tr').find('td:eq(2)').text(); 

        $("#inventoryid").val(inventoryid);
        $("#inventorycode").val(inventorycode);
        $("#inventoryname").val(inventoryname); 

        console.log(id);
        $('#detailModals').modal('toggle');
      }


      function saveheader(id)
      {
        save_method = 'update';  

        //Ajax Load data from ajax
        $.ajax({
          url: '../../materialused/saveheader/{{$materialused->id}}' ,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'matusedtypeid': $('#matusedtypeid').val(),
            'buildingid': $('#buildingid').val(),
            'employeeid': $('#employeeid').val(),
            'remark': $('#remark').val()
          },
          success: function(data) {  
            if ((data.errors)) { 
              toastr.error('Data wajib diisi!', 'Validasi Error', options);
            } 
          },
        })
      };

      function savedetail(id)
      {
        save_method = 'update';  

        //Ajax Load data from ajax
        $.ajax({
          url: '../../materialused/savedetail/{{$materialused->id}}' ,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'inventoryid': $('#inventoryid').val(),
            'unit': $('#unit').val(),
            'floorid': $('#floorid').val(),
            'roomid': $('#roomid').val(),
            'quantity': $('#quantity').val()
          },
          success: function(data) {

           var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully add detail data!', 'Success Alert', options);

          if ((data.errors)) { 
            toastr.error('Data wajib diisi!', 'Validasi Error', options);
          } 
          reload_table_detail();
          reload_table();

          $('#inventoryid').val(''),
          $('#inventoryname').val(''),
          $('#inventorycode').val(''),
          $('#unit').val(''),
          $('#floorid').val(''),
          $('#roomid').val(''),
          $('#quantity').val('')

        },
      })
      };

      function delete_id(id, inventoryname)
      {
        //alert("asdasd");
        //var varnamre= $('#inventoryname').val();
        var inventoryname = inventoryname.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : '../../materialused/deletedet/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error deleteing data!',
                      });
              }
            });
           }
         },

       }
     });
      }
    </script>
    @stop
