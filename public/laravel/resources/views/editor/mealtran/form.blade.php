 @extends('layouts.editor.template')
 @section('title', 'Uang Makan')
 @section('content')

 <style type="text/css">
 	.toolbar {
	    float: left;
	}
	.input-smform {
		height: 22px;
		padding: 1px 3px;
		font-size: 12px;
		line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
		border-radius: 0px;
		width: 95% !important;
	}

	th,td  {
	    overflow: hidden;
	    white-space: nowrap;
	    text-overflow: ellipsis;
	}

	.dataTables_scroll
	{
	    overflow:auto;
	}
 </style>

 <!-- Content Header (Page header) -->
 <section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
 	<h4>
 		Uang Makan & Transport
 	</h4>
 	<ol class="breadcrumb">
 		<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
 		<li><a href="#">Master Data</a></li>
 		<li class="active">Uang Makan & Transport</li>
 	</ol>
 </section>

 <section class="content">
 	<section class="content box box-danger">
 		<div class="row">
			<div class="col-md-12">   
			{!! Form::model($mealtran, array('route' => ['editor.mealtran.updatedetail', $mealtran->id, $mealtran->departmentid], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_mealtran'))!!}
			<div id="consummable" class="tab-pane fade in active"> 
				<div class="box-body">
					<table class="table table-bordered table-hover stripe" id="mealtranTable">
						<thead>
							<tr>
								<th rowspan="2">Department</th>
								<th rowspan="2">NIK</th>
								<th rowspan="2">Karyawan</th>
								<th rowspan="2">Lokasi</th>
								<th rowspan="2" style="width: 30px !important">Ceklis</th>
								<th colspan="7"><center>Uang Makan & Transport</center></th>
								<th colspan="7"><center>(Insentif + TML + TLK)</center></th>  
								<th rowspan="2">Total</th>
								<th rowspan="2">Pot Absence</th>
								<th rowspan="2">Total Pajak</th>
								<th rowspan="2">Grand Total</th>
							</tr>
							<tr>
								<!-- uang makan -->
								<th>Minggu 1</th> 
								<th>Minggu 2</th>
								<th>Minggu 3</th>
								<th>Minggu 4</th>
								<th>Minggu 5</th>
								<th>Koreksi UM</th>
								<th>Total UM</th>

								<!-- insentif -->
								<th>Minggu 1</th> 
								<th>Minggu 2</th>
								<th>Minggu 3</th>
								<th>Minggu 4</th>
								<th>Minggu 5</th>
								<th>Koreksi Insentif</th>
								<th>Total</th> 

							</tr>
						</thead>
						<tbody> 
							@foreach($mealtran_detail as $key => $mealtran_details)
							<tr style="background-color: @if($mealtran_details->checklist == 1) #defee7 @else #fff @endif !important"> 
								<td class="col-sm-1 col-md-1">
									{{$mealtran_details->departmentname}} 
								</td>
								<td class="col-sm-1 col-md-1">
									{{$mealtran_details->nik}} 
								</td>
								<td class="col-sm-1 col-md-1">
									{{$mealtran_details->employeename}} 
								</td>
								<td class="col-sm-1 col-md-1">
									{{$mealtran_details->project}} 
								</td>
								<td class="col-sm-1" style="width: 30px !important">
									<input type="checkbox" id="checklist" name="detail[{{$mealtran_details->id}}][checklist]" @if($mealtran_details->checklist == 1) checked @endif>
								</td>
								<td class="col-sm-1 col-md-1">
									{{ Form::text('detail['.$mealtran_details->id.'][um1]', old($mealtran_details->um1.'[um1]', $mealtran_details->um1), ['id' => 'um1'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'UM 1*', 'oninput' => 'cal_sparator_um1('.$mealtran_details->id.');']) }} 
								</td>  
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][um2]', old($mealtran_details->um2.'[um2]', $mealtran_details->um2), ['id' => 'um2'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'UM 2*', 'oninput' => 'cal_sparator_um2('.$mealtran_details->id.');']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][um3]', old($mealtran_details->um3.'[um3]', $mealtran_details->um3), ['id' => 'um3'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'UM 3*', 'oninput' => 'cal_sparator_um3('.$mealtran_details->id.');']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][um4]', old($mealtran_details->id.'[um4]', $mealtran_details->um4), ['class' => 'form-control input-smform', 'placeholder' => 'UM 4*', 'id' => 'um4'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_um4('.$mealtran_details->id.');']) }} 
								</td> 
								<td class="col-sm-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][um5]', old($mealtran_details->id.'[um5]', $mealtran_details->um5), ['class' => 'form-control input-smform', 'placeholder' => 'UM 5*', 'id' => 'um5'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_um5('.$mealtran_details->id.');']) }} 
								</td>  
								<td class="col-sm-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][addmeal]', old($mealtran_details->id.'[addmeal]', $mealtran_details->addmeal), ['class' => 'form-control input-smform', 'placeholder' => 'Koreksi*', 'id' => 'addmeal'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_addmeal_x('.$mealtran_details->id.');']) }}
								</td> 
								<td class="col-sm-1">  
									{{ $mealtran_details->tum }}
								</td> 

								<td class="col-sm-1 col-md-1">
									{{ Form::text('detail['.$mealtran_details->id.'][insentif1]', old($mealtran_details->insentif1.'[insentif1]', $mealtran_details->insentif1), ['id' => 'insentif1'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '1*', 'oninput' => 'cal_sparator_insentif1('.$mealtran_details->id.')']) }} 
								</td>  
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][insentif2]', old($mealtran_details->insentif2.'[insentif2]', $mealtran_details->insentif2), ['id' => 'insentif2'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '2*', 'oninput' => 'cal_sparator_insentif2('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][insentif3]', old($mealtran_details->insentif3.'[insentif3]', $mealtran_details->insentif3), ['id' => 'insentif3'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => '3*', 'oninput' => 'cal_sparator_insentif3('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][insentif4]', old($mealtran_details->id.'[insentif4]', $mealtran_details->insentif4), ['class' => 'form-control input-smform', 'placeholder' => '4*', 'id' => 'insentif4'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_insentif4('.$mealtran_details->id.');']) }} 
								</td> 
								<td class="col-sm-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][insentif5]', old($mealtran_details->id.'[insentif5]', $mealtran_details->insentif5), ['class' => 'form-control input-smform', 'placeholder' => '5*', 'id' => 'insentif5'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_insentif5('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-2 col-md-2">  
									 {{ number_format(str_replace(",","",$mealtran_details->insentif1) + str_replace(",","",$mealtran_details->insentif2) + str_replace(",","",$mealtran_details->insentif3) + str_replace(",","",$mealtran_details->insentif4) + str_replace(",","",$mealtran_details->insentif5),0) }} 
								</td> 

								<td class="col-sm-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][addinsentif]', old($mealtran_details->id.'[addinsentif]', $mealtran_details->addinsentif), ['class' => 'form-control input-smform', 'placeholder' => 'Koreksi*', 'id' => 'addinsentif'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_addinsentif_x('.$mealtran_details->id.');']) }}
								</td> 

								<td class="col-sm-1">  
									 {{ number_format((str_replace(",","",$mealtran_details->insentif1) + str_replace(",","",$mealtran_details->insentif2) + str_replace(",","",$mealtran_details->insentif3) + str_replace(",","",$mealtran_details->insentif4) + str_replace(",","",$mealtran_details->insentif5)) + str_replace(",","",$mealtran_details->addinsentif),0) }} 
								</td> 
								<td class="col-sm-1">  
									 {{$mealtran_details->pottelat}} 
								</td>
								<td class="col-sm-1">  
									  {{ number_format((str_replace(",","",$mealtran_details->um1) + str_replace(",","",$mealtran_details->um2) + str_replace(",","",$mealtran_details->um3) + str_replace(",","",$mealtran_details->um4) + str_replace(",","",$mealtran_details->um5)) + (str_replace(",","",$mealtran_details->insentif1) + str_replace(",","",$mealtran_details->insentif2) + str_replace(",","",$mealtran_details->insentif3) + str_replace(",","",$mealtran_details->insentif4) + str_replace(",","",$mealtran_details->insentif5)) +  str_replace(",","",$mealtran_details->addmeal) + str_replace(",","",$mealtran_details->addinsentif) + $mealtran_details->pottelat1 + $mealtran_details->pottelat2 + $mealtran_details->pottelat3 + $mealtran_details->pottelat4 + $mealtran_details->pottelat5,0) }}  
								</td> 
								<td class="col-sm-1">  
									 {{ number_format((str_replace(",","",$mealtran_details->um1) + str_replace(",","",$mealtran_details->um2) + str_replace(",","",$mealtran_details->um3) + str_replace(",","",$mealtran_details->um4) + str_replace(",","",$mealtran_details->um5)) + (str_replace(",","",$mealtran_details->insentif1) + str_replace(",","",$mealtran_details->insentif2) + str_replace(",","",$mealtran_details->insentif3) + str_replace(",","",$mealtran_details->insentif4) + str_replace(",","",$mealtran_details->insentif5))  +  str_replace(",","",$mealtran_details->addmeal)  + str_replace(",","",$mealtran_details->addinsentif) - str_replace(",","",$mealtran_details->potabsence) - $mealtran_details->pottelat1 - $mealtran_details->pottelat2 - $mealtran_details->pottelat3 - $mealtran_details->pottelat4 - $mealtran_details->pottelat5,0) }} 
								</td> 
							</td>  
						</tr>  
						@endforeach
					</tbody>

					<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th align="right">Total</th>
						<th>{{$mealtran_total->um1}}</th>
						<th>{{$mealtran_total->um2}}</th>
						<th>{{$mealtran_total->um3}}</th>
						<th>{{$mealtran_total->um4}}</th>
						<th>{{$mealtran_total->um5}}</th> 
						<th>{{$mealtran_total->addmeal}}</th>
						<th>{{$mealtran_total->tum}}</th>

						<th>{{$mealtran_total->insentif1}}</th>
						<th>{{$mealtran_total->insentif2}}</th>
						<th>{{$mealtran_total->insentif3}}</th>
						<th>{{$mealtran_total->insentif4}}</th>
						<th>{{$mealtran_total->insentif5}}</th> 
						<th>{{ number_format(str_replace(",","",$mealtran_total->insentif1) + str_replace(",","",$mealtran_total->insentif2) + str_replace(",","",$mealtran_total->insentif3) + str_replace(",","",$mealtran_total->insentif4) + str_replace(",","",$mealtran_total->insentif5),0) }} </th>
						<th>{{ number_format((str_replace(",","",$mealtran_total->insentif1) + str_replace(",","",$mealtran_total->insentif2) + str_replace(",","",$mealtran_total->insentif3) + str_replace(",","",$mealtran_total->insentif4) + str_replace(",","",$mealtran_total->insentif5)) + str_replace(",","",$mealtran_total->tum),0) }} </th>
						<th>{{ number_format($mealtran_total->pottelat1 + $mealtran_total->pottelat2 + $mealtran_total->pottelat3 + $mealtran_total->pottelat4 + $mealtran_total->pottelat5,0) }} </th>
						{{-- <th>{{$mealtran_total->addmeal}} </th> --}}
						<th>{{$mealtran_total->addinsentif}} </th>
						<th>{{ number_format((str_replace(",","",$mealtran_total->insentif1) + str_replace(",","",$mealtran_total->insentif2) + str_replace(",","",$mealtran_total->insentif3) + str_replace(",","",$mealtran_total->insentif4) + str_replace(",","",$mealtran_total->insentif5)) + str_replace(",","",$mealtran_total->tum) + str_replace(",","",$mealtran_total->addmeal),0) }} </th>
						<th>{{ number_format((str_replace(",","",$mealtran_total->insentif1) + str_replace(",","",$mealtran_total->insentif2) + str_replace(",","",$mealtran_total->insentif3) + str_replace(",","",$mealtran_total->insentif4) + str_replace(",","",$mealtran_total->insentif5)) + str_replace(",","",$mealtran_total->tum) +  str_replace(",","",$mealtran_total->addmeal)  - $mealtran_total->pottelat1 - $mealtran_total->pottelat2 - $mealtran_total->pottelat3 - $mealtran_total->pottelat4 - $mealtran_total->pottelat5,0) }}</th>
					</tr>
				</tfoot>
					
				</table> 
			<!-- /.box-body -->
			<a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Submit</a>
			<a href="{{ URL::route('editor.mealtran.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
		</div>
		{!! Form::close() !!} 
	</div>
 </div>
</section>

@stop 

@section('scripts')

<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#mealtranTable").DataTable({
			"sScrollX": true,
	         "scrollY": "350px",
	         "bPaginate": false,
	         "autoWidth": true,
	         "dom": '<"toolbar">frtip',
	         // "ordering": false,
	         fixedColumns:{
	          leftColumns: 3
	         },
		});
		$("div.toolbar").html('<h4>{{ $mealtran->description }} </h4>');
	}); 
</script>  
<script type="text/javascript"> 
	$('#btn_submit').on('click', function()
	{ 
		$.confirm({
			title: 'Confirm!',
			content: 'Are you sure to create data?',
			type: 'green',
			typeAnimated: true,
			buttons: {
				cancel: {
					action: function () { 
					}
				},
				confirm: {
					text: 'CREATE',
					btnClass: 'btn-green',
					action: function () {  
						$('#form_mealtran').submit(); 
					}
				},

			}
		});
	});

	function cal_sparator_um1(id) {  
		//um 1
	    var um1 = document.getElementById('um1' + id).value;
	    var result = document.getElementById('um1' + id);
	    var rsamount = (um1);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('um1' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='um1' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}

  	function cal_sparator_um2(id) {  
	    //um 2
	    var um2 = document.getElementById('um2' + id).value;
	    var result2 = document.getElementById('um2' + id);
	    var rsamount2 = (um2);
	    result2.value = rsamount2.replace(/,/g, "");  

	    n3= document.getElementById('um2' + id);

	    n3.onkeyup=n3.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp1;
	      if(who.id==='um2' + id)  temp1= validDigits(who.value,0); 
	      else temp1= validDigits(who.value);
	      who.value= addCommas(temp1);
	    }   
	    n3.onblur= function(){
	      var 
	      temp3=parseFloat(validDigits(n3.value));
	      if(temp3)n3.value=addCommas(temp3.toFixed(0));
	    }
  	}

  	function cal_sparator_um3(id) {  
	    //um 3
	    var um3 = document.getElementById('um3' + id).value;
	    var result = document.getElementById('um3' + id);
	    var rsamount = (um3);
	    result.value = rsamount.replace(/,/g, "");  

	    n4= document.getElementById('um3' + id);

	    n4.onkeyup=n4.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='um3' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n4.onblur= function(){
	      var 
	      temp4=parseFloat(validDigits(n4.value));
	      if(temp4)n4.value=addCommas(temp4.toFixed(0));
	    }
  	}

  	function cal_sparator_um4(id) {  
	    //um 4
	    var um4 = document.getElementById('um4' + id).value;
	    var result = document.getElementById('um4' + id);
	    var rsamount = (um4);
	    result.value = rsamount.replace(/,/g, "");  

	    n5= document.getElementById('um4' + id);

	    n5.onkeyup=n5.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='um4' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n5.onblur= function(){
	      var 
	      temp5=parseFloat(validDigits(n5.value));
	      if(temp5)n5.value=addCommas(temp5.toFixed(0));
	    }
  	}

  	function cal_sparator_um5(id) {  
	    //um 5
	    var um5 = document.getElementById('um5' + id).value;
	    var result = document.getElementById('um5' + id);
	    var rsamount = (um5);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('um5' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='um5' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}

  	function cal_sparator_addmeal(id) {  
	    //um 5
	    var addmeal = document.getElementById('addmeal' + id).value;
	    var result = document.getElementById('addmeal' + id);
	    var rsamount = (addmeal);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('addmeal' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='addmeal' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}


  	function cal_sparator_insentif1(id) {  
		//insentif 1
	    var insentif1 = document.getElementById('insentif1' + id).value;
	    var result = document.getElementById('insentif1' + id);
	    var rsamount = (insentif1);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('insentif1' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='insentif1' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}

  	function cal_sparator_insentif2(id) {  
	    //insentif 2
	    var insentif2 = document.getElementById('insentif2' + id).value;
	    var result2 = document.getElementById('insentif2' + id);
	    var rsamount2 = (insentif2);
	    result2.value = rsamount2.replace(/,/g, "");  

	    n3= document.getElementById('insentif2' + id);

	    n3.onkeyup=n3.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp1;
	      if(who.id==='insentif2' + id)  temp1= validDigits(who.value,0); 
	      else temp1= validDigits(who.value);
	      who.value= addCommas(temp1);
	    }   
	    n3.onblur= function(){
	      var 
	      temp3=parseFloat(validDigits(n3.value));
	      if(temp3)n3.value=addCommas(temp3.toFixed(0));
	    }
  	}

  	function cal_sparator_insentif3(id) {  
	    //insentif 3
	    var insentif3 = document.getElementById('insentif3' + id).value;
	    var result = document.getElementById('insentif3' + id);
	    var rsamount = (insentif3);
	    result.value = rsamount.replace(/,/g, "");  

	    n4= document.getElementById('insentif3' + id);

	    n4.onkeyup=n4.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='insentif3' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n4.onblur= function(){
	      var 
	      temp4=parseFloat(validDigits(n4.value));
	      if(temp4)n4.value=addCommas(temp4.toFixed(0));
	    }
  	}

  	function cal_sparator_insentif4(id) {  
	    //insentif 4
	    var insentif4 = document.getElementById('insentif4' + id).value;
	    var result = document.getElementById('insentif4' + id);
	    var rsamount = (insentif4);
	    result.value = rsamount.replace(/,/g, "");  

	    n5= document.getElementById('insentif4' + id);

	    n5.onkeyup=n5.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='insentif4' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n5.onblur= function(){
	      var 
	      temp5=parseFloat(validDigits(n5.value));
	      if(temp5)n5.value=addCommas(temp5.toFixed(0));
	    }
  	}

  	function cal_sparator_insentif5(id) {  
	    //insentif 5
	    var insentif5 = document.getElementById('insentif5' + id).value;
	    var result = document.getElementById('insentif5' + id);
	    var rsamount = (insentif5);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('insentif5' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='insentif5' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}

  	function cal_sparator_insentif(id) {  
	    //insentif 5
	    var insentif = document.getElementById('insentif' + id).value;
	    var result = document.getElementById('insentif' + id);
	    var rsamount = (insentif);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('insentif' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='insentif' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}

  	 
  	/**
   * Module for displaying "Waiting for..." dialog using Bootstrap
   *
   * @author Eugene Maslovich <ehpc@em42.ru>
   */

    var waitingDialog = waitingDialog || (function ($) {
        'use strict';

      // Creating modal dialog's DOM
      var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-content">' +
          '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
          '<div class="modal-body">' +
            '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
          '</div>' +
        '</div></div></div>');

      return {
        /**
         * Opens our dialog
         * @param message Process...
         * @param options Custom options:
         *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
          // Assigning defaults
          if (typeof options === 'undefined') {
            options = {};
          }
          if (typeof message === 'undefined') {
            message = 'Loading';
          }
          var settings = $.extend({
            dialogSize: 'm',
            progressType: '',
            onHide: null // This callback runs after the dialog was hidden
          }, options);

          // Configuring dialog
          $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
          $dialog.find('.progress-bar').attr('class', 'progress-bar');
          if (settings.progressType) {
            $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
          }
          $dialog.find('h3').text(message);
          // Adding callbacks
          if (typeof settings.onHide === 'function') {
            $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
              settings.onHide.call($dialog);
            });
          }
          // Opening dialog
          $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
          $dialog.modal('hide');
        }
      };

    })(jQuery);
</script>
@stop

