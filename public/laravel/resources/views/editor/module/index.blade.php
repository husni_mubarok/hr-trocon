@extends('layouts.editor.template')
@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-user"></i> Module
    <small>Auth</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{url('/')}}/editor"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Auth</a></li>
    <li class="active">Module</li>
  </ol>
</section>
@actionStart('module', 'read')
<section class="content">
	<div class="row">
		<div class="col-xs-12"> 
	        <div class="box box-danger">
		        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
		          <a href="{{ URL::route('editor.module.create') }}" type="button" class="btn btn-primary"> <i class="fa fa-sticky-note-o"></i> Add New</a>
		          <button onClick="history.back()" type="button" class="btn btn-primary"> <i class="fa fa-undo"></i> Back</button>
		          <button onClick="reload_table()" type="button" class="btn btn-primary"> <i class="fa fa-refresh"></i> Refresh</button> 
		          <div class="box-tools pull-right">
		            <div class="tableTools-container">
		            </div>
		          </div><!-- /.box-tools -->
		        </div>
	            <div class="box-body">
	                <table id="dtTable" class="table table-bordered table-hover">
					  	<thead>
					  	  	<tr>
						      	<th width="5%">#</th>
						      	<th>Name</th>
						      	<th>Description</th>
						      	<th width="10%">Action</th>
					    	</tr>
					  	</thead>
					  	<tbody>
					    @foreach($modules as $key => $module)
					    	<tr>
					      		<td data-th="#">{{$number++}}</td>
						      	<td data-th="Name">{{$module->name}}</td>
						      	<td data-th="Description">{{$module->description}}</td>
						      	<td align="center">
					      			@actionStart('module', 'update')
					      				<a href="{{ URL::route('editor.module.edit', [$module->id]) }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a>
					      			@actionEnd
					      		</td>
						    </tr>
					    @endforeach
						</tbody>
					</table>
			    </div> 
	        </div>
		</div>
	</section>
</section>
@actionEnd
@stop
@section('scripts')
<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(document).ready(function () {
    $("#dtTable").DataTable();
    });
</script>
@stop