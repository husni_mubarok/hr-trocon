@extends('layouts.editor.template')
@section('content') 

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-exchange"></i> Mutation
    <small>Personal Management</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Personal Management</a></li>
    <li class="active">Mutation</li>
  </ol>
</section>

<section class="content">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border">
          <a href="{{ URL::route('editor.mutation.create') }}" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Add New</a>
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Kembali</button>
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button> 
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>#</th>
                  <th>No Trans</th>
                  <th>Date Trans</th>
                  <th>Employee Name</th>
                  <th>SK No</th>
                  <th>Current Location</th>
                  <th>Next Location</th>
                  <th>Attachment</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
        //editor 
         colReorder: true,
         fixedHeader: true, 
         responsive: true,
         //rowReorder: true, 
         "rowReorder": {
            "update": false,
        },  
         //dttables
         processing: true,
         serverSide: true,
         "pageLength": 25,  
         fixedColumns:   {
          leftColumns: 5,
          rightColumns: 1
         },
         "aoColumnDefs": [
        { "sClass": "my_class", "aTargets": [ 0, 1, 2 ] }
        ],
         "scrollY": "360px", 
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],  
         ajax: "{{ url('editor/mutation/data') }}",
         columns: [  
         { data: 'id', name: 'id' },
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'datetrans', name: 'datetrans', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'employeename', name: 'employeename' },
         { data: 'skno', name: 'skno' }, 
         { data: 'curlocationname', name: 'curlocationname' },
         { data: 'nexlocationname', name: 'nexlocationname' },
         { data: 'attachment', name: 'attachment' },
         { data: 'mstatus', name: 'mstatus' }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });

        //auto number
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

      });
 
    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax 
      } 
   </script> 

   <!-- Add fancyBox -->
   <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
   <script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
   <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox").fancybox();
    });
  </script>
  @stop
