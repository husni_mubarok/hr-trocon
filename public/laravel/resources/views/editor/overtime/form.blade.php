 @extends('layouts.editor.template')
 @section('title', 'Lembur')
 @section('content')

 <style type="text/css">
 	.toolbar {
	    float: left;
	}
 	.input-smform {
	  height: 22px;
	  padding: 1px 3px;
	  font-size: 12px;
	  line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
	  border-radius: 0px;
	  width: 95% !important;
	}
	th,td  {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
	}
 </style>
 <!-- Content Header (Page header) -->
 <section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
 	<h4>
 		Lembur
 	</h4>
 	<ol class="breadcrumb">
 		<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
 		<li><a href="#">Master Data</a></li>
 		<li class="active">Lembur</li>
 	</ol>
 </section>

 <section class="content">
 	<section class="content box box-danger">
 		<div class="row">
			<div class="col-md-12"> 
				{!! Form::model($overtime, array('route' => ['editor.overtime.updatedetail', $overtime->id, $overtime->departmentid], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_overtime'))!!}
					<div class="box-body"> 
						<table class="table table-bordered table-hover stripe" id="overtimeTable">
							<thead>
								<tr>
									<th rowspan="2">Department</th>
									<th rowspan="2">NIK</th>
									<th rowspan="2">Karyawan</th>
									<th rowspan="2">Lokasi </th>
									<th rowspan="2">Ceklis</th>
									<th colspan="5"><center>Lembur</center></th>  
									<th rowspan="2">Koreksi</th>
									<th rowspan="2">Jumlah</th>
									<th rowspan="2">Rate</th>
									<th rowspan="2">Total</th>
									<th rowspan="2">Koreksi</th>
									<th rowspan="2">Grand Total</th>
								</tr>
								<tr>
									<th>Minggu 1</th> 
									<th>Minggu 2</th>
									<th>Minggu 3</th>
									<th>Minggu 4</th>
									<th>Minggu 5</th>
								</tr>
							</thead>
							<tbody> 
								@php
								$ttotal = 0;
								$tgrandtotal = 0;
								@endphp
								@foreach($overtime_detail as $key => $overtime_details)
								<tr style="background-color: @if($overtime_details->checklist == 1) #defee7 @else #fff @endif !important"> 
									<td class="col-sm-1 col-md-1">
										{{$overtime_details->departmentname}} 
									</td>
									<td class="col-sm-1 col-md-1">
										{{$overtime_details->nik}} 
									</td>
									<td class="col-sm-1 col-md-1">
										{{$overtime_details->employeename}} 
									</td>
									<td class="col-sm-1 col-md-1">
										{{$overtime_details->project}} 
									</td>
									<td class="col-sm-1" style="width: 30px !important">
									<input type="checkbox" id="checklist" name="detail[{{$overtime_details->id}}][checklist]" @if($overtime_details->checklist == 1) checked @endif>
								</td>
									<td class="col-sm-1 col-md-1">
										{{ Form::text('detail['.$overtime_details->id.'][ot1]', old($overtime_details->ot1.'[ot1]', $overtime_details->ot1), ['id' => 'ot1'.$overtime_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'OT1*', 'oninput' => 'cal_sparator_ot1('.$overtime_details->id.');']) }} 
									</td>  
									<td class="col-sm-1 col-md-1">  
										{{ Form::text('detail['.$overtime_details->id.'][ot2]', old($overtime_details->ot2.'[ot2]', $overtime_details->ot2), ['id' => 'ot2'.$overtime_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'OT2*', 'oninput' => 'cal_sparator_ot2('.$overtime_details->id.')']) }} 
									</td> 
									<td class="col-sm-1 col-md-1">  
										{{ Form::text('detail['.$overtime_details->id.'][ot3]', old($overtime_details->ot3.'[ot3]', $overtime_details->ot3), ['id' => 'ot3'.$overtime_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'OT3*', 'oninput' => 'cal_sparator_ot3('.$overtime_details->id.')']) }} 
									</td> 
									<td class="col-sm-1 col-md-1">  
										{{ Form::text('detail['.$overtime_details->id.'][ot4]', old($overtime_details->id.'[ot4]', $overtime_details->ot4), ['class' => 'form-control input-smform', 'placeholder' => 'OT4*', 'id' => 'ot4'.$overtime_details->id, 'min' => '0', 'oninput' => 'cal_sparator_ot4('.$overtime_details->id.')']) }} 
									</td> 
									<td class="col-sm-1">  
										{{ Form::text('detail['.$overtime_details->id.'][ot5]', old($overtime_details->id.'[ot5]', $overtime_details->ot5), ['class' => 'form-control input-smform', 'placeholder' => 'OT5*', 'id' => 'ot5'.$overtime_details->id, 'min' => '0', 'oninput' => 'cal_sparator_ot5('.$overtime_details->id.')']) }} 
									</td>  
									<td class="col-sm-1">  
										{{ Form::text('detail['.$overtime_details->id.'][otcorrection]', old($overtime_details->id.'[otcorrection]', $overtime_details->otcorrection), ['class' => 'form-control input-smform', 'placeholder' => 'Koreksi*', 'id' => 'otcorrection'.$overtime_details->id, 'min' => '0', 'oninput' => 'cal_sparator_otcorrection('.$overtime_details->id.')']) }} 
									</td>  
									<td class="col-sm-1">  
										 {{$overtime_details->tovertime}}
									</td> 
									<td class="col-sm-1">  
										{{ Form::text('detail['.$overtime_details->id.'][rateovertime]', old($overtime_details->id.'[rateovertime]', $overtime_details->rateovertime), ['class' => 'form-control input-smform', 'placeholder' => 'Add Overtime*', 'id' => 'rateovertime'.$overtime_details->id, 'min' => '0', 'oninput' => 'cal_sparator_rateovertime('.$overtime_details->id.')']) }} 
									</td> 
									<td class="col-sm-1">  
										 {{number_format($overtime_details->tovertime * str_replace(",","",$overtime_details->rateovertime))}}
									</td>
									<td class="col-sm-1">  
										{{ Form::text('detail['.$overtime_details->id.'][addovertime]', old($overtime_details->id.'[addovertime]', $overtime_details->addovertime), ['class' => 'form-control input-smform', 'placeholder' => 'Add Overtime*', 'id' => 'addovertime'.$overtime_details->id, 'oninput' => 'cal_sparator_addovertime('.$overtime_details->id.')']) }} 
									</td> 
									<td class="col-sm-1">  
										{{number_format(($overtime_details->tovertime * str_replace(",","",$overtime_details->rateovertime)) +   str_replace(",","",$overtime_details->addovertime))}}
									</td>   
							</tr>  
							@php
								$tgrandtotal+= ($overtime_details->tovertime * str_replace(",","",$overtime_details->rateovertime)) +   str_replace(",","",$overtime_details->addovertime);
								$ttotal+= ($overtime_details->tovertime * str_replace(",","",$overtime_details->rateovertime));
							@endphp

							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th align="right">Total</th>
								<th>{{$overtime_total->ot1}}</th>
								<th>{{$overtime_total->ot2}}</th>
								<th>{{$overtime_total->ot3}}</th>
								<th>{{$overtime_total->ot4}}</th>
								<th>{{$overtime_total->ot5}}</th>
								<th>{{$overtime_total->otcorrection}}</th>
								<th>{{$overtime_total->tovertime}}</th>
								<th></th>
								<th>@php echo number_format($ttotal,0); @endphp</th>
								<th>{{$overtime_total->addovertime}}</th>
								<th>@php echo number_format($tgrandtotal,0); @endphp</th>
							</tr>
						</tfoot>
					</table>
				</div> 
				<!-- /.box-body -->
				<a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Submit</a>
				<a href="{{ URL::route('editor.overtime.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
			{!! Form::close() !!} 
 			</div>
 		</div>
 	</div>
 </div>
</section>
</section>

@stop 

@section('scripts')

<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#overtimeTable").DataTable({
		"sScrollX": true,
	         "scrollY": "350px",
	         "bPaginate": false,
	         "autoWidth": false,
	         // "ordering": false,
	         "dom": '<"toolbar">frtip',
	         fixedColumns:{
	          leftColumns: 3
	         },
		});
		$("div.toolbar").html('<h4>{{ $overtime->description }} </h4>');
	});
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
</script>  
<script type="text/javascript"> 
	$('#btn_submit').on('click', function()
	{ 
		$.confirm({
			title: 'Confirm!',
			content: 'Are you sure to create data?',
			type: 'green',
			typeAnimated: true,
			buttons: {
				cancel: {
					action: function () { 
					}
				},
				confirm: {
				text: 'CREATE',
				btnClass: 'btn-green',
				action: function () {  
					$('#form_overtime').submit(); 
				}
			},

			}
		});
	});


	function cal_sparator_ot1(id) {  
		//ot 1
	    var ot1 = document.getElementById('ot1' + id).value;
	    var result = document.getElementById('ot1' + id);
	    var rsamount = (ot1);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('ot1' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='ot1' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}

  	function cal_sparator_ot2(id) {  
	    //ot 2
	    var ot2 = document.getElementById('ot2' + id).value;
	    var result2 = document.getElementById('ot2' + id);
	    var rsamount2 = (ot2);
	    result2.value = rsamount2.replace(/,/g, "");  

	    n3= document.getElementById('ot2' + id);

	    n3.onkeyup=n3.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp1;
	      if(who.id==='ot2' + id)  temp1= validDigits(who.value,0); 
	      else temp1= validDigits(who.value);
	      who.value= addCommas(temp1);
	    }   
	    n3.onblur= function(){
	      var 
	      temp3=parseFloat(validDigits(n3.value));
	      if(temp3)n3.value=addCommas(temp3.toFixed(0));
	    }
  	}

  	function cal_sparator_ot3(id) {  
	    //ot 3
	    var ot3 = document.getElementById('ot3' + id).value;
	    var result = document.getElementById('ot3' + id);
	    var rsamount = (ot3);
	    result.value = rsamount.replace(/,/g, "");  

	    n4= document.getElementById('ot3' + id);

	    n4.onkeyup=n4.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='ot3' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n4.onblur= function(){
	      var 
	      temp4=parseFloat(validDigits(n4.value));
	      if(temp4)n4.value=addCommas(temp4.toFixed(0));
	    }
  	}

  	function cal_sparator_ot4(id) {  
	    //ot 4
	    var ot4 = document.getElementById('ot4' + id).value;
	    var result = document.getElementById('ot4' + id);
	    var rsamount = (ot4);
	    result.value = rsamount.replace(/,/g, "");  

	    n5= document.getElementById('ot4' + id);

	    n5.onkeyup=n5.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='ot4' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n5.onblur= function(){
	      var 
	      temp5=parseFloat(validDigits(n5.value));
	      if(temp5)n5.value=addCommas(temp5.toFixed(0));
	    }
  	}

  	function cal_sparator_ot5(id) {  
	    //ot 5
	    var ot5 = document.getElementById('ot5' + id).value;
	    var result = document.getElementById('ot5' + id);
	    var rsamount = (ot5);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('ot5' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='ot5' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}

  	function cal_sparator_rateovertime(id) {  
	    //ot 5
	    var rateovertime = document.getElementById('rateovertime' + id).value;
	    var result = document.getElementById('rateovertime' + id);
	    var rsamount = (rateovertime);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('rateovertime' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='rateovertime' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}


  	function cal_sparator_overtime(id) {  
	    //ot 5
	    var overtime = document.getElementById('overtime' + id).value;
	    var result = document.getElementById('overtime' + id);
	    var rsamount = (overtime);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('overtime' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='overtime' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}


  	function cal_sparator_addovertime_x(id) {  
	    //ot 5
	    var addovertime = document.getElementById('addovertime' + id).value;
	    var result = document.getElementById('addovertime' + id);
	    var rsamount = (addovertime);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('addovertime' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='addovertime' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}

  	function cal_sparator_amount(id) {  
	    //ot 5
	    var amount = document.getElementById('amount' + id).value;
	    var result = document.getElementById('amount' + id);
	    var rsamount = (amount);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('amount' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='amount' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}

	 
</script>
@stop

