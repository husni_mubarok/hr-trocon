@extends('layouts.editor.template')

@section('content')
<style type="text/css">
    .my_class {
        background-color: white;
    }
</style>


<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
    <h4>Hasil Perhitungan Bonus</h4>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Hasil Perhitungan Bonus</li>
    </ol>
</section>

<section class="content">
    <div class="row"> 
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
                    <button onClick="history.back()" type="button" class="btn btn-danger btn-flat"> 
                        <i class="fa fa-undo"></i> Kembali
                    </button>
                </div>
                @actionStart('bonushasilperhitungan', 'read')
                <div class="box-header">
                    <div class="box-body">
                        <table id="dtTable" class="table table-bordered table-hover stripe">
                            <thead>
                                <th>Aksi</th> 
                                <th>Periode Bonus</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                @actionEnd
            </div>
        </div>
    </div>
</section>  
@stop

@section('scripts')
<script>
    var table;
    $(document).ready(function() {
        table = $('#dtTable').DataTable({ 
            processing: true,
            serverSide: true,
            "pageLength": 25,
            "scrollY": "360px",
            "rowReorder": true,
            "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
            ajax: "{{ url('editor/payroll/bonus/payslip/result/data') }}",
            columns: [  
                { data: 'action', name: 'action', orderable: false, searchable: false }, 
                { data: 'date_format', name: 'date_format', orderable: true, searchable: true }
            ]
        });
        
        //check all
        $("#check-all").click(function () {
            $(".data-check").prop('checked', $(this).prop('checked'));
        });
    });

    function reload_table() {
        table.ajax.reload(null,false); //reload datatable ajax 
    }

    function delete_id(id, name) {
        var name = name.bold();

        $.confirm({
            title: 'Confirm!',
            content: 'Apakah anda yakin untuk menghapus Bonus ' + name + ' ?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                cancel: {
                    action: function () {}
                },
                confirm: {
                    text: 'DELETE',
                    btnClass: 'btn-red',
                    action: function () {
                        waitingDialog.show('Proses...', {dialogSize: 'sm', progressType: 'primary'});
                        $.ajax({
                            url : 'result/' + id,
                            type: "DELETE",
                            data: {
                                '_token': $('input[name=_token]').val() 
                            },
                            success: function(data) { 
                                var options = { 
                                    "positionClass": "toast-bottom-right", 
                                    "timeOut": 1000, 
                                };
                                waitingDialog.hide();
                                toastr.success('Berhasil hapus data!', 'Success Alert', options);
                                reload_table();
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.alert({
                                    type: 'red',
                                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                    title: 'Warning',
                                    content: 'Hapus data gagal!',
                                });
                                location.reload()
                            }
                        });
                    }
                }
            }
        });
    }

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });

     var waitingDialog = waitingDialog || (function ($) {
        'use strict';

        var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
              '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
              '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
              '</div>' +
            '</div></div></div>');

        return {
            show: function (message, options) {
                // Assigning defaults
                if (typeof options === 'undefined') {
                    options = {};
                }
                if (typeof message === 'undefined') {
                    message = 'Loading';
                }
                var settings = $.extend({
                    dialogSize: 'm',
                    progressType: '',
                    onHide: null // This callback runs after the dialog was hidden
                }, options);

                // Configuring dialog
                $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
                $dialog.find('.progress-bar').attr('class', 'progress-bar');
                if (settings.progressType) {
                    $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
                }
                $dialog.find('h3').text(message);
                
                // Adding callbacks
                if (typeof settings.onHide === 'function') {
                    $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                        settings.onHide.call($dialog);
                    });
                }
                // Opening dialog
                $dialog.modal();
            },
            hide: function () {
                $dialog.modal('hide');
            }
        };

    })(jQuery);
</script> 
@stop