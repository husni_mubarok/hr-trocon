@extends('layouts.editor.templatereport')

@section('content')
    <html>
        <head>
            <style>
                .bold { font-weight: bold; }
                .text-center { text-align: center; }

                .font-size-12 { font-size: 10px; }
                .font-size-14 { font-size: 14px; }
                .font-size-16 { font-size: 16px; }

                .pl3 { padding-left: 3px; }
                .p3 { padding: 3px; }

                .m5 { margin: 5px; }

                .border-top { border-top: 1px solid #000; }
                .border-bottom { border-bottom: 1px solid #000; }
            </style>
        </head>
        <body id="main-body">
            <div style="width: 100%; height: auto;">
                @foreach ($resultCalculation as $index => $result)
                    <div style="width:340px; height: 380px; border: 1px solid #000; margin:10px; box-sizing: border-box; display: inline-block;">
                        <table style="width:100%" class="font-size-12">
                            <tr>
                                <td colspan="6" class="bold font-size-16 p3 border-bottom">{{ $result['other_information']['title'] }}</td>
                            </tr>
                            <tr><td></td></td>
                            <tr>
                                <td style="width: 20%;" class="pl3">Nama</td>
                                <td style="width: 1%;" class="pl3">:</td>
                                <td style="width: 29%;" class="pl3">{{ $result['other_information']['employee_name'] }}</td>
                                <td style="width: 15%;" class="pl3">Jabatan</td>
                                <td style="width: 1%;" class="pl3">:</td>
                                <td style="width: 34%;" class="pl3">{{ $result['other_information']['jobtitle'] }}</td>
                            </tr>
                            <tr>
                                <td class="pl3">Grade</td>
                                <td class="pl3">:</td>
                                <td class="pl3">{{ $result['other_information']['grade'] }}</td>
                                <td class="pl3">Status</td>
                                <td class="pl3">:</td>
                                <td class="pl3">{{ $result['other_information']['tax_status'] }}</td>
                            </tr>

                            <tr><td></td></td>
                            <tr>
                                <td colspan="6" class="font-size-14 p3 border-bottom border-top text-center">PENGHASILAN</td>
                            </tr>
                            <tr><td></td></td>
                            
                            <tr>
                                <td class="pl3" colspan="4">Bonus</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format(@($result['bonus'] / $result['value'])) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Value</td>
                                <td class="pl3">:</td>
                                <td class="pl3">{{ $result['value'] }}</td>
                            </tr>
                           
                            <tr>
                                <td colspan="5" class="pl3 bold">BONUS BRUTO</td>
                                <td class="pl3 bold">Rp {{ number_format($result['bonus']) }}</td>
                            </tr>

                            <tr><td></td></td>
                            <tr>
                                <td colspan="6" class="font-size-14 p3 border-bottom border-top text-center">POTONGAN</td>
                            </tr>
                            <tr><td></td></td>
                            
                            <tr>
                                <td class="pl3" colspan="4">PPh Pasal 21</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['tax']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Potongan Pinjaman</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['potongan']) }}</td>
                            </tr>
                            <tr>
                                <td colspan="5" class="pl3 bold">JUMLAH POTONGAN</td>
                                <td class="pl3 bold">Rp {{ number_format($result['tax'] + $result['potongan']) }}</td>
                            </tr>

                            <tr><td></td></td>
                            <tr>
                                <td colspan="6" class="border-bottom"></td>
                            </td>
                            <tr>
                                <td colspan="5" class="pl3 bold">Bonus Netto</td>
                                <td class="pl3 bold">Rp {{ number_format($result['bonus_final']) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" class="border-bottom"></td>
                            </td>
                            <tr>
                                <td class="pl3" colspan="4">Sisa Pinjaman Karyawan</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['hutang']) }}</td>
                            </tr>
                            <td></td></td>
                            
                            <tr>
                                <td class="pl3 text-center" colspan="2"><img src="{{ asset('laravel/storage/payroll-report/mgr_finance.jpeg') }}" width="70px"></td>
                                <td class="pl3 text-center" style="padding-left:20px" colspan="2"><img src="{{ asset('laravel/storage/payroll-report/finance.jpeg') }}" width="70px"></td>
                                <td class="pl3 text-center" colspan="2"><img src="{{ asset('laravel/storage/payroll-report/hrd.jpeg') }}" width="70px"></td>
                            </tr>
                            <tr>
                                <td class="pl3 text-center" colspan="2">Mgr Finance</td>
                                <td class="pl3 text-center" colspan="2">Finance</td>
                                <td class="pl3" style="padding-left:30px" colspan="2">HRD</td>
                            </tr>
                        </table>
                    </div>
                @endforeach
            </div>
        </body>
    <html>
@stop