@extends('layouts.editor.template')

@section('content')
<style type="text/css">
    .my_class {
        background-color: white;
    }
</style>


<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
    <h4>Perhitungan Gaji</h4>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Perhitungan Gaji</li>
    </ol>
</section>

<section class="content">
    <div class="row"> 
        {!! Form::model($datafilter)!!}
        {{ csrf_field() }}
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
                    <div class="col-sm-2" data-toggle="tooltip" data-placement="top" title="Department">
                        {{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control', 'placeholder' => 'Select Department', 'id' => 'departmentid', 'onchange' => 'refreshData();')) }} 
                    </div>
                    <div class="col-sm-2" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Period">
                        {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'refreshData();')) }} 
                    </div>
                    @actionStart('payrollperhitungan', 'create')
                    <button onClick="generateData()" type="button" class="btn btn-danger btn-flat">
                        <i class="fa fa-magic"></i> Generate!
                    </button>
                    @actionEnd
                    <button onClick="history.back()" type="button" class="btn btn-danger btn-flat"> 
                        <i class="fa fa-undo"></i> Kembali
                    </button>
                    <div class="box-tools pull-right">
                        <div class="tableTools-container"></div>
                    </div>
        {!! Form::close() !!}
                </div>  
                @actionStart('payrollperhitungan', 'read')
                <div class="box-header">
                    <div class="box-body">
                        <table id="dtTable" class="table table-bordered table-hover stripe">
                            <thead>
                                <tr> 
                                    <th>ID</th>
                                    <th>Nama Karyawan</th> 
                                    <th>Posisi</th>
                                    <th>Department</th>
                                    <th>Gaji Pokok</th>
                                    <th>Tunj. Makan</th>
                                    <th>Tunj. Makan Proyek</th>
                                    <th>Lembur Tetap</th>
                                    <th>Lembur Proyek</th>
                                    <th>Tunj. Keahlian</th>
                                    <th>Tunj. Kesehatan</th>
                                    <th>TM, TLK, Inst</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot align="right">
                                <tr><th></th><th></th><th></th><th></th></tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                @actionEnd
            </div>
        </div>
    </div>
</section>  
@stop

@section('scripts')
<script>
    var table;
    $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
            processing: true,
            serverSide: true,
            "pageLength": 25,
            "scrollY": "360px",
            "scrollX": true,
            "sScrollXInner": "150%",
            "order": [[ 0, "asc" ]],
            "autoWidth": true,
            "rowReorder": true,
            fixedColumns:   {
                leftColumns: 4
            },
            "aoColumnDefs": [
                { "sClass": "my_class", "aTargets": [ 0, 1, 2, 3 ] }
            ],
            "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]], 
            ajax: "{{ url('editor/payroll/calculation/preview/data') }}",
            columns: [   
                { data: 'employeeid', name: 'employeeid' }, 
                { data: 'employee', name: 'employee' }, 
                { data: 'position', name: 'position' },
                { data: 'departmentname', name: 'departmentname' },
                { data: 'basic', name: 'basic', render: $.fn.dataTable.render.number( ',', '.', 0) },
                { data: 'mealtransall', name: 'mealtransall', render: $.fn.dataTable.render.number( ',', '.', 0) },
                { data: 'tunj_keahlian', name: 'tunj_keahlian', render: $.fn.dataTable.render.number( ',', '.', 0) },
                { data: 'tunj_kesehatan', name: 'tunj_kesehatan', render: $.fn.dataTable.render.number( ',', '.', 0) },
                { data: 'uang_makan', name: 'uang_makan', render: $.fn.dataTable.render.number( ',', '.', 0) },
                { data: 'overtimeall', name: 'overtimeall', render: $.fn.dataTable.render.number( ',', '.', 0) },
                { data: 'lembur_proyek', name: 'lembur_proyek', render: $.fn.dataTable.render.number( ',', '.', 0) },
                { data: 'tm_tdk_tlk_instpd', name: 'tm_tdk_tlk_instpd', render: $.fn.dataTable.render.number( ',', '.', 0) }
            ]
        });
        
        //check all
        $("#check-all").click(function () {
            $(".data-check").prop('checked', $(this).prop('checked'));
        });
    });

    function reload_table() {
        table.ajax.reload(null,false); //reload datatable ajax 
    }

    function generateData() {
        var periodid = $('#periodid').val();
        var perioddesc = $("#periodid option:selected").text();

        $.confirm({
            title: 'Confirm!',
            content: 'Apakah anda yakin untuk menghitung gaji untuk period <b><u>' + perioddesc + '</u></b> ini?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                cancel: {
                    action: function () {}
                },
                confirm: {
                    text: 'CREATE',
                    btnClass: 'btn-red',
                    action: function () { 
                        waitingDialog.show('Proses...', {dialogSize: 'sm', progressType: 'primary'});
                        $.ajax({
                            url : 'payslip/generate',
                            type: "POST",
                            data: {
                                '_token': $('input[name=_token]').val(),
                                'period_id': periodid
                            },
                            success: function(data) { 
                                var options = { 
                                    "positionClass": "toast-bottom-right", 
                                    "timeOut": 1000, 
                                };
                                waitingDialog.hide();
                                toastr.success('Payroll berhasil dihitung!', 'Success Alert', options);
                                window.location = "{{url('editor/payroll/calculation/payslip/result')}}";
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.alert({
                                    type: 'red',
                                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                    title: 'Warning',
                                    content: jqXHR.responseJSON.message + '!',
                                });
                                location.reload()
                            }
                        });
                    }
                }
            }
        });
    }
      
    function refreshData() {  
        $.ajax({
            type: 'POST',
            url: "{{ URL::route('editor.periodfilteronly') }}",
            data: {
                '_token': $('input[name=_token]').val(),     
                'periodid': $('#periodid').val(),   
                'departmentid': $('#departmentid').val(),   
            }, 
            success: function(data) { 
                var options = { 
                    "positionClass": "toast-bottom-right", 
                    "timeOut": 1000, 
                };
                toastr.success('Berhasil memfilter data!', 'Success Alert', options);
                reload_table();
            }
        }) 
    }; 

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });

    var waitingDialog = waitingDialog || (function ($) {
        'use strict';

        var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
              '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
              '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
              '</div>' +
            '</div></div></div>');

        return {
            show: function (message, options) {
                // Assigning defaults
                if (typeof options === 'undefined') {
                    options = {};
                }
                if (typeof message === 'undefined') {
                    message = 'Loading';
                }
                var settings = $.extend({
                    dialogSize: 'm',
                    progressType: '',
                    onHide: null // This callback runs after the dialog was hidden
                }, options);

                // Configuring dialog
                $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
                $dialog.find('.progress-bar').attr('class', 'progress-bar');
                if (settings.progressType) {
                    $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
                }
                $dialog.find('h3').text(message);
                
                // Adding callbacks
                if (typeof settings.onHide === 'function') {
                    $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                        settings.onHide.call($dialog);
                    });
                }
                // Opening dialog
                $dialog.modal();
            },
            hide: function () {
                $dialog.modal('hide');
            }
        };

    })(jQuery);
</script> 
@stop