@extends('layouts.editor.template')

@section('content')
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- <div class="col-xs-2">
		</div> -->
		<div class="col-xs-12">
			<div class="box box-danger">
				@include('errors.error')
				@if(isset($employee))
					{!! Form::model($employee, array('route' => ['correction.update', $employee->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_correction'))!!}

				@else
					{!! Form::open(array('route' => 'correction.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_correction'))!!}
				@endif
				
				{{ csrf_field() }}
				<div class="box-header with-border">
					<section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
						<h4>
							@if(isset($employee))
								<i class="fa fa-pencil"></i> Tambah
							@else
								<i class="fa fa-plus"></i> 
							@endif
							Pembetulan
						</h4>
					</section>
				</div>

				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('name', 'Nama') }}
								<span> : {{ $employee->employeename }}</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('netto_income', 'Jumlah Netto Income yang Sudah Diterima') }}
								{{ Form::text('netto_income', 0, array('class' => 'form-control', 'placeholder' => 'Masukkan jumlah *', 'required' => 'true', 'id' => 'netto_income')) }} 
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('tax', 'Jumlah Pajak yang Sudah Dibayar') }}
								{{ Form::text('tax', 0, array('class' => 'form-control', 'placeholder' => 'Masukkan jumlah *', 'required' => 'true', 'id' => 'tax')) }} 
							</div>
						</div>
					</div>
				</div>


				<div class="box-header with-border">
					<div class="col-md-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Simpan</button>
							<a href="{{ url('editor/payroll/correction') }}" class="btn btn-danger pull-right" style="margin-right: 10px">
								<i class="fa fa-close"></i> Batal
							</a>
						</div>
					</div>
				</div>
			</section><!-- /.content -->
		</div>
	</div>
</section>
@stop

