 @extends('layouts.editor.template')
 @section('title', 'Pendapatan dan Potongan Lainnya')
 @section('content')
  <style type="text/css">
  .toolbar {
	    float: left;
	}
 .input-smform {
	  height: 22px;
	  font-size: 12px;
	  line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
	  border-radius: 0px;
	  width: 97% !important;
	}

	th,td  {
	    overflow: hidden;
	    white-space: nowrap;
	    text-overflow: ellipsis;
	}
</style>
 <!-- Content Header (Page header) -->
 <section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
 	<h4>
 		Pendapatan dan Potongan Lainnya
 	</h4>
 	<ol class="breadcrumb">
 		<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
 		<li><a href="#">Master Data</a></li>
 		<li class="active">Pendapatan dan Potongan Lainnya</li>
 	</ol>
 </section>

 <section class="content">
 	<section class="content box box-solid">
 		<div class="row">
 			<div class="col-md-12 col-sm-12 col-xs-12"> 
 				<div class="col-md-12"> 
 					<div class="col-md-12">   
 						<div class="tab-content">

 							{!! Form::model($payroll, array('route' => ['editor.payroll.update', $payroll->id], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_payroll'))!!}
 							<div id="consummable" class="tab-pane fade in active"> 
 								<div class="box-body">
 									<div class="div_overflow"> 

 										<table class="table table-bordered table-hover stripe" id="payrollTable">
 											<thead>
 												<tr>
 													<th width="20%">Employee Name </th>
 													<th width="20%">Department </th>
 													<th width="10%">Pot Lainnya</th> 
 													<th width="10%">Pendapatan Lainnya</th>
 													<!-- <th width="10%">Pot Kendaraan</th> -->
 													<!-- <th width="10%">Pot Rumah</th>  -->
 												</tr>
 											</thead>
 											<tbody> 
 												@foreach($payroll_detail as $key => $payroll_details)
 												<tr>
 													<td>
 														{{$payroll_details->employeename}} 
 													</td>
 													<td>
 														{{$payroll_details->departmentname}} 
 													</td>
 													<td>
 														{{ Form::text('detail['.$payroll_details->id.'][otherded]', old($payroll_details->otherded.'[otherded]', $payroll_details->otherded), ['id' => 'otherded'.$payroll_details->id, 'min' => '0', 'class' => 'form-smform', 'placeholder' => 'Pot Lainnya*', 'oninput' => 'cal_sparator_otherded('.$payroll_details->id.')']) }} 
 													</td>  
 													<td>  
 														{{ Form::text('detail['.$payroll_details->id.'][otherall]', old($payroll_details->otherall.'[otherall]', $payroll_details->otherall), ['id' => 'otherall'.$payroll_details->id, 'min' => '0', 'class' => 'form-smform', 'placeholder' => '', 'oninput' => 'cal_sparator_otherall('.$payroll_details->id.')']) }} 
 													</td> 
 													<!-- <td>  
 														{{ Form::text('detail['.$payroll_details->id.'][vehicleloan]', old($payroll_details->vehicleloan.'[vehicleloan]', $payroll_details->vehicleloan), ['id' => 'vehicleloan'.$payroll_details->id, 'min' => '0', 'class' => 'form-smform', 'placeholder' => 'Pot Kendaraan*', 'oninput' => 'cal_sparator_vehicleloan('.$payroll_details->id.')']) }} 
 													</td>  -->
 													<!-- <td>  
 														{{ Form::text('detail['.$payroll_details->id.'][homeloan]', old($payroll_details->id.'[homeloan]', $payroll_details->homeloan), ['class' => 'form-smform', 'placeholder' => 'Pot Rumah*', 'id' => 'homeloan'.$payroll_details->id, 'min' => '0', 'oninput' => 'cal_sparator_homeloan('.$payroll_details->id.');']) }}
 													</td>  -->
 												</td>  
 											</tr>  
 											@endforeach

 										</tbody>
 									</table>
 								</div> 
 								<!-- /.box-body -->
 							</div>
 						</div> 
 					</div> 
 					<a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Submit</a>
 					<a href="{{ URL::route('editor.payroll.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
 				</div>
 				{!! Form::close() !!} 
 			</div>
 		</div>
 	</div>
 </div>
</section>

@stop 

@section('scripts')

<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#payrollTable").DataTable({
			"sScrollX": true,
	         "scrollY": "350px",
	         "bPaginate": false,
	         "autoWidth": true,
	         "dom": '<"toolbar">frtip',
	         // "ordering": false,
	         // fixedColumns:{
	         //  leftColumns: 2
	         // },
		});
		$("div.toolbar").html('<h4>{{ $payroll->description }} </h4>');
	}); 
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
</script>  
<script type="text/javascript"> 
	$('#btn_submit').on('click', function()
	{ 
		$.confirm({
			title: 'Confirm!',
			content: 'Are you sure to create data?',
			type: 'green',
			typeAnimated: true,
			buttons: {
				cancel: {
					action: function () { 
					}
				},
				confirm: {
					text: 'CREATE',
					btnClass: 'btn-green',
					action: function () {  
						$('#form_payroll').submit(); 
					}
				},

			}
		});
	});

	function cal_sparator_otherded(id) {  
	    //um 2
	    var otherded = document.getElementById('otherded' + id).value;
	    var result2 = document.getElementById('otherded' + id);
	    var rsamount2 = (otherded);
	    result2.value = rsamount2.replace(/,/g, "");  

	    n3= document.getElementById('otherded' + id);

	    n3.onkeyup=n3.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp1;
	      if(who.id==='otherded' + id)  temp1= validDigits(who.value,0); 
	      else temp1= validDigits(who.value);
	      who.value= addCommas(temp1);
	    }   
	    n3.onblur= function(){
	      var 
	      temp3=parseFloat(validDigits(n3.value));
	      if(temp3)n3.value=addCommas(temp3.toFixed(0));
	    }
  	}

  	function cal_sparator_otherall(id) {  
	    //um 2
	    var otherall = document.getElementById('otherall' + id).value;
	    var result2 = document.getElementById('otherall' + id);
	    var rsamount2 = (otherall);
	    result2.value = rsamount2.replace(/,/g, "");  

	    n3= document.getElementById('otherall' + id);

	    n3.onkeyup=n3.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp1;
	      if(who.id==='otherall' + id)  temp1= validDigits(who.value,0); 
	      else temp1= validDigits(who.value);
	      who.value= addCommas(temp1);
	    }   
	    n3.onblur= function(){
	      var 
	      temp3=parseFloat(validDigits(n3.value));
	      if(temp3)n3.value=addCommas(temp3.toFixed(0));
	    }
  	}

  	function cal_sparator_vehicleloan(id) {   
	    //um 2
	    var vehicleloan = document.getElementById('vehicleloan' + id).value;
	    var result2 = document.getElementById('vehicleloan' + id);
	    var rsamount2 = (vehicleloan);
	    result2.value = rsamount2.replace(/,/g, "");  

	    n3= document.getElementById('vehicleloan' + id);

	    n3.onkeyup=n3.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp1;
	      if(who.id==='vehicleloan' + id)  temp1= validDigits(who.value,0); 
	      else temp1= validDigits(who.value);
	      who.value= addCommas(temp1);
	    }   
	    n3.onblur= function(){
	      var 
	      temp3=parseFloat(validDigits(n3.value));
	      if(temp3)n3.value=addCommas(temp3.toFixed(0));
	    }
  	}


  	function cal_sparator_homeloan(id) {   
	    //um 2
	    var homeloan = document.getElementById('homeloan' + id).value;
	    var result2 = document.getElementById('homeloan' + id);
	    var rsamount2 = (homeloan);
	    result2.value = rsamount2.replace(/,/g, "");  

	    n3= document.getElementById('homeloan' + id);

	    n3.onkeyup=n3.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp1;
	      if(who.id==='homeloan' + id)  temp1= validDigits(who.value,0); 
	      else temp1= validDigits(who.value);
	      who.value= addCommas(temp1);
	    }   
	    n3.onblur= function(){
	      var 
	      temp3=parseFloat(validDigits(n3.value));
	      if(temp3)n3.value=addCommas(temp3.toFixed(0));
	    }
  	}
</script>
@stop

