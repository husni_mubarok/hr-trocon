@extends('layouts.editor.template')
@section('title', 'Pendapatan dan Potongan Lainnya')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    Pendapatan dan Potongan Lainnya
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Pendapatan dan Potongan Lainnya</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          
            <!-- <a onClick="CraeteData()" class="btn btn-warning btn-flat" style="float:left; margin-right:5px"  data-toggle="tooltip" data-placement="top" title="Create for get data from selected period"> <i class="fa fa-play"></i>  Create</a> -->
                <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Period">
                  <div class="col-sm-12 pull-right">
                    <input type="hidden" id="type" name="type" value="generate"> 
                    <input type="hidden" id="userid" name="userid" value="">  
                       <!-- {{ Form::select('periodidfilter', $payperiod_list, old('periodidfilter'), array('class' => 'form-control', 'placeholder' => 'Pilih Periode', 'id' => 'periodidfilter', 'onchange' => 'RefreshData();')) }}   -->
                      <select class="form-control" id="periodid" onchange="CraeteData();" name="periodid">
                        @if(isset($datafilter))
                          <option value="{{$datafilter->periodid}}">{{$datafilter->description}}</option>
                        @else
                          <option>Pilih Periode</option>
                        @endif

                        @foreach($payperiod_list AS $payperiod_lists)
                        <option value="{{$payperiod_lists->id}}">{{$payperiod_lists->description}}</option>
                        @endforeach
                      </select> 
                </div>
              </div>
              <a onClick="GenerateData()" class="btn btn-danger btn-flat" style="float:left; margin-right:5px"> <i class="fa fa-magic"></i> Generate</a>
              <a onClick="RefreshData()" class="btn btn-success btn-flat" style="float:left; margin-left:5px"> <i class="fa fa-refresh"></i>  Refresh</a>
 
          <div class="box-tools pull-right">
            <div class="tableTools-container">
              </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th style="width:5%">
                    <label class="control control--checkbox">
                      <input type="checkbox" id="check-all"/>
                      <div class="control__indicator"></div>
                    </label>
                  </th>
                  <th>Aksi</th> 
                  <th>Description</th> 
                  <th>Period</th> 
                  <th>Start Date</th> 
                  <th>End Date</th>
                  <th>Payment Date</th>
                  <th>Month</th>
                  <th>Year</th>
                  <th>Jenis Gaji</th> 
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
          <div class="form-group pull-right">
            <label for="real_name" class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8 pull-right">
              <select class="form-control" style="width: 100%;" name="status"  id="status">
               <option value="0">Aktif</option>
               <option value="1">Tidak Aktif</option>
             </select>
           </div>
         </div>
         <h3 class="modal-title">Payroll Form</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row">
 
            <div class="form-group">
              <label class="control-label col-md-3">Payroll Name</label>
              <div class="col-md-8">
                <input name="payrollname" id="payrollname" class="form-control" type="text">
                <small class="errorPayrollName hidden alert-danger"></small> 
              </div>
            </div> 
          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Tutup</button>
      <button type="button" id="btnSaveAdd" onClick="saveadd()" class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-plus-square"></i> Simpan & Tambah</button>
      <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Simpan & Tutup</button>

    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/payroll/data') }}",
         columns: [  
         { data: 'check', name: 'check', orderable: false, searchable: false },
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'description', name: 'description' },
         { data: 'dateperiod', name: 'dateperiod', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'begindate', name: 'begindate', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'enddate', name: 'enddate', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'paydate', name: 'paydate', render: function(d){return moment(d).format("DD-MM-YYYY");} }, 
         { data: 'month', name: 'month' },
         { data: 'year', name: 'year' },
         { data: 'payrolltypename', name: 'payrolltypename' }, 
         { data: 'mstatus', name: 'mstatus' }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });

      function reload_table()
      {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function CraeteData()
      {
 
        var periodid = $('#periodid').val();
        var perioddesc = $("#periodid option:selected").text();
  
         waitingDialog.show('Proses...', {dialogSize: 'sm', progressType: 'primary'});
         $.ajax({
          url : 'payroll/create/' + periodid,
          type: "POST",
          data: {
            '_token': $('input[name=_token]').val() 
          },
          success: function(data)
          { 
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully created data!', 'Success Alert', options);
            RefreshData();
            reload_table();
            waitingDialog.hide();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            $.alert({
              type: 'red',
              icon: 'fa fa-danger', // glyphicon glyphicon-heart
              title: 'Warning',
              content: 'Error creating data!',
            });
          }
        }); 
      }

      function GenerateData()
      {
 
        var periodid = $('#periodid').val();
        var perioddesc = $("#periodid option:selected").text();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to generate <b><u>' + perioddesc + '</u></b> data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'CREATE',
            btnClass: 'btn-red',
            action: function () { 
              waitingDialog.show('Proses...', {dialogSize: 'sm', progressType: 'primary'});
             $.ajax({
              url : 'payroll/generate/' + periodid,
              type: "POST",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully genareted data!', 'Success Alert', options);
                reload_table();
                waitingDialog.hide();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error generate data!',
                });
                waitingDialog.hide();
              }
            });
           }
         },

       }
     });
      }
      function RefreshData()
       {  
        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.periodfilteronly') }}",
          data: {
            '_token': $('input[name=_token]').val(),     
            'periodid': $('#periodid').val()   
          }, 
          success: function(data) { 
            // var options = { 
            //   "positionClass": "toast-bottom-right", 
            //   "timeOut": 1000, 
            // };
            // toastr.success('Successfully filtering data!', 'Success Alert', options);
            reload_table();
          }
        }) 
      }; 
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();   
      });

      /**
       * Module for displaying "Waiting for..." dialog using Bootstrap
       *
       * @author Eugene Maslovich <ehpc@em42.ru>
       */

        var waitingDialog = waitingDialog || (function ($) {
            'use strict';

          // Creating modal dialog's DOM
          var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
              '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
              '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
              '</div>' +
            '</div></div></div>');

          return {
            /**
             * Opens our dialog
             * @param message Process...
             * @param options Custom options:
             *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
             *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
             */
            show: function (message, options) {
              // Assigning defaults
              if (typeof options === 'undefined') {
                options = {};
              }
              if (typeof message === 'undefined') {
                message = 'Loading';
              }
              var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
              }, options);

              // Configuring dialog
              $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
              $dialog.find('.progress-bar').attr('class', 'progress-bar');
              if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
              }
              $dialog.find('h3').text(message);
              // Adding callbacks
              if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                  settings.onHide.call($dialog);
                });
              }
              // Opening dialog
              $dialog.modal();
            },
            /**
             * Closes dialog
             */
            hide: function () {
              $dialog.modal('hide');
            }
          };

        })(jQuery);
   </script> 
   @stop
