@extends('layouts.editor.template')

@section('content')
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- <div class="col-xs-2">
		</div> -->
		<div class="col-xs-12">
			<div class="box box-danger">
				@include('errors.error')
				@if(isset($masterPtkp))
					{!! Form::model($masterPtkp, array('route' => ['master-ptkp.update', $masterPtkp->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_master_ptkp'))!!}

				@else
					{!! Form::open(array('route' => 'master-ptkp.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_master_ptkp'))!!}
				@endif
				
				{{ csrf_field() }}
				<div class="box-header with-border">
					<section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
						<h4>
							@if(isset($masterPtkp))
								<i class="fa fa-pencil"></i> Ubah
							@else
								<i class="fa fa-plus"></i> 
							@endif
							Master PTKP
						</h4>
					</section>
				</div>

				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('name', 'Nama') }}
								{{ Form::select('name', $masterPtkps, old('name'), array('class' => 'form-control', 'placeholder' => 'Pilih Nama PTKP *', 'required' => 'true', 'id' => 'name')) }} 
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('amount', 'Jumlah') }}
								{{ Form::text('amount', old('amount'), array('class' => 'form-control', 'placeholder' => 'Masukkan jumlah *', 'required' => 'true', 'id' => 'amount')) }} 
							</div>
						</div>
					</div>
				</div>


				<div class="box-header with-border">
					<div class="col-md-12">
						<div class="form-group">
							<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Simpan</button>
							<a href="{{ url('editor/payroll/setting/master-ptkp') }}" class="btn btn-danger pull-right" style="margin-right: 10px">
								<i class="fa fa-close"></i> Batal
							</a>
						</div>
					</div>
				</div>
			</section><!-- /.content -->
		</div>
	</div>
</section>
@stop

