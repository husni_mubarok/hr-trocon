@extends('layouts.editor.template')

@section('content')
<style type="text/css">
    .my_class {
        background-color: white;
    }
</style>

<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
    <h4>Master PTKP</h4>
    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li><a href="#">Payroll</a></li>
        <li><a href="#">Pengaturan</a></li>
        <li class="active">Master PTKP</li>
    </ol>
</section>

<section class="content">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div>
        </div>
        
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
                    @actionStart('masterptkp', 'create')
                    <a href="{{ url('editor/payroll/setting/master-ptkp/create') }}" type="button" class="btn btn-primary btn-flat">
                        <i class="fa fa-sticky-note-o"></i> Tambah Baru
                    </a>
                    @actionEnd
                    @actionStart('masterptkp', 'delete')
                    <button class="btn btn-danger btn-flat" onclick="bulk_delete()">
                        <i class="glyphicon glyphicon-trash"></i> Hapus Sekaligus
                    </button>
                    @actionEnd
                    <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> 
                        <i class="fa fa-refresh"></i> Refresh
                    </button>
                    <button onClick="history.back()" type="button" class="btn btn-danger btn-flat"> 
                        <i class="fa fa-undo"></i> Kembali
                    </button>
                    <div class="box-tools pull-right">
                        <div class="tableTools-container"></div>
                    </div>
                </div>
                @actionStart('masterptkp', 'read')
                <div class="box-header">
                    <div class="box-body">
                        <table id="dtTable" class="table table-bordered table-hover stripe">
                            <thead>
                                <tr>
                                    <th style="width:5%">
                                        <label class="control control--checkbox">
                                            <input type="checkbox" id="check-all"/>
                                            <div class="control__indicator"></div>
                                        </label> 
                                    </th>
                                    <th>Aksi</th> 
                                    <th>Nama</th>
                                    <th>Jumlah</th> 
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                @actionEnd
            </div>
        </div>
    </div>
</section>  
@stop

@section('scripts')
<script>
    var table;
    $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
            processing: true,
            serverSide: true,
            "pageLength": 25,
            "scrollY": "360px",
            "rowReorder": true,
            "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
            ajax: "{{ url('editor/payroll/setting/master-ptkp/data') }}",
            columns: [  
                { data: 'check', name: 'check', orderable: false, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }, 
                { data: 'name', name: 'name' },
                { data: 'amount', name: 'amount', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp ' ) }
            ]
        });
            
        //check all
        $("#check-all").click(function () {
            $(".data-check").prop('checked', $(this).prop('checked'));
        });
    });
      
    function reload_table() {
        table.ajax.reload(null,false); //reload datatable ajax 
    }

    function delete_id(id, name) {
        var name = name.bold();

        $.confirm({
            title: 'Confirm!',
            content: 'Apakah anda yakin untuk menghapus ' + name + ' ?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                cancel: {
                    action: function () {}
                },
                confirm: {
                    text: 'DELETE',
                    btnClass: 'btn-red',
                    action: function () {
                        $.ajax({
                            url : 'master-ptkp/' + id,
                            type: "DELETE",
                            data: {
                                '_token': $('input[name=_token]').val() 
                            },
                            success: function(data) { 
                                var options = { 
                                    "positionClass": "toast-bottom-right", 
                                    "timeOut": 1000, 
                                };
                                toastr.success('Berhasil hapus data!', 'Success Alert', options);
                                reload_table();
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.alert({
                                    type: 'red',
                                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                    title: 'Warning',
                                    content: 'Hapus data gagal!',
                                });
                                location.reload()
                            }
                        });
                    }
                }
            }
        });
    }

    function bulk_delete() {
        var list_id = [];
        $(".data-check:checked").each(function() {
            list_id.push(this.value);
        });

        if (list_id.length > 0) {
            $.confirm({
                title: 'Confirm!',
                content: 'Yakin akan menghapus '+list_id.length+' data?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    cancel: {
                        action: function () {}
                    },
                    confirm: {
                        text: 'DELETE',
                        btnClass: 'btn-red',
                        action: function () {
                            $.ajax({
                                data: {
                                    '_token': $('input[name=_token]').val(),
                                    'ids': list_id,
                                },
                                url: "master-ptkp/destroy-all",
                                type: "POST", 
                                dataType: "JSON",
                                success: function(data) {
                                    if(data.status) {
                                        var options = { 
                                            "positionClass": "toast-bottom-right", 
                                            "timeOut": 1000, 
                                        };
                                        toastr.success('Berhasil hapus data!', 'Success Alert', options);
                                        reload_table();
                                    } else {
                                        $.alert({
                                            type: 'red',
                                            icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                            title: 'Warning',
                                            content: 'Hapus data gagal!',
                                        });
                                        location.reload()
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    $.alert({
                                        type: 'red',
                                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                        title: 'Warning',
                                        content: 'Hapus data gagal!',
                                    });
                                    location.reload()
                                }
                            });
                        }
                    }
                }
            });
        } else {
            $.alert({
                type: 'orange',
                icon: 'fa fa-warning', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'Tidak ada data yang dipilih!',
            });
        }
    }
</script> 

<!-- Add fancyBox -->
<link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
</script>
@stop