@extends('layouts.editor.template')

@section('content')
<style type="text/css">
    .my_class {
        background-color: white;
    }
    table.payslip-table td {
        padding: 5px;
    }
    .text-bold {
        font-weight: bold;
    }
    .text-center {
        text-align: center;
    }
    .border-top-bottom {
        border-top: 1px solid black; 
        border-bottom: 1px solid black;
    }
</style>


<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
    <h4>Hasil Perhitungan Gaji</h4>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Hasil Perhitungan Gaji</li>
    </ol>
</section>

<section class="content">
    <div class="row"> 
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
                    <button onClick="history.back()" type="button" class="btn btn-danger btn-flat"> 
                        <i class="fa fa-undo"></i> Kembali
                    </button>
                    @actionStart('payrollhasilperhitungan', 'read')
                    <a class="btn btn-success" target="_blank" href="{{ url('editor/payroll/calculation/payslip/result/detail-payslip/' . $periodId . '/download') }}">Unduh File</a>
                    @actionEnd
                    @actionStart('payrollhasilperhitungan', 'create')
                    <a class="btn btn-success" onclick="send_detail_payslip('{{ $periodId }}')">Kirim Payslip</a>
                    @actionEnd
                </div>
                @actionStart('payrollhasilperhitungan', 'read')
                <div class="box-header">
                    <div class="box-body">
                        <table id="dtTable" class="table table-bordered table-hover stripe">
                            <input type="hidden" id="period_id" value="{{$periodId}}">
                            <thead>
                                <th>Aksi</th>
                                <th>Periode</th>
                                <th>Nama</th>
                                <th>Status PTKP</th>
                                <th>No. BPJS Kesehatan</th>
                                <th>No. BPJS Jamsostek</th>
                                <th>NPWP</th>
                                <th>Gaji Pokok</th>
                                <th>Tunjangan Makan Tetap</th>
                                <th>Tunjangan Makan Proyek</th>
                                <th>Uang Lembur</th>
                                <th>Uang Lembur Proyek</th>
                                <th>Tunjangan Keahlian</th>
                                <th>Tunjangan Kesehatan</th>
                                <th>Klaim</th>
                                <th>TM, TLK, Inst</th>
                                <th>Tunjangan Lain</th>
                                <th>Tunjangan Jamsostek</th>
                                <th>Potongan Jamsostek</th>
                                <th>PPh 21</th>
                                <th>Gaji Dibayar di Muka</th>
                                <th>Pinjaman</th>
                                <th>Gaji Bersih</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                @actionEnd
            </div>
        </div>
    </div>
</section> 

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="payslip-title"></h4>
            </div>
            <div class="modal-body" id="payslip-content"></div>
            <div class="modal-footer">
                <input type="hidden" id="payslip-id">
                <button class="btn btn-success" onclick="download()">Unduh Payslip</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
    var table;
    $(document).ready(function() {
        table = $('#dtTable').DataTable({ 
            processing: true,
            serverSide: true,
            "pageLength": 25,
            "scrollY": "600px",
            "scrollX": true,
            "sScrollXInner": "150%",
            "order": [[ 0, "asc" ]],
            "autoWidth": true,
            "rowReorder": true,
            fixedColumns:   {
                leftColumns: 3
            },
            "aoColumnDefs": [
                { "sClass": "my_class", "aTargets": [ 0, 1, 2, 3 ] }
            ],
            "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]], 
            ajax: {
                "url"    : "{{ url('editor/payroll/calculation/payslip/result/detail/data') }}",
                "data"   : function( d ) {
                  d.period_id= $('#period_id').val();
                }
            },
            columns: [
                { data: 'action', name: 'action', orderable: true, searchable: true },
                { data: 'date_format', name: 'date_format', orderable: true, searchable: true },
                { data: 'employee_name', name: 'employee_name', orderable: true, searchable: true },
                { data: 'ptkp_status', name: 'ptkp_status', orderable: true, searchable: true },
                { data: 'health_insurance_number', name: 'health_insurance_number', orderable: true, searchable: true },
                { data: 'employment_insurance_number', name: 'employment_insurance_number', orderable: true, searchable: true },
                { data: 'tax_number', name: 'tax_number', orderable: true, searchable: true },
                { data: 'gaji_pokok', name: 'gaji_pokok', orderable: true, searchable: true },
                { data: 'tunjangan_makan_tetap', name: 'tunjangan_makan_tetap', orderable: true, searchable: true },
                { data: 'tunjangan_makan_proyek', name: 'tunjangan_makan_proyek', orderable: true, searchable: true },
                { data: 'uang_lembur', name: 'uang_lembur', orderable: true, searchable: true },
                { data: 'uang_lembur_proyek', name: 'uang_lembur_proyek', orderable: true, searchable: true },
                { data: 'tunjangan_keahlian', name: 'tunjangan_keahlian', orderable: true, searchable: true },
                { data: 'tunjangan_kesehatan', name: 'tunjangan_kesehatan', orderable: true, searchable: true },
                { data: 'klaim', name: 'klaim', orderable: true, searchable: true },
                { data: 'tm_tlk_isnst', name: 'tm_tlk_isnst', orderable: true, searchable: true },
                { data: 'tunjangan_lain', name: 'tunjangan_lain', orderable: true, searchable: true },
                { data: 'tunjangan_jamsostek', name: 'tunjangan_jamsostek', orderable: true, searchable: true },
                { data: 'potongan_jamsostek', name: 'potongan_jamsostek', orderable: true, searchable: true },
                { data: 'pph_21', name: 'pph_21', orderable: true, searchable: true },
                { data: 'pre_salary', name: 'pre_salary', orderable: true, searchable: true },
                { data: 'loan', name: 'loan', orderable: true, searchable: true },
                { data: 'gaji_bersih', name: 'gaji_bersih', orderable: true, searchable: true }
            ]
        });
        
        //check all
        $("#check-all").click(function () {
            $(".data-check").prop('checked', $(this).prop('checked'));
        });
    });

    function reload_table() {
        table.ajax.reload(null,false); //reload datatable ajax 
    }

    function delete_id(id, name) {
        var name = name.bold();

        $.confirm({
            title: 'Confirm!',
            content: 'Apakah anda yakin untuk menghapus Payslip ' + name + ' ?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                cancel: {
                    action: function () {}
                },
                confirm: {
                    text: 'DELETE',
                    btnClass: 'btn-red',
                    action: function () {
                        $.ajax({
                            url : 'result/' + id,
                            type: "DELETE",
                            data: {
                                '_token': $('input[name=_token]').val() 
                            },
                            success: function(data) { 
                                var options = { 
                                    "positionClass": "toast-bottom-right", 
                                    "timeOut": 1000, 
                                };
                                toastr.success('Berhasil hapus data!', 'Success Alert', options);
                                reload_table();
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.alert({
                                    type: 'red',
                                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                                    title: 'Warning',
                                    content: 'Gagal hapus data!',
                                });
                            }
                        });
                    }
                }
            }
        });
    }

    function detail_payslip(payslipId) {
        $.ajax({
            url : '../detail-payslip/' + payslipId,
            type: "GET",
            success: function(data) { 
                $("#payslip-title").html(data.title)
                $("#payslip-content").html(data.content)
                $("#payslip-id").val(payslipId)
            }       
        });
    }

    function download() {
        payslipId = $('#payslip-id').val();

        $.ajax({
            url : '../detail-payslip/' + payslipId + '/payslip-download',
            type: "GET",
            success: function(data) { 
                var options = { 
                    "positionClass": "toast-bottom-right", 
                    "timeOut": 1000, 
                };
                window.location = data.url;
                waitingDialog.hide();
                toastr.success('Berhasil mengunduh data!', 'Success Alert', options);
            }       
        });
    }

    function send_detail_payslip(payslipId) {
        $.ajax({
            url : '../detail-payslip/' + payslipId + '/send',
            type: "GET",
            success: function(data) { 
                if (!data.result) alert('Gagal mengirim email payslip');
                else alert('Berhasil mengirim email payslip');
            }       
        });
    }

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });

    var waitingDialog = waitingDialog || (function ($) {
        'use strict';

        var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
              '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
              '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
              '</div>' +
            '</div></div></div>');

        return {
            show: function (message, options) {
                // Assigning defaults
                if (typeof options === 'undefined') {
                    options = {};
                }
                if (typeof message === 'undefined') {
                    message = 'Loading';
                }
                var settings = $.extend({
                    dialogSize: 'm',
                    progressType: '',
                    onHide: null // This callback runs after the dialog was hidden
                }, options);

                // Configuring dialog
                $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
                $dialog.find('.progress-bar').attr('class', 'progress-bar');
                if (settings.progressType) {
                    $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
                }
                $dialog.find('h3').text(message);
                
                // Adding callbacks
                if (typeof settings.onHide === 'function') {
                    $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                        settings.onHide.call($dialog);
                    });
                }
                // Opening dialog
                $dialog.modal();
            },
            hide: function () {
                $dialog.modal('hide');
            }
        };

    })(jQuery);
</script> 
@stop