@extends('layouts.editor.templatereport')

@section('content')
    <html>
        <head>
            <style>
                .bold { font-weight: bold; }
                .text-center { text-align: center; }

                .font-size-12 { font-size: 10px; }
                .font-size-14 { font-size: 14px; }
                .font-size-16 { font-size: 16px; }

                .pl3 { padding-left: 3px; }
                .p3 { padding: 3px; }

                .m5 { margin: 5px; }

                .border-top { border-top: 1px solid #000; }
                .border-bottom { border-bottom: 1px solid #000; }
            </style>
        </head>
        <body id="main-body">
            <div style="width: 100%; height: auto;">
                @foreach ($resultCalculation as $index => $result)
                    <div style="width:340px; height: 720px; border: 1px solid #000; margin:10px; box-sizing: border-box; display: inline-block;">
                        <table style="width:100%" class="font-size-12">
                            <tr>
                                <td colspan="6" class="bold font-size-16 p3 border-bottom">{{ $result['other_information']['title'] }}</td>
                            </tr>
                            <tr><td></td></td>
                            <tr>
                                <td style="width: 20%;" class="pl3">Nama</td>
                                <td style="width: 1%;" class="pl3">:</td>
                                <td style="width: 29%;" class="pl3">{{ $result['other_information']['employee_name'] }}</td>
                                <td style="width: 15%;" class="pl3">Jabatan</td>
                                <td style="width: 1%;" class="pl3">:</td>
                                <td style="width: 34%;" class="pl3">{{ $result['other_information']['jobtitle'] }}</td>
                            </tr>
                            <tr>
                                <td class="pl3">Grade</td>
                                <td class="pl3">:</td>
                                <td class="pl3">{{ $result['other_information']['grade'] }}</td>
                                <td class="pl3">Status</td>
                                <td class="pl3">:</td>
                                <td class="pl3">{{ $result['other_information']['tax_status'] }}</td>
                            </tr>
                            <tr>
                                <td class="pl3">Periode UM & Transport</td>
                                <td class="pl3">:</td>
                                <td class="pl3">{{ $result['other_information']['meal_trans_period_start'] .'-' . $result['other_information']['meal_trans_period_end'] }}</td>
                                <td class="pl3">Pembayaran</td>
                                <td class="pl3">:</td>
                                <td class="pl3">{{ $result['other_information']['pay_period'] }}</td>
                            </tr>

                            <tr><td></td></td>
                            <tr>
                                <td colspan="6" class="font-size-14 p3 border-bottom border-top text-center">PENGHASILAN</td>
                            </tr>
                            <tr><td></td></td>
                            
                            <tr>
                                <td class="pl3" colspan="4">Gaji Pokok</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['income']['basic_salary']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Tunjangan Jabatan</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['income']['occupation_allowance']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Uang Makan & Transport</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['income']['meal_transport_allowance']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Lembur</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['income']['overtime']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Insentif, TLK, TM</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['income']['insentif']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Tunjangan Kesehatan</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['income']['medical_allowance']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Tunj. Jamsostek</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['income']['bpjs_allowance']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Pendapatan Lainnya</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['income']['other_income']) }}</td>
                            </tr>

                            <tr>
                                <td colspan="5" class="pl3 bold">GAJI BRUTO</td>
                                <td class="pl3 bold">Rp {{ number_format($result['bruto_income']) }}</td>
                            </tr>

                            <tr><td></td></td>
                            <tr>
                                <td colspan="6" class="font-size-14 p3 border-bottom border-top text-center">POTONGAN</td>
                            </tr>
                            <tr><td></td></td>
                            
                            <tr>
                                <td class="pl3" colspan="4">Pinjaman Karyawan</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['deduction']['employee_loan']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Pinjaman Asuransi</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['deduction']['insurance_loan']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Pot Uang Makan, Transport</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['deduction']['meal_trans_deduction']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Pot Insentif, TLK, TMLM</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['deduction']['insentif_deduction']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Pot Tunjangan Kesehatan</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['deduction']['medical_deduction']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">PPh Pasal 21</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['deduction']['tax']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Potongan Jamsostek</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['deduction']['bpjs_deduction']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Potongan Pensiun</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['deduction']['retire_deduction']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Pot Lainnya</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['deduction']['other_deduction']) }}</td>
                            </tr>
                            <tr>
                                <td colspan="5" class="pl3 bold">JUMLAH POTONGAN</td>
                                <td class="pl3 bold">Rp {{ number_format($result['total_deduction']) }}</td>
                            </tr>

                            <tr><td></td></td>
                            <tr>
                                <td colspan="6" class="border-bottom"></td>
                            </td>
                            <tr>
                                <td colspan="5" class="pl3 bold">GAJI BERSIH</td>
                                <td class="pl3 bold">Rp {{ number_format($result['take_home_pay']) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" class="border-bottom"></td>
                            </td>
                            <tr><td></td></td>

                            <tr>
                                <td class="pl3" colspan="4">Sisa Hutang Pinjaman Karyawan</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['other']['hutang']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">JHT dby Perusahaan 3.7%</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['other']['bpjs_paid_by_company']) }}</td>
                            </tr>
                            <tr>
                                <td class="pl3" colspan="4">Tj Pensiunan dby Perusahaan 2%</td>
                                <td class="pl3">:</td>
                                <td class="pl3">Rp {{ number_format($result['other']['retire_paid_by_company']) }}</td>
                            </tr>
                            
                            <tr>
                                <td class="pl3 text-center" colspan="2"><img src="{{ asset('laravel/storage/payroll-report/mgr_finance.jpeg') }}" width="70px"></td>
                                <td class="pl3 text-center" style="padding-left:20px" colspan="2"><img src="{{ asset('laravel/storage/payroll-report/finance.jpeg') }}" width="70px"></td>
                                <td class="pl3 text-center" colspan="2"><img src="{{ asset('laravel/storage/payroll-report/hrd.jpeg') }}" width="70px"></td>
                            </tr>
                            <tr>
                                <td class="pl3 text-center" colspan="2">Mgr Finance</td>
                                <td class="pl3 text-center" colspan="2">Finance</td>
                                <td class="pl3" style="padding-left:30px" colspan="2">HRD</td>
                            </tr>
                        </table>
                    </div>
                @endforeach
            </div>
        </body>
    <html>
@stop