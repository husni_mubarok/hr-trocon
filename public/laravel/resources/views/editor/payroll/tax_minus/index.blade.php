@extends('layouts.editor.template')

@section('content')
<style type="text/css">
    .my_class {
        background-color: white;
    }
</style>


<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
    <h4>Laporan Pengembalian Pajak</h4>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li><a href="#">Payroll</a></li>
        <li class="active">Laporan Pengembalian Pajak</li>
    </ol>
</section>

<section class="content">
    <div class="row"> 
        <div class="col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
                    <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Period">
                        <!-- <div class="col-sm-12 pull-right">
                            <select class="form-control" id="periodid" onchange="filterData();" name="periodid">
                                @foreach($yearFilter AS $year)
                                    @if ($year === $currentYear)
                                    <option value="{{$year}}" selected>{{$year}}</option>
                                    @else
                                        <option value="{{$year}}">{{$year}}</option>
                                    @endif
                                @endforeach
                            </select> 
                        </div> -->
                    </div>
                    <button onClick="history.back()" type="button" class="btn btn-danger btn-flat"> 
                        <i class="fa fa-undo"></i> Kembali
                    </button>
                    @actionStart('payrolltaxminus', 'read')
                    <button class="btn btn-success" onclick="download()">Unduh File</button>
                    @actionEnd
                </div>
                @actionStart('payrolltaxminus', 'read')
                <div class="box-header">
                    <div class="box-body">
                        <table id="dtTable" class="table table-bordered table-hover stripe">
                            <thead>
                                <th>Nama Staff</th>
                                <th>Periode</th>
                                <th>Total Pajak Yang Belum Dikembalikan</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                @actionEnd
            </div>
        </div>
    </div>
</section>  
@stop

@section('scripts')
<script>
    var table;
    $(document).ready(function() {
        table = $('#dtTable').DataTable({ 
            processing: true,
            serverSide: true,
            "pageLength": 25,
            "scrollY": "360px",
            "rowReorder": true,
            "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
            ajax: "{{ url('editor/payroll/calculation/payslip/tax-minus/data') }}",
            columns: [  
                { data: 'employee_name', name: 'employee_name', orderable: true, searchable: true }, 
                { data: 'periode', name: 'periode', orderable: true, searchable: true },
                { data: 'total', name: 'total', orderable: true, searchable: true }
            ]
        });
        
        //check all
        $("#check-all").click(function () {
            $(".data-check").prop('checked', $(this).prop('checked'));
        });
    });

    function download() {
        $.ajax({
            url : "{{url('editor/payroll/calculation/payslip/tax-minus/download/')}}",
            type: "GET",
            data: {},
            success: function(data) { 
                var options = { 
                    "positionClass": "toast-bottom-right", 
                    "timeOut": 1000, 
                };
                window.location = data.url;
                waitingDialog.hide();
                toastr.success('Berhasil mengunduh data!', 'Success Alert', options);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.alert({
                    type: 'red',
                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                    title: 'Warning',
                    content: 'Hapus data gagal!',
                });
            }
        });
    }

    var waitingDialog = waitingDialog || (function ($) {
        'use strict';

        var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
              '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
              '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
              '</div>' +
            '</div></div></div>');

        return {
            show: function (message, options) {
                // Assigning defaults
                if (typeof options === 'undefined') {
                    options = {};
                }
                if (typeof message === 'undefined') {
                    message = 'Loading';
                }
                var settings = $.extend({
                    dialogSize: 'm',
                    progressType: '',
                    onHide: null // This callback runs after the dialog was hidden
                }, options);

                // Configuring dialog
                $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
                $dialog.find('.progress-bar').attr('class', 'progress-bar');
                if (settings.progressType) {
                    $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
                }
                $dialog.find('h3').text(message);
                
                // Adding callbacks
                if (typeof settings.onHide === 'function') {
                    $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                        settings.onHide.call($dialog);
                    });
                }
                // Opening dialog
                $dialog.modal();
            },
            hide: function () {
                $dialog.modal('hide');
            }
        };

    })(jQuery);
</script> 
@stop