@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->

 <style type="text/css">

.toolbar {
    float: left;
}
th,td  {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top: -10px; margin-bottom: -10px">
  <h1>
    <i class="fa fa-usd"></i> PIB
    <small>Master Data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">PIB</li>
  </ol>
</section>

<section class="content">
{{ csrf_field() }}
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
           <button onClick="add()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Add New</button>
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Back</button>
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <button class="btn btn-danger btn-flat" onclick="generate()"><i class="fa fa-magic"></i> Generate Container</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th>Action</th> 
                  <th>No Container</th> 
                  <th>No PIB/AJU</th>
                  <th>Plant</th>
                  <th>Port</th>
                  <th>Nama EMKL</th>
                  <th>Shipping Line</th>
                  <th>Cost PIB/EMKL</th> 
                  <th>Description</th>
                  <th>ETA</th>
                  <th>ATA</th>
                  <th>Original Doc</th>
                  <th>Pajak PIB</th>
                  <th>KT2</th>
                  <th>Inspect/KT9</th>
                  <th>Tgl Respon</th>
                  <th>Status Respon</th>
                  <th>Delivery Date</th>
                  <th>Delivery Time</th>
                  <th>Remarks</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:50% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
          <div class="form-group pull-right">
            <label for="real_name" class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8 pull-right">
              <select class="form-control" style="width: 100%;" name="status"  id="status">
               <option value="0">Active</option>
               <option value="1">Not Active</option>
             </select>
           </div>
         </div>
         <h3 class="modal-title">PIB Form</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row">
            <div class="form-group">
              <label class="control-label col-md-3">Container</label>
              <div class="col-md-8">
                <select class="form-control" style="width: 100%;" name="container_id"  id="container_id">
                   @foreach($container_list as $container_lists)
                    <option value="{{$container_lists->id}}">{{$container_lists->container_no}}</option>
                   @endforeach
                 </select>
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">PIB No</label>
              <div class="col-md-8">
                <input name="no_pib" id="no_pib" class="form-control" type="text">
                <small class="errorDaily PIBName hidden alert-danger"></small> 
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">PIB Cost</label>
              <div class="col-md-8">
                <input name="cost_pib" id="cost_pib" class="form-control" type="text">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Nama EMKL</label>
              <div class="col-md-8">
                <input name="emkl" id="emkl" class="form-control" type="text">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Shipping Line</label>
              <div class="col-md-8">
                <input name="shipping_line" id="shipping_line" class="form-control" type="text">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Description</label>
              <div class="col-md-8">
                <input name="description" id="description" class="form-control" type="text">
              </div>
            </div> 
           <div class="form-group">
              <label class="control-label col-md-3">ETA</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
                    <input name="eta" id="eta" class="form-control" type="text">
                </div>
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">ATA</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
                    <input name="ata" id="ata" class="form-control" type="text">
                </div>
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Original Doc</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
                    <input name="original_doc" id="original_doc" class="form-control" type="text">
                </div>
              </div>
            </div> 

            <div class="form-group">
              <label class="control-label col-md-3">Pajak PIB</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
                    <input name="pajak_pib" id="pajak_pib" class="form-control" type="text">
                </div>
              </div>
            </div> 

            <div class="form-group">
              <label class="control-label col-md-3">KT 2</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
                    <input name="kt2" id="kt2" class="form-control" type="text">
                </div>
              </div>
            </div> 

            <div class="form-group">
              <label class="control-label col-md-3">Inspect KT 9</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
                    <input name="inspect_kt9" id="inspect_kt9" class="form-control" type="text">
                </div>
              </div>
            </div> 
            
             <div class="form-group">
              <label class="control-label col-md-3">Tgl Respon</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
                    <input name="tlg_respon" id="tlg_respon" class="form-control" type="text">
                </div>
              </div>
            </div> 
             <div class="form-group">
              <label class="control-label col-md-3">Status Respon</label>
              <div class="col-md-8">
                <select name="status_respon" id="status_respon" class="form-control">
                  <option value="Green">Green</option>
                  <option value="Yellow">Yellow</option>
                  <option value="Red">Red</option>
                </select> 
              </div>
            </div> 
             <div class="form-group">
              <label class="control-label col-md-3">Delivery Date</label>
              <div class="col-md-8">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
                    <input name="delivery_date" id="delivery_date" class="form-control" type="text">
                </div>
              </div>
            </div> 
              <div class="form-group">
              <label class="control-label col-md-3">Delivery Time</label>
              <div class="col-md-8">
                <input name="delivery_time" id="delivery_time" class="form-control" type="time">
              </div>
            </div> 
            <div class="form-group">
              <label class="control-label col-md-3">Remarks</label>
              <div class="col-md-8">
                <input name="remarks" id="remarks" class="form-control" type="text">
              </div>
            </div> 
          </div>
        </div>
      </div>
    </form>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
      <button type="button" id="btnSaveAdd" onClick="saveadd()" class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-plus-square"></i> Save & Add</button>
      <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Save & Close</button>

    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "sScrollX": true,
         "scrollY": "360px",
         "sScrollXInner": "150%",
         fixedColumns:   {
          leftColumns: 6
         },
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/pib/data') }}",
         columns: [  
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'container_no', name: 'container_no' },
         { data: 'no_pib', name: 'no_pib' },
         { data: 'branch_name', name: 'branch_name' },
         { data: 'store_location_name', name: 'store_location_name' },
         { data: 'emkl', name: 'emkl' },
         { data: 'shipping_line', name: 'shipping_line' },
         { data: 'cost_pib', name: 'cost_pib' },
         { data: 'description', name: 'description' },
         { data: 'eta', name: 'eta' },
         { data: 'ata', name: 'ata' },
         { data: 'original_doc', name: 'original_doc' },
         { data: 'pajak_pib', name: 'pajak_pib' },
         { data: 'kt2', name: 'kt2' },
         { data: 'inspect_kt9', name: 'inspect_kt9' },
         { data: 'tlg_respon', name: 'tlg_respon' },
         { data: 'status_respon', name: 'status_respon' },
         { data: 'delivery_date', name: 'delivery_date' },
         { data: 'delivery_time', name: 'delivery_time' },
         { data: 'remarks', name: 'remarks' },
         { data: 'mstatus', name: 'mstatus' }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }
 
       function add()
      {
        $("#btnSave").attr("onclick","save()");
        $("#btnSaveAdd").attr("onclick","saveadd()");
 
        $('.errorDaily PIBName').addClass('hidden');
        $('.errorDaily PIBNo').addClass('hidden');

        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Daily PIB'); // Set Title to Bootstrap modal title
      }

      function save()
      {   

        var url;
        url = "{{ URL::route('editor.pib.store') }}";
        
        $.ajax({
          type: 'POST',
          url: url,
          data: { 

            '_token': $('input[name=_token]').val(), 
            'container_id': $('#container_id').val(),
            'no_pib': $('#no_pib').val(), 
            'emkl': $('#emkl').val(), 
            'shipping_line': $('#shipping_line').val(), 
            'cost_pib': $('#cost_pib').val(), 
            'description': $('#description').val(), 
'eta': $('#eta').val(), 
            'ata': $('#ata').val(), 
            'original_doc': $('#original_doc').val(), 
            'pajak_pib': $('#pajak_pib').val(), 
            'kt2': $('#kt2').val(), 
            'inspect_kt9': $('#inspect_kt9').val(), 
            'tlg_respon': $('#tlg_respon').val(), 
            'status_respon': $('#status_respon').val(), 
            'delivery_date': $('#delivery_date').val(), 
            'delivery_time': $('#delivery_time').val(), 
            'remarks': $('#remarks').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
 
            $('.errorDaily PIBName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
              
              if (data.errors.pibname) {
                $('.errorDaily PIBName').removeClass('hidden');
                $('.errorDaily PIBName').text(data.errors.pibname);
              }
            } else {

              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully added data!', 'Success Alert', options);
              $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };

      function saveadd()
      {   
       $.ajax({
        type: 'POST',
        url: "{{ URL::route('editor.pib.store') }}",
        data: {
          '_token': $('input[name=_token]').val(), 
          'container_id': $('#container_id').val(),
          'no_pib': $('#no_pib').val(), 
          'cost_pib': $('#cost_pib').val(), 
          'emkl': $('#emkl').val(), 
          'shipping_line': $('#shipping_line').val(), 
          'description': $('#description').val(), 
'eta': $('#eta').val(), 
          'ata': $('#ata').val(), 
          'original_doc': $('#original_doc').val(), 
          'pajak_pib': $('#pajak_pib').val(), 
          'kt2': $('#kt2').val(), 
          'inspect_kt9': $('#inspect_kt9').val(), 
          'tlg_respon': $('#tlg_respon').val(), 
          'status_respon': $('#status_respon').val(), 
          'delivery_date': $('#delivery_date').val(), 
          'delivery_time': $('#delivery_time').val(), 
          'remarks': $('#remarks').val(), 
          'status': $('#status').val()
        },
        success: function(data) {  
            $('.errorDaily PIBName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
            
              if (data.errors.rate) {
                $('.errorDaily PIBName').removeClass('hidden');
                $('.errorDaily PIBName').text(data.errors.rate);
              }
            } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully added data!', 'Success Alert', options);
            $('#form')[0].reset(); // reset form on modals
            reload_table(); 
          } 
        },
      })
     };

     function edit(id)
     { 

      $('.errorDaily PIBName').addClass('hidden');

      //alert("asdad");

      $("#btnSave").attr("onclick","update("+id+")");

      $("#btnSaveAdd").attr("onclick","updateadd("+id+")");

      save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : 'pib/edit/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {


            $('[name="id_key"]').val(data.id); 
            $('[name="container_id"]').val(data.container_id);
            $('[name="no_pib"]').val(data.no_pib);
            $('[name="cost_pib"]').val(data.cost_pib);
            $('[name="emkl"]').val(data.emkl);
            $('[name="shipping_line"]').val(data.shipping_line);
            $('[name="description"]').val(data.description);
$('[name="eta"]').val(data.eta);
            $('[name="ata"]').val(data.ata);
            $('[name="original_doc"]').val(data.original_doc);
            $('[name="pajak_pib"]').val(data.pajak_pib);
            $('[name="kt2"]').val(data.kt2);
            $('[name="inspect_kt9"]').val(data.inspect_kt9);
            $('[name="tlg_respon"]').val(data.tlg_respon);
            $('[name="status_respon"]').val(data.status_respon);
            $('[name="delivery_date"]').val(data.delivery_date);
            $('[name="delivery_time"]').val(data.delivery_time);
            $('[name="status"]').val(data.status);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Daily PIB'); // Set title to Bootstrap modal title
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
      }

      function update(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'pib/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'container_id': $('#container_id').val(), 
            'no_pib': $('#no_pib').val(), 
            'cost_pib': $('#cost_pib').val(), 
            'emkl': $('#emkl').val(), 
            'shipping_line': $('#shipping_line').val(), 
            'description': $('#description').val(), 
'eta': $('#eta').val(), 
            'ata': $('#ata').val(), 
            'original_doc': $('#original_doc').val(), 
            'pajak_pib': $('#pajak_pib').val(), 
            'kt2': $('#kt2').val(), 
            'inspect_kt9': $('#inspect_kt9').val(), 
            'tlg_respon': $('#tlg_respon').val(), 
            'status_respon': $('#status_respon').val(), 
            'delivery_date': $('#delivery_date').val(), 
            'delivery_time': $('#delivery_time').val(), 
            'remarks': $('#remarks').val(), 
            'status': $('#status').val()
          },
          success: function(data) {  
            $('.errorDaily PIBName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
             
              if (data.errors.pibname) {
                $('.errorDaily PIBName').removeClass('hidden');
                $('.errorDaily PIBName').text(data.errors.pibname);
              }
            } else {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
            $('#modal_form').modal('hide');
              $('#form')[0].reset(); // reset form on modals
              reload_table(); 
            } 
          },
        })
      };

      function updateadd(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'pib/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'container_id': $('#container_id').val(), 
            'no_pib': $('#no_pib').val(), 
            'cost_pib': $('#cost_pib').val(), 
            'emkl': $('#emkl').val(), 
            'shipping_line': $('#shipping_line').val(), 
'eta': $('#eta').val(), 
            'ata': $('#ata').val(), 
            'original_doc': $('#original_doc').val(), 
            'pajak_pib': $('#pajak_pib').val(), 
            'kt2': $('#kt2').val(), 
            'inspect_kt9': $('#inspect_kt9').val(), 
            'tlg_respon': $('#tlg_respon').val(), 
            'status_respon': $('#status_respon').val(), 
            'delivery_date': $('#delivery_date').val(), 
            'delivery_time': $('#delivery_time').val(), 
            'remarks': $('#remarks').val(), 
            'description': $('#description').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
            if ((data.errors)) {
             swal("Error!", "Gat data failed!", "error")
           } else { 
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
              $('#form')[0].reset(); // reset form on modals
              reload_table(); 
              $("#btnSave").attr("onclick","save()");
              $("#btnSaveAdd").attr("onclick","saveadd()");
            } 
          },
        })
      };

      function delete_id(id, pibname, date)
      {
        //var varnamre= $('#pibname').val();
        var pibname = pibname.bold();
        var date = date.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete ' + pibname + ' in ' + date + ' data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : 'pib/delete/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error deleteing data!',
                });
              }
            });
           }
         },

       }
     });
      }


      function generate(id)
      {

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to generate container data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'GENERATE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : 'pib/generate',
              type: "POST",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully generate container data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error generating data!',
                });
              }
            });
           }
         },

       }
     });
      }

      window.onload= function(){ 

        n2= document.getElementById('cost_pib');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        }

      }
   </script> 

   <!-- Add fancyBox -->
   <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
   <script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
   <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox").fancybox();
    });
  </script>
  @stop
