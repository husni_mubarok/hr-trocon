@extends('layouts.editor.template')
@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-question"></i> Popup
    <small>Popup</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{url('/')}}/editor"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Popup</a></li>
    <li class="active">Popup</li>
  </ol>
</section>
@actionStart('popup', 'create|update')
<section class="content">
	<div class="col-md-8 col-sm-8 col-xs-8"> 
		<section class="content box box-solid">
			<div class="row">
			    <div class="col-md-12 col-sm-12 col-xs-12">
			    	<div class="col-md-1"></div>
			    	<div class="col-md-12">
			        <div class="x_panel">
		                <h2>
		                	@if(isset($popup))
		                	<i class="fa fa-pencil"></i>
		                	@else
		                	<i class="fa fa-plus"></i>
		                	@endif
		                	&nbsp;Popup
	                	</h2>
		                <hr>
			            <div class="x_content">
			                @include('errors.error')

			                @if(isset($popup))
			                {!! Form::model($popup, array('route' => ['editor.popup.update', $popup->id], 'method' => 'PUT', 'files' => 'true'))!!}
		                    @else
		                    {!! Form::open(array('route' => 'editor.popup.store', 'files' => 'true'))!!}
		                    @endif
		                    {{ csrf_field() }}
		                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
		                    	{{ Form::label('popup_name', 'Popup Name') }} 
		                    	{{ Form::text('popup_name', old('popup_name'), ['class' => 'form-control']) }}
		                    	<br>

		                    	{{ Form::label('description', 'Content') }}
		                    	<textarea id="description" name="description" rows="10" cols="80">@if(isset($popup)) {{$popup->description}} @endif</textarea>
		                    	<br>
		                    	<br>

		                    	 <div class="form-group">
					              {{ Form::label('description', 'Date') }}
					                <div class="input-group">
					                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
					                    <input name="date" id="date" class="form-control" type="text">
					              </div>
					            </div> 

	                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
	                    	</div>
	                        {!! Form::close() !!}
				        </div>
			        </div>
			    </div>
			</div>
		</section>
	</div>
</section>
@actionEnd
@stop

@section('scripts')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>  
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('description');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });
</script>
@stop





