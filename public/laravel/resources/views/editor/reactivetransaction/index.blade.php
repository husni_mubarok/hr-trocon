@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top: -10px; margin-bottom: -10px">
  <h1>
    <i class="fa fa-money"></i> Reactive Transaction
    <small>Master Data</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Reactive Transaction</li>
  </ol>
</section>

<section class="content">
{{ csrf_field() }}
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Back</button>
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <!-- <button class="btn btn-danger btn-flat" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Bulk Delete</button> -->
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th>Action</th>  
                  <th>No Cont</th>
                  <th>PO No</th>
                  <th>GR Date</th>
                  <th>Item</th>
                  <th>Plant</th>
                  <th>Port</th>
                  <th>Qty GR</th>
                  <th>Qty Order</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  
@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/reactivetransaction/data') }}",
         columns: [  
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'container_no', name: 'container_no' },
         { data: 'po_number', name: 'po_number' },
         { data: 'gr_date', name: 'gr_date' },
         { data: 'item_name', name: 'item_name' },
         { data: 'plant', name: 'plant' },
         { data: 'store_location_name', name: 'store_location_name' },
         { data: 'gr_qty', name: 'gr_qty' },
         { data: 'order_qty_po', name: 'order_qty_po' } 
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
      function reload_table()
      {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function delete_id(id)
      {

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to reactive ?',
          type: 'green',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'REACTIVE',
            btnClass: 'btn-green',
            action: function () {
             $.ajax({
              url : 'reactivetransaction/delete/'+id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully reactiveing data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error reactiving data!',
                });
              }
            });
           }
         },
       }
     });
    } 
   </script> 

   <!-- Add fancyBox -->
   <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
   <script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
   <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox").fancybox();
    });
  </script>
  @stop
