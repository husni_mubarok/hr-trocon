 @extends('layouts.editor.template')
 @section('title', 'Reimburse')
 @section('content')
 <style type="text/css">


  #detailModals .modal-dialog
  {
    width: 60%;
  }
  th { font-size: 13px; }
  td { font-size: 12px; }
</style>
<!-- Content Wrapper. Contains page content --> 
<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    <i class="fa fa-adjust"></i> Reimburse Karyawan
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Time & Payroll</a></li>
    <li class="active">Reimburse Karyawan</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        {!! Form::model($reimburse, array('route' => ['editor.reimburse.update', $reimburse->id], 'method' => 'PUT', 'class'=>'form-horizontal', 'files' => 'true', 'id'=>'form_employee'))!!}
        {{ csrf_field() }}
        <!--  Hidden element -->
        <input type="hidden" value="" id="idtrans" name="idtrans">
        <input type="hidden" value="" id="status" name="status">
        <div class="box-header with-border">
          <div class="row">
            <!-- Coloumn 1-->
            <div class="col-md-4">
              <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Trans No</label>
                <div class="col-sm-9">  
                  {{ Form::text('notrans', old('notrans'), array('class' => 'form-control', 'placeholder' => 'No Trans*', 'required' => 'true', 'id' => 'notrans', 'disabled' => 'disabled')) }}
                </div>
              </div>
              <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Trans Date</label>
                <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('datetrans', old('datetrans'), array('class' => 'form-control', 'placeholder' => 'Date Trans*', 'required' => 'true', 'id' => 'datetrans', 'onclick' => 'saveheader();')) }}
                </div><!-- /.input group --> 
              </div>
            </div> 
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Jenis Medikal</label>
              <div class="col-sm-9"> 
                {{ Form::select('medicaltypeid', $reimbursetype_list, old('medicaltypeid'), array('class' => 'form-control select2', 'placeholder' => 'Pilih Jenis Medikal', 'id' => 'medicaltypeid', 'onchange' => 'RefreshData();')) }} 
              </div>
            </div>
            <div class="form-group">
              <label for="real_name" class="col-sm-3 control-label">Periode</label>
              <div class="col-sm-9"> 
                {{ Form::select('periodid', $period_list, old('periodid'), array('class' => 'form-control select2', 'placeholder' => 'Pilih Periode', 'id' => 'periodid', 'onchange' => 'RefreshData();')) }} 
              </div>
            </div>
          </div>
          <!-- Coloumn 2-->   
          <div class="col-md-4">
           <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Karyawan</label>
              <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>  
                    {{ Form::select('employeeid', $employee_list, old('employeeid'), array('class' => 'form-control select2', 'placeholder' => 'Pilih Karyawan', 'id' => 'employeeid', 'required' => 'true', 'onchange' => 'RefreshData();')) }}
                </div><!-- /.input group --> 
              </div>
          </div>  
           <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Departemen</label>
              <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>  
                    {{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control select2', 'placeholder' => 'Pilih Departemen', 'id' => 'departmentid', 'onchange' => 'RefreshData();')) }}
                </div><!-- /.input group --> 
              </div>
          </div>  

           <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Nama Pasien</label>
              <div class="col-sm-9"> 
                  {{ Form::text('patientname', old('patientname'), array('class' => 'form-control', 'placeholder' => 'Nama Pasien*', 'required' => 'true', 'id' => 'patientname', 'onclick' => 'saveheader();')) }}
              </div>
          </div>  
           
        </div>
        <!-- Coloumn 3-->                                   
        <div class="col-md-4">  
            <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Plafond</label>
              <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon">Rp.</span>  
                  {{ Form::text('plafond', old('plafond'), array('class' => 'form-control', 'placeholder' => 'Plafond*', 'required' => 'true', 'id' => 'plafond', 'onkeyup' => 'cal_sparator_plafond();')) }}
                </div><!-- /.input group --> 
              </div>
          </div>
         <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Digunakan</label>
              <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon">Rp.</span>  
                  {{ Form::text('used', old('used'), array('class' => 'form-control', 'placeholder' => 'Digunakan*', 'required' => 'true', 'id' => 'used', 'onkeyou' => 'cal_sparator_used();')) }}
                </div><!-- /.input group --> 
              </div>
          </div>
      <div class="form-group">
            <label for="real_name" class="col-sm-3 control-label">Sisa</label>
              <div class="col-sm-9">
                 <div class="input-group">
                  <span class="input-group-addon">Rp.</span>  
                  {{ Form::text('remain', old('remain'), array('class' => 'form-control', 'placeholder' => 'Digunakan*', 'required' => 'true', 'id' => 'remain', 'onkeyup' => 'cal_sparator_remain();')) }}
                </div><!-- /.input group --> 
              </div>
          </div>
      </div>
    </div><!-- /.box-header -->

    <hr>
               
    <div class="box-body">
      <div class="col-md-2" style="margin-right: 5px">
        <div class="form-group">
          <label>Keterangan</label> 
            {{ Form::text('description', old('description'), array('class' => 'form-control', 'placeholder' => 'Keterangan*', 'id' => 'description', 'onclick' => 'saveheader();')) }}
        </div> 
      </div> 

      <div class="col-md-2" style="margin-right: 5px">
        <div class="form-group">
          <label>Tanggal Klaim</label>
          <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
              {{ Form::text('dateclaim', old('dateclaim'), array('class' => 'form-control', 'placeholder' => 'Tanggal Klaim*', 'id' => 'dateclaim', 'onclick' => 'saveheader();')) }}
            </div><!-- /.input group --> 
        </div> 
      </div>

      <div class="col-md-2" style="margin-right: 5px">
        <div class="form-group">
          <label>No Ref</label>
          {{ Form::text('noref', old('noref'), array('class' => 'form-control', 'placeholder' => 'No Ref*', 'id' => 'noref', 'onclick' => 'saveheader();')) }}
        </div> 
      </div>

      <div class="col-md-2" style="margin-right: 5px">
        <div class="form-group">
          <label>Total</label>
          {{ Form::text('amount', old('amount'), array('class' => 'form-control', 'placeholder' => 'Total*', 'id' => 'amount', 'onclick' => 'saveheader();')) }}
        </div> 
      </div>
  
      <div class="col-md-2" style="margin-right: 5px">
        <div class="form-group">
         <label>Action</label><br/>
         <a href="#" onclick="savedetail();" type="button" class="btn btn-primary btn-flat" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-plus"></i> Add</a>
       </div>   
     </div> 
   </div>  
   <div class="box-body">  
     <table id="dtTable" class="table table-bordered table-hover stripe">
      <thead>
       <tr>  
        <th>Keterangan</th> 
        <th>Tanggal Klaim</th>  
        <th>No Ref</th>
        <th>Total</th>
        <th>Action</th> 
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table> 
</div>  
<hr style="margin-top: -10px">   
<div class="box-header with-border">
  <div class="col-md-6">
    <div class="form-group">
      <label for="real_name" class="col-sm-3 control-label">Remark</label>
      <div class="col-sm-9">
        <textarea style="height: 40px !important" class="form-control" id="remark" name="remark" onchange="saveheader();"> {{$reimburse->remark}}</textarea>
      </div>
    </div>
  </div>

  <button type="submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Save</button>   
  <a href="#" class="btn btn-primary btn-flat pull-right" name="saveclose" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow'); saveheader();" data-toggle="modal" data-easein="swoopIn" data-target=".MyModals"><i class="fa fa-print"></i> Print</a>
  <a href="#" class="btn btn-danger btn-flat pull-right" name="saveclose" style="margin-left: 2px; margin-right: 2px" onclick="GenerateData();"><i class="fa fa-magic"></i> Generate</a>
  <!-- <a class="btn btn-warning btn-flat pull-right" name="cancel" style="margin-left: 2px; margin-right: 2px" onclick="javascript:$('#dg').edatagrid('saveRow'); canceltransaction();" ><i class="fa fa-minus-square"></i> Cancel</a> -->
  <a href="{{ URL::route('editor.reimburse.index') }}" type="button" class="btn btn-danger btn-flat pull-right" style="margin-left: 2px; margin-right: 2px"> <i class="fa fa-close"></i> Close</a>
  <iframe src='' height="0" width="0" frameborder='0' name="print_frame"></iframe>

</div>
{!! Form::close() !!}
</div>
</section><!-- /.content -->  

<!-- Modal Popup -->
<div class="modal fade MyModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog" style="width:400px !important;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Print Out</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="form-control" id="idperiod" name="idperiod">
        <input type="hidden" class="form-control" id="type" name="type">
        <select multiple class="form-control" id="slipid" class="slipid" value="{{$reimburse->id}}">
          <option ondblclick="showslip()" value="reimburse">SPK</option>
        </select>
      </div>
      <div class="modal-footer">
        <a href="#" onclick="showslip();" class="btn btn-primary btn-flat"> <i class="fa fa-search"></i> Preview</a> 
        <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Popup -->
<div class="modal fade detailModals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" id="detailModals" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
       <h3> <i class="fa fa-user"></i> Employee Master</h3>
     </div>
     <div class="modal-body">
      <table id="detailTable" class="table table-bordered table-hover stripe">
        <thead>
         <tr>  
           <th>#</th>
           <th>Keterangan</th>
           <th>Tanggal Klaim</th>    
           <th>No Ref</th>
           <th>Total</th> 
           <th>Action</th>   
         </tr>
       </thead>
       <tbody>
       </tbody>
     </table> 
   </div>
   <div class="modal-footer">  
    <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
  </div>
</div>
</div>
</div>
@stop

@section('scripts')

<script type="text/javascript">

  var table;
  $(document).ready(function() {
        @if(isset($day_work->day_work))
        @if($day_work->day_work <= 183)
        $.alert({
            title: 'Reimburse ditolak!',
            content: 'Masa kerja kurang 6 bulan!',
            type: 'red',
            typeAnimated: true,
        });
        @endif
        @endif

        @if(isset($reimburse->remain_val))
        @if($reimburse->remain_val < 0)
        $.alert({
            title: 'Reimburse ditolak!',
            content: 'Julmah claim melebihi plafond!',
            type: 'red',
            typeAnimated: true,
        });
        @endif
        @endif

        //datatables
        table = $('#dtTable').DataTable({ 
          processing: true,
          serverSide: true,
          "pageLength": 25,
          "scrollY": "120px",
          "rowReorder": true,
          "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
          ajax: "{{ url('editor/reimburse/datadetail') }}/{{$reimburse->id}}",
          columns: [   
          { data: 'description', name: 'description' },
          { data: 'dateclaim', name: 'dateclaim' }, 
          { data: 'noref', name: 'noref' }, 
          { data: 'amount', name: 'amount' }, 
          { data: 'action', name: 'action', orderable: false, searchable: false }
          ]
        });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        }); 
      });
      function reload_table()
      {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function add()
      {
       $("#btnSave").attr("onclick","save()");
       $("#btnSaveAdd").attr("onclick","saveadd()");

       $('.errorMaterial UsedName').addClass('hidden');

       save_method = 'add'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Asset Request'); // Set Title to Bootstrap modal title
      } 

      function reload_table_detail()
      {
        table_detail.ajax.reload(null,false); //reload datatable ajax 
      }

      function saveheader(id)
      {
        save_method = 'update';  

        //Ajax Load data from ajax
        $.ajax({
          url: '../../reimburse/saveheader/{{$reimburse->id}}' ,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'medicaltypeid': $('#medicaltypeid').val(),
            'employeeid': $('#employeeid').val(),
            'departmentid': $('#departmentid').val(),
            'patientname': $('#patientname').val(),
            'plafond': $('#plafond').val(),
            'used': $('#used').val(),
            'remain': $('#remain').val(),
            'remark': $('#remark').val()
          },
          success: function(data) {  
            if ((data.errors)) { 
              toastr.error('Data is required!', 'Error Validation', options);
            } 
          },
        })
      };

      function savedetail(id)
      {
        var employeeid = $("#employeeid").val();
        save_method = 'update';  

        if(employeeid == '')
        {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.error('Employee data is required!', 'Error Validation', options);
        }else{
            //Ajax Load data from ajax
            $.ajax({
              url: '../../reimburse/savedetail/{{$reimburse->id}}' ,
              type: "PUT",
              data: {
                '_token': $('input[name=_token]').val(), 
                'description': $('#description').val(),
                'dateclaim': $('#dateclaim').val(),
                'noref': $('#noref').val(),
                'amount': $('#amount').val(),
              },
              success: function(data) {

              
               var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully add detail data!', 'Success Alert', options);

              if ((data.errors)) { 
                toastr.error('Data is required!', 'Error Validation', options);
              } 
              reload_table();

              $('#description').val(''),
              $('#dateclaim').val(''),
              $('#noref').val(''),
              $('#amount').val('')
            },
          })
        }
      };

      function delete_id(id, employeename)
      {
        //alert("asdasd");
        //var varnamre= $('#employeename').val();
        var employeename = employeename.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : '../../reimburse/deletedet/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error deleteing data!',
                });
              }
            });
           }
         },

       }
     });
      }

      function cal_sparator_plafond() {  
        var plafond = document.getElementById('plafond').value;
        var result = document.getElementById('plafond');
        var rsamount = (plafond);
        result.value = rsamount.replace(/,/g, "");  


        n2= document.getElementById('plafond');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='plafond')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }


      function cal_sparator_used() {  
        var used = document.getElementById('used').value;
        var result = document.getElementById('used');
        var rsamount = (used);
        result.value = rsamount.replace(/,/g, "");  


        n2= document.getElementById('used');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='used')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }


      function cal_sparator_remain() {  
        var remain = document.getElementById('remain').value;
        var result = document.getElementById('remain');
        var rsamount = (remain);
        result.value = rsamount.replace(/,/g, "");  

        n2= document.getElementById('remain');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          if(who.id==='remain')  temp= validDigits(who.value,0); 
          else temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        } 
      }
    </script>

     <script type="text/javascript">
      function GenerateData()
      {
        // var periodid = $('#periodidfilter').val();
        // var perioddesc = $("#periodidfilter option:selected").text();
        saveheader();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to generate data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'CREATE',
            btnClass: 'btn-red',
            action: function () { 
             $.ajax({
              url : '../../reimburse/generate/' + {{ $reimburse->id }},
              type: "POST",
              data: {
                '_token': $('input[name=_token]').val(), 
                'medicaltypeid': $('#medicaltypeid').val(),
                'employeeid': $('#employeeid').val(),
                'departmentid': $('#departmentid').val(),
                'patientname': $('#patientname').val(),
                'plafond': $('#plafond').val(),
                'used': $('#used').val(),
                'remain': $('#remain').val(),
                'periodid': $('#periodid').val(),
                'remark': $('#remark').val()
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully genareted data!', 'Success Alert', options);
                reload_table();
                location.reload();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error generate data!',
                });
              }
            });
           }
         },

       }
     });
      }
    </script>

    <script type="text/javascript">
      function showslip()
      {
         var slipid = $("#slipid").value;
         var url = "{{ URL::route('editor.reimburse.slip', $reimburse->id) }}";
         PopupCenter(url,'Popup_Window','700','650');
      }

      function PopupCenter(url, title, w, h) {  
        // Fixes dual-screen position                         Most browsers      Firefox  
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
        var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
        var top = ((height / 2) - (h / 2)) + dualScreenTop;  
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
        
        // Puts focus on the newWindow  
        if (window.focus) {  
          newWindow.focus();  
        }  
      } 
    </script>
    @stop
