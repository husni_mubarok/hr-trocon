@extends('layouts.editor.template')
@section('title', 'Reimburse')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
   <i class="fa fa-clock-o"></i> Reimburse Karyawan
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Time & Payroll</a></li>
    <li class="active">Reimburse Karyawan</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border">
          <button onClick="add()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Tambah Baru</button>
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Kembali</button>
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>#</th> 
                  <th>Action</th> 
                  <th>No Trans</th> 
                  <th>Date Trans</th> 
                  <th>Karyawan</th>
                  <th>Departemen</th>
                  <th>Jenis Medikal</th>
                  <th>Periode</th>
                  <th>Nama Pasien</th>
                  <th>Plafond</th>
                  <th>Dipakai</th>
                  <th>Sisa</th>
                  <th>Remark</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

<!-- Modal Popup -->
<div class="modal fade modal_form"  id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog" style="width:420px !important;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Select the transaction code</h4>
        </div>
        {!! Form::open(array('route' => 'editor.reimburse.store', 'class'=>'form-horizontal', 'files' => 'true', 'id'=>'form_floor'))!!}
        {{ csrf_field() }}
            <div class="modal-body">
               <input type="hidden" class="form-control" id="idperiod" name="idperiod">
               <input type="hidden" class="form-control" id="type" name="type">
               <div class="form-group">
                  <label for="real_name" class="col-sm-3 control-label">Transaction</label>
                  <div class="col-sm-8">
                    <select class="form-control"  style="width: 100%;" name="codetrans" id="codetrans"  placeholder="Transaction Code">
                      <option value="RMBS">Reimburse Karyawan</option> 
                  </select>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary-ghci btn-flat"><i class="fa fa-plus"></i> Submit</button>
        <input type="hidden" value="1" name="submit" />
        <button type="button" onclick="ClearVal()" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
    </div>

      {!! Form::close() !!}
</form>
</div>

</section>
</div>
</div>

@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "scrollX": true,
         "order": [[ 0, "desc" ]],
         "sScrollXInner": "150%",
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/reimburse/data') }}",
         columns: [  
         { data: 'id', name: 'id' },
         { data: 'action', name: 'action', orderable: false, searchable: false },  
         { data: 'notrans', name: 'notrans' },  
         { data: 'datetrans', name: 'datetrans' },
         { data: 'employeename', name: 'employeename' },
         { data: 'departmentname', name: 'departmentname' },
         { data: 'medicaltypename', name: 'medicaltypename' },
         { data: 'description', name: 'description' },
         { data: 'patientname', name: 'patientname' },
         { data: 'plafond', name: 'plafond' },
         { data: 'used', name: 'used' },
         { data: 'remain', name: 'remain' },
         { data: 'remark', name: 'remark' }, 
         { data: 'mstatus', name: 'mstatus' }
         ]
       });

         //auto number
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();


        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function add()
      {
        $("#btnSave").attr("onclick","save()");
        $("#btnSaveAdd").attr("onclick","saveadd()");
 
        $('.errorMaterial UsedName').addClass('hidden');

        save_method = 'add'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Reimburse'); // Set Title to Bootstrap modal title
      }

       
      function bulk_cancel()
      {


        var list_id = [];
        $(".data-check:checked").each(function() {
          list_id.push(this.value);
        });
        if(list_id.length > 0)
        {


          $.confirm({
            title: 'Confirm!',
            content: 'Are you sure to cancel '+list_id.length+' data?',
            type: 'orange',
            typeAnimated: true,
            buttons: {
              cancel: {
               action: function () {

               }
             },
             confirm: {
              text: 'SUBMIT',
              btnClass: 'btn-orange',
              action: function () {
               $.ajax({
                 data: {
                  '_token': $('input[name=_token]').val(),
                  'idkey': list_id,
                }, 
                url: "reimburse/deletebulk",
                type: "POST", 
                dataType: "JSON",
                success: function(data)
                {
                  if(data.status)
                  {


                    reload_table();
                  }
                  else
                  {
                    alert('Failed.');
                  }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                  $.alert({
                    type: 'red',
                                icon: 'fa fa-warning', // glyphicon glyphicon-heart
                                title: 'Warning',
                                content: 'Error canceling data!',
                              });
                }
              });
             }
           },

         }
       });
        }
        else
        {
         $.alert({
          type: 'orange',
                icon: 'fa fa-warning', // glyphicon glyphicon-heart
                title: 'Warning',
                content: 'No data selected!',
              });
       }
     }
   </script> 
   @stop
