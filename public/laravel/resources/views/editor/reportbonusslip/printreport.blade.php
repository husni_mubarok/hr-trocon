@extends('layouts.editor.templatereport')
@section('content')
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
 
</style> 
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 15">
<link rel=File-List href="Report%20Insentif_files/filelist.xml">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<style id="Report (Autosaved)_19996_Styles">
<!--table
  {mso-displayed-decimal-separator:"\,";
  mso-displayed-thousand-separator:"\,";}
.xl1519996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6319996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6419996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border:.5pt solid windowtext;
  background:#D6DCE4;
  mso-pattern:black none;
  white-space:nowrap;}
.xl6519996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6619996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6719996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6819996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6919996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7019996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7119996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7219996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:16.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7319996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7419996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7519996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7619996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
-->
</style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="Report (Autosaved)_19996" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=1246 style='border-collapse:
 collapse;table-layout:fixed;width:937pt'>
 <col width=19 style='mso-width-source:userset;mso-width-alt:694;width:14pt'>
 <col width=37 style='mso-width-source:userset;mso-width-alt:1353;width:28pt'>
 <col width=116 style='mso-width-source:userset;mso-width-alt:4242;width:87pt'>
 <col width=232 style='mso-width-source:userset;mso-width-alt:8484;width:174pt'>
 <col width=93 span=5 style='mso-width-source:userset;mso-width-alt:3401;
 width:70pt'>
 <col width=100 style='mso-width-source:userset;mso-width-alt:3657;width:75pt'>
 <col width=74 style='mso-width-source:userset;mso-width-alt:2706;width:56pt'>
 <col width=90 style='mso-width-source:userset;mso-width-alt:3291;width:68pt'>
 <col width=113 style='mso-width-source:userset;mso-width-alt:4132;width:85pt'>
 <tr class=xl6619996 height=7 style='mso-height-source:userset;height:5.25pt'>
  <td height=7 width=19 style='height:5.25pt;width:14pt' align=left valign=top><!--[if gte vml 1]><v:shapetype
   id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
   path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="Picture_x0020_1" o:spid="_x0000_s8193" type="#_x0000_t75"
   style='position:absolute;margin-left:7.5pt;margin-top:1.5pt;width:43.5pt;
   height:36.75pt;z-index:1;visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQBamK3CDAEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhvROFGcle3tSdZNgokh7Rjb3+8vyWK5tSObIJBxWPPbsuIM
UDltsK/5x/qleOCMokQtR4dQ8x0QXzbXV4v1zgOxRCPVfIjRPwpBagArqXQeMHU6F6yM6Rh64aX6
lD2Iu6q6F8phBIxFzBm8WbTQyc0Y2fM2lWcTjz1nT/NcXlVzYzOf6+JPIsBIJ4j0fjRKxnQ3MaE+
8SoOTmUi9zM0GE83SfzMhtz57fRzwYF7S48ZjAa2kiG+SpvMhQ4kvFFxEyBNlf/nZFFLhes6o6Bs
A61m8ih2boF2XxhgujS9Tdg7TMd0sf/X5hsAAP//AwBQSwMEFAAGAAgAAAAhAAjDGKTUAAAAkwEA
AAsAAABfcmVscy8ucmVsc6SQwWrDMAyG74O+g9F9cdrDGKNOb4NeSwu7GltJzGLLSG7avv1M2WAZ
ve2oX+j7xL/dXeOkZmQJlAysmxYUJkc+pMHA6fj+/ApKik3eTpTQwA0Fdt3qaXvAyZZ6JGPIoiol
iYGxlPymtbgRo5WGMqa66YmjLXXkQWfrPu2AetO2L5p/M6BbMNXeG+C934A63nI1/2HH4JiE+tI4
ipr6PrhHVO3pkg44V4rlAYsBz3IPGeemPgf6sXf9T28OrpwZP6phof7Oq/nHrhdVdl8AAAD//wMA
UEsDBBQABgAIAAAAIQDXOoyQZwIAAKYFAAASAAAAZHJzL3BpY3R1cmV4bWwueG1srFTbjtMwEH1H
4h8sv7dxQrJtoyar0u4ipNVSIfgAr+M0Fokd2e5lhfh3xnayVVUQiOVt4vHMOZ5zJsvbU9eiA9dG
KFngeEow4pKpSshdgb9+uZ/MMTKWyoq2SvICP3ODb8u3b5anSudUskZpBC2kyeGgwI21fR5FhjW8
o2aqei4hWyvdUQufehdVmh6heddGCSE3kek1p5VpOLebkMGl722Pas3bdhUgeCXsyhQYOLjT4U6t
VRduM9WWZBk5Ui70HSD4VNflIkuyc8qd+KxWx7HCheOZy8cLMlZAylf4zmc4q14gyuTXsHGcviO/
wR1KrnDnZDHLQrsL4BGuFyzgysNWsK0eSDwethqJqsAJRpJ2oBJk7V5zFOPofCdU0By6PCj2zQy6
0X9QraNCApZaN1Tu+Mr0nFlwj0MLGgClAOc/L+g+taK/Fy2IRHMXv5pGsN9fmU/VtWB8o9i+49IG
B2reUgvuN43oDUY6590Th2Hqj1WMEQPzW5hor4W0YDua85N9MHaI0F6LAn9P5itCFsn7yToj60lK
ZneT1SKdTWbkbpaSdB6v4/UPVx2n+d5wGD9tN70Ynx6nVxp0gmllVG2nTHVR4D3uDvCOSRQ0ONC2
wMQP3lMDAc4UIXQTdlyN1dyyZkS8wvvzpno816oG8T6D4E7sl8aD8Gdx3Sqa3nmU5qdad/8DGcaA
TgX2G43RMzjObap7vH8zYpDMsiSFM8Qgnd7czJJsGI4j4S722tgPXL2aEHKNwCYwCe8LegBbhJmM
EMNQwhj8JsDuDQvZCnDghlo67szFD2+oDD/Y8icAAAD//wMAUEsDBBQABgAIAAAAIQCqJg6+vAAA
ACEBAAAdAAAAZHJzL19yZWxzL3BpY3R1cmV4bWwueG1sLnJlbHOEj0FqwzAQRfeF3EHMPpadRSjF
sjeh4G1IDjBIY1nEGglJLfXtI8gmgUCX8z//PaYf//wqfillF1hB17QgiHUwjq2C6+V7/wkiF2SD
a2BSsFGGcdh99GdasdRRXlzMolI4K1hKiV9SZr2Qx9yESFybOSSPpZ7Jyoj6hpbkoW2PMj0zYHhh
iskoSJPpQFy2WM3/s8M8O02noH88cXmjkM5XdwVislQUeDIOH2HXRLYgh16+PDbcAQAA//8DAFBL
AwQUAAYACAAAACEANmKyIxUBAACHAQAADwAAAGRycy9kb3ducmV2LnhtbEyQTU/DMAyG70j8hyhI
3FjSQctalk4TCGniMGkDDtyi1mkrmmRKsq7j1+N1TOVk+eN5/drzRa9b0oHzjTWCRhNOCZjClo2p
BP14f72bUeKDNKVsrQFBj+DpIr++msustAezgW4bKoIixmdS0DqEXcaYL2rQ0k/sDgz2lHVaBkxd
xUonDyiuWzblPGFaNgY31HIHzzUU39u9xr3L9fFltY+lSjoHCbx9ftWbSIjbm375REmAPozDf/Sq
FHRKT6fgGTRHf327NEVtHVEb8M0Pmj/XlbOaOHsQFI8tbDtEzNdKeQg4lfL43LlU0niKFXYSDfaM
4qoBxfgfnfH0MR5aFzaKHu75ALPRUj7HZPxf/gsAAP//AwBQSwMECgAAAAAAAAAhAI0MNmeGGgAA
hhoAABQAAABkcnMvbWVkaWEvaW1hZ2UxLnBuZ4lQTkcNChoKAAAADUlIRFIAAABOAAAAQwgGAAAB
RTMDVQAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L/GEFAAAACXBIWXMAACHVAAAh1QEEnLSdAAAa
G0lEQVRoQ+2bCVhTx77AD7KEfVWQRVAURAXUal1aLdX22rq2vVfb+nrVV2tdURQQhSSc7GHfXXBD
7Sp1qwsQIGQ/2SEiasW2ttper7TV3rbWaoW8mZMJEAghKH297333933/LzNz5pzM+c/2n//MwQaM
J676DQXt42/79fFYSq0RRbGEvZq/oyCGBTDEt1EQw9adNmKpdcad5772QylduPObH8BfX1zxGZYs
ML58QDMZ2/hp51M7cWFffEQGwJMqjUZHmBn+fQRD+B6ZbmbM5lYKCvZiYqHIFwVtM3evdg4KWjKS
K+OMy1HUouggsLZc54yCmCNV8zsKmnjjiCEaBTFsa3VvtfRkHFtCm5ij5EI1oSTMaDQOQcFugAxH
Dbc8sC1Vxpm5wudg0iuHGheQ18w476h/iCWDOgaZyCdCpQM2V7UOIzMMiBlF6v9CQfs41vKjvy+9
QRDBUryJkvrHP1N0zJMmfuTLUFVMKVClzShRpQVxNPs8MsQPQjiyoyibbdwzJPdQ0JIt1UZvmuQr
FLPAky69hoKWJJ27Og4FLZiXX+NPahdqNk3cf+WacWBesGwmEFg1SdXGxWXq18mHkvGaXg8dVyCb
gIJduPMukL2wE1giKNvrOybskoyI26cK6y4ol4nEmhgU6iKQJRajIHh/0G5B23VJEX7vsv70XbIx
wYHD/CcwDNP6A/S9XfA3ElcEuuwU3p6/TzcXB23bOa2eTWZAgPbu4M/RXoXhALrkFpnYF1OLNE/7
sdR6FLUKeKCzN1vTMpKvIPvJ/y6zd2nneTG133gyVXpvuoj2DGjQPvSGNB+mRunG0n+dUNY0CWXt
m9h81cfrTl0ZiaKdjGLW92q0Cyournhuly4JRe1j4R4ilKxpK7x+1BCLgpb40kTWFQ+bA2jAlHTR
LyjFgmFs+WgU7CKQJb2Cgp0k8IXLgunianN7S6gQuaJLnXjRxHdRsItxudLpKNjFhtPG+aVEXFfj
re/1uo6ZxJco2MU7+1v8UZBkBEt+khzNN583LijXryD76zaBMbWyZTjKQuJDF36Egl3glS0uKGgC
dSEHNDaTutt8zoitPWFZumRBGgp1sazbwzzo0q7+COjZ6eP45zrn7ymFqmMo2MXTpeoAFOws1RCg
I+/Umm/I1zXrDf52ayquGeJeFYfFFahegb9DkgTt2FYwjplv2ACMA/gwsgKQAN0Np9Z+Ay87cy50
WR5m3KkND1FwQETlKAtQsIu5u/XkbDSWr0iClRHAJD4fRhfpXzv6eaAnrrwBr00u0uf4UhuuUtIa
3hmyvZ60rCYWNlk3Poaz5SoUtIsRfOI8ClrHiy5ve2G35kUUtcrEfOXbfizFzyhqH3P3NL0Wl0cI
fGgNAj+akLvyI23vfjiYVLW2Ujjnrk9y31E1GUOy4silyRXXjb368aAzuVQfEsZXnXdlqP8VzpFX
Lv+geSa6NCBml+mfC+QQArdM4re4AjVvVcX1xy/8pAL1mnAeUYOi/RKBC89gW84b5+1RTURJNhGJ
jE5+TMXV9ScMgSjJPhbuuzA7mCU9jKL9EpYpMpAdGY5bYAD8S3njfHSpXzxpkl/Gl4k8UdQ2U4CB
7UKV/Iqi/bL8UGMUOdrAgpmHLCDP7dInoyw2SQD9PDpbUYqitonJURWNy1XQUNQWDtjak79Hpldr
cFGLJ5aCxtFuw+DYbOUBlNcmTswm2zaRGReqrGMcWxGBolbZduyGG7YVaAq0MSzxrNF7u+CbZZWV
jo7bG+5ZDPhAAugiBbqtTwJoDZem728OQlEbMBsfYN3WTj0p14FrcIY2G8PbQJUmVRn9ttddh9f9
MkS3sW1gYoECqxu8gMvWmu/Im/tgcr6iIopnxTLoiSun6UGviRoBRubI7tVGCihkGFv2CcpCEok3
FPimVZf7pgLZdqbcN/FE+Yhtpw6iy70YklK7Y8mBpoUoagO6/gFYdvbS3JQi9bJeBQPVFpunpE/i
CyeCZZ9pSQGmW1OVwuumPCYt1hgdtwpMy/Qe+NBE5xbvU/deevQEGAOPxuRYmndRfEW2aZhAAgsA
bIJ5+3SLns2Vzcc2niGrliwELEzPl0AvAu9xAFVdWWl0RI8mcaBprltfJPdgZqn+zZFZquMoio3M
qD2FbTrThm051wbakOk38fx38/eqpybwhMux1Z+0YetOg3RwbaugDWirDbTJ3pICBVzbWtOGbTrX
NqtAFon+AvOlCm22SQvc6fLWVadMdsbGU5dWBOGif0RwZXrYFj2ooh9GcCSGmCzpHrzm2jO+DJk6
hKu+vqPuC5+4YmJNEENSF5zd9CO41SEoW3czkCk6PJQpUcJnubB17Qm4yMkxXdYO45A5uzVLQnjS
Lv+EPXhSZfdREHu6WL151sHWYbBwQWxZgchodIrJIT5548jl6DC2XB7JJ5rwmhv+i/drVwXhktpQ
LtEC7wN5a0JYsuOjc7VkZwjIbHhrRon22UC6mDRnNp28HOCSIbNfa92JylF9OowpvU3cAOPaIBPC
kl4EzUeHok/GKrCaC+dLP3ZnaX/wYSi+G87TfjScIVkXiIte9Eivj5++Sxk/C4gvCIPp6AVgt60J
4ioL/dhqnWt6Q4crQ9cSwZOzufVf2DHY/oc/mfijAg8wc3RaweP4dZPZwksRYNyyGMv+UHDB54Fh
bGmaL0enccuU/+rDVN4O4GirJxTo94WxJNuhawJKdJY8bQSPyBvKVZ/2YhI3XemyX31YGuX4XFUi
jtsx0A6EmGzVcp9M6UNXXG2IzVclFqtavdElu8knbviP4smpbizN10MZisvPlOqnokuPx+kr33lR
MhTto3iyEpRkkxF0YZkc3IOifdLSYnQJ4xNXwXz62yorzoz+MRodXDLkv6z+UB+CUmwC2peLw5Zq
ow9N8jlK6pf4IkXsUIastyOkP6aW6N+YVaYvQtF+8afWi80T//xD2jiU3C9+uLz1pQMXxqKofbil
i9sXlevcUdQmy8qAiQ60RnougG3nkibsnDf7Y/3Zy7EBTEtb0CarKluGe1Ab/oGi/eKdInhAeonM
dhwwj6YVqlaiy/3itlPY2/3cF/HAZI4vVG5A0X7x31l3o3P1hWw3l50iq0alNQJwmd1rY8w9Q3yP
IDrsmujj8NqPoQnksK2mo6eROYorK0bZbBKVJX93w/HmTtvOJo50ZQcK2iSKVpOCvfOJEa66gujC
U+SCp1vhgIEJPWcOptx9s/S9xvFjsyXbUbRvcJHIyR1XtqGoTRw3n32IrT9l9NxadQea3Y5pwh6F
qzOO4MgaUfY+wYFt6E+X2PZ7Qebu18eHIavVFjFc6XFyzQpdpWD9MCtbmjCGJ9lKVq25euFvsqAj
objHlocVKBnSZhTsG7+M+vlPFxJlKGqVl99TeZO+XLiYgf4RUEhKcs0jOHBTdjY8NC2CyIKR1wOo
9eR61hbunItfoGDfjM2SU8dw5V2buFYIZogNSCumAsKeuvm88ZVDmhEL9zdPIdPMBYcDc+I54+xc
ic1dEye67iYK2iCphvrXIxdXoFgvFh4wxHa2KbKAQEAhwlldw4FfuvC2yTsOqn0TWDKCdumR+OkP
6LJVPJj6OyjYN0GZDblL37/UZ+H8cemXnYXr3q4qujzC2fIrXhar/SQgG0+UJ3+g6729hrCrcFhy
DfWVIwarhVt0oGkeuYLvXjiguRnFxPGXi6socaDh99oG6CYT8iQj0KN6Q2+yvh3anVmlqkwPmtjq
tjBlp/iuRcGA1py215GOfPeUulayKtH82uWKAEJWPUgH8298tjSTfFgPfLNa+rdmxmYpXhrFlZSj
aCcv7FZvIKsQFcrc3sZnK3ZsPt0yHjZ6aw7Ezvyo52JJAuPq01csbD64d+mFSwwo2jdrjn02yo8u
uoCiJGt1Rmcsua7D9MdAUC+lpNaRu9nOiaDRw/EO/jm83rNw3QsIXmA0S3KSfDBi2zHCLYJnp2Xi
hmssDpRMyFKUkEMC/HNYPWiIiMuSzZ/Kql2CrTtJOhD71JxZzNoD1Y+Lrnd2oIR9TZOis2TvoKht
nKiKzu0n+FYOidXfYZvOIkcOEPDrkVJLDqwuaz5pw9aebMM2nAHXatpAwUwOm74cOcngelJ1m3dK
jZT8A0AIR7J+/YnP7fOoD2dJiFePNCeg6B9OAFNejYL9s/Bg40R/mvAiimIxuSq1E13RMbVQfTq2
tPnISK5U4c/T3cdFRqdQLnErIFNcOjZP0wSatoMPQ34/gC7+5OlSdfLzuw2gc8lbhvF0N14s1Yf4
4ApiaqGmJDpLOQvbXDUePR6YaAPcunNKlzx6ubjrDJD7TiF5tiE2T3nYly7Sjc5R34ceJ29qwxGY
Dv5Yt2CfelIYW/Q2jAMcRvFV5PEFuLgezZOzfaiimrg85Z5ZJbqNYDwlB2Rc9NnQYUyFCIbtZs5u
/eq4fHUuimIjWJKf4O/s/c374G90loIorqqiROWoa4czpR9H56oq4BGHEJ5aH86Vn4kpMrwZV0A8
FcUntMFsWfOyMvXwkTzZOXhvKFd9eShu8scFs6UNk/KJgTch1wzpv1Z91PucwGCxrFLn407t43BN
f8CjUo4ZRPuCigv/jZIGjcX7DS8PyVA+evGILhwlDRw4lISwZMUUXP1LTDZRnF9zw2LjfyCs/eCz
mFFc6cduDOW9YJ56L37mW7uWnnYxqUj7ji9LrXdLl/7uw1QpRvEUOeNyidenFavmhOL1pOMQ/Hl8
fJFyVihHsnAkV54Wytef8mXKf3Bjqu76MKTnn9ulWYwe98ey+pNLk2PzlVuGZkp2hXOkAngkLIQp
FcRkK4/NKNKw33rv8ryq1o4+T/39h//wRwBMtWVlIs9ni3ThU/MUsIkuGJ+nTg7mEWWhfI3Aj0k0
BeDS7512in50zxC1U3YCSWtod00TWYjLDlG7Wzq8Lr7vRZf+6MeQtw5jEw1BXG15XKGaFpMtXRTG
VcSCvhm29ox9/qd/K2g1X06cWKDKH8YQ33TG1Q8d8KZH7pnyjmG45KtIPnH02T3axBUnP5tF7kSs
1TmDqdUJTBSOJk+xETougMDfnkI6NRwwHB+CwbxwW29tuXNWnc5n0aELT83eq9s0Jkvx8TBc9A8X
GtGO4bpHlAzZPTCxCl88oF/R0/b+U1l67IYbPJPjTpXLKTTiX764/J8jc7QH4abshBzt6FyBwQNl
HTCvH9I+9bf92udRdOCAlr78g8+GxuSppkwo0GQMZRJaN1CBHgzV9agc5Z43P7zU/8buYPNfHzT7
RWepjrvQlPcj+Mqzr79/xfqR7cdkTrFqiley4JbjVoExPkuSDr1e6NITAz228QWaVG+OutmdLv/X
zF16s+35xzI+n/D3ZchFXjTpz4knrtp1ImggvFiiWE6B/kboYgGLuyHJNQ8j2XJeAm4cNOWRgC4f
m69Kd06XPRidpWDCFoquDD5wXzqMLZMD4+f6nN2age0a2EEcX77GJVlwj3RowNUvXN0m1xgdUus7
4gqUFkvwwQA6imNziVxHquzX6Bw5dM0PvvJwUEOhHHmuU7r83oQcwu7NBXuZXahkOG2paTd5gYDC
oNfR7DYgXQf1xuFsov7lg42P8alC30AHTWSW8owXXXYzAK+1aw9tQDxTqFziTJd/E8GVnoOH8VDy
oDCO25DksBUoBzq+ST9LN6VZ+FnqjYFshXRz1cC3Um0Rn6sa5ZEp//LZvYa/oqTBIQIXuQZkNjR7
ZNQ/BDNVnx7Tx+FGh2mDaPlhw0pn5FYmlUb6HXsqziReVOnPSaevDNpQAVe+oRxZfgRPehG0wMHb
QJ9/sOltZzphDGYrLD+jegLWvKcKC0w6dYWy9vjdKbhgKUybmCN7jZIm/t6yi/ZWHBRPuuSrGSXa
aeTDBoFJBcSLrgzNnTmlmiUo6ckJ5xISsMz/bVKBinzBJ+WZHOFo/9TzBvLY/rvHjc7rTrU/m92Q
Aq8tKlfO8siQ3LXV4sziSRX//MqhC7PJhz4h0MRyZBm+DmWJ96CkJyOjui3YJV30my9NfHVsofyJ
u8fKj1pH++8Q3CWd2tB3vAnKp0Zs46cdo3HhXlx03fW1o4ZAP5roAlCcad/WmgJRusNO0UPSh/yE
5gScJCjpklYPqkiNn+ryUT82f9mrnefKufgogCY+FJynG4qSH4uZRepFlB3CuyYHPBQwg5q3xRLP
GR0Sq9vHMMWnYN6Vx5pCg3FxS2fLsyawS4PZ1yVV8BCshQf22ZAVPDKE1b685tYlAz2gYY2EPfpt
GE3/IJIvS1p6jHjsI33PlzW+QiqNHPjhCyOBSiQVCZUIt5OrjMMyhJdWVTSNhD6p6GzFCQdyUwrd
13k/mHnJE8mme4eAZ8VyGvaRn7Y+JnG5RKUDw3DNJa3W6veGA8Izo4FK4RgezNmtX9H9KyF7gV7r
qSWa7Y47xb26WVerAYJaoUuK8MHCPapEdDuWUHHdNZIt2eWwreaRSUldyiJbK9xkg91946fGIRvO
tIdnVB9ehD+mdySldgfcS11SYc/p6X5wSqnNhN9Twg3hvo6A9wkYN8bw5LhTat3vFuNTTwWiFuS+
s/6nl/doLT/aRSSUKJnYNrAU2wK3QoFsBsqDJ7g3wQMPQHHrwTj57kkoHSNTzirBmDXgSjYrrteH
w49FUg3Vg988YMXBTZpxuYoKx7SG3grrLqTiao2UtIYHCSW2v46CrXdZpdGxP4EHgkj30wABK6Jy
CkN/69lS5eN7ZcwEMsRUR07Tg6kFqhXj8Uq7FPdqqTogiCU7jaX2Po9jIUhpflTJ9ZRzl4CdLXKa
yBPzXVMFdynbBHcoybV3KCl1dyipSLbX36Gk9RCYth1dh3lTwD3wvmTBnVCqkHjrPW0wKla/DGOI
P3LmGL4YnWXlM+aBMrNYs9qB1vhwcoGSaY8zEFrh4VmEBC6P+lQWUhgU70y54bmixijQRh1iuA05
TolVYL2Kll6dqwgk5nuh9HweFHM+OHGgMdBnu+D+W4cuPIWKZxNgw10aki5rjimwX9l98vxew3QK
XdUexZWLZu/S9H0GBLD6fX1EABXMnOTxjG4v1F1IhcEXrDWGMySfJ6FPGOI4wrMOiec6yMG+0zuC
1qzwHnhv9wqwJt2fD5UHFb+l2ui6vf52PF8ylyxkH8Clliud+MKLJhKCtfCT7zAdNdzyAMboD14M
xa24PNUUlNyLhDL1JJ+dDa0m8wIUmjQX4AuglyBfxPQyQ7ZWd4SzpKfhd26wW4em156FR5vgzNhL
ceZTH/YoDoo5r4XyqoyuqXW/LD3Y2OfKZ0GFevgQVvPNMI6Mi5KenDFZihJHmrJjUqHG6qGejZWG
WR6p9T+SL2x+afi9UqeYap4UEB/FEp/e/J7KO+WowSM85ZwYexcsvdYBxcFPjOEsST4DvjQQUhFI
KdtB97dHSAUiJUIFIuXBcz9xXAmNnDx6EJurWOhKV95dfEhns2UOiB2nroz0oIl/GsqSXT/zbY9t
ZDh7TSv2xv5+1KNLBFbElD5mc1VXNwjLd8OWlXla3tvH/Sndfu2RnvebZfVpL3KDqBvQPg1hS0sj
+dImaA2g5CcHTvEzSzQcCo34aUIekYeSOyFPHaYI3wYt5C0srfZ1LKlupT9NtBSaD+OzJNOwZOFy
bItg1ZDUupXPlGlfqRTd7vxmdQlY3kTyFDuCOJL8acWareyaaxbj6LxdqjkRXDlrNE+R89KBC0vy
CdMHSXBt+erhK1PG5iiYI9jSvAl5yg28k5c7v7z3oNfP8UwX/pX8MBMQlt/i75hWv5RUXA8m5yoi
3DPlrS/sv/gGSho8VoHFNyjgCRea7McZVs4KwMEV2EFb4QGdxfubg2Ac2n2xebrDbumi91/Yq40D
697pYWz5hWCG9ATcARuTJT/pz1HLFh5uCoXPmLu/aZonrvhyfBaR+NqJC2E+DNmV4UzpAfPLR2cr
34DnF0DQIZyvPByVo6m58p3pM4uX9mqfcseJtkieZD2M+9JFgkCmTOKdKfl1eqlm7YQ8zTOO22sf
mg/8dGdsjnK/G13c5sYhyHIMOvgZnXsYW3rFF5e3PtXjc0jImCxiqxtQXCxXSX69BRU3oVB32CFF
uBdbe8Z9cYVuhgdV8m0ER5pbWVnpODlf+aE/LjfM2qWPn1as8p5epF3gw1bffLpQ+e6bH34R5I/L
msO5xOnoPNHQiYVNvrHF+s2Tygxb4PAQkastG8ZSVD1dph4+JUvnM7+8cb4XQ35vNF9GuvU9aSIB
llz3FqhAlxFcQhLMVnzhmCr4tafiEnY3rnFOl/0ewhDjcH8XJQ8+8/YT/sM4CpEXTXZ/+QeWB+ng
B4UbTl4dB7s2SsJw+dchGyuvD0dRjF93NZIq+GoU3MOAcTCz+sTkqaOd08Rxk/OJMSDeOYbCQ+3x
uYZAYExPiOZK40GrCu4a2OFegc7nqXwizpcmjptWohrV/QuSlOOXIjpd7EAhq0F83bHW8d3X2zNL
dK+60mU/BrHENYN6BKgvoCE8NofY5ZIhexDGIY5uPNYyBl36PwEcSqKyldnuNMmvMbmq3aJB3Le1
i4VwYzpbyfNgaa574srfovJ0olll2uXwu2+4gkDZ/lTKdd+6w63MSQXqVZG5GoFbJvGTC0N9M4qv
fH/ln7Gbb401Jy9Hx+UqSvxw6fcOdO0jZ6qyPZAp/hqMO8VLKrQLufX/DDKfFYGeE3Tb42I6RwKf
BZ656cjlgL9/2PyXiYVq7mi+osmTJn3kQNO0OzMbfw9iSb+dXKjlnrx4u9e4/G9JruCWx/RC5cjZ
Zernxuao1o3MUR8M5Sm1fgzpbfcMye+udPlDUPs/u7AMba5M/T8dMjWfOzEbr/nwDZ3izGq8NoSh
vebC1H4L87kw9XcoNMUDN6rskWeG6IEHXXYthK8Sj8rV7ostVG+JL1DMjcwWR8GxGG44o6L8/wYa
n1DgpGAWcxrK8m8Ehv0PtWdK5TnRD4AAAAAASUVORK5CYIJQSwECLQAUAAYACAAAACEAWpitwgwB
AAAYAgAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBLAQItABQABgAIAAAA
IQAIwxik1AAAAJMBAAALAAAAAAAAAAAAAAAAAD0BAABfcmVscy8ucmVsc1BLAQItABQABgAIAAAA
IQDXOoyQZwIAAKYFAAASAAAAAAAAAAAAAAAAADoCAABkcnMvcGljdHVyZXhtbC54bWxQSwECLQAU
AAYACAAAACEAqiYOvrwAAAAhAQAAHQAAAAAAAAAAAAAAAADRBAAAZHJzL19yZWxzL3BpY3R1cmV4
bWwueG1sLnJlbHNQSwECLQAUAAYACAAAACEANmKyIxUBAACHAQAADwAAAAAAAAAAAAAAAADIBQAA
ZHJzL2Rvd25yZXYueG1sUEsBAi0ACgAAAAAAAAAhAI0MNmeGGgAAhhoAABQAAAAAAAAAAAAAAAAA
CgcAAGRycy9tZWRpYS9pbWFnZTEucG5nUEsFBgAAAAAGAAYAhAEAAMIhAAAAAA==
">
   <v:imagedata src="Report%20Insentif_files/Report%20(Autosaved)_19996_image001.png"
    o:title=""/>
   <x:ClientData ObjectType="Pict">
    <x:SizeWithCells/>
    <x:CF>Bitmap</x:CF>
    <x:AutoPict/>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:10px;margin-top:2px;width:58px;
  height:49px'><img width=58 height=49
  src="{{Config::get('constants.path.img')}}/logoreport.png" v:shapes="Picture_x0020_1"></span><![endif]><span
  style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td height=7 class=xl6619996 width=19 style='height:5.25pt;width:14pt'></td>
   </tr>
  </table>
  </span></td>
  <td class=xl6619996 width=37 style='width:28pt'></td>
  <td class=xl6619996 width=116 style='width:87pt'></td>
  <td class=xl6619996 width=232 style='width:174pt'></td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=100 style='width:75pt'></td>
  <td class=xl6619996 width=74 style='width:56pt'></td>
  <td class=xl6619996 width=90 style='width:68pt'></td>
  <td class=xl6619996 width=113 style='width:85pt'></td>
 </tr>
 <tr class=xl6719996 height=25 style='mso-height-source:userset;height:18.75pt'>
  <td height=25 class=xl6719996 width=19 style='height:18.75pt;width:14pt'></td>
  <td colspan=12 class=xl7219996 width=1227 style='width:923pt'><span
  style='mso-spacerun:yes'>         </span>PT. TROCON INDAH PERKASA</td>
 </tr>
 <tr class=xl6819996 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6819996 width=19 style='height:17.25pt;width:14pt'></td>
  <td colspan=12 class=xl7519996 width=1227 style='width:923pt'><span
  style='mso-spacerun:yes'>           </span>LAPORAN INSENTIF</td>
 </tr>
 <tr class=xl6619996 height=4 style='mso-height-source:userset;height:3.0pt'>
  <td height=4 class=xl6619996 width=19 style='height:3.0pt;width:14pt'></td>
  <td class=xl6619996 width=37 style='width:28pt'></td>
  <td colspan=4 class=xl7119996 width=534 style='width:401pt'></td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=100 style='width:75pt'></td>
  <td class=xl6619996 width=74 style='width:56pt'></td>
  <td class=xl6619996 width=90 style='width:68pt'></td>
  <td class=xl6619996 width=113 style='width:85pt'></td>
 </tr>
 <tr class=xl6619996 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6619996 width=19 style='height:17.25pt;width:14pt'></td>
  <td colspan=2 class=xl7419996 width=153 style='width:115pt'>PERIODE</td>
  <td colspan=3 class=xl7419996 width=418 style='width:314pt'>:  {{ $datafilter->period }}</td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=100 style='width:75pt'></td>
  <td class=xl6619996 width=74 style='width:56pt'></td>
  <td class=xl6619996 width=90 style='width:68pt'></td>
  <td class=xl6619996 width=113 style='width:85pt'></td>
 </tr>
 <tr class=xl6619996 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6619996 width=19 style='height:17.25pt;width:14pt'></td>
  <td colspan=2 class=xl7319996 width=153 style='width:115pt'>DEPARTEMEN</td>
  <td colspan=3 class=xl7319996 width=418 style='width:314pt'>: {{ $datafilter->departmentname }}</td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=93 style='width:70pt'></td>
  <td class=xl6619996 width=100 style='width:75pt'></td>
  <td class=xl6619996 width=74 style='width:56pt'></td>
  <td class=xl6619996 width=90 style='width:68pt'></td>
  <td class=xl6619996 width=113 style='width:85pt'></td>
 </tr>
 <tr height=32 style='mso-height-source:userset;height:24.0pt'>
  <td height=32 class=xl1519996 style='height:24.0pt'></td>
  <td class=xl6419996 style='border-top:none'>NO</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>Departemen</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>Karyawan</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>Minggu 1</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>Minggu 2</td>
  <td class=xl6419996 style='border-left:none'>Minggu 3</td>
  <td class=xl6419996 style='border-left:none'>Minggu 4</td>
  <td class=xl6419996 style='border-left:none'>Minggu 5</td>
  <td class=xl6419996 style='border-left:none'>Total</td>
  <td class=xl6419996 style='border-left:none'>Rate</td>
  <td class=xl6419996 style='border-left:none'>Pot Absen</td>
  <td class=xl6419996 style='border-left:none'>Total</td>
 </tr>
 @php
   $t_total = 0; 

   $n = 1;
 @endphp
 @foreach($itemdata as $itemdatas)
 <tr height=25 style='mso-height-source:userset;height:18.75pt'>
  <td height=25 class=xl1519996 style='height:18.75pt'></td>
  <td class=xl7019996 style='border-top:none'>@php echo $n++; @endphp</td>
  <td class=xl6319996 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->departmentname }}</td>
  <td class=xl6319996 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->employee }}</td>
  <td class=xl6519996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->day1,0) }}&nbsp;</td>
  <td class=xl6519996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->day2,0) }}&nbsp;</td>
  <td class=xl6519996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->day3,0) }}&nbsp;</td>
  <td class=xl6519996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->day4,0) }}&nbsp;</td>
  <td class=xl6519996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->day5,0) }}&nbsp;</td>
  <td class=xl6519996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->tday,0) }}&nbsp;</td>
  <td class=xl6519996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->rate,0) }}&nbsp;</td>
  <td class=xl6519996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->tpotabsence,0) }}&nbsp;</td>
  <td class=xl6519996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->amount,0) }}&nbsp;</td>
 </tr>
 @php 
 $t_total += $itemdatas->amount; 
 @endphp
 @endforeach
 <tr height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl1519996 style='height:17.25pt'></td>
  <td colspan=11 class=xl7619996>TOTAL</td>
  <td class=xl6919996 align=right style='border-top:none;border-left:none'>@php echo number_format($t_total,0); @endphp</td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=19 style='width:14pt'></td>
  <td width=37 style='width:28pt'></td>
  <td width=116 style='width:87pt'></td>
  <td width=232 style='width:174pt'></td>
  <td width=93 style='width:70pt'></td>
  <td width=93 style='width:70pt'></td>
  <td width=93 style='width:70pt'></td>
  <td width=93 style='width:70pt'></td>
  <td width=93 style='width:70pt'></td>
  <td width=100 style='width:75pt'></td>
  <td width=74 style='width:56pt'></td>
  <td width=90 style='width:68pt'></td>
  <td width=113 style='width:85pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>
