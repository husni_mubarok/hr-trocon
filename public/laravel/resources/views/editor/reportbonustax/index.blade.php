@extends('layouts.editor.template')
@section('title', 'Laporan Bonus (Pajak)')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    Laporan Bonus (Pajak)
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Laporan Bonus (Pajak)</li>
  </ol>
</section>

<section class="content">
  <div class="row"> 
    {!! Form::model($datafilter)!!}
        {{ csrf_field() }}
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <div class="col-sm-2" data-toggle="tooltip" data-placement="top" title="Department">
            {{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control', 'placeholder' => 'Select Department', 'id' => 'departmentid', 'onchange' => 'RefreshData();')) }} 
          </div>
          <div class="col-sm-2" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Period">
            {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'RefreshData();')) }} 
          </div>   
          <div class="col-sm-2" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Employee">
            {{ Form::select('employeeid', $employee_list, old('employeeid'), array('class' => 'form-control', 'placeholder' => 'Select Employee', 'id' => 'employeeid', 'onchange' => 'RefreshData();')) }} 
          </div>   
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <button onClick="showreport()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-print"></i> Cetak</button>

          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
          {!! Form::close() !!}
        </div>  
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>Karyawan</th> 
                  <th>Posisi</th>   
                  <th>Periode Gaji</th>
                  <th>Periode Bonus</th> 
                  <th>Status Pajak</th>
                  <th>Gaji Pokok</th>
                  <th>Perkalian</th>
                  <th>Total Bonus</th>
                  <th>Kumulatif</th>
                  <th>Last Bruto</th>
                  <th>T Bruto dgn Bonus</th>
                  <th>T Bruto tnp Bonus</th>
                  <th>Biaya Jab dgn Bonus</th>
                  <th>Biaya Jab tnp Bonus</th>
                  <th>Netto PTKP dgn Bonus</th>
                  <th>Netto PTKP tnp Bonus</th>
                  <th>PTKP dgn Bonus</th>
                  <th>PTKP tnp Bonus</th>
                  <th>PPH21 dgn Bonus</th>
                  <th>PPH21 tanpa Bonus</th>
                  <th>PPh21</th>
                  <th>Pot Pinjaman</th>
                  <th>Netto</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  



@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
    //datatables
    table = $('#dtTable').DataTable({ 
     processing: true,
     serverSide: true,
     "pageLength": 25,
     "scrollY": "360px",
     "scrollX": true,
     "sScrollXInner": "150%",
     "autoWidth": true,
     "rowReorder": true,
     "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
     ajax: "{{ url('editor/reportbonustax/data') }}",
     columns: [   
     { data: 'employee', name: 'employee' }, 
     { data: 'position', name: 'position' },  
     { data: 'periodegaji', name: 'periodegaji' }, 
     { data: 'periodebonus', name: 'periodebonus' },  
     { data: 'taxstatus', name: 'taxstatus' },  
     { data: 'gajipokok', name: 'gajipokok', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },  
     { data: 'perkalian', name: 'perkalian', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },  
     { data: 'totalbonus', name: 'totalbonus', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },  
     { data: 'kumulatif', name: 'kumulatif', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'lastbruto', name: 'lastbruto', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'tbrutodgnbonus', name: 'tbrutodgnbonus', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'tbrutotnpbonus', name: 'tbrutotnpbonus', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'biayajabdgnbonus', name: 'biayajabdgnbonus', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'biayajabtnpbonus', name: 'biayajabtnpbonus', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'nettoptkpdgnbonus', name: 'nettoptkpdgnbonus', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'nettoptkptnpbonus', name: 'nettoptkptnpbonus', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'ptkpdgnbonus', name: 'ptkpdgnbonus', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'ptkptnpbonus', name: 'ptkptnpbonus', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'pph21dgnbonus', name: 'pph21dgnbonus', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'pph21tanpabonus', name: 'pph21tanpabonus', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'pph21', name: 'pph21', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'potpinjaman', name: 'potpinjaman', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     { data: 'netto', name: 'netto', render: $.fn.dataTable.render.number( ',', '.', 0, 'Rp. ' ) },     
     ]
   });
    //check all
    $("#check-all").click(function () {
      $(".data-check").prop('checked', $(this).prop('checked'));
    });
  });
  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax 
  }

  function RefreshData()
   { 

    $.ajax({
      type: 'POST',
      url: "{{ URL::route('editor.periodfilteremp') }}",
      data: {
        '_token': $('input[name=_token]').val(), 
        'departmentid': $('#departmentid').val(),  
        'departmentid': $('#departmentid').val(),    
        'periodid': $('#periodid').val()   
      }, 
      success: function(data) { 
        reload_table();
      }
    }) 
  }; 
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

$(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   
  });
   function showreport()
  {
   var url = '../editor/reportbonustax/printreport';
     PopupCenter(url,'Popup_Window','1200','600');
  } 

  function PopupCenter(url, title, w, h) {  
    // Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
          
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
          
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
    
    // Puts focus on the newWindow  
    if (window.focus) {  
      newWindow.focus();  
    }  
  } 
</script> 
@stop
