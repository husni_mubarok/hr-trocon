@extends('layouts.editor.templatereport')
@section('content')
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
 
</style> 
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 15">
<link rel=File-List href="Transfer%20Bonus%20BCA_files/filelist.xml">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<style id="reportbonusthrtax_25812_Styles">
<!--table
  {mso-displayed-decimal-separator:"\,";
  mso-displayed-thousand-separator:"\,";}
.xl6325812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6425812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6525812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6625812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6725812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:"\#\,\#\#0";
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6825812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6925812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7025812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:1.0pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7125812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:16.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7225812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7325812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border:.5pt solid windowtext;
  background:#D9D9D9;
  mso-pattern:black none;
  white-space:normal;}
.xl7425812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7525812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7625812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7725812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7825812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border-top:.5pt solid windowtext;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7925812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border-top:.5pt solid windowtext;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl8025812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:"\#\,\#\#0";
  text-align:left;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl8125812
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border-top:.5pt solid windowtext;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
-->
</style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="reportbonusthrtax_25812" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=947 class=xl6325812
 style='border-collapse:collapse;table-layout:fixed;width:713pt'>
 <col class=xl6325812 width=10 style='mso-width-source:userset;mso-width-alt:
 365;width:8pt'>
 <col class=xl6325812 width=25 style='mso-width-source:userset;mso-width-alt:
 914;width:19pt'>
 <col class=xl6325812 width=194 style='mso-width-source:userset;mso-width-alt:
 7094;width:146pt'>
 <col class=xl6325812 width=190 style='mso-width-source:userset;mso-width-alt:
 6948;width:143pt'>
 <col class=xl6325812 width=189 style='mso-width-source:userset;mso-width-alt:
 6912;width:142pt'>
 <col class=xl6325812 width=173 style='mso-width-source:userset;mso-width-alt:
 6326;width:130pt'>
 <col class=xl6325812 width=166 style='mso-width-source:userset;mso-width-alt:
 6070;width:125pt'>
 <tr height=7 style='mso-height-source:userset;height:5.25pt'>
  <td height=7 class=xl6325812 width=10 style='height:5.25pt;width:8pt'></td>
  <td class=xl6325812 width=25 style='width:19pt'></td>
  <td class=xl6325812 width=194 style='width:146pt'></td>
  <td class=xl6325812 width=190 style='width:143pt'></td>
  <td class=xl6325812 width=189 style='width:142pt'></td>
  <td class=xl6325812 width=173 style='width:130pt'></td>
  <td class=xl6325812 width=166 style='width:125pt'></td>
 </tr>
 <tr class=xl6425812 height=25 style='mso-height-source:userset;height:18.75pt'>
  <td height=25 width=10 style='height:18.75pt;width:8pt' align=left
  valign=top><!--[if gte vml 1]><v:shapetype id="_x0000_t75" coordsize="21600,21600"
   o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe" filled="f"
   stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="Picture_x0020_1" o:spid="_x0000_s14337" type="#_x0000_t75"
   style='position:absolute;margin-left:6.75pt;margin-top:0;width:39.75pt;
   height:33pt;z-index:1;visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQBamK3CDAEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhvROFGcle3tSdZNgokh7Rjb3+8vyWK5tSObIJBxWPPbsuIM
UDltsK/5x/qleOCMokQtR4dQ8x0QXzbXV4v1zgOxRCPVfIjRPwpBagArqXQeMHU6F6yM6Rh64aX6
lD2Iu6q6F8phBIxFzBm8WbTQyc0Y2fM2lWcTjz1nT/NcXlVzYzOf6+JPIsBIJ4j0fjRKxnQ3MaE+
8SoOTmUi9zM0GE83SfzMhtz57fRzwYF7S48ZjAa2kiG+SpvMhQ4kvFFxEyBNlf/nZFFLhes6o6Bs
A61m8ih2boF2XxhgujS9Tdg7TMd0sf/X5hsAAP//AwBQSwMEFAAGAAgAAAAhAAjDGKTUAAAAkwEA
AAsAAABfcmVscy8ucmVsc6SQwWrDMAyG74O+g9F9cdrDGKNOb4NeSwu7GltJzGLLSG7avv1M2WAZ
ve2oX+j7xL/dXeOkZmQJlAysmxYUJkc+pMHA6fj+/ApKik3eTpTQwA0Fdt3qaXvAyZZ6JGPIoiol
iYGxlPymtbgRo5WGMqa66YmjLXXkQWfrPu2AetO2L5p/M6BbMNXeG+C934A63nI1/2HH4JiE+tI4
ipr6PrhHVO3pkg44V4rlAYsBz3IPGeemPgf6sXf9T28OrpwZP6phof7Oq/nHrhdVdl8AAAD//wMA
UEsDBBQABgAIAAAAIQCpJl2vbAIAAKIFAAASAAAAZHJzL3BpY3R1cmV4bWwueG1srFRhb5swEP0+
af/B8vcUQyEQVKiypJ0mVVs0bT/ANaZYAxvZTppq2n/f2YZEXTVtWvftuLPvPb93x9X1cejRgWsj
lKxwfEEw4pKpRsiHCn/9crsoMDKWyob2SvIKP3GDr+u3b66OjS6pZJ3SCFpIU0Kiwp21YxlFhnV8
oOZCjVxCtVV6oBY+9UPUaPoIzYc+SghZRmbUnDam49xuQwXXvrd9VBve9+sAwRth16bCwMFlpzOt
VkM4zVRfk6vIkXKh7wDBp7atiyxPslPJZXxVq8c6DmkXzjlXnxpB2p/2Xc9QVp3a18mp7ynnriRZ
Hue/wZyu/IoZ58WKFCc+Z+AZbhQsYMjDTrCdngA/HnYaiabCCUaSDuAQVO1ecxTj6Hwm3KAldLlT
7JuZPKP/4NhAhQQstemofOBrM3JmYXIcWtAfKAU4//mM7n0vxlvRg0G0dPGraYTR+6vBU20rGN8q
th+4tGH6NO+phck3nRgNRrrkwz0HMfWHJsaIweBbUHTUQloYOVryo70zdorQXosKf0+KNSGr5N1i
k5HNIiX5zWK9SvNFTm7ylKRFvIk3P9ztOC33hoP8tN+OYn56nL7wYBBMK6Nae8HUEAXe894A75hE
wYMD7StMvPCeGhhwpgihU9hxNVZzy7oZ8QXen7fU47lWLZj3GQx3Zp8aT8afzXVraEY3o7Q8tnr4
H8ggAzpW2G8zRk8VXi6XeeYe79+MGBQzkDvJwDgop3FOLi8ncRwJd3DUxr7n6tWEkGsEYwJK+Lmg
BxiLoMkMMYkSZPCbALs3LWQvYAK31NJ5Z5797Kab4eda/wQAAP//AwBQSwMEFAAGAAgAAAAhAKom
Dr68AAAAIQEAAB0AAABkcnMvX3JlbHMvcGljdHVyZXhtbC54bWwucmVsc4SPQWrDMBBF94XcQcw+
lp1FKMWyN6HgbUgOMEhjWcQaCUkt9e0jyCaBQJfzP/89ph///Cp+KWUXWEHXtCCIdTCOrYLr5Xv/
CSIXZINrYFKwUYZx2H30Z1qx1FFeXMyiUjgrWEqJX1JmvZDH3IRIXJs5JI+lnsnKiPqGluShbY8y
PTNgeGGKyShIk+lAXLZYzf+zwzw7TaegfzxxeaOQzld3BWKyVBR4Mg4fYddEtiCHXr48NtwBAAD/
/wMAUEsDBBQABgAIAAAAIQCz/bZnEgEAAIMBAAAPAAAAZHJzL2Rvd25yZXYueG1sTFDLTsMwELwj
8Q/WInGpqJOgkLbUrQISElxAbfkAK3EeIvZGtmlSvp5NShVO1sx6Zmd2ve11w47KuhqNgHAeAFMm
w7w2pYDPw8vdApjz0uSyQaMEnJSD7eb6ai1XOXZmp457XzIyMW4lBVTetyvOXVYpLd0cW2VoVqDV
0hO0Jc+t7MhcNzwKggeuZW1oQyVb9Vyp7Gv/rQXcc0xPeft2egqSbobFsvw4zDohbm/69BGYV72f
Pv+pX3MBEQxVqAZsKF/fpCar0LJip1z9Q+HPfGFRM4vdgFmGjQAqTfi9KJzyIyL2ghZxEsXAB0OP
ZxmtGWX0/pOFi2CZxOPooo3iJCSKxHyKM4LpdptfAAAA//8DAFBLAwQKAAAAAAAAACEAjQw2Z4Ya
AACGGgAAFAAAAGRycy9tZWRpYS9pbWFnZTEucG5niVBORw0KGgoAAAANSUhEUgAAAE4AAABDCAYA
AAFFMwNVAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAIdUAACHVAQSctJ0A
ABobSURBVGhD7ZsJWFPHvsAPsoR9VZBFUBREBdRqXVot1fbaura9V9v6etVXa11RFBCFJJzsYd9d
cEPtKnWrCxAgZD/ZISJqxba22l6vtNXettZqhbyZkwkQCCEofb3vfff3ff8vM3PmnMz5z/af/8zB
BownrvoNBe3jb/v18VhKrRFFsYS9mr+jIIYFMMS3URDD1p02Yql1xp3nvvZDKV2485sfwF9fXPEZ
liwwvnxAMxnb+GnnUztxYV98RAbAkyqNRkeYGf59BEP4HpluZszmVgoK9mJiocgXBW0zd692Dgpa
MpIr44zLUdSi6CCwtlznjIKYI1XzOwqaeOOIIRoFMWxrdW+19GQcW0KbmKPkQjWhJMxoNA5BwW6A
DEcNtzywLVXGmbnC52DSK4caF5DXzDjvqH+IJYM6BpnIJ0KlAzZXtQ4jMwyIGUXq/0JB+zjW8qO/
L71BEMFSvImS+sc/U3TMkyZ+5MtQVUwpUKXNKFGlBXE0+zwyxA9COLKjKJtt3DMk91DQki3VRm+a
5CsUs8CTLr2GgpYknbs6DgUtmJdf409qF2o2Tdx/5ZpxYF6wbCYQWDVJ1cbFZerXyYeS8ZpeDx1X
IJuAgl248y6QvbATWCIo2+s7JuySjIjbpwrrLiiXicSaGBTqIpAlFqMgeH/QbkHbdUkRfu+y/vRd
sjHBgcP8JzAM0/oD9L1d8DcSVwS67BTenr9PNxcHbds5rZ5NZkCA9u7gz9FeheEAuuQWmdgXU4s0
T/ux1HoUtQp4oLM3W9Mykq8g+8n/LrN3aed5MbXfeDJVem+6iPYMaNA+9IY0H6ZG6cbSf51Q1jQJ
Ze2b2HzVx+tOXRmJop2MYtb3arQLKi6ueG6XLglF7WPhHiKUrGkrvH7UEIuClvjSRNYVD5sDaMCU
dNEvKMWCYWz5aBTsIpAlvYKCnSTwhcuC6eJqc3tLqBC5okudeNHEd1Gwi3G50uko2MWG08b5pURc
V+Ot7/W6jpnElyjYxTv7W/xRkGQES36SHM03nzcuKNevIPvrNoExtbJlOMpC4kMXfoSCXeCVLS4o
aAJ1IQc0NpO623zOiK09YVm6ZEEaCnWxrNvDPOjSrv4I6Nnp4/jnOufvKYWqYyjYxdOl6gAU7CzV
EKAj79Sab8jXNesN/nZrKq4Z4l4Vh8UVqF6Bv0OSBO3YVjCOmW/YAIwD+DCyApAA3Q2n1n4DLztz
LnRZHmbcqQ0PUXBAROUoC1Cwi7m79eRsNJavSIKVEcAkPh9GF+lfO/p5oCeuvAGvTS7S5/hSG65S
0hreGbK9nrSsJhY2WTc+hrPlKhS0ixF84jwKWseLLm97YbfmRRS1ysR85dt+LMXPKGofc/c0vRaX
Rwh8aA0CP5qQu/Ijbe9+OJhUtbZSOOeuT3LfUTUZQ7LiyKXJFdeNvfrxoDO5VB8Sxledd2Wo/xXO
kVcu/6B5Jro0IGaX6Z8L5BACt0zit7gCNW9VxfXHL/ykAvWacB5Rg6L9EoELz2Bbzhvn7VFNREk2
EYmMTn5MxdX1JwyBKMk+Fu67MDuYJT2Mov0SlikykB0ZjltgAPxLeeN8dKlfPGmSX8aXiTxR1DZT
gIHtQpX8iqL9svxQYxQ52sCCmYcsIM/t0iejLDZJAP08OltRiqK2iclRFY3LVdBQ1BYO2NqTv0em
V2twUYsnloLG0W7D4Nhs5QGU1yZOzCbbNpEZF6qsYxxbEYGiVtl27IYbthVoCrQxLPGs0Xu74Jtl
lZWOjtsb7lkM+EAC6CIFuq1PAmgNl6bvbw5CURswGx9g3dZOPSnXgWtwhjYbw9tAlSZVGf22112H
1/0yRLexbWBigQKrG7yAy9aa78ib+2ByvqIiimfFMuiJK6fpQa+JGgFG5sju1UYKKGQYW/YJykIS
iTcU+KZVl/umAtl2ptw38UT5iG2nDqLLvRiSUrtjyYGmhShqA7r+AVh29tLclCL1sl4FA9UWm6ek
T+ILJ4Jln2lJAaZbU5XC66Y8Ji3WGB23CkzL9B740ETnFu9T91569AQYA4/G5Fiad1F8RbZpmEAC
CwBsgnn7dIuezZXNxzaeIauWLAQsTM+XQC8C73EAVV1ZaXREjyZxoGmuW18k92Bmqf7NkVmq4yiK
jcyoPYVtOtOGbTnXBtqQ6Tfx/Hfz96qnJvCEy7HVn7Rh606DdHBtq6ANaKsNtMnekgIFXNta04Zt
Otc2q0AWif4C86UKbbZJC9zp8tZVp0x2xsZTl1YE4aJ/RHBletgWPaiiH0ZwJIaYLOkevObaM74M
mTqEq76+o+4Ln7hiYk0QQ1IXnN30I7jVIShbdzOQKTo8lClRwme5sHXtCbjIyTFd1g7jkDm7NUtC
eNIu/4Q9eFJl91EQe7pYvXnWwdZhsHBBbFmByGh0iskhPnnjyOXoMLZcHsknmvCaG/6L92tXBeGS
2lAu0QLvA3lrQliy46NztWRnCMhseGtGifbZQLqYNGc2nbwc4JIhs19r3YnKUX06jCm9TdwA49og
E8KSXgTNR4eiT8YqsJoL50s/dmdpf/BhKL4bztN+NJwhWReIi170SK+Pn75LGT8LiC8Ig+noBWC3
rQniKgv92Gqda3pDhytD1xLBk7O59V/YMdj+hz+Z+KMCDzBzdFrB4/h1k9nCSxFg3LIYy/5QcMHn
gWFsaZovR6dxy5T/6sNU3g7gaKsnFOj3hbEk26FrAkp0ljxtBI/IG8pVn/ZiEjdd6bJffVga5fhc
VSKO2zHQDoSYbNVyn0zpQ1dcbYjNVyUWq1q90SW7ySdu+I/iyaluLM3XQxmKy8+U6qeiS4/H6Svf
eVEyFO2jeLISlGSTEXRhmRzcg6J90tJidAnjE1fBfPrbKivOjP4xGh1cMuS/rP5QH4JSbALal4vD
lmqjD03yOUrql/giRexQhqy3I6Q/ppbo35hVpi9C0X7xp9aLzRP//EPaOJTcL364vPWlAxfGoqh9
uKWL2xeV69xR1CbLyoCJDrRGei6AbeeSJuycN/tj/dnLsQFMS1vQJqsqW4Z7UBv+gaL94p0ieEB6
icx2HDCPphWqVqLL/eK2U9jb/dwX8cBkji9UbkDRfvHfWXejc/WFbDeXnSKrRqU1AnCZ3WtjzD1D
fI8gOuya6OPw2o+hCeSwraajp5E5iisrRtlsEpUlf3fD8eZO284mjnRlBwraJIpWk4K984kRrrqC
6MJT5IKnW+GAgQk9Zw6m3H2z9L3G8WOzJdtRtG9wkcjJHVe2oahNHDeffYitP2X03Fp1B5rdjmnC
HoWrM47gyBpR9j7BgW3oT5fY9ntB5u7Xx4chq9UWMVzpcXLNCl2lYP0wK1uaMIYn2UpWrbl64W+y
oCOhuMeWhxUoGdJmFOwbv4z6+U8XEmUoapWX31N5k75cuJiB/hFQSEpyzSM4cFN2Njw0LYLIgpHX
A6j15HrWFu6ci1+gYN+MzZJTx3DlXZu4VghmiA1IK6YCwp66+bzxlUOaEQv3N08h08wFhwNz4jnj
7FyJzV0TJ7ruJgraIKmG+tcjF1egWC8WHjDEdrYpsoBAQCHCWV3DgV+68LbJOw6qfRNYMoJ26ZH4
6Q/oslU8mPo7KNg3QZkNuUvfv9Rn4fxx6Zedheveriq6PMLZ8iteFqv9JCAbT5Qnf6Drvb2GsKtw
WHIN9ZUjBquFW3SgaR65gu9eOKC5GcXE8ZeLqyhxoOH32gboJhPyJCPQo3pDb7K+HdqdWaWqTA+a
2Oq2MGWn+K5FwYDWnLbXkY5895S6VrIq0fza5YoAQlY9SAfzb3y2NJN8WA98s1r6t2bGZileGsWV
lKNoJy/sVm8gqxAVytzexmcrdmw+3TIeNnprDsTO/KjnYkkC4+rTVyxsPrh36YVLDCjaN2uOfTbK
jy66gKIka3VGZyy5rsP0x0BQL6Wk1pG72c6JoNHD8Q7+Obzes3DdCwheYDRLcpJ8MGLbMcItgmen
ZeKGaywOlEzIUpSQQwL8c1g9aIiIy5LNn8qqXYKtO0k6EPvUnFnM2gPVj4uud3aghH1Nk6KzZO+g
qG2cqIrO7Sf4Vg6J1d9hm84iRw4Q8OuRUksOrC5rPmnD1p5swzacAddq2kDBTA6bvhw5yeB6UnWb
d0qNlPwDQAhHsn79ic/t86gPZ0mIV480J6DoH04AU16Ngv2z8GDjRH+a8CKKYjG5KrUTXdExtVB9
Ora0+chIrlThz9Pdx0VGp1AucSsgU1w6Nk/TBJq2gw9Dfj+ALv7k6VJ18vO7DaBzyVuG8XQ3XizV
h/jgCmJqoaYkOks5C9tcNR49HphoA9y6c0qXPHq5uOsMkPtOIXm2ITZPediXLtKNzlHfhx4nb2rD
EZgO/li3YJ96Uhhb9DaMAxxG8VXk8QW4uB7Nk7N9qKKauDzlnlkluo1gPCUHZFz02dBhTIUIhu1m
zm796rh8dS6KYiNYkp/g7+z9zfvgb3SWgiiuqqJE5ahrhzOlH0fnqirgEYcQnlofzpWfiSkyvBlX
QDwVxSe0wWxZ87Iy9fCRPNk5eG8oV315KG7yxwWzpQ2T8omBNyHXDOm/Vn3U+5zAYLGsUufjTu3j
cE1/wKNSjhlE+4KKC/+NkgaNxfsNLw/JUD568YguHCUNHDiUhLBkxRRc/UtMNlGcX3PDYuN/IKz9
4LOYUVzpx24M5b1gnnovfuZbu5aedjGpSPuOL0utd0uX/u7DVClG8RQ543KJ16cVq+aE4vWk4xD8
eXx8kXJWKEeycCRXnhbK15/yZcp/cGOq7vowpOef26VZjB73x7L6k0uTY/OVW4ZmSnaFc6QCeCQs
hCkVxGQrj80o0rDfeu/yvKrWjj5P/f2H//BHAEy1ZWUiz2eLdOFT8xSwiS4Yn6dODuYRZaF8jcCP
STQF4NLvnXaKfnTPELVTdgJJa2h3TRNZiMsOUbtbOrwuvu9Fl/7ox5C3DmMTDUFcbXlcoZoWky1d
FMZVxIK+Gbb2jH3+p38raDVfTpxYoMofxhDfdMbVDx3wpkfumfKOYbjkq0g+cfTZPdrEFSc/m0Xu
RKzVOYOp1QlMFI4mT7EROi6AwN+eQjo1HDAcH4LBvHBbb225c1adzmfRoQtPzd6r2zQmS/HxMFz0
Dxca0Y7hukeUDNk9MLEKXzygX9HT9v5TWXrshhs8k+NOlcspNOJfvrj8nyNztAfhpuyEHO3oXIHB
A2UdMK8f0j71t/3a51F04ICWvvyDz4bG5KmmTCjQZAxlElo3UIEeDNX1qBzlnjc/vNT/xu5g818f
NPtFZ6mOu9CU9yP4yrOvv3/F+pHtx2ROsWqKV7LgluNWgTE+S5IOvV7o0hMDPbbxBZpUb4662Z0u
/9fMXXqz7fnHMj6f8PdlyEVeNOnPiSeu2nUiaCC8WKJYToH+RuhiAYu7Ick1DyPZcl4Cbhw05ZGA
Lh+br0p3Tpc9GJ2lYMIWiq4MPnBfOowtkwPj5/qc3ZqB7RrYQRxfvsYlWXCPdGjA1S9c3SbXGB1S
6zviCpQWS/DBADqKY3OJXEeq7NfoHDl0zQ++8nBQQ6Ecea5TuvzehBzC7s0Fe5ldqGQ4balpN3mB
gMKg19HsNiBdB/XG4Wyi/uWDjY/xqULfQAdNZJbyjBdddjMAr7VrD21APFOoXOJMl38TwZWeg4fx
UPKgMI7bkOSwFSgHOr5JP0s3pVn4WeqNgWyFdHPVwLdSbRGfqxrlkSn/8tm9hr+ipMEhAhe5BmQ2
NHtk1D8EM1WfHtPH4UaHaYNo+WHDSmfkViaVRvodeyrOJF5U6c9Jp68M2lABV76hHFl+BE96EbTA
wdtAn3+w6W1nOmEMZissP6N6Ata8pwoLTDp1hbL2+N0puGApTJuYI3uNkib+3rKL9lYcFE+65KsZ
Jdpp5MMGgUkFxIuuDM2dOaWaJSjpyQnnEhKwzP9tUoGKfMEn5Zkc4Wj/1PMG8tj+u8eNzutOtT+b
3ZACry0qV87yyJDctdXizOJJFf/8yqELs8mHPiHQxHJkGb4OZYn3oKQnI6O6LdglXfSbL018dWyh
/Im7x8qPWkf77xDcJZ3a0He8CcqnRmzjpx2jceFeXHTd9bWjhkA/mugCUJxp39aaAlG6w07RQ9KH
/ITmBJwkKOmSVg+qSI2f6vJRPzZ/2aud58q5+CiAJj4UnKcbipIfi5lF6kWUHcK7Jgc8FDCDmrfF
Es8ZHRKr28cwxadg3pXHmkKDcXFLZ8uzJrBLg9nXJVXwEKyFB/bZkBU8MoTVvrzm1iUDPaBhjYQ9
+m0YTf8gki9LWnqMeOwjfc+XNb5CKo0c+OELI4FKJBUJlQi3k6uMwzKEl1ZVNI2EPqnobMUJB3JT
Ct3XeT+YeckTyaZ7h4BnxXIa9pGftj4mcblEpQPDcM0lrdbq94YDwjOjgUrhGB7M2a1f0f0rIXuB
XuupJZrtjjvFvbpZV6sBglqhS4rwwcI9qkR0O5ZQcd01ki3Z5bCt5pFJSV3KIlsr3GSD3X3jp8Yh
G860h2dUH16EP6Z3JKV2B9xLXVJhz+npfnBKqc2E31PCDeG+joD3CRg3xvDkuFNq3e8W41NPBaIW
5L6z/qeX92gtP9pFJJQomdg2sBTbArdCgWwGyoMnuDfBAw9AcevBOPnuSSgdI1POKsGYNeBKNiuu
14fDj0VSDdWD3zxgxcFNmnG5igrHtIbeCusupOJqjZS0hgcJJba/joKtd1ml0bE/gQeCSPfTAAEr
onIKQ3/r2VLl43tlzAQyxFRHTtODqQWqFePxSrsU92qpOiCIJTuNpfY+j2MhSGl+VMn1lHOXgJ0t
cprIE/NdUwV3KdsEdyjJtXcoKXV3KKlIttffoaT1EJi2HV2HeVPAPfC+ZMGdUKqQeOs9bTAqVr8M
Y4g/cuYYvhidZeUz5oEys1iz2oHW+HBygZJpjzMQWuHhWYQELo/6VBZSGBTvTLnhuaLGKNBGHWK4
DTlOiVVgvYqWXp2rCCTme6H0fB4Ucz44caAx0Ge74P5bhy48hYpnE2DDXRqSLmuOKbBf2X3y/F7D
dApd1R7FlYtm79L0fQYEsPp9fUQAFcyc5PGMbi/UXUiFwResNYYzJJ8noU8Y4jjCsw6J5zrIwb7T
O4LWrPAeeG/3CrAm3Z8PlQcVv6Xa6Lq9/nY8XzKXLGQfwKWWK534wosmEoK18JPvMB013PIAxugP
XgzFrbg81RSU3IuEMvUkn50NrSbzAhSaNBfgC6CXIF/E9DJDtlZ3hLOkp+F3brBbh6bXnoVHm+DM
2Etx5lMf9igOijmvhfKqjK6pdb8sPdjY58pnQYV6+BBW880wjoyLkp6cMVmKEkeasmNSocbqoZ6N
lYZZHqn1P5IvbH5p+L1Sp5hqnhQQH8USn978nso75ajBIzzlnBh7Fyy91gHFwU+M4SxJPgO+NBBS
EUgp20H3t0dIBSIlQgUi5cFzP3FcCY2cPHoQm6tY6EpX3l18SGezZQ6IHaeujPSgiX8aypJdP/Nt
j21kOHtNK/bG/n7Uo0sEVsSUPmZzVVc3CMt3w5aVeVre28f9Kd1+7ZGe95tl9WkvcoOoG9A+DWFL
SyP50iZoDaDkJwdO8TNLNBwKjfhpQh6Rh5I7IU8dpgjfBi3kLSyt9nUsqW6lP020FJoP47Mk07Bk
4XJsi2DVkNS6lc+UaV+pFN3u/GZ1CVjeRPIUO4I4kvxpxZqt7JprFuPovF2qORFcOWs0T5Hz0oEL
S/IJ0wdJcG356uErU8bmKJgj2NK8CXnKDbyTlzu/vPeg18/xTBf+lfwwExCW3+LvmFa/lFRcDybn
KiLcM+WtL+y/+AZKGjxWgcU3KOAJF5rsxxlWzgrAwRXYQVvhAZ3F+5uDYBzafbF5usNu6aL3X9ir
jQPr3ulhbPmFYIb0BNwBG5MlP+nPUcsWHm4Khc+Yu79pmieu+HJ8FpH42okLYT4M2ZXhTOkB88tH
ZyvfgOcXQNAhnK88HJWjqbnynekzi5f2ap9yx4m2SJ5kPYz70kWCQKZM4p0p+XV6qWbthDzNM47b
ax+aD/x0Z2yOcr8bXdzmxiHIcgw6+BmdexhbesUXl7c+1eNzSMiYLGKrG1BcLFdJfr0FFTehUHfY
IUW4F1t7xn1xhW6GB1XybQRHmltZWek4OV/5oT8uN8zapY+fVqzynl6kXeDDVt98ulD57psffhHk
j8uaw7nE6eg80dCJhU2+scX6zZPKDFvg8BCRqy0bxlJUPV2mHj4lS+czv7xxvhdDfm80X0a69T1p
IgGWXPcWqECXEVxCEsxWfOGYKvi1p+ISdjeucU6X/R7CEONwfxclDz7z9hP+wzgKkRdNdn/5B5YH
6eAHhRtOXh0HuzZKwnD51yEbK68PR1GMX3c1kir4ahTcw4BxMLP6xOSpo53TxHGT84kxIN45hsJD
7fG5hkBgTE+I5krjQasK7hrY4V6BzuepfCLOlyaOm1aiGtX9C5KU45ciOl3sQCGrQXzdsdbx3dfb
M0t0r7rSZT8GscQ1g3oEqC+gITw2h9jlkiF7EMYhjm481jIGXfo/ARxKorKV2e40ya8xuardokHc
t7WLhXBjOlvJ82Bprnviyt+i8nSiWWXa5fC7b7iCQNn+VMp137rDrcxJBepVkbkagVsm8ZMLQ30z
iq98f+WfsZtvjTUnL0fH5SpK/HDp9w507SNnqrI9kCn+Gow7xUsqtAu59f8MMp8VgZ4TdNvjYjpH
Ap8FnrnpyOWAv3/Y/JeJhWruaL6iyZMmfeRA07Q7Mxt/D2JJv51cqOWevHi717j8b0mu4JbH9ELl
yNll6ufG5qjWjcxRHwzlKbV+DOlt9wzJ7650+UNQ+z+7sAxtrkz9Px0yNZ87MRuv+fANneLMarw2
hKG95sLUfgvzuTD1dyg0xQM3quyRZ4bogQdddi2ErxKPytXuiy1Ub4kvUMyNzBZHwbEYbjijovz/
BhqfUOCkYBZzGsrybwSG/Q+1Z0rlOdEPgAAAAABJRU5ErkJgglBLAQItABQABgAIAAAAIQBamK3C
DAEAABgCAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAGAAgA
AAAhAAjDGKTUAAAAkwEAAAsAAAAAAAAAAAAAAAAAPQEAAF9yZWxzLy5yZWxzUEsBAi0AFAAGAAgA
AAAhAKkmXa9sAgAAogUAABIAAAAAAAAAAAAAAAAAOgIAAGRycy9waWN0dXJleG1sLnhtbFBLAQIt
ABQABgAIAAAAIQCqJg6+vAAAACEBAAAdAAAAAAAAAAAAAAAAANYEAABkcnMvX3JlbHMvcGljdHVy
ZXhtbC54bWwucmVsc1BLAQItABQABgAIAAAAIQCz/bZnEgEAAIMBAAAPAAAAAAAAAAAAAAAAAM0F
AABkcnMvZG93bnJldi54bWxQSwECLQAKAAAAAAAAACEAjQw2Z4YaAACGGgAAFAAAAAAAAAAAAAAA
AAAMBwAAZHJzL21lZGlhL2ltYWdlMS5wbmdQSwUGAAAAAAYABgCEAQAAxCEAAAAA
">
   <v:imagedata src="Transfer%20Bonus%20BCA_files/reportbonusthrtax_25812_image001.png"
    o:title=""/>
   <x:ClientData ObjectType="Pict">
    <x:SizeWithCells/>
    <x:CF>Bitmap</x:CF>
    <x:AutoPict/>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:9px;margin-top:0px;width:53px;
  height:44px'><img width=53 height=44
  src="{{Config::get('constants.path.img')}}/logoreport.png"
  v:shapes="Picture_x0020_1"></span><![endif]><span style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td height=25 class=xl6425812 width=10 style='height:18.75pt;width:8pt'></td>
   </tr>
  </table>
  </span></td>
  <td colspan=6 class=xl7125812 width=937 style='width:705pt'><span
  style='mso-spacerun:yes'>          </span>PT. TROCON INDAH PERKASA</td>
 </tr>
 <tr class=xl6525812 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6525812 width=10 style='height:17.25pt;width:8pt'></td>
  <td colspan=6 class=xl7025812 width=937 style='width:705pt'><span
  style='mso-spacerun:yes'>             </span>TRANSFER BONUS BCA</td>
 </tr>
 <tr height=4 style='mso-height-source:userset;height:3.0pt'>
  <td height=4 class=xl6325812 width=10 style='height:3.0pt;width:8pt'></td>
  <td colspan=6 class=xl6825812 width=937 style='width:705pt'></td>
 </tr>
 <tr height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6325812 width=10 style='height:17.25pt;width:8pt'></td>
  <td colspan=2 class=xl6925812 width=219 style='width:165pt'>PERIODE</td>
  <td class=xl6925812 width=190 style='width:143pt'>: {{ $datafilter->period }}</td>
  <td class=xl6825812 width=189 style='width:142pt'></td>
  <td class=xl6825812 width=173 style='width:130pt'></td>
  <td class=xl6825812 width=166 style='width:125pt'></td>
 </tr>
 <tr height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6325812 width=10 style='height:17.25pt;width:8pt'></td>
  <td colspan=2 class=xl7225812 width=219 style='width:165pt'>DEPARTEMENT</td>
  <td colspan=4 class=xl7225812 width=718 style='width:540pt'>: {{ $datafilter->departmentname }}</td>
 </tr>
 <tr height=35 style='mso-height-source:userset;height:26.25pt'>
  <td height=35 class=xl6325812 width=10 style='height:26.25pt;width:8pt'></td>
  <td class=xl7325812 width=25 style='border-top:none;width:19pt'>No</td>
  <td class=xl7325812 width=194 style='border-top:none;border-left:none;
  width:146pt'>Karyawan</td>
  <td class=xl7325812 width=190 style='border-top:none;border-left:none;
  width:143pt'>Nama Akun Bank</td>
  <td class=xl7325812 width=189 style='border-top:none;border-left:none;
  width:142pt'>Nama Bank</td>
  <td class=xl7325812 width=173 style='border-top:none;border-left:none;
  width:130pt'>No Rek BCA</td>
  <td class=xl7325812 width=166 style='border-top:none;border-left:none;
  width:125pt'>Netto</td>
 </tr>
 @php
 $t_netto = 0; 

 $n = 1;
 @endphp
 @foreach($itemdata as $itemdatas)
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6325812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl6625812 width=25 style='border-top:none;width:19pt'>@php echo $n++; @endphp</td>
  <td class=xl6625812 width=194 style='border-top:none;border-left:none;
  width:146pt'>&nbsp;{{ $itemdatas->employeename }}</td>
  <td class=xl6625812 width=190 style='border-top:none;border-left:none;
  width:143pt'>&nbsp;{{ $itemdatas->accountname }}</td>
  <td class=xl8025812 width=189 style='border-top:none;border-left:none;
  width:142pt'>&nbsp;{{ $itemdatas->bankname }}</td>
  <td class=xl6625812 width=173 style='border-top:none;border-left:none;
  width:130pt'>&nbsp;{{ $itemdatas->norekeningbca }}</td>
  <td class=xl6725812 width=166 style='border-top:none;border-left:none;
  width:125pt'>{{ number_format($itemdatas->netto,0) }}</td>
 </tr>
 @php
   $t_netto += $itemdatas->netto; 
 @endphp

 @endforeach
 <tr class=xl7725812 height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl7725812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl7825812 width=25 style='border-top:none;width:19pt'>&nbsp;</td>
  <td class=xl7925812 width=194 style='border-top:none;width:146pt'>&nbsp;</td>
  <td class=xl7925812 width=190 style='border-top:none;width:143pt'>&nbsp;</td>
  <td class=xl7925812 width=189 style='border-top:none;width:142pt'>&nbsp;</td>
  <td class=xl8125812 width=173 style='border-top:none;width:130pt'>NETTO</td>
  <td class=xl7625812 align=right width=166 style='border-top:none;width:125pt'>{{ number_format($t_netto,0) }}</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6325812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl6325812 width=25 style='width:19pt'></td>
  <td class=xl6325812 width=194 style='width:146pt'></td>
  <td class=xl6325812 width=190 style='width:143pt'></td>
  <td class=xl6325812 width=189 style='width:142pt'></td>
  <td class=xl6325812 width=173 style='width:130pt'></td>
  <td class=xl6325812 width=166 style='width:125pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6325812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl6325812 width=25 style='width:19pt'></td>
  <td class=xl6325812 width=194 style='width:146pt'></td>
  <td class=xl6325812 width=190 style='width:143pt'></td>
  <td class=xl6325812 width=189 style='width:142pt'></td>
  <td class=xl6325812 width=173 style='width:130pt'></td>
  <td class=xl6325812 width=166 style='width:125pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6325812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl6325812 width=25 style='width:19pt'></td>
  <td class=xl6325812 width=194 style='width:146pt'></td>
  <td class=xl6325812 width=190 style='width:143pt'></td>
  <td class=xl6325812 width=189 style='width:142pt'></td>
  <td class=xl6325812 width=173 style='width:130pt'></td>
  <td class=xl6325812 width=166 style='width:125pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6325812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl6325812 width=25 style='width:19pt'></td>
  <td class=xl6325812 width=194 style='width:146pt'></td>
  <td class=xl6325812 width=190 style='width:143pt'></td>
  <td class=xl6325812 width=189 style='width:142pt'></td>
  <td class=xl6325812 width=173 style='width:130pt'></td>
  <td class=xl6325812 width=166 style='width:125pt'></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl6325812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl6325812 width=25 style='width:19pt'></td>
  <td class=xl6325812 width=194 style='width:146pt'></td>
  <td class=xl6325812 width=190 style='width:143pt'></td>
  <td class=xl6325812 width=189 style='width:142pt'></td>
  <td colspan=2 class=xl7525812 width=339 style='width:255pt'>JAKARTA, 20 Jul
  2018</td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl6325812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl6325812 width=25 style='width:19pt'></td>
  <td class=xl6325812 width=194 style='width:146pt'></td>
  <td class=xl6325812 width=190 style='width:143pt'></td>
  <td class=xl7425812 width=189 style='width:142pt'>DISETUJUI</td>
  <td class=xl6425812 width=173 style='width:130pt'>MENGETAHUI</td>
  <td class=xl7425812 width=166 style='width:125pt'>DISETUJUI</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6325812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl6325812 width=25 style='width:19pt'></td>
  <td class=xl6325812 width=194 style='width:146pt'></td>
  <td class=xl6325812 width=190 style='width:143pt'></td>
  <td class=xl6325812 width=189 style='width:142pt'></td>
  <td class=xl6325812 width=173 style='width:130pt'></td>
  <td class=xl6325812 width=166 style='width:125pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6325812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl6325812 width=25 style='width:19pt'></td>
  <td class=xl6325812 width=194 style='width:146pt'></td>
  <td class=xl6325812 width=190 style='width:143pt'></td>
  <td class=xl6325812 width=189 style='width:142pt'></td>
  <td class=xl6325812 width=173 style='width:130pt'></td>
  <td class=xl6325812 width=166 style='width:125pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6325812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl6325812 width=25 style='width:19pt'></td>
  <td class=xl6325812 width=194 style='width:146pt'></td>
  <td class=xl6325812 width=190 style='width:143pt'></td>
  <td class=xl6325812 width=189 style='width:142pt'></td>
  <td class=xl6325812 width=173 style='width:130pt'></td>
  <td class=xl6325812 width=166 style='width:125pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6325812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl6325812 width=25 style='width:19pt'></td>
  <td class=xl6325812 width=194 style='width:146pt'></td>
  <td class=xl6325812 width=190 style='width:143pt'></td>
  <td class=xl6325812 width=189 style='width:142pt'></td>
  <td class=xl6325812 width=173 style='width:130pt'></td>
  <td class=xl6325812 width=166 style='width:125pt'></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.0pt'>
  <td height=20 class=xl6325812 width=10 style='height:15.0pt;width:8pt'></td>
  <td class=xl6325812 width=25 style='width:19pt'></td>
  <td class=xl6325812 width=194 style='width:146pt'></td>
  <td class=xl6325812 width=190 style='width:143pt'></td>
  <td class=xl7425812 width=189 style='width:142pt'>(_________________)</td>
  <td class=xl7425812 width=173 style='width:130pt'>(_________________)</td>
  <td class=xl7425812 width=166 style='width:125pt'>(_________________)</td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=10 style='width:8pt'></td>
  <td width=25 style='width:19pt'></td>
  <td width=194 style='width:146pt'></td>
  <td width=190 style='width:143pt'></td>
  <td width=189 style='width:142pt'></td>
  <td width=173 style='width:130pt'></td>
  <td width=166 style='width:125pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>
