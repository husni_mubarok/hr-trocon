@extends('layouts.editor.templatereport')
@section('content')
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
 
</style> 
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 15">
<link rel=File-List href="Report%20Master_files/filelist.xml">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<style id="Report (Autosaved)_27136_Styles">
<!--table
  {mso-displayed-decimal-separator:"\,";
  mso-displayed-thousand-separator:"\,";}
.xl6327136
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6427136
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:16.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6527136
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6627136
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6727136
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6827136
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6927136
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7027136
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7127136
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border:.5pt solid windowtext;
  background:#D6DCE4;
  mso-pattern:black none;
  white-space:normal;}
.xl7227136
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7327136
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7427136
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
-->
</style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="Report (Autosaved)_27136" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=1282 class=xl6327136
 style='border-collapse:collapse;table-layout:fixed;width:963pt'>
 <col class=xl6327136 width=15 style='mso-width-source:userset;mso-width-alt:
 548;width:11pt'>
 <col class=xl6327136 width=97 style='mso-width-source:userset;mso-width-alt:
 3547;width:73pt'>
 <col class=xl6327136 width=210 style='mso-width-source:userset;mso-width-alt:
 7680;width:158pt'>
 <col class=xl6327136 width=104 style='mso-width-source:userset;mso-width-alt:
 3803;width:78pt'>
 <col class=xl6327136 width=170 style='mso-width-source:userset;mso-width-alt:
 6217;width:128pt'>
 <col class=xl6327136 width=96 span=6 style='mso-width-source:userset;
 mso-width-alt:3510;width:72pt'>
 <col class=xl6327136 width=110 style='mso-width-source:userset;mso-width-alt:
 4022;width:83pt'>
 <tr height=7 style='mso-height-source:userset;height:5.25pt'>
  <td height=7 class=xl6327136 width=15 style='height:5.25pt;width:11pt'></td>
  <td class=xl6327136 width=97 style='width:73pt'></td>
  <td class=xl6327136 width=210 style='width:158pt'></td>
  <td class=xl6327136 width=104 style='width:78pt'></td>
  <td class=xl6327136 width=170 style='width:128pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=110 style='width:83pt'></td>
 </tr>
 <tr class=xl6527136 height=25 style='mso-height-source:userset;height:18.75pt'>
  <td height=25 width=15 style='height:18.75pt;width:11pt' align=left
  valign=top><!--[if gte vml 1]><v:shapetype id="_x0000_t75" coordsize="21600,21600"
   o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe" filled="f"
   stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="Picture_x0020_1" o:spid="_x0000_s10241" type="#_x0000_t75"
   style='position:absolute;margin-left:6.75pt;margin-top:3pt;width:43.5pt;
   height:30.75pt;z-index:1;visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQBamK3CDAEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhvROFGcle3tSdZNgokh7Rjb3+8vyWK5tSObIJBxWPPbsuIM
UDltsK/5x/qleOCMokQtR4dQ8x0QXzbXV4v1zgOxRCPVfIjRPwpBagArqXQeMHU6F6yM6Rh64aX6
lD2Iu6q6F8phBIxFzBm8WbTQyc0Y2fM2lWcTjz1nT/NcXlVzYzOf6+JPIsBIJ4j0fjRKxnQ3MaE+
8SoOTmUi9zM0GE83SfzMhtz57fRzwYF7S48ZjAa2kiG+SpvMhQ4kvFFxEyBNlf/nZFFLhes6o6Bs
A61m8ih2boF2XxhgujS9Tdg7TMd0sf/X5hsAAP//AwBQSwMEFAAGAAgAAAAhAAjDGKTUAAAAkwEA
AAsAAABfcmVscy8ucmVsc6SQwWrDMAyG74O+g9F9cdrDGKNOb4NeSwu7GltJzGLLSG7avv1M2WAZ
ve2oX+j7xL/dXeOkZmQJlAysmxYUJkc+pMHA6fj+/ApKik3eTpTQwA0Fdt3qaXvAyZZ6JGPIoiol
iYGxlPymtbgRo5WGMqa66YmjLXXkQWfrPu2AetO2L5p/M6BbMNXeG+C934A63nI1/2HH4JiE+tI4
ipr6PrhHVO3pkg44V4rlAYsBz3IPGeemPgf6sXf9T28OrpwZP6phof7Oq/nHrhdVdl8AAAD//wMA
UEsDBBQABgAIAAAAIQB+bgCwbgIAAKcFAAASAAAAZHJzL3BpY3R1cmV4bWwueG1srFTbbtwgFHyv
1H9AvO8aO3a9a8WOtt6kqhSlUdV+AME4RrXBAvYSVf33HsDOaptKrZq+YQ7MDGfm+PLqOPRoz7UR
SpY4XhKMuGSqEfKxxF+/3CxWGBlLZUN7JXmJn7jBV9XbN5fHRhdUsk5pBBDSFLBR4s7asYgiwzo+
ULNUI5dQbZUeqIVP/Rg1mh4AfOijhJB3kRk1p43pOLfbUMGVx7YHVfO+3wQK3gi7MSUGDW53OtNq
NYTTTPUVuYycKLf0CLD41LbVKsuT7LnkdnxVq0MVh223nPdc/WIVkwkMSv6GRz7RWfVMMYP8Spuu
s4sZZVIyc1TJ73njNcnmK2fEM90oWOCV+3vB7vUk4m5/r5FoSpxgJOkALkHV7jRHMY5OZ8INWgDK
rWLfzOQb/QfXBiokcKm6o/KRb8zImYX0OLbgAUgKdP7zTO5DL8Yb0YNJtHDrV8sI8fur8Km2FYxv
FdsNXNqQQM17aiH9phOjwUgXfHjg0Ez9sYkxYhB+Cx0dtZAWYkcLfrS3xk4rtNOixN+T1YaQdfJ+
UWekXqQkv15s1mm+yMl1npJ0Fddx/cPdjtNiZzi0n/bbUcxPj9MXHgyCaWVUa5dMDVHQPc8O6I5J
FDzY077ExDfeSwMDThJh6TrstBqruWXdzPiC78+T6vkcVAvmfQbDndnPwJPxJ3PdKJrRZZQWx1YP
/4MZ2oCOJfYTjdETJI6keZ651/tHIwbVLEvSDH5jDOoXMFGJr4NSp8IdHLWxH7h6tSLkgCAn0Aof
DLqHXISmzBRTV0If/CjA8E0T2QuI4JZaOg/N2R9vuhn+sNVPAAAA//8DAFBLAwQUAAYACAAAACEA
qiYOvrwAAAAhAQAAHQAAAGRycy9fcmVscy9waWN0dXJleG1sLnhtbC5yZWxzhI9BasMwEEX3hdxB
zD6WnUUoxbI3oeBtSA4wSGNZxBoJSS317SPIJoFAl/M//z2mH//8Kn4pZRdYQde0IIh1MI6tguvl
e/8JIhdkg2tgUrBRhnHYffRnWrHUUV5czKJSOCtYSolfUma9kMfchEhcmzkkj6WeycqI+oaW5KFt
jzI9M2B4YYrJKEiT6UBctljN/7PDPDtNp6B/PHF5o5DOV3cFYrJUFHgyDh9h10S2IIdevjw23AEA
AP//AwBQSwMEFAAGAAgAAAAhAJCMVLMYAQAAhwEAAA8AAABkcnMvZG93bnJldi54bWxckN1LwzAU
xd8F/4dwBV/EpR/WtXXpGIIwkA02ffEttukHNsmWxLXbX7/bTZn4eM7N79xzM5n2siU7YWyjFQN/
5AERKtdFoyoG728v9zEQ67gqeKuVYLAXFqbZ9dWEp4Xu1Ers1q4iGKJsyhnUzm1SSm1eC8ntSG+E
wlmpjeQOpaloYXiH4bKlgec9UskbhRtqvhHPtci/1t+SwXzbe8lyu/jYBrvQP3Sv47vF/pOx25t+
9gTEid5dHv/Q84JBAMMpeAZk2K9vZyqvtSHlStjmgOXPfmm0JEZ3gya5bhng0aiXZWmFYxDGvocO
Tn6dOBoHEdAh1OkziqtOKEb8Qf3Ei/6xD0kUooUwvVQ6icv/ZUcAAAD//wMAUEsDBAoAAAAAAAAA
IQCNDDZnhhoAAIYaAAAUAAAAZHJzL21lZGlhL2ltYWdlMS5wbmeJUE5HDQoaCgAAAA1JSERSAAAA
TgAAAEMIBgAAAUUzA1UAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAh1QAA
IdUBBJy0nQAAGhtJREFUaEPtmwlYU8e+wA+yhH1VkEVQFEQF1GpdWi3V9tq6tr1X2/p61VdrXVEU
EIUknOxh311wQ+0qdasLECBkP9khImrFtrbaXq+01d621mqFvJmTCRAIISh9ve999/d9/y8zc+ac
zPnP9p//zMEGjCeu+g0F7eNv+/XxWEqtEUWxhL2av6MghgUwxLdREMPWnTZiqXXGnee+9kMpXbjz
mx/AX19c8RmWLDC+fEAzGdv4aedTO3FhX3xEBsCTKo1GR5gZ/n0EQ/gemW5mzOZWCgr2YmKhyBcF
bTN3r3YOCloykivjjMtR1KLoILC2XOeMgpgjVfM7Cpp444ghGgUxbGt1b7X0ZBxbQpuYo+RCNaEk
zGg0DkHBboAMRw23PLAtVcaZucLnYNIrhxoXkNfMOO+of4glgzoGmcgnQqUDNle1DiMzDIgZRer/
QkH7ONbyo78vvUEQwVK8iZL6xz9TdMyTJn7ky1BVTClQpc0oUaUFcTT7PDLED0I4sqMom23cMyT3
UNCSLdVGb5rkKxSzwJMuvYaCliSduzoOBS2Yl1/jT2oXajZN3H/lmnFgXrBsJhBYNUnVxsVl6tfJ
h5Lxml4PHVcgm4CCXbjzLpC9sBNYIijb6zsm7JKMiNunCusuKJeJxJoYFOoikCUWoyB4f9BuQdt1
SRF+77L+9F2yMcGBw/wnMAzT+gP0vV3wNxJXBLrsFN6ev083Fwdt2zmtnk1mQID27uDP0V6F4QC6
5BaZ2BdTizRP+7HUehS1Cnigszdb0zKSryD7yf8us3dp53kxtd94MlV6b7qI9gxo0D70hjQfpkbp
xtJ/nVDWNAll7ZvYfNXH605dGYminYxi1vdqtAsqLq54bpcuCUXtY+EeIpSsaSu8ftQQi4KW+NJE
1hUPmwNowJR00S8oxYJhbPloFOwikCW9goKdJPCFy4Lp4mpze0uoELmiS5140cR3UbCLcbnS6SjY
xYbTxvmlRFxX463v9bqOmcSXKNjFO/tb/FGQZARLfpIczTefNy4o168g++s2gTG1smU4ykLiQxd+
hIJd4JUtLihoAnUhBzQ2k7rbfM6IrT1hWbpkQRoKdbGs28M86NKu/gjo2enj+Oc65+8phapjKNjF
06XqABTsLNUQoCPv1JpvyNc16w3+dmsqrhniXhWHxRWoXoG/Q5IE7dhWMI6Zb9gAjAP4MLICkADd
DafWfgMvO3MudFkeZtypDQ9RcEBE5SgLULCLubv15Gw0lq9IgpURwCQ+H0YX6V87+nmgJ668Aa9N
LtLn+FIbrlLSGt4Zsr2etKwmFjZZNz6Gs+UqFLSLEXziPApax4sub3tht+ZFFLXKxHzl234sxc8o
ah9z9zS9FpdHCHxoDQI/mpC78iNt7344mFS1tlI4565Pct9RNRlDsuLIpckV1429+vGgM7lUHxLG
V513Zaj/Fc6RVy7/oHkmujQgZpfpnwvkEAK3TOK3uAI1b1XF9ccv/KQC9ZpwHlGDov0SgQvPYFvO
G+ftUU1ESTYRiYxOfkzF1fUnDIEoyT4W7rswO5glPYyi/RKWKTKQHRmOW2AA/Et543x0qV88aZJf
xpeJPFHUNlOAge1ClfyKov2y/FBjFDnawIKZhywgz+3SJ6MsNkkA/Tw6W1GKoraJyVEVjctV0FDU
Fg7Y2pO/R6ZXa3BRiyeWgsbRbsPg2GzlAZTXJk7MJts2kRkXqqxjHFsRgaJW2Xbshhu2FWgKtDEs
8azRe7vgm2WVlY6O2xvuWQz4QALoIgW6rU8CaA2Xpu9vDkJRGzAbH2Dd1k49KdeBa3CGNhvD20CV
JlUZ/bbXXYfX/TJEt7FtYGKBAqsbvIDL1prvyJv7YHK+oiKKZ8Uy6Ikrp+lBr4kaAUbmyO7VRgoo
ZBhb9gnKQhKJNxT4plWX+6YC2Xam3DfxRPmIbacOosu9GJJSu2PJgaaFKGoDuv4BWHb20tyUIvWy
XgUD1Rabp6RP4gsngmWfaUkBpltTlcLrpjwmLdYYHbcKTMv0HvjQROcW71P3Xnr0BBgDj8bkWJp3
UXxFtmmYQAILAGyCeft0i57Nlc3HNp4hq5YsBCxMz5dALwLvcQBVXVlpdESPJnGgaa5bXyT3YGap
/s2RWarjKIqNzKg9hW0604ZtOdcG2pDpN/H8d/P3qqcm8ITLsdWftGHrToN0cG2roA1oqw20yd6S
AgVc21rThm061zarQBaJ/gLzpQpttkkL3Ony1lWnTHbGxlOXVgThon9EcGV62BY9qKIfRnAkhpgs
6R685tozvgyZOoSrvr6j7gufuGJiTRBDUhec3fQjuNUhKFt3M5ApOjyUKVHCZ7mwde0JuMjJMV3W
DuOQObs1S0J40i7/hD14UmX3URB7uli9edbB1mGwcEFsWYHIaHSKySE+eePI5egwtlweySea8Job
/ov3a1cF4ZLaUC7RAu8DeWtCWLLjo3O1ZGcIyGx4a0aJ9tlAupg0ZzadvBzgkiGzX2vdicpRfTqM
Kb1N3ADj2iATwpJeBM1Hh6JPxiqwmgvnSz92Z2l/8GEovhvO0340nCFZF4iLXvRIr4+fvksZPwuI
LwiD6egFYLetCeIqC/3Yap1rekOHK0PXEsGTs7n1X9gx2P6HP5n4owIPMHN0WsHj+HWT2cJLEWDc
shjL/lBwweeBYWxpmi9Hp3HLlP/qw1TeDuBoqycU6PeFsSTboWsCSnSWPG0Ej8gbylWf9mISN13p
sl99WBrl+FxVIo7bMdAOhJhs1XKfTOlDV1xtiM1XJRarWr3RJbvJJ274j+LJqW4szddDGYrLz5Tq
p6JLj8fpK995UTIU7aN4shKUZJMRdGGZHNyDon3S0mJ0CeMTV8F8+tsqK86M/jEaHVwy5L+s/lAf
glJsAtqXi8OWaqMPTfI5SuqX+CJF7FCGrLcjpD+mlujfmFWmL0LRfvGn1ovNE//8Q9o4lNwvfri8
9aUDF8aiqH24pYvbF5Xr3FHUJsvKgIkOtEZ6LoBt55Im7Jw3+2P92cuxAUxLW9AmqypbhntQG/6B
ov3inSJ4QHqJzHYcMI+mFapWosv94rZT2Nv93BfxwGSOL1RuQNF+8d9Zd6Nz9YVsN5edIqtGpTUC
cJnda2PMPUN8jyA67Jro4/Daj6EJ5LCtpqOnkTmKKytG2WwSlSV/d8Px5k7bziaOdGUHCtokilaT
gr3ziRGuuoLowlPkgqdb4YCBCT1nDqbcfbP0vcbxY7Ml21G0b3CRyMkdV7ahqE0cN599iK0/ZfTc
WnUHmt2OacIehaszjuDIGlH2PsGBbehPl9j2e0Hm7tfHhyGr1RYxXOlxcs0KXaVg/TArW5owhifZ
SlatuXrhb7KgI6G4x5aHFSgZ0mYU7Bu/jPr5TxcSZShqlZffU3mTvly4mIH+EVBISnLNIzhwU3Y2
PDQtgsiCkdcDqPXketYW7pyLX6Bg34zNklPHcOVdm7hWCGaIDUgrpgLCnrr5vPGVQ5oRC/c3TyHT
zAWHA3PiOePsXInNXRMnuu4mCtogqYb61yMXV6BYLxYeMMR2timygEBAIcJZXcOBX7rwtsk7Dqp9
E1gygnbpkfjpD+iyVTyY+jso2DdBmQ25S9+/1Gfh/HHpl52F696uKro8wtnyK14Wq/0kIBtPlCd/
oOu9vYawq3BYcg31lSMGq4VbdKBpHrmC7144oLkZxcTxl4urKHGg4ffaBugmE/IkI9CjekNvsr4d
2p1ZpapMD5rY6rYwZaf4rkXBgNactteRjnz3lLpWsirR/NrligBCVj1IB/NvfLY0k3xYD3yzWvq3
ZsZmKV4axZWUo2gnL+xWbyCrEBXK3N7GZyt2bD7dMh42emsOxM78qOdiSQLj6tNXLGw+uHfphUsM
KNo3a459NsqPLrqAoiRrdUZnLLmuw/THQFAvpaTWkbvZzomg0cPxDv45vN6zcN0LCF5gNEtyknww
Ytsxwi2CZ6dl4oZrLA6UTMhSlJBDAvxzWD1oiIjLks2fyqpdgq07SToQ+9ScWczaA9WPi653dqCE
fU2TorNk76CobZyois7tJ/hWDonV32GbziJHDhDw65FSSw6sLms+acPWnmzDNpwB12raQMFMDpu+
HDnJ4HpSdZt3So2U/ANACEeyfv2Jz+3zqA9nSYhXjzQnoOgfTgBTXo2C/bPwYONEf5rwIopiMbkq
tRNd0TG1UH06trT5yEiuVOHP093HRUanUC5xKyBTXDo2T9MEmraDD0N+P4Au/uTpUnXy87sNoHPJ
W4bxdDdeLNWH+OAKYmqhpiQ6SzkL21w1Hj0emGgD3LpzSpc8erm46wyQ+04hebYhNk952Jcu0o3O
Ud+HHidvasMRmA7+WLdgn3pSGFv0NowDHEbxVeTxBbi4Hs2Ts32oopq4POWeWSW6jWA8JQdkXPTZ
0GFMhQiG7WbObv3quHx1LopiI1iSn+Dv7P3N++BvdJaCKK6qokTlqGuHM6UfR+eqKuARhxCeWh/O
lZ+JKTK8GVdAPBXFJ7TBbFnzsjL18JE82Tl4byhXfXkobvLHBbOlDZPyiYE3IdcM6b9WfdT7nMBg
saxS5+NO7eNwTX/Ao1KOGUT7gooL/42SBo3F+w0vD8lQPnrxiC4cJQ0cOJSEsGTFFFz9S0w2UZxf
c8Ni438grP3gs5hRXOnHbgzlvWCeei9+5lu7lp52MalI+44vS613S5f+7sNUKUbxFDnjconXpxWr
5oTi9aTjEPx5fHyRclYoR7JwJFeeFsrXn/Jlyn9wY6ru+jCk55/bpVmMHvfHsvqTS5Nj85VbhmZK
doVzpAJ4JCyEKRXEZCuPzSjSsN967/K8qtaOPk/9/Yf/8EcATLVlZSLPZ4t04VPzFLCJLhifp04O
5hFloXyNwI9JNAXg0u+ddop+dM8QtVN2AklraHdNE1mIyw5Ru1s6vC6+70WX/ujHkLcOYxMNQVxt
eVyhmhaTLV0UxlXEgr4ZtvaMff6nfytoNV9OnFigyh/GEN90xtUPHfCmR+6Z8o5huOSrSD5x9Nk9
2sQVJz+bRe5ErNU5g6nVCUwUjiZPsRE6LoDA355COjUcMBwfgsG8cFtvbblzVp3OZ9GhC0/N3qvb
NCZL8fEwXPQPFxrRjuG6R5QM2T0wsQpfPKBf0dP2/lNZeuyGGzyT406Vyyk04l++uPyfI3O0B+Gm
7IQc7ehcgcEDZR0wrx/SPvW3/drnUXTggJa+/IPPhsbkqaZMKNBkDGUSWjdQgR4M1fWoHOWeNz+8
1P/G7mDzXx80+0VnqY670JT3I/jKs6+/f8X6ke3HZE6xaopXsuCW41aBMT5Lkg69XujSEwM9tvEF
mlRvjrrZnS7/18xderPt+ccyPp/w92XIRV406c+JJ67adSJoILxYolhOgf5G6GIBi7shyTUPI9ly
XgJuHDTlkYAuH5uvSndOlz0YnaVgwhaKrgw+cF86jC2TA+Pn+pzdmoHtGthBHF++xiVZcI90aMDV
L1zdJtcYHVLrO+IKlBZL8MEAOopjc4lcR6rs1+gcOXTND77ycFBDoRx5rlO6/N6EHMLuzQV7mV2o
ZDhtqWk3eYGAwqDX0ew2IF0H9cbhbKL+5YONj/GpQt9AB01klvKMF112MwCvtWsPbUA8U6hc4kyX
fxPBlZ6Dh/FQ8qAwjtuQ5LAVKAc6vkk/SzelWfhZ6o2BbIV0c9XAt1JtEZ+rGuWRKf/y2b2Gv6Kk
wSECF7kGZDY0e2TUPwQzVZ8e08fhRodpg2j5YcNKZ+RWJpVG+h17Ks4kXlTpz0mnrwzaUAFXvqEc
WX4ET3oRtMDB20Cff7DpbWc6YQxmKyw/o3oC1rynCgtMOnWFsvb43Sm4YClMm5gje42SJv7esov2
VhwUT7rkqxkl2mnkwwaBSQXEi64MzZ05pZolKOnJCecSErDM/21SgYp8wSflmRzhaP/U8wby2P67
x43O6061P5vdkAKvLSpXzvLIkNy11eLM4kkV//zKoQuzyYc+IdDEcmQZvg5lifegpCcjo7ot2CVd
9JsvTXx1bKH8ibvHyo9aR/vvENwlndrQd7wJyqdGbOOnHaNx4V5cdN31taOGQD+a6AJQnGnf1poC
UbrDTtFD0of8hOYEnCQo6ZJWD6pIjZ/q8lE/Nn/Zq53nyrn4KIAmPhScpxuKkh+LmUXqRZQdwrsm
BzwUMIOat8USzxkdEqvbxzDFp2DelceaQoNxcUtny7MmsEuD2dclVfAQrIUH9tmQFTwyhNW+vObW
JQM9oGGNhD36bRhN/yCSL0taeox47CN9z5c1vkIqjRz44QsjgUokFQmVCLeTq4zDMoSXVlU0jYQ+
qehsxQkHclMK3dd5P5h5yRPJpnuHgGfFchr2kZ+2PiZxuUSlA8NwzSWt1ur3hgPCM6OBSuEYHszZ
rV/R/Sshe4Fe66klmu2OO8W9ullXqwGCWqFLivDBwj2qRHQ7llBx3TWSLdnlsK3mkUlJXcoiWyvc
ZIPdfeOnxiEbzrSHZ1QfXoQ/pnckpXYH3EtdUmHP6el+cEqpzYTfU8IN4b6OgPcJGDfG8OS4U2rd
7xbjU08FohbkvrP+p5f3aC0/2kUklCiZ2DawFNsCt0KBbAbKgye4N8EDD0Bx68E4+e5JKB0jU84q
wZg14Eo2K67Xh8OPRVIN1YPfPGDFwU2acbmKCse0ht4K6y6k4mqNlLSGBwkltr+Ogq13WaXRsT+B
B4JI99MAASuicgpDf+vZUuXje2XMBDLEVEdO04OpBaoV4/FKuxT3aqk6IIglO42l9j6PYyFIaX5U
yfWUc5eAnS1ymsgT811TBXcp2wR3KMm1dygpdXcoqUi219+hpPUQmLYdXYd5U8A98L5kwZ1QqpB4
6z1tMCpWvwxjiD9y5hi+GJ1l5TPmgTKzWLPagdb4cHKBkmmPMxBa4eFZhAQuj/pUFlIYFO9MueG5
osYo0EYdYrgNOU6JVWC9ipZenasIJOZ7ofR8HhRzPjhxoDHQZ7vg/luHLjyFimcTYMNdGpIua44p
sF/ZffL8XsN0Cl3VHsWVi2bv0vR9BgSw+n19RAAVzJzk8YxuL9RdSIXBF6w1hjMknyehTxjiOMKz
DonnOsjBvtM7gtas8B54b/cKsCbdnw+VBxW/pdrour3+djxfMpcsZB/ApZYrnfjCiyYSgrXwk+8w
HTXc8gDG6A9eDMWtuDzVFJTci4Qy9SSfnQ2tJvMCFJo0F+ALoJcgX8T0MkO2VneEs6Sn4XdusFuH
pteehUeb4MzYS3HmUx/2KA6KOa+F8qqMrql1vyw92NjnymdBhXr4EFbzzTCOjIuSnpwxWYoSR5qy
Y1Khxuqhno2VhlkeqfU/ki9sfmn4vVKnmGqeFBAfxRKf3vyeyjvlqMEjPOWcGHsXLL3WAcXBT4zh
LEk+A740EFIRSCnbQfe3R0gFIiVCBSLlwXM/cVwJjZw8ehCbq1joSlfeXXxIZ7NlDogdp66M9KCJ
fxrKkl0/822PbWQ4e00r9sb+ftSjSwRWxJQ+ZnNVVzcIy3fDlpV5Wt7bx/0p3X7tkZ73m2X1aS9y
g6gb0D4NYUtLI/nSJmgNoOQnB07xM0s0HAqN+GlCHpGHkjshTx2mCN8GLeQtLK32dSypbqU/TbQU
mg/jsyTTsGThcmyLYNWQ1LqVz5RpX6kU3e78ZnUJWN5E8hQ7gjiS/GnFmq3smmsW4+i8Xao5EVw5
azRPkfPSgQtL8gnTB0lwbfnq4StTxuYomCPY0rwJecoNvJOXO7+896DXz/FMF/6V/DATEJbf4u+Y
Vr+UVFwPJucqItwz5a0v7L/4BkoaPFaBxTco4AkXmuzHGVbOCsDBFdhBW+EBncX7m4NgHNp9sXm6
w27povdf2KuNA+ve6WFs+YVghvQE3AEbkyU/6c9RyxYebgqFz5i7v2maJ674cnwWkfjaiQthPgzZ
leFM6QHzy0dnK9+A5xdA0CGcrzwclaOpufKd6TOLl/Zqn3LHibZInmQ9jPvSRYJApkzinSn5dXqp
Zu2EPM0zjttrH5oP/HRnbI5yvxtd3ObGIchyDDr4GZ17GFt6xReXtz7V43NIyJgsYqsbUFwsV0l+
vQUVN6FQd9ghRbgXW3vGfXGFboYHVfJtBEeaW1lZ6Tg5X/mhPy43zNqlj59WrPKeXqRd4MNW33y6
UPnumx9+EeSPy5rDucTp6DzR0ImFTb6xxfrNk8oMW+DwEJGrLRvGUlQ9XaYePiVL5zO/vHG+F0N+
bzRfRrr1PWkiAZZc9xaoQJcRXEISzFZ84Zgq+LWn4hJ2N65xTpf9HsIQ43B/FyUPPvP2E/7DOAqR
F012f/kHlgfp4AeFG05eHQe7NkrCcPnXIRsrrw9HUYxfdzWSKvhqFNzDgHEws/rE5KmjndPEcZPz
iTEg3jmGwkPt8bmGQGBMT4jmSuNBqwruGtjhXoHO56l8Is6XJo6bVqIa1f0LkpTjlyI6XexAIatB
fN2x1vHd19szS3SvutJlPwaxxDWDegSoL6AhPDaH2OWSIXsQxiGObjzWMgZd+j8BHEqispXZ7jTJ
rzG5qt2iQdy3tYuFcGM6W8nzYGmue+LK36LydKJZZdrl8LtvuIJA2f5UynXfusOtzEkF6lWRuRqB
WybxkwtDfTOKr3x/5Z+xm2+NNScvR8flKkr8cOn3DnTtI2eqsj2QKf4ajDvFSyq0C7n1/wwynxWB
nhN02+NiOkcCnwWeuenI5YC/f9j8l4mFau5ovqLJkyZ95EDTtDszG38PYkm/nVyo5Z68eLvXuPxv
Sa7glsf0QuXI2WXq58bmqNaNzFEfDOUptX4M6W33DMnvrnT5Q1D7P7uwDG2uTP0/HTI1nzsxG6/5
8A2d4sxqvDaEob3mwtR+C/O5MPV3KDTFAzeq7JFnhuiBB112LYSvEo/K1e6LLVRviS9QzI3MFkfB
sRhuOKOi/P8GGp9Q4KRgFnMayvJvBIb9D7VnSuU50Q+AAAAAAElFTkSuQmCCUEsBAi0AFAAGAAgA
AAAhAFqYrcIMAQAAGAIAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwEC
LQAUAAYACAAAACEACMMYpNQAAACTAQAACwAAAAAAAAAAAAAAAAA9AQAAX3JlbHMvLnJlbHNQSwEC
LQAUAAYACAAAACEAfm4AsG4CAACnBQAAEgAAAAAAAAAAAAAAAAA6AgAAZHJzL3BpY3R1cmV4bWwu
eG1sUEsBAi0AFAAGAAgAAAAhAKomDr68AAAAIQEAAB0AAAAAAAAAAAAAAAAA2AQAAGRycy9fcmVs
cy9waWN0dXJleG1sLnhtbC5yZWxzUEsBAi0AFAAGAAgAAAAhAJCMVLMYAQAAhwEAAA8AAAAAAAAA
AAAAAAAAzwUAAGRycy9kb3ducmV2LnhtbFBLAQItAAoAAAAAAAAAIQCNDDZnhhoAAIYaAAAUAAAA
AAAAAAAAAAAAABQHAABkcnMvbWVkaWEvaW1hZ2UxLnBuZ1BLBQYAAAAABgAGAIQBAADMIQAAAAA=
">
   <v:imagedata src="Report%20Master_files/Report%20(Autosaved)_27136_image001.png"
    o:title=""/>
   <x:ClientData ObjectType="Pict">
    <x:SizeWithCells/>
    <x:CF>Bitmap</x:CF>
    <x:AutoPict/>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:9px;margin-top:4px;width:58px;
  height:41px'><img width=58 height=41
  src="{{Config::get('constants.path.img')}}/logoreport.png" v:shapes="Picture_x0020_1"></span><![endif]><span
  style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td height=25 class=xl6527136 width=15 style='height:18.75pt;width:11pt'></td>
   </tr>
  </table>
  </span></td>
  <td colspan=11 class=xl6427136 width=1267 style='width:952pt'><span
  style='mso-spacerun:yes'>         </span>PT. TROCON INDAH PERKASA</td>
 </tr>
 <tr class=xl6727136 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6727136 width=15 style='height:17.25pt;width:11pt'></td>
  <td colspan=11 class=xl6627136 width=1267 style='width:952pt'><span
  style='mso-spacerun:yes'>           </span>LAPORAN MASTER DAN GAJI KARYAWAN</td>
 </tr>
 <tr height=4 style='mso-height-source:userset;height:3.0pt'>
  <td height=4 class=xl6327136 width=15 style='height:3.0pt;width:11pt'></td>
  <td class=xl6327136 width=97 style='width:73pt'></td>
  <td colspan=4 class=xl6827136 width=580 style='width:436pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=110 style='width:83pt'></td>
 </tr>
 <tr height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6327136 width=15 style='height:17.25pt;width:11pt'></td>
  <td colspan=2 class=xl6927136 width=307 style='width:231pt'>PERIODE</td>
  <td colspan=3 class=xl6927136 width=370 style='width:278pt'>: {{ $datafilter->period }}</td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=110 style='width:83pt'></td>
 </tr>
 <tr height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6327136 width=15 style='height:17.25pt;width:11pt'></td>
  <td colspan=2 class=xl7027136 width=307 style='width:231pt'>DEPARTEMEN</td>
  <td colspan=3 class=xl7027136 width=370 style='width:278pt'>: {{ $datafilter->departmentname }}</td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=96 style='width:72pt'></td>
  <td class=xl6327136 width=110 style='width:83pt'></td>
 </tr>
 <tr height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl6327136 width=15 style='height:30.0pt;width:11pt'></td>
  <td class=xl7127136 width=97 style='border-top:none;width:73pt'>Department</td>
  <td class=xl7127136 width=210 style='border-top:none;border-left:none;
  width:158pt'>Karyawan</td>
  <td class=xl7127136 width=104 style='border-top:none;border-left:none;
  width:78pt'>Tanggal Masuk</td>
  <td class=xl7127136 width=170 style='border-top:none;border-left:none;
  width:128pt'>Posisi</td>
  <td class=xl7127136 width=96 style='border-top:none;border-left:none;
  width:72pt'>Gaji Pokok</td>
  <td class=xl7127136 width=96 style='border-left:none;width:72pt'>UM Kantor</td>
  <td class=xl7127136 width=96 style='border-left:none;width:72pt'>Lembur Tetap</td>
  <td class=xl7127136 width=96 style='border-left:none;width:72pt'>Tunj
  Keahlian</td>
  <td class=xl7127136 width=96 style='border-left:none;width:72pt'>Tunj
  Jamsostek</td>
  <td class=xl7127136 width=96 style='border-left:none;width:72pt'>Tunj
  Kesehatan</td>
  <td class=xl7127136 width=110 style='border-left:none;width:83pt'>Gaji Bruto</td>
 </tr>
  @php
   $t_mealtransall = 0; 
   $t_overtimeall = 0; 
   $t_jamsostek = 0; 
   $t_tunjkeahlian = 0; 
   $t_medicalall = 0; 
   $t_netto = 0;  

   $n = 1;
 @endphp
 @foreach($itemdata as $itemdatas)
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6327136 width=15 style='height:15.0pt;width:11pt'></td>
  <td class=xl7227136 width=97 style='border-top:none;width:73pt'>&nbsp;{{ $itemdatas->departmentname }}</td>
  <td class=xl7227136 width=210 style='border-top:none;border-left:none;
  width:158pt'>&nbsp;{{ $itemdatas->employeename }}</td>
  <td class=xl7227136 align=right width=104 style='border-top:none;border-left:
  none;width:78pt'>{{ $itemdatas->joindate }}&nbsp;</td>
  <td class=xl7227136 width=170 style='border-top:none;border-left:none;
  width:128pt'>&nbsp;{{ $itemdatas->positionname }}</td>
  <td class=xl7227136 align=right width=96 style='border-top:none;border-left:
  none;width:72pt'>{{ number_format($itemdatas->basic,0) }}&nbsp;</td>
  <td class=xl7227136 align=right width=96 style='border-top:none;border-left:
  none;width:72pt'>{{ number_format($itemdatas->mealtransall,0) }}&nbsp;</td>
  <td class=xl7227136 align=right width=96 style='border-top:none;border-left:
  none;width:72pt'>{{ number_format($itemdatas->overtimeall,0) }}&nbsp;</td>
  <td class=xl7227136 align=right width=96 style='border-top:none;border-left:
  none;width:72pt'>{{ number_format($itemdatas->tunjkeahlian,0) }}&nbsp;</td>
  <td class=xl7227136 width=96 style='border-top:none;border-left:none;
  width:72pt'>{{ number_format($itemdatas->jamsostek,0) }}&nbsp;</td>
  <td class=xl7227136 width=96 style='border-top:none;border-left:none;
  width:72pt'>{{ number_format($itemdatas->medicalall,0) }}&nbsp;</td>
  <td class=xl7227136 align=right width=110 style='border-top:none;border-left:
  none;width:83pt'>{{ number_format($itemdatas->netto,0) }}&nbsp;</td>
 </tr>
 @php 
 $t_mealtransall += $itemdatas->mealtransall; 
 $t_overtimeall += $itemdatas->overtimeall; 
 $t_jamsostek += $itemdatas->jamsostek; 
 $t_tunjkeahlian += $itemdatas->tunjkeahlian; 
 $t_medicalall += $itemdatas->medicalall; 
 $t_netto += $itemdatas->netto;  
 @endphp
 @endforeach
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6327136 width=15 style='height:15.0pt;width:11pt'></td>
  <td colspan=4 class=xl7427136 width=581 style='width:437pt'>TOTAL</td>
  <td class=xl7327136 align=right width=96 style='border-top:none;border-left:
  none;width:72pt'></td>
  <td class=xl7327136 align=right width=96 style='border-top:none;border-left:
  none;width:72pt'>@php echo number_format($t_mealtransall,0); @endphp&nbsp;</td>
  <td class=xl7327136 align=right width=96 style='border-top:none;border-left:
  none;width:72pt'>@php echo number_format($t_overtimeall,0); @endphp&nbsp;</td>
  <td class=xl7327136 align=right width=96 style='border-top:none;border-left:
  none;width:72pt'>@php echo number_format($t_tunjkeahlian,0); @endphp&nbsp;</td>
  <td class=xl7327136 align=right width=96 style='border-top:none;border-left:
  none;width:72pt'>@php echo number_format($t_jamsostek,0); @endphp; @endphp&nbsp;</td>
  <td class=xl7327136 align=right width=96 style='border-top:none;border-left:
  none;width:72pt'>@php echo number_format($t_medicalall,0); @endphp&nbsp;</td>
  <td class=xl7327136 align=right width=110 style='border-top:none;border-left:
  none;width:83pt'>@php echo number_format($t_netto,0); @endphp&nbsp;</td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=15 style='width:11pt'></td>
  <td width=97 style='width:73pt'></td>
  <td width=210 style='width:158pt'></td>
  <td width=104 style='width:78pt'></td>
  <td width=170 style='width:128pt'></td>
  <td width=96 style='width:72pt'></td>
  <td width=96 style='width:72pt'></td>
  <td width=96 style='width:72pt'></td>
  <td width=96 style='width:72pt'></td>
  <td width=96 style='width:72pt'></td>
  <td width=96 style='width:72pt'></td>
  <td width=110 style='width:83pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>
