@extends('layouts.editor.templatereport')
@section('content')
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
 
</style> 
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 14">
<link rel=File-List href="Insentif_files/filelist.xml">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<style id="Report (Autosaved)_19996_Styles">
<!--table
  {mso-displayed-decimal-separator:"\,";
  mso-displayed-thousand-separator:"\.";}
.xl1519996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6319996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6419996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6519996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6619996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6719996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6819996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6919996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7019996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7119996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:16.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7219996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7319996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7419996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7519996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7619996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7719996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border:.5pt solid windowtext;
  background:#E7E6E6;
  mso-pattern:black none;
  white-space:nowrap;}
.xl7819996
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
-->
</style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="Report (Autosaved)_19996" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=1246 style='border-collapse:
 collapse;table-layout:fixed;width:937pt'>
 <col width=19 style='mso-width-source:userset;mso-width-alt:694;width:14pt'>
 <col width=37 style='mso-width-source:userset;mso-width-alt:1353;width:28pt'>
 <col width=116 style='mso-width-source:userset;mso-width-alt:4242;width:87pt'>
 <col width=232 style='mso-width-source:userset;mso-width-alt:8484;width:174pt'>
 <col width=93 span=5 style='mso-width-source:userset;mso-width-alt:3401;
 width:70pt'>
 <col width=100 style='mso-width-source:userset;mso-width-alt:3657;width:75pt'>
 <col width=74 style='mso-width-source:userset;mso-width-alt:2706;width:56pt'>
 <col width=90 style='mso-width-source:userset;mso-width-alt:3291;width:68pt'>
 <col width=113 style='mso-width-source:userset;mso-width-alt:4132;width:85pt'>
 <tr class=xl6519996 height=7 style='mso-height-source:userset;height:5.25pt'>
  <td height=7 width=19 style='height:5.25pt;width:14pt' align=left valign=top><!--[if gte vml 1]><v:shapetype
   id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
   path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="Picture_x0020_1" o:spid="_x0000_s5121" type="#_x0000_t75"
   style='position:absolute;margin-left:7.5pt;margin-top:1.5pt;width:43.5pt;
   height:36.75pt;z-index:1;visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQBamK3CDAEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhvROFGcle3tSdZNgokh7Rjb3+8vyWK5tSObIJBxWPPbsuIM
UDltsK/5x/qleOCMokQtR4dQ8x0QXzbXV4v1zgOxRCPVfIjRPwpBagArqXQeMHU6F6yM6Rh64aX6
lD2Iu6q6F8phBIxFzBm8WbTQyc0Y2fM2lWcTjz1nT/NcXlVzYzOf6+JPIsBIJ4j0fjRKxnQ3MaE+
8SoOTmUi9zM0GE83SfzMhtz57fRzwYF7S48ZjAa2kiG+SpvMhQ4kvFFxEyBNlf/nZFFLhes6o6Bs
A61m8ih2boF2XxhgujS9Tdg7TMd0sf/X5hsAAP//AwBQSwMEFAAGAAgAAAAhAAjDGKTUAAAAkwEA
AAsAAABfcmVscy8ucmVsc6SQwWrDMAyG74O+g9F9cdrDGKNOb4NeSwu7GltJzGLLSG7avv1M2WAZ
ve2oX+j7xL/dXeOkZmQJlAysmxYUJkc+pMHA6fj+/ApKik3eTpTQwA0Fdt3qaXvAyZZ6JGPIoiol
iYGxlPymtbgRo5WGMqa66YmjLXXkQWfrPu2AetO2L5p/M6BbMNXeG+C934A63nI1/2HH4JiE+tI4
ipr6PrhHVO3pkg44V4rlAYsBz3IPGeemPgf6sXf9T28OrpwZP6phof7Oq/nHrhdVdl8AAAD//wMA
UEsDBBQABgAIAAAAIQDXOoyQZwIAAKYFAAASAAAAZHJzL3BpY3R1cmV4bWwueG1srFTbjtMwEH1H
4h8sv7dxQrJtoyar0u4ipNVSIfgAr+M0Fokd2e5lhfh3xnayVVUQiOVt4vHMOZ5zJsvbU9eiA9dG
KFngeEow4pKpSshdgb9+uZ/MMTKWyoq2SvICP3ODb8u3b5anSudUskZpBC2kyeGgwI21fR5FhjW8
o2aqei4hWyvdUQufehdVmh6heddGCSE3kek1p5VpOLebkMGl722Pas3bdhUgeCXsyhQYOLjT4U6t
VRduM9WWZBk5Ui70HSD4VNflIkuyc8qd+KxWx7HCheOZy8cLMlZAylf4zmc4q14gyuTXsHGcviO/
wR1KrnDnZDHLQrsL4BGuFyzgysNWsK0eSDwethqJqsAJRpJ2oBJk7V5zFOPofCdU0By6PCj2zQy6
0X9QraNCApZaN1Tu+Mr0nFlwj0MLGgClAOc/L+g+taK/Fy2IRHMXv5pGsN9fmU/VtWB8o9i+49IG
B2reUgvuN43oDUY6590Th2Hqj1WMEQPzW5hor4W0YDua85N9MHaI0F6LAn9P5itCFsn7yToj60lK
ZneT1SKdTWbkbpaSdB6v4/UPVx2n+d5wGD9tN70Ynx6nVxp0gmllVG2nTHVR4D3uDvCOSRQ0ONC2
wMQP3lMDAc4UIXQTdlyN1dyyZkS8wvvzpno816oG8T6D4E7sl8aD8Gdx3Sqa3nmU5qdad/8DGcaA
TgX2G43RMzjObap7vH8zYpDMsiSFM8Qgnd7czJJsGI4j4S722tgPXL2aEHKNwCYwCe8LegBbhJmM
EMNQwhj8JsDuDQvZCnDghlo67szFD2+oDD/Y8icAAAD//wMAUEsDBBQABgAIAAAAIQCqJg6+vAAA
ACEBAAAdAAAAZHJzL19yZWxzL3BpY3R1cmV4bWwueG1sLnJlbHOEj0FqwzAQRfeF3EHMPpadRSjF
sjeh4G1IDjBIY1nEGglJLfXtI8gmgUCX8z//PaYf//wqfillF1hB17QgiHUwjq2C6+V7/wkiF2SD
a2BSsFGGcdh99GdasdRRXlzMolI4K1hKiV9SZr2Qx9yESFybOSSPpZ7Jyoj6hpbkoW2PMj0zYHhh
iskoSJPpQFy2WM3/s8M8O02noH88cXmjkM5XdwVislQUeDIOH2HXRLYgh16+PDbcAQAA//8DAFBL
AwQUAAYACAAAACEAqDwafBcBAACHAQAADwAAAGRycy9kb3ducmV2LnhtbEyQy07DMBBF90j8g2Uk
dtRJSKEJcaoIhIqKQLSFBTsrdh4itiPbbQJfzzShCqvRPM6dO5Mse9mggzC21opif+ZhJFSuea1K
it93j1cLjKxjirNGK0Hxt7B4mZ6fJSzmulMbcdi6EoGIsjGjuHKujQmxeSUkszPdCgW9QhvJHKSm
JNywDsRlQwLPuyGS1Qo2VKwV95XIv7Z7CXsf1qH5eHkOV3LXRau9y3r++Ubp5UWf3WHkRO+m4T/6
iVMc4OMpcAZOwV/fZCqvtEHFRtj6B8yP9cJoiYzuKIZjc90MEfLXorDCwVTkzcfOqRLNA6iQo6jT
IwqrBhTif3ThRbfzoXVifT+89gaYTJbSBJLpf+kvAAAA//8DAFBLAwQKAAAAAAAAACEAjQw2Z4Ya
AACGGgAAFAAAAGRycy9tZWRpYS9pbWFnZTEucG5niVBORw0KGgoAAAANSUhEUgAAAE4AAABDCAYA
AAFFMwNVAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAIdUAACHVAQSctJ0A
ABobSURBVGhD7ZsJWFPHvsAPsoR9VZBFUBREBdRqXVot1fbaura9V9v6etVXa11RFBCFJJzsYd9d
cEPtKnWrCxAgZD/ZISJqxba22l6vtNXettZqhbyZkwkQCCEofb3vfff3ff8vM3PmnMz5z/af/8zB
BownrvoNBe3jb/v18VhKrRFFsYS9mr+jIIYFMMS3URDD1p02Yql1xp3nvvZDKV2485sfwF9fXPEZ
liwwvnxAMxnb+GnnUztxYV98RAbAkyqNRkeYGf59BEP4HpluZszmVgoK9mJiocgXBW0zd692Dgpa
MpIr44zLUdSi6CCwtlznjIKYI1XzOwqaeOOIIRoFMWxrdW+19GQcW0KbmKPkQjWhJMxoNA5BwW6A
DEcNtzywLVXGmbnC52DSK4caF5DXzDjvqH+IJYM6BpnIJ0KlAzZXtQ4jMwyIGUXq/0JB+zjW8qO/
L71BEMFSvImS+sc/U3TMkyZ+5MtQVUwpUKXNKFGlBXE0+zwyxA9COLKjKJtt3DMk91DQki3VRm+a
5CsUs8CTLr2GgpYknbs6DgUtmJdf409qF2o2Tdx/5ZpxYF6wbCYQWDVJ1cbFZerXyYeS8ZpeDx1X
IJuAgl248y6QvbATWCIo2+s7JuySjIjbpwrrLiiXicSaGBTqIpAlFqMgeH/QbkHbdUkRfu+y/vRd
sjHBgcP8JzAM0/oD9L1d8DcSVwS67BTenr9PNxcHbds5rZ5NZkCA9u7gz9FeheEAuuQWmdgXU4s0
T/ux1HoUtQp4oLM3W9Mykq8g+8n/LrN3aed5MbXfeDJVem+6iPYMaNA+9IY0H6ZG6cbSf51Q1jQJ
Ze2b2HzVx+tOXRmJop2MYtb3arQLKi6ueG6XLglF7WPhHiKUrGkrvH7UEIuClvjSRNYVD5sDaMCU
dNEvKMWCYWz5aBTsIpAlvYKCnSTwhcuC6eJqc3tLqBC5okudeNHEd1Gwi3G50uko2MWG08b5pURc
V+Ot7/W6jpnElyjYxTv7W/xRkGQES36SHM03nzcuKNevIPvrNoExtbJlOMpC4kMXfoSCXeCVLS4o
aAJ1IQc0NpO623zOiK09YVm6ZEEaCnWxrNvDPOjSrv4I6Nnp4/jnOufvKYWqYyjYxdOl6gAU7CzV
EKAj79Sab8jXNesN/nZrKq4Z4l4Vh8UVqF6Bv0OSBO3YVjCOmW/YAIwD+DCyApAA3Q2n1n4DLztz
LnRZHmbcqQ0PUXBAROUoC1Cwi7m79eRsNJavSIKVEcAkPh9GF+lfO/p5oCeuvAGvTS7S5/hSG65S
0hreGbK9nrSsJhY2WTc+hrPlKhS0ixF84jwKWseLLm97YbfmRRS1ysR85dt+LMXPKGofc/c0vRaX
Rwh8aA0CP5qQu/Ijbe9+OJhUtbZSOOeuT3LfUTUZQ7LiyKXJFdeNvfrxoDO5VB8Sxledd2Wo/xXO
kVcu/6B5Jro0IGaX6Z8L5BACt0zit7gCNW9VxfXHL/ykAvWacB5Rg6L9EoELz2Bbzhvn7VFNREk2
EYmMTn5MxdX1JwyBKMk+Fu67MDuYJT2Mov0SlikykB0ZjltgAPxLeeN8dKlfPGmSX8aXiTxR1DZT
gIHtQpX8iqL9svxQYxQ52sCCmYcsIM/t0iejLDZJAP08OltRiqK2iclRFY3LVdBQ1BYO2NqTv0em
V2twUYsnloLG0W7D4Nhs5QGU1yZOzCbbNpEZF6qsYxxbEYGiVtl27IYbthVoCrQxLPGs0Xu74Jtl
lZWOjtsb7lkM+EAC6CIFuq1PAmgNl6bvbw5CURswGx9g3dZOPSnXgWtwhjYbw9tAlSZVGf22112H
1/0yRLexbWBigQKrG7yAy9aa78ib+2ByvqIiimfFMuiJK6fpQa+JGgFG5sju1UYKKGQYW/YJykIS
iTcU+KZVl/umAtl2ptw38UT5iG2nDqLLvRiSUrtjyYGmhShqA7r+AVh29tLclCL1sl4FA9UWm6ek
T+ILJ4Jln2lJAaZbU5XC66Y8Ji3WGB23CkzL9B740ETnFu9T91569AQYA4/G5Fiad1F8RbZpmEAC
CwBsgnn7dIuezZXNxzaeIauWLAQsTM+XQC8C73EAVV1ZaXREjyZxoGmuW18k92Bmqf7NkVmq4yiK
jcyoPYVtOtOGbTnXBtqQ6Tfx/Hfz96qnJvCEy7HVn7Rh606DdHBtq6ANaKsNtMnekgIFXNta04Zt
Otc2q0AWif4C86UKbbZJC9zp8tZVp0x2xsZTl1YE4aJ/RHBletgWPaiiH0ZwJIaYLOkevObaM74M
mTqEq76+o+4Ln7hiYk0QQ1IXnN30I7jVIShbdzOQKTo8lClRwme5sHXtCbjIyTFd1g7jkDm7NUtC
eNIu/4Q9eFJl91EQe7pYvXnWwdZhsHBBbFmByGh0iskhPnnjyOXoMLZcHsknmvCaG/6L92tXBeGS
2lAu0QLvA3lrQliy46NztWRnCMhseGtGifbZQLqYNGc2nbwc4JIhs19r3YnKUX06jCm9TdwA49og
E8KSXgTNR4eiT8YqsJoL50s/dmdpf/BhKL4bztN+NJwhWReIi170SK+Pn75LGT8LiC8Ig+noBWC3
rQniKgv92Gqda3pDhytD1xLBk7O59V/YMdj+hz+Z+KMCDzBzdFrB4/h1k9nCSxFg3LIYy/5QcMHn
gWFsaZovR6dxy5T/6sNU3g7gaKsnFOj3hbEk26FrAkp0ljxtBI/IG8pVn/ZiEjdd6bJffVga5fhc
VSKO2zHQDoSYbNVyn0zpQ1dcbYjNVyUWq1q90SW7ySdu+I/iyaluLM3XQxmKy8+U6qeiS4/H6Svf
eVEyFO2jeLISlGSTEXRhmRzcg6J90tJidAnjE1fBfPrbKivOjP4xGh1cMuS/rP5QH4JSbALal4vD
lmqjD03yOUrql/giRexQhqy3I6Q/ppbo35hVpi9C0X7xp9aLzRP//EPaOJTcL364vPWlAxfGoqh9
uKWL2xeV69xR1CbLyoCJDrRGei6AbeeSJuycN/tj/dnLsQFMS1vQJqsqW4Z7UBv+gaL94p0ieEB6
icx2HDCPphWqVqLL/eK2U9jb/dwX8cBkji9UbkDRfvHfWXejc/WFbDeXnSKrRqU1AnCZ3WtjzD1D
fI8gOuya6OPw2o+hCeSwraajp5E5iisrRtlsEpUlf3fD8eZO284mjnRlBwraJIpWk4K984kRrrqC
6MJT5IKnW+GAgQk9Zw6m3H2z9L3G8WOzJdtRtG9wkcjJHVe2oahNHDeffYitP2X03Fp1B5rdjmnC
HoWrM47gyBpR9j7BgW3oT5fY9ntB5u7Xx4chq9UWMVzpcXLNCl2lYP0wK1uaMIYn2UpWrbl64W+y
oCOhuMeWhxUoGdJmFOwbv4z6+U8XEmUoapWX31N5k75cuJiB/hFQSEpyzSM4cFN2Njw0LYLIgpHX
A6j15HrWFu6ci1+gYN+MzZJTx3DlXZu4VghmiA1IK6YCwp66+bzxlUOaEQv3N08h08wFhwNz4jnj
7FyJzV0TJ7ruJgraIKmG+tcjF1egWC8WHjDEdrYpsoBAQCHCWV3DgV+68LbJOw6qfRNYMoJ26ZH4
6Q/oslU8mPo7KNg3QZkNuUvfv9Rn4fxx6Zedheveriq6PMLZ8iteFqv9JCAbT5Qnf6Drvb2GsKtw
WHIN9ZUjBquFW3SgaR65gu9eOKC5GcXE8ZeLqyhxoOH32gboJhPyJCPQo3pDb7K+HdqdWaWqTA+a
2Oq2MGWn+K5FwYDWnLbXkY5895S6VrIq0fza5YoAQlY9SAfzb3y2NJN8WA98s1r6t2bGZileGsWV
lKNoJy/sVm8gqxAVytzexmcrdmw+3TIeNnprDsTO/KjnYkkC4+rTVyxsPrh36YVLDCjaN2uOfTbK
jy66gKIka3VGZyy5rsP0x0BQL6Wk1pG72c6JoNHD8Q7+Obzes3DdCwheYDRLcpJ8MGLbMcItgmen
ZeKGaywOlEzIUpSQQwL8c1g9aIiIy5LNn8qqXYKtO0k6EPvUnFnM2gPVj4uud3aghH1Nk6KzZO+g
qG2cqIrO7Sf4Vg6J1d9hm84iRw4Q8OuRUksOrC5rPmnD1p5swzacAddq2kDBTA6bvhw5yeB6UnWb
d0qNlPwDQAhHsn79ic/t86gPZ0mIV480J6DoH04AU16Ngv2z8GDjRH+a8CKKYjG5KrUTXdExtVB9
Ora0+chIrlThz9Pdx0VGp1AucSsgU1w6Nk/TBJq2gw9Dfj+ALv7k6VJ18vO7DaBzyVuG8XQ3XizV
h/jgCmJqoaYkOks5C9tcNR49HphoA9y6c0qXPHq5uOsMkPtOIXm2ITZPediXLtKNzlHfhx4nb2rD
EZgO/li3YJ96Uhhb9DaMAxxG8VXk8QW4uB7Nk7N9qKKauDzlnlkluo1gPCUHZFz02dBhTIUIhu1m
zm796rh8dS6KYiNYkp/g7+z9zfvgb3SWgiiuqqJE5ahrhzOlH0fnqirgEYcQnlofzpWfiSkyvBlX
QDwVxSe0wWxZ87Iy9fCRPNk5eG8oV315KG7yxwWzpQ2T8omBNyHXDOm/Vn3U+5zAYLGsUufjTu3j
cE1/wKNSjhlE+4KKC/+NkgaNxfsNLw/JUD568YguHCUNHDiUhLBkxRRc/UtMNlGcX3PDYuN/IKz9
4LOYUVzpx24M5b1gnnovfuZbu5aedjGpSPuOL0utd0uX/u7DVClG8RQ543KJ16cVq+aE4vWk4xD8
eXx8kXJWKEeycCRXnhbK15/yZcp/cGOq7vowpOef26VZjB73x7L6k0uTY/OVW4ZmSnaFc6QCeCQs
hCkVxGQrj80o0rDfeu/yvKrWjj5P/f2H//BHAEy1ZWUiz2eLdOFT8xSwiS4Yn6dODuYRZaF8jcCP
STQF4NLvnXaKfnTPELVTdgJJa2h3TRNZiMsOUbtbOrwuvu9Fl/7ox5C3DmMTDUFcbXlcoZoWky1d
FMZVxIK+Gbb2jH3+p38raDVfTpxYoMofxhDfdMbVDx3wpkfumfKOYbjkq0g+cfTZPdrEFSc/m0Xu
RKzVOYOp1QlMFI4mT7EROi6AwN+eQjo1HDAcH4LBvHBbb225c1adzmfRoQtPzd6r2zQmS/HxMFz0
Dxca0Y7hukeUDNk9MLEKXzygX9HT9v5TWXrshhs8k+NOlcspNOJfvrj8nyNztAfhpuyEHO3oXIHB
A2UdMK8f0j71t/3a51F04ICWvvyDz4bG5KmmTCjQZAxlElo3UIEeDNX1qBzlnjc/vNT/xu5g818f
NPtFZ6mOu9CU9yP4yrOvv3/F+pHtx2ROsWqKV7LgluNWgTE+S5IOvV7o0hMDPbbxBZpUb4662Z0u
/9fMXXqz7fnHMj6f8PdlyEVeNOnPiSeu2nUiaCC8WKJYToH+RuhiAYu7Ick1DyPZcl4Cbhw05ZGA
Lh+br0p3Tpc9GJ2lYMIWiq4MPnBfOowtkwPj5/qc3ZqB7RrYQRxfvsYlWXCPdGjA1S9c3SbXGB1S
6zviCpQWS/DBADqKY3OJXEeq7NfoHDl0zQ++8nBQQ6Ecea5TuvzehBzC7s0Fe5ldqGQ4balpN3mB
gMKg19HsNiBdB/XG4Wyi/uWDjY/xqULfQAdNZJbyjBdddjMAr7VrD21APFOoXOJMl38TwZWeg4fx
UPKgMI7bkOSwFSgHOr5JP0s3pVn4WeqNgWyFdHPVwLdSbRGfqxrlkSn/8tm9hr+ipMEhAhe5BmQ2
NHtk1D8EM1WfHtPH4UaHaYNo+WHDSmfkViaVRvodeyrOJF5U6c9Jp68M2lABV76hHFl+BE96EbTA
wdtAn3+w6W1nOmEMZissP6N6Ata8pwoLTDp1hbL2+N0puGApTJuYI3uNkib+3rKL9lYcFE+65KsZ
Jdpp5MMGgUkFxIuuDM2dOaWaJSjpyQnnEhKwzP9tUoGKfMEn5Zkc4Wj/1PMG8tj+u8eNzutOtT+b
3ZACry0qV87yyJDctdXizOJJFf/8yqELs8mHPiHQxHJkGb4OZYn3oKQnI6O6LdglXfSbL018dWyh
/Im7x8qPWkf77xDcJZ3a0He8CcqnRmzjpx2jceFeXHTd9bWjhkA/mugCUJxp39aaAlG6w07RQ9KH
/ITmBJwkKOmSVg+qSI2f6vJRPzZ/2aud58q5+CiAJj4UnKcbipIfi5lF6kWUHcK7Jgc8FDCDmrfF
Es8ZHRKr28cwxadg3pXHmkKDcXFLZ8uzJrBLg9nXJVXwEKyFB/bZkBU8MoTVvrzm1iUDPaBhjYQ9
+m0YTf8gki9LWnqMeOwjfc+XNb5CKo0c+OELI4FKJBUJlQi3k6uMwzKEl1ZVNI2EPqnobMUJB3JT
Ct3XeT+YeckTyaZ7h4BnxXIa9pGftj4mcblEpQPDcM0lrdbq94YDwjOjgUrhGB7M2a1f0f0rIXuB
XuupJZrtjjvFvbpZV6sBglqhS4rwwcI9qkR0O5ZQcd01ki3Z5bCt5pFJSV3KIlsr3GSD3X3jp8Yh
G860h2dUH16EP6Z3JKV2B9xLXVJhz+npfnBKqc2E31PCDeG+joD3CRg3xvDkuFNq3e8W41NPBaIW
5L6z/qeX92gtP9pFJJQomdg2sBTbArdCgWwGyoMnuDfBAw9AcevBOPnuSSgdI1POKsGYNeBKNiuu
14fDj0VSDdWD3zxgxcFNmnG5igrHtIbeCusupOJqjZS0hgcJJba/joKtd1ml0bE/gQeCSPfTAAEr
onIKQ3/r2VLl43tlzAQyxFRHTtODqQWqFePxSrsU92qpOiCIJTuNpfY+j2MhSGl+VMn1lHOXgJ0t
cprIE/NdUwV3KdsEdyjJtXcoKXV3KKlIttffoaT1EJi2HV2HeVPAPfC+ZMGdUKqQeOs9bTAqVr8M
Y4g/cuYYvhidZeUz5oEys1iz2oHW+HBygZJpjzMQWuHhWYQELo/6VBZSGBTvTLnhuaLGKNBGHWK4
DTlOiVVgvYqWXp2rCCTme6H0fB4Ucz44caAx0Ge74P5bhy48hYpnE2DDXRqSLmuOKbBf2X3y/F7D
dApd1R7FlYtm79L0fQYEsPp9fUQAFcyc5PGMbi/UXUiFwResNYYzJJ8noU8Y4jjCsw6J5zrIwb7T
O4LWrPAeeG/3CrAm3Z8PlQcVv6Xa6Lq9/nY8XzKXLGQfwKWWK534wosmEoK18JPvMB013PIAxugP
XgzFrbg81RSU3IuEMvUkn50NrSbzAhSaNBfgC6CXIF/E9DJDtlZ3hLOkp+F3brBbh6bXnoVHm+DM
2Etx5lMf9igOijmvhfKqjK6pdb8sPdjY58pnQYV6+BBW880wjoyLkp6cMVmKEkeasmNSocbqoZ6N
lYZZHqn1P5IvbH5p+L1Sp5hqnhQQH8USn978nso75ajBIzzlnBh7Fyy91gHFwU+M4SxJPgO+NBBS
EUgp20H3t0dIBSIlQgUi5cFzP3FcCY2cPHoQm6tY6EpX3l18SGezZQ6IHaeujPSgiX8aypJdP/Nt
j21kOHtNK/bG/n7Uo0sEVsSUPmZzVVc3CMt3w5aVeVre28f9Kd1+7ZGe95tl9WkvcoOoG9A+DWFL
SyP50iZoDaDkJwdO8TNLNBwKjfhpQh6Rh5I7IU8dpgjfBi3kLSyt9nUsqW6lP020FJoP47Mk07Bk
4XJsi2DVkNS6lc+UaV+pFN3u/GZ1CVjeRPIUO4I4kvxpxZqt7JprFuPovF2qORFcOWs0T5Hz0oEL
S/IJ0wdJcG356uErU8bmKJgj2NK8CXnKDbyTlzu/vPeg18/xTBf+lfwwExCW3+LvmFa/lFRcDybn
KiLcM+WtL+y/+AZKGjxWgcU3KOAJF5rsxxlWzgrAwRXYQVvhAZ3F+5uDYBzafbF5usNu6aL3X9ir
jQPr3ulhbPmFYIb0BNwBG5MlP+nPUcsWHm4Khc+Yu79pmieu+HJ8FpH42okLYT4M2ZXhTOkB88tH
ZyvfgOcXQNAhnK88HJWjqbnynekzi5f2ap9yx4m2SJ5kPYz70kWCQKZM4p0p+XV6qWbthDzNM47b
ax+aD/x0Z2yOcr8bXdzmxiHIcgw6+BmdexhbesUXl7c+1eNzSMiYLGKrG1BcLFdJfr0FFTehUHfY
IUW4F1t7xn1xhW6GB1XybQRHmltZWek4OV/5oT8uN8zapY+fVqzynl6kXeDDVt98ulD57psffhHk
j8uaw7nE6eg80dCJhU2+scX6zZPKDFvg8BCRqy0bxlJUPV2mHj4lS+czv7xxvhdDfm80X0a69T1p
IgGWXPcWqECXEVxCEsxWfOGYKvi1p+ISdjeucU6X/R7CEONwfxclDz7z9hP+wzgKkRdNdn/5B5YH
6eAHhRtOXh0HuzZKwnD51yEbK68PR1GMX3c1kir4ahTcw4BxMLP6xOSpo53TxHGT84kxIN45hsJD
7fG5hkBgTE+I5krjQasK7hrY4V6BzuepfCLOlyaOm1aiGtX9C5KU45ciOl3sQCGrQXzdsdbx3dfb
M0t0r7rSZT8GscQ1g3oEqC+gITw2h9jlkiF7EMYhjm481jIGXfo/ARxKorKV2e40ya8xuardokHc
t7WLhXBjOlvJ82Bprnviyt+i8nSiWWXa5fC7b7iCQNn+VMp137rDrcxJBepVkbkagVsm8ZMLQ30z
iq98f+WfsZtvjTUnL0fH5SpK/HDp9w507SNnqrI9kCn+Gow7xUsqtAu59f8MMp8VgZ4TdNvjYjpH
Ap8FnrnpyOWAv3/Y/JeJhWruaL6iyZMmfeRA07Q7Mxt/D2JJv51cqOWevHi717j8b0mu4JbH9ELl
yNll6ufG5qjWjcxRHwzlKbV+DOlt9wzJ7650+UNQ+z+7sAxtrkz9Px0yNZ87MRuv+fANneLMarw2
hKG95sLUfgvzuTD1dyg0xQM3quyRZ4bogQdddi2ErxKPytXuiy1Ub4kvUMyNzBZHwbEYbjijovz/
BhqfUOCkYBZzGsrybwSG/Q+1Z0rlOdEPgAAAAABJRU5ErkJgglBLAQItABQABgAIAAAAIQBamK3C
DAEAABgCAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAGAAgA
AAAhAAjDGKTUAAAAkwEAAAsAAAAAAAAAAAAAAAAAPQEAAF9yZWxzLy5yZWxzUEsBAi0AFAAGAAgA
AAAhANc6jJBnAgAApgUAABIAAAAAAAAAAAAAAAAAOgIAAGRycy9waWN0dXJleG1sLnhtbFBLAQIt
ABQABgAIAAAAIQCqJg6+vAAAACEBAAAdAAAAAAAAAAAAAAAAANEEAABkcnMvX3JlbHMvcGljdHVy
ZXhtbC54bWwucmVsc1BLAQItABQABgAIAAAAIQCoPBp8FwEAAIcBAAAPAAAAAAAAAAAAAAAAAMgF
AABkcnMvZG93bnJldi54bWxQSwECLQAKAAAAAAAAACEAjQw2Z4YaAACGGgAAFAAAAAAAAAAAAAAA
AAAMBwAAZHJzL21lZGlhL2ltYWdlMS5wbmdQSwUGAAAAAAYABgCEAQAAxCEAAAAA
">
   <v:imagedata src="Insentif_files/Report%20(Autosaved)_19996_image001.png"
    o:title=""/>
   <x:ClientData ObjectType="Pict">
    <x:SizeWithCells/>
    <x:CF>Bitmap</x:CF>
    <x:AutoPict/>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:10px;margin-top:2px;width:58px;
  height:49px'><img width=58 height=49
  src="{{Config::get('constants.path.img')}}/logoreport.png" v:shapes="Picture_x0020_1"></span><![endif]><span
  style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td height=7 class=xl6519996 width=19 style='height:5.25pt;width:14pt'></td>
   </tr>
  </table>
  </span></td>
  <td class=xl6519996 width=37 style='width:28pt'></td>
  <td class=xl6519996 width=116 style='width:87pt'></td>
  <td class=xl6519996 width=232 style='width:174pt'></td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=100 style='width:75pt'></td>
  <td class=xl6519996 width=74 style='width:56pt'></td>
  <td class=xl6519996 width=90 style='width:68pt'></td>
  <td class=xl6519996 width=113 style='width:85pt'></td>
 </tr>
 <tr class=xl6619996 height=25 style='mso-height-source:userset;height:18.75pt'>
  <td height=25 class=xl6619996 width=19 style='height:18.75pt;width:14pt'></td>
  <td colspan=12 class=xl7119996 width=1227 style='width:923pt'><span
  style='mso-spacerun:yes'>         </span>PT. TROCON INDAH PERKASA</td>
 </tr>
 <tr class=xl6719996 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6719996 width=19 style='height:17.25pt;width:14pt'></td>
  <td colspan=12 class=xl7419996 width=1227 style='width:923pt'><span
  style='mso-spacerun:yes'>           </span>LAPORAN INSENTIF</td>
 </tr>
 <tr class=xl6519996 height=4 style='mso-height-source:userset;height:3.0pt'>
  <td height=4 class=xl6519996 width=19 style='height:3.0pt;width:14pt'></td>
  <td class=xl6519996 width=37 style='width:28pt'></td>
  <td colspan=4 class=xl7019996 width=534 style='width:401pt'></td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=100 style='width:75pt'></td>
  <td class=xl6519996 width=74 style='width:56pt'></td>
  <td class=xl6519996 width=90 style='width:68pt'></td>
  <td class=xl6519996 width=113 style='width:85pt'></td>
 </tr>
 <tr class=xl6519996 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6519996 width=19 style='height:17.25pt;width:14pt'></td>
  <td colspan=2 class=xl7319996 width=153 style='width:115pt'>PERIODE</td>
  <td colspan=3 class=xl7319996 width=418 style='width:314pt'>: {{ $datafilter->period }}</td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=100 style='width:75pt'></td>
  <td class=xl6519996 width=74 style='width:56pt'></td>
  <td class=xl6519996 width=90 style='width:68pt'></td>
  <td class=xl6519996 width=113 style='width:85pt'></td>
 </tr>
 <tr class=xl6519996 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6519996 width=19 style='height:17.25pt;width:14pt'></td>
  <td colspan=2 class=xl7219996 width=153 style='width:115pt'>DEPARTEMEN</td>
  <td colspan=3 class=xl7219996 width=418 style='width:314pt'>: {{ $datafilter->departmentname }}</td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=93 style='width:70pt'></td>
  <td class=xl6519996 width=100 style='width:75pt'></td>
  <td class=xl6519996 width=74 style='width:56pt'></td>
  <td class=xl6519996 width=90 style='width:68pt'></td>
  <td class=xl6519996 width=113 style='width:85pt'></td>
 </tr>
 <tr height=32 style='mso-height-source:userset;height:24.0pt'>
  <td height=32 class=xl1519996 style='height:24.0pt'></td>
  <td class=xl7719996 style='border-top:none'>NO</td>
  <td class=xl7719996 style='border-top:none;border-left:none'>Departemen</td>
  <td class=xl7719996 style='border-top:none;border-left:none'>Karyawan</td>
  <td class=xl7719996 style='border-top:none;border-left:none'>Minggu 1</td>
  <td class=xl7719996 style='border-top:none;border-left:none'>Minggu 2</td>
  <td class=xl7719996 style='border-left:none'>Minggu 3</td>
  <td class=xl7719996 style='border-left:none'>Minggu 4</td>
  <td class=xl7719996 style='border-left:none'>Minggu 5</td>
  <td class=xl7719996 style='border-left:none'>Total</td>
  <td class=xl7719996 style='border-left:none'>Rate</td>
  <td class=xl7719996 style='border-left:none'>Pot Absen</td>
  <td class=xl7719996 style='border-left:none'>Total</td>
 </tr>
  @php
   $t_total = 0; 

   $n = 1;
 @endphp
 @foreach($itemdata as $itemdatas)
 <tr height=25 style='mso-height-source:userset;height:18.75pt'>
  <td height=25 class=xl1519996 style='height:18.75pt'></td>
  <td class=xl6919996 style='border-top:none'>@php echo $n++; @endphp</td>
  <td class=xl6319996 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->departmentname }}</td>
  <td class=xl6319996 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->employee }}</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->day1,0) }}&nbsp;</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->day2,0) }}&nbsp;</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->day3,0) }}&nbsp;</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->day4,0) }}&nbsp;</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->day5,0) }}&nbsp;</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->tday,0) }}&nbsp;</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->rate,0) }}&nbsp;</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->tpotabsence,0) }}&nbsp;</td>
  <td class=xl6419996 style='border-top:none;border-left:none'>{{ number_format($itemdatas->amount,0) }}&nbsp;</td>
 </tr>
  @php 
 $t_total += $itemdatas->amount; 
 @endphp
 @endforeach
 <tr height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl1519996 style='height:17.25pt'></td>
  <td colspan=11 class=xl7519996>TOTAL</td>
  <td class=xl6819996 align=right style='border-top:none;border-left:none'>@php echo number_format($t_total,0); @endphp</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1519996 style='height:15.0pt'></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1519996 style='height:15.0pt'></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1519996 style='height:15.0pt'></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1519996 style='height:15.0pt'></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td colspan=3 class=xl7619996>JAKARTA, @php echo date("d M Y"); @endphp</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1519996 style='height:15.0pt'></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td colspan=2 class=xl7819996>DISETUJUI</td>
  <td colspan=2 class=xl7819996>MENGETAHUI</td>
  <td colspan=2 class=xl7819996>DIBUAT OLEH</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1519996 style='height:15.0pt'></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1519996 style='height:15.0pt'></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1519996 style='height:15.0pt'></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1519996 style='height:15.0pt'></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1519996 style='height:15.0pt'></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td class=xl1519996></td>
  <td colspan=2 class=xl7819996>(_________________)</td>
  <td colspan=2 class=xl7819996>(_________________)</td>
  <td colspan=2 class=xl7819996>(_________________)</td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=19 style='width:14pt'></td>
  <td width=37 style='width:28pt'></td>
  <td width=116 style='width:87pt'></td>
  <td width=232 style='width:174pt'></td>
  <td width=93 style='width:70pt'></td>
  <td width=93 style='width:70pt'></td>
  <td width=93 style='width:70pt'></td>
  <td width=93 style='width:70pt'></td>
  <td width=93 style='width:70pt'></td>
  <td width=100 style='width:75pt'></td>
  <td width=74 style='width:56pt'></td>
  <td width=90 style='width:68pt'></td>
  <td width=113 style='width:85pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>
