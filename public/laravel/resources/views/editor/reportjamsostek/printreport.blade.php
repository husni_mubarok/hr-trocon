@extends('layouts.editor.templatereport')
@section('content')
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
 
</style> 

<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 14">
<link rel=File-List href="Jamsostek_files/filelist.xml">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<style id="Report (Autosaved)_24351_Styles">
<!--table
  {mso-displayed-decimal-separator:"\,";
  mso-displayed-thousand-separator:"\.";}
.xl1524351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6324351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6424351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:"\#\,\#\#0";
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6524351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:"Short Date";
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6624351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6724351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6824351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6924351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7024351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7124351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7224351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:16.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7324351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7424351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7524351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7624351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7724351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7824351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7924351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl8024351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl8124351
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border:.5pt solid windowtext;
  background:#E7E6E6;
  mso-pattern:black none;
  white-space:nowrap;}
-->
</style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="Report (Autosaved)_24351" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=713 style='border-collapse:
 collapse;table-layout:fixed;width:537pt'>
 <col width=17 style='mso-width-source:userset;mso-width-alt:621;width:13pt'>
 <col width=34 style='mso-width-source:userset;mso-width-alt:1243;width:26pt'>
 <col width=158 style='mso-width-source:userset;mso-width-alt:5778;width:119pt'>
 <col width=242 style='mso-width-source:userset;mso-width-alt:8850;width:182pt'>
 <col width=136 style='mso-width-source:userset;mso-width-alt:4973;width:102pt'>
 <col width=126 style='mso-width-source:userset;mso-width-alt:4608;width:95pt'>
 <tr class=xl6624351 height=7 style='mso-height-source:userset;height:5.25pt'>
  <td height=7 width=17 style='height:5.25pt;width:13pt' align=left valign=top><!--[if gte vml 1]><v:shapetype
   id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
   path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="Picture_x0020_3" o:spid="_x0000_s3073" type="#_x0000_t75"
   style='position:absolute;margin-left:6.75pt;margin-top:4.5pt;width:43.5pt;
   height:33pt;z-index:1;visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQBamK3CDAEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhvROFGcle3tSdZNgokh7Rjb3+8vyWK5tSObIJBxWPPbsuIM
UDltsK/5x/qleOCMokQtR4dQ8x0QXzbXV4v1zgOxRCPVfIjRPwpBagArqXQeMHU6F6yM6Rh64aX6
lD2Iu6q6F8phBIxFzBm8WbTQyc0Y2fM2lWcTjz1nT/NcXlVzYzOf6+JPIsBIJ4j0fjRKxnQ3MaE+
8SoOTmUi9zM0GE83SfzMhtz57fRzwYF7S48ZjAa2kiG+SpvMhQ4kvFFxEyBNlf/nZFFLhes6o6Bs
A61m8ih2boF2XxhgujS9Tdg7TMd0sf/X5hsAAP//AwBQSwMEFAAGAAgAAAAhAAjDGKTUAAAAkwEA
AAsAAABfcmVscy8ucmVsc6SQwWrDMAyG74O+g9F9cdrDGKNOb4NeSwu7GltJzGLLSG7avv1M2WAZ
ve2oX+j7xL/dXeOkZmQJlAysmxYUJkc+pMHA6fj+/ApKik3eTpTQwA0Fdt3qaXvAyZZ6JGPIoiol
iYGxlPymtbgRo5WGMqa66YmjLXXkQWfrPu2AetO2L5p/M6BbMNXeG+C934A63nI1/2HH4JiE+tI4
ipr6PrhHVO3pkg44V4rlAYsBz3IPGeemPgf6sXf9T28OrpwZP6phof7Oq/nHrhdVdl8AAAD//wMA
UEsDBBQABgAIAAAAIQBFQM+2bwIAAKYFAAASAAAAZHJzL3BpY3R1cmV4bWwueG1srFTLbtswELwX
6D8QvNuibCmyhUiBaydFgSA1ivYDGIqyiEqkQNKPoOi/d0lKNtwcWjS9rbjcneHOrG7vTl2LDlwb
oWSB4ynBiEumKiF3Bf729WGywMhYKivaKskL/MINvivfv7s9VTqnkjVKI2ghTQ4HBW6s7fMoMqzh
HTVT1XMJ2Vrpjlr41Luo0vQIzbs2mhFyE5lec1qZhnO7CRlc+t72qNa8bVcBglfCrkyBgYM7He7U
WnXhNlNtSW4jR8qFvgMEn+u6XKTZLD2n3InPanUcK1w4nrl8msXp0AxSvsJ3vsBZdYYoZ+fe5zNX
EqezhFwoXeEOJb/jxjfL+WIe2l0Bj3C9YAFDHraCbfUA+HTYaiSqAicYSdqBSpC1e83RHEeXO6GC
5tDlUbHvZtCN/oNqHRUSsNS6oXLHV6bnzIJ7HFrQACgFOP95Rfe5Ff2DaEEkmrv4zTSC/f7KfKqu
BeMbxfYdlzY4UPOWWnC/aURvMNI57545DFN/qmKMGJjfwkR7LaQF29Gcn+yjsUOE9loU+MdssSJk
OfswWadkPUlIdj9ZLZNskpH7LCHJIl7H65+uOk7yveEwftpuejE+PU5eadAJppVRtZ0y1UWB97g7
wDsmUdDgQNsCEz94Tw0EuFCE0E3YcTVWc8uaEfEV3p831eO5VjWI9wUEd2KfGw/CX8R1q2h651Ga
n2rd/Q9kGAM6FdhvNEYvBfab6h7v34wYJFPYuxT+YgzSSZyRud8BIOpIuIu9NvYjV28mhFwjsAlM
wvuCHsAWYSYjxDCUMAa/CbB7w0K2Ahy4oZaOO3P1wxsqww+2/AUAAP//AwBQSwMEFAAGAAgAAAAh
AKomDr68AAAAIQEAAB0AAABkcnMvX3JlbHMvcGljdHVyZXhtbC54bWwucmVsc4SPQWrDMBBF94Xc
Qcw+lp1FKMWyN6HgbUgOMEhjWcQaCUkt9e0jyCaBQJfzP/89ph///Cp+KWUXWEHXtCCIdTCOrYLr
5Xv/CSIXZINrYFKwUYZx2H30Z1qx1FFeXMyiUjgrWEqJX1JmvZDH3IRIXJs5JI+lnsnKiPqGluSh
bY8yPTNgeGGKyShIk+lAXLZYzf+zwzw7TaegfzxxeaOQzld3BWKyVBR4Mg4fYddEtiCHXr48NtwB
AAD//wMAUEsDBBQABgAIAAAAIQDcynjMFAEAAIcBAAAPAAAAZHJzL2Rvd25yZXYueG1sXJDLTsMw
EEX3SPyDNUjsqJMoJlWoUxUkoKuiNkiInZU4D/Ajsk2b8vU4TVEQq9Gd8blzx4tlLwXac2NbrSiE
swAQV4UuW1VTeM0fb+aArGOqZEIrTuHILSyzy4sFS0t9UFu+37kaeRNlU0ahca5LMbZFwyWzM91x
5WeVNpI5L02NS8MO3lwKHAXBLZasVX5Dwzr+0PDic/clKbwI0mySp34tkuTtXr+TPD8+f1B6fdWv
7gA53rvp8ZlelxRiGE7xZ0Dm8/VipYpGG1RtuW2/ffixXxktkdEHCv7YQotT9XpTVZY7CiQJyTj5
7cxJEhHAg6nTIxqdUV//oGESxv/YkERxEAwwniKdxPR/2Q8AAAD//wMAUEsDBAoAAAAAAAAAIQCN
DDZnhhoAAIYaAAAUAAAAZHJzL21lZGlhL2ltYWdlMS5wbmeJUE5HDQoaCgAAAA1JSERSAAAATgAA
AEMIBgAAAUUzA1UAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAh1QAAIdUB
BJy0nQAAGhtJREFUaEPtmwlYU8e+wA+yhH1VkEVQFEQF1GpdWi3V9tq6tr1X2/p61VdrXVEUEIUk
nOxh311wQ+0qdasLECBkP9khImrFtrbaXq+01d621mqFvJmTCRAIISh9ve999/d9/y8zc+aczPnP
9p//zMEGjCeu+g0F7eNv+/XxWEqtEUWxhL2av6MghgUwxLdREMPWnTZiqXXGnee+9kMpXbjzmx/A
X19c8RmWLDC+fEAzGdv4aedTO3FhX3xEBsCTKo1GR5gZ/n0EQ/gemW5mzOZWCgr2YmKhyBcFbTN3
r3YOCloykivjjMtR1KLoILC2XOeMgpgjVfM7Cpp444ghGgUxbGt1b7X0ZBxbQpuYo+RCNaEkzGg0
DkHBboAMRw23PLAtVcaZucLnYNIrhxoXkNfMOO+of4glgzoGmcgnQqUDNle1DiMzDIgZRer/QkH7
ONbyo78vvUEQwVK8iZL6xz9TdMyTJn7ky1BVTClQpc0oUaUFcTT7PDLED0I4sqMom23cMyT3UNCS
LdVGb5rkKxSzwJMuvYaCliSduzoOBS2Yl1/jT2oXajZN3H/lmnFgXrBsJhBYNUnVxsVl6tfJh5Lx
ml4PHVcgm4CCXbjzLpC9sBNYIijb6zsm7JKMiNunCusuKJeJxJoYFOoikCUWoyB4f9BuQdt1SRF+
77L+9F2yMcGBw/wnMAzT+gP0vV3wNxJXBLrsFN6ev083Fwdt2zmtnk1mQID27uDP0V6F4QC65BaZ
2BdTizRP+7HUehS1Cnigszdb0zKSryD7yf8us3dp53kxtd94MlV6b7qI9gxo0D70hjQfpkbpxtJ/
nVDWNAll7ZvYfNXH605dGYminYxi1vdqtAsqLq54bpcuCUXtY+EeIpSsaSu8ftQQi4KW+NJE1hUP
mwNowJR00S8oxYJhbPloFOwikCW9goKdJPCFy4Lp4mpze0uoELmiS5140cR3UbCLcbnS6SjYxYbT
xvmlRFxX463v9bqOmcSXKNjFO/tb/FGQZARLfpIczTefNy4o168g++s2gTG1smU4ykLiQxd+hIJd
4JUtLihoAnUhBzQ2k7rbfM6IrT1hWbpkQRoKdbGs28M86NKu/gjo2enj+Oc65+8phapjKNjF06Xq
ABTsLNUQoCPv1JpvyNc16w3+dmsqrhniXhWHxRWoXoG/Q5IE7dhWMI6Zb9gAjAP4MLICkADdDafW
fgMvO3MudFkeZtypDQ9RcEBE5SgLULCLubv15Gw0lq9IgpURwCQ+H0YX6V87+nmgJ668Aa9NLtLn
+FIbrlLSGt4Zsr2etKwmFjZZNz6Gs+UqFLSLEXziPApax4sub3tht+ZFFLXKxHzl234sxc8oah9z
9zS9FpdHCHxoDQI/mpC78iNt7344mFS1tlI4565Pct9RNRlDsuLIpckV1429+vGgM7lUHxLGV513
Zaj/Fc6RVy7/oHkmujQgZpfpnwvkEAK3TOK3uAI1b1XF9ccv/KQC9ZpwHlGDov0SgQvPYFvOG+ft
UU1ESTYRiYxOfkzF1fUnDIEoyT4W7rswO5glPYyi/RKWKTKQHRmOW2AA/Et543x0qV88aZJfxpeJ
PFHUNlOAge1ClfyKov2y/FBjFDnawIKZhywgz+3SJ6MsNkkA/Tw6W1GKoraJyVEVjctV0FDUFg7Y
2pO/R6ZXa3BRiyeWgsbRbsPg2GzlAZTXJk7MJts2kRkXqqxjHFsRgaJW2Xbshhu2FWgKtDEs8azR
e7vgm2WVlY6O2xvuWQz4QALoIgW6rU8CaA2Xpu9vDkJRGzAbH2Dd1k49KdeBa3CGNhvD20CVJlUZ
/bbXXYfX/TJEt7FtYGKBAqsbvIDL1prvyJv7YHK+oiKKZ8Uy6Ikrp+lBr4kaAUbmyO7VRgooZBhb
9gnKQhKJNxT4plWX+6YC2Xam3DfxRPmIbacOosu9GJJSu2PJgaaFKGoDuv4BWHb20tyUIvWyXgUD
1Rabp6RP4gsngmWfaUkBpltTlcLrpjwmLdYYHbcKTMv0HvjQROcW71P3Xnr0BBgDj8bkWJp3UXxF
tmmYQAILAGyCeft0i57Nlc3HNp4hq5YsBCxMz5dALwLvcQBVXVlpdESPJnGgaa5bXyT3YGap/s2R
WarjKIqNzKg9hW0604ZtOdcG2pDpN/H8d/P3qqcm8ITLsdWftGHrToN0cG2roA1oqw20yd6SAgVc
21rThm061zarQBaJ/gLzpQpttkkL3Ony1lWnTHbGxlOXVgThon9EcGV62BY9qKIfRnAkhpgs6R68
5tozvgyZOoSrvr6j7gufuGJiTRBDUhec3fQjuNUhKFt3M5ApOjyUKVHCZ7mwde0JuMjJMV3WDuOQ
Obs1S0J40i7/hD14UmX3URB7uli9edbB1mGwcEFsWYHIaHSKySE+eePI5egwtlweySea8Job/ov3
a1cF4ZLaUC7RAu8DeWtCWLLjo3O1ZGcIyGx4a0aJ9tlAupg0ZzadvBzgkiGzX2vdicpRfTqMKb1N
3ADj2iATwpJeBM1Hh6JPxiqwmgvnSz92Z2l/8GEovhvO0340nCFZF4iLXvRIr4+fvksZPwuILwiD
6egFYLetCeIqC/3Yap1rekOHK0PXEsGTs7n1X9gx2P6HP5n4owIPMHN0WsHj+HWT2cJLEWDcshjL
/lBwweeBYWxpmi9Hp3HLlP/qw1TeDuBoqycU6PeFsSTboWsCSnSWPG0Ej8gbylWf9mISN13psl99
WBrl+FxVIo7bMdAOhJhs1XKfTOlDV1xtiM1XJRarWr3RJbvJJ274j+LJqW4szddDGYrLz5Tqp6JL
j8fpK995UTIU7aN4shKUZJMRdGGZHNyDon3S0mJ0CeMTV8F8+tsqK86M/jEaHVwy5L+s/lAfglJs
AtqXi8OWaqMPTfI5SuqX+CJF7FCGrLcjpD+mlujfmFWmL0LRfvGn1ovNE//8Q9o4lNwvfri89aUD
F8aiqH24pYvbF5Xr3FHUJsvKgIkOtEZ6LoBt55Im7Jw3+2P92cuxAUxLW9AmqypbhntQG/6Bov3i
nSJ4QHqJzHYcMI+mFapWosv94rZT2Nv93BfxwGSOL1RuQNF+8d9Zd6Nz9YVsN5edIqtGpTUCcJnd
a2PMPUN8jyA67Jro4/Daj6EJ5LCtpqOnkTmKKytG2WwSlSV/d8Px5k7bziaOdGUHCtokilaTgr3z
iRGuuoLowlPkgqdb4YCBCT1nDqbcfbP0vcbxY7Ml21G0b3CRyMkdV7ahqE0cN599iK0/ZfTcWnUH
mt2OacIehaszjuDIGlH2PsGBbehPl9j2e0Hm7tfHhyGr1RYxXOlxcs0KXaVg/TArW5owhifZSlat
uXrhb7KgI6G4x5aHFSgZ0mYU7Bu/jPr5TxcSZShqlZffU3mTvly4mIH+EVBISnLNIzhwU3Y2PDQt
gsiCkdcDqPXketYW7pyLX6Bg34zNklPHcOVdm7hWCGaIDUgrpgLCnrr5vPGVQ5oRC/c3TyHTzAWH
A3PiOePsXInNXRMnuu4mCtogqYb61yMXV6BYLxYeMMR2timygEBAIcJZXcOBX7rwtsk7Dqp9E1gy
gnbpkfjpD+iyVTyY+jso2DdBmQ25S9+/1Gfh/HHpl52F696uKro8wtnyK14Wq/0kIBtPlCd/oOu9
vYawq3BYcg31lSMGq4VbdKBpHrmC7144oLkZxcTxl4urKHGg4ffaBugmE/IkI9CjekNvsr4d2p1Z
papMD5rY6rYwZaf4rkXBgNactteRjnz3lLpWsirR/NrligBCVj1IB/NvfLY0k3xYD3yzWvq3ZsZm
KV4axZWUo2gnL+xWbyCrEBXK3N7GZyt2bD7dMh42emsOxM78qOdiSQLj6tNXLGw+uHfphUsMKNo3
a459NsqPLrqAoiRrdUZnLLmuw/THQFAvpaTWkbvZzomg0cPxDv45vN6zcN0LCF5gNEtyknwwYtsx
wi2CZ6dl4oZrLA6UTMhSlJBDAvxzWD1oiIjLks2fyqpdgq07SToQ+9ScWczaA9WPi653dqCEfU2T
orNk76CobZyois7tJ/hWDonV32GbziJHDhDw65FSSw6sLms+acPWnmzDNpwB12raQMFMDpu+HDnJ
4HpSdZt3So2U/ANACEeyfv2Jz+3zqA9nSYhXjzQnoOgfTgBTXo2C/bPwYONEf5rwIopiMbkqtRNd
0TG1UH06trT5yEiuVOHP093HRUanUC5xKyBTXDo2T9MEmraDD0N+P4Au/uTpUnXy87sNoHPJW4bx
dDdeLNWH+OAKYmqhpiQ6SzkL21w1Hj0emGgD3LpzSpc8erm46wyQ+04hebYhNk952Jcu0o3OUd+H
HidvasMRmA7+WLdgn3pSGFv0NowDHEbxVeTxBbi4Hs2Ts32oopq4POWeWSW6jWA8JQdkXPTZ0GFM
hQiG7WbObv3quHx1LopiI1iSn+Dv7P3N++BvdJaCKK6qokTlqGuHM6UfR+eqKuARhxCeWh/OlZ+J
KTK8GVdAPBXFJ7TBbFnzsjL18JE82Tl4byhXfXkobvLHBbOlDZPyiYE3IdcM6b9WfdT7nMBgsaxS
5+NO7eNwTX/Ao1KOGUT7gooL/42SBo3F+w0vD8lQPnrxiC4cJQ0cOJSEsGTFFFz9S0w2UZxfc8Ni
438grP3gs5hRXOnHbgzlvWCeei9+5lu7lp52MalI+44vS613S5f+7sNUKUbxFDnjconXpxWr5oTi
9aTjEPx5fHyRclYoR7JwJFeeFsrXn/Jlyn9wY6ru+jCk55/bpVmMHvfHsvqTS5Nj85VbhmZKdoVz
pAJ4JCyEKRXEZCuPzSjSsN967/K8qtaOPk/9/Yf/8EcATLVlZSLPZ4t04VPzFLCJLhifp04O5hFl
oXyNwI9JNAXg0u+ddop+dM8QtVN2AklraHdNE1mIyw5Ru1s6vC6+70WX/ujHkLcOYxMNQVxteVyh
mhaTLV0UxlXEgr4ZtvaMff6nfytoNV9OnFigyh/GEN90xtUPHfCmR+6Z8o5huOSrSD5x9Nk92sQV
Jz+bRe5ErNU5g6nVCUwUjiZPsRE6LoDA355COjUcMBwfgsG8cFtvbblzVp3OZ9GhC0/N3qvbNCZL
8fEwXPQPFxrRjuG6R5QM2T0wsQpfPKBf0dP2/lNZeuyGGzyT406Vyyk04l++uPyfI3O0B+Gm7IQc
7ehcgcEDZR0wrx/SPvW3/drnUXTggJa+/IPPhsbkqaZMKNBkDGUSWjdQgR4M1fWoHOWeNz+81P/G
7mDzXx80+0VnqY670JT3I/jKs6+/f8X6ke3HZE6xaopXsuCW41aBMT5Lkg69XujSEwM9tvEFmlRv
jrrZnS7/18xderPt+ccyPp/w92XIRV406c+JJ67adSJoILxYolhOgf5G6GIBi7shyTUPI9lyXgJu
HDTlkYAuH5uvSndOlz0YnaVgwhaKrgw+cF86jC2TA+Pn+pzdmoHtGthBHF++xiVZcI90aMDVL1zd
JtcYHVLrO+IKlBZL8MEAOopjc4lcR6rs1+gcOXTND77ycFBDoRx5rlO6/N6EHMLuzQV7mV2oZDht
qWk3eYGAwqDX0ew2IF0H9cbhbKL+5YONj/GpQt9AB01klvKMF112MwCvtWsPbUA8U6hc4kyXfxPB
lZ6Dh/FQ8qAwjtuQ5LAVKAc6vkk/SzelWfhZ6o2BbIV0c9XAt1JtEZ+rGuWRKf/y2b2Gv6KkwSEC
F7kGZDY0e2TUPwQzVZ8e08fhRodpg2j5YcNKZ+RWJpVG+h17Ks4kXlTpz0mnrwzaUAFXvqEcWX4E
T3oRtMDB20Cff7DpbWc6YQxmKyw/o3oC1rynCgtMOnWFsvb43Sm4YClMm5gje42SJv7esov2VhwU
T7rkqxkl2mnkwwaBSQXEi64MzZ05pZolKOnJCecSErDM/21SgYp8wSflmRzhaP/U8wby2P67x43O
6061P5vdkAKvLSpXzvLIkNy11eLM4kkV//zKoQuzyYc+IdDEcmQZvg5lifegpCcjo7ot2CVd9Jsv
TXx1bKH8ibvHyo9aR/vvENwlndrQd7wJyqdGbOOnHaNx4V5cdN31taOGQD+a6AJQnGnf1poCUbrD
TtFD0of8hOYEnCQo6ZJWD6pIjZ/q8lE/Nn/Zq53nyrn4KIAmPhScpxuKkh+LmUXqRZQdwrsmBzwU
MIOat8USzxkdEqvbxzDFp2DelceaQoNxcUtny7MmsEuD2dclVfAQrIUH9tmQFTwyhNW+vObWJQM9
oGGNhD36bRhN/yCSL0taeox47CN9z5c1vkIqjRz44QsjgUokFQmVCLeTq4zDMoSXVlU0jYQ+qehs
xQkHclMK3dd5P5h5yRPJpnuHgGfFchr2kZ+2PiZxuUSlA8NwzSWt1ur3hgPCM6OBSuEYHszZrV/R
/Sshe4Fe66klmu2OO8W9ullXqwGCWqFLivDBwj2qRHQ7llBx3TWSLdnlsK3mkUlJXcoiWyvcZIPd
feOnxiEbzrSHZ1QfXoQ/pnckpXYH3EtdUmHP6el+cEqpzYTfU8IN4b6OgPcJGDfG8OS4U2rd7xbj
U08FohbkvrP+p5f3aC0/2kUklCiZ2DawFNsCt0KBbAbKgye4N8EDD0Bx68E4+e5JKB0jU84qwZg1
4Eo2K67Xh8OPRVIN1YPfPGDFwU2acbmKCse0ht4K6y6k4mqNlLSGBwkltr+Ogq13WaXRsT+BB4JI
99MAASuicgpDf+vZUuXje2XMBDLEVEdO04OpBaoV4/FKuxT3aqk6IIglO42l9j6PYyFIaX5UyfWU
c5eAnS1ymsgT811TBXcp2wR3KMm1dygpdXcoqUi219+hpPUQmLYdXYd5U8A98L5kwZ1QqpB46z1t
MCpWvwxjiD9y5hi+GJ1l5TPmgTKzWLPagdb4cHKBkmmPMxBa4eFZhAQuj/pUFlIYFO9MueG5osYo
0EYdYrgNOU6JVWC9ipZenasIJOZ7ofR8HhRzPjhxoDHQZ7vg/luHLjyFimcTYMNdGpIua44psF/Z
ffL8XsN0Cl3VHsWVi2bv0vR9BgSw+n19RAAVzJzk8YxuL9RdSIXBF6w1hjMknyehTxjiOMKzDonn
OsjBvtM7gtas8B54b/cKsCbdnw+VBxW/pdrour3+djxfMpcsZB/ApZYrnfjCiyYSgrXwk+8wHTXc
8gDG6A9eDMWtuDzVFJTci4Qy9SSfnQ2tJvMCFJo0F+ALoJcgX8T0MkO2VneEs6Sn4XdusFuHptee
hUeb4MzYS3HmUx/2KA6KOa+F8qqMrql1vyw92NjnymdBhXr4EFbzzTCOjIuSnpwxWYoSR5qyY1Kh
xuqhno2VhlkeqfU/ki9sfmn4vVKnmGqeFBAfxRKf3vyeyjvlqMEjPOWcGHsXLL3WAcXBT4zhLEk+
A740EFIRSCnbQfe3R0gFIiVCBSLlwXM/cVwJjZw8ehCbq1joSlfeXXxIZ7NlDogdp66M9KCJfxrK
kl0/822PbWQ4e00r9sb+ftSjSwRWxJQ+ZnNVVzcIy3fDlpV5Wt7bx/0p3X7tkZ73m2X1aS9yg6gb
0D4NYUtLI/nSJmgNoOQnB07xM0s0HAqN+GlCHpGHkjshTx2mCN8GLeQtLK32dSypbqU/TbQUmg/j
syTTsGThcmyLYNWQ1LqVz5RpX6kU3e78ZnUJWN5E8hQ7gjiS/GnFmq3smmsW4+i8Xao5EVw5azRP
kfPSgQtL8gnTB0lwbfnq4StTxuYomCPY0rwJecoNvJOXO7+896DXz/FMF/6V/DATEJbf4u+YVr+U
VFwPJucqItwz5a0v7L/4BkoaPFaBxTco4AkXmuzHGVbOCsDBFdhBW+EBncX7m4NgHNp9sXm6w27p
ovdf2KuNA+ve6WFs+YVghvQE3AEbkyU/6c9RyxYebgqFz5i7v2maJ674cnwWkfjaiQthPgzZleFM
6QHzy0dnK9+A5xdA0CGcrzwclaOpufKd6TOLl/Zqn3LHibZInmQ9jPvSRYJApkzinSn5dXqpZu2E
PM0zjttrH5oP/HRnbI5yvxtd3ObGIchyDDr4GZ17GFt6xReXtz7V43NIyJgsYqsbUFwsV0l+vQUV
N6FQd9ghRbgXW3vGfXGFboYHVfJtBEeaW1lZ6Tg5X/mhPy43zNqlj59WrPKeXqRd4MNW33y6UPnu
mx9+EeSPy5rDucTp6DzR0ImFTb6xxfrNk8oMW+DwEJGrLRvGUlQ9XaYePiVL5zO/vHG+F0N+bzRf
Rrr1PWkiAZZc9xaoQJcRXEISzFZ84Zgq+LWn4hJ2N65xTpf9HsIQ43B/FyUPPvP2E/7DOAqRF012
f/kHlgfp4AeFG05eHQe7NkrCcPnXIRsrrw9HUYxfdzWSKvhqFNzDgHEws/rE5KmjndPEcZPziTEg
3jmGwkPt8bmGQGBMT4jmSuNBqwruGtjhXoHO56l8Is6XJo6bVqIa1f0LkpTjlyI6XexAIatBfN2x
1vHd19szS3SvutJlPwaxxDWDegSoL6AhPDaH2OWSIXsQxiGObjzWMgZd+j8BHEqispXZ7jTJrzG5
qt2iQdy3tYuFcGM6W8nzYGmue+LK36LydKJZZdrl8LtvuIJA2f5UynXfusOtzEkF6lWRuRqBWybx
kwtDfTOKr3x/5Z+xm2+NNScvR8flKkr8cOn3DnTtI2eqsj2QKf4ajDvFSyq0C7n1/wwynxWBnhN0
2+NiOkcCnwWeuenI5YC/f9j8l4mFau5ovqLJkyZ95EDTtDszG38PYkm/nVyo5Z68eLvXuPxvSa7g
lsf0QuXI2WXq58bmqNaNzFEfDOUptX4M6W33DMnvrnT5Q1D7P7uwDG2uTP0/HTI1nzsxG6/58A2d
4sxqvDaEob3mwtR+C/O5MPV3KDTFAzeq7JFnhuiBB112LYSvEo/K1e6LLVRviS9QzI3MFkfBsRhu
OKOi/P8GGp9Q4KRgFnMayvJvBIb9D7VnSuU50Q+AAAAAAElFTkSuQmCCUEsBAi0AFAAGAAgAAAAh
AFqYrcIMAQAAGAIAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAU
AAYACAAAACEACMMYpNQAAACTAQAACwAAAAAAAAAAAAAAAAA9AQAAX3JlbHMvLnJlbHNQSwECLQAU
AAYACAAAACEARUDPtm8CAACmBQAAEgAAAAAAAAAAAAAAAAA6AgAAZHJzL3BpY3R1cmV4bWwueG1s
UEsBAi0AFAAGAAgAAAAhAKomDr68AAAAIQEAAB0AAAAAAAAAAAAAAAAA2QQAAGRycy9fcmVscy9w
aWN0dXJleG1sLnhtbC5yZWxzUEsBAi0AFAAGAAgAAAAhANzKeMwUAQAAhwEAAA8AAAAAAAAAAAAA
AAAA0AUAAGRycy9kb3ducmV2LnhtbFBLAQItAAoAAAAAAAAAIQCNDDZnhhoAAIYaAAAUAAAAAAAA
AAAAAAAAABEHAABkcnMvbWVkaWEvaW1hZ2UxLnBuZ1BLBQYAAAAABgAGAIQBAADJIQAAAAA=
">
   <v:imagedata src="Jamsostek_files/Report%20(Autosaved)_24351_image001.png"
    o:title=""/>
   <x:ClientData ObjectType="Pict">
    <x:SizeWithCells/>
    <x:CF>Bitmap</x:CF>
    <x:AutoPict/>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:9px;margin-top:6px;width:58px;
  height:44px'><img width=58 height=44
  src="{{Config::get('constants.path.img')}}/logoreport.png" v:shapes="Picture_x0020_3"></span><![endif]><span
  style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td height=7 class=xl6624351 width=17 style='height:5.25pt;width:13pt'></td>
   </tr>
  </table>
  </span></td>
  <td class=xl6624351 width=34 style='width:26pt'></td>
  <td class=xl6624351 width=158 style='width:119pt'></td>
  <td class=xl6624351 width=242 style='width:182pt'></td>
  <td class=xl6624351 width=136 style='width:102pt'></td>
  <td class=xl6624351 width=126 style='width:95pt'></td>
 </tr>
 <tr class=xl6724351 height=25 style='mso-height-source:userset;height:18.75pt'>
  <td height=25 class=xl6724351 width=17 style='height:18.75pt;width:13pt'></td>
  <td class=xl6724351 width=34 style='width:26pt'></td>
  <td colspan=4 class=xl7224351 width=662 style='width:498pt'><span
  style='mso-spacerun:yes'>   </span>PT. TROCON INDAH PERKASA</td>
 </tr>
 <tr class=xl6824351 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6824351 width=17 style='height:17.25pt;width:13pt'></td>
  <td colspan=5 class=xl7524351 width=696 style='width:524pt'><span
  style='mso-spacerun:yes'>            </span>LAPORAN JAMSOSTEK</td>
 </tr>
 <tr class=xl6624351 height=4 style='mso-height-source:userset;height:3.0pt'>
  <td height=4 class=xl6624351 width=17 style='height:3.0pt;width:13pt'></td>
  <td class=xl6624351 width=34 style='width:26pt'></td>
  <td colspan=4 class=xl7124351 width=662 style='width:498pt'></td>
 </tr>
 <tr class=xl6624351 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6624351 width=17 style='height:17.25pt;width:13pt'></td>
  <td colspan=2 class=xl7424351 width=192 style='width:145pt'>PERIODE</td>
  <td colspan=3 class=xl7424351 width=504 style='width:379pt'>: {{ $datafilter->period }}</td>
 </tr>
 <tr class=xl6624351 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6624351 width=17 style='height:17.25pt;width:13pt'></td>
  <td colspan=2 class=xl7324351 width=192 style='width:145pt'>DEPARTEMEN</td>
  <td colspan=3 class=xl7324351 width=504 style='width:379pt'>: {{ $datafilter->departmentname }}</td>
 </tr>
 <tr height=30 style='mso-height-source:userset;height:22.5pt'>
  <td height=30 class=xl1524351 style='height:22.5pt'></td>
  <td class=xl8124351 style='border-top:none'>No</td>
  <td class=xl8124351 style='border-top:none;border-left:none'>Departemen</td>
  <td class=xl8124351 style='border-top:none;border-left:none'>Nama Karyawan</td>
  <td class=xl8124351 style='border-top:none;border-left:none'>Tanggal Masuk</td>
  <td class=xl8124351 style='border-top:none;border-left:none'>Total Jamsostek</td>
 </tr>
 @php
   $t_total = 0; 

   $n = 1;
 @endphp
 @foreach($itemdata as $itemdatas)
 <tr height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl1524351 style='height:17.25pt'></td>
  <td class=xl7024351 style='border-top:none'>@php echo $n++; @endphp</td>
  <td class=xl6324351 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->departmentname }}</td>
  <td class=xl6324351 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->employee }}</td>
  <td class=xl6524351 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->joinemployee }}</td>
  <td class=xl6424351 style='border-top:none;border-left:none'>{{ number_format($itemdatas->jamsostek,0) }} &nbsp;</td>
 </tr>
  @php 
   $t_total += $itemdatas->jamsostek; 
   @endphp
 @endforeach
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524351 style='height:15.0pt'></td>
  <td colspan=4 class=xl7624351>TOTAL</td>
  <td class=xl6924351 align=right style='border-top:none;border-left:none'>@php echo number_format($t_total,0); @endphp&nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524351 style='height:15.0pt'></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524351 style='height:15.0pt'></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524351 style='height:15.0pt'></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524351 style='height:15.0pt'></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td colspan=3 class=xl8024351>JAKARTA, @php echo date("d M Y"); @endphp</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524351 style='height:15.0pt'></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl7824351><span
  style='mso-spacerun:yes'>                                 </span>DISETUJUI</td>
  <td class=xl7824351>MENGETAHUI</td>
  <td class=xl7924351>DIBUAT OLEH</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524351 style='height:15.0pt'></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl7724351></td>
  <td class=xl7724351></td>
  <td class=xl7724351></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524351 style='height:15.0pt'></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl7724351></td>
  <td class=xl7724351></td>
  <td class=xl7724351></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524351 style='height:15.0pt'></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl7724351></td>
  <td class=xl7724351></td>
  <td class=xl7724351></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524351 style='height:15.0pt'></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl7724351></td>
  <td class=xl7724351></td>
  <td class=xl7724351></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524351 style='height:15.0pt'></td>
  <td class=xl1524351></td>
  <td class=xl1524351></td>
  <td class=xl7924351><span
  style='mso-spacerun:yes'>                                 
  </span>(________________)</td>
  <td class=xl7824351>(_________________)</td>
  <td class=xl7824351>(________________)</td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=17 style='width:13pt'></td>
  <td width=34 style='width:26pt'></td>
  <td width=158 style='width:119pt'></td>
  <td width=242 style='width:182pt'></td>
  <td width=136 style='width:102pt'></td>
  <td width=126 style='width:95pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>
