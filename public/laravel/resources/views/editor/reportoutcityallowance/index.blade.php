@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    Report Out City Allowance
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Report Out City Allowance</li>
  </ol>
</section>

<section class="content">
  <div class="row"> 
    {!! Form::model($datafilter)!!}
        {{ csrf_field() }}
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <div class="col-sm-3" data-toggle="tooltip" data-placement="top" title="Department">
            {{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control', 'placeholder' => 'Select Department', 'id' => 'departmentid', 'onchange' => 'RefreshData();')) }} 
          </div>
          <div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Period">
            {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'RefreshData();')) }} 
          </div>   
          <div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Employee">
            {{ Form::select('employeeid', $employee_list, old('employeeid'), array('class' => 'form-control', 'placeholder' => 'Select Employee', 'id' => 'employeeid', 'onchange' => 'RefreshData();')) }} 
          </div>   
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
          {!! Form::close() !!}
        </div>  
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>Period</th> 
                  <th>Department</th>  
                  <th>Employee</th> 
                  <th>DK1</th>
                  <th>DK2</th> 
                  <th>DK3</th> 
                  <th>DK4</th> 
                  <th>DK5</th> 
                  <th>T DK</th> 
                  <th>Rate</th> 
                  <th>Total</th>  
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  



@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "scrollX": true,
         "sScrollXInner": "100%",
         "autoWidth": true,
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/reportoutcityallowance/data') }}",
         columns: [   
         { data: 'period', name: 'period' }, 
         { data: 'departmentname', name: 'departmentname' },  
         { data: 'employee', name: 'employee' }, 
         { data: 'dk1', name: 'dk1' },  
         { data: 'dk2', name: 'dk2' },  
         { data: 'dk3', name: 'dk3' },  
         { data: 'dk4', name: 'dk4' },  
         { data: 'dk5', name: 'dk5' },  
         { data: 't_dk', name: 't_dk' },  
         { data: 'rate', name: 'rate' },    
         { data: 'total', name: 'total' },     
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function RefreshData()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.periodfilteremp') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'departmentid': $('#departmentid').val(),  
            'departmentid': $('#departmentid').val(),    
            'periodid': $('#periodid').val()   
          }, 
          success: function(data) { 
            reload_table();
          }
        }) 
      }; 
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
    </script> 
    @stop
