@extends('layouts.editor.templatereport')
@section('content')
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
 
</style> 

<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 14">
<link rel=File-List href="Overtime_files/filelist.xml">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<style id="Report (Autosaved)_7378_Styles">
<!--table
  {mso-displayed-decimal-separator:"\,";
  mso-displayed-thousand-separator:"\.";}
.xl157378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl637378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl647378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl657378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl667378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl677378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl687378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl697378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl707378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:16.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl717378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl727378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl737378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl747378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl757378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border:.5pt solid windowtext;
  background:#E7E6E6;
  mso-pattern:black none;
  white-space:nowrap;}
.xl767378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl777378
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
-->
</style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="Report (Autosaved)_7378" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=1212 style='border-collapse:
 collapse;table-layout:fixed;width:912pt'>
 <col width=19 style='mso-width-source:userset;mso-width-alt:694;width:14pt'>
 <col width=78 style='mso-width-source:userset;mso-width-alt:2852;width:59pt'>
 <col width=116 style='mso-width-source:userset;mso-width-alt:4242;width:87pt'>
 <col width=232 style='mso-width-source:userset;mso-width-alt:8484;width:174pt'>
 <col width=102 span=5 style='mso-width-source:userset;mso-width-alt:3730;
 width:77pt'>
 <col width=69 style='mso-width-source:userset;mso-width-alt:2523;width:52pt'>
 <col width=81 style='mso-width-source:userset;mso-width-alt:2962;width:61pt'>
 <col width=107 style='mso-width-source:userset;mso-width-alt:3913;width:80pt'>
 <tr class=xl657378 height=7 style='mso-height-source:userset;height:5.25pt'>
  <td height=7 width=19 style='height:5.25pt;width:14pt' align=left valign=top><!--[if gte vml 1]><v:shapetype
   id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
   path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="Picture_x0020_1" o:spid="_x0000_s7170" type="#_x0000_t75"
   style='position:absolute;margin-left:7.5pt;margin-top:1.5pt;width:43.5pt;
   height:36.75pt;z-index:1;visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQBamK3CDAEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhvROFGcle3tSdZNgokh7Rjb3+8vyWK5tSObIJBxWPPbsuIM
UDltsK/5x/qleOCMokQtR4dQ8x0QXzbXV4v1zgOxRCPVfIjRPwpBagArqXQeMHU6F6yM6Rh64aX6
lD2Iu6q6F8phBIxFzBm8WbTQyc0Y2fM2lWcTjz1nT/NcXlVzYzOf6+JPIsBIJ4j0fjRKxnQ3MaE+
8SoOTmUi9zM0GE83SfzMhtz57fRzwYF7S48ZjAa2kiG+SpvMhQ4kvFFxEyBNlf/nZFFLhes6o6Bs
A61m8ih2boF2XxhgujS9Tdg7TMd0sf/X5hsAAP//AwBQSwMEFAAGAAgAAAAhAAjDGKTUAAAAkwEA
AAsAAABfcmVscy8ucmVsc6SQwWrDMAyG74O+g9F9cdrDGKNOb4NeSwu7GltJzGLLSG7avv1M2WAZ
ve2oX+j7xL/dXeOkZmQJlAysmxYUJkc+pMHA6fj+/ApKik3eTpTQwA0Fdt3qaXvAyZZ6JGPIoiol
iYGxlPymtbgRo5WGMqa66YmjLXXkQWfrPu2AetO2L5p/M6BbMNXeG+C934A63nI1/2HH4JiE+tI4
ipr6PrhHVO3pkg44V4rlAYsBz3IPGeemPgf6sXf9T28OrpwZP6phof7Oq/nHrhdVdl8AAAD//wMA
UEsDBBQABgAIAAAAIQDy5meAZQIAAKYFAAASAAAAZHJzL3BpY3R1cmV4bWwueG1srFTRitswEHwv
9B+E3hPLxokTE/tIk7tSOHqhtB+gk+VY1JaMpORylP57V5KdEFJo6fVN3tXujHZmvbo7dS06cm2E
kgWOpwQjLpmqhNwX+NvXh8kCI2OprGirJC/wKzf4rnz/bnWqdE4la5RG0EKaHAIFbqzt8ygyrOEd
NVPVcwnZWumOWvjU+6jS9AWad22UEDKPTK85rUzDud2GDC59b/uiNrxt1wGCV8KuTYGBg4sOd2qt
unCbqbYkq8iRckffAQ5PdV0uZ8nsknIRn9XqZaxwxzHm8vGSjBWQ8hW+8wXOqjNEGf8eNp3Ps2R2
zl3hJiF8g7sgy2wouQIe4XrBAq487gTb6YHE5+NOI1EVOMFI0g5Ugqw9aI5iHF3uhAqaQ5dHxb6b
QTf6D6p1VEjAUpuGyj1fm54zC+5xaEEDoBTg/OcV3edW9A+iBZFo7s5vphHs91fmU3UtGN8qdui4
tMGBmrfUgvtNI3qDkc5598xhmPpTFWPEwPwWJtprIS3Yjub8ZB+NHU7ooEWBfySLNSHL5MNkMyOb
SUqy+8l6mWaTjNxnKUkX8Sbe/HTVcZofDIfx03bbi/HpcXqjQSeYVkbVdspUFwXe4+4A75hEQYMj
bQtM/OA9NRDgQhGObsKOq7GaW9aMiDd4f95Uj+da1SDeFxDciX1uPAh/EdetoumdR2l+qnX3P5Bh
DOhUYL/RGL2C49ymusf7NyMGydksSSGGGKTDDg7DcSTcxV4b+5GrNxNCrhHYBCbhfUGPYIswkxFi
GEoYg98E2L1hIVsBDtxSS8edufrhDZXhB1v+AgAA//8DAFBLAwQUAAYACAAAACEAqiYOvrwAAAAh
AQAAHQAAAGRycy9fcmVscy9waWN0dXJleG1sLnhtbC5yZWxzhI9BasMwEEX3hdxBzD6WnUUoxbI3
oeBtSA4wSGNZxBoJSS317SPIJoFAl/M//z2mH//8Kn4pZRdYQde0IIh1MI6tguvle/8JIhdkg2tg
UrBRhnHYffRnWrHUUV5czKJSOCtYSolfUma9kMfchEhcmzkkj6WeycqI+oaW5KFtjzI9M2B4YYrJ
KEiT6UBctljN/7PDPDtNp6B/PHF5o5DOV3cFYrJUFHgyDh9h10S2IIdevjw23AEAAP//AwBQSwME
FAAGAAgAAAAhALNFUh4XAQAAhwEAAA8AAABkcnMvZG93bnJldi54bWxEUMtOwzAQvCPxD9YicaNO
o6ZtQt0qAqEiKhBt4cDNSpyHiO3KdpvA17NJiHKyZnZmZ9arTSMrchHGlloxmE48IEIlOi1VzuDj
+HS3BGIdVymvtBIMfoSFzfr6asWjVNdqLy4HlxNcomzEGRTOnSJKbVIIye1En4TCWaaN5A6hyWlq
eI3LZUV9z5tTyUuFCQU/iYdCJN+Hs8Tcx5eZ+XzdzbbyWIfbs4ub9OudsdubJr4H4kTjRvG/+zll
4EN7Cp4Ba+zXVLFKCm1Ithe2/MXyPZ8ZLYnRNQM8NtFV9yJ+yzIrHKpCL+gnAxMGPjK0Xep0b8Wo
zjqFFg/C6dILF0E3GqjZfL7wg9ZMx0odGP9v/QcAAP//AwBQSwMECgAAAAAAAAAhAI0MNmeGGgAA
hhoAABQAAABkcnMvbWVkaWEvaW1hZ2UxLnBuZ4lQTkcNChoKAAAADUlIRFIAAABOAAAAQwgGAAAB
RTMDVQAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L/GEFAAAACXBIWXMAACHVAAAh1QEEnLSdAAAa
G0lEQVRoQ+2bCVhTx77AD7KEfVWQRVAURAXUal1aLdX22rq2vVfb+nrVV2tdURQQhSSc7GHfXXBD
7Sp1qwsQIGQ/2SEiasW2ttper7TV3rbWaoW8mZMJEAghKH297333933/LzNz5pzM+c/2n//MwQaM
J676DQXt42/79fFYSq0RRbGEvZq/oyCGBTDEt1EQw9adNmKpdcad5772QylduPObH8BfX1zxGZYs
ML58QDMZ2/hp51M7cWFffEQGwJMqjUZHmBn+fQRD+B6ZbmbM5lYKCvZiYqHIFwVtM3evdg4KWjKS
K+OMy1HUouggsLZc54yCmCNV8zsKmnjjiCEaBTFsa3VvtfRkHFtCm5ij5EI1oSTMaDQOQcFugAxH
Dbc8sC1Vxpm5wudg0iuHGheQ18w476h/iCWDOgaZyCdCpQM2V7UOIzMMiBlF6v9CQfs41vKjvy+9
QRDBUryJkvrHP1N0zJMmfuTLUFVMKVClzShRpQVxNPs8MsQPQjiyoyibbdwzJPdQ0JIt1UZvmuQr
FLPAky69hoKWJJ27Og4FLZiXX+NPahdqNk3cf+WacWBesGwmEFg1SdXGxWXq18mHkvGaXg8dVyCb
gIJduPMukL2wE1giKNvrOybskoyI26cK6y4ol4nEmhgU6iKQJRajIHh/0G5B23VJEX7vsv70XbIx
wYHD/CcwDNP6A/S9XfA3ElcEuuwU3p6/TzcXB23bOa2eTWZAgPbu4M/RXoXhALrkFpnYF1OLNE/7
sdR6FLUKeKCzN1vTMpKvIPvJ/y6zd2nneTG133gyVXpvuoj2DGjQPvSGNB+mRunG0n+dUNY0CWXt
m9h81cfrTl0ZiaKdjGLW92q0Cyournhuly4JRe1j4R4ilKxpK7x+1BCLgpb40kTWFQ+bA2jAlHTR
LyjFgmFs+WgU7CKQJb2Cgp0k8IXLgunianN7S6gQuaJLnXjRxHdRsItxudLpKNjFhtPG+aVEXFfj
re/1uo6ZxJco2MU7+1v8UZBkBEt+khzNN583LijXryD76zaBMbWyZTjKQuJDF36Egl3glS0uKGgC
dSEHNDaTutt8zoitPWFZumRBGgp1sazbwzzo0q7+COjZ6eP45zrn7ymFqmMo2MXTpeoAFOws1RCg
I+/Umm/I1zXrDf52ayquGeJeFYfFFahegb9DkgTt2FYwjplv2ACMA/gwsgKQAN0Np9Z+Ay87cy50
WR5m3KkND1FwQETlKAtQsIu5u/XkbDSWr0iClRHAJD4fRhfpXzv6eaAnrrwBr00u0uf4UhuuUtIa
3hmyvZ60rCYWNlk3Poaz5SoUtIsRfOI8ClrHiy5ve2G35kUUtcrEfOXbfizFzyhqH3P3NL0Wl0cI
fGgNAj+akLvyI23vfjiYVLW2Ujjnrk9y31E1GUOy4silyRXXjb368aAzuVQfEsZXnXdlqP8VzpFX
Lv+geSa6NCBml+mfC+QQArdM4re4AjVvVcX1xy/8pAL1mnAeUYOi/RKBC89gW84b5+1RTURJNhGJ
jE5+TMXV9ScMgSjJPhbuuzA7mCU9jKL9EpYpMpAdGY5bYAD8S3njfHSpXzxpkl/Gl4k8UdQ2U4CB
7UKV/Iqi/bL8UGMUOdrAgpmHLCDP7dInoyw2SQD9PDpbUYqitonJURWNy1XQUNQWDtjak79Hpldr
cFGLJ5aCxtFuw+DYbOUBlNcmTswm2zaRGReqrGMcWxGBolbZduyGG7YVaAq0MSzxrNF7u+CbZZWV
jo7bG+5ZDPhAAugiBbqtTwJoDZem728OQlEbMBsfYN3WTj0p14FrcIY2G8PbQJUmVRn9ttddh9f9
MkS3sW1gYoECqxu8gMvWmu/Im/tgcr6iIopnxTLoiSun6UGviRoBRubI7tVGCihkGFv2CcpCEok3
FPimVZf7pgLZdqbcN/FE+Yhtpw6iy70YklK7Y8mBpoUoagO6/gFYdvbS3JQi9bJeBQPVFpunpE/i
CyeCZZ9pSQGmW1OVwuumPCYt1hgdtwpMy/Qe+NBE5xbvU/deevQEGAOPxuRYmndRfEW2aZhAAgsA
bIJ5+3SLns2Vzcc2niGrliwELEzPl0AvAu9xAFVdWWl0RI8mcaBprltfJPdgZqn+zZFZquMoio3M
qD2FbTrThm051wbakOk38fx38/eqpybwhMux1Z+0YetOg3RwbaugDWirDbTJ3pICBVzbWtOGbTrX
NqtAFon+AvOlCm22SQvc6fLWVadMdsbGU5dWBOGif0RwZXrYFj2ooh9GcCSGmCzpHrzm2jO+DJk6
hKu+vqPuC5+4YmJNEENSF5zd9CO41SEoW3czkCk6PJQpUcJnubB17Qm4yMkxXdYO45A5uzVLQnjS
Lv+EPXhSZfdREHu6WL151sHWYbBwQWxZgchodIrJIT5548jl6DC2XB7JJ5rwmhv+i/drVwXhktpQ
LtEC7wN5a0JYsuOjc7VkZwjIbHhrRon22UC6mDRnNp28HOCSIbNfa92JylF9OowpvU3cAOPaIBPC
kl4EzUeHok/GKrCaC+dLP3ZnaX/wYSi+G87TfjScIVkXiIte9Eivj5++Sxk/C4gvCIPp6AVgt60J
4ioL/dhqnWt6Q4crQ9cSwZOzufVf2DHY/oc/mfijAg8wc3RaweP4dZPZwksRYNyyGMv+UHDB54Fh
bGmaL0enccuU/+rDVN4O4GirJxTo94WxJNuhawJKdJY8bQSPyBvKVZ/2YhI3XemyX31YGuX4XFUi
jtsx0A6EmGzVcp9M6UNXXG2IzVclFqtavdElu8knbviP4smpbizN10MZisvPlOqnokuPx+kr33lR
MhTto3iyEpRkkxF0YZkc3IOifdLSYnQJ4xNXwXz62yorzoz+MRodXDLkv6z+UB+CUmwC2peLw5Zq
ow9N8jlK6pf4IkXsUIastyOkP6aW6N+YVaYvQtF+8afWi80T//xD2jiU3C9+uLz1pQMXxqKofbil
i9sXlevcUdQmy8qAiQ60RnougG3nkibsnDf7Y/3Zy7EBTEtb0CarKluGe1Ab/oGi/eKdInhAeonM
dhwwj6YVqlaiy/3itlPY2/3cF/HAZI4vVG5A0X7x31l3o3P1hWw3l50iq0alNQJwmd1rY8w9Q3yP
IDrsmujj8NqPoQnksK2mo6eROYorK0bZbBKVJX93w/HmTtvOJo50ZQcK2iSKVpOCvfOJEa66gujC
U+SCp1vhgIEJPWcOptx9s/S9xvFjsyXbUbRvcJHIyR1XtqGoTRw3n32IrT9l9NxadQea3Y5pwh6F
qzOO4MgaUfY+wYFt6E+X2PZ7Qebu18eHIavVFjFc6XFyzQpdpWD9MCtbmjCGJ9lKVq25euFvsqAj
objHlocVKBnSZhTsG7+M+vlPFxJlKGqVl99TeZO+XLiYgf4RUEhKcs0jOHBTdjY8NC2CyIKR1wOo
9eR61hbunItfoGDfjM2SU8dw5V2buFYIZogNSCumAsKeuvm88ZVDmhEL9zdPIdPMBYcDc+I54+xc
ic1dEye67iYK2iCphvrXIxdXoFgvFh4wxHa2KbKAQEAhwlldw4FfuvC2yTsOqn0TWDKCdumR+OkP
6LJVPJj6OyjYN0GZDblL37/UZ+H8cemXnYXr3q4qujzC2fIrXhar/SQgG0+UJ3+g6729hrCrcFhy
DfWVIwarhVt0oGkeuYLvXjiguRnFxPGXi6socaDh99oG6CYT8iQj0KN6Q2+yvh3anVmlqkwPmtjq
tjBlp/iuRcGA1py215GOfPeUulayKtH82uWKAEJWPUgH8298tjSTfFgPfLNa+rdmxmYpXhrFlZSj
aCcv7FZvIKsQFcrc3sZnK3ZsPt0yHjZ6aw7Ezvyo52JJAuPq01csbD64d+mFSwwo2jdrjn02yo8u
uoCiJGt1Rmcsua7D9MdAUC+lpNaRu9nOiaDRw/EO/jm83rNw3QsIXmA0S3KSfDBi2zHCLYJnp2Xi
hmssDpRMyFKUkEMC/HNYPWiIiMuSzZ/Kql2CrTtJOhD71JxZzNoD1Y+Lrnd2oIR9TZOis2TvoKht
nKiKzu0n+FYOidXfYZvOIkcOEPDrkVJLDqwuaz5pw9aebMM2nAHXatpAwUwOm74cOcngelJ1m3dK
jZT8A0AIR7J+/YnP7fOoD2dJiFePNCeg6B9OAFNejYL9s/Bg40R/mvAiimIxuSq1E13RMbVQfTq2
tPnISK5U4c/T3cdFRqdQLnErIFNcOjZP0wSatoMPQ34/gC7+5OlSdfLzuw2gc8lbhvF0N14s1Yf4
4ApiaqGmJDpLOQvbXDUePR6YaAPcunNKlzx6ubjrDJD7TiF5tiE2T3nYly7Sjc5R34ceJ29qwxGY
Dv5Yt2CfelIYW/Q2jAMcRvFV5PEFuLgezZOzfaiimrg85Z5ZJbqNYDwlB2Rc9NnQYUyFCIbtZs5u
/eq4fHUuimIjWJKf4O/s/c374G90loIorqqiROWoa4czpR9H56oq4BGHEJ5aH86Vn4kpMrwZV0A8
FcUntMFsWfOyMvXwkTzZOXhvKFd9eShu8scFs6UNk/KJgTch1wzpv1Z91PucwGCxrFLn407t43BN
f8CjUo4ZRPuCigv/jZIGjcX7DS8PyVA+evGILhwlDRw4lISwZMUUXP1LTDZRnF9zw2LjfyCs/eCz
mFFc6cduDOW9YJ56L37mW7uWnnYxqUj7ji9LrXdLl/7uw1QpRvEUOeNyidenFavmhOL1pOMQ/Hl8
fJFyVihHsnAkV54Wytef8mXKf3Bjqu76MKTnn9ulWYwe98ey+pNLk2PzlVuGZkp2hXOkAngkLIQp
FcRkK4/NKNKw33rv8ryq1o4+T/39h//wRwBMtWVlIs9ni3ThU/MUsIkuGJ+nTg7mEWWhfI3Aj0k0
BeDS7512in50zxC1U3YCSWtod00TWYjLDlG7Wzq8Lr7vRZf+6MeQtw5jEw1BXG15XKGaFpMtXRTG
VcSCvhm29ox9/qd/K2g1X06cWKDKH8YQ33TG1Q8d8KZH7pnyjmG45KtIPnH02T3axBUnP5tF7kSs
1TmDqdUJTBSOJk+xETougMDfnkI6NRwwHB+CwbxwW29tuXNWnc5n0aELT83eq9s0Jkvx8TBc9A8X
GtGO4bpHlAzZPTCxCl88oF/R0/b+U1l67IYbPJPjTpXLKTTiX764/J8jc7QH4abshBzt6FyBwQNl
HTCvH9I+9bf92udRdOCAlr78g8+GxuSppkwo0GQMZRJaN1CBHgzV9agc5Z43P7zU/8buYPNfHzT7
RWepjrvQlPcj+Mqzr79/xfqR7cdkTrFqiley4JbjVoExPkuSDr1e6NITAz228QWaVG+OutmdLv/X
zF16s+35xzI+n/D3ZchFXjTpz4knrtp1ImggvFiiWE6B/kboYgGLuyHJNQ8j2XJeAm4cNOWRgC4f
m69Kd06XPRidpWDCFoquDD5wXzqMLZMD4+f6nN2age0a2EEcX77GJVlwj3RowNUvXN0m1xgdUus7
4gqUFkvwwQA6imNziVxHquzX6Bw5dM0PvvJwUEOhHHmuU7r83oQcwu7NBXuZXahkOG2paTd5gYDC
oNfR7DYgXQf1xuFsov7lg42P8alC30AHTWSW8owXXXYzAK+1aw9tQDxTqFziTJd/E8GVnoOH8VDy
oDCO25DksBUoBzq+ST9LN6VZ+FnqjYFshXRz1cC3Um0Rn6sa5ZEp//LZvYa/oqTBIQIXuQZkNjR7
ZNQ/BDNVnx7Tx+FGh2mDaPlhw0pn5FYmlUb6HXsqziReVOnPSaevDNpQAVe+oRxZfgRPehG0wMHb
QJ9/sOltZzphDGYrLD+jegLWvKcKC0w6dYWy9vjdKbhgKUybmCN7jZIm/t6yi/ZWHBRPuuSrGSXa
aeTDBoFJBcSLrgzNnTmlmiUo6ckJ5xISsMz/bVKBinzBJ+WZHOFo/9TzBvLY/rvHjc7rTrU/m92Q
Aq8tKlfO8siQ3LXV4sziSRX//MqhC7PJhz4h0MRyZBm+DmWJ96CkJyOjui3YJV30my9NfHVsofyJ
u8fKj1pH++8Q3CWd2tB3vAnKp0Zs46cdo3HhXlx03fW1o4ZAP5roAlCcad/WmgJRusNO0UPSh/yE
5gScJCjpklYPqkiNn+ryUT82f9mrnefKufgogCY+FJynG4qSH4uZRepFlB3CuyYHPBQwg5q3xRLP
GR0Sq9vHMMWnYN6Vx5pCg3FxS2fLsyawS4PZ1yVV8BCshQf22ZAVPDKE1b685tYlAz2gYY2EPfpt
GE3/IJIvS1p6jHjsI33PlzW+QiqNHPjhCyOBSiQVCZUIt5OrjMMyhJdWVTSNhD6p6GzFCQdyUwrd
13k/mHnJE8mme4eAZ8VyGvaRn7Y+JnG5RKUDw3DNJa3W6veGA8Izo4FK4RgezNmtX9H9KyF7gV7r
qSWa7Y47xb26WVerAYJaoUuK8MHCPapEdDuWUHHdNZIt2eWwreaRSUldyiJbK9xkg91946fGIRvO
tIdnVB9ehD+mdySldgfcS11SYc/p6X5wSqnNhN9Twg3hvo6A9wkYN8bw5LhTat3vFuNTTwWiFuS+
s/6nl/doLT/aRSSUKJnYNrAU2wK3QoFsBsqDJ7g3wQMPQHHrwTj57kkoHSNTzirBmDXgSjYrrteH
w49FUg3Vg988YMXBTZpxuYoKx7SG3grrLqTiao2UtIYHCSW2v46CrXdZpdGxP4EHgkj30wABK6Jy
CkN/69lS5eN7ZcwEMsRUR07Tg6kFqhXj8Uq7FPdqqTogiCU7jaX2Po9jIUhpflTJ9ZRzl4CdLXKa
yBPzXVMFdynbBHcoybV3KCl1dyipSLbX36Gk9RCYth1dh3lTwD3wvmTBnVCqkHjrPW0wKla/DGOI
P3LmGL4YnWXlM+aBMrNYs9qB1vhwcoGSaY8zEFrh4VmEBC6P+lQWUhgU70y54bmixijQRh1iuA05
TolVYL2Kll6dqwgk5nuh9HweFHM+OHGgMdBnu+D+W4cuPIWKZxNgw10aki5rjimwX9l98vxew3QK
XdUexZWLZu/S9H0GBLD6fX1EABXMnOTxjG4v1F1IhcEXrDWGMySfJ6FPGOI4wrMOiec6yMG+0zuC
1qzwHnhv9wqwJt2fD5UHFb+l2ui6vf52PF8ylyxkH8Clliud+MKLJhKCtfCT7zAdNdzyAMboD14M
xa24PNUUlNyLhDL1JJ+dDa0m8wIUmjQX4AuglyBfxPQyQ7ZWd4SzpKfhd26wW4em156FR5vgzNhL
ceZTH/YoDoo5r4XyqoyuqXW/LD3Y2OfKZ0GFevgQVvPNMI6Mi5KenDFZihJHmrJjUqHG6qGejZWG
WR6p9T+SL2x+afi9UqeYap4UEB/FEp/e/J7KO+WowSM85ZwYexcsvdYBxcFPjOEsST4DvjQQUhFI
KdtB97dHSAUiJUIFIuXBcz9xXAmNnDx6EJurWOhKV95dfEhns2UOiB2nroz0oIl/GsqSXT/zbY9t
ZDh7TSv2xv5+1KNLBFbElD5mc1VXNwjLd8OWlXla3tvH/Sndfu2RnvebZfVpL3KDqBvQPg1hS0sj
+dImaA2g5CcHTvEzSzQcCo34aUIekYeSOyFPHaYI3wYt5C0srfZ1LKlupT9NtBSaD+OzJNOwZOFy
bItg1ZDUupXPlGlfqRTd7vxmdQlY3kTyFDuCOJL8acWareyaaxbj6LxdqjkRXDlrNE+R89KBC0vy
CdMHSXBt+erhK1PG5iiYI9jSvAl5yg28k5c7v7z3oNfP8UwX/pX8MBMQlt/i75hWv5RUXA8m5yoi
3DPlrS/sv/gGSho8VoHFNyjgCRea7McZVs4KwMEV2EFb4QGdxfubg2Ac2n2xebrDbumi91/Yq40D
697pYWz5hWCG9ATcARuTJT/pz1HLFh5uCoXPmLu/aZonrvhyfBaR+NqJC2E+DNmV4UzpAfPLR2cr
34DnF0DQIZyvPByVo6m58p3pM4uX9mqfcseJtkieZD2M+9JFgkCmTOKdKfl1eqlm7YQ8zTOO22sf
mg/8dGdsjnK/G13c5sYhyHIMOvgZnXsYW3rFF5e3PtXjc0jImCxiqxtQXCxXSX69BRU3oVB32CFF
uBdbe8Z9cYVuhgdV8m0ER5pbWVnpODlf+aE/LjfM2qWPn1as8p5epF3gw1bffLpQ+e6bH34R5I/L
msO5xOnoPNHQiYVNvrHF+s2Tygxb4PAQkastG8ZSVD1dph4+JUvnM7+8cb4XQ35vNF9GuvU9aSIB
llz3FqhAlxFcQhLMVnzhmCr4tafiEnY3rnFOl/0ewhDjcH8XJQ8+8/YT/sM4CpEXTXZ/+QeWB+ng
B4UbTl4dB7s2SsJw+dchGyuvD0dRjF93NZIq+GoU3MOAcTCz+sTkqaOd08Rxk/OJMSDeOYbCQ+3x
uYZAYExPiOZK40GrCu4a2OFegc7nqXwizpcmjptWohrV/QuSlOOXIjpd7EAhq0F83bHW8d3X2zNL
dK+60mU/BrHENYN6BKgvoCE8NofY5ZIhexDGIY5uPNYyBl36PwEcSqKyldnuNMmvMbmq3aJB3Le1
i4VwYzpbyfNgaa574srfovJ0olll2uXwu2+4gkDZ/lTKdd+6w63MSQXqVZG5GoFbJvGTC0N9M4qv
fH/ln7Gbb401Jy9Hx+UqSvxw6fcOdO0jZ6qyPZAp/hqMO8VLKrQLufX/DDKfFYGeE3Tb42I6RwKf
BZ656cjlgL9/2PyXiYVq7mi+osmTJn3kQNO0OzMbfw9iSb+dXKjlnrx4u9e4/G9JruCWx/RC5cjZ
Zernxuao1o3MUR8M5Sm1fgzpbfcMye+udPlDUPs/u7AMba5M/T8dMjWfOzEbr/nwDZ3izGq8NoSh
vebC1H4L87kw9XcoNMUDN6rskWeG6IEHXXYthK8Sj8rV7ostVG+JL1DMjcwWR8GxGG44o6L8/wYa
n1DgpGAWcxrK8m8Ehv0PtWdK5TnRD4AAAAAASUVORK5CYIJQSwECLQAUAAYACAAAACEAWpitwgwB
AAAYAgAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBLAQItABQABgAIAAAA
IQAIwxik1AAAAJMBAAALAAAAAAAAAAAAAAAAAD0BAABfcmVscy8ucmVsc1BLAQItABQABgAIAAAA
IQDy5meAZQIAAKYFAAASAAAAAAAAAAAAAAAAADoCAABkcnMvcGljdHVyZXhtbC54bWxQSwECLQAU
AAYACAAAACEAqiYOvrwAAAAhAQAAHQAAAAAAAAAAAAAAAADPBAAAZHJzL19yZWxzL3BpY3R1cmV4
bWwueG1sLnJlbHNQSwECLQAUAAYACAAAACEAs0VSHhcBAACHAQAADwAAAAAAAAAAAAAAAADGBQAA
ZHJzL2Rvd25yZXYueG1sUEsBAi0ACgAAAAAAAAAhAI0MNmeGGgAAhhoAABQAAAAAAAAAAAAAAAAA
CgcAAGRycy9tZWRpYS9pbWFnZTEucG5nUEsFBgAAAAAGAAYAhAEAAMIhAAAAAA==
">
   <v:imagedata src="Overtime_files/Report%20(Autosaved)_7378_image001.png"
    o:title=""/>
   <x:ClientData ObjectType="Pict">
    <x:SizeWithCells/>
    <x:CF>Bitmap</x:CF>
    <x:AutoPict/>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:10px;margin-top:2px;width:58px;
  height:49px'><img width=58 height=49
  src="{{Config::get('constants.path.img')}}/logoreport.png" v:shapes="Picture_x0020_1"></span><![endif]><span
  style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td height=7 class=xl657378 width=19 style='height:5.25pt;width:14pt'></td>
   </tr>
  </table>
  </span></td>
  <td class=xl657378 width=78 style='width:59pt'></td>
  <td class=xl657378 width=116 style='width:87pt'></td>
  <td class=xl657378 width=232 style='width:174pt'></td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=69 style='width:52pt'></td>
  <td class=xl657378 width=81 style='width:61pt'></td>
  <td class=xl657378 width=107 style='width:80pt'></td>
 </tr>
 <tr class=xl667378 height=25 style='mso-height-source:userset;height:18.75pt'>
  <td height=25 class=xl667378 width=19 style='height:18.75pt;width:14pt'></td>
  <td colspan=11 class=xl707378 width=1193 style='width:898pt'><span
  style='mso-spacerun:yes'>         </span>PT. TROCON INDAH PERKASA</td>
 </tr>
 <tr class=xl677378 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl677378 width=19 style='height:17.25pt;width:14pt'></td>
  <td colspan=11 class=xl737378 width=1193 style='width:898pt'><span
  style='mso-spacerun:yes'>           </span>LAPORAN OVERTIME</td>
 </tr>
 <tr class=xl657378 height=4 style='mso-height-source:userset;height:3.0pt'>
  <td height=4 class=xl657378 width=19 style='height:3.0pt;width:14pt'></td>
  <td class=xl657378 width=78 style='width:59pt'></td>
  <td colspan=4 class=xl697378 width=552 style='width:415pt'></td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=69 style='width:52pt'></td>
  <td class=xl657378 width=81 style='width:61pt'></td>
  <td class=xl657378 width=107 style='width:80pt'></td>
 </tr>
 <tr class=xl657378 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl657378 width=19 style='height:17.25pt;width:14pt'></td>
  <td colspan=2 class=xl727378 width=194 style='width:146pt'>PERIODE</td>
  <td colspan=3 class=xl727378 width=436 style='width:328pt'>: {{ $datafilter->period }}</td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=69 style='width:52pt'></td>
  <td class=xl657378 width=81 style='width:61pt'></td>
  <td class=xl657378 width=107 style='width:80pt'></td>
 </tr>
 <tr class=xl657378 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl657378 width=19 style='height:17.25pt;width:14pt'></td>
  <td colspan=2 class=xl717378 width=194 style='width:146pt'>DEPARTEMEN</td>
  <td colspan=3 class=xl717378 width=436 style='width:328pt'>: {{ $datafilter->departmentname }}</td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=102 style='width:77pt'></td>
  <td class=xl657378 width=69 style='width:52pt'></td>
  <td class=xl657378 width=81 style='width:61pt'></td>
  <td class=xl657378 width=107 style='width:80pt'></td>
 </tr>
 <tr height=32 style='mso-height-source:userset;height:24.0pt'>
  <td height=32 class=xl157378 style='height:24.0pt'></td>
  <td class=xl757378 style='border-top:none'>NO</td>
  <td class=xl757378 style='border-top:none;border-left:none'>Department</td>
  <td class=xl757378 style='border-top:none;border-left:none'>Employee</td>
  <td class=xl757378 style='border-top:none;border-left:none'>OT1</td>
  <td class=xl757378 style='border-top:none;border-left:none'>OT2</td>
  <td class=xl757378 style='border-left:none'>OT3</td>
  <td class=xl757378 style='border-left:none'>OT4</td>
  <td class=xl757378 style='border-left:none'>OT5</td>
  <td class=xl757378 style='border-left:none'>Rate</td>
  <td class=xl757378 style='border-left:none'>Overtime</td>
  <td class=xl757378 style='border-left:none'>Amount</td>
 </tr>
 @php
   $t_total = 0; 

   $n = 1;
 @endphp
 @foreach($itemdata as $itemdatas)
 <tr height=25 style='mso-height-source:userset;height:18.75pt'>
  <td height=25 class=xl157378 style='height:18.75pt'></td>
  <td class=xl637378 style='border-top:none'>&nbsp;@php echo $n++; @endphp</td>
  <td class=xl637378 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->departmentname }}</td>
  <td class=xl637378 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->employee }}</td>
  <td class=xl647378 style='border-top:none;border-left:none'>{{ $itemdatas->ot1 }}&nbsp;</td>
  <td class=xl647378 style='border-top:none;border-left:none'>{{ $itemdatas->ot2 }}&nbsp;</td>
  <td class=xl647378 style='border-top:none;border-left:none'>{{ $itemdatas->ot3 }}&nbsp;</td>
  <td class=xl647378 style='border-top:none;border-left:none'>{{ $itemdatas->ot4 }}&nbsp;</td>
  <td class=xl647378 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->ot5 }}</td>
  <td class=xl647378 style='border-top:none;border-left:none'>{{ number_format($itemdatas->rate,0) }} &nbsp;</td>
  <td class=xl647378 style='border-top:none;border-left:none'>{{ number_format($itemdatas->overtime,0) }} &nbsp;</td>
  <td class=xl647378 style='border-top:none;border-left:none'>{{ number_format($itemdatas->amount,0) }} &nbsp;</td>
 </tr>
  @php 
 $t_total += $itemdatas->amount; 
 @endphp
 @endforeach
 <tr height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl157378 style='height:17.25pt'></td>
  <td colspan=10 class=xl747378>TOTAL</td>
  <td class=xl687378 align=right style='border-top:none;border-left:none'>@php echo number_format($t_total,0); @endphp &nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl157378 style='height:15.0pt'></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl157378 style='height:15.0pt'></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl157378 style='height:15.0pt'></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl157378 style='height:15.0pt'></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td colspan=4 class=xl777378>JAKARTA, @php echo date("d M Y"); @endphp</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl157378 style='height:15.0pt'></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td colspan=2 class=xl767378>DISETUJUI</td>
  <td colspan=2 class=xl767378>MENGETAHUI</td>
  <td colspan=2 class=xl767378>DIBUAT OLEH</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl157378 style='height:15.0pt'></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl157378 style='height:15.0pt'></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl157378 style='height:15.0pt'></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl157378 style='height:15.0pt'></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl157378 style='height:15.0pt'></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td class=xl157378></td>
  <td colspan=2 class=xl767378>(_________________)</td>
  <td colspan=2 class=xl767378>(_________________)</td>
  <td colspan=2 class=xl767378>(_________________)</td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=19 style='width:14pt'></td>
  <td width=78 style='width:59pt'></td>
  <td width=116 style='width:87pt'></td>
  <td width=232 style='width:174pt'></td>
  <td width=102 style='width:77pt'></td>
  <td width=102 style='width:77pt'></td>
  <td width=102 style='width:77pt'></td>
  <td width=102 style='width:77pt'></td>
  <td width=102 style='width:77pt'></td>
  <td width=69 style='width:52pt'></td>
  <td width=81 style='width:61pt'></td>
  <td width=107 style='width:80pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>
