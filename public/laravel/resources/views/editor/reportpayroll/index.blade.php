@extends('layouts.editor.template')
@section('title', 'Laporan Gaji')
@section('content')
<!-- Content Wrapper. Contains page content -->
<style type="text/css">
.my_class {
  background-color: white;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    Report Payroll
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Report Payroll</li>
  </ol>
</section>

<section class="content">
  <div class="row"> 
    {!! Form::model($datafilter)!!}
    {{ csrf_field() }}
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <div class="col-sm-2" data-toggle="tooltip" data-placement="top" title="Department">
            {{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control', 'placeholder' => 'Select Department', 'id' => 'departmentid', 'onchange' => 'RefreshData();')) }} 
          </div>
          <div class="col-sm-2" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Period">
            {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'RefreshData();')) }} 
          </div>  
          <button onClick="GenerateData()" type="button" class="btn btn-danger btn-flat"> <i class="fa fa-magic"></i> Generate!</button>
          <button onClick="showreport()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-print"></i> Cetak Laporan</button>
          <!-- <button onClick="showslip()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-print"></i> Cetak Slip</button> -->
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
          {!! Form::close() !!}
        </div>  
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>ID</th>
                  <th>Employee</th> 
                  <th>Position</th> 
                  <th>Basic</th>
                  <th>UM Kantor Tetap</th>
                  <th>Lembur Tetap</th>
                  <th>Tunj Keahlian</th>
                  <th>Tun Jamsostek</th>
                  <th>Klaim</th>
                  <th>Tun Kesehatan</th>
                  <th>U Makan,Trnsprt</th>
                  <th>Lembur Proyek</th>
                  <th>TM,TDK,TLK,Inst</th>
                  <th>Pendapatan Lain</th>
                  <th>Gaji Bruto</th>
                  <th>Biaya Jabatan</th>
                  <th>THT</th>
                  <th>Netto</th>
                  <th>PTKP</th>
                  <th>PKP</th>
                  <th>PPh21</th>
                  <th>Pot Jamsostek</th>
                  <th>Pot Pensiun</th>
                  <th>Pot Lainnya</th>
                  <th>Pot Telat</th>
                  <th>Gaji Bersih</th>
                  <th>MH Dibayar</th>
                  <th>Pinjaman Personal</th>
                  <th>Pinjaman Asuransi</th>
                  <!-- <th>Total Pinjaman</th> -->
                  <th>Total Netto</th>
                  <th>Department</th>
                </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot align="right">
                <tr><th></th><th></th><th></th><th></th></tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  



@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "scrollX": true,
         "sScrollXInner": "250%",
         "order": [[ 0, "asc" ]],
         "autoWidth": true,
         "rowReorder": true,
         fixedColumns:   {
          leftColumns: 3
        },
        "aoColumnDefs": [
        { "sClass": "my_class", "aTargets": [ 0, 1, 2 ] }
        ],
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]], 
        ajax: "{{ url('editor/reportpayroll/data') }}",
        columns: [   
        { data: 'employeeid', name: 'employeeid' }, 
        { data: 'employee', name: 'employee' }, 
        { data: 'position', name: 'position' }, 
        { data: 'basic', name: 'basic', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'mealtransall', name: 'mealtransall', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'overtimeall', name: 'overtimeall', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'tunj_keahlian', name: 'tunj_keahlian', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'tunj_jamsostek', name: 'tunj_jamsostek', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'medicalclaim', name: 'medicalclaim', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'tunjangankesehatan', name: 'tunjangankesehatan', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'uang_makan', name: 'uang_makan', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'lembur_proyek', name: 'lembur_proyek', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'tm_tdk_tlk_instpd', name: 'tm_tdk_tlk_instpd', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'otherall', name: 'otherall', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'gaji_bruto', name: 'gaji_bruto', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'biaya_jabatan', name: 'biaya_jabatan', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'tht', name: 'tht', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'netto', name: 'netto', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'ptkp', name: 'ptkp', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'pkp', name: 'pkp', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'pph21', name: 'pph21', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'pot_jamsostek', name: 'pot_jamsostek', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'pot_pensiun', name: 'pot_pensiun', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'otherded', name: 'otherded', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'potterlambat', name: 'potterlambat', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'gaji_bersih', name: 'gaji_bersih', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'mh_dibayar', name: 'mh_dibayar', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'employeeloan', name: 'employeeloan', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'insuranceloan', name: 'insuranceloan', render: $.fn.dataTable.render.number( ',', '.', 0) },
        // { data: 'loan', name: 'loan', render: $.fn.dataTable.render.number( ',', '.', 0) },
        { data: 'totalnetto2', name: 'totalnetto2', render: $.fn.dataTable.render.number( ',', '.', 0) }, 
        { data: 'departmentname', name: 'departmentname' }, 
        ]
      });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
        function reload_table()
        {
          // GenerateData();
          table.ajax.reload(null,false); //reload datatable ajax 
        }

        function GenerateData()
        {
   
          var periodid = $('#periodid').val();
          var perioddesc = $("#periodid option:selected").text();

          $.confirm({
            title: 'Confirm!',
            content: 'Are you sure to generate <b><u>' + perioddesc + '</u></b> data?',
            type: 'red',
            typeAnimated: true,
            buttons: {
              cancel: {
               action: function () { 
               }
             },
             confirm: {
              text: 'CREATE',
              btnClass: 'btn-red',
              action: function () { 
                waitingDialog.show('Proses...', {dialogSize: 'sm', progressType: 'primary'});
               $.ajax({
                url : 'payroll/generate/' + periodid,
                type: "POST",
                data: {
                  '_token': $('input[name=_token]').val() 
                },
                success: function(data)
                { 
                  var options = { 
                    "positionClass": "toast-bottom-right", 
                    "timeOut": 1000, 
                  };
                  waitingDialog.hide();
                  toastr.success('Successfully genareted data!', 'Success Alert', options);
                  reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                  $.alert({
                    type: 'red',
                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                    title: 'Warning',
                    content: 'Error generate data!',
                  });
                }
              });
             }
           },

         }
       });
      }
      function RefreshData()
           {  
            $.ajax({
              type: 'POST',
              url: "{{ URL::route('editor.periodfilteronly') }}",
              data: {
                '_token': $('input[name=_token]').val(),     
                'periodid': $('#periodid').val(),   
                'departmentid': $('#departmentid').val(),   
              }, 
              success: function(data) { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully filtering data!', 'Success Alert', options);
                reload_table();
              }
            }) 
          }; 
         $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();   
      });

      /**
       * Module for displaying "Waiting for..." dialog using Bootstrap
       *
       * @author Eugene Maslovich <ehpc@em42.ru>
       */

        var waitingDialog = waitingDialog || (function ($) {
            'use strict';

          // Creating modal dialog's DOM
          var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
              '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
              '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
              '</div>' +
            '</div></div></div>');

          return {
            /**
             * Opens our dialog
             * @param message Process...
             * @param options Custom options:
             *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
             *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
             */
            show: function (message, options) {
              // Assigning defaults
              if (typeof options === 'undefined') {
                options = {};
              }
              if (typeof message === 'undefined') {
                message = 'Loading';
              }
              var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
              }, options);

              // Configuring dialog
              $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
              $dialog.find('.progress-bar').attr('class', 'progress-bar');
              if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
              }
              $dialog.find('h3').text(message);
              // Adding callbacks
              if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                  settings.onHide.call($dialog);
                });
              }
              // Opening dialog
              $dialog.modal();
            },
            /**
             * Closes dialog
             */
            hide: function () {
              $dialog.modal('hide');
            }
          };

        })(jQuery);


      //   $(document).ready(function() {
      //     $('#dtTable').dataTable({
      //       "footerCallback": function ( row, data, start, end, display ) {
      //             var api = this.api(), data;
       
      //             // converting to interger to find total
      //             var intVal = function ( i ) {
      //                 return typeof i === 'string' ?
      //                     i.replace(/[\$,]/g, '')*1 :
      //                     typeof i === 'number' ?
      //                         i : 0;
      //             };
       
      //             // computing column Total of the complete result 
      //             var monTotal = api
      //                 .column( 1 )
      //                 .data()
      //                 .reduce( function (a, b) {
      //                     return intVal(a) + intVal(b);
      //                 }, 0 );
              
              
      //             // Update footer by showing the total with the reference of the column index 
      //       $( api.column( 0 ).footer() ).html('Total');
      //             $( api.column( 1 ).footer() ).html(monTotal); 
      //         },
      //         "processing": true,
      //         "serverSide": true,
      //         "ajax": "{{ url('editor/reportpayroll/data') }}"
      //     } );
      // } );

      function showreport()
      {
       var url = '../editor/reportpayroll/printreport';
         PopupCenter(url,'Popup_Window','1200','650');
      }

      function showslip()
      {
       var url = '../editor/reportpayroll/printslip';
         PopupCenter(url,'Popup_Window','500','650');
      }

      function PopupCenter(url, title, w, h) {  
        // Fixes dual-screen position                         Most browsers      Firefox  
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
        var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
        var top = ((height / 2) - (h / 2)) + dualScreenTop;  
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
        
        // Puts focus on the newWindow  
        if (window.focus) {  
          newWindow.focus();  
        }  
      } 
    </script> 
    @stop
