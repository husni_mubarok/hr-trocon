<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 15">
<link rel=File-List href="Report%20payroll_files/filelist.xml">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<style id="Report Payroll_28673_Styles"><!--table
  {mso-displayed-decimal-separator:"\,";
  mso-displayed-thousand-separator:"\,";}
.xl6328673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6428673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6528673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6628673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:16.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6728673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6828673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6928673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7028673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border-top:none;
  border-right:.5pt solid windowtext;
  border-bottom:.5pt solid windowtext;
  border-left:.5pt solid windowtext;
  background:#D9D9D9;
  mso-pattern:black none;
  white-space:normal;}
.xl7128673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border-top:none;
  border-right:.5pt solid windowtext;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  background:#D9D9D9;
  mso-pattern:black none;
  white-space:normal;}
.xl7228673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border-top:.5pt solid windowtext;
  border-right:.5pt solid windowtext;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  background:#D9D9D9;
  mso-pattern:black none;
  white-space:normal;}
.xl7328673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border-top:none;
  border-right:.5pt solid windowtext;
  border-bottom:.5pt solid windowtext;
  border-left:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7428673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  border-top:none;
  border-right:.5pt solid windowtext;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7528673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border-top:none;
  border-right:.5pt solid windowtext;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7628673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:"\#\,\#\#0";
  text-align:right;
  vertical-align:bottom;
  border-top:none;
  border-right:.5pt solid windowtext;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7728673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  border-top:none;
  border-right:.5pt solid windowtext;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7828673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border-top:none;
  border-right:.5pt solid windowtext;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7928673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl8028673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:1.0pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl8128673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:1.0pt solid windowtext;
  border-right:none;
  border-bottom:none;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl8228673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl8328673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl8428673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl8528673
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
--></style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="Report Payroll_28673" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=2761 class=xl8328673
 style='border-collapse:collapse;table-layout:fixed;width:2067pt'>
 <col class=xl8328673 width=13 style='mso-width-source:userset;mso-width-alt:
 475;width:10pt'>
 <col class=xl8328673 width=34 style='mso-width-source:userset;mso-width-alt:
 1243;width:26pt'>
 <col class=xl8328673 width=232 style='mso-width-source:userset;mso-width-alt:
 8484;width:174pt'>
 <col class=xl8328673 width=176 style='mso-width-source:userset;mso-width-alt:
 6436;width:132pt'>
 <col class=xl8328673 width=91 span=7 style='mso-width-source:userset;
 mso-width-alt:3328;width:68pt'>
 <col class=xl8328673 width=96 style='mso-width-source:userset;mso-width-alt:
 3510;width:72pt'>
 <col class=xl8328673 width=91 span=12 style='mso-width-source:userset;
 mso-width-alt:3328;width:68pt'>
 <col class=xl8328673 width=130 style='mso-width-source:userset;mso-width-alt:
 4754;width:98pt'>
 <col class=xl8328673 width=91 span=2 style='mso-width-source:userset;
 mso-width-alt:3328;width:68pt'>
 <col class=xl8328673 width=105 style='mso-width-source:userset;mso-width-alt:
 3840;width:79pt'>
 <col class=xl8328673 width=64 style='width:48pt'>
 <tr height=4 style='mso-height-source:userset;height:3.0pt'>
  <td rowspan=2 height=7 class=xl6528673 width=13 style='height:5.25pt;
  width:10pt'></td>
  <td colspan=27 class=xl7928673 width=2684 style='width:2009pt'></td>
  <td rowspan=2 class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=3 style='mso-height-source:userset;height:2.25pt'>
  <td colspan=27 rowspan=3 height=26 width=2684 style='height:19.5pt;
  width:2009pt' align=left valign=top><!--[if gte vml 1]><v:shapetype id="_x0000_t75"
   coordsize="21600,21600" o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe"
   filled="f" stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="Picture_x0020_1" o:spid="_x0000_s1027" type="#_x0000_t75"
   alt="/trocon-payroll/public/assets/img/logoreport.png" style='position:absolute;
   margin-left:0;margin-top:.75pt;width:39.75pt;height:33pt;z-index:1;
   visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQBamK3CDAEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhvROFGcle3tSdZNgokh7Rjb3+8vyWK5tSObIJBxWPPbsuIM
UDltsK/5x/qleOCMokQtR4dQ8x0QXzbXV4v1zgOxRCPVfIjRPwpBagArqXQeMHU6F6yM6Rh64aX6
lD2Iu6q6F8phBIxFzBm8WbTQyc0Y2fM2lWcTjz1nT/NcXlVzYzOf6+JPIsBIJ4j0fjRKxnQ3MaE+
8SoOTmUi9zM0GE83SfzMhtz57fRzwYF7S48ZjAa2kiG+SpvMhQ4kvFFxEyBNlf/nZFFLhes6o6Bs
A61m8ih2boF2XxhgujS9Tdg7TMd0sf/X5hsAAP//AwBQSwMEFAAGAAgAAAAhAAjDGKTUAAAAkwEA
AAsAAABfcmVscy8ucmVsc6SQwWrDMAyG74O+g9F9cdrDGKNOb4NeSwu7GltJzGLLSG7avv1M2WAZ
ve2oX+j7xL/dXeOkZmQJlAysmxYUJkc+pMHA6fj+/ApKik3eTpTQwA0Fdt3qaXvAyZZ6JGPIoiol
iYGxlPymtbgRo5WGMqa66YmjLXXkQWfrPu2AetO2L5p/M6BbMNXeG+C934A63nI1/2HH4JiE+tI4
ipr6PrhHVO3pkg44V4rlAYsBz3IPGeemPgf6sXf9T28OrpwZP6phof7Oq/nHrhdVdl8AAAD//wMA
UEsDBBQABgAIAAAAIQAb7HcfBgMAALAHAAASAAAAZHJzL3BpY3R1cmV4bWwueG1srFXbjpswEH2v
1H9AficYSi6gJasUkqpSL6uq/QDHmGDV2Mh2Lquq/94xl1y0D6023Zc1Y/ucM3NmnIfHUyO8A9OG
K5mhcIKRxyRVJZe7DP34vvEXyDOWyJIIJVmGnplBj8u3bx5OpU6JpLXSHkBIk0IgQ7W1bRoEhtas
IWaiWiZht1K6IRY+9S4oNTkCeCOCCONZYFrNSGlqxmzR76Blh22PKmdCrHoKVnK7MhkCDS46nKm0
avrTVIll+BA4UW7ZIcDia1Ut8TnsvrodrY7jabccY24/mUbT/gLsdBc60AuTVWf0ZXSGPsfclXCB
k/mAMogYKZbxGfyG9vrKDfFI13LaX5CHJ06f9ED45fCkPV5mKEKeJA0YBLt2r5kXIq9khoIngdWK
Kum35FkrIYJ2vxWcBsQYZk3Am10g1E5p1iptJ63coeAC3lORFOg/KfrTDF6TVzjdEC5BpMprInds
ZVpGLXTcVUhD5rXrBhcGEb2dkGKvovu8SR/yaDdcgN8kdeu71fWd/E99rKqKU1Youm+YtH0zayaI
hUEyNW8N8nTKmi0Dc/THMoSWJSk72U/GDitvr3mGfkWLFcZJ9N7Ppzj3Yzxf+6sknvtzvJ7HOF6E
eZj/drfDON0bBjYQUbR8zDWMX3jRcKqVUZWdUNUEvdBx7kBoiIPeiwMRGcJdpTtpUPGLRFi6kjqt
RtNvYNbI+ILv71Pe8YGjgGU1s7S+F8tBVeC80+U65Qw8dM2lM9yTYFqYku3xsyphQMjeqs6MU6Wb
/6EDCuydMjTDyQzD8/mcoXg+i6aurl05PQq7U3ASYh5122ESwslet1PhDrba2A9M3a3Ic0DQclCY
LktygI7rqUYKRyeVG5x70x9TBIp7ocZi9UOR4GS9WC9iP45maxiKovBXmzz2Z5twPi3eFXlehONQ
1LwsmbxO5/Uz4VQYJXg5PitG77a50F43K5vubzDu6ljgZvMiY5yj8X838t3r5Rpx6FB4UYdnVnB4
QApiiXPJtevNT98Q639ql38AAAD//wMAUEsDBBQABgAIAAAAIQCqJg6+vAAAACEBAAAdAAAAZHJz
L19yZWxzL3BpY3R1cmV4bWwueG1sLnJlbHOEj0FqwzAQRfeF3EHMPpadRSjFsjeh4G1IDjBIY1nE
GglJLfXtI8gmgUCX8z//PaYf//wqfillF1hB17QgiHUwjq2C6+V7/wkiF2SDa2BSsFGGcdh99Gda
sdRRXlzMolI4K1hKiV9SZr2Qx9yESFybOSSPpZ7Jyoj6hpbkoW2PMj0zYHhhiskoSJPpQFy2WM3/
s8M8O02noH88cXmjkM5XdwVislQUeDIOH2HXRLYgh16+PDbcAQAA//8DAFBLAwQUAAYACAAAACEA
SbxakAsBAACCAQAADwAAAGRycy9kb3ducmV2LnhtbFSQ0UrDMBSG7wXfIRzBO5e0WLfWpaMIghcy
2OYDhCZtik1SknStPr3pVqlehf+cfP/5z9nuRtWis7CuMZpCtCKAhC4Nb3RN4eP0+rAB5DzTnLVG
CwpfwsEuv73ZsoybQR/E+ehrFEy0yxgF6X2XYexKKRRzK9MJHXqVsYr5IG2NuWVDMFctjgl5woo1
OkyQrBMvUpSfx15ROBUy4T1Z8zaJ+s6l+xi/K0Xp/d1YPAPyYvTL55l+4xRimFYJa0Ae8o1toUtp
LKoOwjXfIfy1XlmjkDXDpFFp2ssb9L6qnPAU0iROLo3fAgE8+XlzpR5nKkz7Q0Ubkq7/c3MpwHhJ
cxHL6fIfAAAA//8DAFBLAwQKAAAAAAAAACEAe/C4HSERAAAhEQAAFAAAAGRycy9tZWRpYS9pbWFn
ZTEucG5niVBORw0KGgoAAAANSUhEUgAAADUAAAAsCAYAAADFP/AjAAAAAXNSR0IArs4c6QAAAAlw
SFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUATWljcm9zb2Z0IE9mZmljZX/tNXEAABCh
SURBVGhD3Vp7eFTVtV/7vOeVzBACJLwjpSJQi5DwViQmIUSo+LpUXmIt4mcBFS1f79Uo0d5KqfVJ
7aVqkUd9gohEBURENEggbSNIeA4QEvKYJDOTmTPntc8+d59JglwkyUztX3d/38nM7LPP3uu311q/
tfY64VauXAn/3xr37wS0PydHqDzgcQNILhWi7v4S1+usbPCtMWx1rDPQIyKXxEQjGJqiWImOlkZH
Jmsvyf9OOX4QqG1iUf+zino1wwo/RpY5CMrNQSbHpVimLiGT5c9FMTAAZk+XBMSW2rKgBZsQilos
y3IWw3rUSv1Y7O/W1FqLEU84BXKqHweHp8s7/D8EZNKgtnmL0hST5IVixq2yhn9MGOSQLKNJ4Jmj
LMN9HdXhlEVI0JeSElWikWi6BDjTAybGHAKOY48HIhaWWAljt1szlFQXwn0FQRyBCS6MKqT/tyBo
p5mbjrod7OY0wu2aqZWGkgWYMKg32NwhQYO5iw9phcAgUSfoiBuRV9PS2EO9IpGTk7WypouLU/WA
bVCIXhpA8YrZAsBQo6SkxJrc3hcfa4+zmwGwr0+Rxx/Ag2VCxunAToyo1goZaQ//mS/4aEAqbJre
lLj2EgL1pqvw1lArfoYKKbOs9T+ZkvXJTHn3WbA95TsonW7okKfqnmCZC4E5AM93NmhyfWlkMsA3
9L59rd3sKsgK6uhnKsDCEwEyv9mZ97t5yq7XE9Fat6A28AX5LTJ+wSmh9we6Ulblh96rjWshwbbN
un5sXdS8n2VR8LOMG0qnhvaeTOTR29r86rlt3tvfrSaR+5oV8/ENYl7TPG3Xtu6e7xLUNnGJoyV2
9EGRQwfO/6bmoZKSKrO7CS+932dCkbN6b+QRBYk+hhi+Ey0wbyoDxcnMMTP0Xg0d//gauLF3s8w8
sq1n0VczQ6XNXc3RJaggOTaNZ2BwVqo1P1lA9qKrD0QKwyouskwMJstCq4EWfta3YPvUph3lyQCz
x/Z3CWtPt+J3q8OxGfTnun8J1H5vDhdpMe8ywfpnfmjPwWSFECfM9YR2n1lsIN4BFvUMYoHKePod
a9IfduTMnj++/C09mTkPLR9bkfH0l1+oJnPzprGz/zani+c71dQJxTFcJfinbhY9lczi9thhw4qZ
R7/YuzRisDfSmNXGgsgCU9cgaKIZ/srm6eMBtiYzr82cXkfe1gZZf7j660AaZc66zp7vFJSGhVyO
wXqm2yxLhhjshfxZQTFcgadprIMFTLXEUO5GFBkxwOBczoCCF8cmzd3tLNsYSQaYTsgBjuUuZAnc
IBoqkgO1yZsj6iH9ZoGFb7IGBM5AVTJL09BU+pLSx3XjeiWqZWscL4L1Hb9YFGSUlXI3VzT8ch7A
H5OZ2V9xoaHvTzOaYoBH0+f2J6UpH/gyA4AHC4A/GlGVHONttnIKYgoeeXJR35d7vHZ2qo7E2RZW
aIpkmyC9CAbDBK4+YizemVW0Lb+m9FSiwFaOqDL/bPaqU01ueDE18ZKqknj2dXm7ovlFNOY6CyFR
YIRvIQkS3ycWec7V1qwwCBo/bv3RXemCb7Uc06bprOgFk6YW8UaRGTFQOOFH/lp1Ie34r0RB2eMY
znlaJWjMjLrtadkAgYRBtSjGSAsxqo8z/cmAOiU3F8mEnWAykngqoj9Z/kDGz8evr3mt2eCWm9Sf
2tRlf1DSoF9bVfyLjyjF0xSoU1O6XOgwYWoA47TKFnd6NpMEKNmC/gyyWrAabIybTALtiDe3V7hW
XoZ5SQRDg7DJ3DJ1nf/Ogn6Zq7dUN+bLnDgSDGqGNmHYDWsQE929TzTqv/ZMKpozuaw0lsAyoCrR
mEAsn8pAD6AZZUKaKi4exj73FJvGmHrDgKA/BmmJLAVQHsb3R0EYF49JlOww4iEUsx7M9AW3uxvQ
87pCXjMQS7V0CWnolDQMs+DsAaaQ5n2bE1nJwxm6QQQErJja2fjv+dSda8GBgXN6BOQfnxbqcIQu
19vpLRgRqJcXGhYV2laETeGmQbNfadT6w9qS+3MGrXn5UO3nzeCYYmk2i9uDqP1hHTAn0VRMW3rq
6qLPh5zpOv2xhTAAx+g5jO6fy2ln9wlpSlF9XoQst2qic4nsXHFxMfOnlfvuU4EfGCcDZMckm5Qs
wBYBeoRYtut43XtX9XI+odTK22SGTwVCA7LtXtS3LEOFVou7/vPT4XuHMLCquzVHeySrSmVIk6Kw
0EmU/V53ZQvPmxzDeSUUBeoC3bWRL1fcFMZoPiEUCM0a4j5jtQNjOBBZ67jKQ3BmU+m3Z9jr31dY
7m5CNRRHRUHR4yMFj6A5ZPxysy9n622o/HhXa56PRGXNkuS+LjHNPqslpKnz/gg4h7igV6qb6Q7U
vglFngtfxpZpwKeAGW0H1B6QOAEkjrQO6uF8emZoxwV78cGp/CtqQJ/eKqT2imuVajKuVZMAzT6u
amiNLY+NK1rmrCrtdDv9dYrGpVtqhoOVEgbl7gUsywAPhGag3TR/RWxGFFtTLerwF1uc3OgfCsrN
4Q8PPTr245KSHfHb0+Xd5QFP3qO4JXaLZVH6sysXNBjbHyaO8SAQzu8P9hgBUNvp0orKcgxlHMMO
CglqKqpw2OmxaMWEdEnm+/rc3j94IbxctywJrMvYmBXAw5jBgV7+TzQRJfu9Xs6TkWG9c8cdhB6m
NuStWvs++HxtQgW/E+yCovI+CHRp9BPHZLlOtnKehigOdhZuvudT6ekckqlvNIaVLnOJ4w3N90RM
/jrQL5eBsi2LIM3NPr9iVsY3i1+b8Ef5gjCEqbUMZ8XeuG4QM9SESHuBIs4pdq99OkFMtQoo0zXk
lTnWl59dSQ+BCOYMEFgDGGrvCWpqWqYPlzZYZlDWnBcLI5c9u7NnwfBgrTafVoDoncvSL1YECZFv
8of0ffGh109Nj+j8Q5hqDhDdI5tE4scQm0zaP+PkYi9A57FZno5tUeU+B3sXfZsdKm24XOyAYYgW
Mh0ugaFJZIKgVPAFTWhtTRHNIcW/KUb2OebSR73e2by/pXGJwnBZQOn4u2YPoyxLFZAiMWtGZ4H5
9kF4yLAxG9TGOo4fcZdrt+w4uO9SJxuVpSMIc+Kkw02RBdkc/P77YvMSNjUH0bVowuY3on5jdBdM
i2JsDly1dq1IJ71UcnhFacgPKtY8kxYlgVxiofaG8wKkIrznwuOZbwx47MSjMcKPs9OhNhDtV4em
OqTtAHhx6wgYlKOaNHL3ntTcLTfKu/9PFh+kRUMXi2M9OdzaWV56xfDFgNGoAZuzNpjlXgD1F0Ed
GTbX1fDPc/ermHPGD38XBaFfaEwSLC2aliI9MePF6t5NrcYiww7Edlpkp0cduDpzhIvg6EBK9yoS
h/kj5gN7nixebpNNx2MpqZ4UoseaJTA6Lb5cEZSIyIkYcNMlztOfxoJ4Zc/OHK767b57ZJMviu88
J7XvfttyDD0Lui1524ez0ssnvnLqBY2lz9pbafuTbXo0TsQ/LzW/ywHapmhHEkocJuIgoGpLfrTy
033zEGzpGMoTuR8B1DI8PRCA+gR9yh7mE5iDika4CGGG0Z//sPvmv1jhC0fV0brOltM0R2/bfYqO
sj8gjuFNXRmULq1e9q4/Q9GMgcSKldGwQICnyWccDB1v0kiLbJ0xl4QLm/rsvnZV2T/j4AykmiC2
YDKqecDsD9NCb8VpgVNigxiE6t5ZdGdrScmV39hcOXtywBFGNYOGjuxj89/syTacXt8KdZN+Be9+
SZ3kRQI3PcNDFr1R/TnNc6Zw/zlZU+0I/0Szl4fd8DPfFEkMnlU1sEuSU67hYO0rmm/FHYIvGLT8
n1NxZ2VwsOpdHRYtEqHadtATJlw7kYffr6I51B0W3AGiVVlntky5hpm0dKhZVULnphWu/jLbR2Ss
IxRQ4sHXBnB06cRQv//ev0fV8cSDmRN6ZteXNVmFL1hjqg5OOj40bQght5q9RKZmxDV9v/6ictwU
ajK+1x2uWD+R2bcSSs+9fU9eYRjj7IyrpeYZI/u96Sz7S8vmPywbFVbhZiZtkDbxWvGD/Katxzev
eXJypDWYRwZA2OWZ+M5/hErPr35yySyPJNQu1naUP3VN7kTqumeqSkriaVaWwvWM6ODzuIXKzlKk
uDavZJUrKY1v4PPei1jWzYebxXH02LwdpgG4azjkjHC3aCZKT3Fzj9cFYqNEnlsqsfgwxnhcLSa9
t3vzToajaLFqkM2MTia8VVHdx+XK3RKIWSWIkArMYKFBJj9521XgVpXYo9TSPlMI0zcWUp96lb2h
BBloiYlJv+09ChdxdbGFWDe2gBM+sOWUONe1LZaaeTSgnqN9nbZOS2T3Zvc++OxXdcejOrmdWvx2
ykB4KsDHJ9ANI4luDL9N3rNtKxTmYdMwoyYju+krKFp9Ol8fxmM1AueWMntfexXnXYhgcyGlxckm
Ruxgp+tZ+mom8rbr9t4ROVwosUzTAmP386+y12fFMPMXBpjBpqaWMZzkbYqxj/CIpKmmTbNtLaiQ
AgS87POltCStKXsCrWyj7BPy1od0KN4sFl13m1b6d7vfwXAMEZGdSoDT40Ae3ahsrI+95HEwqYSg
gZlO5yd1hvHA62bugxxrDe8hsftTebITWez4Ok1bvk7MZx2mdswhCF/Lmj7+r3zur1mEMp0sqqSs
WyOJLEEm/gOyxPtEnr3f8HDxxHKft2ig0qzkI8t640Gt66N/l7X0WflXb1m3/ehdjZpa0mfC3Nvr
yzaq13ikN1WJlpIpnWZmpR3yORh/tvFp9ZF+t/xOUQwxu7702GZXblNAg0G03lwxqIdYQV/TxHZ6
i1b4g/owC5tEVcnhBai0fpPrhlVY5YaZxDo0INX7j9HARSs9wY0OPLYGS/5nIjj4MY7o39haqZL1
n9MKF9tDMD/orhjUJSgnLUqmunNXBSJkwxNfNz49eVhRSXZV6cUT8QhKAHQT7QtGnNl6psNMbpN3
V1GTrYov3h5L8kOl1bSvOt7XTuhz5L02N7a9Cg21PU1N/ARA/KgSptcX9pe/irl5qmHemyKyaxYY
e+n9rlu376fmyLu/eN2R91A4Zr2w7xQedbxn0eopWZ49aeVvJVS/6E6Azu7TYI+ufbbi6mbdnKto
5i8cAvPxXWPS10JZ9zN2C8qe4h5t15ZNKQXnmxXrgbpm47k3gy1nBLZgZxoHnxVC9UmnVpXAwb97
YWrpK9Ivg/iqCMET1vz2wCSaZf1EM6HBxcJjc8cMfdtZlthb/IRA2eLMkXfYr3Pu3sQWZLcibKdL
98qK8at1fN9jEt/vKxUzh2ni0OABtRnjSKtPkrSIY4A2DT6xrpo2DU5/AvDJx340YBYnXFAdPC0V
eGhdKZX6VA9g9L48x16LA/oY2bD6W6yg06S1qo9HeCwsBz9doJVFoWxX97vSPiJhUB0zzjHj4A7u
7FM0oKbZyJEJPzJm4lGIJYUGJo4AYQyT7dXapDGqobZo64QJpmNLBBRCU9vxPra+HtGkEQmspaXQ
uCXQE6XGM6ymYFyrgLjDAfhYlkAOT9d2nWnz1uRb0qA6lsivp45PkyR6vefNyeFeqPT08Lm5jDBl
PYVYfVTd9Hl4SNexzimmSThgkddJoWhmkKa4QZZnG52MWRNUzfOZQzNCM6s2tsbnttPEH+it/zKo
S/cvVF6OFwA0UmEaaX/lxXsdJ9OOfy249GTWcc9mwiRfFXWnu/8F36feYWnOvN4AAAAASUVORK5C
YIJQSwECLQAUAAYACAAAACEAWpitwgwBAAAYAgAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRlbnRf
VHlwZXNdLnhtbFBLAQItABQABgAIAAAAIQAIwxik1AAAAJMBAAALAAAAAAAAAAAAAAAAAD0BAABf
cmVscy8ucmVsc1BLAQItABQABgAIAAAAIQAb7HcfBgMAALAHAAASAAAAAAAAAAAAAAAAADoCAABk
cnMvcGljdHVyZXhtbC54bWxQSwECLQAUAAYACAAAACEAqiYOvrwAAAAhAQAAHQAAAAAAAAAAAAAA
AABwBQAAZHJzL19yZWxzL3BpY3R1cmV4bWwueG1sLnJlbHNQSwECLQAUAAYACAAAACEASbxakAsB
AACCAQAADwAAAAAAAAAAAAAAAABnBgAAZHJzL2Rvd25yZXYueG1sUEsBAi0ACgAAAAAAAAAhAHvw
uB0hEQAAIREAABQAAAAAAAAAAAAAAAAAnwcAAGRycy9tZWRpYS9pbWFnZTEucG5nUEsFBgAAAAAG
AAYAhAEAAPIYAAAAAA==
">
   <v:imagedata src="Report%20payroll_files/Report%20Payroll_28673_image001.png"
    o:title=""/>
   <x:ClientData ObjectType="Pict">
    <x:SizeWithCells/>
    <x:CF>Bitmap</x:CF>
    <x:AutoPict/>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:0px;margin-top:1px;width:53px;
  height:44px'><img width=53 height=44
  src="{{Config::get('constants.path.img')}}/logoreport.png"
  alt="/trocon-payroll/public/assets/img/logoreport.png" v:shapes="Picture_x0020_1"></span><![endif]><span
  style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td colspan=27 rowspan=3 height=26 class=xl6628673 width=2684
    style='height:19.5pt;width:2009pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PT.
    TROCON INDAH PERKASA</td>
   </tr>
  </table>
  </span></td>
 </tr>
 <tr height=3 style='mso-height-source:userset;height:2.25pt'>
  <td height=3 class=xl6528673 width=13 style='height:2.25pt;width:10pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt;break-inside: avoid;break-after: auto'>
  <td height=20 class=xl6428673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=23 style='height:17.25pt;break-inside: avoid;break-after: auto'>
  <td height=23 class=xl6728673 width=13 style='height:17.25pt;width:10pt'></td>
  <td colspan=27 class=xl8028673 width=2684 style='width:2009pt'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LAPORAN
  GAJI</td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=22 style='height:16.5pt;break-inside: avoid;break-after: auto'>
  <td height=22 class=xl6328673 width=13 style='height:16.5pt;width:10pt'></td>
  <td colspan=27 class=xl8128673 width=2684 style='width:2009pt'>&nbsp;</td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=22 style='height:16.5pt;break-inside: avoid;break-after: auto'>
  <td height=22 class=xl6328673 width=13 style='height:16.5pt;width:10pt'></td>
  <td colspan=2 class=xl6928673 width=266 style='width:200pt'>PERIODE</td>
  <td colspan=6 class=xl6928673 width=631 style='width:472pt'>: {{ $datafilter->period }}
  </td>
  <td class=xl6928673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=96 style='width:72pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=130 style='width:98pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=91 style='width:68pt'></td>
  <td class=xl6828673 width=105 style='width:79pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt;break-inside: avoid;break-after: auto'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td colspan=2 class=xl8228673 width=266 style='width:200pt'>DEPARTEMENT</td>
  <td colspan=8 class=xl8228673 width=813 style='width:608pt'>: {{ $datafilter->departmentname }}</td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=130 style='width:98pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=105 style='width:79pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=34 style='height:25.5pt;break-inside: avoid;break-after: auto'>
  <td height=34 class=xl6328673 width=13 style='height:25.5pt;width:10pt'></td>
  <td class=xl7028673 width=34 style='width:26pt;border-image: initial'>No</td>
  <td class=xl7128673 width=232 style='width:174pt;border-image: initial'>Employee</td>
  <td class=xl7128673 width=176 style='width:132pt;border-image: initial'>Position</td>
  <td class=xl7128673 width=91 style='width:68pt;border-image: initial'>Basic</td>
  <td class=xl7128673 width=91 style='width:68pt;border-image: initial'>UM
  Kantor Tetap</td>
  <td class=xl7128673 width=91 style='width:68pt;border-image: initial'>Lembur
  Tetap</td>
  <td class=xl7128673 width=91 style='width:68pt;border-image: initial'>Tunj
  Keahlian</td>
  <td class=xl7128673 width=91 style='width:68pt;border-image: initial'>Tun
  Jamsostek</td>
  <td class=xl7128673 width=91 style='width:68pt'>Klaim</td>
  <td class=xl7128673 width=91 style='width:68pt;border-image: initial'>Tunj
  Kesehatan</td>
  <td class=xl7228673 width=96 style='width:72pt;border-image: initial'>U
  Makan,Trnsprt</td>
  <td class=xl7228673 width=91 style='width:68pt;border-image: initial'>Lembur
  Proyek</td>
  <td class=xl7228673 width=91 style='width:68pt;border-image: initial'>TM,TDK,TLK,
  Inst</td>
  <td class=xl7228673 width=91 style='width:68pt'>Pendapatan Lain</td>
  <td class=xl7228673 width=91 style='width:68pt;border-image: initial'>Gaji
  Bruto</td>
  <td class=xl7228673 width=91 style='width:68pt;border-image: initial'>Biaya
  Jabatan</td>
  <td class=xl7228673 width=91 style='width:68pt;border-image: initial'>THT</td>
  <td class=xl7228673 width=91 style='width:68pt;border-image: initial'>Netto</td>
  <td class=xl7228673 width=91 style='width:68pt;border-image: initial'>PTKP</td>
  <td class=xl7228673 width=91 style='width:68pt;border-image: initial'>PKP</td>
  <td class=xl7228673 width=91 style='width:68pt;border-image: initial'>PPh21</td>
  <td class=xl7228673 width=91 style='width:68pt;border-image: initial'>Pot
  Jamsostek</td>
  <td class=xl7228673 width=91 style='width:68pt;border-image: initial'>Pot
  Pensiun</td>
  <td class=xl7228673 width=130 style='width:98pt;border-image: initial'>Gaji
  Bersih</td>
  <td class=xl7228673 width=91 style='width:68pt;border-image: initial'>MH
  Dibayar</td>
  <td class=xl7228673 width=91 style='width:68pt'>Pinjaman</td>
  <td class=xl7228673 width=105 style='width:79pt;border-image: initial'>Total
  Netto</td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 @php
   $t_basic = 0;
   $t_mealtransall = 0;
   $t_overtimeall = 0;
   $t_tunj_keahlian = 0;
   $t_tunj_jamsostek = 0;
   $t_tunj_kesehatan = 0;
   $t_medical_claim = 0;
   $t_uang_makan = 0;
   $t_lembur_proyek = 0;
   $t_tm_tdk_tlk_instpd = 0;
   $t_gaji_bruto = 0;
   $t_biaya_jabatan = 0;
   $t_tht = 0;
   $t_netto = 0;
   $t_ptkp = 0;
   $t_pkp = 0;
   $t_pph21 = 0;
   $t_pot_jamsostek = 0;
   $t_pot_pensiun = 0;
   $t_gaji_bersih = 0;
   $t_mh_dibayar = 0;
   $t_totalnetto2 = 0;
   $t_otherall = 0; 
   $t_loan = 0; 

   $n = 1;
   @endphp
 @foreach($itemdata as $itemdatas)
 <tr height=20 style='height:15.0pt;break-inside: avoid;break-after: auto'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl7328673 width=34 style='width:26pt;border-image: initial'>@php echo $n++; @endphp</td>
  <td class=xl7428673 width=232 style='width:174pt;border-image: initial'>&nbsp;{{ $itemdatas->employee }}</td>
  <td class=xl7428673 width=176 style='width:132pt;border-image: initial'>&nbsp;{{ $itemdatas->position }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->basic,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->mealtransall,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->overtimeall,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->tunj_keahlian,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->tunj_jamsostek,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt'>{{ number_format($itemdatas->medicalclaim,0) }}&nbsp;</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->tunjangankesehatan,0) }}
  &nbsp;</td>
  <td class=xl7528673 width=96 style='width:72pt;border-image: initial'>{{ number_format($itemdatas->uang_makan,0) }}
  &nbsp;</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->lembur_proyek,0) }}
  &nbsp;</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->tm_tdk_tlk_instpd,0) }}
  &nbsp;</td>
  <td class=xl7528673 width=91 style='width:68pt'>{{ number_format($itemdatas->otherall,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->gaji_bruto,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->biaya_jabatan,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->tht,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->netto,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->ptkp,0) }}</td>
  <td class=xl7628673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->pkp,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->pph21,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->pot_jamsostek,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->pot_pensiun,0) }}</td>
  <td class=xl7528673 width=130 style='width:98pt;border-image: initial'>{{ number_format($itemdatas->gaji_bersih,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt;border-image: initial'>{{ number_format($itemdatas->mh_dibayar,0) }}</td>
  <td class=xl7528673 width=91 style='width:68pt'>{{ number_format($itemdatas->loan,0) }}</td>
  <td class=xl7528673 width=105 style='width:79pt;border-image: initial'>{{ number_format($itemdatas->totalnetto2,0) }}</td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 @php
   $t_basic += $itemdatas->basic;
   $t_mealtransall += $itemdatas->mealtransall;
   $t_overtimeall += $itemdatas->overtimeall;
   $t_tunj_keahlian += $itemdatas->tunj_keahlian;
   $t_tunj_jamsostek += $itemdatas->tunj_jamsostek;
   $t_tunj_kesehatan += $itemdatas->tunjangankesehatan;
   $t_medical_claim += $itemdatas->medicalclaim;
   $t_uang_makan += $itemdatas->uang_makan;
   $t_lembur_proyek += $itemdatas->lembur_proyek;
   $t_tm_tdk_tlk_instpd += $itemdatas->tm_tdk_tlk_instpd;
   $t_gaji_bruto += $itemdatas->gaji_bruto;
   $t_biaya_jabatan += $itemdatas->biaya_jabatan;
   $t_tht += $itemdatas->tht;
   $t_netto += $itemdatas->netto;
   $t_ptkp += $itemdatas->ptkp;
   $t_pkp += $itemdatas->pkp;
   $t_pph21 += $itemdatas->pph21;
   $t_pot_jamsostek += $itemdatas->pot_jamsostek;
   $t_pot_pensiun += $itemdatas->pot_pensiun;
   $t_gaji_bersih += $itemdatas->gaji_bersih;
   $t_mh_dibayar += $itemdatas->mh_dibayar;
   $t_totalnetto2 += $itemdatas->totalnetto2;
   $t_loan += $itemdatas->loan;
   $t_otherall += $itemdatas->otherall; 
 @endphp

 @endforeach
 <tr height=20 style='height:15.0pt;break-inside: avoid;break-after: auto'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl7328673 width=34 style='width:26pt;border-image: initial'>&nbsp;</td>
  <td class=xl7428673 width=232 style='width:174pt;border-image: initial'>&nbsp;</td>
  <td class=xl7728673 width=176 style='width:132pt;border-image: initial'>Total&nbsp;</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_basic,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_mealtransall,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_overtimeall,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_tunj_keahlian,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_tunj_jamsostek,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt'>@php echo number_format($t_medical_claim,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_tunj_kesehatan,0); @endphp 
  &nbsp;</td>
  <td class=xl7828673 width=96 style='width:72pt;border-image: initial'>@php echo number_format($t_uang_makan,0); @endphp
  &nbsp;</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_lembur_proyek,0); @endphp
  &nbsp;</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_tm_tdk_tlk_instpd,0); @endphp
  &nbsp;</td>
  <td class=xl7828673 width=91 style='width:68pt'>@php echo number_format($t_otherall,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_gaji_bruto,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_biaya_jabatan,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_tht,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_netto,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_ptkp,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_pkp,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_pph21,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_pot_jamsostek,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_pot_pensiun,0); @endphp</td>
  <td class=xl7828673 width=130 style='width:98pt;border-image: initial'>@php echo number_format($t_gaji_bersih,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt;border-image: initial'>@php echo number_format($t_mh_dibayar,0); @endphp</td>
  <td class=xl7828673 width=91 style='width:68pt'>@php echo number_format($t_loan,0); @endphp</td>
  <td class=xl7828673 width=105 style='width:79pt;border-image: initial'>@php echo number_format($t_totalnetto2,0); @endphp</td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl6428673 width=34 style='width:26pt'></td>
  <td class=xl6328673 width=232 style='width:174pt'></td>
  <td class=xl6328673 width=176 style='width:132pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=130 style='width:98pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=105 style='width:79pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl6428673 width=34 style='width:26pt'></td>
  <td class=xl6328673 width=232 style='width:174pt'></td>
  <td class=xl6328673 width=176 style='width:132pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=130 style='width:98pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=105 style='width:79pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl6428673 width=34 style='width:26pt'></td>
  <td class=xl6328673 width=232 style='width:174pt'></td>
  <td class=xl6328673 width=176 style='width:132pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=130 style='width:98pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=105 style='width:79pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td colspan=2 class=xl6428673 width=266 style='width:200pt'></td>
  <td class=xl6328673 width=176 style='width:132pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=130 style='width:98pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=105 style='width:79pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl6428673 width=34 style='width:26pt'></td>
  <td class=xl6328673 width=232 style='width:174pt'></td>
  <td class=xl6328673 width=176 style='width:132pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=130 style='width:98pt'></td>
  <td colspan=3 class=xl8428673 width=287 style='width:215pt'>JAKARTA, @php echo date("d M Y"); @endphp</td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl6428673 width=34 style='width:26pt'></td>
  <td class=xl6328673 width=232 style='width:174pt'></td>
  <td class=xl6328673 width=176 style='width:132pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6428673 width=130 style='width:98pt'>DISETUJUI</td>
  <td colspan=2 class=xl8528673 width=182 style='width:136pt'>MENGETAHUI</td>
  <td class=xl8528673 width=105 style='width:79pt'>DIBUAT OLEH</td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl6428673 width=34 style='width:26pt'></td>
  <td class=xl6328673 width=232 style='width:174pt'></td>
  <td class=xl6328673 width=176 style='width:132pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=130 style='width:98pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=105 style='width:79pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl6428673 width=34 style='width:26pt'></td>
  <td class=xl6328673 width=232 style='width:174pt'></td>
  <td class=xl6328673 width=176 style='width:132pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=130 style='width:98pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=105 style='width:79pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl6428673 width=34 style='width:26pt'></td>
  <td class=xl6328673 width=232 style='width:174pt'></td>
  <td class=xl6328673 width=176 style='width:132pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=130 style='width:98pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=105 style='width:79pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl6428673 width=34 style='width:26pt'></td>
  <td class=xl6328673 width=232 style='width:174pt'></td>
  <td class=xl6328673 width=176 style='width:132pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=130 style='width:98pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=105 style='width:79pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl6328673 width=13 style='height:15.0pt;width:10pt'></td>
  <td class=xl6428673 width=34 style='width:26pt'></td>
  <td class=xl6328673 width=232 style='width:174pt'></td>
  <td class=xl6328673 width=176 style='width:132pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=130 style='width:98pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=105 style='width:79pt'></td>
  <td class=xl6328673 width=64 style='width:48pt'></td>
 </tr>
 <tr height=40 style='height:30.0pt'>
  <td height=40 class=xl6328673 width=13 style='height:30.0pt;width:10pt'></td>
  <td class=xl6428673 width=34 style='width:26pt'></td>
  <td class=xl6328673 width=232 style='width:174pt'></td>
  <td class=xl6328673 width=176 style='width:132pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=96 style='width:72pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=91 style='width:68pt'></td>
  <td class=xl6328673 width=130 style='width:98pt'>(________________)</td>
  <td colspan=2 class=xl8528673 width=182 style='width:136pt'>(_________________)</td>
  <td colspan=2 class=xl6328673 width=169 style='width:127pt'>(____________)</td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=13 style='width:10pt'></td>
  <td width=34 style='width:26pt'></td>
  <td width=232 style='width:174pt'></td>
  <td width=176 style='width:132pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=96 style='width:72pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=130 style='width:98pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=91 style='width:68pt'></td>
  <td width=105 style='width:79pt'></td>
  <td width=64 style='width:48pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>

