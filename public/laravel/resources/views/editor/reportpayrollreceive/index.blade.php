@extends('layouts.editor.template')
@section('title', 'Tanda Terima Gaji')
@section('content')
<!-- Content Wrapper. Contains page content -->
<style type="text/css">
.my_class {
  background-color: white;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    Tanda Terima Gaji
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Tanda Terima Gaji</li>
  </ol>
</section>

<section class="content">
  <div class="row"> 
    {!! Form::model($datafilter)!!}
    {{ csrf_field() }}
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <div class="col-sm-3" data-toggle="tooltip" data-placement="top" title="Department">
            {{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control', 'placeholder' => 'Select Department', 'id' => 'departmentid', 'onchange' => 'RefreshData();')) }} 
          </div>
          <div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Period">
            {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'RefreshData();')) }} 
          </div>  
          <!-- <button onClick="GenerateData()" type="button" class="btn btn-danger btn-flat"> <i class="fa fa-magic"></i> Generate!</button> -->
          <button onClick="showreport()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-print"></i> Cetak</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
          {!! Form::close() !!}
        </div>  
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>ID</th>
                  <th>Employee</th> 
                  <th>Department</th>
                  <th>Position</th>  
                  <th>Tanda Tangan</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  



@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px", 
         "order": [[ 0, "asc" ]],
         "autoWidth": true,  
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]], 
        ajax: "{{ url('editor/reportpayrollreceive/data') }}",
        columns: [   
        { data: 'employeeid', name: 'employeeid' }, 
        { data: 'employee', name: 'employee' }, 
        { data: 'departmentname', name: 'departmentname' }, 
        { data: 'position', name: 'position' },  
        { data: 'sign', name: 'sign' },  
        ]
      });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
        function reload_table()
        {
          // GenerateData();
          table.ajax.reload(null,false); //reload datatable ajax 
        }

        function GenerateData()
        {
   
          var periodid = $('#periodid').val();
          var perioddesc = $("#periodid option:selected").text();

          $.confirm({
            title: 'Confirm!',
            content: 'Are you sure to generate <b><u>' + perioddesc + '</u></b> data?',
            type: 'red',
            typeAnimated: true,
            buttons: {
              cancel: {
               action: function () { 
               }
             },
             confirm: {
              text: 'CREATE',
              btnClass: 'btn-red',
              action: function () { 
                waitingDialog.show('Proses...', {dialogSize: 'sm', progressType: 'primary'});
               $.ajax({
                url : 'payroll/generate/' + periodid,
                type: "POST",
                data: {
                  '_token': $('input[name=_token]').val() 
                },
                success: function(data)
                { 
                  var options = { 
                    "positionClass": "toast-bottom-right", 
                    "timeOut": 1000, 
                  };
                  waitingDialog.hide();
                  toastr.success('Successfully genareted data!', 'Success Alert', options);
                  reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                  $.alert({
                    type: 'red',
                    icon: 'fa fa-danger', // glyphicon glyphicon-heart
                    title: 'Warning',
                    content: 'Error generate data!',
                  });
                }
              });
             }
           },

         }
       });
      }
      function RefreshData()
           {  
            $.ajax({
              type: 'POST',
              url: "{{ URL::route('editor.periodfilteronly') }}",
              data: {
                '_token': $('input[name=_token]').val(),     
                'periodid': $('#periodid').val(),   
                'departmentid': $('#departmentid').val(),   
              }, 
              success: function(data) { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully filtering data!', 'Success Alert', options);
                reload_table();
              }
            }) 
          }; 
         $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();   
      });

      /**
       * Module for displaying "Waiting for..." dialog using Bootstrap
       *
       * @author Eugene Maslovich <ehpc@em42.ru>
       */

        var waitingDialog = waitingDialog || (function ($) {
            'use strict';

          // Creating modal dialog's DOM
          var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
              '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
              '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
              '</div>' +
            '</div></div></div>');

          return {
            /**
             * Opens our dialog
             * @param message Process...
             * @param options Custom options:
             *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
             *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
             */
            show: function (message, options) {
              // Assigning defaults
              if (typeof options === 'undefined') {
                options = {};
              }
              if (typeof message === 'undefined') {
                message = 'Loading';
              }
              var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
              }, options);

              // Configuring dialog
              $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
              $dialog.find('.progress-bar').attr('class', 'progress-bar');
              if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
              }
              $dialog.find('h3').text(message);
              // Adding callbacks
              if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                  settings.onHide.call($dialog);
                });
              }
              // Opening dialog
              $dialog.modal();
            },
            /**
             * Closes dialog
             */
            hide: function () {
              $dialog.modal('hide');
            }
          };

        })(jQuery);

        function showreport()
        {
         var url = '../editor/reportpayrollreceive/printreport';
           PopupCenter(url,'Popup_Window','1000','650');
        }

        function PopupCenter(url, title, w, h) {  
          // Fixes dual-screen position                         Most browsers      Firefox  
          var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
          var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
                
          width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
          height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
                
          var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
          var top = ((height / 2) - (h / 2)) + dualScreenTop;  
          var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
          
          // Puts focus on the newWindow  
          if (window.focus) {  
            newWindow.focus();  
          }  
        } 
    </script> 
    @stop
