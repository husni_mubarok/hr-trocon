@extends('layouts.editor.templatereport')
@section('content')
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
 
</style> 
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 15">
<link rel=File-List href="Tanda%20Terima%20Gaji_files/filelist.xml">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<style id="Report (Autosaved)_1018_Styles"><!--table
  {mso-displayed-decimal-separator:"\,";
  mso-displayed-thousand-separator:"\.";}
.xl151018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl631018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl641018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl651018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl661018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl671018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl681018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl691018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:16.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl701018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl711018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl721018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl731018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl741018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl751018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl761018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl771018
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border:.5pt solid windowtext;
  background:#E7E6E6;
  mso-pattern:black none;
  white-space:nowrap;}
--></style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="Report (Autosaved)_1018" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=2096 style='border-collapse:
 collapse;table-layout:fixed;width:1574pt'>
 <col width=13 style='mso-width-source:userset;mso-width-alt:475;width:10pt'>
 <col width=34 style='mso-width-source:userset;mso-width-alt:1243;width:26pt'>
 <col width=204 style='mso-width-source:userset;mso-width-alt:7460;width:153pt'>
 <col width=206 style='mso-width-source:userset;mso-width-alt:7533;width:155pt'>
 <col width=218 style='mso-width-source:userset;mso-width-alt:7972;width:164pt'>
 <col width=141 style='mso-width-source:userset;mso-width-alt:5156;width:106pt'>
 <col width=64 span=20 style='width:48pt'>
 <tr class=xl641018 height=7 style='mso-height-source:userset;height:5.25pt'>
  <td height=7 width=13 style='height:5.25pt;width:10pt' align=left valign=top><!--[if gte vml 1]><v:shapetype
   id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
   path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="Picture_x0020_1" o:spid="_x0000_s2050" type="#_x0000_t75"
   style='position:absolute;margin-left:6.75pt;margin-top:2.25pt;width:43.5pt;
   height:39pt;z-index:1;visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQBamK3CDAEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhvROFGcle3tSdZNgokh7Rjb3+8vyWK5tSObIJBxWPPbsuIM
UDltsK/5x/qleOCMokQtR4dQ8x0QXzbXV4v1zgOxRCPVfIjRPwpBagArqXQeMHU6F6yM6Rh64aX6
lD2Iu6q6F8phBIxFzBm8WbTQyc0Y2fM2lWcTjz1nT/NcXlVzYzOf6+JPIsBIJ4j0fjRKxnQ3MaE+
8SoOTmUi9zM0GE83SfzMhtz57fRzwYF7S48ZjAa2kiG+SpvMhQ4kvFFxEyBNlf/nZFFLhes6o6Bs
A61m8ih2boF2XxhgujS9Tdg7TMd0sf/X5hsAAP//AwBQSwMEFAAGAAgAAAAhAAjDGKTUAAAAkwEA
AAsAAABfcmVscy8ucmVsc6SQwWrDMAyG74O+g9F9cdrDGKNOb4NeSwu7GltJzGLLSG7avv1M2WAZ
ve2oX+j7xL/dXeOkZmQJlAysmxYUJkc+pMHA6fj+/ApKik3eTpTQwA0Fdt3qaXvAyZZ6JGPIoiol
iYGxlPymtbgRo5WGMqa66YmjLXXkQWfrPu2AetO2L5p/M6BbMNXeG+C934A63nI1/2HH4JiE+tI4
ipr6PrhHVO3pkg44V4rlAYsBz3IPGeemPgf6sXf9T28OrpwZP6phof7Oq/nHrhdVdl8AAAD//wMA
UEsDBBQABgAIAAAAIQAMmbembAIAAKYFAAASAAAAZHJzL3BpY3R1cmV4bWwueG1srFTBjtowEL1X
6j9YvkOckBSISFYUdqtKqy2q2g/wOg6xmtiRbVhWVf+9YzsBIQ6tur1NPJ55z/PeZHV36lp05NoI
JQscTwlGXDJVCbkv8PdvD5MFRsZSWdFWSV7gV27wXfn+3epU6ZxK1iiNoIU0ORwUuLG2z6PIsIZ3
1ExVzyVka6U7auFT76NK0xdo3rVRQsiHyPSa08o0nNttyODS97YvasPbdh0geCXs2hQYOLjT4U6t
VRduM9WWZBU5Ui70HSD4UtflIpsn2TnlTnxWq5exwoXjmcsnUDJUQMpX+M4XOKvOEGVy7n0+cyXx
kmTkQukKdyi5wY3nhCxCuyvgEa4XLGDI406wnR4An447jURV4AQjSTtQCbL2oDmKcXS5EypoDl0e
FfthBt3oP6jWUSEBS20aKvd8bXrOLLjHoQUNgFKA859XdJ9b0T+IFkSiuYvfTCPY76/Mp+paML5V
7NBxaYMDNW+pBfebRvQGI53z7pnDMPXnKsaIgfktTLTXQlqwHc35yT4aO0TooEWBfyaLNSHL5ONk
k5HNJCXz+8l6mc4nc3I/T0m6iDfx5perjtP8YDiMn7bbXoxPj9MbDTrBtDKqtlOmuijwHncHeMck
ChocaVtg4gfvqYEAF4oQugk7rsZqblkzIt7g/XlTPZ5rVYN4X0FwJ/a58SD8RVy3iqZ3HqX5qdbd
/0CGMaBTgf1GY/QKhneb6h7v34wYJLMsSTP4izFIp8tZMpsNw3Ek3MVeG/uJqzcTQq4R2AQm4X1B
j2CLMJMRYhhKGIPfBNi9YSFbAQ7cUkvHnbn64Q2V4Qdb/gYAAP//AwBQSwMEFAAGAAgAAAAhAKom
Dr68AAAAIQEAAB0AAABkcnMvX3JlbHMvcGljdHVyZXhtbC54bWwucmVsc4SPQWrDMBBF94XcQcw+
lp1FKMWyN6HgbUgOMEhjWcQaCUkt9e0jyCaBQJfzP/89ph///Cp+KWUXWEHXtCCIdTCOrYLr5Xv/
CSIXZINrYFKwUYZx2H30Z1qx1FFeXMyiUjgrWEqJX1JmvZDH3IRIXJs5JI+lnsnKiPqGluShbY8y
PTNgeGGKyShIk+lAXLZYzf+zwzw7TaegfzxxeaOQzld3BWKyVBR4Mg4fYddEtiCHXr48NtwBAAD/
/wMAUEsDBBQABgAIAAAAIQCL4B6/DwEAAIIBAAAPAAAAZHJzL2Rvd25yZXYueG1sTFDLTsMwELwj
8Q/WInGjdoICpdSpKgQqEuLR191KNg8R25VtmpSvZ5u2KidrZj2zMzuedLphW3S+tkZCNBDA0GQ2
r00pYbV8uRkC80GZXDXWoIQdepiklxdjNcpta+a4XYSSkYnxIyWhCmEz4txnFWrlB3aDhmaFdVoF
gq7kuVMtmeuGx0Lcca1qQxsqtcGnCrPvxY+W8I5fdrZ8XnefIdrN3qpW1OtyJeX1VTd9BBawC+fP
R/VrLiGGfRWqASnl65qpySrrWDFHX/9S+ANfOKuZs60EKpvZpn8JfxSFx0Auw+Q+6Scnhog4Ab43
DfYgvT1KaeU/6cHwJIseRCLEXsfPaXpwPl36BwAA//8DAFBLAwQKAAAAAAAAACEAjQw2Z4YaAACG
GgAAFAAAAGRycy9tZWRpYS9pbWFnZTEucG5niVBORw0KGgoAAAANSUhEUgAAAE4AAABDCAYAAAFF
MwNVAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAIdUAACHVAQSctJ0AABob
SURBVGhD7ZsJWFPHvsAPsoR9VZBFUBREBdRqXVot1fbaura9V9v6etVXa11RFBCFJJzsYd9dcEPt
KnWrCxAgZD/ZISJqxba22l6vtNXettZqhbyZkwkQCCEofb3vfff3ff8vM3PmnMz5z/af/8zBBown
rvoNBe3jb/v18VhKrRFFsYS9mr+jIIYFMMS3URDD1p02Yql1xp3nvvZDKV2485sfwF9fXPEZliww
vnxAMxnb+GnnUztxYV98RAbAkyqNRkeYGf59BEP4HpluZszmVgoK9mJiocgXBW0zd692DgpaMpIr
44zLUdSi6CCwtlznjIKYI1XzOwqaeOOIIRoFMWxrdW+19GQcW0KbmKPkQjWhJMxoNA5BwW6ADEcN
tzywLVXGmbnC52DSK4caF5DXzDjvqH+IJYM6BpnIJ0KlAzZXtQ4jMwyIGUXq/0JB+zjW8qO/L71B
EMFSvImS+sc/U3TMkyZ+5MtQVUwpUKXNKFGlBXE0+zwyxA9COLKjKJtt3DMk91DQki3VRm+a5CsU
s8CTLr2GgpYknbs6DgUtmJdf409qF2o2Tdx/5ZpxYF6wbCYQWDVJ1cbFZerXyYeS8ZpeDx1XIJuA
gl248y6QvbATWCIo2+s7JuySjIjbpwrrLiiXicSaGBTqIpAlFqMgeH/QbkHbdUkRfu+y/vRdsjHB
gcP8JzAM0/oD9L1d8DcSVwS67BTenr9PNxcHbds5rZ5NZkCA9u7gz9FeheEAuuQWmdgXU4s0T/ux
1HoUtQp4oLM3W9Mykq8g+8n/LrN3aed5MbXfeDJVem+6iPYMaNA+9IY0H6ZG6cbSf51Q1jQJZe2b
2HzVx+tOXRmJop2MYtb3arQLKi6ueG6XLglF7WPhHiKUrGkrvH7UEIuClvjSRNYVD5sDaMCUdNEv
KMWCYWz5aBTsIpAlvYKCnSTwhcuC6eJqc3tLqBC5okudeNHEd1Gwi3G50uko2MWG08b5pURcV+Ot
7/W6jpnElyjYxTv7W/xRkGQES36SHM03nzcuKNevIPvrNoExtbJlOMpC4kMXfoSCXeCVLS4oaAJ1
IQc0NpO623zOiK09YVm6ZEEaCnWxrNvDPOjSrv4I6Nnp4/jnOufvKYWqYyjYxdOl6gAU7CzVEKAj
79Sab8jXNesN/nZrKq4Z4l4Vh8UVqF6Bv0OSBO3YVjCOmW/YAIwD+DCyApAA3Q2n1n4DLztzLnRZ
HmbcqQ0PUXBAROUoC1Cwi7m79eRsNJavSIKVEcAkPh9GF+lfO/p5oCeuvAGvTS7S5/hSG65S0hre
GbK9nrSsJhY2WTc+hrPlKhS0ixF84jwKWseLLm97YbfmRRS1ysR85dt+LMXPKGofc/c0vRaXRwh8
aA0CP5qQu/Ijbe9+OJhUtbZSOOeuT3LfUTUZQ7LiyKXJFdeNvfrxoDO5VB8Sxledd2Wo/xXOkVcu
/6B5Jro0IGaX6Z8L5BACt0zit7gCNW9VxfXHL/ykAvWacB5Rg6L9EoELz2Bbzhvn7VFNREk2EYmM
Tn5MxdX1JwyBKMk+Fu67MDuYJT2Mov0SlikykB0ZjltgAPxLeeN8dKlfPGmSX8aXiTxR1DZTgIHt
QpX8iqL9svxQYxQ52sCCmYcsIM/t0iejLDZJAP08OltRiqK2iclRFY3LVdBQ1BYO2NqTv0emV2tw
UYsnloLG0W7D4Nhs5QGU1yZOzCbbNpEZF6qsYxxbEYGiVtl27IYbthVoCrQxLPGs0Xu74JtllZWO
jtsb7lkM+EAC6CIFuq1PAmgNl6bvbw5CURswGx9g3dZOPSnXgWtwhjYbw9tAlSZVGf22112H1/0y
RLexbWBigQKrG7yAy9aa78ib+2ByvqIiimfFMuiJK6fpQa+JGgFG5sju1UYKKGQYW/YJykISiTcU
+KZVl/umAtl2ptw38UT5iG2nDqLLvRiSUrtjyYGmhShqA7r+AVh29tLclCL1sl4FA9UWm6ekT+IL
J4Jln2lJAaZbU5XC66Y8Ji3WGB23CkzL9B740ETnFu9T91569AQYA4/G5Fiad1F8RbZpmEACCwBs
gnn7dIuezZXNxzaeIauWLAQsTM+XQC8C73EAVV1ZaXREjyZxoGmuW18k92Bmqf7NkVmq4yiKjcyo
PYVtOtOGbTnXBtqQ6Tfx/Hfz96qnJvCEy7HVn7Rh606DdHBtq6ANaKsNtMnekgIFXNta04ZtOtc2
q0AWif4C86UKbbZJC9zp8tZVp0x2xsZTl1YE4aJ/RHBletgWPaiiH0ZwJIaYLOkevObaM74MmTqE
q76+o+4Ln7hiYk0QQ1IXnN30I7jVIShbdzOQKTo8lClRwme5sHXtCbjIyTFd1g7jkDm7NUtCeNIu
/4Q9eFJl91EQe7pYvXnWwdZhsHBBbFmByGh0iskhPnnjyOXoMLZcHsknmvCaG/6L92tXBeGS2lAu
0QLvA3lrQliy46NztWRnCMhseGtGifbZQLqYNGc2nbwc4JIhs19r3YnKUX06jCm9TdwA49ogE8KS
XgTNR4eiT8YqsJoL50s/dmdpf/BhKL4bztN+NJwhWReIi170SK+Pn75LGT8LiC8Ig+noBWC3rQni
Kgv92Gqda3pDhytD1xLBk7O59V/YMdj+hz+Z+KMCDzBzdFrB4/h1k9nCSxFg3LIYy/5QcMHngWFs
aZovR6dxy5T/6sNU3g7gaKsnFOj3hbEk26FrAkp0ljxtBI/IG8pVn/ZiEjdd6bJffVga5fhcVSKO
2zHQDoSYbNVyn0zpQ1dcbYjNVyUWq1q90SW7ySdu+I/iyaluLM3XQxmKy8+U6qeiS4/H6SvfeVEy
FO2jeLISlGSTEXRhmRzcg6J90tJidAnjE1fBfPrbKivOjP4xGh1cMuS/rP5QH4JSbALal4vDlmqj
D03yOUrql/giRexQhqy3I6Q/ppbo35hVpi9C0X7xp9aLzRP//EPaOJTcL364vPWlAxfGoqh9uKWL
2xeV69xR1CbLyoCJDrRGei6AbeeSJuycN/tj/dnLsQFMS1vQJqsqW4Z7UBv+gaL94p0ieEB6icx2
HDCPphWqVqLL/eK2U9jb/dwX8cBkji9UbkDRfvHfWXejc/WFbDeXnSKrRqU1AnCZ3WtjzD1DfI8g
Ouya6OPw2o+hCeSwraajp5E5iisrRtlsEpUlf3fD8eZO284mjnRlBwraJIpWk4K984kRrrqC6MJT
5IKnW+GAgQk9Zw6m3H2z9L3G8WOzJdtRtG9wkcjJHVe2oahNHDeffYitP2X03Fp1B5rdjmnCHoWr
M47gyBpR9j7BgW3oT5fY9ntB5u7Xx4chq9UWMVzpcXLNCl2lYP0wK1uaMIYn2UpWrbl64W+yoCOh
uMeWhxUoGdJmFOwbv4z6+U8XEmUoapWX31N5k75cuJiB/hFQSEpyzSM4cFN2Njw0LYLIgpHXA6j1
5HrWFu6ci1+gYN+MzZJTx3DlXZu4VghmiA1IK6YCwp66+bzxlUOaEQv3N08h08wFhwNz4jnj7FyJ
zV0TJ7ruJgraIKmG+tcjF1egWC8WHjDEdrYpsoBAQCHCWV3DgV+68LbJOw6qfRNYMoJ26ZH46Q/o
slU8mPo7KNg3QZkNuUvfv9Rn4fxx6Zedheveriq6PMLZ8iteFqv9JCAbT5Qnf6Drvb2GsKtwWHIN
9ZUjBquFW3SgaR65gu9eOKC5GcXE8ZeLqyhxoOH32gboJhPyJCPQo3pDb7K+HdqdWaWqTA+a2Oq2
MGWn+K5FwYDWnLbXkY5895S6VrIq0fza5YoAQlY9SAfzb3y2NJN8WA98s1r6t2bGZileGsWVlKNo
Jy/sVm8gqxAVytzexmcrdmw+3TIeNnprDsTO/KjnYkkC4+rTVyxsPrh36YVLDCjaN2uOfTbKjy66
gKIka3VGZyy5rsP0x0BQL6Wk1pG72c6JoNHD8Q7+Obzes3DdCwheYDRLcpJ8MGLbMcItgmenZeKG
aywOlEzIUpSQQwL8c1g9aIiIy5LNn8qqXYKtO0k6EPvUnFnM2gPVj4uud3aghH1Nk6KzZO+gqG2c
qIrO7Sf4Vg6J1d9hm84iRw4Q8OuRUksOrC5rPmnD1p5swzacAddq2kDBTA6bvhw5yeB6UnWbd0qN
lPwDQAhHsn79ic/t86gPZ0mIV480J6DoH04AU16Ngv2z8GDjRH+a8CKKYjG5KrUTXdExtVB9Ora0
+chIrlThz9Pdx0VGp1AucSsgU1w6Nk/TBJq2gw9Dfj+ALv7k6VJ18vO7DaBzyVuG8XQ3XizVh/jg
CmJqoaYkOks5C9tcNR49HphoA9y6c0qXPHq5uOsMkPtOIXm2ITZPediXLtKNzlHfhx4nb2rDEZgO
/li3YJ96Uhhb9DaMAxxG8VXk8QW4uB7Nk7N9qKKauDzlnlkluo1gPCUHZFz02dBhTIUIhu1mzm79
6rh8dS6KYiNYkp/g7+z9zfvgb3SWgiiuqqJE5ahrhzOlH0fnqirgEYcQnlofzpWfiSkyvBlXQDwV
xSe0wWxZ87Iy9fCRPNk5eG8oV315KG7yxwWzpQ2T8omBNyHXDOm/Vn3U+5zAYLGsUufjTu3jcE1/
wKNSjhlE+4KKC/+NkgaNxfsNLw/JUD568YguHCUNHDiUhLBkxRRc/UtMNlGcX3PDYuN/IKz94LOY
UVzpx24M5b1gnnovfuZbu5aedjGpSPuOL0utd0uX/u7DVClG8RQ543KJ16cVq+aE4vWk4xD8eXx8
kXJWKEeycCRXnhbK15/yZcp/cGOq7vowpOef26VZjB73x7L6k0uTY/OVW4ZmSnaFc6QCeCQshCkV
xGQrj80o0rDfeu/yvKrWjj5P/f2H//BHAEy1ZWUiz2eLdOFT8xSwiS4Yn6dODuYRZaF8jcCPSTQF
4NLvnXaKfnTPELVTdgJJa2h3TRNZiMsOUbtbOrwuvu9Fl/7ox5C3DmMTDUFcbXlcoZoWky1dFMZV
xIK+Gbb2jH3+p38raDVfTpxYoMofxhDfdMbVDx3wpkfumfKOYbjkq0g+cfTZPdrEFSc/m0XuRKzV
OYOp1QlMFI4mT7EROi6AwN+eQjo1HDAcH4LBvHBbb225c1adzmfRoQtPzd6r2zQmS/HxMFz0Dxca
0Y7hukeUDNk9MLEKXzygX9HT9v5TWXrshhs8k+NOlcspNOJfvrj8nyNztAfhpuyEHO3oXIHBA2Ud
MK8f0j71t/3a51F04ICWvvyDz4bG5KmmTCjQZAxlElo3UIEeDNX1qBzlnjc/vNT/xu5g818fNPtF
Z6mOu9CU9yP4yrOvv3/F+pHtx2ROsWqKV7LgluNWgTE+S5IOvV7o0hMDPbbxBZpUb4662Z0u/9fM
XXqz7fnHMj6f8PdlyEVeNOnPiSeu2nUiaCC8WKJYToH+RuhiAYu7Ick1DyPZcl4Cbhw05ZGALh+b
r0p3Tpc9GJ2lYMIWiq4MPnBfOowtkwPj5/qc3ZqB7RrYQRxfvsYlWXCPdGjA1S9c3SbXGB1S6zvi
CpQWS/DBADqKY3OJXEeq7NfoHDl0zQ++8nBQQ6Ecea5TuvzehBzC7s0Fe5ldqGQ4balpN3mBgMKg
19HsNiBdB/XG4Wyi/uWDjY/xqULfQAdNZJbyjBdddjMAr7VrD21APFOoXOJMl38TwZWeg4fxUPKg
MI7bkOSwFSgHOr5JP0s3pVn4WeqNgWyFdHPVwLdSbRGfqxrlkSn/8tm9hr+ipMEhAhe5BmQ2NHtk
1D8EM1WfHtPH4UaHaYNo+WHDSmfkViaVRvodeyrOJF5U6c9Jp68M2lABV76hHFl+BE96EbTAwdtA
n3+w6W1nOmEMZissP6N6Ata8pwoLTDp1hbL2+N0puGApTJuYI3uNkib+3rKL9lYcFE+65KsZJdpp
5MMGgUkFxIuuDM2dOaWaJSjpyQnnEhKwzP9tUoGKfMEn5Zkc4Wj/1PMG8tj+u8eNzutOtT+b3ZAC
ry0qV87yyJDctdXizOJJFf/8yqELs8mHPiHQxHJkGb4OZYn3oKQnI6O6LdglXfSbL018dWyh/Im7
x8qPWkf77xDcJZ3a0He8CcqnRmzjpx2jceFeXHTd9bWjhkA/mugCUJxp39aaAlG6w07RQ9KH/ITm
BJwkKOmSVg+qSI2f6vJRPzZ/2aud58q5+CiAJj4UnKcbipIfi5lF6kWUHcK7Jgc8FDCDmrfFEs8Z
HRKr28cwxadg3pXHmkKDcXFLZ8uzJrBLg9nXJVXwEKyFB/bZkBU8MoTVvrzm1iUDPaBhjYQ9+m0Y
Tf8gki9LWnqMeOwjfc+XNb5CKo0c+OELI4FKJBUJlQi3k6uMwzKEl1ZVNI2EPqnobMUJB3JTCt3X
eT+YeckTyaZ7h4BnxXIa9pGftj4mcblEpQPDcM0lrdbq94YDwjOjgUrhGB7M2a1f0f0rIXuBXuup
JZrtjjvFvbpZV6sBglqhS4rwwcI9qkR0O5ZQcd01ki3Z5bCt5pFJSV3KIlsr3GSD3X3jp8YhG860
h2dUH16EP6Z3JKV2B9xLXVJhz+npfnBKqc2E31PCDeG+joD3CRg3xvDkuFNq3e8W41NPBaIW5L6z
/qeX92gtP9pFJJQomdg2sBTbArdCgWwGyoMnuDfBAw9AcevBOPnuSSgdI1POKsGYNeBKNiuu14fD
j0VSDdWD3zxgxcFNmnG5igrHtIbeCusupOJqjZS0hgcJJba/joKtd1ml0bE/gQeCSPfTAAEronIK
Q3/r2VLl43tlzAQyxFRHTtODqQWqFePxSrsU92qpOiCIJTuNpfY+j2MhSGl+VMn1lHOXgJ0tcprI
E/NdUwV3KdsEdyjJtXcoKXV3KKlIttffoaT1EJi2HV2HeVPAPfC+ZMGdUKqQeOs9bTAqVr8MY4g/
cuYYvhidZeUz5oEys1iz2oHW+HBygZJpjzMQWuHhWYQELo/6VBZSGBTvTLnhuaLGKNBGHWK4DTlO
iVVgvYqWXp2rCCTme6H0fB4Ucz44caAx0Ge74P5bhy48hYpnE2DDXRqSLmuOKbBf2X3y/F7DdApd
1R7FlYtm79L0fQYEsPp9fUQAFcyc5PGMbi/UXUiFwResNYYzJJ8noU8Y4jjCsw6J5zrIwb7TO4LW
rPAeeG/3CrAm3Z8PlQcVv6Xa6Lq9/nY8XzKXLGQfwKWWK534wosmEoK18JPvMB013PIAxugPXgzF
rbg81RSU3IuEMvUkn50NrSbzAhSaNBfgC6CXIF/E9DJDtlZ3hLOkp+F3brBbh6bXnoVHm+DM2Etx
5lMf9igOijmvhfKqjK6pdb8sPdjY58pnQYV6+BBW880wjoyLkp6cMVmKEkeasmNSocbqoZ6NlYZZ
Hqn1P5IvbH5p+L1Sp5hqnhQQH8USn978nso75ajBIzzlnBh7Fyy91gHFwU+M4SxJPgO+NBBSEUgp
20H3t0dIBSIlQgUi5cFzP3FcCY2cPHoQm6tY6EpX3l18SGezZQ6IHaeujPSgiX8aypJdP/Ntj21k
OHtNK/bG/n7Uo0sEVsSUPmZzVVc3CMt3w5aVeVre28f9Kd1+7ZGe95tl9WkvcoOoG9A+DWFLSyP5
0iZoDaDkJwdO8TNLNBwKjfhpQh6Rh5I7IU8dpgjfBi3kLSyt9nUsqW6lP020FJoP47Mk07Bk4XJs
i2DVkNS6lc+UaV+pFN3u/GZ1CVjeRPIUO4I4kvxpxZqt7JprFuPovF2qORFcOWs0T5Hz0oELS/IJ
0wdJcG356uErU8bmKJgj2NK8CXnKDbyTlzu/vPeg18/xTBf+lfwwExCW3+LvmFa/lFRcDybnKiLc
M+WtL+y/+AZKGjxWgcU3KOAJF5rsxxlWzgrAwRXYQVvhAZ3F+5uDYBzafbF5usNu6aL3X9irjQPr
3ulhbPmFYIb0BNwBG5MlP+nPUcsWHm4Khc+Yu79pmieu+HJ8FpH42okLYT4M2ZXhTOkB88tHZyvf
gOcXQNAhnK88HJWjqbnynekzi5f2ap9yx4m2SJ5kPYz70kWCQKZM4p0p+XV6qWbthDzNM47bax+a
D/x0Z2yOcr8bXdzmxiHIcgw6+BmdexhbesUXl7c+1eNzSMiYLGKrG1BcLFdJfr0FFTehUHfYIUW4
F1t7xn1xhW6GB1XybQRHmltZWek4OV/5oT8uN8zapY+fVqzynl6kXeDDVt98ulD57psffhHkj8ua
w7nE6eg80dCJhU2+scX6zZPKDFvg8BCRqy0bxlJUPV2mHj4lS+czv7xxvhdDfm80X0a69T1pIgGW
XPcWqECXEVxCEsxWfOGYKvi1p+ISdjeucU6X/R7CEONwfxclDz7z9hP+wzgKkRdNdn/5B5YH6eAH
hRtOXh0HuzZKwnD51yEbK68PR1GMX3c1kir4ahTcw4BxMLP6xOSpo53TxHGT84kxIN45hsJD7fG5
hkBgTE+I5krjQasK7hrY4V6BzuepfCLOlyaOm1aiGtX9C5KU45ciOl3sQCGrQXzdsdbx3dfbM0t0
r7rSZT8GscQ1g3oEqC+gITw2h9jlkiF7EMYhjm481jIGXfo/ARxKorKV2e40ya8xuardokHct7WL
hXBjOlvJ82Bprnviyt+i8nSiWWXa5fC7b7iCQNn+VMp137rDrcxJBepVkbkagVsm8ZMLQ30ziq98
f+WfsZtvjTUnL0fH5SpK/HDp9w507SNnqrI9kCn+Gow7xUsqtAu59f8MMp8VgZ4TdNvjYjpHAp8F
nrnpyOWAv3/Y/JeJhWruaL6iyZMmfeRA07Q7Mxt/D2JJv51cqOWevHi717j8b0mu4JbH9ELlyNll
6ufG5qjWjcxRHwzlKbV+DOlt9wzJ7650+UNQ+z+7sAxtrkz9Px0yNZ87MRuv+fANneLMarw2hKG9
5sLUfgvzuTD1dyg0xQM3quyRZ4bogQdddi2ErxKPytXuiy1Ub4kvUMyNzBZHwbEYbjijovz/Bhqf
UOCkYBZzGsrybwSG/Q+1Z0rlOdEPgAAAAABJRU5ErkJgglBLAQItABQABgAIAAAAIQBamK3CDAEA
ABgCAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAGAAgAAAAh
AAjDGKTUAAAAkwEAAAsAAAAAAAAAAAAAAAAAPQEAAF9yZWxzLy5yZWxzUEsBAi0AFAAGAAgAAAAh
AAyZt6ZsAgAApgUAABIAAAAAAAAAAAAAAAAAOgIAAGRycy9waWN0dXJleG1sLnhtbFBLAQItABQA
BgAIAAAAIQCqJg6+vAAAACEBAAAdAAAAAAAAAAAAAAAAANYEAABkcnMvX3JlbHMvcGljdHVyZXht
bC54bWwucmVsc1BLAQItABQABgAIAAAAIQCL4B6/DwEAAIIBAAAPAAAAAAAAAAAAAAAAAM0FAABk
cnMvZG93bnJldi54bWxQSwECLQAKAAAAAAAAACEAjQw2Z4YaAACGGgAAFAAAAAAAAAAAAAAAAAAJ
BwAAZHJzL21lZGlhL2ltYWdlMS5wbmdQSwUGAAAAAAYABgCEAQAAwSEAAAAA
">
   <v:imagedata src="Tanda%20Terima%20Gaji_files/Report%20(Autosaved)_1018_image001.png"
    o:title=""/>
   <x:ClientData ObjectType="Pict">
    <x:SizeWithCells/>
    <x:CF>Bitmap</x:CF>
    <x:AutoPict/>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:9px;margin-top:3px;width:58px;
  height:52px'><img width=58 height=52
  src="{{Config::get('constants.path.img')}}/logoreport.png"
  v:shapes="Picture_x0020_1"></span><![endif]><span style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td height=7 class=xl641018 width=13 style='height:5.25pt;width:10pt'></td>
   </tr>
  </table>
  </span></td>
  <td class=xl641018 width=34 style='width:26pt'></td>
  <td class=xl641018 width=204 style='width:153pt'></td>
  <td class=xl641018 width=206 style='width:155pt'></td>
  <td class=xl641018 width=218 style='width:164pt'></td>
  <td class=xl641018 width=141 style='width:106pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
 </tr>
 <tr class=xl651018 height=25 style='mso-height-source:userset;height:18.75pt'>
  <td height=25 class=xl651018 width=13 style='height:18.75pt;width:10pt'></td>
  <td class=xl651018 width=34 style='width:26pt'></td>
  <td colspan=4 class=xl691018 width=769 style='width:578pt'><span
  style='mso-spacerun:yes'>     </span>PT. TROCON INDAH PERKASA</td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
  <td class=xl651018 width=64 style='width:48pt'></td>
 </tr>
 <tr class=xl661018 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl661018 width=13 style='height:17.25pt;width:10pt'></td>
  <td colspan=5 class=xl721018 width=803 style='width:604pt'><span
  style='mso-spacerun:yes'>              </span>TANDA TERIMA GAJI</td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
  <td class=xl661018 width=64 style='width:48pt'></td>
 </tr>
 <tr class=xl641018 height=4 style='mso-height-source:userset;height:3.0pt'>
  <td height=4 class=xl641018 width=13 style='height:3.0pt;width:10pt'></td>
  <td class=xl641018 width=34 style='width:26pt'></td>
  <td colspan=4 class=xl681018 width=769 style='width:578pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
 </tr>
 <tr class=xl641018 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl641018 width=13 style='height:17.25pt;width:10pt'></td>
  <td colspan=2 class=xl711018 width=238 style='width:179pt'>PERIODE</td>
  <td colspan=3 class=xl711018 width=565 style='width:425pt'>: {{ $datafilter->period }}</td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
 </tr>
 <tr class=xl641018 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl641018 width=13 style='height:17.25pt;width:10pt'></td>
  <td colspan=2 class=xl701018 width=238 style='width:179pt'>DEPARTEMENT</td>
  <td colspan=3 class=xl701018 width=565 style='width:425pt'>: {{ $datafilter->departmentname }}</td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
  <td class=xl641018 width=64 style='width:48pt'></td>
 </tr>
 <tr height=28 style='mso-height-source:userset;height:21.0pt'>
  <td height=28 class=xl151018 style='height:21.0pt'></td>
  <td class=xl771018 style='border-top:none'>No</td>
  <td class=xl771018 style='border-top:none;border-left:none'>Employee</td>
  <td class=xl771018 style='border-top:none;border-left:none'>Department</td>
  <td class=xl771018 style='border-top:none;border-left:none'>Position</td>
  <td class=xl771018 style='border-top:none;border-left:none'>Tanda Tangan</td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
 </tr>
 @php
 $n = 1;
 @endphp
 @foreach($itemdata AS $itemdatas)
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl151018 style='height:15.0pt'></td>
  <td class=xl671018 style='border-top:none'>@php echo $n++; @endphp</td>
  <td class=xl631018 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->employee }}</td>
  <td class=xl631018 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->departmentname }}</td>
  <td class=xl631018 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->position }}</td>
  <td class=xl631018 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
 </tr>
 @endforeach
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl151018 style='height:15.0pt'></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl151018 style='height:15.0pt'></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl151018 style='height:15.0pt'></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl151018 style='height:15.0pt'></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl151018 style='height:15.0pt'></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td colspan=3 class=xl761018>JAKARTA, @php echo date("d M Y"); @endphp</td>
  <td class=xl731018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl151018 style='height:15.0pt'></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl741018>DISETUJUI</td>
  <td class=xl741018>MENGETAHUI</td>
  <td class=xl751018>DIBUAT OLEH</td>
  <td class=xl151018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl151018 style='height:15.0pt'></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl151018 style='height:15.0pt'></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl151018 style='height:15.0pt'></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl151018 style='height:15.0pt'></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl731018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl151018 style='height:15.0pt'></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl751018>(________________)</td>
  <td class=xl741018>(_________________)</td>
  <td class=xl741018>(________________)</td>
  <td class=xl741018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
  <td class=xl151018></td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=13 style='width:10pt'></td>
  <td width=34 style='width:26pt'></td>
  <td width=204 style='width:153pt'></td>
  <td width=206 style='width:155pt'></td>
  <td width=218 style='width:164pt'></td>
  <td width=141 style='width:106pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>



