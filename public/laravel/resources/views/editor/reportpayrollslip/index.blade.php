@extends('layouts.editor.template')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    Report Jamsostek
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Report Jamsostek</li>
  </ol>
</section>

<section class="content">
  <div class="row"> 
    {!! Form::model($datafilter)!!}
        {{ csrf_field() }}
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <div class="col-sm-3" data-toggle="tooltip" data-placement="top" title="Department">
            {{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control', 'placeholder' => 'Select Department', 'id' => 'departmentid', 'onchange' => 'RefreshData();')) }} 
          </div>
          <div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Period">
            {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'RefreshData();')) }} 
          </div>  
          <div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Employee">
            {{ Form::select('employeeid', $employee_list, old('employeeid'), array('class' => 'form-control', 'placeholder' => 'Select Employee', 'id' => 'employeeid', 'onchange' => 'RefreshData();')) }} 
          </div>  
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
          {!! Form::close() !!}
        </div>  
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>ID</th>
                  <th>Nama Karyawan</th>  
                  <th>Jabatan</th>
                  <th>Periode</th>
                  <th>Pembayaran</th>
                  <th>Grade</th>
                  <th>Status</th> 
                  <th>Penghasilan</th>
                  <th>Gaji Pokok</th>
                  <th>Tunjangan Jabatan</th>
                  <th>Uang Makan Transport</th>
                  <th>Lembur</th>
                  <th>TMlm, TLK, Inst Pd</th>
                  <th>Pengobatan</th>
                  <th>Tunj Jamsostek</th>
                  <th>Gaji Bruto</th>
                  <th>Potongan</th>
                  <th>Pinjaman Karyawan</th>
                  <th>Kendaraan</th>
                  <th>Pot Absence</th>
                  <th>Pot.Uang Makan,Trnsprt</th>
                  <th>Pot.TMlm,TLK,Inst</th>
                  <th>Potongan Pengobatan</th>
                  <th>PPh Pasal 21</th>
                  <th>Potongan Jamsostek</th>
                  <th>Potongan Pensiun</th>
                  <th>Potongan Lainnya</th>
                  <th>Jumlah Potongan</th>
                  <th>Gaji Bersih</th>
                  <th>Sisa Hutang Pnjaman</th>
                  <th>THT dby Perusahaan 3,7%</th>
                  <th>Tj Pensiun dby Prusahaan 2%</th> 
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  



@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "scrollX": true,
         "order": [[ 0, "asc" ]],
         "sScrollXInner": "330%",
         "autoWidth": true,
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/reportpayrollslip/data') }}",
         columns: [   
         { data: 'employeeid', name: 'employeeid' }, 
         { data: 'employeename', name: 'employeename' }, 
         { data: 'jabatan', name: 'jabatan' },  
         { data: 'periode', name: 'periode' }, 
         { data: 'pembayaran', name: 'pembayaran' },  
         { data: 'grade', name: 'grade' },  
         { data: 'status', name: 'status' },
         { data: 'penghasilan', name: 'penghasilan', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'gaji_pokok', name: 'gaji_pokok', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'tunjangan_jabatan', name: 'tunjangan_jabatan', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'uang_makan_transport', name: 'uang_makan_transport', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'lembur', name: 'lembur', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'tlm_tlk', name: 'tlm_tlk', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'pengobatan', name: 'pengobatan', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'tunj_jamsostek', name: 'tunj_jamsostek', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'gaji_bruto', name: 'gaji_bruto', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'potongan', name: 'potongan', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'pinjaman_karyawan', name: 'pinjaman_karyawan', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'kendaraan', name: 'kendaraan', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'pot_absence', name: 'pot_absence', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'pot_uang_makan', name: 'pot_uang_makan', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'pot_t_malam', name: 'pot_t_malam', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'pot_pengobatan', name: 'pot_pengobatan', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'pph_21', name: 'pph_21', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'pot_jamsostek', name: 'pot_jamsostek', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'pot_pensiun', name: 'pot_pensiun', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'pot_lainnya', name: 'pot_lainnya', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'jum_potongan', name: 'jum_potongan', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'gaji_bersih', name: 'gaji_bersih', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'sisa_hutang_pinjaman', name: 'sisa_hutang_pinjaman', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'tht', name: 'tht', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         { data: 'tunj_pensiun', name: 'tunj_pensiun', render: $.fn.dataTable.render.number( ',', '.', 0) },   
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function RefreshData()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.periodfilteremp') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'departmentid': $('#departmentid').val(),    
            'periodid': $('#periodid').val(),    
            'employeeid': $('#employeeid').val()   
          }, 
          success: function(data) { 
            reload_table();
          }
        }) 
      }; 
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
    </script> 
    @stop
