<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 14">
<link rel=File-List href="Report%20(Autosaved)%20(Autosaved)_files/filelist.xml">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<style id="Report (Autosaved) (Autosaved)_29058_Styles"><!--table
  {mso-displayed-decimal-separator:"\,";
  mso-displayed-thousand-separator:"\.";}
.xl1529058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6329058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6429058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border:.5pt solid windowtext;
  background:#D6DCE4;
  mso-pattern:black none;
  white-space:nowrap;}
.xl6529058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6629058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6729058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6829058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6929058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7029058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:16.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7129058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7229058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:16.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7329058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7429058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7529058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7629058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7729058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border-top:.5pt solid windowtext;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7829058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border-top:.5pt solid windowtext;
  border-right:.5pt solid windowtext;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7929058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl8029058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl8129058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl8229058
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
--></style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="Report (Autosaved) (Autosaved)_29058" align=center
x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=1062 style='border-collapse:
 collapse;table-layout:fixed;width:797pt'>
 <col width=20 style='mso-width-source:userset;mso-width-alt:731;width:15pt'>
 <col width=356 style='mso-width-source:userset;mso-width-alt:13019;width:267pt'>
 <col width=99 style='mso-width-source:userset;mso-width-alt:3620;width:74pt'>
 <col width=153 span=3 style='mso-width-source:userset;mso-width-alt:5595;
 width:115pt'>
 <col width=64 span=2 style='width:48pt'>
 <tr class=xl6629058 height=7 style='mso-height-source:userset;height:5.25pt'>
  <td height=7 class=xl6629058 width=20 style='height:5.25pt;width:15pt'></td>
  <td class=xl6629058 width=356 style='width:267pt'></td>
  <td class=xl6629058 width=99 style='width:74pt'></td>
  <td class=xl6629058 width=153 style='width:115pt'></td>
  <td class=xl6629058 width=153 style='width:115pt'></td>
  <td class=xl6629058 width=153 style='width:115pt'></td>
  <td class=xl6629058 width=64 style='width:48pt'></td>
  <td class=xl6629058 width=64 style='width:48pt'></td>
 </tr>
 <tr class=xl6729058 height=25 style='mso-height-source:userset;height:18.75pt'>
  <td height=25 width=20 style='height:18.75pt;width:15pt' align=left
  valign=top><!--[if gte vml 1]><v:shapetype id="_x0000_t75" coordsize="21600,21600"
   o:spt="75" o:preferrelative="t" path="m@4@5l@4@11@9@11@9@5xe" filled="f"
   stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="Picture_x0020_1" o:spid="_x0000_s11266" type="#_x0000_t75"
   style='position:absolute;margin-left:9pt;margin-top:0;width:43.5pt;height:36pt;
   z-index:1;visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQBamK3CDAEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhvROFGcle3tSdZNgokh7Rjb3+8vyWK5tSObIJBxWPPbsuIM
UDltsK/5x/qleOCMokQtR4dQ8x0QXzbXV4v1zgOxRCPVfIjRPwpBagArqXQeMHU6F6yM6Rh64aX6
lD2Iu6q6F8phBIxFzBm8WbTQyc0Y2fM2lWcTjz1nT/NcXlVzYzOf6+JPIsBIJ4j0fjRKxnQ3MaE+
8SoOTmUi9zM0GE83SfzMhtz57fRzwYF7S48ZjAa2kiG+SpvMhQ4kvFFxEyBNlf/nZFFLhes6o6Bs
A61m8ih2boF2XxhgujS9Tdg7TMd0sf/X5hsAAP//AwBQSwMEFAAGAAgAAAAhAAjDGKTUAAAAkwEA
AAsAAABfcmVscy8ucmVsc6SQwWrDMAyG74O+g9F9cdrDGKNOb4NeSwu7GltJzGLLSG7avv1M2WAZ
ve2oX+j7xL/dXeOkZmQJlAysmxYUJkc+pMHA6fj+/ApKik3eTpTQwA0Fdt3qaXvAyZZ6JGPIoiol
iYGxlPymtbgRo5WGMqa66YmjLXXkQWfrPu2AetO2L5p/M6BbMNXeG+C934A63nI1/2HH4JiE+tI4
ipr6PrhHVO3pkg44V4rlAYsBz3IPGeemPgf6sXf9T28OrpwZP6phof7Oq/nHrhdVdl8AAAD//wMA
UEsDBBQABgAIAAAAIQD0nlLhYQIAAJ8FAAASAAAAZHJzL3BpY3R1cmV4bWwueG1srFRhb5swEP0+
af/B8vcEQyGkKFBlSTtNqrpo2n6Aa0ywBjaynTTVtP++sw2Numpate7b4TvfO95759XVqe/QkWsj
lCxxPCcYcclULeS+xN++3syWGBlLZU07JXmJH7nBV9X7d6tTrQsqWas0ghbSFHBQ4tbaoYgiw1re
UzNXA5eQbZTuqYVPvY9qTR+ged9FCSGLyAya09q0nNttyODK97YPasO7bh0geC3s2pQYZnCnY02j
VR+qmeoqsorcUC70HSD43DRVHKcX5JxzRz6t1UMVhysunM5cfqyGY1/t256xrHrqPzX4HTPNF0n2
B8yL12NOSINgAVIed4Lt9Ih/d9xpJOoSJxhJ2oM6kLUHzVGMo3NNuEEL6HKr2Hcz6kX/Qa2eCglY
atNSuedrM3BmwTUOLXAPIwU4//ls3PtODDeiA3Fo4eI3jxFs9yrTqaYRjG8VO/Rc2uA8zTtqwfWm
FYPBSBe8v+dApv5UxxgxML0FRgctpAW70YKf7K2xY4QOWpT4R7JcE3KZfJhtMrKZpSS/nq0v03yW
k+s8Jeky3sSbn+52nBYHw4F+2m0HMf16nL7QoBdMK6MaO2eqj8Lc087A3DGJggZH2pWYeOL9aCDA
eUQIHcNuVmM1t6ydEF/g/X1DPZ5r1YB4X0BwJ/ZT41H4s7huBc3gPEqLU6P7/4EMNKAT+MxvMkaP
JV4sFnnm/t7/NGKQzbIkzeD5YpBOsxxel5EdN4UrHLSxH7l680TINQKfABXeGPQIvgikTBAjK4EH
vwqwfONGdgIsuKWWTkvz7KUbb4aXtfoFAAD//wMAUEsDBBQABgAIAAAAIQCqJg6+vAAAACEBAAAd
AAAAZHJzL19yZWxzL3BpY3R1cmV4bWwueG1sLnJlbHOEj0FqwzAQRfeF3EHMPpadRSjFsjeh4G1I
DjBIY1nEGglJLfXtI8gmgUCX8z//PaYf//wqfillF1hB17QgiHUwjq2C6+V7/wkiF2SDa2BSsFGG
cdh99GdasdRRXlzMolI4K1hKiV9SZr2Qx9yESFybOSSPpZ7Jyoj6hpbkoW2PMj0zYHhhiskoSJPp
QFy2WM3/s8M8O02noH88cXmjkM5XdwVislQUeDIOH2HXRLYgh16+PDbcAQAA//8DAFBLAwQUAAYA
CAAAACEAXT8QYQ0BAAB/AQAADwAAAGRycy9kb3ducmV2LnhtbHSQXUvDMBSG7wX/QziCdy5dW6fU
pmMKiiDIPkTxLqbpUtYkJYlr56/3dJvsysvnJM/J+yaf9rohW+l8bQ2D8SgCIo2wZW3WDN5Wj1e3
QHzgpuSNNZLBTnqYFudnOc9K25mF3C7DmuAS4zPOQIXQZpR6oaTmfmRbafCssk7zgOjWtHS8w+W6
oXEUTajmtcEXFG/lg5Jis/zWDD43YvWh5u/tV9Ilu1TU9/OXJ8XY5UU/uwMSZB9Ol4/2c8kghqEK
1oAC8/XNzAhlHakW0tc/GP4wr5zVxNluYCJswwBLI79WlZdhTzj9o/E4TaII6LAx2IOXHD30//XS
m0l8vffoKUqRI5z+rfgFAAD//wMAUEsDBAoAAAAAAAAAIQCNDDZnhhoAAIYaAAAUAAAAZHJzL21l
ZGlhL2ltYWdlMS5wbmeJUE5HDQoaCgAAAA1JSERSAAAATgAAAEMIBgAAAUUzA1UAAAABc1JHQgCu
zhzpAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAh1QAAIdUBBJy0nQAAGhtJREFUaEPtmwlYU8e+
wA+yhH1VkEVQFEQF1GpdWi3V9tq6tr1X2/p61VdrXVEUEIUknOxh311wQ+0qdasLECBkP9khImrF
trbaXq+01d621mqFvJmTCRAIISh9ve999/d9/y8zc+aczPnP9p//zMEGjCeu+g0F7eNv+/XxWEqt
EUWxhL2av6MghgUwxLdREMPWnTZiqXXGnee+9kMpXbjzmx/AX19c8RmWLDC+fEAzGdv4aedTO3Fh
X3xEBsCTKo1GR5gZ/n0EQ/gemW5mzOZWCgr2YmKhyBcFbTN3r3YOCloykivjjMtR1KLoILC2XOeM
gpgjVfM7Cpp444ghGgUxbGt1b7X0ZBxbQpuYo+RCNaEkzGg0DkHBboAMRw23PLAtVcaZucLnYNIr
hxoXkNfMOO+of4glgzoGmcgnQqUDNle1DiMzDIgZRer/QkH7ONbyo78vvUEQwVK8iZL6xz9TdMyT
Jn7ky1BVTClQpc0oUaUFcTT7PDLED0I4sqMom23cMyT3UNCSLdVGb5rkKxSzwJMuvYaCliSduzoO
BS2Yl1/jT2oXajZN3H/lmnFgXrBsJhBYNUnVxsVl6tfJh5Lxml4PHVcgm4CCXbjzLpC9sBNYIijb
6zsm7JKMiNunCusuKJeJxJoYFOoikCUWoyB4f9BuQdt1SRF+77L+9F2yMcGBw/wnMAzT+gP0vV3w
NxJXBLrsFN6ev083Fwdt2zmtnk1mQID27uDP0V6F4QC65BaZ2BdTizRP+7HUehS1Cnigszdb0zKS
ryD7yf8us3dp53kxtd94MlV6b7qI9gxo0D70hjQfpkbpxtJ/nVDWNAll7ZvYfNXH605dGYminYxi
1vdqtAsqLq54bpcuCUXtY+EeIpSsaSu8ftQQi4KW+NJE1hUPmwNowJR00S8oxYJhbPloFOwikCW9
goKdJPCFy4Lp4mpze0uoELmiS5140cR3UbCLcbnS6SjYxYbTxvmlRFxX463v9bqOmcSXKNjFO/tb
/FGQZARLfpIczTefNy4o168g++s2gTG1smU4ykLiQxd+hIJd4JUtLihoAnUhBzQ2k7rbfM6IrT1h
WbpkQRoKdbGs28M86NKu/gjo2enj+Oc65+8phapjKNjF06XqABTsLNUQoCPv1JpvyNc16w3+dmsq
rhniXhWHxRWoXoG/Q5IE7dhWMI6Zb9gAjAP4MLICkADdDafWfgMvO3MudFkeZtypDQ9RcEBE5SgL
ULCLubv15Gw0lq9IgpURwCQ+H0YX6V87+nmgJ668Aa9NLtLn+FIbrlLSGt4Zsr2etKwmFjZZNz6G
s+UqFLSLEXziPApax4sub3tht+ZFFLXKxHzl234sxc8oah9z9zS9FpdHCHxoDQI/mpC78iNt7344
mFS1tlI4565Pct9RNRlDsuLIpckV1429+vGgM7lUHxLGV513Zaj/Fc6RVy7/oHkmujQgZpfpnwvk
EAK3TOK3uAI1b1XF9ccv/KQC9ZpwHlGDov0SgQvPYFvOG+ftUU1ESTYRiYxOfkzF1fUnDIEoyT4W
7rswO5glPYyi/RKWKTKQHRmOW2AA/Et543x0qV88aZJfxpeJPFHUNlOAge1ClfyKov2y/FBjFDna
wIKZhywgz+3SJ6MsNkkA/Tw6W1GKoraJyVEVjctV0FDUFg7Y2pO/R6ZXa3BRiyeWgsbRbsPg2Gzl
AZTXJk7MJts2kRkXqqxjHFsRgaJW2Xbshhu2FWgKtDEs8azRe7vgm2WVlY6O2xvuWQz4QALoIgW6
rU8CaA2Xpu9vDkJRGzAbH2Dd1k49KdeBa3CGNhvD20CVJlUZ/bbXXYfX/TJEt7FtYGKBAqsbvIDL
1prvyJv7YHK+oiKKZ8Uy6Ikrp+lBr4kaAUbmyO7VRgooZBhb9gnKQhKJNxT4plWX+6YC2Xam3Dfx
RPmIbacOosu9GJJSu2PJgaaFKGoDuv4BWHb20tyUIvWyXgUD1Rabp6RP4gsngmWfaUkBpltTlcLr
pjwmLdYYHbcKTMv0HvjQROcW71P3Xnr0BBgDj8bkWJp3UXxFtmmYQAILAGyCeft0i57Nlc3HNp4h
q5YsBCxMz5dALwLvcQBVXVlpdESPJnGgaa5bXyT3YGap/s2RWarjKIqNzKg9hW0604ZtOdcG2pDp
N/H8d/P3qqcm8ITLsdWftGHrToN0cG2roA1oqw20yd6SAgVc21rThm061zarQBaJ/gLzpQpttkkL
3Ony1lWnTHbGxlOXVgThon9EcGV62BY9qKIfRnAkhpgs6R685tozvgyZOoSrvr6j7gufuGJiTRBD
Uhec3fQjuNUhKFt3M5ApOjyUKVHCZ7mwde0JuMjJMV3WDuOQObs1S0J40i7/hD14UmX3URB7uli9
edbB1mGwcEFsWYHIaHSKySE+eePI5egwtlweySea8Job/ov3a1cF4ZLaUC7RAu8DeWtCWLLjo3O1
ZGcIyGx4a0aJ9tlAupg0ZzadvBzgkiGzX2vdicpRfTqMKb1N3ADj2iATwpJeBM1Hh6JPxiqwmgvn
Sz92Z2l/8GEovhvO0340nCFZF4iLXvRIr4+fvksZPwuILwiD6egFYLetCeIqC/3Yap1rekOHK0PX
EsGTs7n1X9gx2P6HP5n4owIPMHN0WsHj+HWT2cJLEWDcshjL/lBwweeBYWxpmi9Hp3HLlP/qw1Te
DuBoqycU6PeFsSTboWsCSnSWPG0Ej8gbylWf9mISN13psl99WBrl+FxVIo7bMdAOhJhs1XKfTOlD
V1xtiM1XJRarWr3RJbvJJ274j+LJqW4szddDGYrLz5Tqp6JLj8fpK995UTIU7aN4shKUZJMRdGGZ
HNyDon3S0mJ0CeMTV8F8+tsqK86M/jEaHVwy5L+s/lAfglJsAtqXi8OWaqMPTfI5SuqX+CJF7FCG
rLcjpD+mlujfmFWmL0LRfvGn1ovNE//8Q9o4lNwvfri89aUDF8aiqH24pYvbF5Xr3FHUJsvKgIkO
tEZ6LoBt55Im7Jw3+2P92cuxAUxLW9AmqypbhntQG/6Bov3inSJ4QHqJzHYcMI+mFapWosv94rZT
2Nv93BfxwGSOL1RuQNF+8d9Zd6Nz9YVsN5edIqtGpTUCcJnda2PMPUN8jyA67Jro4/Daj6EJ5LCt
pqOnkTmKKytG2WwSlSV/d8Px5k7bziaOdGUHCtokilaTgr3ziRGuuoLowlPkgqdb4YCBCT1nDqbc
fbP0vcbxY7Ml21G0b3CRyMkdV7ahqE0cN599iK0/ZfTcWnUHmt2OacIehaszjuDIGlH2PsGBbehP
l9j2e0Hm7tfHhyGr1RYxXOlxcs0KXaVg/TArW5owhifZSlatuXrhb7KgI6G4x5aHFSgZ0mYU7Bu/
jPr5TxcSZShqlZffU3mTvly4mIH+EVBISnLNIzhwU3Y2PDQtgsiCkdcDqPXketYW7pyLX6Bg34zN
klPHcOVdm7hWCGaIDUgrpgLCnrr5vPGVQ5oRC/c3TyHTzAWHA3PiOePsXInNXRMnuu4mCtogqYb6
1yMXV6BYLxYeMMR2timygEBAIcJZXcOBX7rwtsk7Dqp9E1gygnbpkfjpD+iyVTyY+jso2DdBmQ25
S9+/1Gfh/HHpl52F696uKro8wtnyK14Wq/0kIBtPlCd/oOu9vYawq3BYcg31lSMGq4VbdKBpHrmC
7144oLkZxcTxl4urKHGg4ffaBugmE/IkI9CjekNvsr4d2p1ZpapMD5rY6rYwZaf4rkXBgNactteR
jnz3lLpWsirR/NrligBCVj1IB/NvfLY0k3xYD3yzWvq3ZsZmKV4axZWUo2gnL+xWbyCrEBXK3N7G
Zyt2bD7dMh42emsOxM78qOdiSQLj6tNXLGw+uHfphUsMKNo3a459NsqPLrqAoiRrdUZnLLmuw/TH
QFAvpaTWkbvZzomg0cPxDv45vN6zcN0LCF5gNEtyknwwYtsxwi2CZ6dl4oZrLA6UTMhSlJBDAvxz
WD1oiIjLks2fyqpdgq07SToQ+9ScWczaA9WPi653dqCEfU2TorNk76CobZyois7tJ/hWDonV32Gb
ziJHDhDw65FSSw6sLms+acPWnmzDNpwB12raQMFMDpu+HDnJ4HpSdZt3So2U/ANACEeyfv2Jz+3z
qA9nSYhXjzQnoOgfTgBTXo2C/bPwYONEf5rwIopiMbkqtRNd0TG1UH06trT5yEiuVOHP093HRUan
UC5xKyBTXDo2T9MEmraDD0N+P4Au/uTpUnXy87sNoHPJW4bxdDdeLNWH+OAKYmqhpiQ6SzkL21w1
Hj0emGgD3LpzSpc8erm46wyQ+04hebYhNk952Jcu0o3OUd+HHidvasMRmA7+WLdgn3pSGFv0NowD
HEbxVeTxBbi4Hs2Ts32oopq4POWeWSW6jWA8JQdkXPTZ0GFMhQiG7WbObv3quHx1LopiI1iSn+Dv
7P3N++BvdJaCKK6qokTlqGuHM6UfR+eqKuARhxCeWh/OlZ+JKTK8GVdAPBXFJ7TBbFnzsjL18JE8
2Tl4byhXfXkobvLHBbOlDZPyiYE3IdcM6b9WfdT7nMBgsaxS5+NO7eNwTX/Ao1KOGUT7gooL/42S
Bo3F+w0vD8lQPnrxiC4cJQ0cOJSEsGTFFFz9S0w2UZxfc8Ni438grP3gs5hRXOnHbgzlvWCeei9+
5lu7lp52MalI+44vS613S5f+7sNUKUbxFDnjconXpxWr5oTi9aTjEPx5fHyRclYoR7JwJFeeFsrX
n/Jlyn9wY6ru+jCk55/bpVmMHvfHsvqTS5Nj85VbhmZKdoVzpAJ4JCyEKRXEZCuPzSjSsN967/K8
qtaOPk/9/Yf/8EcATLVlZSLPZ4t04VPzFLCJLhifp04O5hFloXyNwI9JNAXg0u+ddop+dM8QtVN2
AklraHdNE1mIyw5Ru1s6vC6+70WX/ujHkLcOYxMNQVxteVyhmhaTLV0UxlXEgr4ZtvaMff6nfyto
NV9OnFigyh/GEN90xtUPHfCmR+6Z8o5huOSrSD5x9Nk92sQVJz+bRe5ErNU5g6nVCUwUjiZPsRE6
LoDA355COjUcMBwfgsG8cFtvbblzVp3OZ9GhC0/N3qvbNCZL8fEwXPQPFxrRjuG6R5QM2T0wsQpf
PKBf0dP2/lNZeuyGGzyT406Vyyk04l++uPyfI3O0B+Gm7IQc7ehcgcEDZR0wrx/SPvW3/drnUXTg
gJa+/IPPhsbkqaZMKNBkDGUSWjdQgR4M1fWoHOWeNz+81P/G7mDzXx80+0VnqY670JT3I/jKs6+/
f8X6ke3HZE6xaopXsuCW41aBMT5Lkg69XujSEwM9tvEFmlRvjrrZnS7/18xderPt+ccyPp/w92XI
RV406c+JJ67adSJoILxYolhOgf5G6GIBi7shyTUPI9lyXgJuHDTlkYAuH5uvSndOlz0YnaVgwhaK
rgw+cF86jC2TA+Pn+pzdmoHtGthBHF++xiVZcI90aMDVL1zdJtcYHVLrO+IKlBZL8MEAOopjc4lc
R6rs1+gcOXTND77ycFBDoRx5rlO6/N6EHMLuzQV7mV2oZDhtqWk3eYGAwqDX0ew2IF0H9cbhbKL+
5YONj/GpQt9AB01klvKMF112MwCvtWsPbUA8U6hc4kyXfxPBlZ6Dh/FQ8qAwjtuQ5LAVKAc6vkk/
SzelWfhZ6o2BbIV0c9XAt1JtEZ+rGuWRKf/y2b2Gv6KkwSECF7kGZDY0e2TUPwQzVZ8e08fhRodp
g2j5YcNKZ+RWJpVG+h17Ks4kXlTpz0mnrwzaUAFXvqEcWX4ET3oRtMDB20Cff7DpbWc6YQxmKyw/
o3oC1rynCgtMOnWFsvb43Sm4YClMm5gje42SJv7esov2VhwUT7rkqxkl2mnkwwaBSQXEi64MzZ05
pZolKOnJCecSErDM/21SgYp8wSflmRzhaP/U8wby2P67x43O6061P5vdkAKvLSpXzvLIkNy11eLM
4kkV//zKoQuzyYc+IdDEcmQZvg5lifegpCcjo7ot2CVd9JsvTXx1bKH8ibvHyo9aR/vvENwlndrQ
d7wJyqdGbOOnHaNx4V5cdN31taOGQD+a6AJQnGnf1poCUbrDTtFD0of8hOYEnCQo6ZJWD6pIjZ/q
8lE/Nn/Zq53nyrn4KIAmPhScpxuKkh+LmUXqRZQdwrsmBzwUMIOat8USzxkdEqvbxzDFp2Delcea
QoNxcUtny7MmsEuD2dclVfAQrIUH9tmQFTwyhNW+vObWJQM9oGGNhD36bRhN/yCSL0taeox47CN9
z5c1vkIqjRz44QsjgUokFQmVCLeTq4zDMoSXVlU0jYQ+qehsxQkHclMK3dd5P5h5yRPJpnuHgGfF
chr2kZ+2PiZxuUSlA8NwzSWt1ur3hgPCM6OBSuEYHszZrV/R/Sshe4Fe66klmu2OO8W9ullXqwGC
WqFLivDBwj2qRHQ7llBx3TWSLdnlsK3mkUlJXcoiWyvcZIPdfeOnxiEbzrSHZ1QfXoQ/pnckpXYH
3EtdUmHP6el+cEqpzYTfU8IN4b6OgPcJGDfG8OS4U2rd7xbjU08FohbkvrP+p5f3aC0/2kUklCiZ
2DawFNsCt0KBbAbKgye4N8EDD0Bx68E4+e5JKB0jU84qwZg14Eo2K67Xh8OPRVIN1YPfPGDFwU2a
cbmKCse0ht4K6y6k4mqNlLSGBwkltr+Ogq13WaXRsT+BB4JI99MAASuicgpDf+vZUuXje2XMBDLE
VEdO04OpBaoV4/FKuxT3aqk6IIglO42l9j6PYyFIaX5UyfWUc5eAnS1ymsgT811TBXcp2wR3KMm1
dygpdXcoqUi219+hpPUQmLYdXYd5U8A98L5kwZ1QqpB46z1tMCpWvwxjiD9y5hi+GJ1l5TPmgTKz
WLPagdb4cHKBkmmPMxBa4eFZhAQuj/pUFlIYFO9MueG5osYo0EYdYrgNOU6JVWC9ipZenasIJOZ7
ofR8HhRzPjhxoDHQZ7vg/luHLjyFimcTYMNdGpIua44psF/ZffL8XsN0Cl3VHsWVi2bv0vR9BgSw
+n19RAAVzJzk8YxuL9RdSIXBF6w1hjMknyehTxjiOMKzDonnOsjBvtM7gtas8B54b/cKsCbdnw+V
BxW/pdrour3+djxfMpcsZB/ApZYrnfjCiyYSgrXwk+8wHTXc8gDG6A9eDMWtuDzVFJTci4Qy9SSf
nQ2tJvMCFJo0F+ALoJcgX8T0MkO2VneEs6Sn4XdusFuHpteehUeb4MzYS3HmUx/2KA6KOa+F8qqM
rql1vyw92NjnymdBhXr4EFbzzTCOjIuSnpwxWYoSR5qyY1Khxuqhno2VhlkeqfU/ki9sfmn4vVKn
mGqeFBAfxRKf3vyeyjvlqMEjPOWcGHsXLL3WAcXBT4zhLEk+A740EFIRSCnbQfe3R0gFIiVCBSLl
wXM/cVwJjZw8ehCbq1joSlfeXXxIZ7NlDogdp66M9KCJfxrKkl0/822PbWQ4e00r9sb+ftSjSwRW
xJQ+ZnNVVzcIy3fDlpV5Wt7bx/0p3X7tkZ73m2X1aS9yg6gb0D4NYUtLI/nSJmgNoOQnB07xM0s0
HAqN+GlCHpGHkjshTx2mCN8GLeQtLK32dSypbqU/TbQUmg/jsyTTsGThcmyLYNWQ1LqVz5RpX6kU
3e78ZnUJWN5E8hQ7gjiS/GnFmq3smmsW4+i8Xao5EVw5azRPkfPSgQtL8gnTB0lwbfnq4StTxuYo
mCPY0rwJecoNvJOXO7+896DXz/FMF/6V/DATEJbf4u+YVr+UVFwPJucqItwz5a0v7L/4BkoaPFaB
xTco4AkXmuzHGVbOCsDBFdhBW+EBncX7m4NgHNp9sXm6w27povdf2KuNA+ve6WFs+YVghvQE3AEb
kyU/6c9RyxYebgqFz5i7v2maJ674cnwWkfjaiQthPgzZleFM6QHzy0dnK9+A5xdA0CGcrzwclaOp
ufKd6TOLl/Zqn3LHibZInmQ9jPvSRYJApkzinSn5dXqpZu2EPM0zjttrH5oP/HRnbI5yvxtd3ObG
IchyDDr4GZ17GFt6xReXtz7V43NIyJgsYqsbUFwsV0l+vQUVN6FQd9ghRbgXW3vGfXGFboYHVfJt
BEeaW1lZ6Tg5X/mhPy43zNqlj59WrPKeXqRd4MNW33y6UPnumx9+EeSPy5rDucTp6DzR0ImFTb6x
xfrNk8oMW+DwEJGrLRvGUlQ9XaYePiVL5zO/vHG+F0N+bzRfRrr1PWkiAZZc9xaoQJcRXEISzFZ8
4Zgq+LWn4hJ2N65xTpf9HsIQ43B/FyUPPvP2E/7DOAqRF012f/kHlgfp4AeFG05eHQe7NkrCcPnX
IRsrrw9HUYxfdzWSKvhqFNzDgHEws/rE5KmjndPEcZPziTEg3jmGwkPt8bmGQGBMT4jmSuNBqwru
GtjhXoHO56l8Is6XJo6bVqIa1f0LkpTjlyI6XexAIatBfN2x1vHd19szS3SvutJlPwaxxDWDegSo
L6AhPDaH2OWSIXsQxiGObjzWMgZd+j8BHEqispXZ7jTJrzG5qt2iQdy3tYuFcGM6W8nzYGmue+LK
36LydKJZZdrl8LtvuIJA2f5UynXfusOtzEkF6lWRuRqBWybxkwtDfTOKr3x/5Z+xm2+NNScvR8fl
Kkr8cOn3DnTtI2eqsj2QKf4ajDvFSyq0C7n1/wwynxWBnhN02+NiOkcCnwWeuenI5YC/f9j8l4mF
au5ovqLJkyZ95EDTtDszG38PYkm/nVyo5Z68eLvXuPxvSa7glsf0QuXI2WXq58bmqNaNzFEfDOUp
tX4M6W33DMnvrnT5Q1D7P7uwDG2uTP0/HTI1nzsxG6/58A2d4sxqvDaEob3mwtR+C/O5MPV3KDTF
Azeq7JFnhuiBB112LYSvEo/K1e6LLVRviS9QzI3MFkfBsRhuOKOi/P8GGp9Q4KRgFnMayvJvBIb9
D7VnSuU50Q+AAAAAAElFTkSuQmCCUEsBAi0AFAAGAAgAAAAhAFqYrcIMAQAAGAIAABMAAAAAAAAA
AAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAUAAYACAAAACEACMMYpNQAAACTAQAA
CwAAAAAAAAAAAAAAAAA9AQAAX3JlbHMvLnJlbHNQSwECLQAUAAYACAAAACEA9J5S4WECAACfBQAA
EgAAAAAAAAAAAAAAAAA6AgAAZHJzL3BpY3R1cmV4bWwueG1sUEsBAi0AFAAGAAgAAAAhAKomDr68
AAAAIQEAAB0AAAAAAAAAAAAAAAAAywQAAGRycy9fcmVscy9waWN0dXJleG1sLnhtbC5yZWxzUEsB
Ai0AFAAGAAgAAAAhAF0/EGENAQAAfwEAAA8AAAAAAAAAAAAAAAAAwgUAAGRycy9kb3ducmV2Lnht
bFBLAQItAAoAAAAAAAAAIQCNDDZnhhoAAIYaAAAUAAAAAAAAAAAAAAAAAPwGAABkcnMvbWVkaWEv
aW1hZ2UxLnBuZ1BLBQYAAAAABgAGAIQBAAC0IQAAAAA=
">
   <v:imagedata src="Report%20(Autosaved)%20(Autosaved)_files/Report%20(Autosaved)%20(Autosaved)_29058_image001.png"
    o:title=""/>
   <x:ClientData ObjectType="Pict">
    <x:SizeWithCells/>
    <x:CF>Bitmap</x:CF>
    <x:AutoPict/>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:12px;margin-top:0px;width:58px;
  height:48px'><img width=58 height=48
  src="{{Config::get('constants.path.img')}}/logoreport.png"
  v:shapes="Picture_x0020_1"></span><![endif]><span style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td height=25 class=xl6729058 width=20 style='height:18.75pt;width:15pt'></td>
   </tr>
  </table>
  </span></td>
  <td colspan=5 class=xl7229058 width=914 style='width:686pt'><span
  style='mso-spacerun:yes'>         </span>PT. TROCON INDAH PERKASA</td>
  <td class=xl7029058 width=64 style='width:48pt'></td>
  <td class=xl7029058 width=64 style='width:48pt'></td>
 </tr>
 <tr class=xl6829058 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6829058 width=20 style='height:17.25pt;width:15pt'></td>
  <td colspan=5 class=xl7329058 width=914 style='width:686pt'><span
  style='mso-spacerun:yes'>           </span>LAPORAN SISA REIMBURSE</td>
  <td class=xl6929058 width=64 style='width:48pt'></td>
  <td class=xl6929058 width=64 style='width:48pt'></td>
 </tr>
 <tr class=xl6629058 height=4 style='mso-height-source:userset;height:3.0pt'>
  <td height=4 class=xl6629058 width=20 style='height:3.0pt;width:15pt'></td>
  <td class=xl6629058 width=356 style='width:267pt'></td>
  <td colspan=4 class=xl7429058 width=558 style='width:419pt'></td>
  <td class=xl6629058 width=64 style='width:48pt'></td>
  <td class=xl6629058 width=64 style='width:48pt'></td>
 </tr>
 <tr class=xl6629058 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6629058 width=20 style='height:17.25pt;width:15pt'></td>
  <td class=xl8129058 width=356 style='width:267pt'>TAHUN</td>
  <td colspan=4 class=xl7529058 width=558 style='width:419pt'>: {{ $datafilter->year }}</td>
  <td class=xl6629058 width=64 style='width:48pt'></td>
  <td class=xl6629058 width=64 style='width:48pt'></td>
 </tr>
 <tr class=xl6629058 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6629058 width=20 style='height:17.25pt;width:15pt'></td>
  <td class=xl8229058 width=356 style='width:267pt'>DEPARTEMEN</td>
  <td colspan=4 class=xl7629058 width=558 style='width:419pt'>: {{ $datafilter->departmentname }}</td>
  <td class=xl6629058 width=64 style='width:48pt'></td>
  <td class=xl6629058 width=64 style='width:48pt'></td>
 </tr>
 <tr height=28 style='mso-height-source:userset;height:21.0pt'>
  <td height=28 class=xl1529058 style='height:21.0pt'></td>
  <td class=xl6429058 style='border-top:none'>Nama Karyawan</td>
  <td class=xl6429058 style='border-top:none;border-left:none'>Tanggal Masuk</td>
  <td class=xl6429058 style='border-top:none;border-left:none'>Plafond</td>
  <td class=xl6429058 style='border-top:none;border-left:none'>Total Klaim</td>
  <td class=xl6429058 style='border-top:none;border-left:none'>Sisa</td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
 </tr>
 @php
   $t_total = 0; 

   $n = 1;
   $t_plafond = 0;
   $t_claimamount = 0;
   $t_remain = 0;
 @endphp
 @foreach($itemdata as $itemdatas)
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1529058 style='height:15.0pt'></td>
  <td class=xl6329058 style='border-top:none'>&nbsp;{{ $itemdatas->employeename }}</td>
  <td class=xl6329058 style='border-top:none;border-left:none'>&nbsp;{{ date("d-m-Y", strtotime($itemdatas->joindate)) }}</td>
  <td class=xl6529058 style='border-top:none;border-left:none'>{{ number_format($itemdatas->plafond,0) }}&nbsp;</td>
  <td class=xl6529058 style='border-top:none;border-left:none'>{{ number_format($itemdatas->claimamount,0) }}&nbsp;</td>
  <td class=xl6529058 style='border-top:none;border-left:none'>{{ number_format($itemdatas->remain,0) }}&nbsp;</td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
 </tr>
 @php 
   $t_plafond += $itemdatas->plafond; 
   $t_claimamount += $itemdatas->claimamount; 
   $t_remain += $itemdatas->remain; 
 @endphp
 @endforeach
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1529058 style='height:15.0pt'></td>
  <td colspan=2 class=xl7729058 style='border-right:.5pt solid black'>TOTAL</td>
  <td class=xl7129058 align=right style='border-top:none;border-left:none'>@php echo number_format($t_plafond); @endphp</td>
  <td class=xl7129058 align=right style='border-top:none;border-left:none'>@php echo number_format($t_claimamount); @endphp</td>
  <td class=xl7129058 align=right style='border-top:none;border-left:none'>@php echo number_format($t_remain); @endphp</td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1529058 style='height:15.0pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1529058 style='height:15.0pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1529058 style='height:15.0pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl8029058 width=153 style='width:115pt'>JAKARTA,  @php echo date("d M Y"); @endphp</td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1529058 style='height:15.0pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl7929058 width=153 style='width:115pt'>DISETUJUI</td>
  <td class=xl8029058 width=153 style='width:115pt'>MENGETAHUI</td>
  <td class=xl8029058 width=153 style='width:115pt'>DIBUAT OLEH</td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1529058 style='height:15.0pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl8029058 width=153 style='width:115pt'></td>
  <td class=xl8029058 width=153 style='width:115pt'></td>
  <td class=xl8029058 width=153 style='width:115pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1529058 style='height:15.0pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl8029058 width=153 style='width:115pt'></td>
  <td class=xl8029058 width=153 style='width:115pt'></td>
  <td class=xl8029058 width=153 style='width:115pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1529058 style='height:15.0pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl8029058 width=153 style='width:115pt'></td>
  <td class=xl8029058 width=153 style='width:115pt'></td>
  <td class=xl8029058 width=153 style='width:115pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1529058 style='height:15.0pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl8029058 width=153 style='width:115pt'></td>
  <td class=xl8029058 width=153 style='width:115pt'></td>
  <td class=xl8029058 width=153 style='width:115pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1529058 style='height:15.0pt'></td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
  <td class=xl8029058 width=153 style='width:115pt'>(________________)</td>
  <td class=xl8029058 width=153 style='width:115pt'>(_________________)</td>
  <td class=xl8029058 width=153 style='width:115pt'>(____________)</td>
  <td class=xl1529058></td>
  <td class=xl1529058></td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=20 style='width:15pt'></td>
  <td width=356 style='width:267pt'></td>
  <td width=99 style='width:74pt'></td>
  <td width=153 style='width:115pt'></td>
  <td width=153 style='width:115pt'></td>
  <td width=153 style='width:115pt'></td>
  <td width=64 style='width:48pt'></td>
  <td width=64 style='width:48pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>

