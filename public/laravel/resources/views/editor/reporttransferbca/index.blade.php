@extends('layouts.editor.template')
@section('title', 'Laporan Transfer BCA')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    Report Transfer BCA
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">Report Transfer BCA</li>
  </ol>
</section>

<section class="content">
  <div class="row"> 
    {!! Form::model($datafilter)!!}
        {{ csrf_field() }}
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important"> 
            {{ Form::hidden('departmentid', $department_list, old('departmentid'), array('class' => 'form-control', 'placeholder' => 'Select Department', 'id' => 'departmentid', 'onchange' => 'RefreshData();')) }}  
          <div class="col-sm-3" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Period">
            {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodid', 'onchange' => 'RefreshData();')) }} 
          </div>  
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <button onClick="showreport()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-print"></i> Cetak</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
          {!! Form::close() !!}
        </div>  
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>  
                  <th>ID</th>
                  <th>Nama Karyawan</th> 
                  <th>Nama Akun</th> 
                  <th>Nama Bank</th> 
                  <th>No Rekening BCA</th> 
                  <th>Gaji Bersih</th>  
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  
@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "scrollX": true,
         "sScrollXInner": "100%",
         "order": [[ 0, "asc" ]],
         "autoWidth": true,
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/reporttransferbca/data') }}",
         columns: [   
         { data: 'employeeid', name: 'employeeid' }, 
         { data: 'employee_name', name: 'employee_name' }, 
         { data: 'account_name', name: 'account_name' },  
         { data: 'bank_name', name: 'bank_name' }, 
         { data: 'rek_no_bca', name: 'rek_no_bca' },  
         { data: 'gaji_bersih', name: 'gaji_bersih', render: $.fn.dataTable.render.number( ',', '.', 0) },    
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function RefreshData()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.periodfilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'departmentid': $('#departmentid').val(),    
            'periodid': $('#periodid').val()   
          }, 
          success: function(data) { 
            reload_table();
          }
        }) 
      }; 
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

 function showreport()
  {
   var url = '../editor/reporttransferbca/printreport';
     PopupCenter(url,'Popup_Window','1000','650');
  } 

  function PopupCenter(url, title, w, h) {  
    // Fixes dual-screen position                         Most browsers      Firefox  
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
          
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
          
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
    var top = ((height / 2) - (h / 2)) + dualScreenTop;  
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
    
    // Puts focus on the newWindow  
    if (window.focus) {  
      newWindow.focus();  
    }  
  } 
  </script> 
  @stop
