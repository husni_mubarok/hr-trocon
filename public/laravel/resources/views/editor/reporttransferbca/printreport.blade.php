@extends('layouts.editor.templatereport')
@section('content')
<style type="text/css">
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
 
</style> 


<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 15">
<link rel=File-List href="BCA_files/filelist.xml">
<!--[if !mso]>
<style>
v\:* {behavior:url(#default#VML);}
o\:* {behavior:url(#default#VML);}
x\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style>
<![endif]-->
<style id="Report (Autosaved)_24334_Styles">
<!--table
  {mso-displayed-decimal-separator:"\,";
  mso-displayed-thousand-separator:"\,";}
.xl1524334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6324334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6424334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  border:.5pt solid windowtext;
  background:#D6DCE4;
  mso-pattern:black none;
  white-space:nowrap;}
.xl6524334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl6624334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6724334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6824334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl6924334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7024334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:general;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7124334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:center;
  vertical-align:middle;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7224334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7324334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:16.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7424334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7524334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:10.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7624334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:13.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:"Times New Roman", serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:left;
  vertical-align:top;
  border-top:none;
  border-right:none;
  border-bottom:.5pt solid windowtext;
  border-left:none;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:normal;}
.xl7724334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:700;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  border:.5pt solid windowtext;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
.xl7824334
  {padding:0px;
  mso-ignore:padding;
  color:black;
  font-size:11.0pt;
  font-weight:400;
  font-style:normal;
  text-decoration:none;
  font-family:Calibri, sans-serif;
  mso-font-charset:0;
  mso-number-format:General;
  text-align:right;
  vertical-align:bottom;
  mso-background-source:auto;
  mso-pattern:auto;
  white-space:nowrap;}
-->
</style>
</head>

<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div id="Report (Autosaved)_24334" align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=918 style='border-collapse:
 collapse;table-layout:fixed;width:689pt'>
 <col width=13 style='mso-width-source:userset;mso-width-alt:475;width:10pt'>
 <col width=34 style='mso-width-source:userset;mso-width-alt:1243;width:26pt'>
 <col width=204 style='mso-width-source:userset;mso-width-alt:7460;width:153pt'>
 <col width=188 style='mso-width-source:userset;mso-width-alt:6875;width:141pt'>
 <col width=183 style='mso-width-source:userset;mso-width-alt:6692;width:137pt'>
 <col width=160 style='mso-width-source:userset;mso-width-alt:5851;width:120pt'>
 <col width=136 style='mso-width-source:userset;mso-width-alt:4973;width:102pt'>
 <tr class=xl6624334 height=7 style='mso-height-source:userset;height:5.25pt'>
  <td height=7 width=13 style='height:5.25pt;width:10pt' align=left valign=top><!--[if gte vml 1]><v:shapetype
   id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
   path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
   <v:stroke joinstyle="miter"/>
   <v:formulas>
    <v:f eqn="if lineDrawn pixelLineWidth 0"/>
    <v:f eqn="sum @0 1 0"/>
    <v:f eqn="sum 0 0 @1"/>
    <v:f eqn="prod @2 1 2"/>
    <v:f eqn="prod @3 21600 pixelWidth"/>
    <v:f eqn="prod @3 21600 pixelHeight"/>
    <v:f eqn="sum @0 0 1"/>
    <v:f eqn="prod @6 1 2"/>
    <v:f eqn="prod @7 21600 pixelWidth"/>
    <v:f eqn="sum @8 21600 0"/>
    <v:f eqn="prod @7 21600 pixelHeight"/>
    <v:f eqn="sum @10 21600 0"/>
   </v:formulas>
   <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
   <o:lock v:ext="edit" aspectratio="t"/>
  </v:shapetype><v:shape id="Picture_x0020_3" o:spid="_x0000_s10242" type="#_x0000_t75"
   style='position:absolute;margin-left:6.75pt;margin-top:2.25pt;width:43.5pt;
   height:33pt;z-index:1;visibility:visible' o:gfxdata="UEsDBBQABgAIAAAAIQBamK3CDAEAABgCAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRwU7DMAyG
70i8Q5QralM4IITW7kDhCBMaDxAlbhvROFGcle3tSdZNgokh7Rjb3+8vyWK5tSObIJBxWPPbsuIM
UDltsK/5x/qleOCMokQtR4dQ8x0QXzbXV4v1zgOxRCPVfIjRPwpBagArqXQeMHU6F6yM6Rh64aX6
lD2Iu6q6F8phBIxFzBm8WbTQyc0Y2fM2lWcTjz1nT/NcXlVzYzOf6+JPIsBIJ4j0fjRKxnQ3MaE+
8SoOTmUi9zM0GE83SfzMhtz57fRzwYF7S48ZjAa2kiG+SpvMhQ4kvFFxEyBNlf/nZFFLhes6o6Bs
A61m8ih2boF2XxhgujS9Tdg7TMd0sf/X5hsAAP//AwBQSwMEFAAGAAgAAAAhAAjDGKTUAAAAkwEA
AAsAAABfcmVscy8ucmVsc6SQwWrDMAyG74O+g9F9cdrDGKNOb4NeSwu7GltJzGLLSG7avv1M2WAZ
ve2oX+j7xL/dXeOkZmQJlAysmxYUJkc+pMHA6fj+/ApKik3eTpTQwA0Fdt3qaXvAyZZ6JGPIoiol
iYGxlPymtbgRo5WGMqa66YmjLXXkQWfrPu2AetO2L5p/M6BbMNXeG+C934A63nI1/2HH4JiE+tI4
ipr6PrhHVO3pkg44V4rlAYsBz3IPGeemPgf6sXf9T28OrpwZP6phof7Oq/nHrhdVdl8AAAD//wMA
UEsDBBQABgAIAAAAIQA8U0mbbgIAAKYFAAASAAAAZHJzL3BpY3R1cmV4bWwueG1srFTbjtMwEH1H
4h8sv7dx0oSmUdNVaXcR0goqBB/gdZzGIrEj272sEP/O2E5alX0AsbxNPJ45x3POZHl37lp05NoI
JUscTwlGXDJVCbkv8bevD5McI2OprGirJC/xMzf4bvX2zfJc6YJK1iiNoIU0BRyUuLG2L6LIsIZ3
1ExVzyVka6U7auFT76NK0xM079ooIeRdZHrNaWUazu02ZPDK97YnteFtuw4QvBJ2bUoMHNzpcKfW
qgu3mWpXZBk5Ui70HSD4XNerPJsn2SXlTnxWq9NY4cLxzOUTKBkqIOUrfOcrnFUXiFVy6X05cyXx
gmTkSukGdyj5HTdOSU7y0O4GeITrBQsY8rgTbKcHwE/HnUaiKnGKkaQdqARZe9AczXB0vRMqaAFd
HhX7bgbd6D+o1lEhAUttGir3fG16ziy4x6EFDYBSgPOfN3SfWtE/iBZEooWLX00j2O+vzKfqWjC+
VezQcWmDAzVvqQX3m0b0BiNd8O6JwzD1xyrGiIH5LUy010JasB0t+Nk+GjtE6KBFiX8k+ZqQRfJ+
ssnIZpKS+f1kvUjnkzm5n6ckzeNNvPnpquO0OBgO46ftthfj0+P0hQadYFoZVdspU10UeI+7A7xj
EgUNjrQtMfGD99RAgCtFCN2EHVdjNbesGRFf4P15Uz2ea1WDeF9AcCf2pfEg/FVct4qmdx6lxbnW
3f9AhjGgc4n9RmP0XGK/qe7x/s2IQTLLkjSDvxiDdLqYJTO/A0DUkXAXe23sB65eTQi5RmATmIT3
BT2CLcJMRohhKGEMfhNg94aFbAU4cEstHXfm5oc3VIYf7OoXAAAA//8DAFBLAwQUAAYACAAAACEA
qiYOvrwAAAAhAQAAHQAAAGRycy9fcmVscy9waWN0dXJleG1sLnhtbC5yZWxzhI9BasMwEEX3hdxB
zD6WnUUoxbI3oeBtSA4wSGNZxBoJSS317SPIJoFAl/M//z2mH//8Kn4pZRdYQde0IIh1MI6tguvl
e/8JIhdkg2tgUrBRhnHYffRnWrHUUV5czKJSOCtYSolfUma9kMfchEhcmzkkj6WeycqI+oaW5KFt
jzI9M2B4YYrJKEiT6UBctljN/7PDPDtNp6B/PHF5o5DOV3cFYrJUFHgyDh9h10S2IIdevjw23AEA
AP//AwBQSwMEFAAGAAgAAAAhABI5Gi0TAQAAhwEAAA8AAABkcnMvZG93bnJldi54bWxcUMtOwzAQ
vCPxD9YicaNOoqSPUKcqBSQuIKUUxNFKnIfwI7JNk/D1OGlREKfVzHpmZ7zedIKjI9OmVpKAP/MA
MZmpvJYlgcPr480SkLFU5pQryQj0zMAmubxY0zhXrUzZcW9L5EykiSmBytomxthkFRPUzFTDpNsV
SgtqHdQlzjVtnbngOPC8ORa0lu5CRRu2q1j2uf8SBHaL9+Yj5/N+9RBGz/dp39wd3hpCrq+67S0g
yzo7PT6rn3ICIQxVXA1IXL6Ob2VWKY2KlJn624U/8YVWAmnVEnBlM8XH6fBLURhmCQTLaBGNm1/G
EUEEeDC16iQNzlI3/0j9MFj+0/orL/K8QYynSCOY/i/5AQAA//8DAFBLAwQKAAAAAAAAACEAjQw2
Z4YaAACGGgAAFAAAAGRycy9tZWRpYS9pbWFnZTEucG5niVBORw0KGgoAAAANSUhEUgAAAE4AAABD
CAYAAAFFMwNVAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAIdUAACHVAQSc
tJ0AABobSURBVGhD7ZsJWFPHvsAPsoR9VZBFUBREBdRqXVot1fbaura9V9v6etVXa11RFBCFJJzs
Yd9dcEPtKnWrCxAgZD/ZISJqxba22l6vtNXettZqhbyZkwkQCCEofb3vfff3ff8vM3PmnMz5z/af
/8zBBownrvoNBe3jb/v18VhKrRFFsYS9mr+jIIYFMMS3URDD1p02Yql1xp3nvvZDKV2485sfwF9f
XPEZliwwvnxAMxnb+GnnUztxYV98RAbAkyqNRkeYGf59BEP4HpluZszmVgoK9mJiocgXBW0zd692
DgpaMpIr44zLUdSi6CCwtlznjIKYI1XzOwqaeOOIIRoFMWxrdW+19GQcW0KbmKPkQjWhJMxoNA5B
wW6ADEcNtzywLVXGmbnC52DSK4caF5DXzDjvqH+IJYM6BpnIJ0KlAzZXtQ4jMwyIGUXq/0JB+zjW
8qO/L71BEMFSvImS+sc/U3TMkyZ+5MtQVUwpUKXNKFGlBXE0+zwyxA9COLKjKJtt3DMk91DQki3V
Rm+a5CsUs8CTLr2GgpYknbs6DgUtmJdf409qF2o2Tdx/5ZpxYF6wbCYQWDVJ1cbFZerXyYeS8Zpe
Dx1XIJuAgl248y6QvbATWCIo2+s7JuySjIjbpwrrLiiXicSaGBTqIpAlFqMgeH/QbkHbdUkRfu+y
/vRdsjHBgcP8JzAM0/oD9L1d8DcSVwS67BTenr9PNxcHbds5rZ5NZkCA9u7gz9FeheEAuuQWmdgX
U4s0T/ux1HoUtQp4oLM3W9Mykq8g+8n/LrN3aed5MbXfeDJVem+6iPYMaNA+9IY0H6ZG6cbSf51Q
1jQJZe2b2HzVx+tOXRmJop2MYtb3arQLKi6ueG6XLglF7WPhHiKUrGkrvH7UEIuClvjSRNYVD5sD
aMCUdNEvKMWCYWz5aBTsIpAlvYKCnSTwhcuC6eJqc3tLqBC5okudeNHEd1Gwi3G50uko2MWG08b5
pURcV+Ot7/W6jpnElyjYxTv7W/xRkGQES36SHM03nzcuKNevIPvrNoExtbJlOMpC4kMXfoSCXeCV
LS4oaAJ1IQc0NpO623zOiK09YVm6ZEEaCnWxrNvDPOjSrv4I6Nnp4/jnOufvKYWqYyjYxdOl6gAU
7CzVEKAj79Sab8jXNesN/nZrKq4Z4l4Vh8UVqF6Bv0OSBO3YVjCOmW/YAIwD+DCyApAA3Q2n1n4D
LztzLnRZHmbcqQ0PUXBAROUoC1Cwi7m79eRsNJavSIKVEcAkPh9GF+lfO/p5oCeuvAGvTS7S5/hS
G65S0hreGbK9nrSsJhY2WTc+hrPlKhS0ixF84jwKWseLLm97YbfmRRS1ysR85dt+LMXPKGofc/c0
vRaXRwh8aA0CP5qQu/Ijbe9+OJhUtbZSOOeuT3LfUTUZQ7LiyKXJFdeNvfrxoDO5VB8Sxledd2Wo
/xXOkVcu/6B5Jro0IGaX6Z8L5BACt0zit7gCNW9VxfXHL/ykAvWacB5Rg6L9EoELz2Bbzhvn7VFN
REk2EYmMTn5MxdX1JwyBKMk+Fu67MDuYJT2Mov0SlikykB0ZjltgAPxLeeN8dKlfPGmSX8aXiTxR
1DZTgIHtQpX8iqL9svxQYxQ52sCCmYcsIM/t0iejLDZJAP08OltRiqK2iclRFY3LVdBQ1BYO2NqT
v0emV2twUYsnloLG0W7D4Nhs5QGU1yZOzCbbNpEZF6qsYxxbEYGiVtl27IYbthVoCrQxLPGs0Xu7
4JtllZWOjtsb7lkM+EAC6CIFuq1PAmgNl6bvbw5CURswGx9g3dZOPSnXgWtwhjYbw9tAlSZVGf22
112H1/0yRLexbWBigQKrG7yAy9aa78ib+2ByvqIiimfFMuiJK6fpQa+JGgFG5sju1UYKKGQYW/YJ
ykISiTcU+KZVl/umAtl2ptw38UT5iG2nDqLLvRiSUrtjyYGmhShqA7r+AVh29tLclCL1sl4FA9UW
m6ekT+ILJ4Jln2lJAaZbU5XC66Y8Ji3WGB23CkzL9B740ETnFu9T91569AQYA4/G5Fiad1F8RbZp
mEACCwBsgnn7dIuezZXNxzaeIauWLAQsTM+XQC8C73EAVV1ZaXREjyZxoGmuW18k92Bmqf7NkVmq
4yiKjcyoPYVtOtOGbTnXBtqQ6Tfx/Hfz96qnJvCEy7HVn7Rh606DdHBtq6ANaKsNtMnekgIFXNta
04ZtOtc2q0AWif4C86UKbbZJC9zp8tZVp0x2xsZTl1YE4aJ/RHBletgWPaiiH0ZwJIaYLOkevOba
M74MmTqEq76+o+4Ln7hiYk0QQ1IXnN30I7jVIShbdzOQKTo8lClRwme5sHXtCbjIyTFd1g7jkDm7
NUtCeNIu/4Q9eFJl91EQe7pYvXnWwdZhsHBBbFmByGh0iskhPnnjyOXoMLZcHsknmvCaG/6L92tX
BeGS2lAu0QLvA3lrQliy46NztWRnCMhseGtGifbZQLqYNGc2nbwc4JIhs19r3YnKUX06jCm9TdwA
49ogE8KSXgTNR4eiT8YqsJoL50s/dmdpf/BhKL4bztN+NJwhWReIi170SK+Pn75LGT8LiC8Ig+no
BWC3rQniKgv92Gqda3pDhytD1xLBk7O59V/YMdj+hz+Z+KMCDzBzdFrB4/h1k9nCSxFg3LIYy/5Q
cMHngWFsaZovR6dxy5T/6sNU3g7gaKsnFOj3hbEk26FrAkp0ljxtBI/IG8pVn/ZiEjdd6bJffVga
5fhcVSKO2zHQDoSYbNVyn0zpQ1dcbYjNVyUWq1q90SW7ySdu+I/iyaluLM3XQxmKy8+U6qeiS4/H
6SvfeVEyFO2jeLISlGSTEXRhmRzcg6J90tJidAnjE1fBfPrbKivOjP4xGh1cMuS/rP5QH4JSbALa
l4vDlmqjD03yOUrql/giRexQhqy3I6Q/ppbo35hVpi9C0X7xp9aLzRP//EPaOJTcL364vPWlAxfG
oqh9uKWL2xeV69xR1CbLyoCJDrRGei6AbeeSJuycN/tj/dnLsQFMS1vQJqsqW4Z7UBv+gaL94p0i
eEB6icx2HDCPphWqVqLL/eK2U9jb/dwX8cBkji9UbkDRfvHfWXejc/WFbDeXnSKrRqU1AnCZ3Wtj
zD1DfI8gOuya6OPw2o+hCeSwraajp5E5iisrRtlsEpUlf3fD8eZO284mjnRlBwraJIpWk4K984kR
rrqC6MJT5IKnW+GAgQk9Zw6m3H2z9L3G8WOzJdtRtG9wkcjJHVe2oahNHDeffYitP2X03Fp1B5rd
jmnCHoWrM47gyBpR9j7BgW3oT5fY9ntB5u7Xx4chq9UWMVzpcXLNCl2lYP0wK1uaMIYn2UpWrbl6
4W+yoCOhuMeWhxUoGdJmFOwbv4z6+U8XEmUoapWX31N5k75cuJiB/hFQSEpyzSM4cFN2Njw0LYLI
gpHXA6j15HrWFu6ci1+gYN+MzZJTx3DlXZu4VghmiA1IK6YCwp66+bzxlUOaEQv3N08h08wFhwNz
4jnj7FyJzV0TJ7ruJgraIKmG+tcjF1egWC8WHjDEdrYpsoBAQCHCWV3DgV+68LbJOw6qfRNYMoJ2
6ZH46Q/oslU8mPo7KNg3QZkNuUvfv9Rn4fxx6Zedheveriq6PMLZ8iteFqv9JCAbT5Qnf6Drvb2G
sKtwWHIN9ZUjBquFW3SgaR65gu9eOKC5GcXE8ZeLqyhxoOH32gboJhPyJCPQo3pDb7K+HdqdWaWq
TA+a2Oq2MGWn+K5FwYDWnLbXkY5895S6VrIq0fza5YoAQlY9SAfzb3y2NJN8WA98s1r6t2bGZile
GsWVlKNoJy/sVm8gqxAVytzexmcrdmw+3TIeNnprDsTO/KjnYkkC4+rTVyxsPrh36YVLDCjaN2uO
fTbKjy66gKIka3VGZyy5rsP0x0BQL6Wk1pG72c6JoNHD8Q7+Obzes3DdCwheYDRLcpJ8MGLbMcIt
gmenZeKGaywOlEzIUpSQQwL8c1g9aIiIy5LNn8qqXYKtO0k6EPvUnFnM2gPVj4uud3aghH1Nk6Kz
ZO+gqG2cqIrO7Sf4Vg6J1d9hm84iRw4Q8OuRUksOrC5rPmnD1p5swzacAddq2kDBTA6bvhw5yeB6
UnWbd0qNlPwDQAhHsn79ic/t86gPZ0mIV480J6DoH04AU16Ngv2z8GDjRH+a8CKKYjG5KrUTXdEx
tVB9Ora0+chIrlThz9Pdx0VGp1AucSsgU1w6Nk/TBJq2gw9Dfj+ALv7k6VJ18vO7DaBzyVuG8XQ3
XizVh/jgCmJqoaYkOks5C9tcNR49HphoA9y6c0qXPHq5uOsMkPtOIXm2ITZPediXLtKNzlHfhx4n
b2rDEZgO/li3YJ96Uhhb9DaMAxxG8VXk8QW4uB7Nk7N9qKKauDzlnlkluo1gPCUHZFz02dBhTIUI
hu1mzm796rh8dS6KYiNYkp/g7+z9zfvgb3SWgiiuqqJE5ahrhzOlH0fnqirgEYcQnlofzpWfiSky
vBlXQDwVxSe0wWxZ87Iy9fCRPNk5eG8oV315KG7yxwWzpQ2T8omBNyHXDOm/Vn3U+5zAYLGsUufj
Tu3jcE1/wKNSjhlE+4KKC/+NkgaNxfsNLw/JUD568YguHCUNHDiUhLBkxRRc/UtMNlGcX3PDYuN/
IKz94LOYUVzpx24M5b1gnnovfuZbu5aedjGpSPuOL0utd0uX/u7DVClG8RQ543KJ16cVq+aE4vWk
4xD8eXx8kXJWKEeycCRXnhbK15/yZcp/cGOq7vowpOef26VZjB73x7L6k0uTY/OVW4ZmSnaFc6QC
eCQshCkVxGQrj80o0rDfeu/yvKrWjj5P/f2H//BHAEy1ZWUiz2eLdOFT8xSwiS4Yn6dODuYRZaF8
jcCPSTQF4NLvnXaKfnTPELVTdgJJa2h3TRNZiMsOUbtbOrwuvu9Fl/7ox5C3DmMTDUFcbXlcoZoW
ky1dFMZVxIK+Gbb2jH3+p38raDVfTpxYoMofxhDfdMbVDx3wpkfumfKOYbjkq0g+cfTZPdrEFSc/
m0XuRKzVOYOp1QlMFI4mT7EROi6AwN+eQjo1HDAcH4LBvHBbb225c1adzmfRoQtPzd6r2zQmS/Hx
MFz0Dxca0Y7hukeUDNk9MLEKXzygX9HT9v5TWXrshhs8k+NOlcspNOJfvrj8nyNztAfhpuyEHO3o
XIHBA2UdMK8f0j71t/3a51F04ICWvvyDz4bG5KmmTCjQZAxlElo3UIEeDNX1qBzlnjc/vNT/xu5g
818fNPtFZ6mOu9CU9yP4yrOvv3/F+pHtx2ROsWqKV7LgluNWgTE+S5IOvV7o0hMDPbbxBZpUb466
2Z0u/9fMXXqz7fnHMj6f8PdlyEVeNOnPiSeu2nUiaCC8WKJYToH+RuhiAYu7Ick1DyPZcl4Cbhw0
5ZGALh+br0p3Tpc9GJ2lYMIWiq4MPnBfOowtkwPj5/qc3ZqB7RrYQRxfvsYlWXCPdGjA1S9c3SbX
GB1S6zviCpQWS/DBADqKY3OJXEeq7NfoHDl0zQ++8nBQQ6Ecea5TuvzehBzC7s0Fe5ldqGQ4balp
N3mBgMKg19HsNiBdB/XG4Wyi/uWDjY/xqULfQAdNZJbyjBdddjMAr7VrD21APFOoXOJMl38TwZWe
g4fxUPKgMI7bkOSwFSgHOr5JP0s3pVn4WeqNgWyFdHPVwLdSbRGfqxrlkSn/8tm9hr+ipMEhAhe5
BmQ2NHtk1D8EM1WfHtPH4UaHaYNo+WHDSmfkViaVRvodeyrOJF5U6c9Jp68M2lABV76hHFl+BE96
EbTAwdtAn3+w6W1nOmEMZissP6N6Ata8pwoLTDp1hbL2+N0puGApTJuYI3uNkib+3rKL9lYcFE+6
5KsZJdpp5MMGgUkFxIuuDM2dOaWaJSjpyQnnEhKwzP9tUoGKfMEn5Zkc4Wj/1PMG8tj+u8eNzutO
tT+b3ZACry0qV87yyJDctdXizOJJFf/8yqELs8mHPiHQxHJkGb4OZYn3oKQnI6O6LdglXfSbL018
dWyh/Im7x8qPWkf77xDcJZ3a0He8CcqnRmzjpx2jceFeXHTd9bWjhkA/mugCUJxp39aaAlG6w07R
Q9KH/ITmBJwkKOmSVg+qSI2f6vJRPzZ/2aud58q5+CiAJj4UnKcbipIfi5lF6kWUHcK7Jgc8FDCD
mrfFEs8ZHRKr28cwxadg3pXHmkKDcXFLZ8uzJrBLg9nXJVXwEKyFB/bZkBU8MoTVvrzm1iUDPaBh
jYQ9+m0YTf8gki9LWnqMeOwjfc+XNb5CKo0c+OELI4FKJBUJlQi3k6uMwzKEl1ZVNI2EPqnobMUJ
B3JTCt3XeT+YeckTyaZ7h4BnxXIa9pGftj4mcblEpQPDcM0lrdbq94YDwjOjgUrhGB7M2a1f0f0r
IXuBXuupJZrtjjvFvbpZV6sBglqhS4rwwcI9qkR0O5ZQcd01ki3Z5bCt5pFJSV3KIlsr3GSD3X3j
p8YhG860h2dUH16EP6Z3JKV2B9xLXVJhz+npfnBKqc2E31PCDeG+joD3CRg3xvDkuFNq3e8W41NP
BaIW5L6z/qeX92gtP9pFJJQomdg2sBTbArdCgWwGyoMnuDfBAw9AcevBOPnuSSgdI1POKsGYNeBK
Niuu14fDj0VSDdWD3zxgxcFNmnG5igrHtIbeCusupOJqjZS0hgcJJba/joKtd1ml0bE/gQeCSPfT
AAEronIKQ3/r2VLl43tlzAQyxFRHTtODqQWqFePxSrsU92qpOiCIJTuNpfY+j2MhSGl+VMn1lHOX
gJ0tcprIE/NdUwV3KdsEdyjJtXcoKXV3KKlIttffoaT1EJi2HV2HeVPAPfC+ZMGdUKqQeOs9bTAq
Vr8MY4g/cuYYvhidZeUz5oEys1iz2oHW+HBygZJpjzMQWuHhWYQELo/6VBZSGBTvTLnhuaLGKNBG
HWK4DTlOiVVgvYqWXp2rCCTme6H0fB4Ucz44caAx0Ge74P5bhy48hYpnE2DDXRqSLmuOKbBf2X3y
/F7DdApd1R7FlYtm79L0fQYEsPp9fUQAFcyc5PGMbi/UXUiFwResNYYzJJ8noU8Y4jjCsw6J5zrI
wb7TO4LWrPAeeG/3CrAm3Z8PlQcVv6Xa6Lq9/nY8XzKXLGQfwKWWK534wosmEoK18JPvMB013PIA
xugPXgzFrbg81RSU3IuEMvUkn50NrSbzAhSaNBfgC6CXIF/E9DJDtlZ3hLOkp+F3brBbh6bXnoVH
m+DM2Etx5lMf9igOijmvhfKqjK6pdb8sPdjY58pnQYV6+BBW880wjoyLkp6cMVmKEkeasmNSocbq
oZ6NlYZZHqn1P5IvbH5p+L1Sp5hqnhQQH8USn978nso75ajBIzzlnBh7Fyy91gHFwU+M4SxJPgO+
NBBSEUgp20H3t0dIBSIlQgUi5cFzP3FcCY2cPHoQm6tY6EpX3l18SGezZQ6IHaeujPSgiX8aypJd
P/Ntj21kOHtNK/bG/n7Uo0sEVsSUPmZzVVc3CMt3w5aVeVre28f9Kd1+7ZGe95tl9WkvcoOoG9A+
DWFLSyP50iZoDaDkJwdO8TNLNBwKjfhpQh6Rh5I7IU8dpgjfBi3kLSyt9nUsqW6lP020FJoP47Mk
07Bk4XJsi2DVkNS6lc+UaV+pFN3u/GZ1CVjeRPIUO4I4kvxpxZqt7JprFuPovF2qORFcOWs0T5Hz
0oELS/IJ0wdJcG356uErU8bmKJgj2NK8CXnKDbyTlzu/vPeg18/xTBf+lfwwExCW3+LvmFa/lFRc
DybnKiLcM+WtL+y/+AZKGjxWgcU3KOAJF5rsxxlWzgrAwRXYQVvhAZ3F+5uDYBzafbF5usNu6aL3
X9irjQPr3ulhbPmFYIb0BNwBG5MlP+nPUcsWHm4Khc+Yu79pmieu+HJ8FpH42okLYT4M2ZXhTOkB
88tHZyvfgOcXQNAhnK88HJWjqbnynekzi5f2ap9yx4m2SJ5kPYz70kWCQKZM4p0p+XV6qWbthDzN
M47bax+aD/x0Z2yOcr8bXdzmxiHIcgw6+BmdexhbesUXl7c+1eNzSMiYLGKrG1BcLFdJfr0FFTeh
UHfYIUW4F1t7xn1xhW6GB1XybQRHmltZWek4OV/5oT8uN8zapY+fVqzynl6kXeDDVt98ulD57psf
fhHkj8uaw7nE6eg80dCJhU2+scX6zZPKDFvg8BCRqy0bxlJUPV2mHj4lS+czv7xxvhdDfm80X0a6
9T1pIgGWXPcWqECXEVxCEsxWfOGYKvi1p+ISdjeucU6X/R7CEONwfxclDz7z9hP+wzgKkRdNdn/5
B5YH6eAHhRtOXh0HuzZKwnD51yEbK68PR1GMX3c1kir4ahTcw4BxMLP6xOSpo53TxHGT84kxIN45
hsJD7fG5hkBgTE+I5krjQasK7hrY4V6BzuepfCLOlyaOm1aiGtX9C5KU45ciOl3sQCGrQXzdsdbx
3dfbM0t0r7rSZT8GscQ1g3oEqC+gITw2h9jlkiF7EMYhjm481jIGXfo/ARxKorKV2e40ya8xuard
okHct7WLhXBjOlvJ82Bprnviyt+i8nSiWWXa5fC7b7iCQNn+VMp137rDrcxJBepVkbkagVsm8ZML
Q30ziq98f+WfsZtvjTUnL0fH5SpK/HDp9w507SNnqrI9kCn+Gow7xUsqtAu59f8MMp8VgZ4TdNvj
YjpHAp8FnrnpyOWAv3/Y/JeJhWruaL6iyZMmfeRA07Q7Mxt/D2JJv51cqOWevHi717j8b0mu4JbH
9ELlyNll6ufG5qjWjcxRHwzlKbV+DOlt9wzJ7650+UNQ+z+7sAxtrkz9Px0yNZ87MRuv+fANneLM
arw2hKG95sLUfgvzuTD1dyg0xQM3quyRZ4bogQdddi2ErxKPytXuiy1Ub4kvUMyNzBZHwbEYbjij
ovz/BhqfUOCkYBZzGsrybwSG/Q+1Z0rlOdEPgAAAAABJRU5ErkJgglBLAQItABQABgAIAAAAIQBa
mK3CDAEAABgCAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAG
AAgAAAAhAAjDGKTUAAAAkwEAAAsAAAAAAAAAAAAAAAAAPQEAAF9yZWxzLy5yZWxzUEsBAi0AFAAG
AAgAAAAhADxTSZtuAgAApgUAABIAAAAAAAAAAAAAAAAAOgIAAGRycy9waWN0dXJleG1sLnhtbFBL
AQItABQABgAIAAAAIQCqJg6+vAAAACEBAAAdAAAAAAAAAAAAAAAAANgEAABkcnMvX3JlbHMvcGlj
dHVyZXhtbC54bWwucmVsc1BLAQItABQABgAIAAAAIQASORotEwEAAIcBAAAPAAAAAAAAAAAAAAAA
AM8FAABkcnMvZG93bnJldi54bWxQSwECLQAKAAAAAAAAACEAjQw2Z4YaAACGGgAAFAAAAAAAAAAA
AAAAAAAPBwAAZHJzL21lZGlhL2ltYWdlMS5wbmdQSwUGAAAAAAYABgCEAQAAxyEAAAAA
">
   <v:imagedata src="BCA_files/Report%20(Autosaved)_24334_image001.png"
    o:title=""/>
   <x:ClientData ObjectType="Pict">
    <x:SizeWithCells/>
    <x:CF>Bitmap</x:CF>
    <x:AutoPict/>
   </x:ClientData>
  </v:shape><![endif]--><![if !vml]><span style='mso-ignore:vglayout;
  position:absolute;z-index:1;margin-left:9px;margin-top:3px;width:58px;
  height:44px'><img width=58 height=44
  src="{{Config::get('constants.path.img')}}/logoreport.png" v:shapes="Picture_x0020_3"></span><![endif]><span
  style='mso-ignore:vglayout2'>
  <table cellpadding=0 cellspacing=0>
   <tr>
    <td height=7 class=xl6624334 width=13 style='height:5.25pt;width:10pt'></td>
   </tr>
  </table>
  </span></td>
  <td class=xl6624334 width=34 style='width:26pt'></td>
  <td class=xl6624334 width=204 style='width:153pt'></td>
  <td class=xl6624334 width=188 style='width:141pt'></td>
  <td class=xl6624334 width=183 style='width:137pt'></td>
  <td class=xl6624334 width=160 style='width:120pt'></td>
  <td class=xl6624334 width=136 style='width:102pt'></td>
 </tr>
 <tr class=xl6724334 height=25 style='mso-height-source:userset;height:18.75pt'>
  <td height=25 class=xl6724334 width=13 style='height:18.75pt;width:10pt'></td>
  <td class=xl6724334 width=34 style='width:26pt'></td>
  <td colspan=4 class=xl7324334 width=735 style='width:551pt'><span
  style='mso-spacerun:yes'>     </span>PT. TROCON INDAH PERKASA</td>
  <td class=xl6724334 width=136 style='width:102pt'></td>
 </tr>
 <tr class=xl6824334 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6824334 width=13 style='height:17.25pt;width:10pt'></td>
  <td colspan=6 class=xl7624334 width=905 style='width:679pt'><span
  style='mso-spacerun:yes'>            </span>LAPORAN TRANSFER BCA</td>
 </tr>
 <tr class=xl6624334 height=4 style='mso-height-source:userset;height:3.0pt'>
  <td height=4 class=xl6624334 width=13 style='height:3.0pt;width:10pt'></td>
  <td class=xl6624334 width=34 style='width:26pt'></td>
  <td colspan=4 class=xl7224334 width=735 style='width:551pt'></td>
  <td class=xl6624334 width=136 style='width:102pt'></td>
 </tr>
 <tr class=xl6624334 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6624334 width=13 style='height:17.25pt;width:10pt'></td>
  <td colspan=2 class=xl7524334 width=238 style='width:179pt'>PERIODE</td>
  <td colspan=3 class=xl7524334 width=531 style='width:398pt'>: {{ $datafilter->period }}</td>
  <td class=xl6624334 width=136 style='width:102pt'></td>
 </tr>
 <tr class=xl6624334 height=23 style='mso-height-source:userset;height:17.25pt'>
  <td height=23 class=xl6624334 width=13 style='height:17.25pt;width:10pt'></td>
  <td colspan=2 class=xl7424334 width=238 style='width:179pt'>DEPARTEMENT</td>
  <td colspan=3 class=xl7424334 width=531 style='width:398pt'>: Proyek 1</td>
  <td class=xl6624334 width=136 style='width:102pt'></td>
 </tr>
 <tr height=28 style='mso-height-source:userset;height:21.0pt'>
  <td height=28 class=xl1524334 style='height:21.0pt'></td>
  <td class=xl6424334 style='border-top:none'>No</td>
  <td class=xl6424334 style='border-top:none;border-left:none'>Nama Karyawan</td>
  <td class=xl6424334 style='border-top:none;border-left:none'>Nama Akun</td>
  <td class=xl6424334 style='border-top:none;border-left:none'>Nama Bank</td>
  <td class=xl6424334 style='border-top:none;border-left:none'>No Rek BCA</td>
  <td class=xl6424334 style='border-left:none'>Gaji Bersih</td>
 </tr>
   @php
   $t_total = 0; 

   $n = 1;
 @endphp
 @foreach($itemdata as $itemdatas)
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524334 style='height:15.0pt'></td>
  <td class=xl6924334 style='border-top:none'>@php echo $n++; @endphp</td>
  <td class=xl6324334 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->employee_name }}</td>
  <td class=xl6324334 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->account_name }}</td>
  <td class=xl6324334 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->bank_name }}</td>
  <td class=xl6324334 style='border-top:none;border-left:none'>&nbsp;{{ $itemdatas->rek_no_bca }}</td>
  <td class=xl6524334 style='border-top:none;border-left:none'>{{ number_format($itemdatas->gaji_bersih,0) }} &nbsp;</td>
 </tr>
  @php 
 $t_total += $itemdatas->gaji_bersih; 
 @endphp
 @endforeach
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524334 style='height:15.0pt'></td>
  <td colspan=5 class=xl7724334>TOTAL</td>
  <td class=xl7024334 align=right style='border-top:none;border-left:none'>@php echo number_format($t_total,0); @endphp &nbsp;</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524334 style='height:15.0pt'></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524334 style='height:15.0pt'></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524334 style='height:15.0pt'></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524334 style='height:15.0pt'></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td colspan=3 class=xl7824334>JAKARTA, 20 Jul 2018</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524334 style='height:15.0pt'></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl7124334>DISETUJUI</td>
  <td class=xl7124334>MENGETAHUI</td>
  <td class=xl7124334>DIBUAT OLEH</td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524334 style='height:15.0pt'></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524334 style='height:15.0pt'></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524334 style='height:15.0pt'></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
 </tr>
 <tr height=20 style='height:15.0pt'>
  <td height=20 class=xl1524334 style='height:15.0pt'></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl1524334></td>
  <td class=xl7124334>(_________________)</td>
  <td class=xl7124334>(_________________)</td>
  <td class=xl7124334>(_________________)</td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=13 style='width:10pt'></td>
  <td width=34 style='width:26pt'></td>
  <td width=204 style='width:153pt'></td>
  <td width=188 style='width:141pt'></td>
  <td width=183 style='width:137pt'></td>
  <td width=160 style='width:120pt'></td>
  <td width=136 style='width:102pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>
