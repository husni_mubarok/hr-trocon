@extends('layouts.editor.template')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-check-square-o"></i> Reward
    <small>Personal Management</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Personal Management</a></li>
    <li class="active">Reward</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- <div class="col-xs-2">
		</div> -->
		<div class="col-xs-7">
			<div class="box box-danger">
				@include('errors.error')
				@if(isset($reward))
				{!! Form::model($reward, array('route' => ['editor.reward.update', $reward->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_reward'))!!}
				@else
				{!! Form::open(array('route' => 'editor.reward.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_reward'))!!}
				@endif
				{{ csrf_field() }}
				<div class="box-header with-border">
					<section class="content-header">
						<h4>
							@if(isset($reward))
							<i class="fa fa-pencil"></i> Edit
							@else
							<i class="fa fa-plus"></i> 
							@endif
							Reward
						</h4>
					</section>
				</div>
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('notrans', 'No Trans') }}
								{{ Form::text('notrans', old('notrans'), array('class' => 'form-control', 'placeholder' => 'No Trans*', 'required' => 'true', 'id' => 'notrans', 'disabled' => 'disabled')) }}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('datetrans', 'Date Trans') }}
								<div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                    {{ Form::text('datetrans', old('datetrans'), array('class' => 'form-control', 'placeholder' => 'Date Trans*', 'required' => 'true', 'id' => 'datetrans')) }}
                </div><!-- /.input group --> 
							</div>
						</div>   
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('employeeid', 'Employee Name') }}
								{{ Form::select('employeeid', $employee_list, old('employeeid'), array('class' => 'form-control select2', 'placeholder' => 'Select Employee', 'id' => 'employeeid', 'onchange' => 'RefreshData();')) }} 
								  
							</div>
						</div>   
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('sktypeid', 'SK Type') }}
								{{ Form::select('sktypeid', $sktype_list, old('sktypeid'), array('class' => 'form-control', 'placeholder' => 'Select SK Type', 'id' => 'sktypeid')) }}  
							</div>
						</div>   
						 
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('attachment', 'Attachment') }}<br>
								<span class="btn btn-default btn-file"><span>Choose file</span><input type="file" name="attachment" /></span>
								<br/>
							</div>
						</div>
					</div>
				</div>
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								@if(isset($reward)) 
				                  @if($reward->status == 9)
				                    <button type="submit" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save and Active</button>   
				                    <script type="text/javascript"> 
				                      $(document).ready(function(){
				                         hidebtnactive();
				                      });
				                    </script>
				                  @else
				                    <a href="#" onclick="cancel();" class="btn btn-danger btn-flat"><i class="fa fa-minus-square"></i> Cancel</a>  
				                  @endif
				                @endif
				               
					            <button type="submit" id="btnsave" class="btn btn-success pull-right btn-flat"><i class="fa fa-check"></i> Save</button>
					             @if(isset($reward)) 
				                <a href="#" onclick="showslip({{$reward->id}});" class="btn btn-primary pull-right btn-flat" style="margin-right: 5px"><i class="fa fa-print"></i> Print</a>
				                @endif
								<a href="{{ URL::route('editor.reward.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 5px"><i class="fa fa-close"></i> Close</a>
							</div>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="col-xs-5 col-md-5">
    <div class="box box-danger">
      <div class="box-header with-border">
          <section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
              <h4><i class="fa fa-sticky-note-o"></i> Reward History</h4>
          </section>
      </div>
      <div class="box-body">
          <table id="dtTable" class="table table-bordered table-hover stripe">
            <thead>
              <tr> 
                <th>No</th>
                <th>Date</th>
                <th>SK Type</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
@stop

@section('scripts') 
@if(isset($reward)) 
<script type="text/javascript"> 
  function cancel()
  {   
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure to cancel this data?',
      type: 'red',
      typeAnimated: true,
      buttons: {
        cancel: {
         action: function () { 
         }
       },
       confirm: {
        text: 'CANCEL',
        btnClass: 'btn-red',
        action: function () {
         $.ajax({
          url : '../../reward/cancel/' + {{$reward->id}},
          type: "PUT", 
          data: {
            '_token': $('input[name=_token]').val() 
          }, 
          success: function(data) {  
            //var loc = 'ap_invoice';
            if ((data.errors)) { 
              alert("Cancel error!");
            } else{
              window.location.href = "{{ URL::route('editor.reward.index') }}";
            }
          }, 
        }); 
       }
     },
    }
  });
  }  
  function hidebtnactive() {
      $('#btnsave').hide(100); 
  }
</script>
@endif

<script>
  var table;
  $(document).ready(function() {
        RefreshData();
        //datatables
        table = $('#dtTable').DataTable({ 
         //dttables
         processing: true,
         serverSide: true,
         "pageLength": 25,  
         "scrollY": "100%", 
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],  
         ajax: "{{ url('editor/reward/datahistory') }}",
         columns: [  
         { data: 'notrans', name: 'notrans', orderable: false, searchable: false }, 
         { data: 'datetrans', name: 'datetrans', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'sktypename', name: 'sktypename' },
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });

      });
 
    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax 
      }  

      function RefreshData()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.employeefilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'employeeid': $('#employeeid').val()   
          }, 
          success: function(data) { 
            reload_table();
          }
        }) 
      };  

      function showslip(id)
      {
       var url = '../../mutation/slip/' + id;
         PopupCenter(url,'Popup_Window','700','650');
      }

      function PopupCenter(url, title, w, h) {  
        // Fixes dual-screen position                         Most browsers      Firefox  
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;  
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;  
              
        width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;  
        height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;  
              
        var left = ((width / 2) - (w / 2)) + dualScreenLeft;  
        var top = ((height / 2) - (h / 2)) + dualScreenTop;  
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);  
        
        // Puts focus on the newWindow  
        if (window.focus) {  
          newWindow.focus();  
        }  
      } 
    </script>
@stop  
