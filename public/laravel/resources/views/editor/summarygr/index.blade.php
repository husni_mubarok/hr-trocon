@extends('layouts.editor.template')
@section('content')
 <style type="text/css">

.toolbar {
    float: left;
}
th,td  {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
</style>
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top: -10px; margin-bottom: -10px">
  <h1>
    <i class="fa fa-file"></i> Summary GR by Container
    <small>Report</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Report</a></li>
    <li class="active">Summary GR by Container</li>
  </ol>
</section>

<section class="content">
{{ csrf_field() }}
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">
          <div class="col-sm-1" data-toggle="tooltip" data-placement="top" title="Filter By">
            <select name="check_filter" id="check_filter" class="form-control" onchange="filter(); filter_week();">
              @if($datafilter->check_filter > 0) <option value="1">By Week</option> @else <option value="0">By Date</option>  @endif
              <option value="0">By Date</option> 
              <option value="1">By Week</option> 
            </select>
          </div>
          <div class="col-sm-2" id="divgrfrom" data-toggle="tooltip" data-placement="top" style="margin-left: -10px" title="Date From" style="height: 10px">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                {{ Form::text("grfrom", old("grfrom", $datafilter->grfrom), array("class" => "form-control", "placeholder" => "GR From*", "required" => "true", "id" => "grfrom", "onchange" => "filter();")) }}
            </div><!-- /.input group -->
          </div>
          <div class="col-sm-2" id="divgrto" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Date To">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
              {{ Form::text("grto", old("grto", $datafilter->grto), array("class" => "form-control", "placeholder" => "GR To*", "required" => "true", "id" => "grto", "onchange" => "filter();")) }}
            </div><!-- /.input group -->
          </div>

          <div class="col-sm-2" id="divweek" style="margin-left: -20px" data-toggle="tooltip" data-placement="top" title="Week"> 
            <select name="week" id="week" class="form-control select2" onchange="filter();">
            @if($datafilter->week > 0) <option value="{{$datafilter->week}}">Week {{$datafilter->week}} Year <?php echo date("Y"); ?></option> @endif
            @foreach($week_list as $week_lists)<option value="{{$week_lists->week_name}}">Week {{$week_lists->week_name}} Year <?php echo date("Y"); ?></option>@endforeach
          </select>
          </div>  
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <!-- <button class="btn btn-danger btn-flat" onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i> Bulk Delete</button> -->
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th>No Container</th> 
                  <th>Qty PO</th>
                  <th>Qty GR</th> 
                  <th>No PIB/AJU</th> 
                  <th>Shipping Line</th>
                  {{-- <th>Cost PIB/EMKL</th>  --}}
                  <th>Description</th>
                  <th>ETA</th>
                  <th>ATA</th>
                  <th>Original Doc</th>
                  <th>Pajak PIB</th>
                  <th>KT2</th>
                  <th>Inspect/KT9</th>
                  <th>Tgl Respon</th>
                  <th>Status Respon</th>
                  <th>Delivery Date</th>
                  <th>Delivery Time</th>
                  <th>Remarks</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  


 


@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "sScrollX": true,
         fixedColumns:   {
          leftColumns: 3
         },
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/summarygr/data') }}",
         columns: [  
         { data: 'container_no', name: 'container_no' },
         { data: 'po_qty', name: 'po_qty' },
         { data: 'gr_qty', name: 'gr_qty' },
         { data: 'no_pib', name: 'no_pib' }, 
         { data: 'shipping_line', name: 'shipping_line' },
         // { data: 'cost_pib', name: 'cost_pib' },
         { data: 'description', name: 'description' },
         { data: 'eta', name: 'eta' },
         { data: 'ata', name: 'ata' },
         { data: 'original_doc', name: 'original_doc' },
         { data: 'pajak_pib', name: 'pajak_pib' },
         { data: 'kt2', name: 'kt2' },
         { data: 'inspect_kt9', name: 'inspect_kt9' },
         { data: 'tlg_respon', name: 'tlg_respon' },
         { data: 'status_respon', name: 'status_respon' },
         { data: 'delivery_date', name: 'delivery_date' },
         { data: 'delivery_time', name: 'delivery_time' },
         { data: 'remarks', name: 'remarks' },
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }
 
       function add()
      {
        $("#btnSave").attr("onclick","save()");
        $("#btnSaveAdd").attr("onclick","saveadd()");
 
        $('.errorItemName').addClass('hidden');
        $('.errorItemNo').addClass('hidden');

        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Item'); // Set Title to Bootstrap modal title
      }

      function save()
      {   
        var url;
        url = "{{ URL::route('editor.item.store') }}";
        
        $.ajax({
          type: 'POST',
          url: url,
          data: {
            '_token': $('input[name=_token]').val(), 
            'item_category_id': $('#item_category_id').val(),
            'item_brand_id': $('#item_brand_id').val(),
            'item_code': $('#item_code').val(), 
            'item_name': $('#item_name').val(), 
            'description': $('#description').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
 
            $('.errorItemName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
              
              if (data.errors.itemname) {
                $('.errorItemName').removeClass('hidden');
                $('.errorItemName').text(data.errors.itemname);
              }
            } else {

              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully added data!', 'Success Alert', options);
              $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };

      function saveadd()
      {   
       $.ajax({
        type: 'POST',
        url: "{{ URL::route('editor.item.store') }}",
        data: {
          '_token': $('input[name=_token]').val(), 
          'item_category_id': $('#item_category_id').val(),
          'item_brand_id': $('#item_brand_id').val(),
          'item_code': $('#item_code').val(), 
          'item_name': $('#item_name').val(), 
          'description': $('#description').val(), 
          'status': $('#status').val()
        },
        success: function(data) {  
            $('.errorItemName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
            
              if (data.errors.rate) {
                $('.errorItemName').removeClass('hidden');
                $('.errorItemName').text(data.errors.rate);
              }
            } else {
          var options = { 
            "positionClass": "toast-bottom-right", 
            "timeOut": 1000, 
          };
          toastr.success('Successfully added data!', 'Success Alert', options);

            $('#form')[0].reset(); // reset form on modals
            reload_table(); 
          } 
        },
      })
     };

     function edit(id)
     { 

      $('.errorItemName').addClass('hidden');

      //alert("asdad");

      $("#btnSave").attr("onclick","update("+id+")");

      $("#btnSaveAdd").attr("onclick","updateadd("+id+")");

      save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : 'item/edit/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

            $('[name="id_key"]').val(data.id); 
            $('[name="item_category_id"]').val(data.item_category_id);
            $('[name="item_brand_id"]').val(data.item_brand_id);
            $('[name="item_code"]').val(data.item_code);
            $('[name="item_name"]').val(data.item_name);
            $('[name="description"]').val(data.description);
            $('[name="status"]').val(data.status);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Item'); // Set title to Bootstrap modal title
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
      }

      function update(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'item/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'item_category_id': $('#item_category_id').val(),
            'item_brand_id': $('#item_brand_id').val(),
            'item_code': $('#item_code').val(), 
            'item_name': $('#item_name').val(), 
            'description': $('#description').val(), 
            'status': $('#status').val()
          },
          success: function(data) {  
            $('.errorItemName').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data is required!', 'Error Validation', options);
             
              if (data.errors.itemname) {
                $('.errorItemName').removeClass('hidden');
                $('.errorItemName').text(data.errors.itemname);
              }
            } else {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
            $('#modal_form').modal('hide');
              $('#form')[0].reset(); // reset form on modals
              reload_table(); 
            } 
          },
        })
      };

      function updateadd(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'item/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'item_category_id': $('#item_category_id').val(),
            'item_brand_id': $('#item_brand_id').val(),
            'item_code': $('#item_code').val(), 
            'item_name': $('#item_name').val(), 
            'description': $('#description').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
            if ((data.errors)) {
             swal("Error!", "Gat data failed!", "error")
           } else { 
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
              $('#form')[0].reset(); // reset form on modals
              reload_table(); 
              $("#btnSave").attr("onclick","save()");
              $("#btnSaveAdd").attr("onclick","saveadd()");
            } 
          },
        })
      };

      function delete_id(id, itemname, date)
      {
        //var varnamre= $('#itemname').val();
        var itemname = itemname.bold();
        var date = date.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete ' + itemname + ' in ' + date + ' data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : 'item/delete/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                  icon: 'fa fa-danger', // glyphicon glyphicon-heart
                  title: 'Warning',
                  content: 'Error deleteing data!',
                });
              }
            });
           }
         },

       }
     });
      }

      window.onload= function(){ 

        n2= document.getElementById('rate');

        n2.onkeyup=n2.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n2.onblur= function(){
          var 
          temp2=parseFloat(validDigits(n2.value));
          if(temp2)n2.value=addCommas(temp2.toFixed(0));
        }

        n3= document.getElementById('plusrate');

        n3.onkeyup=n3.onchange= function(e){
          e=e|| window.event; 
          var who=e.target || e.srcElement,temp;
          temp= validDigits(who.value);
          who.value= addCommas(temp);
        }   
        n3.onblur= function(){
          var 
          temp3=parseFloat(validDigits(n3.value));
          if(temp3)n3.value=addCommas(temp3.toFixed(0));
        }

      }

      function reload_table()
      {
        table.ajax.reload(null,false); //reload datatable ajax 
      }
 

      function filter()
       { 

        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.datefilter') }}",
          data: {
            '_token': $('input[name=_token]').val(), 
            'grfrom': $('#grfrom').val(),    
            'grto': $('#grto').val(),
            'item_category_id': $('#item_category_id').val(),   
          }, 
          success: function(data) { 
            reload_table();
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Filter GR date has been change!', 'Success Alert', options);
          }
        }) 
      }; 

      function filter_week()
    {
      var filter_week = $("#check_filter").val();
       if(filter_week == 1){
        $("#divgrfrom").hide(500);
        $("#divgrto").hide(500);
        $("#divweek").show(500); 
       }else{
        $("#divgrfrom").show(500);
        $("#divgrto").show(500);
        $("#divweek").hide(500); 
       };
    };

    $(document).ready(function() {   
        @if($datafilter->check_filter == 1)
            $("#divgrfrom").hide(500);
            $("#divgrto").hide(500);
        @else
            $("#divweek").hide(500); 
        @endif
    });
   </script> 

   <!-- Add fancyBox -->
   <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
   <script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
   <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox").fancybox();
    });
  </script>
  @stop
