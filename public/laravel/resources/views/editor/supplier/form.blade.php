 
@extends('layouts.editor.template')
@section('content')
<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- <div class="col-xs-2">
	</div> -->
	<div class="col-xs-8">
		<div class="box box-danger">
			@include('errors.error')
			@if(isset($supplier))
			{!! Form::model($supplier, array('route' => ['editor.supplier.update', $supplier->id], 'method' => 'PUT', 'class'=>'update', 'files' => 'true', 'id'=>'form_supplier'))!!}
			@else
			{!! Form::open(array('route' => 'editor.supplier.store', 'class'=>'create', 'files' => 'true', 'id'=>'form_supplier'))!!}
			@endif
			{{ csrf_field() }}
			<div class="box-header with-border">
				<section class="content-header" style="margin-top:-25px !important; margin-bottom:-10px !important; margin-left: -15px !important">
					<h4>
						@if(isset($module))
						<i class="fa fa-pencil"></i> Edit
						@else
						<i class="fa fa-plus"></i> 
						@endif
						Supplier
					</h4>
				</section>
			</div>
			<div class="box-header with-border">
				<div class="row"> 
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('registrationno', 'Registration No') }}
							{{ Form::text('registrationno', old('registrationno'), array('class' => 'form-control', 'placeholder' => 'Registration No*', 'required' => 'true', 'id' => 'registrationno')) }} 
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('suppliercode', 'Supplier Code') }}
							{{ Form::text('suppliercode', old('suppliercode'), array('class' => 'form-control', 'placeholder' => 'Supplier Code*', 'required' => 'true', 'id' => 'suppliercode')) }} 
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('suppliername', 'Supplier Name') }}
							{{ Form::text('suppliername', old('suppliername'), array('class' => 'form-control', 'placeholder' => 'Supplier Name*', 'required' => 'true', 'id' => 'suppliername')) }} 
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('address', 'Address') }}
							{{ Form::text('address', old('address'), array('class' => 'form-control', 'placeholder' => 'Address*', 'required' => 'true', 'id' => 'address')) }}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('sex', 'City') }}
							{{ Form::select('cityid', $city_list, old('cityid'), array('class' => 'form-control', 'placeholder' => 'Select City', 'required' => 'true', 'id' => 'cityid')) }} 
						</div>
					</div>  
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('top1', 'TOP') }}
							{{ Form::text('top1', old('top1'), array('class' => 'form-control', 'placeholder' => 'TOP*', 'required' => 'true', 'id' => 'top1')) }}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('phone', 'Phone') }}
							{{ Form::text('phone', old('phone'), array('class' => 'form-control', 'placeholder' => 'Phone*', 'required' => 'true', 'id' => 'phone')) }}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('emailaddress', 'Email') }}
							{{ Form::text('emailaddress', old('emailaddress'), array('class' => 'form-control', 'placeholder' => 'Email*', 'required' => 'true', 'id' => 'emailaddress')) }}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('website', 'Website') }}
							{{ Form::text('website', old('website'), array('class' => 'form-control', 'placeholder' => 'Website*', 'required' => 'true', 'id' => 'website')) }}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('fax', 'Fax') }}
							{{ Form::text('fax', old('fax'), array('class' => 'form-control', 'placeholder' => 'Fax*', 'required' => 'true', 'id' => 'fax')) }}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('address', 'Address') }}
							{{ Form::text('address', old('address'), array('class' => 'form-control', 'placeholder' => 'Address*', 'required' => 'true', 'id' => 'address')) }}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{{ Form::label('image', 'Image') }}
							{{ Form::file('image') }}<br/>
						</div>
					</div>
				</div>
			</div>
			<div class="box-header with-border">
				<div class="col-md-12">
					<div class="form-group">
						<button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Save</button>
						<a href="{{ URL::route('editor.supplier.index') }}" class="btn btn-default pull-right" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
					</div>
				</div>
			</div>
		</section><!-- /.content -->
	</div>
</div>
</section>
@stop

