 @extends('layouts.editor.template')
 @section('title', 'THR')
 @section('content')

 <style type="text/css">
 	.input-smform {
  height: 22px;
  padding: 1px 3px;
  font-size: 12px;
  line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
  border-radius: 0px;
  width: 95% !important;
}

th,td  {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
 </style>
 <!-- Content Header (Page header) -->
 <section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
 	<h4>
 		THR
 	</h4>
 	<ol class="breadcrumb">
 		<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
 		<li><a href="#">Master Data</a></li>
 		<li class="active">THR</li>
 	</ol>
 </section>


 <section class="content">
 	<section class="content box box-danger">
 		<div class="row">
			<div class="col-md-12"> 

					{!! Form::model($thr, array('route' => ['editor.thr.updatedetail', $thr->id, $thr->departmentid], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_thr'))!!}
					<div id="consummable" class="tab-pane fade in active"> 
						<div class="box-body">
							<div class="div_overflow"> 

								<table class="table table-bordered table-hover stripe" id="thrTable" style="background-color: #fff">
									<thead>
										<tr>
											<th>Employee</th>
											<th>Join Date </th>
											<th>NPWP</th> 
											<th>Tax Status</th>
											<th>PTKP</th>
											<th>Basic</th>
											<th>Value</th>
											<th>THR</th>
											<th>Adj THR</th>
											<th>Total THR</th>
											<!-- <th>PKP ((THR*12)-PTKP)</th> -->
											<th>Tarif</th>
											<th>PPh 21</th>
											<th>Netto</th> 
										</tr>
									</thead>
									<tbody> 
										@php
											$tthr = 0;
											$tadjthr = 0;
											$ttthr = 0;
											$tpph21 = 0;
											$tnetto = 0;
										@endphp
										@foreach($thr_detail as $key => $thr_details)
										<tr>
											<td class="col-sm-1 col-md-1">
												{{$thr_details->employeename}} 
											</td>
											<td class="col-sm-1 col-md-1">
												{{$thr_details->joindate}} 
											</td>
											<td class="col-sm-1 col-md-1">
												{{$thr_details->npwp}} 
											</td>
											<td class="col-sm-1 col-md-1">
												{{$thr_details->taxstatus}} 
											</td>
											<td class="col-sm-1 col-md-1">
												{{number_format($thr_details->ptkp1,0)}} 
											</td>
											<td class="col-sm-1 col-md-1">
												{{number_format($thr_details->basic,0)}} 
											</td>
											<td class="col-sm-1 col-md-1">
												{{ Form::text('detail['.$thr_details->id.'][value]', old($thr_details->value.'[value]', $thr_details->value), ['id' => 'value'.$thr_details->id, 'class' => 'form-control input-sm', 'placeholder' => 'Value*']) }} 
											</td>  
											<td class="col-sm-1 col-md-1">  
												{{ Form::text('detail['.$thr_details->id.'][thr]', old($thr_details->thr.'[thr]', $thr_details->thr), ['id' => 'thr'.$thr_details->id, 'min' => '0', 'class' => 'form-control input-sm', 'placeholder' => 'THR*', 'oninput' => 'cal_sparator_thr('.$thr_details->id.')']) }} 
											</td> 
											<td class="col-sm-1 col-md-1">  
												{{ Form::text('detail['.$thr_details->id.'][adjthr]', old($thr_details->adjthr.'[adjthr]', $thr_details->adjthr), ['id' => 'adjthr'.$thr_details->id, 'min' => '0', 'class' => 'form-control input-sm', 'placeholder' => 'Adj THR*', 'oninput' => 'cal_sparator_adjthr('.$thr_details->id.')']) }} 
											</td> 
											<td class="col-sm-1 col-md-1">  
												{{ Form::text('detail['.$thr_details->id.'][tthr]', old($thr_details->id.'[tthr]', $thr_details->tthr), ['class' => 'form-control input-sm', 'placeholder' => 'T THR*', 'id' => 'tthr'.$thr_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$thr_details->id.'(); cal_sparator_tthr('.$thr_details->id.');']) }} 
											</td>
											<!-- <td class="col-sm-1">  
												{{number_format($thr_details->pkp1,0)}} 
											</td>   -->
											<td class="col-sm-1">  
												{{number_format($thr_details->tarif,0)}}%
											</td>  
											<td class="col-sm-1">  
												<!-- {{ Form::text('detail['.$thr_details->id.'][pph21]', old($thr_details->id.'[pph21]', $thr_details->pph21), ['class' => 'form-control input-sm', 'placeholder' => 'PPh21*', 'id' => 'pph21'.$thr_details->id, 'min' => '0', 'oninput' => 'cal_sparator'.$thr_details->id.'(); cal_sparator_pph21('.$thr_details->id.');']) }}  -->
												{{number_format($thr_details->pph21,0)}} 
											</td> 
											<td class="col-sm-1">  
												{{number_format($thr_details->netto,0)}} 
											</td>  
										</td>  
									</tr>  

									@php 
										$tthr = str_replace(",","",$thr_details->thr);
										$tadjthr = str_replace(",","",$thr_details->adjthr);
										$ttthr = str_replace(",","",$thr_details->tthr);
										$tpph21 = str_replace(",","",$thr_details->pph21);
										$tnetto+= ($thr_details->netto);

									@endphp
									@endforeach

								</tbody>
								<tfoot>
									<tr>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<!-- <th></th> -->
										<th align="right"></th>
										<th><!-- @php echo number_format($tthr,0); @endphp --></th>
										<th><!-- @php echo $tadjthr; @endphp --></th>
										<th><!-- @php echo number_format($ttthr,0); @endphp --></th>
										<th><!-- @php echo number_format($tpph21,0); @endphp -->Total</th> 
										<th>@php echo number_format($tnetto,0); @endphp</th>
									</tr>
								</tfoot>
							</table>
						</div> 
						<!-- /.box-body -->
					</div>
				</div> 
			</div> 
			<a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Submit</a>
			<a href="{{ URL::route('editor.thr.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
		</div>
		{!! Form::close() !!} 
 	</div>
 </div>
</section>

@stop 

@section('scripts')

<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#thrTable").DataTable({
			"sScrollX": true,
	         "scrollY": "350px",
	         "bPaginate": false,
	         "autoWidth": false,
	         "ordering": false
		});
	});
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
</script>  
<script type="text/javascript"> 
	$('#btn_submit').on('click', function()
	{ 
		$.confirm({
			title: 'Confirm!',
			content: 'Are you sure to create data?',
			type: 'green',
			typeAnimated: true,
			buttons: {
				cancel: {
					action: function () { 
					}
				},
				confirm: {
					text: 'CREATE',
					btnClass: 'btn-green',
					action: function () {  
						$('#form_thr').submit(); 
					}
				},

			}
		});
	});

	function cal_sparator_value(id) {  
		//ot 1
	    var value = document.getElementById('value' + id).value;
	    var result = document.getElementById('value' + id);
	    var rsamount = (value);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('value' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='value' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}

  	function cal_sparator_thr(id) {  
		//ot 1
	    var thr = document.getElementById('thr' + id).value;
	    var result = document.getElementById('thr' + id);
	    var rsamount = (thr);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('thr' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='thr' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}

	function cal_sparator_adjthr(id) {  
		//ot 1
	    var adjthr = document.getElementById('adjthr' + id).value;
	    var result = document.getElementById('adjthr' + id);
	    var rsamount = (adjthr);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('adjthr' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='adjthr' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}

  	function cal_sparator_tthr(id) {  
		//ot 1
	    var tthr = document.getElementById('tthr' + id).value;
	    var result = document.getElementById('tthr' + id);
	    var rsamount = (tthr);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('tthr' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='tthr' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}

  	function cal_sparator_pph21(id) {  
		//ot 1
	    var pph21 = document.getElementById('pph21' + id).value;
	    var result = document.getElementById('pph21' + id);
	    var rsamount = (pph21);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('pph21' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='pph21' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}

  	function cal_sparator_netto(id) {  
		//ot 1
	    var netto = document.getElementById('netto' + id).value;
	    var result = document.getElementById('netto' + id);
	    var rsamount = (netto);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('netto' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='netto' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}
</script>
@stop

