@extends('layouts.editor.template')
@section('title', 'THR')
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
  <h4>
    THR
  </h4>
  <ol class="breadcrumb">
    <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Master Data</a></li>
    <li class="active">THR</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger"> 
        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important">

          <button onClick="add()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-sticky-note-o"></i> Tambah Baru</button>
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Kembali</button> 
          <div class="box-tools pull-right">
            <div class="tableTools-container">
              <a onClick="GenerateData()" class="btn btn-danger btn-flat" style="float:left; margin-right:5px"> <i class="fa fa-magic"></i> Generate</a>
            </div>
          </div><!-- /.box-tools -->
        </div>

        <div class="box-header with-border" style="height:50px !important; margin-top:-2px !important"> 
          <button onClick="reload_table()" type="button" class="btn btn-success btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <!-- <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Period">
            <div class="col-sm-12 pull-right">   
              <h5><b>Period</b></h5>  
            </div>
          </div>   --> 
          <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Period" style="margin-left: -20px !important">
            <div class="col-sm-12 pull-right">   
              {{ Form::select('periodidfilter', $payperiod_list, old('periodidfilter'), array('class' => 'form-control', 'placeholder' => 'Select Period', 'id' => 'periodidfilter', 'onchange' => 'RefreshData();')) }}   
            </div>
          </div>  


       <!--  <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Period">
            <div class="col-sm-12 pull-right">   
              <h5><b>Period</b></h5>  
            </div>
          </div>  -->  
          <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Year" style="margin-left: -20px !important">
            <div class="col-sm-12 pull-right">   
              {{ Form::select('year', $year_list, old('year'), array('class' => 'form-control', 'placeholder' => 'Select Year', 'id' => 'year', 'onchange' => 'RefreshData();')) }}   
            </div>
          </div>   

          <!--  <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Period">
            <div class="col-sm-12 pull-right">   
              <h5><b>Period</b></h5>  
            </div>
          </div>  -->  
          <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Bagin Kumulatif" style="margin-left: -20px !important">
            <div class="col-sm-12 pull-right">   
              {{ Form::select('bmonth', $month_list, old('bmonth'), array('class' => 'form-control', 'placeholder' => 'Select Bagin Kumulatif', 'id' => 'bmonth', 'onchange' => 'RefreshData();')) }}   
            </div>
          </div>  


          <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Ending Kumulatif" style="margin-left: -20px !important">
            <div class="col-sm-12 pull-right">   
              {{ Form::select('emonth', $month_list, old('emonth'), array('class' => 'form-control', 'placeholder' => 'Select Ending Kumulatif', 'id' => 'emonth', 'onchange' => 'RefreshData();')) }}   
            </div>
          </div>  

          <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Last Bruto Month" style="margin-left: -20px !important">
            <div class="col-sm-12 pull-right">   
              {{ Form::select('lastbrutothrmonth', $month_list, old('lastbrutothrmonth'), array('class' => 'form-control', 'placeholder' => 'Select Last Bruto Month', 'id' => 'lastbrutothrmonth', 'onchange' => 'RefreshData();')) }}   
            </div>
          </div>  

          <div class="form-group pull-left" data-toggle="tooltip" data-placement="top" title="Last Bruto Year" style="margin-left: -20px !important">
            <div class="col-sm-12 pull-right">   
              {{ Form::select('lastbrutothr', $year_list, old('lastbrutothr'), array('class' => 'form-control', 'placeholder' => 'Select Last Bruto Year', 'id' => 'lastbrutothr', 'onchange' => 'RefreshData();')) }}   
            </div>
          </div>     
        </div>



        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr>
                  <th style="width:5%">
                    <label class="control control--checkbox">
                      <input type="checkbox" id="check-all"/>
                      <div class="control__indicator"></div>
                    </label>

                  </th>
                  <th>Aksi</th> 
                  <th>Detail</th>
                  <th>Period</th> 
                  <th>Department</th>
                  <th>Year</th>
                  <th>Idul Fitri Date</th>
                  <th>Payment Date</th>
                  <th>Description</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width:40% !important">
    <div class="modal-content">
      <form action="#" id="form" class="form-horizontal">
        {{ csrf_field() }}
        <div class="modal-header" style="height: 60px">
          <div class="form-group pull-right">
            <label for="real_name" class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8 pull-right">
              <select class="form-control" style="width: 100%;" name="status"  id="status">
               <option value="0">Aktif</option>
               <option value="1">Tidak Aktif</option>
             </select>
           </div>
         </div>
         <h3 class="modal-title">Thr Form</h3>
       </div>
       <div class="modal-body">
         <input type="hidden" value="" name="idrack"/> 
         <div class="form-body">
          <div class="row">

            <div class="form-group">
              <label class="control-label col-md-3">Payroll Period</label>
              <div class="col-md-8">  
                {{ Form::select('periodid', $payperiod_list, old('periodid'), array('class' => 'form-control', 'placeholder' => 'Select Payroll Period', 'id' => 'periodid')) }} 
                <small class="errorperiodid hidden alert-danger"></small> 
              </div>
            </div>   
            <div class="form-group">
              <label class="control-label col-md-3">Department</label>
              <div class="col-md-8">  
                {{ Form::select('departmentid', $department_list, old('departmentid'), array('class' => 'form-control', 'placeholder' => 'Select Department', 'id' => 'departmentid')) }} 
                <small class="errorMandatory hidden alert-danger"></small> 
              </div>
            </div> 

            <div class="form-group">
              <label class="control-label col-md-3">Year</label>
              <div class="col-md-8">  
                {{ Form::select('yearform', $year_list, old('yearform'), array('class' => 'form-control', 'placeholder' => 'Select Year', 'id' => 'yearform')) }} 
                <small class="errorMandatory hidden alert-danger"></small> 
              </div>
            </div>   

            <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Idul Fiti Date</label>
                <div class="col-sm-8">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('idulfitridate', old('idulfitridate'), array('class' => 'form-control', 'placeholder' => 'Idul Fiti Date*', 'required' => 'true', 'id' => 'idulfitridate', 'onclick' => 'saveheader();')) }}
                </div><!-- /.input group --> 
              </div>
            </div> 

            <div class="form-group">
                <label for="real_name" class="col-sm-3 control-label">Payment Date</label>
                <div class="col-sm-8">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>  
                  {{ Form::text('paymentdate', old('paymentdate'), array('class' => 'form-control', 'placeholder' => 'Payment Date*', 'required' => 'true', 'id' => 'paymentdate', 'onclick' => 'saveheader();')) }}
                </div><!-- /.input group --> 
              </div>
            </div> 

            <div class="form-group">
              <label class="control-label col-md-3">Description</label>
              <div class="col-md-8">
                <input name="description" id="description" class="form-control" type="text">
                <small class="errorDescription hidden alert-danger"></small> 
              </div>
            </div>

          </div>
        </div>
      </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Tutup</button>
        <button type="button" id="btnSaveAdd" onClick="saveadd()" class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-plus-square"></i> Simpan & Tambah</button>
        <button type="button" id="btnSave"  class="btn btn-primary btn-flat pull-right" style="margin-left:5px !important">  <i class="fa fa-save"></i> Simpan & Tutup</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
         processing: true,
         serverSide: true,
         "pageLength": 25,
         "scrollY": "360px",
         "rowReorder": true,
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
         ajax: "{{ url('editor/thr/data') }}",
         columns: [  
         { data: 'check', name: 'check', orderable: false, searchable: false },
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'actiondetail', name: 'actiondetail', orderable: false, searchable: false }, 
         { data: 'description', name: 'description' },
         { data: 'departmentname', name: 'departmentname' },
         { data: 'year', name: 'year' },
         { data: 'idulfitridate', name: 'idulfitridate' },
         { data: 'paymentdate', name: 'paymentdate' },
         { data: 'description', name: 'description' },
         { data: 'mstatus', name: 'mstatus' }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });
      });
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

      function add()
      {
        $("#btnSave").attr("onclick","save()");
        $("#btnSaveAdd").attr("onclick","saveadd()");

        $('.errorperiodid').addClass('hidden');

        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Tambah Thr'); // Set Title to Bootstrap modal title
      }

      function save()
      {   
        var url;
        url = "{{ URL::route('editor.thr.store') }}";
        
        $.ajax({
          type: 'POST',
          url: url,
          data: {
            '_token': $('input[name=_token]').val(), 
            'periodid': $('#periodid').val(), 
            'departmentid': $('#departmentid').val(), 
            'yearform': $('#yearform').val(), 
            'idulfitridate': $('#idulfitridate').val(), 
            'paymentdate': $('#paymentdate').val(), 
            'description': $('#description').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
            $('.errorperiodid').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data wajib diisi!', 'Validasi Error', options);
              
              if (data.errors.periodid) {
                $('.errorperiodid').removeClass('hidden');
                $('.errorperiodid').text(data.errors.periodid);
              }
            } else {

              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Tambah data berhasil!', 'Success Alert', options);
              $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };

      function saveadd()
      {   
       $.ajax({
        type: 'POST',
        url: "{{ URL::route('editor.thr.store') }}",
        data: {
          '_token': $('input[name=_token]').val(), 
          'periodid': $('#periodid').val(), 
          'departmentid': $('#departmentid').val(),  
          'yearform': $('#yearform').val(), 
          'idulfitridate': $('#idulfitridate').val(), 
          'paymentdate': $('#paymentdate').val(), 
          'description': $('#description').val(), 
          'status': $('#status').val()
        },
        success: function(data) {  
          $('.errorperiodid').addClass('hidden');

          if ((data.errors)) {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.error('Data wajib diisi!', 'Validasi Error', options);
            
            if (data.errors.periodid) {
              $('.errorperiodid').removeClass('hidden');
              $('.errorperiodid').text(data.errors.periodid);
            }
          } else {
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Tambah data berhasil!', 'Success Alert', options);

                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
     };

     function edit(id)
     { 

      $('.errorperiodid').addClass('hidden');

      //alert("asdad");

      $("#btnSave").attr("onclick","update("+id+")");

      $("#btnSaveAdd").attr("onclick","updateadd("+id+")");

      save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url : 'thr/edit/' + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

            $('[name="id_key"]').val(data.id); 
            $('[name="periodid"]').val(data.periodid);
            $('[name="departmentid"]').val(data.departmentid);
            $('[name="year"]').val(data.year);
            $('[name="paymentdate"]').val(data.paymentdate);
            $('[name="idulfitridate"]').val(data.idulfitridate);
            $('[name="description"]').val(data.description);
            $('[name="status"]').val(data.status);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Thr'); // Set title to Bootstrap modal title
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                alert('Error get data from ajax');
              }
            });
      }

      function update(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'thr/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'periodid': $('#periodid').val(), 
            'departmentid': $('#departmentid').val(), 
            'yearform': $('#yearform').val(), 
            'idulfitridate': $('#idulfitridate').val(), 
            'paymentdate': $('#paymentdate').val(), 
            'description': $('#description').val(), 
            'status': $('#status').val()
          },
          success: function(data) {  
            $('.errorperiodid').addClass('hidden');

            if ((data.errors)) {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.error('Data wajib diisi!', 'Validasi Error', options);

              if (data.errors.periodid) {
                $('.errorperiodid').removeClass('hidden');
                $('.errorperiodid').text(data.errors.periodid);
              }
            } else {
              var options = { 
                "positionClass": "toast-bottom-right", 
                "timeOut": 1000, 
              };
              toastr.success('Successfully updated data!', 'Success Alert', options);
              $('#modal_form').modal('hide');
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                  } 
                },
              })
      };

      function updateadd(id)
      {
        save_method = 'update'; 
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: 'thr/edit/' + id,
          type: "PUT",
          data: {
            '_token': $('input[name=_token]').val(), 
            'periodid': $('#periodid').val(), 
            'departmentid': $('#departmentid').val(), 
            'yearform': $('#yearform').val(), 
            'idulfitridate': $('#idulfitridate').val(), 
            'paymentdate': $('#paymentdate').val(), 
            'description': $('#description').val(), 
            'status': $('#status').val()
          },
          success: function(data) { 
            if ((data.errors)) {
             swal("Error!", "Gat data failed!", "error")
           } else { 
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully updated data!', 'Success Alert', options);
                    $('#form')[0].reset(); // reset form on modals
                    reload_table(); 
                    $("#btnSave").attr("onclick","save()");
                    $("#btnSaveAdd").attr("onclick","saveadd()");
                  } 
                },
              })
      };

      function delete_id(id, periodid)
      {

        //var varnamre= $('#periodid').val();
        var periodid = periodid.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to delete ' + periodid + ' data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'DELETE',
            btnClass: 'btn-red',
            action: function () {
             $.ajax({
              url : 'thr/delete/' + id,
              type: "DELETE",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully deleted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error deleteing data!',
                      });
              }
            });
           }
         },

       }
     });
      }

      function GenerateData()
      {

        var periodid = $('#periodidfilter').val();
        var perioddesc = $("#periodidfilter option:selected").text();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to generate <b><u>' + perioddesc + '</u></b> data?',
          type: 'red',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'CREATE',
            btnClass: 'btn-red',
            action: function () { 
             waitingDialog.show('Proses...', {dialogSize: 'sm', progressType: 'primary'});
             $.ajax({
              url : 'thr/generate/' + periodid,
              type: "POST",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                waitingDialog.hide();
                toastr.success('Successfully genareted data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error generate data!',
                      });
              }
            });
           }
         },

       }
     });
      }

      function RefreshData()
       {  
        $.ajax({
          type: 'POST',
          url: "{{ URL::route('editor.periodfilterthr') }}",
          data: {
            '_token': $('input[name=_token]').val(),     
            'periodidfilter': $('#periodidfilter').val(),     
            'year': $('#year').val(),     
            'bmonth': $('#bmonth').val(),     
            'emonth': $('#emonth').val(),     
            'lastbrutothrmonth': $('#lastbrutothrmonth').val(),     
            'lastbrutothr': $('#lastbrutothr').val()   
          }, 
          success: function(data) { 
            var options = { 
              "positionClass": "toast-bottom-right", 
              "timeOut": 1000, 
            };
            toastr.success('Successfully filtering data!', 'Success Alert', options);
            reload_table();
          }
        }) 
      }; 
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();   
      });

       /**
       * Module for displaying "Waiting for..." dialog using Bootstrap
       *
       * @author Eugene Maslovich <ehpc@em42.ru>
       */

        var waitingDialog = waitingDialog || (function ($) {
            'use strict';

          // Creating modal dialog's DOM
          var $dialog = $(
            '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
            '<div class="modal-dialog modal-m">' +
            '<div class="modal-content">' +
              '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
              '<div class="modal-body">' +
                '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
              '</div>' +
            '</div></div></div>');

          return {
            /**
             * Opens our dialog
             * @param message Process...
             * @param options Custom options:
             *          options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
             *          options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
             */
            show: function (message, options) {
              // Assigning defaults
              if (typeof options === 'undefined') {
                options = {};
              }
              if (typeof message === 'undefined') {
                message = 'Loading';
              }
              var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
              }, options);

              // Configuring dialog
              $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
              $dialog.find('.progress-bar').attr('class', 'progress-bar');
              if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
              }
              $dialog.find('h3').text(message);
              // Adding callbacks
              if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                  settings.onHide.call($dialog);
                });
              }
              // Opening dialog
              $dialog.modal();
            },
            /**
             * Closes dialog
             */
            hide: function () {
              $dialog.modal('hide');
            }
          };

        })(jQuery);
    </script> 
    @stop
