 @extends('layouts.editor.template')
 @section('content')

 <style type="text/css">
 	.input-smform {
  height: 22px;
  padding: 1px 3px;
  font-size: 12px;
  line-height: 2.5; /* If Placeholder of the input is moved up, rem/modify this. */
  border-radius: 0px;
  width: 95% !important;
}

th,td  {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
 </style>

 <!-- Content Header (Page header) -->
 <section class="content-header" style="margin-top:-15px !important; margin-bottom:-20px !important">
 	<h4>
 		TLK, TMLM & Insentif
 	</h4>
 	<ol class="breadcrumb">
 		<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
 		<li><a href="#">Master Data</a></li>
 		<li class="active">TLK, TMLM & Insentif</li>
 	</ol>
 </section>

 <section class="content">
 	<section class="content box box-danger">
 		<div class="row">
			<div class="col-md-12">   
			{!! Form::model($mealtran, array('route' => ['editor.tlk.updatedetail', $mealtran->id, $mealtran->departmentid], 'method' => 'PUT', 'class'=>'update', 'id'=>'form_mealtran'))!!}
				<div class="box-body">
					<table class="table table-bordered table-hover stripe" id="mealtranTable">
						<thead>
							<tr>
								<th rowspan="2" style="width: 30px !important">Ceklis</th>
								<th rowspan="2">Karyawan</th>
								<th rowspan="2">Lokasi</th>
								<th colspan="6"><center>Insentif</center></th>
								<th colspan="8"><center>Tunjangan Luar Kota</center></th>
								<th colspan="8"><center>Tunjangan Malam</center></th> 
								<th rowspan="2">(Insentif+TLK+<br>TLM)-(Pot. Telat)</th>
								<th rowspan="2">Koreksi</th>
								<th rowspan="2">Grand Total</th>

							</tr>
							<tr>
								<!-- uang makan -->
								<th>Minggu 1</th> 
								<th>Minggu 2</th>
								<th>Minggu 3</th>
								<th>Minggu 4</th>
								<th>Minggu 5</th>
								<th>Total Insentif</th> 

								<!-- tunjangan luar kota -->
								<th>Minggu 1</th> 
								<th>Minggu 2</th>
								<th>Minggu 3</th>
								<th>Minggu 4</th>
								<th>Minggu 5</th>
								<th>Total Hari</th> 
								<th>Rate TLK</th> 
								<th>Total TLK</th> 


								<!-- tunjangan luar malam -->
								<th>Minggu 1</th> 
								<th>Minggu 2</th>
								<th>Minggu 3</th>
								<th>Minggu 4</th>
								<th>Minggu 5</th>
								<th>Total Hari</th> 
								<th>Rate TM</th> 
								<th>Total TM</th> 
							</tr>
						</thead>
						
						<tbody> 
							@foreach($mealtran_detail as $key => $mealtran_details)
							<tr style="background-color: #fff !important">
								<td class="col-sm-1" style="width: 40px !important">
									<input type="checkbox" id="checklist" name="detail[{{$mealtran_details->id}}][checklist]" @if($mealtran_details->checklist == 1) checked @endif>
								</td>
								<td class="col-sm-1 col-md-1">
									{{$mealtran_details->employeename}} 
								</td>
								<td class="col-sm-1 col-md-1">
									{{$mealtran_details->locationname}} 
								</td>
								<td class="col-sm-1 col-md-1">
									{{ Form::text('detail['.$mealtran_details->id.'][insentif1]', old($mealtran_details->insentif1.'[insentif1]', $mealtran_details->insentif1), ['id' => 'insentif1'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'Insentif 1*', 'oninput' => 'cal_sparator_insentif1('.$mealtran_details->id.')']) }} 
								</td>  
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][insentif2]', old($mealtran_details->insentif2.'[insentif2]', $mealtran_details->insentif2), ['id' => 'insentif2'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'Insentif 2*', 'oninput' => 'cal_sparator_insentif2('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][insentif3]', old($mealtran_details->insentif3.'[insentif3]', $mealtran_details->insentif3), ['id' => 'insentif3'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'Insentif 3*', 'oninput' => 'cal_sparator_insentif3('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][insentif4]', old($mealtran_details->id.'[insentif4]', $mealtran_details->insentif4), ['class' => 'form-control input-smform', 'placeholder' => 'Insentif 4*', 'id' => 'insentif4'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_insentif4('.$mealtran_details->id.');']) }} 
								</td> 
								<td class="col-sm-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][insentif5]', old($mealtran_details->id.'[insentif5]', $mealtran_details->insentif5), ['class' => 'form-control input-smform', 'placeholder' => 'Insentif 5*', 'id' => 'insentif5'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_insentif5('.$mealtran_details->id.')']) }} 
								</td> 


								<td class="col-sm-2 col-md-2">  
									{{ Form::text('detail['.$mealtran_details->id.'][insentif]', old($mealtran_details->id.'[insentif]', $mealtran_details->insentif), ['class' => 'form-control input-smform', 'placeholder' => 'Uang Makan*', 'id' => 'insentif'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_insentif('.$mealtran_details->id.')']) }} 
								</td> 


								<!-- tunjangan luar kota -->
								<td class="col-sm-1 col-md-1">
									{{ Form::text('detail['.$mealtran_details->id.'][doc1]', old($mealtran_details->doc1.'[doc1]', $mealtran_details->doc1), ['id' => 'doc1'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'TLK 1*', 'oninput' => 'cal_sparator_doc1('.$mealtran_details->id.')']) }} 
								</td>  
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][doc2]', old($mealtran_details->doc2.'[doc2]', $mealtran_details->doc2), ['id' => 'doc2'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'TLK 2*', 'oninput' => 'cal_sparator_doc2('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][doc3]', old($mealtran_details->doc3.'[doc3]', $mealtran_details->doc3), ['id' => 'doc3'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'TLK 3*', 'oninput' => 'cal_sparator_doc3('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][doc4]', old($mealtran_details->id.'[doc4]', $mealtran_details->doc4), ['class' => 'form-control input-smform', 'placeholder' => 'TLK 4*', 'id' => 'doc4'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_doc4('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][doc5]', old($mealtran_details->id.'[doc5]', $mealtran_details->doc5), ['class' => 'form-control input-smform', 'placeholder' => 'TLK 5*', 'id' => 'doc5'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_doc5('.$mealtran_details->id.')']) }} 
								</td> 


								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][dayoutcity]', old($mealtran_details->id.'[dayoutcity]', $mealtran_details->dayoutcity), ['class' => 'form-control input-smform', 'placeholder' => 'Day(s)*', 'id' => 'dayoutcity'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_dayoutcity('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][ratemealoutcity]', old($mealtran_details->id.'[ratemealoutcity]', $mealtran_details->ratemealoutcity), ['class' => 'form-control input-smform', 'placeholder' => 'Rate*', 'id' => 'ratemealoutcity'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_ratemealoutcity('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">
									{{ Form::text('detail['.$mealtran_details->id.'][tunluarkota]', old($mealtran_details->tunluarkota.'[tunluarkota]', $mealtran_details->tunluarkota), ['id' => 'tunluarkota'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'TLK*', 'oninput' => 'cal_sparator_tunluarkota('.$mealtran_details->id.')']) }} 
								</td>  	


								<!-- tunjangan malam -->
								
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][dtunjmalam1]', old($mealtran_details->dtunjmalam1.'[dtunjmalam1]', $mealtran_details->dtunjmalam1), ['id' => 'dtunjmalam1'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'TM 1*', 'oninput' => 'cal_sparator_dtunjmalam1('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][dtunjmalam2]', old($mealtran_details->dtunjmalam2.'[dtunjmalam2]', $mealtran_details->dtunjmalam2), ['id' => 'dtunjmalam2'.$mealtran_details->id, 'min' => '0', 'class' => 'form-control input-smform', 'placeholder' => 'TM 2*', 'oninput' => 'cal_sparator_dtunjmalam2('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][dtunjmalam3]', old($mealtran_details->id.'[dtunjmalam3]', $mealtran_details->dtunjmalam3), ['class' => 'form-control input-smform', 'placeholder' => 'TM 3*', 'id' => 'dtunjmalam3'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_dtunjmalam3('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][dtunjmalam4]', old($mealtran_details->id.'[dtunjmalam4]', $mealtran_details->dtunjmalam4), ['class' => 'form-control input-smform', 'placeholder' => 'TM 4*', 'id' => 'dtunjmalam4'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_dtunjmalam4('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1 col-md-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][dtunjmalam5]', old($mealtran_details->id.'[dtunjmalam5]', $mealtran_details->dtunjmalam5), ['class' => 'form-control input-smform', 'placeholder' => 'TM 5*', 'id' => 'dtunjmalam5'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_dtunjmalam5('.$mealtran_details->id.')']) }} 
								</td> 


								<td class="col-sm-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][dtunjmalam]', old($mealtran_details->id.'[dtunjmalam]', $mealtran_details->dtunjmalam), ['class' => 'form-control input-smform', 'placeholder' => 'TM 5*', 'id' => 'dtunjmalam'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_dtunjmalam('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][ratetunjmalam]', old($mealtran_details->id.'[ratetunjmalam]', $mealtran_details->ratetunjmalam), ['class' => 'form-control input-smform', 'placeholder' => 'TM 5*', 'id' => 'ratetunjmalam'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_ratetunjmalam('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][tunjmalam]', old($mealtran_details->id.'[tunjmalam]', $mealtran_details->tunjmalam), ['class' => 'form-control input-smform', 'placeholder' => 'Day 5*', 'id' => 'tunjmalam'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_tunjmalam('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1">  
									{{$mealtran_details->total}}
								</td> 
								<td class="col-sm-1">  
									{{ Form::text('detail['.$mealtran_details->id.'][correction]', old($mealtran_details->id.'[correction]', $mealtran_details->correction), ['class' => 'form-control input-smform', 'placeholder' => 'Correction*', 'id' => 'correction'.$mealtran_details->id, 'min' => '0', 'oninput' => 'cal_sparator_correction('.$mealtran_details->id.')']) }} 
								</td> 
								<td class="col-sm-1">  
								 	{{$mealtran_details->grandtotal}}
								</td> 
						</tr>  
						@endforeach
					 	</tbody>
					 	
					 	<tfoot>
						<tr>
							<th></th>
							<th></th> 
							<th align="right">Total</th>
							<th>{{$mealtran_total->insentif1}}</th>
							<th>{{$mealtran_total->insentif2}}</th>
							<th>{{$mealtran_total->insentif3}}</th>
							<th>{{$mealtran_total->insentif4}}</th>
							<th>{{$mealtran_total->insentif5}}</th> 
							<th>{{$mealtran_total->insentif}}</th> 


							<th>{{$mealtran_total->doc1}}</th>
							<th>{{$mealtran_total->doc2}}</th>
							<th>{{$mealtran_total->doc3}}</th>
							<th>{{$mealtran_total->doc4}}</th>
							<th>{{$mealtran_total->doc5}}</th> 
							<th>{{$mealtran_total->dayoutcity}}</th> 
							<th>{{$mealtran_total->ratemealoutcity}}</th> 
							<th>{{$mealtran_total->tunluarkota}}</th> 

							<th>{{$mealtran_total->dtunjmalam1}}</th>
							<th>{{$mealtran_total->dtunjmalam2}}</th>
							<th>{{$mealtran_total->dtunjmalam3}}</th>
							<th>{{$mealtran_total->dtunjmalam4}}</th>
							<th>{{$mealtran_total->dtunjmalam5}}</th> 
							<th>{{$mealtran_total->total}}</th> 
							<th>{{$mealtran_total->correction}}</th> 
							<th>{{$mealtran_total->grandtotal}}</th> 
							<th>{{$mealtran_total->grandtotal}}</th> 
							<th>{{$mealtran_total->grandtotal}}</th> 
							<th>{{$mealtran_total->grandtotal}}</th> 
						</tr>
					</tfoot>
				</table> 
			<!-- /.box-body -->
			<a href="#" type="button" id="btn_submit" class="btn btn-primary pull-right btn-flat"><i class="fa fa-check"></i> Submit</a>
			<a href="{{ URL::route('editor.mealtran.index') }}" class="btn btn-default pull-right btn-flat" style="margin-right: 10px"><i class="fa fa-close"></i> Close</a>
		</div>
		{!! Form::close() !!} 
	</div>
 </div>
</section>

@stop 

@section('scripts')

<script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
<script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
<script>
	$(document).ready(function () {
		$("#mealtranTable").DataTable({
			"sScrollX": true,
			// "scrollCollapse": true,
	         "scrollY": "350px",
	         "bPaginate": false,
	         "autoWidth": false,
	         "ordering": true,
	         fixedColumns:{
	          leftColumns: 2
	         },
		});
		$('#mealtranTable').DataTable().columns.adjust().draw();
	}); 
</script>  
<script type="text/javascript"> 
	$('#btn_submit').on('click', function()
	{ 
		$.confirm({
			title: 'Confirm!',
			content: 'Are you sure to create data?',
			type: 'green',
			typeAnimated: true,
			buttons: {
				cancel: {
					action: function () { 
					}
				},
				confirm: {
					text: 'CREATE',
					btnClass: 'btn-green',
					action: function () {  
						$('#form_mealtran').submit(); 
					}
				},

			}
		});
	});

	function cal_sparator_insentif1(id) {  
		//insentif 1
	    var insentif1 = document.getElementById('insentif1' + id).value;
	    var result = document.getElementById('insentif1' + id);
	    var rsamount = (insentif1);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('insentif1' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='insentif1' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}

  	function cal_sparator_insentif2(id) {  
	    //insentif 2
	    var insentif2 = document.getElementById('insentif2' + id).value;
	    var result2 = document.getElementById('insentif2' + id);
	    var rsamount2 = (insentif2);
	    result2.value = rsamount2.replace(/,/g, "");  

	    n3= document.getElementById('insentif2' + id);

	    n3.onkeyup=n3.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp1;
	      if(who.id==='insentif2' + id)  temp1= validDigits(who.value,0); 
	      else temp1= validDigits(who.value);
	      who.value= addCommas(temp1);
	    }   
	    n3.onblur= function(){
	      var 
	      temp3=parseFloat(validDigits(n3.value));
	      if(temp3)n3.value=addCommas(temp3.toFixed(0));
	    }
  	}

  	function cal_sparator_insentif3(id) {  
	    //insentif 3
	    var insentif3 = document.getElementById('insentif3' + id).value;
	    var result = document.getElementById('insentif3' + id);
	    var rsamount = (insentif3);
	    result.value = rsamount.replace(/,/g, "");  

	    n4= document.getElementById('insentif3' + id);

	    n4.onkeyup=n4.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='insentif3' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n4.onblur= function(){
	      var 
	      temp4=parseFloat(validDigits(n4.value));
	      if(temp4)n4.value=addCommas(temp4.toFixed(0));
	    }
  	}

  	function cal_sparator_insentif4(id) {  
	    //insentif 4
	    var insentif4 = document.getElementById('insentif4' + id).value;
	    var result = document.getElementById('insentif4' + id);
	    var rsamount = (insentif4);
	    result.value = rsamount.replace(/,/g, "");  

	    n5= document.getElementById('insentif4' + id);

	    n5.onkeyup=n5.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='insentif4' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n5.onblur= function(){
	      var 
	      temp5=parseFloat(validDigits(n5.value));
	      if(temp5)n5.value=addCommas(temp5.toFixed(0));
	    }
  	}

  	function cal_sparator_insentif5(id) {  
	    //insentif 5
	    var insentif5 = document.getElementById('insentif5' + id).value;
	    var result = document.getElementById('insentif5' + id);
	    var rsamount = (insentif5);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('insentif5' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='insentif5' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}

  	function cal_sparator_insentif(id) {  
	    //insentif 5
	    var insentif = document.getElementById('insentif' + id).value;
	    var result = document.getElementById('insentif' + id);
	    var rsamount = (insentif);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('insentif' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='insentif' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}


  	function cal_sparator_doc1(id) {  
		//doc 1
	    var doc1 = document.getElementById('doc1' + id).value;
	    var result = document.getElementById('doc1' + id);
	    var rsamount = (doc1);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('doc1' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='doc1' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}

  	function cal_sparator_doc2(id) {  
	    //doc 2
	    var doc2 = document.getElementById('doc2' + id).value;
	    var result2 = document.getElementById('doc2' + id);
	    var rsamount2 = (doc2);
	    result2.value = rsamount2.replace(/,/g, "");  

	    n3= document.getElementById('doc2' + id);

	    n3.onkeyup=n3.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp1;
	      if(who.id==='doc2' + id)  temp1= validDigits(who.value,0); 
	      else temp1= validDigits(who.value);
	      who.value= addCommas(temp1);
	    }   
	    n3.onblur= function(){
	      var 
	      temp3=parseFloat(validDigits(n3.value));
	      if(temp3)n3.value=addCommas(temp3.toFixed(0));
	    }
  	}

  	function cal_sparator_doc3(id) {  
	    //doc 3
	    var doc3 = document.getElementById('doc3' + id).value;
	    var result = document.getElementById('doc3' + id);
	    var rsamount = (doc3);
	    result.value = rsamount.replace(/,/g, "");  

	    n4= document.getElementById('doc3' + id);

	    n4.onkeyup=n4.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='doc3' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n4.onblur= function(){
	      var 
	      temp4=parseFloat(validDigits(n4.value));
	      if(temp4)n4.value=addCommas(temp4.toFixed(0));
	    }
  	}

  	function cal_sparator_doc4(id) {  
	    //doc 4
	    var doc4 = document.getElementById('doc4' + id).value;
	    var result = document.getElementById('doc4' + id);
	    var rsamount = (doc4);
	    result.value = rsamount.replace(/,/g, "");  

	    n5= document.getElementById('doc4' + id);

	    n5.onkeyup=n5.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='doc4' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n5.onblur= function(){
	      var 
	      temp5=parseFloat(validDigits(n5.value));
	      if(temp5)n5.value=addCommas(temp5.toFixed(0));
	    }
  	}

  	function cal_sparator_doc5(id) {  
	    //doc 5
	    var doc5 = document.getElementById('doc5' + id).value;
	    var result = document.getElementById('doc5' + id);
	    var rsamount = (doc5);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('doc5' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='doc5' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}


  	function cal_sparator_dayoutcity(id) {  
	    //doc 4
	    var dayoutcity = document.getElementById('dayoutcity' + id).value;
	    var result = document.getElementById('dayoutcity' + id);
	    var rsamount = (dayoutcity);
	    result.value = rsamount.replace(/,/g, "");  

	    n5= document.getElementById('dayoutcity' + id);

	    n5.onkeyup=n5.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='dayoutcity' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n5.onblur= function(){
	      var 
	      temp5=parseFloat(validDigits(n5.value));
	      if(temp5)n5.value=addCommas(temp5.toFixed(0));
	    }
  	}

  	function cal_sparator_ratemealoutcity(id) {  
	    //doc 4
	    var ratemealoutcity = document.getElementById('ratemealoutcity' + id).value;
	    var result = document.getElementById('ratemealoutcity' + id);
	    var rsamount = (ratemealoutcity);
	    result.value = rsamount.replace(/,/g, "");  

	    n5= document.getElementById('ratemealoutcity' + id);

	    n5.onkeyup=n5.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='ratemealoutcity' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n5.onblur= function(){
	      var 
	      temp5=parseFloat(validDigits(n5.value));
	      if(temp5)n5.value=addCommas(temp5.toFixed(0));
	    }
  	}

  	function cal_sparator_tunluarkota(id) {  
	    //doc 4
	    var tunluarkota = document.getElementById('tunluarkota' + id).value;
	    var result = document.getElementById('tunluarkota' + id);
	    var rsamount = (tunluarkota);
	    result.value = rsamount.replace(/,/g, "");  

	    n5= document.getElementById('tunluarkota' + id);

	    n5.onkeyup=n5.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='tunluarkota' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n5.onblur= function(){
	      var 
	      temp5=parseFloat(validDigits(n5.value));
	      if(temp5)n5.value=addCommas(temp5.toFixed(0));
	    }
  	}




  	function cal_sparator_dtunjmalam1(id) {  
		//dtunjmalam 1
	    var dtunjmalam1 = document.getElementById('dtunjmalam1' + id).value;
	    var result = document.getElementById('dtunjmalam1' + id);
	    var rsamount = (dtunjmalam1);
	    result.value = rsamount.replace(/,/g, "");  

	    n2= document.getElementById('dtunjmalam1' + id);

	    n2.onkeyup=n2.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='dtunjmalam1' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n2.onblur= function(){
	      var 
	      temp2=parseFloat(validDigits(n2.value));
	      if(temp2)n2.value=addCommas(temp2.toFixed(0));
	    }
  	}

  	function cal_sparator_dtunjmalam2(id) {  
	    //dtunjmalam 2
	    var dtunjmalam2 = document.getElementById('dtunjmalam2' + id).value;
	    var result2 = document.getElementById('dtunjmalam2' + id);
	    var rsamount2 = (dtunjmalam2);
	    result2.value = rsamount2.replace(/,/g, "");  

	    n3= document.getElementById('dtunjmalam2' + id);

	    n3.onkeyup=n3.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp1;
	      if(who.id==='dtunjmalam2' + id)  temp1= validDigits(who.value,0); 
	      else temp1= validDigits(who.value);
	      who.value= addCommas(temp1);
	    }   
	    n3.onblur= function(){
	      var 
	      temp3=parseFloat(validDigits(n3.value));
	      if(temp3)n3.value=addCommas(temp3.toFixed(0));
	    }
  	}

  	function cal_sparator_dtunjmalam3(id) {  
	    //dtunjmalam 3
	    var dtunjmalam3 = document.getElementById('dtunjmalam3' + id).value;
	    var result = document.getElementById('dtunjmalam3' + id);
	    var rsamount = (dtunjmalam3);
	    result.value = rsamount.replace(/,/g, "");  

	    n4= document.getElementById('dtunjmalam3' + id);

	    n4.onkeyup=n4.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='dtunjmalam3' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n4.onblur= function(){
	      var 
	      temp4=parseFloat(validDigits(n4.value));
	      if(temp4)n4.value=addCommas(temp4.toFixed(0));
	    }
  	}

  	function cal_sparator_dtunjmalam4(id) {  
	    //dtunjmalam 4
	    var dtunjmalam4 = document.getElementById('dtunjmalam4' + id).value;
	    var result = document.getElementById('dtunjmalam4' + id);
	    var rsamount = (dtunjmalam4);
	    result.value = rsamount.replace(/,/g, "");  

	    n5= document.getElementById('dtunjmalam4' + id);

	    n5.onkeyup=n5.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='dtunjmalam4' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n5.onblur= function(){
	      var 
	      temp5=parseFloat(validDigits(n5.value));
	      if(temp5)n5.value=addCommas(temp5.toFixed(0));
	    }
  	}

  	function cal_sparator_dtunjmalam5(id) {  
	    //dtunjmalam 5
	    var dtunjmalam5 = document.getElementById('dtunjmalam5' + id).value;
	    var result = document.getElementById('dtunjmalam5' + id);
	    var rsamount = (dtunjmalam5);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('dtunjmalam5' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='dtunjmalam5' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}


  	function cal_sparator_dtunjmalam(id) {  
	    //ddtunjmalam 5
	    var dtunjmalam = document.getElementById('dtunjmalam' + id).value;
	    var result = document.getElementById('dtunjmalam' + id);
	    var rsamount = (dtunjmalam);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('dtunjmalam' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='dtunjmalam' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}


  	function cal_sparator_ratetunjmalam(id) {  
	    //dratetunjmalam 5
	    var ratetunjmalam = document.getElementById('ratetunjmalam' + id).value;
	    var result = document.getElementById('ratetunjmalam' + id);
	    var rsamount = (ratetunjmalam);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('ratetunjmalam' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='ratetunjmalam' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}

  	function cal_sparator_tunjmalam(id) {  
	    //dtunjmalam 5
	    var tunjmalam = document.getElementById('tunjmalam' + id).value;
	    var result = document.getElementById('tunjmalam' + id);
	    var rsamount = (tunjmalam);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('tunjmalam' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='tunjmalam' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}

  	function cal_sparator_correction(id) {  
	    //dcorrection 5
	    var correction = document.getElementById('correction' + id).value;
	    var result = document.getElementById('correction' + id);
	    var rsamount = (correction);
	    result.value = rsamount.replace(/,/g, "");  

	    n6= document.getElementById('correction' + id);

	    n6.onkeyup=n6.onchange= function(e){
	      e=e|| window.event; 
	      var who=e.target || e.srcElement,temp;
	      if(who.id==='correction' + id)  temp= validDigits(who.value,0); 
	      else temp= validDigits(who.value);
	      who.value= addCommas(temp);
	    }   
	    n6.onblur= function(){
	      var 
	      temp6=parseFloat(validDigits(n6.value));
	      if(temp6)n6.value=addCommas(temp6.toFixed(0));
	    }
  	}

</script>
@stop

