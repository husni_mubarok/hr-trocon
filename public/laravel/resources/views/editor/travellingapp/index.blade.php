@extends('layouts.editor.template')
@section('content') 

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <i class="fa fa-plane"></i> Travelling Approval
    <small>Personal Management</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Personal Management</a></li>
    <li class="active">Travelling Approval</li>
  </ol>
</section>

<section class="content">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-danger">
        <div class="box-header with-border">
          <button onClick="history.back()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-undo"></i> Kembali</button>
          <button onClick="reload_table()" type="button" class="btn btn-primary btn-flat"> <i class="fa fa-refresh"></i> Refresh</button>
          <div class="box-tools pull-right">
            <div class="tableTools-container">
            </div>
          </div><!-- /.box-tools -->
        </div>
        <div class="box-header">
          <!-- /.panel-heading -->
          <div class="box-body">
            <table id="dtTable" class="table table-bordered table-hover stripe">
              <thead>
                <tr> 
                  <th>#</th>
                  <th>No Trans</th>
                  <th>Date Trans</th>
                  <th>Employee Name</th>
                  <th>From</th>
                  <th>To</th>
                  <th>Type</th>
                  <th>City</th>
                  <th>Actual In</th>
                  <th>Amount</th>
                  <th>Attachment</th>
                  <th>Status</th>
                  <th>Approve</th> 
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  

@stop
@section('scripts')
<script>
  var table;
  $(document).ready(function() {
        //datatables
        table = $('#dtTable').DataTable({ 
        //editor 
         colReorder: true,
         fixedHeader: true, 
         responsive: true,
         //rowReorder: true, 
         "rowReorder": {
            "update": false,
        },  
         //dttables
         processing: true,
         serverSide: true,
         "pageLength": 25,  
         fixedColumns:   {
          leftColumns: 5,
          rightColumns: 1
         },
         "aoColumnDefs": [
        { "sClass": "my_class", "aTargets": [ 0, 1, 2 ] }
        ],
         "scrollY": "360px", 
         "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],  
         ajax: "{{ url('editor/travelling/data') }}",
         columns: [  
         { data: 'id', name: 'id' },
         { data: 'action', name: 'action', orderable: false, searchable: false }, 
         { data: 'datetrans', name: 'datetrans', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'employeename', name: 'employeename' },
         { data: 'travellingfrom', name: 'travellingfrom', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'travellingto', name: 'travellingto', render: function(d){return moment(d).format("DD-MM-YYYY");} },
         { data: 'travelingtypename', name: 'travelingtypename' },
         { data: 'cityname', name: 'cityname' },
         { data: 'actualin', name: 'actualin' },
         { data: 'amount', name: 'amount' },
         { data: 'attachment', name: 'attachment' },
         { data: 'mstatus', name: 'mstatus' }, 
         { data: 'approval', name: 'approval' }
         ]
       });
        //check all
        $("#check-all").click(function () {
          $(".data-check").prop('checked', $(this).prop('checked'));
        });

        //auto number
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

      });
 
  function reload_table()
  {
        table.ajax.reload(null,false); //reload datatable ajax 
      }

       function approve_id(id, notrans)
      {

        //var varnamre= $('#notrans').val();
        var notrans = notrans.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to approve ' + notrans + ' data?',
          type: 'green',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'APPROVE',
            btnClass: 'btn-green',
            action: function () {
             $.ajax({
              url : 'travellingapp/approve/' + id,
              type: "PUT",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully approve data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error deleteing data!',
                      });
              }
            });
           }
         },

       }
     });
      }

       function not_approve_id(id, notrans)
      {

        //var varnamre= $('#notrans').val();
        var notrans = notrans.bold();

        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure to not approve ' + notrans + ' data?',
          type: 'orange',
          typeAnimated: true,
          buttons: {
            cancel: {
             action: function () { 
             }
           },
           confirm: {
            text: 'NOT APPROVE',
            btnClass: 'btn-orange',
            action: function () {
             $.ajax({
              url : 'travellingapp/notapprove/' + id,
              type: "PUT",
              data: {
                '_token': $('input[name=_token]').val() 
              },
              success: function(data)
              { 
                var options = { 
                  "positionClass": "toast-bottom-right", 
                  "timeOut": 1000, 
                };
                toastr.success('Successfully not approve data!', 'Success Alert', options);
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                $.alert({
                  type: 'red',
                        icon: 'fa fa-danger', // glyphicon glyphicon-heart
                        title: 'Warning',
                        content: 'Error deleteing data!',
                      });
              }
            });
           }
         },

       }
     });
      }
   </script> 

   <!-- Add fancyBox -->
   <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
   <script type="text/javascript" src="{{Config::get('constants.path.plugin')}}/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
   <script type="text/javascript">
    $(document).ready(function() {
      $(".fancybox").fancybox();
    });
  </script>
  @stop
