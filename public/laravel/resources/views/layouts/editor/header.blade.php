  <header class="main-header">
  <nav class="navbar navbar-static-top" role="navigation">
    <div class="container">
      <div class="navbar-header" style="margin-left: -70px !important">
        <!-- <a href="dashboard" class="navbar-brand"><b>ERP</b>System</a> -->
        <a href="{{url('/')}}/editor"><img src="{{Config::get('constants.path.img')}}/logo_index.png" alt="" /> </a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-building-o"></i> Master Data<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu"> 
              @actionStart('holiday', 'read')
              <li><a href="{{ URL::route('editor.holiday.index') }}">Hari Libur</a></li> 
              @actionEnd
              @actionStart('payrolltype', 'read')
              <li><a href="{{ URL::route('editor.payrolltype.index') }}">Jenis Gaji</a></li>
              @actionEnd
              @actionStart('year', 'read')
              <li><a href="{{ URL::route('editor.year.index') }}">Tahun</a></li>   
              @actionEnd
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Lokasi</a>
                <ul class="dropdown-menu">
                  @actionStart('location', 'read')
                  <li><a href="{{ URL::route('editor.location.index') }}">Lokasi</a></li>
                  @actionEnd
                  @actionStart('city', 'read')
                  <li><a href="{{ URL::route('editor.city.index') }}">Kota</a></li> 
                  @actionEnd
                </ul>
              </li> 
            </ul>
          </li>

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users"></i> Karyawan <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              @actionStart('employee', 'read')
              <li><a href="{{ URL::route('editor.employee.index') }}">Data Karyawan</a></li> 
              @actionEnd
              @actionStart('employeesalary', 'read')
              <li><a href="{{ URL::route('editor.employeesalary.index') }}">Gaji Karyawan</a></li>
              @actionEnd  
              @actionStart('employeestatus', 'read')
              <li><a href="{{ URL::route('editor.employeestatus.index') }}">Status Karyawan</a></li> 
              @actionEnd
              @actionStart('department', 'read')
              <li><a href="{{ URL::route('editor.department.index') }}">Departemen</a></li>
              @actionEnd
              @actionStart('position', 'read') 
              <li><a href="{{ URL::route('editor.position.index') }}">Posisi</a></li>  
              @actionEnd
              @actionStart('religion', 'read')
              <li><a href="{{ URL::route('editor.religion.index') }}">Agama</a></li> 
              @actionEnd
              @actionStart('golongan', 'read')
              <li><a href="{{ URL::route('editor.golongan.index') }}">Golongan</a></li> 
              @actionEnd
              @actionStart('medicaltype', 'read')
              <li><a href="{{ URL::route('editor.medicaltype.index') }}">Jenis Medikal</a></li> 
              @actionEnd
              @actionStart('taxstatus', 'read')
              <li><a href="{{ URL::route('editor.taxstatus.index') }}">Status Pajak</a></li> 
              @actionEnd
              <li class="divider"></li>   
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Pendidikan</a>
                <ul class="dropdown-menu">
                  @actionStart('educationlevel', 'read')
                  <li><a href="{{ URL::route('editor.educationlevel.index') }}">Level Pendidikan</a></li>
                  @actionEnd
                  @actionStart('educationmajor', 'read')  
                  <li><a href="{{ URL::route('editor.educationmajor.index') }}">Jurusan</a></li>  
                  @actionEnd
                  @actionStart('educationtype', 'read')
                  <li><a href="{{ URL::route('editor.educationtype.index') }}">Jenis Pendidikan</a></li>
                  @actionEnd 
                </ul>
              </li>    
            </ul>
          </li>


          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-money"></i> Pengaturan Gaji <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              @actionStart('umr', 'read')
              <li><a href="{{ URL::route('editor.umr.index') }}">UMR</a></li>
              @actionEnd
              @actionStart('iuranpensiun', 'read')
              <li><a href="{{ URL::route('editor.iuranpensiun.index') }}">Iuran Pensiun</a></li>
              @actionEnd
              @actionStart('tarif', 'read')
              <li><a href="{{ URL::route('editor.tarif.index') }}">Tarif</a></li> 
              @actionEnd
            </ul>
          </li>

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-clock-o"></i> Absen & Gaji <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <!-- <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Gaji</a>
                <ul class="dropdown-menu">
                  <li><a href="{{ URL::route('editor.payperiod.index') }}"> Periode Gaji</a></li>
                  <li><a href="{{ URL::route('editor.payroll.index') }}"> Gaji</a></li> 
                </ul>
              </li> -->
              @actionStart('payperiod', 'read')
              <li><a href="{{ URL::route('editor.payperiod.index') }}"> Periode</a></li>
              @actionEnd
              @actionStart('overtime', 'read')
              <li><a href="{{ URL::route('editor.overtime.index') }}">Lembur</a></li> 
              @actionEnd
              @actionStart('mealtran', 'read')
              <li><a href="{{ URL::route('editor.mealtran.index') }}">Uang Makan</a></li>
              @actionEnd
              @actionStart('payroll', 'read') 
              <li><a href="{{ URL::route('editor.payroll.index') }}"> Pendapatan dan Potongan Lainnya</a></li> 
              @actionEnd
              <!-- <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Uang Makan</a>
                <ul class="dropdown-menu">
                  <li><a href="{{ URL::route('editor.mealtran.index') }}">Uang Makan dan Transport</a></li>  
                  <li><a href="{{ URL::route('editor.tlk.index') }}">TLK, TMLM & Insentif</a></li> 
                </ul>
              </li>  -->
              <!-- <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Bonus</a>
                <ul class="dropdown-menu">
                  @actionStart('payperiodbonus', 'read')
                  <li><a href="{{ URL::route('editor.payperiodbonus.index') }}"> Periode Bonus</a></li>
                  @actionEnd
                  @actionStart('bonus', 'read')
                  <li><a href="{{ URL::route('editor.bonus.index') }}"> Bonus</a></li>
                  @actionEnd
                </ul>
              </li>   -->
              @actionStart('bonus', 'read')
                  <li><a href="{{ URL::route('editor.bonus.index') }}"> Bonus</a></li>
                  @actionEnd
              @actionStart('thr', 'read')
              <li><a href="{{ URL::route('editor.thr.index') }}">THR</a></li>  
              @actionEnd
              @actionStart('loan', 'read')
              <li><a href="{{ URL::route('editor.loan.index') }}">Pinjaman Karyawan</a></li>
              @actionEnd
              @actionStart('reimburse', 'read')
              <li><a href="{{ URL::route('editor.reimburse.index') }}">Reimburse Kesehatan</a></li> 
              @actionEnd
            </ul>
          </li> 
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sticky-note-o"></i> Laporan <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"> Gaji</a>
                <ul class="dropdown-menu">
                  @actionStart('reportpayroll', 'read')
                  <li><a href="{{ URL::route('editor.reportpayroll.index') }}"> Laporan Gaji</a></li>
                  @actionEnd 
                  @actionStart('reportpayrollreceive', 'read')
                  <li><a href="{{ URL::route('editor.reportpayrollreceive.index') }}"> Tanda Terima Gaji</a></li> 
                  @actionEnd
                  <!-- @actionStart('emailpayrollslip', 'read')
                  <li><a href="{{ URL::route('editor.emailpayrollslip.index') }}"> Email Slip Gaji</a></li>
                  @actionEnd  -->
                </ul>
              </li>

              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"> Bonus dan THR</a>
                <ul class="dropdown-menu">
                  @actionStart('reportbonusthrtax', 'read')
                  {{-- <li><a href="{{ URL::route('editor.reportbonusthrtax.index') }}"> THR dan Bonus (Pajak)</a></li>  --}}
                  @actionEnd
                  @actionStart('reportbonusthr', 'read')
                  <li><a href="{{ URL::route('editor.reportbonusthr.index') }}"> THR dan Bonus</a></li> 
                  @actionEnd
                  @actionStart('reportbonustax', 'read')
                  {{-- <li><a href="{{ URL::route('editor.reportbonustax.index') }}"> Bonus (Pajak)</a></li>   --}}
                  @actionEnd
                  @actionStart('reportbonus', 'read')
                  <li><a href="{{ URL::route('editor.reportbonus.index') }}"> Bonus</a></li>  
                  @actionEnd
                  @actionStart('reportbonustransfer', 'read')
                  <li><a href="{{ URL::route('editor.reportbonustransfer.index') }}"> Transfer Bonus BCA</a></li>  
                  @actionEnd
                  @actionStart('reportbonusslip', 'read')
                  <li><a href="{{ URL::route('editor.reportbonusslip.index') }}"> Slip Bonus</a></li> 
                  @actionEnd
                </ul>
              </li> 
              @actionStart('reportjamsostek', 'read')
              <li><a href="{{ URL::route('editor.reportjamsostek.index') }}">Jamsostek</a></li> 
              @actionEnd
              @actionStart('reportloan', 'read')
              <li><a href="{{ URL::route('editor.reportloan.index') }}">Pinjaman Karyawan</a></li> 
              @actionEnd
              @actionStart('reporttransferbca', 'read')
              <li><a href="{{ URL::route('editor.reporttransferbca.index') }}">Transfer BCA</a></li> 
              @actionEnd
              @actionStart('reporttransferrequest', 'read')
              <li><a href="{{ URL::route('editor.reporttransferrequest.index') }}">Permohonan Transfer</a></li> 
              @actionEnd
              @actionStart('reportovertime', 'read')
              <li><a href="{{ URL::route('editor.reportovertime.index') }}">Lembur</a></li> 
              @actionEnd
              @actionStart('reportmeal', 'read')
              <li><a href="{{ URL::route('editor.reportmeal.index') }}">Uang Makan Staf</a></li>
              @actionEnd   
              <!-- <li><a href="{{ URL::route('editor.reportabsencededuction.index') }}">Potongan Absen</a></li>  -->
              @actionStart('reportinsentive', 'read')
              <li><a href="{{ URL::route('editor.reportinsentive.index') }}">Insentif</a></li> 
              @actionEnd
              @actionStart('reportemployeemaster', 'read')
              <li><a href="{{ URL::route('editor.reportemployeemaster.index') }}">Master Karyawan dan Gaji</a></li> 
              @actionEnd 
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"> Reimburse Kesehatan</a>
                <ul class="dropdown-menu">
                  @actionStart('reportreimburse', 'read')
                  <li><a href="{{ URL::route('editor.reportreimburse.index') }}"> Laporan Histori Reimburse</a></li>
                  @actionEnd 
                  @actionStart('reportreimburse', 'read')
                  <li><a href="{{ URL::route('editor.reportreimburseremain.index') }}"> Laporan Sisa Reimburse</a></li> 
                  @actionEnd 
                </ul>
              </li> 
            </ul>
          </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-money"></i> Payroll <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    @actionStart('masterptkp', 'read')
                    <li class="dropdown-submenu">
                        <a tabindex="-1" href="#">Pengaturan</a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('editor/payroll/setting/master-ptkp') }}"> Master PTKP </a></li> 
                        </ul>
                    </li>
                    @actionEnd
                    @actionStart('payrollpembetulan', 'read')
                    <li><a href="{{ url('editor/payroll/correction') }}">Pembetulan</a></li> 
                    @actionEnd
                    @actionStart('payrollperhitungan', 'read')
                    <li><a href="{{ url('editor/payroll/calculation/preview') }}">Perhitungan Gaji</a></li> 
                    @actionEnd
                    @actionStart('payrollhasilperhitungan', 'read')
                    <li><a href="{{ url('editor/payroll/calculation/payslip/result') }}">Hasil Perhitungan Gaji</a></li>
                    @actionEnd
                    @actionStart('payrollreport', 'read')
                    <li><a href="{{ url('editor/payroll/calculation/report') }}">Laporan Perhitungan Gaji</a></li>
                    @actionEnd
                    @actionStart('payrolltaxminus', 'read')
                    <li><a href="{{ url('editor/payroll/calculation/payslip/tax-minus') }}">Laporan Pengembalian Pajak</a></li>
                    @actionEnd
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-money"></i> THR <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    @actionStart('thrperhitungan', 'read')
                    <li><a href="{{ url('editor/payroll/thr/preview') }}">Perhitungan THR</a></li> 
                    @actionEnd
                    @actionStart('thrhasilperhitungan', 'read')
                    <li><a href="{{ url('editor/payroll/thr/payslip/result') }}">Hasil Perhitungan THR</a></li>
                    @actionEnd
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-money"></i> Bonus <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    @actionStart('bonusperhitungan', 'read')
                    <li><a href="{{ url('editor/payroll/bonus/preview') }}">Perhitungan Bonus</a></li> 
                    @actionEnd
                    @actionStart('bonushasilperhitungan', 'read')
                    <li><a href="{{ url('editor/payroll/bonus/payslip/result') }}">Hasil Perhitungan Bonus</a></li>
                    @actionEnd
                </ul>
            </li>
            <!-- PAYROLL ERICK -->

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i> <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li class="dropdown-submenu">
                <a tabindex="-1" href="#"> Role Management</a>
                <ul class="dropdown-menu">
                  <li><a href="{{ URL::route('editor.user.index') }}"> User List</a></li> 
                  <li><a href="{{ URL::route('editor.action.index') }}">Action</a></li> 
                  <li><a href="{{ URL::route('editor.privilege.index') }}"> Previlage</a></li> 
                  <li><a href="{{ URL::route('editor.module.index') }}"> Module</a></li>  
                </ul>
              </li> 

              <li><a href="{{ URL::route('editor.popup.index') }}">Notification</a></li> 
              <!-- <li><a href="bank">Layout</a></li>  -->
            </ul>
          </li>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav"> 
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                @if(Auth::user()->filename == null)
                <img src="{{Config::get('constants.path.img')}}/avatar5.png" class="user-image" alt="User Image">
                {{Auth::user()->username}}
                @else
                <img src="{{Config::get('constants.path.uploads')}}/user/{{Auth::user()->username}}/thumbnail/{{Auth::user()->filename}}" class="user-image" alt="User Image">
                {{Auth::user()->username}}
                @endif
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs"></span>
              </a>
              <ul class="dropdown-menu">
               <li class="user-header">
                @if(Auth::user()->filename == null)
                <img src="{{Config::get('constants.path.img')}}/avatar5.png" class="img-circle" alt="User Image">
                @else
                <img src="{{Config::get('constants.path.uploads')}}/user/{{Auth::user()->username}}/thumbnail/{{Auth::user()->filename}}" class="img-circle" alt="User Image">
                @endif

                <p>
                 {{Auth::user()->username}}
                 <small>Member since {{date("d-m-Y", strtotime(Auth::user()->created_at))}}</small>
               </p>
             </li>
             <!-- Menu Body -->
             <li class="user-body">
              <div class="row">
                <div class="col-xs-4 text-center">
                  <a href="#">Followers</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Sales</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Friends</a>
                </div>
              </div>
              <!-- /.row -->
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
               <a href="{{ URL::route('editor.profile.show') }}" class="btn btn-default btn-flat">Profile</a>
             </div>
             <div class="pull-right">
              <form id="logout-form" action="{{ url('/logout') }}" method="POST">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Logout</a>
                </form>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
    <!-- /.navbar-custom-menu -->
  </div>
  <!-- /.container-fluid -->
</nav>
</header>