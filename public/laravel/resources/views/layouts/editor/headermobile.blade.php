<header class="main-header">
  <nav class="navbar navbar-static-top" role="navigation" style="font-size: 15px !important">
    <div class="container">
      <div class="navbar-header">
        <!-- <a href="dashboard" class="navbar-brand"><b>ERP</b>System</a> -->
        <a href="{{url('/')}}/editor"><img src="{{Config::get('constants.path.img')}}/logo_lmu_sidebar_mobile.png" alt="" /> </a>
        {{-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"> --}}
          <i class="fa fa-bars"></i>
        </button>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-briefcase"></i> Master Data<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu"> 
              @actionStart('container', 'read')
              <li><a href="{{ URL::route('editor.container.index') }}">Container</a></li> 
              @actionEnd
              @actionStart('dailyrate', 'read')
              <li><a href="{{ URL::route('editor.dailyrate.index') }}">Daily Rate</a></li>
              @actionEnd
              @actionStart('branch', 'read')
              <li><a href="{{ URL::route('editor.branch.index') }}">Branch</a></li> 
              @actionEnd
              @actionStart('pib', 'read')
              <li><a href="{{ URL::route('editor.pib.index') }}">PIB</a></li>   
              @actionEnd
              @actionStart('emklcost', 'read')
              <li><a href="{{ URL::route('editor.emklcost.index') }}">EMKL Cost</a></li> 
              @actionEnd
              @actionStart('currency', 'read')
              <li><a href="{{ URL::route('editor.currency.index') }}">Currency</a></li>  
              @actionEnd
              @actionStart('storelocation', 'read')
              <li><a href="{{ URL::route('editor.storelocation.index') }}">Store Location</a></li> 
              @actionEnd
            </ul>
          </li>

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cubes"></i> Item <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              @actionStart('item', 'read')
              <li><a href="{{ URL::route('editor.item.index') }}">Item</a></li>
              @actionEnd

              @actionStart('itemcategory', 'read')
              <li><a href="{{ URL::route('editor.itemcategory.index') }}">Item Category</a></li> 
              @actionEnd

              @actionStart('itembrand', 'read')
              <li><a href="{{ URL::route('editor.itembrand.index') }}">Item Brand</a></li> 
              @actionEnd

              @actionStart('itemunit', 'read')
              <li><a href="{{ URL::route('editor.itemunit.index') }}">Item Unit</a></li>   
              @actionEnd

            </ul>
          </li>

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cart-plus"></i> Transaction <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              @actionStart('capitalgood', 'read')
              <li><a href="{{ URL::route('editor.capitalgood.index') }}">Branch Allocation</a></li>
              @actionEnd

              @actionStart('branchalloc', 'read')
              <li><a href="{{ URL::route('editor.branchalloc.index') }}">Capital Goods</a></li>   
              @actionEnd

              @actionStart('importbranchalloc', 'read')
              <li><a href="{{ URL::route('editor.importbranchalloc.index') }}">Import Branch Allocation</a></li>   
              @actionEnd

              <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Delete Transaction</a>
                <ul class="dropdown-menu">
                  @actionStart('deletebydetail', 'read')
                  <li><a href="{{ URL::route('editor.deletebydetail.index') }}">Delete Transaction by Detail</a></li>
                  @actionEnd

                  @actionStart('deletebypo', 'read')
                  <li><a href="{{ URL::route('editor.deletebypo.index') }}">Delete Transaction by PO</a></li>
                  @actionEnd
                  
                  @actionStart('deletebycontainer', 'read')
                  <li><a href="{{ URL::route('editor.deletebycontainer.index') }}">Delete Transaction by Container</a></li>
                  @actionEnd

                  @actionStart('deletebyitem', 'read')
                  <li><a href="{{ URL::route('editor.deletebyitem.index') }}">Delete Transaction by Item</a></li>
                  @actionEnd

                </ul>
              </li>
              @actionStart('reactivetransaction', 'read')
              <li><a href="{{ URL::route('editor.reactivetransaction.index') }}">Reactive Transaction</a></li>   
              @actionEnd

            </ul>
          </li>

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sticky-note"></i> Report <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu"> 
              @actionStart('summarygr', 'read')
              <li><a href="{{ URL::route('editor.summarygr.index') }}">Summary GR by Container</a></li>
              @actionEnd

              @actionStart('monitoringcontainer', 'read')
              <li><a href="{{ URL::route('editor.monitoringcontainer.index') }}">Monitoring Container</a></li>
              @actionEnd

              @actionStart('emkltemplate', 'read')
              <li><a href="{{ URL::route('editor.emkltemplate.index') }}">EMKL Template</a></li> 
              @actionEnd

              @actionStart('branchallocationreport', 'read')
              <li><a href="{{ URL::route('editor.branchallocationreport.index') }}">Branch Allocation Report</a>
              </li> 
              @actionEnd

              @actionStart('kkbpreport', 'read')
              <li><a href="{{ URL::route('editor.kkbpreport.index') }}">KKBP Report</a>
              </li> 
              @actionEnd

              @actionStart('pibreport', 'read')
              <li><a href="{{ URL::route('editor.pibreport.index') }}">PIB Report</a>
              </li> 
              @actionEnd

            </ul>
          </li>

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i> <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              @actionStart('user', 'read')
              <li><a href="{{ URL::route('editor.user.index') }}"> User List</a></li> 
              @actionEnd

              @actionStart('action', 'read')
              <li><a href="{{ URL::route('editor.action.index') }}">Action</a></li> 
              @actionEnd

              @actionStart('privilege', 'read')
              <li><a href="{{ URL::route('editor.privilege.index') }}"> Previlage</a></li> 
              @actionEnd

              @actionStart('module', 'read')
              <li><a href="{{ URL::route('editor.module.index') }}"> Module</a></li> 
              @actionEnd

              @actionStart('userbranch', 'read')
              <li><a href="{{ URL::route('editor.userbranch.index') }}"> User Branch</a></li>  
              @actionEnd

              @actionStart('popup', 'update')
              <li><a href="{{ URL::route('editor.popup.index') }}"> Popup</a></li>  
              @actionEnd

              @actionStart('userfilter', 'update')
              <li><a href="{{ URL::route('editor.userfilter.index') }}"> User Filter</a></li>  
              @actionEnd
              
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-question"></i> <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="{{ URL::route('editor.faq.index') }}"> Faq</a></li>  
            </ul>
          </li>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav"> 
            <!-- Notifications Menu -->
            <li class="dropdown messages-menu">
              <!-- Menu toggle button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="read_notif();">
                <i class="fa fa-bell-o" onclick="read_notif();"></i>
                @if(Auth::user()->read_notif == 1)
                <span class="label label-danger" id="count_notif">
                    <i class="fa fa-exclamation"></i>
                    @php
                      // echo count($notif_limit);
                    @endphp
                </span>
                @endif
              </a>
              <ul class="dropdown-menu">
                <li class="header">
                  You have 
                  @php
                    echo count($notif_limit);
                  @endphp
                 notifications</li>
                <li>
                  <!-- Inner Menu: contains the notifications -->
                  <ul class="menu">
                    @foreach ($notif_limit AS $notifs) 
                    <li><!-- start message -->
                      <a href="#">
                        <div class="pull-left">
                          <img src="{{Config::get('constants.path.uploads')}}/user/{{ $notifs->username }}/thumbnail/{{Auth::user()->filename}}" class="img-circle">
                        </div>
                        <b> @if($notifs->type == "Branch Allocation" && $notifs->currency_name != "") Capital Goods @else {{$notifs->type}} @endif</b>
                        <h4>
                          {{ $notifs->username }}
                          <small><i class="fa fa-clock-o"></i> {{ $notifs->updated_at }}</small>
                        </h4>
                        @if($notifs->type != 'EMKL Cost' && $notifs->type != 'Branch Allocation')
                        <p>
                          {!! $notifs->log_desc !!}
                        </p>
                        @endif
                        @if($notifs->type == 'EMKL Cost')
                        <p>
                          Period: {{ $notifs->datefrom }} to {{ $notifs->dateto }}<br> 
                          @if($notifs->emkl != $notifs->emkl_to)
                          EMKL: {{ $notifs->emkl }} to {{ $notifs->emkl_to }}<br> 
                          @endif
                          @if($notifs->plugin != $notifs->plugin_to)
                          &nbsp; Plugin: {{ $notifs->plugin }} to {{ $notifs->plugin_to }}<br> 
                          @endif
                          @if($notifs->ls != $notifs->ls_to)
                          &nbsp; LS: {{ $notifs->ls }} to {{ $notifs->ls_to }}<br> 
                          @endif
                          @if($notifs->kalog != $notifs->kalog_to)
                          &nbsp; Kalog: {{ $notifs->kalog }} to {{ $notifs->kalog_to }}
                          @endif
                        </p>
                        @else
                        @if($notifs->currency_name != "")
                          <p>
                            Currency: {{ $notifs->currency_name }} to {{ $notifs->currency_name_to }} <br>
                            &nbsp; Modal: {{ $notifs->totalmodal }} to {{ $notifs->totalmodal_to }} <br>
                          </p><br>
                          @endif
                          @if($notifs->type == "Branch Allocation")
                            <p>
                              Container No: {{ $notifs->container_no }} <br>
                              &nbsp; PO No: {{ $notifs->po_number }}<br>
                              &nbsp; Doc Date: {{ $notifs->po_date }}<br>
                              @if($notifs->tarik ==1)
                                &nbsp;Tarik: Yes<br>
                              @else
                                &nbsp;Tarik: No<br>
                              @endif
                            </p><br>
                            <p>
                              @php
                                foreach ($branch_global AS $branch_globals) {
                                  $field_name = $branch_globals->field_name;
                                  $field_nameto = $branch_globals->field_name."_to";
                                  echo "&nbsp; ".$branch_globals->branch_name.": "; echo $notifs->$field_name." to " .$notifs->$field_nameto." </br>"; 
                                }
                              @endphp
                            </p>
                           @endif
                          @endif
                      </a>
                    </li>
                    @endforeach
                    <!-- end notification -->
                  </ul>
                </li>
                <li class="footer"><a href="{{ URL::route('editor.notif.index') }}">View all</a></li>
              </ul>
            </li>
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                @if(Auth::user()->filename == null)
                <img src="{{Config::get('constants.path.img')}}/avatar5.png" class="user-image" alt="User Image">
                {{-- {{Auth::user()->username}} --}}
                @else
                <img src="{{Config::get('constants.path.uploads')}}/user/{{Auth::user()->username}}/thumbnail/{{Auth::user()->filename}}" class="user-image" alt="User Image">
                {{-- {{Auth::user()->username}} --}}
                @endif
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs"></span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  @if(Auth::user()->filename == null)
                  <img src="{{Config::get('constants.path.img')}}/avatar5.png" class="img-circle" alt="User Image">
                  @else
                  <img src="{{Config::get('constants.path.uploads')}}/user/{{Auth::user()->username}}/thumbnail/{{Auth::user()->filename}}" class="img-circle" alt="User Image">
                  @endif

                  <p>
                   {{Auth::user()->username}}
                  <small>Member since {{date("d-m-Y", strtotime(Auth::user()->created_at))}}</small>
                  </p>
                </li>
                <!-- Menu Body -->
                {{-- <li class="user-body">
                  <div class="row">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </div>
                </li> --}}
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                   {{-- <a href="{{ URL::route('editor.profile.show') }}" class="btn btn-default btn-flat">Profile</a> --}}
                 </div>
                 <div class="pull-right">
                  <form id="logout-form" action="{{ url('/logout') }}" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Logout</a>
                    </form>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
