<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="{{Config::get('constants.path.img')}}/favicon.png" type="image/gif" sizes="16x16">
  <title>LMU E-Modal</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- dataTables -->
  <!-- <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.css" /> -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datatables/extensions/datatables.min.css" />
  
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{Config::get('constants.path.bootstrap')}}/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"> -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/bower_components/select2/dist/css/select2.min.css"> 
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{Config::get('constants.path.css')}}/ionicons.min.css">
  <!-- Checkbox -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/checkbox/checkbox.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{Config::get('constants.path.scss')}}/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <!-- <link rel="stylesheet" href="{{Config::get('constants.path.css')}}/skins/_all-skins.min.css"> -->
  <!-- iCheck -->
  <!-- <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/iCheck/flat/blue.css"> -->
  <!-- Morris chart -->
  <!-- <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/morris/morris.css"> -->
  <!-- jvectormap -->
  <!-- <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/jvectormap/jquery-jvectormap-1.2.2.css"> -->


  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/jQueryConfirm/jquery-confirm.min.css">
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/jQueryConfirm/jquery-confirm.min.css">
   
  <!-- fixed coloumn -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datatables/fixedColumns.datatables.min.css">
  
  <!-- toastr notifications -->
  <link rel="stylesheet" href="{{Config::get('constants.path.css')}}/toastr.min.css">
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/daterangepicker/daterangepicker.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/daterangepicker/daterangepicker.css">
  <!-- Datetime Picker -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datetimepicker/bootstrap-datetimepicker.css">

  <style type="text/css">
    .container {
        margin-right: auto;
        margin-left: auto;
        padding-left: 20px;
        padding-right: 15px;
    }
  </style>

  <!-- jQuery 2.2.3 -->
  <script src="{{Config::get('constants.path.plugin')}}/jQuery/jquery-2.2.3.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{Config::get('constants.path.plugin')}}/jQueryUI/jquery-ui.min.js"></script>
   


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script type="text/javascript">
    // insert commas as thousands separators 
    function addCommas(n){
      var rx=  /(\d+)(\d{3})/;
      return String(n).replace(/^\d+/, function(w){
        while(rx.test(w)){
          w= w.replace(rx, '$1,$2');
        }
        return w;
      });
    }
// return integers and decimal numbers from input
// optionally truncates decimals- does not 'round' input
function validDigits(n, dec){
  n= n.replace(/[^\d\.]+/g, '');
  var ax1= n.indexOf('.'), ax2= -1;
  if(ax1!= -1){
    ++ax1;
    ax2= n.indexOf('.', ax1);
    if(ax2> ax1) n= n.substring(0, ax2);
    if(typeof dec=== 'number') n= n.substring(0, ax1+dec);
  }
  return n;
}
</script>
<style>
    .dropdown-submenu {
      position: relative;
    }

    .dropdown-submenu>.dropdown-menu {
      top: 0;
      left: 100%;
      margin-top: -6px;
      margin-left: -1px;
      -webkit-border-radius: 0 6px 6px 6px;
      -moz-border-radius: 0 6px 6px;
      border-radius: 0 6px 6px 6px;
    }

    .dropdown-submenu:hover>.dropdown-menu {
      display: block;
    }

    .dropdown-submenu>a:after {
      display: block;
      content: " ";
      float: right;
      width: 0;
      height: 0;
      border-color: transparent;
      border-style: solid;
      border-width: 5px 0 5px 5px;
      border-left-color: #ccc;
      margin-top: 5px;
      margin-right: -10px;
    }

    .dropdown-submenu:hover>a:after {
      border-left-color: #fff;
    }

    body {
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
      font-weight: 400;
      overflow-x: hidden;
      overflow-y: auto;
      font-size: 14px 
    }

    .content-headerFixed
    {
      position:fixed;
      background-color: #ecf0f5;
      margin-left:230px;
      left:0;
      right:0;
      padding: 15px 15px 0 15px;
    }
  
  .content-wrapper {
    min-height: 738px;
  }
    /*.skin-blue .main-header .navbar .nav > li > a {
      color: #fff;
      background-color: #101010;
  }*/

  

.page    { display: none; padding: 0 0.5em; } 

#loading {
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 100;
  width: 100vw;
  height: 100vh;
  background-color: rgba(192, 192, 192, 0.5);
  background-image: url("https://i.stack.imgur.com/MnyxU.gif");
  background-repeat: no-repeat;
  background-position: center;
}

</style>


</head>
<body class="hold-transition skin-blue fixed layout-top-nav" data-spy="scroll" data-target="#scrollspy">

<!-- <div class="wrapper">  -->

    @include('layouts.editor.headermobile') 
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <div class="page">
      @yield('content')
      </div>
      <div id="loading"></div>
    </div>
    <!-- /.content-wrapper -->

    @include('layouts.editor.footer')

  </div>
  <!-- ./wrapper -->

  @yield('modal')


  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
    function onReady(callback) {
      var intervalId = window.setInterval(function() {
        if (document.getElementsByTagName('body')[0] !== undefined) {
          window.clearInterval(intervalId);
          callback.call(this);
        }
      }, 1000);
    }

    function setVisible(selector, visible) {
      document.querySelector(selector).style.display = visible ? 'block' : 'none';
    }

    onReady(function() {
      setVisible('.page', true);
      setVisible('#loading', false);
    });

  </script>
  @yield('scripts')

  <!-- Bootstrap 3.3.6 -->
  <script src="{{Config::get('constants.path.bootstrap')}}/js/bootstrap.min.js"></script>
  <!-- Morris.js charts -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
  <!-- <script src="{{Config::get('constants.path.plugin')}}/morris/morris.min.js"></script> -->
  <!-- Sparkline -->
  <script src="{{Config::get('constants.path.plugin')}}/sparkline/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="{{Config::get('constants.path.plugin')}}/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- jQuery Knob Chart -->
  <!-- <script src="{{Config::get('constants.path.plugin')}}/knob/jquery.knob.js"></script> -->
  <!-- daterangepicker -->
  <script src="{{Config::get('constants.path.js')}}/moment.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="{{Config::get('constants.path.plugin')}}/datepicker/bootstrap-datepicker.js"></script>
  <!-- datetimepicker -->
  <script src="{{Config::get('constants.path.plugin')}}/datetimepicker/bootstrap-datetimepicker.min.js"></script>

  
  <!-- Slimscroll -->
  <script src="{{Config::get('constants.path.plugin')}}/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="{{Config::get('constants.path.plugin')}}/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="{{Config::get('constants.path.js')}}/app.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <!-- <script src="{{Config::get('constants.path.js')}}/pages/dashboard.js"></script> -->
  <script src="{{Config::get('constants.path.js')}}/dataTables.fixedColumns.min.js"></script>
 
  <!-- AdminLTE for demo purposes -->
  <script src="{{Config::get('constants.path.js')}}/demo.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/datatables/extensions/datatables.min.js"></script>
  <!-- <script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.datatables.min.js"></script>  -->
  <script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.tableTools.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.colVis.min.js"></script> 
  <script src="{{Config::get('constants.path.plugin')}}/select2/select2.full.min.js"></script> 

  <script src="{{Config::get('constants.path.plugin')}}/jQueryConfirm/jquery-confirm.min.js"></script>

  <script type="text/javascript" src="{{Config::get('constants.path.js')}}/toastr.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/handlebars/handlebars-v4.0.5.js"></script> 
  

<script type="text/javascript"> 
   $(document).ready(function(){
     $(".select2").select2();
     $(".select3").select2();
    $('#grfrom').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#grto').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#indate').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#dateholiday').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#dateperiod').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#begindate').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#enddate').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#paydate').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#idulfitridate').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#paymentdate').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#datefrom').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#dateto').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#datetrans').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
    $('#trainingfrom').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#trainingto').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#travellingfrom').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });  
    $('#travellingto').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#leavingfrom').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#leavingto').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    }); 
    $('#effectivedate').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });  
     $('#startdeduction').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });  
     $('#tlg_respon').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });  
     $('#delivery_date').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });  
    $('#ata').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });  
    $('#original_doc').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });  
    $('#pajak_pib').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });  
    $('#kt2').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });  
    $('#inspect_kt9').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });  
    $('#tlg_respon').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });  
 $('#eta').datepicker({
      sideBySide: true,
      format: 'yyyy-mm-dd',
    });
  });
</script>


</body>
</html>
