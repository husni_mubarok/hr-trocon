<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <link rel="icon" href="{{Config::get('constants.path.img')}}/favicon.png" type="image/gif" sizes="16x16">
  <title>LMU E-Modal - @yield('title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{Config::get('constants.path.bootstrap')}}/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/bower_components/select2/dist/css/select2.min.css"> 
  
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Checkbox -->
  <!-- <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/checkbox/checkbox.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="{{Config::get('constants.path.scss')}}/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <!-- <link rel="stylesheet" href="{{Config::get('constants.path.css')}}/skins/_all-skins.min.css"> -->
  <!-- iCheck -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <!-- <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/morris/morris.css"> -->
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- dataTables -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.css" />
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/daterangepicker/daterangepicker.css">
  <!-- Datetime Picker -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/datetimepicker/bootstrap-datetimepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/jQueryConfirm/jquery-confirm.min.css">
  <link rel="stylesheet" href="{{Config::get('constants.path.plugin')}}/jQueryConfirm/jquery-confirm.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/3.2.3/css/fixedColumns.dataTables.min.css">
  <!-- toastr notifications -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  <!-- jQuery 2.2.3 -->
  <script src="{{Config::get('constants.path.plugin')}}/jQuery/jquery-2.2.3.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  {{-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> --}}
  <!-- GHCI JS -->
  {{-- <script src="{{Config::get('constants.path.js')}}/ghci.js"></script> --}}
</head>
<body class="hold-transition skin-blue fixed layout-top-nav" data-spy="scroll" data-target="#scrollspy">
<!-- <div class="wrapper"> -->
@include('layouts.editor.header') 
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  @yield('content')
</div>
<!-- /.content-wrapper -->
<!-- ./wrapper -->
@yield('modal')

  @include('layouts.editor.footer')
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  @yield('scripts')
  <!-- Bootstrap 3.3.6 -->
  {{-- <script src="{{Config::get('constants.path.bootstrap')}}/js/bootstrap.min.js"></script> --}}
  <!-- Morris.js charts -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
  <!-- <script src="{{Config::get('constants.path.plugin')}}/morris/morris.min.js"></script> -->
  <!-- Sparkline -->
  <script src="{{Config::get('constants.path.plugin')}}/sparkline/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="{{Config::get('constants.path.plugin')}}/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="{{Config::get('constants.path.plugin')}}/knob/jquery.knob.js"></script>
  <!-- daterangepicker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="{{Config::get('constants.path.plugin')}}/datepicker/bootstrap-datepicker.js"></script>
  <!-- datetimepicker -->
  <script src="{{Config::get('constants.path.plugin')}}/datetimepicker/bootstrap-datetimepicker.min.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="{{Config::get('constants.path.plugin')}}/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="{{Config::get('constants.path.plugin')}}/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="{{Config::get('constants.path.plugin')}}/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="{{Config::get('constants.path.js')}}/app.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <!-- <script src="{{Config::get('constants.path.js')}}/pages/dashboard.js"></script> -->
  <!-- AdminLTE for demo purposes -->
  <script src="{{Config::get('constants.path.js')}}/demo.js"></script>
 <!--  <script src="{{Config::get('constants.path.plugin')}}/datatables/extensions/dataTables.min.js"></script> -->
  <script src="{{Config::get('constants.path.plugin')}}/datatables/jquery.dataTables.min.js"></script> 
  <script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.bootstrap.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.tableTools.min.js"></script>
  <script src="{{Config::get('constants.path.plugin')}}/datatables/dataTables.colVis.min.js"></script> 
  <script src="{{Config::get('constants.path.plugin')}}/select2/select2.full.min.js"></script> 
  <script src="https://cdn.datatables.net/fixedcolumns/3.2.3/js/dataTables.fixedColumns.min.js"></script>
  {{-- <script src="{{Config::get('constants.path.plugin')}}/jQueryConfirm/jquery-confirm.min.js"></script> --}}
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  {{-- <script src="http://troconadm.com/suntakhr/assets/plugins/handlebars/handlebars-v4.0.5.js"></script>   --}}
  <!-- GHCI JS ADD -->
  {{-- <script src="{{Config::get('constants.path.js')}}/ghciadd.js"></script> --}}
</body>
</html>
