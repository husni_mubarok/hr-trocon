<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Auth::routes();

Route::group(['middleware' => 'auth', 'namespace' => 'Editor'], function()
{
	Route::get('/', ['as' => 'editor.index', 'uses' => 'EditorController@index']);
});

//User Management
Route::group(['middleware' => 'auth', 'prefix' => 'editor', 'namespace' => 'Editor'], function()
{
	//Home
	Route::get('/', ['as' => 'editor.index', 'uses' => 'EditorController@index']);

	//Profile
		//detail
	Route::get('/profile', ['as' => 'editor.profile.show', 'uses' => 'ProfileController@show']);
		//edit
	Route::get('/profile/edit', ['as' => 'editor.profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('/profile/edit', ['as' => 'editor.profile.update', 'uses' => 'ProfileController@update']);
		//edit password
	Route::get('/profile/password', ['as' => 'editor.profile.edit_password', 'uses' => 'ProfileController@edit_password']);
	Route::put('/profile/password', ['as' => 'editor.profile.update_password', 'uses' => 'ProfileController@update_password']);

	//User
		//index
	Route::get('/user', ['middleware' => ['role:user|read'], 'as' => 'editor.user.index', 'uses' => 'UserController@index']);
		//create 
	Route::get('/user/create', ['middleware' => ['role:user|create'], 'as' => 'editor.user.create', 'uses' => 'UserController@create']);
	Route::post('/user/create', ['middleware' => ['role:user|create'], 'as' => 'editor.user.store', 'uses' => 'UserController@store']);
		//edit  
	Route::get('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'editor.user.edit', 'uses' => 'UserController@edit']);
	Route::put('/user/{id}/edit', ['middleware' => ['role:user|update'], 'as' => 'editor.user.update', 'uses' => 'UserController@update']);
		//delete
	Route::delete('/user/{id}/delete', ['middleware' => ['role:user|delete'], 'as' => 'editor.user.delete', 'uses' => 'UserController@delete']);

	//FILTER
	Route::post('/datefilter', ['as' => 'editor.datefilter', 'uses' => 'UserController@datefilter']);
	Route::post('/employeefilter', ['as' => 'editor.employeefilter', 'uses' => 'UserController@employeefilter']);
	Route::post('/periodfilter', ['as' => 'editor.periodfilter', 'uses' => 'UserController@periodfilter']);
	Route::post('/periodfilterthr', ['as' => 'editor.periodfilterthr', 'uses' => 'UserController@periodfilterthr']);
	Route::post('/periodfilteronly', ['as' => 'editor.periodfilteronly', 'uses' => 'UserController@periodfilteronly']);
	Route::post('/periodfilteremp', ['as' => 'editor.periodfilteremp', 'uses' => 'UserController@periodfilteremp']);
	Route::post('/periodfilterempyear', ['as' => 'editor.periodfilterempyear', 'uses' => 'UserController@periodfilterempyear']);

	//Module
		//index
	Route::get('/module', ['middleware' => ['role:module|read'], 'as' => 'editor.module.index', 'uses' => 'ModuleController@index']);
		//create
	Route::get('/module/create', ['middleware' => ['role:module|create'], 'as' => 'editor.module.create', 'uses' => 'ModuleController@create']);
	Route::post('/module/create', ['middleware' => ['role:module|create'], 'as' => 'editor.module.store', 'uses' => 'ModuleController@store']);
		//edit
	Route::get('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'editor.module.edit', 'uses' => 'ModuleController@edit']);
	Route::put('/module/{id}/edit', ['middleware' => ['role:module|update'], 'as' => 'editor.module.update', 'uses' => 'ModuleController@update']);
		//delete
	Route::delete('/module/{id}/delete', ['middleware' => ['role:module|delete'], 'as' => 'editor.module.delete', 'uses' => 'ModuleController@delete']);

	//Action
		//index
	Route::get('/action', ['middleware' => ['role:action|read'], 'as' => 'editor.action.index', 'uses' => 'ActionController@index']);
		//create
	Route::get('/action/create', ['middleware' => ['role:action|create'], 'as' => 'editor.action.create', 'uses' => 'ActionController@create']);
	Route::post('/action/create', ['middleware' => ['role:action|create'], 'as' => 'editor.action.store', 'uses' => 'ActionController@store']);
		//edit
	Route::get('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'editor.action.edit', 'uses' => 'ActionController@edit']);
	Route::put('/action/{id}/edit', ['middleware' => ['role:action|update'], 'as' => 'editor.action.update', 'uses' => 'ActionController@update']);
		//delete
	Route::delete('/action/{id}/delete', ['middleware' => ['role:action|delete'], 'as' => 'editor.action.delete', 'uses' => 'ActionController@delete']);

	//Privilege
		//index
	Route::get('/privilege', ['middleware' => ['role:privilege|read'], 'as' => 'editor.privilege.index', 'uses' => 'PrivilegeController@index']);
		//create
	Route::get('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'editor.privilege.create', 'uses' => 'PrivilegeController@create']);
	Route::post('/privilege/create', ['middleware' => ['role:privilege|create'], 'as' => 'editor.privilege.store', 'uses' => 'PrivilegeController@store']);
		//edit
	Route::get('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'editor.privilege.edit', 'uses' => 'PrivilegeController@edit']);
	Route::put('/privilege/{id}/edit', ['middleware' => ['role:privilege|update'], 'as' => 'editor.privilege.update', 'uses' => 'PrivilegeController@update']);
		//delete
	Route::delete('/privilege/{id}/delete', ['middleware' => ['role:privilege|delete'], 'as' => 'editor.privilege.delete', 'uses' => 'PrivilegeController@delete']);

	//Document
		//index
	Route::get('/document', ['middleware' => ['role:document|read'], 'as' => 'editor.document.index', 'uses' => 'DocumentController@index']);
	Route::get('/document/data', ['as' => 'editor.document.data', 'uses' => 'DocumentController@data']);
	
		//create
	Route::get('/document/create', ['middleware' => ['role:document|create'], 'as' => 'editor.document.create', 'uses' => 'DocumentController@create']);
	Route::post('/document/create', ['middleware' => ['role:document|create'], 'as' => 'editor.document.store', 'uses' => 'DocumentController@store']);
		//edit
	Route::get('/document/{id}/edit', ['middleware' => ['role:document|update'], 'as' => 'editor.document.edit', 'uses' => 'DocumentController@edit']);
	Route::put('/document/{id}/edit', ['middleware' => ['role:document|update'], 'as' => 'editor.document.update', 'uses' => 'DocumentController@update']);
		//delete
	Route::delete('/document/delete/{id}', ['middleware' => ['role:document|delete'], 'as' => 'editor.document.delete', 'uses' => 'DocumentController@delete']);
	Route::post('/document/deletebulk', ['middleware' => ['role:document|delete'], 'as' => 'editor.document.deletebulk', 'uses' => 'DocumentController@deletebulk']);

	//City
		//index
	Route::get('/city', ['middleware' => ['role:city|read'], 'as' => 'editor.city.index', 'uses' => 'CityController@index']);
	Route::get('/city/data', ['as' => 'editor.city.data', 'uses' => 'CityController@data']);
	
		//create
	Route::get('/city/create', ['middleware' => ['role:city|create'], 'as' => 'editor.city.create', 'uses' => 'CityController@create']);
	Route::post('/city/create', ['middleware' => ['role:city|create'], 'as' => 'editor.city.store', 'uses' => 'CityController@store']);
		//edit
	Route::get('/city/edit/{id}', ['middleware' => ['role:city|update'], 'as' => 'editor.city.edit', 'uses' => 'CityController@edit']);
	Route::put('/city/edit/{id}', ['middleware' => ['role:city|update'], 'as' => 'editor.city.update', 'uses' => 'CityController@update']);
		//delete
	Route::delete('/city/delete/{id}', ['middleware' => ['role:city|delete'], 'as' => 'editor.city.delete', 'uses' => 'CityController@delete']);
	Route::post('/city/deletebulk', ['middleware' => ['role:city|delete'], 'as' => 'editor.city.deletebulk', 'uses' => 'CityController@deletebulk']);


	//Absencetype
		//index
	Route::get('/absencetype', ['middleware' => ['role:absencetype|read'], 'as' => 'editor.absencetype.index', 'uses' => 'AbsencetypeController@index']);
	Route::get('/absencetype/data', ['as' => 'editor.absencetype.data', 'uses' => 'AbsencetypeController@data']);
	
		//create
	Route::get('/absencetype/create', ['middleware' => ['role:absencetype|create'], 'as' => 'editor.absencetype.create', 'uses' => 'AbsencetypeController@create']);
	Route::post('/absencetype/create', ['middleware' => ['role:absencetype|create'], 'as' => 'editor.absencetype.store', 'uses' => 'AbsencetypeController@store']);
		//edit
	Route::get('/absencetype/edit/{id}', ['middleware' => ['role:absencetype|update'], 'as' => 'editor.absencetype.edit', 'uses' => 'AbsencetypeController@edit']);
	Route::put('/absencetype/edit/{id}', ['middleware' => ['role:absencetype|update'], 'as' => 'editor.absencetype.update', 'uses' => 'AbsencetypeController@update']);
		//delete
	Route::delete('/absencetype/delete/{id}', ['middleware' => ['role:absencetype|delete'], 'as' => 'editor.absencetype.delete', 'uses' => 'AbsencetypeController@delete']);
	Route::post('/absencetype/deletebulk', ['middleware' => ['role:absencetype|delete'], 'as' => 'editor.absencetype.deletebulk', 'uses' => 'AbsencetypeController@deletebulk']);


	//Payperiod
		//index
	Route::get('/payperiod', ['middleware' => ['role:payperiod|read'], 'as' => 'editor.payperiod.index', 'uses' => 'PayperiodController@index']);
	Route::get('/payperiod/data', ['as' => 'editor.payperiod.data', 'uses' => 'PayperiodController@data']);
	
		//create
	Route::get('/payperiod/create', ['middleware' => ['role:payperiod|create'], 'as' => 'editor.payperiod.create', 'uses' => 'PayperiodController@create']);
	Route::post('/payperiod/create', ['middleware' => ['role:payperiod|create'], 'as' => 'editor.payperiod.store', 'uses' => 'PayperiodController@store']);
		//edit
	Route::get('/payperiod/edit/{id}', ['middleware' => ['role:payperiod|update'], 'as' => 'editor.payperiod.edit', 'uses' => 'PayperiodController@edit']);
	Route::put('/payperiod/edit/{id}', ['middleware' => ['role:payperiod|update'], 'as' => 'editor.payperiod.update', 'uses' => 'PayperiodController@update']);
		//delete
	Route::delete('/payperiod/delete/{id}', ['middleware' => ['role:payperiod|delete'], 'as' => 'editor.payperiod.delete', 'uses' => 'PayperiodController@delete']);
	Route::post('/payperiod/deletebulk', ['middleware' => ['role:payperiod|delete'], 'as' => 'editor.payperiod.deletebulk', 'uses' => 'PayperiodController@deletebulk']);

	//Payperiodbonus
		//index
	Route::get('/payperiodbonus', ['middleware' => ['role:payperiodbonus|read'], 'as' => 'editor.payperiodbonus.index', 'uses' => 'PayperiodbonusController@index']);
	Route::get('/payperiodbonus/data', ['as' => 'editor.payperiodbonus.data', 'uses' => 'PayperiodbonusController@data']);
	
		//create
	Route::get('/payperiodbonus/create', ['middleware' => ['role:payperiodbonus|create'], 'as' => 'editor.payperiodbonus.create', 'uses' => 'PayperiodbonusController@create']);
	Route::post('/payperiodbonus/create', ['middleware' => ['role:payperiodbonus|create'], 'as' => 'editor.payperiodbonus.store', 'uses' => 'PayperiodbonusController@store']);
		//edit
	Route::get('/payperiodbonus/edit/{id}', ['middleware' => ['role:payperiodbonus|update'], 'as' => 'editor.payperiodbonus.edit', 'uses' => 'PayperiodbonusController@edit']);
	Route::put('/payperiodbonus/edit/{id}', ['middleware' => ['role:payperiodbonus|update'], 'as' => 'editor.payperiodbonus.update', 'uses' => 'PayperiodbonusController@update']);
		//delete
	Route::delete('/payperiodbonus/delete/{id}', ['middleware' => ['role:payperiodbonus|delete'], 'as' => 'editor.payperiodbonus.delete', 'uses' => 'PayperiodbonusController@delete']);
	Route::post('/payperiodbonus/deletebulk', ['middleware' => ['role:payperiodbonus|delete'], 'as' => 'editor.payperiodbonus.deletebulk', 'uses' => 'PayperiodbonusController@deletebulk']);

	//Taxstatus
		//index
	Route::get('/taxstatus', ['middleware' => ['role:taxstatus|read'], 'as' => 'editor.taxstatus.index', 'uses' => 'TaxstatusController@index']);
	Route::get('/taxstatus/data', ['as' => 'editor.taxstatus.data', 'uses' => 'TaxstatusController@data']);
	
		//create
	Route::get('/taxstatus/create', ['middleware' => ['role:taxstatus|create'], 'as' => 'editor.taxstatus.create', 'uses' => 'TaxstatusController@create']);
	Route::post('/taxstatus/create', ['middleware' => ['role:taxstatus|create'], 'as' => 'editor.taxstatus.store', 'uses' => 'TaxstatusController@store']);
		//edit
	Route::get('/taxstatus/edit/{id}', ['middleware' => ['role:taxstatus|update'], 'as' => 'editor.taxstatus.edit', 'uses' => 'TaxstatusController@edit']);
	Route::put('/taxstatus/edit/{id}', ['middleware' => ['role:taxstatus|update'], 'as' => 'editor.taxstatus.update', 'uses' => 'TaxstatusController@update']);
		//delete
	Route::delete('/taxstatus/delete/{id}', ['middleware' => ['role:taxstatus|delete'], 'as' => 'editor.taxstatus.delete', 'uses' => 'TaxstatusController@delete']);
	Route::post('/taxstatus/deletebulk', ['middleware' => ['role:taxstatus|delete'], 'as' => 'editor.taxstatus.deletebulk', 'uses' => 'TaxstatusController@deletebulk']);

	//Employeesalary
		//index
	Route::get('/employeesalary', ['middleware' => ['role:employeesalary|read'], 'as' => 'editor.employeesalary.index', 'uses' => 'EmployeesalaryController@index']);
	Route::get('/employeesalary/data', ['as' => 'editor.employeesalary.data', 'uses' => 'EmployeesalaryController@data']);
	
		//create
	Route::get('/employeesalary/create', ['middleware' => ['role:employeesalary|create'], 'as' => 'editor.employeesalary.create', 'uses' => 'EmployeesalaryController@create']);
	Route::post('/employeesalary/create', ['middleware' => ['role:employeesalary|create'], 'as' => 'editor.employeesalary.store', 'uses' => 'EmployeesalaryController@store']);
		//edit
	Route::get('/employeesalary/edit/{id}', ['middleware' => ['role:employeesalary|update'], 'as' => 'editor.employeesalary.edit', 'uses' => 'EmployeesalaryController@edit']);
	Route::put('/employeesalary/edit/{id}', ['middleware' => ['role:employeesalary|update'], 'as' => 'editor.employeesalary.update', 'uses' => 'EmployeesalaryController@update']);
		//delete
	Route::delete('/employeesalary/delete/{id}', ['middleware' => ['role:employeesalary|delete'], 'as' => 'editor.employeesalary.delete', 'uses' => 'EmployeesalaryController@delete']);
	Route::post('/employeesalary/deletebulk', ['middleware' => ['role:employeesalary|delete'], 'as' => 'editor.employeesalary.deletebulk', 'uses' => 'EmployeesalaryController@deletebulk']);

	//Medicaltype
		//index
	Route::get('/medicaltype', ['middleware' => ['role:medicaltype|read'], 'as' => 'editor.medicaltype.index', 'uses' => 'MedicaltypeController@index']);
	Route::get('/medicaltype/data', ['as' => 'editor.medicaltype.data', 'uses' => 'MedicaltypeController@data']);
	
		//create
	Route::get('/medicaltype/create', ['middleware' => ['role:medicaltype|create'], 'as' => 'editor.medicaltype.create', 'uses' => 'MedicaltypeController@create']);
	Route::post('/medicaltype/create', ['middleware' => ['role:medicaltype|create'], 'as' => 'editor.medicaltype.store', 'uses' => 'MedicaltypeController@store']);
		//edit
	Route::get('/medicaltype/edit/{id}', ['middleware' => ['role:medicaltype|update'], 'as' => 'editor.medicaltype.edit', 'uses' => 'MedicaltypeController@edit']);
	Route::put('/medicaltype/edit/{id}', ['middleware' => ['role:medicaltype|update'], 'as' => 'editor.medicaltype.update', 'uses' => 'MedicaltypeController@update']);
		//delete
	Route::delete('/medicaltype/delete/{id}', ['middleware' => ['role:medicaltype|delete'], 'as' => 'editor.medicaltype.delete', 'uses' => 'MedicaltypeController@delete']);
	Route::post('/medicaltype/deletebulk', ['middleware' => ['role:medicaltype|delete'], 'as' => 'editor.medicaltype.deletebulk', 'uses' => 'MedicaltypeController@deletebulk']);

	//Umr
		//index
	Route::get('/umr', ['middleware' => ['role:umr|read'], 'as' => 'editor.umr.index', 'uses' => 'UmrController@index']);
	Route::get('/umr/data', ['as' => 'editor.umr.data', 'uses' => 'UmrController@data']);
	
		//create
	Route::get('/umr/create', ['middleware' => ['role:umr|create'], 'as' => 'editor.umr.create', 'uses' => 'UmrController@create']);
	Route::post('/umr/create', ['middleware' => ['role:umr|create'], 'as' => 'editor.umr.store', 'uses' => 'UmrController@store']);
		//edit
	Route::get('/umr/edit/{id}', ['middleware' => ['role:umr|update'], 'as' => 'editor.umr.edit', 'uses' => 'UmrController@edit']);
	Route::put('/umr/edit/{id}', ['middleware' => ['role:umr|update'], 'as' => 'editor.umr.update', 'uses' => 'UmrController@update']);
		//delete
	Route::delete('/umr/delete/{id}', ['middleware' => ['role:umr|delete'], 'as' => 'editor.umr.delete', 'uses' => 'UmrController@delete']);
	Route::post('/umr/deletebulk', ['middleware' => ['role:umr|delete'], 'as' => 'editor.umr.deletebulk', 'uses' => 'UmrController@deletebulk']);

	//Tarif
		//index
	Route::get('/tarif', ['middleware' => ['role:tarif|read'], 'as' => 'editor.tarif.index', 'uses' => 'TarifController@index']);
	Route::get('/tarif/data', ['as' => 'editor.tarif.data', 'uses' => 'TarifController@data']);
	
		//create
	Route::get('/tarif/create', ['middleware' => ['role:tarif|create'], 'as' => 'editor.tarif.create', 'uses' => 'TarifController@create']);
	Route::post('/tarif/create', ['middleware' => ['role:tarif|create'], 'as' => 'editor.tarif.store', 'uses' => 'TarifController@store']);
		//edit
	Route::get('/tarif/edit/{id}', ['middleware' => ['role:tarif|update'], 'as' => 'editor.tarif.edit', 'uses' => 'TarifController@edit']);
	Route::put('/tarif/edit/{id}', ['middleware' => ['role:tarif|update'], 'as' => 'editor.tarif.update', 'uses' => 'TarifController@update']);
		//delete
	Route::delete('/tarif/delete/{id}', ['middleware' => ['role:tarif|delete'], 'as' => 'editor.tarif.delete', 'uses' => 'TarifController@delete']);
	Route::post('/tarif/deletebulk', ['middleware' => ['role:tarif|delete'], 'as' => 'editor.tarif.deletebulk', 'uses' => 'TarifController@deletebulk']);

	//Iuranpensiun
		//index
	Route::get('/iuranpensiun', ['middleware' => ['role:iuranpensiun|read'], 'as' => 'editor.iuranpensiun.index', 'uses' => 'IuranpensiunController@index']);
	Route::get('/iuranpensiun/data', ['as' => 'editor.iuranpensiun.data', 'uses' => 'IuranpensiunController@data']);
	
		//create
	Route::get('/iuranpensiun/create', ['middleware' => ['role:iuranpensiun|create'], 'as' => 'editor.iuranpensiun.create', 'uses' => 'IuranpensiunController@create']);
	Route::post('/iuranpensiun/create', ['middleware' => ['role:iuranpensiun|create'], 'as' => 'editor.iuranpensiun.store', 'uses' => 'IuranpensiunController@store']);
		//edit
	Route::get('/iuranpensiun/edit/{id}', ['middleware' => ['role:iuranpensiun|update'], 'as' => 'editor.iuranpensiun.edit', 'uses' => 'IuranpensiunController@edit']);
	Route::put('/iuranpensiun/edit/{id}', ['middleware' => ['role:iuranpensiun|update'], 'as' => 'editor.iuranpensiun.update', 'uses' => 'IuranpensiunController@update']);
		//delete
	Route::delete('/iuranpensiun/delete/{id}', ['middleware' => ['role:iuranpensiun|delete'], 'as' => 'editor.iuranpensiun.delete', 'uses' => 'IuranpensiunController@delete']);
	Route::post('/iuranpensiun/deletebulk', ['middleware' => ['role:iuranpensiun|delete'], 'as' => 'editor.iuranpensiun.deletebulk', 'uses' => 'IuranpensiunController@deletebulk']);

	//Religion
		//index
	Route::get('/religion', ['middleware' => ['role:religion|read'], 'as' => 'editor.religion.index', 'uses' => 'ReligionController@index']);
	Route::get('/religion/data', ['as' => 'editor.religion.data', 'uses' => 'ReligionController@data']);
	
		//create
	Route::get('/religion/create', ['middleware' => ['role:religion|create'], 'as' => 'editor.religion.create', 'uses' => 'ReligionController@create']);
	Route::post('/religion/create', ['middleware' => ['role:religion|create'], 'as' => 'editor.religion.store', 'uses' => 'ReligionController@store']);
		//edit
	Route::get('/religion/edit/{id}', ['middleware' => ['role:religion|update'], 'as' => 'editor.religion.edit', 'uses' => 'ReligionController@edit']);
	Route::put('/religion/edit/{id}', ['middleware' => ['role:religion|update'], 'as' => 'editor.religion.update', 'uses' => 'ReligionController@update']);
		//delete
	Route::delete('/religion/delete/{id}', ['middleware' => ['role:religion|delete'], 'as' => 'editor.religion.delete', 'uses' => 'ReligionController@delete']);
	Route::post('/religion/deletebulk', ['middleware' => ['role:religion|delete'], 'as' => 'editor.religion.deletebulk', 'uses' => 'ReligionController@deletebulk']);

	//Golongan
		//index
	Route::get('/golongan', ['middleware' => ['role:golongan|read'], 'as' => 'editor.golongan.index', 'uses' => 'GolonganController@index']);
	Route::get('/golongan/data', ['as' => 'editor.golongan.data', 'uses' => 'GolonganController@data']);
	
		//create
	Route::get('/golongan/create', ['middleware' => ['role:golongan|create'], 'as' => 'editor.golongan.create', 'uses' => 'GolonganController@create']);
	Route::post('/golongan/create', ['middleware' => ['role:golongan|create'], 'as' => 'editor.golongan.store', 'uses' => 'GolonganController@store']);
		//edit
	Route::get('/golongan/edit/{id}', ['middleware' => ['role:golongan|update'], 'as' => 'editor.golongan.edit', 'uses' => 'GolonganController@edit']);
	Route::put('/golongan/edit/{id}', ['middleware' => ['role:golongan|update'], 'as' => 'editor.golongan.update', 'uses' => 'GolonganController@update']);
		//delete
	Route::delete('/golongan/delete/{id}', ['middleware' => ['role:golongan|delete'], 'as' => 'editor.golongan.delete', 'uses' => 'GolonganController@delete']);
	Route::post('/golongan/deletebulk', ['middleware' => ['role:golongan|delete'], 'as' => 'editor.golongan.deletebulk', 'uses' => 'GolonganController@deletebulk']);

	//Educationlevel
		//index
	Route::get('/educationlevel', ['middleware' => ['role:educationlevel|read'], 'as' => 'editor.educationlevel.index', 'uses' => 'EducationlevelController@index']);
	Route::get('/educationlevel/data', ['as' => 'editor.educationlevel.data', 'uses' => 'EducationlevelController@data']);
	
		//create
	Route::get('/educationlevel/create', ['middleware' => ['role:educationlevel|create'], 'as' => 'editor.educationlevel.create', 'uses' => 'EducationlevelController@create']);
	Route::post('/educationlevel/create', ['middleware' => ['role:educationlevel|create'], 'as' => 'editor.educationlevel.store', 'uses' => 'EducationlevelController@store']);
		//edit
	Route::get('/educationlevel/edit/{id}', ['middleware' => ['role:educationlevel|update'], 'as' => 'editor.educationlevel.edit', 'uses' => 'EducationlevelController@edit']);
	Route::put('/educationlevel/edit/{id}', ['middleware' => ['role:educationlevel|update'], 'as' => 'editor.educationlevel.update', 'uses' => 'EducationlevelController@update']);
		//delete
	Route::delete('/educationlevel/delete/{id}', ['middleware' => ['role:educationlevel|delete'], 'as' => 'editor.educationlevel.delete', 'uses' => 'EducationlevelController@delete']);
	Route::post('/educationlevel/deletebulk', ['middleware' => ['role:educationlevel|delete'], 'as' => 'editor.educationlevel.deletebulk', 'uses' => 'EducationlevelController@deletebulk']);

	//Educationmajor
		//index
	Route::get('/educationmajor', ['middleware' => ['role:educationmajor|read'], 'as' => 'editor.educationmajor.index', 'uses' => 'EducationmajorController@index']);
	Route::get('/educationmajor/data', ['as' => 'editor.educationmajor.data', 'uses' => 'EducationmajorController@data']);
	
		//create
	Route::get('/educationmajor/create', ['middleware' => ['role:educationmajor|create'], 'as' => 'editor.educationmajor.create', 'uses' => 'EducationmajorController@create']);
	Route::post('/educationmajor/create', ['middleware' => ['role:educationmajor|create'], 'as' => 'editor.educationmajor.store', 'uses' => 'EducationmajorController@store']);
		//edit
	Route::get('/educationmajor/edit/{id}', ['middleware' => ['role:educationmajor|update'], 'as' => 'editor.educationmajor.edit', 'uses' => 'EducationmajorController@edit']);
	Route::put('/educationmajor/edit/{id}', ['middleware' => ['role:educationmajor|update'], 'as' => 'editor.educationmajor.update', 'uses' => 'EducationmajorController@update']);
		//delete
	Route::delete('/educationmajor/delete/{id}', ['middleware' => ['role:educationmajor|delete'], 'as' => 'editor.educationmajor.delete', 'uses' => 'EducationmajorController@delete']);
	Route::post('/educationmajor/deletebulk', ['middleware' => ['role:educationmajor|delete'], 'as' => 'editor.educationmajor.deletebulk', 'uses' => 'EducationmajorController@deletebulk']);


	//Educationtype
		//index
	Route::get('/educationtype', ['middleware' => ['role:educationtype|read'], 'as' => 'editor.educationtype.index', 'uses' => 'EducationtypeController@index']);
	Route::get('/educationtype/data', ['as' => 'editor.educationtype.data', 'uses' => 'EducationtypeController@data']);
	
		//create
	Route::get('/educationtype/create', ['middleware' => ['role:educationtype|create'], 'as' => 'editor.educationtype.create', 'uses' => 'EducationtypeController@create']);
	Route::post('/educationtype/create', ['middleware' => ['role:educationtype|create'], 'as' => 'editor.educationtype.store', 'uses' => 'EducationtypeController@store']);
		//edit
	Route::get('/educationtype/edit/{id}', ['middleware' => ['role:educationtype|update'], 'as' => 'editor.educationtype.edit', 'uses' => 'EducationtypeController@edit']);
	Route::put('/educationtype/edit/{id}', ['middleware' => ['role:educationtype|update'], 'as' => 'editor.educationtype.update', 'uses' => 'EducationtypeController@update']);
		//delete
	Route::delete('/educationtype/delete/{id}', ['middleware' => ['role:educationtype|delete'], 'as' => 'editor.educationtype.delete', 'uses' => 'EducationtypeController@delete']);
	Route::post('/educationtype/deletebulk', ['middleware' => ['role:educationtype|delete'], 'as' => 'editor.educationtype.deletebulk', 'uses' => 'EducationtypeController@deletebulk']);

	//Travelingtype
		//index
	Route::get('/travelingtype', ['middleware' => ['role:travelingtype|read'], 'as' => 'editor.travelingtype.index', 'uses' => 'TravelingtypeController@index']);
	Route::get('/travelingtype/data', ['as' => 'editor.travelingtype.data', 'uses' => 'TravelingtypeController@data']); 
		//create
	Route::get('/travelingtype/create', ['middleware' => ['role:travelingtype|create'], 'as' => 'editor.travelingtype.create', 'uses' => 'TravelingtypeController@create']);
	Route::post('/travelingtype/create', ['middleware' => ['role:travelingtype|create'], 'as' => 'editor.travelingtype.store', 'uses' => 'TravelingtypeController@store']);
		//edit
	Route::get('/travelingtype/edit/{id}', ['middleware' => ['role:travelingtype|update'], 'as' => 'editor.travelingtype.edit', 'uses' => 'TravelingtypeController@edit']);
	Route::put('/travelingtype/edit/{id}', ['middleware' => ['role:travelingtype|update'], 'as' => 'editor.travelingtype.update', 'uses' => 'TravelingtypeController@update']);
		//delete
	Route::delete('/travelingtype/delete/{id}', ['middleware' => ['role:travelingtype|delete'], 'as' => 'editor.travelingtype.delete', 'uses' => 'TravelingtypeController@delete']);
	Route::post('/travelingtype/deletebulk', ['middleware' => ['role:travelingtype|delete'], 'as' => 'editor.travelingtype.deletebulk', 'uses' => 'TravelingtypeController@deletebulk']);

	//Employeestatus
		//index
	Route::get('/employeestatus', ['middleware' => ['role:employeestatus|read'], 'as' => 'editor.employeestatus.index', 'uses' => 'EmployeestatusController@index']);
	Route::get('/employeestatus/data', ['as' => 'editor.employeestatus.data', 'uses' => 'EmployeestatusController@data']);
	
		//create
	Route::get('/employeestatus/create', ['middleware' => ['role:employeestatus|create'], 'as' => 'editor.employeestatus.create', 'uses' => 'EmployeestatusController@create']);
	Route::post('/employeestatus/create', ['middleware' => ['role:employeestatus|create'], 'as' => 'editor.employeestatus.store', 'uses' => 'EmployeestatusController@store']);
		//edit
	Route::get('/employeestatus/edit/{id}', ['middleware' => ['role:employeestatus|update'], 'as' => 'editor.employeestatus.edit', 'uses' => 'EmployeestatusController@edit']);
	Route::put('/employeestatus/edit/{id}', ['middleware' => ['role:employeestatus|update'], 'as' => 'editor.employeestatus.update', 'uses' => 'EmployeestatusController@update']);
		//delete
	Route::delete('/employeestatus/delete/{id}', ['middleware' => ['role:employeestatus|delete'], 'as' => 'editor.employeestatus.delete', 'uses' => 'EmployeestatusController@delete']);
	Route::post('/employeestatus/deletebulk', ['middleware' => ['role:employeestatus|delete'], 'as' => 'editor.employeestatus.deletebulk', 'uses' => 'EmployeestatusController@deletebulk']);

	//Travelingitem
		//index
	Route::get('/travelingitem', ['middleware' => ['role:travelingitem|read'], 'as' => 'editor.travelingitem.index', 'uses' => 'TravelingitemController@index']);
	Route::get('/travelingitem/data', ['as' => 'editor.travelingitem.data', 'uses' => 'TravelingitemController@data']); 
		//create
	Route::get('/travelingitem/create', ['middleware' => ['role:travelingitem|create'], 'as' => 'editor.travelingitem.create', 'uses' => 'TravelingitemController@create']);
	Route::post('/travelingitem/create', ['middleware' => ['role:travelingitem|create'], 'as' => 'editor.travelingitem.store', 'uses' => 'TravelingitemController@store']);
		//edit
	Route::get('/travelingitem/edit/{id}', ['middleware' => ['role:travelingitem|update'], 'as' => 'editor.travelingitem.edit', 'uses' => 'TravelingitemController@edit']);
	Route::put('/travelingitem/edit/{id}', ['middleware' => ['role:travelingitem|update'], 'as' => 'editor.travelingitem.update', 'uses' => 'TravelingitemController@update']);
		//delete
	Route::delete('/travelingitem/delete/{id}', ['middleware' => ['role:travelingitem|delete'], 'as' => 'editor.travelingitem.delete', 'uses' => 'TravelingitemController@delete']);
	Route::post('/travelingitem/deletebulk', ['middleware' => ['role:travelingitem|delete'], 'as' => 'editor.travelingitem.deletebulk', 'uses' => 'TravelingitemController@deletebulk']);

	//Year
		//index
	Route::get('/year', ['middleware' => ['role:year|read'], 'as' => 'editor.year.index', 'uses' => 'YearController@index']);
	Route::get('/year/data', ['as' => 'editor.year.data', 'uses' => 'YearController@data']); 
		//create
	Route::get('/year/create', ['middleware' => ['role:year|create'], 'as' => 'editor.year.create', 'uses' => 'YearController@create']);
	Route::post('/year/create', ['middleware' => ['role:year|create'], 'as' => 'editor.year.store', 'uses' => 'YearController@store']);
		//edit
	Route::get('/year/edit/{id}', ['middleware' => ['role:year|update'], 'as' => 'editor.year.edit', 'uses' => 'YearController@edit']);
	Route::put('/year/edit/{id}', ['middleware' => ['role:year|update'], 'as' => 'editor.year.update', 'uses' => 'YearController@update']);
		//delete
	Route::delete('/year/delete/{id}', ['middleware' => ['role:year|delete'], 'as' => 'editor.year.delete', 'uses' => 'YearController@delete']);
	Route::post('/year/deletebulk', ['middleware' => ['role:year|delete'], 'as' => 'editor.year.deletebulk', 'uses' => 'YearController@deletebulk']);

	//Country
		//index
	Route::get('/country', ['middleware' => ['role:country|read'], 'as' => 'editor.country.index', 'uses' => 'CountryController@index']);
	Route::get('/country/data', ['as' => 'editor.country.data', 'uses' => 'CountryController@data']);
	
		//create
	Route::get('/country/create', ['middleware' => ['role:country|create'], 'as' => 'editor.country.create', 'uses' => 'CountryController@create']);
	Route::post('/country/create', ['middleware' => ['role:country|create'], 'as' => 'editor.country.store', 'uses' => 'CountryController@store']);
		//edit
	Route::get('/country/edit/{id}', ['middleware' => ['role:country|update'], 'as' => 'editor.country.edit', 'uses' => 'CountryController@edit']);
	Route::put('/country/edit/{id}', ['middleware' => ['role:country|update'], 'as' => 'editor.country.update', 'uses' => 'CountryController@update']);
		//delete
	Route::delete('/country/delete/{id}', ['middleware' => ['role:country|delete'], 'as' => 'editor.country.delete', 'uses' => 'CountryController@delete']);
	Route::post('/country/deletebulk', ['middleware' => ['role:country|delete'], 'as' => 'editor.country.deletebulk', 'uses' => 'CountryController@deletebulk']);

	//Position
		//index
	Route::get('/position', ['middleware' => ['role:position|read'], 'as' => 'editor.position.index', 'uses' => 'PositionController@index']);
	Route::get('/position/data', ['as' => 'editor.position.data', 'uses' => 'PositionController@data']);
	
		//create
	Route::get('/position/create', ['middleware' => ['role:position|create'], 'as' => 'editor.position.create', 'uses' => 'PositionController@create']);
	Route::post('/position/create', ['middleware' => ['role:position|create'], 'as' => 'editor.position.store', 'uses' => 'PositionController@store']);
		//edit
	Route::get('/position/edit/{id}', ['middleware' => ['role:position|update'], 'as' => 'editor.position.edit', 'uses' => 'PositionController@edit']);
	Route::put('/position/edit/{id}', ['middleware' => ['role:position|update'], 'as' => 'editor.position.update', 'uses' => 'PositionController@update']);
		//delete
	Route::delete('/position/delete/{id}', ['middleware' => ['role:position|delete'], 'as' => 'editor.position.delete', 'uses' => 'PositionController@delete']);
	Route::post('/position/deletebulk', ['middleware' => ['role:position|delete'], 'as' => 'editor.position.deletebulk', 'uses' => 'PositionController@deletebulk']);


	//Unit
		//index
	Route::get('/unit', ['middleware' => ['role:unit|read'], 'as' => 'editor.unit.index', 'uses' => 'UnitController@index']);
	Route::get('/unit/data', ['as' => 'editor.unit.data', 'uses' => 'UnitController@data']);
	
		//create
	Route::get('/unit/create', ['middleware' => ['role:unit|create'], 'as' => 'editor.unit.create', 'uses' => 'UnitController@create']);
	Route::post('/unit/create', ['middleware' => ['role:unit|create'], 'as' => 'editor.unit.store', 'uses' => 'UnitController@store']);
		//edit
	Route::get('/unit/edit/{id}', ['middleware' => ['role:unit|update'], 'as' => 'editor.unit.edit', 'uses' => 'UnitController@edit']);
	Route::put('/unit/edit/{id}', ['middleware' => ['role:unit|update'], 'as' => 'editor.unit.update', 'uses' => 'UnitController@update']);
		//delete
	Route::delete('/unit/delete/{id}', ['middleware' => ['role:unit|delete'], 'as' => 'editor.unit.delete', 'uses' => 'UnitController@delete']);
	Route::post('/unit/deletebulk', ['middleware' => ['role:unit|delete'], 'as' => 'editor.unit.deletebulk', 'uses' => 'UnitController@deletebulk']);

	//Location
		//index
	Route::get('/location', ['middleware' => ['role:location|read'], 'as' => 'editor.location.index', 'uses' => 'LocationController@index']);
	Route::get('/location/data', ['as' => 'editor.location.data', 'uses' => 'LocationController@data']);
	
		//create
	Route::get('/location/create', ['middleware' => ['role:location|create'], 'as' => 'editor.location.create', 'uses' => 'LocationController@create']);
	Route::post('/location/create', ['middleware' => ['role:location|create'], 'as' => 'editor.location.store', 'uses' => 'LocationController@store']);
		//edit
	Route::get('/location/edit/{id}', ['middleware' => ['role:location|update'], 'as' => 'editor.location.edit', 'uses' => 'LocationController@edit']);
	Route::put('/location/edit/{id}', ['middleware' => ['role:location|update'], 'as' => 'editor.location.update', 'uses' => 'LocationController@update']);
		//delete
	Route::delete('/location/delete/{id}', ['middleware' => ['role:location|delete'], 'as' => 'editor.location.delete', 'uses' => 'LocationController@delete']);
	Route::post('/location/deletebulk', ['middleware' => ['role:location|delete'], 'as' => 'editor.location.deletebulk', 'uses' => 'LocationController@deletebulk']);

	//Holiday
		//index
	Route::get('/holiday', ['middleware' => ['role:holiday|read'], 'as' => 'editor.holiday.index', 'uses' => 'HolidayController@index']);
	Route::get('/holiday/data', ['as' => 'editor.holiday.data', 'uses' => 'HolidayController@data']);
	
		//create
	Route::get('/holiday/create', ['middleware' => ['role:holiday|create'], 'as' => 'editor.holiday.create', 'uses' => 'HolidayController@create']);
	Route::post('/holiday/create', ['middleware' => ['role:holiday|create'], 'as' => 'editor.holiday.store', 'uses' => 'HolidayController@store']);
		//edit
	Route::get('/holiday/edit/{id}', ['middleware' => ['role:holiday|update'], 'as' => 'editor.holiday.edit', 'uses' => 'HolidayController@edit']);
	Route::put('/holiday/edit/{id}', ['middleware' => ['role:holiday|update'], 'as' => 'editor.holiday.update', 'uses' => 'HolidayController@update']);
		//delete
	Route::delete('/holiday/delete/{id}', ['middleware' => ['role:holiday|delete'], 'as' => 'editor.holiday.delete', 'uses' => 'HolidayController@delete']);
	Route::post('/holiday/deletebulk', ['middleware' => ['role:holiday|delete'], 'as' => 'editor.holiday.deletebulk', 'uses' => 'HolidayController@deletebulk']);

	//Payrolltype
		//index
	Route::get('/payrolltype', ['middleware' => ['role:payrolltype|read'], 'as' => 'editor.payrolltype.index', 'uses' => 'PayrolltypeController@index']);
	Route::get('/payrolltype/data', ['as' => 'editor.payrolltype.data', 'uses' => 'PayrolltypeController@data']);
	
		//create
	Route::get('/payrolltype/create', ['middleware' => ['role:payrolltype|create'], 'as' => 'editor.payrolltype.create', 'uses' => 'PayrolltypeController@create']);
	Route::post('/payrolltype/create', ['middleware' => ['role:payrolltype|create'], 'as' => 'editor.payrolltype.store', 'uses' => 'PayrolltypeController@store']);
		//edit
	Route::get('/payrolltype/edit/{id}', ['middleware' => ['role:payrolltype|update'], 'as' => 'editor.payrolltype.edit', 'uses' => 'PayrolltypeController@edit']);
	Route::put('/payrolltype/edit/{id}', ['middleware' => ['role:payrolltype|update'], 'as' => 'editor.payrolltype.update', 'uses' => 'PayrolltypeController@update']);
		//delete
	Route::delete('/payrolltype/delete/{id}', ['middleware' => ['role:payrolltype|delete'], 'as' => 'editor.payrolltype.delete', 'uses' => 'PayrolltypeController@delete']);
	Route::post('/payrolltype/deletebulk', ['middleware' => ['role:payrolltype|delete'], 'as' => 'editor.payrolltype.deletebulk', 'uses' => 'PayrolltypeController@deletebulk']);

	//Employee
		//index
	Route::get('/employee', ['middleware' => ['role:employee|read'], 'as' => 'editor.employee.index', 'uses' => 'EmployeeController@index']);
	Route::get('/employee/data', ['as' => 'editor.employee.data', 'uses' => 'EmployeeController@data']);
	
	//create
	Route::get('/employee/create', ['middleware' => ['role:employee|create'], 'as' => 'editor.employee.create', 'uses' => 'EmployeeController@create']);
	Route::post('/employee/create', ['middleware' => ['role:employee|create'], 'as' => 'editor.employee.store', 'uses' => 'EmployeeController@store']);
		
		//edit
	Route::get('/employee/{id}/edit', ['middleware' => ['role:employee|update'], 'as' => 'editor.employee.edit', 'uses' => 'EmployeeController@edit']);
	Route::put('/employee/{id}/edit', ['middleware' => ['role:employee|update'], 'as' => 'editor.employee.update', 'uses' => 'EmployeeController@update']);

	Route::put('/employee/edit/{id}', ['middleware' => ['role:employee|update'], 'as' => 'editor.employee.update', 'uses' => 'EmployeeController@update']);
		//delete
	Route::delete('/employee/delete/{id}', ['middleware' => ['role:employee|delete'], 'as' => 'editor.employee.delete', 'uses' => 'EmployeeController@delete']);
	Route::post('/employee/deletebulk', ['middleware' => ['role:employee|delete'], 'as' => 'editor.employee.deletebulk', 'uses' => 'EmployeeController@deletebulk']);
		//lookup
	Route::get('/employee/datalookup', ['as' => 'editor.employee.datalookup', 'uses' => 'EmployeeController@datalookup']);

	//Supplier
		//index
	Route::get('/supplier', ['middleware' => ['role:supplier|read'], 'as' => 'editor.supplier.index', 'uses' => 'SupplierController@index']);
	Route::get('/supplier/data', ['as' => 'editor.supplier.data', 'uses' => 'SupplierController@data']);
	
	//create
	Route::get('/supplier/create', ['middleware' => ['role:supplier|create'], 'as' => 'editor.supplier.create', 'uses' => 'SupplierController@create']);
	Route::post('/supplier/create', ['middleware' => ['role:supplier|create'], 'as' => 'editor.supplier.store', 'uses' => 'SupplierController@store']);
		
		//edit
	Route::get('/supplier/{id}/edit', ['middleware' => ['role:supplier|update'], 'as' => 'editor.supplier.edit', 'uses' => 'SupplierController@edit']);
	Route::put('/supplier/{id}/edit', ['middleware' => ['role:supplier|update'], 'as' => 'editor.supplier.update', 'uses' => 'SupplierController@update']);

	Route::put('/supplier/edit/{id}', ['middleware' => ['role:supplier|update'], 'as' => 'editor.supplier.update', 'uses' => 'SupplierController@update']);
		//delete
	Route::delete('/supplier/delete/{id}', ['middleware' => ['role:supplier|delete'], 'as' => 'editor.supplier.delete', 'uses' => 'SupplierController@delete']);
	Route::post('/supplier/deletebulk', ['middleware' => ['role:supplier|delete'], 'as' => 'editor.supplier.deletebulk', 'uses' => 'SupplierController@deletebulk']);

	//Inventory
		//index
	Route::get('/inventory', ['middleware' => ['role:inventory|read'], 'as' => 'editor.inventory.index', 'uses' => 'InventoryController@index']);
	Route::get('/inventory/data', ['as' => 'editor.inventory.data', 'uses' => 'InventoryController@data']);
	Route::get('/inventory/datalookup', ['as' => 'editor.inventory.datalookup', 'uses' => 'InventoryController@datalookup']);
	
	//create
	Route::get('/inventory/create', ['middleware' => ['role:inventory|create'], 'as' => 'editor.inventory.create', 'uses' => 'InventoryController@create']);
	Route::post('/inventory/create', ['middleware' => ['role:inventory|create'], 'as' => 'editor.inventory.store', 'uses' => 'InventoryController@store']);
		
		//edit
	Route::get('/inventory/{id}/edit', ['middleware' => ['role:inventory|update'], 'as' => 'editor.inventory.edit', 'uses' => 'InventoryController@edit']);
	Route::put('/inventory/{id}/edit', ['middleware' => ['role:inventory|update'], 'as' => 'editor.inventory.update', 'uses' => 'InventoryController@update']);

	Route::put('/inventory/{id}/edit', ['middleware' => ['role:inventory|update'], 'as' => 'editor.inventory.update', 'uses' => 'InventoryController@update']);
		//delete
	Route::delete('/inventory/delete/{id}', ['middleware' => ['role:inventory|delete'], 'as' => 'editor.inventory.delete', 'uses' => 'InventoryController@delete']);
	Route::post('/inventory/deletebulk', ['middleware' => ['role:inventory|delete'], 'as' => 'editor.inventory.deletebulk', 'uses' => 'InventoryController@deletebulk']);

	//Inventorybrand
		//index
	Route::get('/inventorybrand', ['middleware' => ['role:inventorybrand|read'], 'as' => 'editor.inventorybrand.index', 'uses' => 'InventorybrandController@index']);
	Route::get('/inventorybrand/data', ['as' => 'editor.inventorybrand.data', 'uses' => 'InventorybrandController@data']);

	
		//create
	Route::get('/inventorybrand/create', ['middleware' => ['role:inventorybrand|create'], 'as' => 'editor.inventorybrand.create', 'uses' => 'InventorybrandController@create']);
	Route::post('/inventorybrand/create', ['middleware' => ['role:inventorybrand|create'], 'as' => 'editor.inventorybrand.store', 'uses' => 'InventorybrandController@store']);
		//edit
	Route::get('/inventorybrand/edit/{id}', ['middleware' => ['role:inventorybrand|update'], 'as' => 'editor.inventorybrand.edit', 'uses' => 'InventorybrandController@edit']);
	Route::put('/inventorybrand/edit/{id}', ['middleware' => ['role:inventorybrand|update'], 'as' => 'editor.inventorybrand.update', 'uses' => 'InventorybrandController@update']);
		//delete
	Route::delete('/inventorybrand/delete/{id}', ['middleware' => ['role:inventorybrand|delete'], 'as' => 'editor.inventorybrand.delete', 'uses' => 'InventorybrandController@delete']);
	Route::post('/inventorybrand/deletebulk', ['middleware' => ['role:inventorybrand|delete'], 'as' => 'editor.inventorybrand.deletebulk', 'uses' => 'InventorybrandController@deletebulk']);

	//Inventorytype
		//index
	Route::get('/inventorytype', ['middleware' => ['role:inventorytype|read'], 'as' => 'editor.inventorytype.index', 'uses' => 'InventorytypeController@index']);
	Route::get('/inventorytype/data', ['as' => 'editor.inventorytype.data', 'uses' => 'InventorytypeController@data']);
	
		//create
	Route::get('/inventorytype/create', ['middleware' => ['role:inventorytype|create'], 'as' => 'editor.inventorytype.create', 'uses' => 'InventorytypeController@create']);
	Route::post('/inventorytype/create', ['middleware' => ['role:inventorytype|create'], 'as' => 'editor.inventorytype.store', 'uses' => 'InventorytypeController@store']);
		//edit
	Route::get('/inventorytype/edit/{id}', ['middleware' => ['role:inventorytype|update'], 'as' => 'editor.inventorytype.edit', 'uses' => 'InventorytypeController@edit']);
	Route::put('/inventorytype/edit/{id}', ['middleware' => ['role:inventorytype|update'], 'as' => 'editor.inventorytype.update', 'uses' => 'InventorytypeController@update']);
		//delete
	Route::delete('/inventorytype/delete/{id}', ['middleware' => ['role:inventorytype|delete'], 'as' => 'editor.inventorytype.delete', 'uses' => 'InventorytypeController@delete']);
	Route::post('/inventorytype/deletebulk', ['middleware' => ['role:inventorytype|delete'], 'as' => 'editor.inventorytype.deletebulk', 'uses' => 'InventorytypeController@deletebulk']);

	//Inventorygroup
		//index
	Route::get('/inventorygroup', ['middleware' => ['role:inventorygroup|read'], 'as' => 'editor.inventorygroup.index', 'uses' => 'InventorygroupController@index']);
	Route::get('/inventorygroup/data', ['as' => 'editor.inventorygroup.data', 'uses' => 'InventorygroupController@data']);
	
		//create
	Route::get('/inventorygroup/create', ['middleware' => ['role:inventorygroup|create'], 'as' => 'editor.inventorygroup.create', 'uses' => 'InventorygroupController@create']);
	Route::post('/inventorygroup/create', ['middleware' => ['role:inventorygroup|create'], 'as' => 'editor.inventorygroup.store', 'uses' => 'InventorygroupController@store']);
		//edit
	Route::get('/inventorygroup/edit/{id}', ['middleware' => ['role:inventorygroup|update'], 'as' => 'editor.inventorygroup.edit', 'uses' => 'InventorygroupController@edit']);
	Route::put('/inventorygroup/edit/{id}', ['middleware' => ['role:inventorygroup|update'], 'as' => 'editor.inventorygroup.update', 'uses' => 'InventorygroupController@update']);
		//delete
	Route::delete('/inventorygroup/delete/{id}', ['middleware' => ['role:inventorygroup|delete'], 'as' => 'editor.inventorygroup.delete', 'uses' => 'InventorygroupController@delete']);
	Route::post('/inventorygroup/deletebulk', ['middleware' => ['role:inventorygroup|delete'], 'as' => 'editor.inventorygroup.deletebulk', 'uses' => 'InventorygroupController@deletebulk']);

	//Inventorysize
		//index
	Route::get('/inventorysize', ['middleware' => ['role:inventorysize|read'], 'as' => 'editor.inventorysize.index', 'uses' => 'InventorysizeController@index']);
	Route::get('/inventorysize/data', ['as' => 'editor.inventorysize.data', 'uses' => 'InventorysizeController@data']);
	
		//create
	Route::get('/inventorysize/create', ['middleware' => ['role:inventorysize|create'], 'as' => 'editor.inventorysize.create', 'uses' => 'InventorysizeController@create']);
	Route::post('/inventorysize/create', ['middleware' => ['role:inventorysize|create'], 'as' => 'editor.inventorysize.store', 'uses' => 'InventorysizeController@store']);
		//edit
	Route::get('/inventorysize/edit/{id}', ['middleware' => ['role:inventorysize|update'], 'as' => 'editor.inventorysize.edit', 'uses' => 'InventorysizeController@edit']);
	Route::put('/inventorysize/edit/{id}', ['middleware' => ['role:inventorysize|update'], 'as' => 'editor.inventorysize.update', 'uses' => 'InventorysizeController@update']);
		//delete
	Route::delete('/inventorysize/delete/{id}', ['middleware' => ['role:inventorysize|delete'], 'as' => 'editor.inventorysize.delete', 'uses' => 'InventorysizeController@delete']);
	Route::post('/inventorysize/deletebulk', ['middleware' => ['role:inventorysize|delete'], 'as' => 'editor.inventorysize.deletebulk', 'uses' => 'InventorysizeController@deletebulk']);

	//Inventorycolor
		//index
	Route::get('/inventorycolor', ['middleware' => ['role:inventorycolor|read'], 'as' => 'editor.inventorycolor.index', 'uses' => 'InventorycolorController@index']);
	Route::get('/inventorycolor/data', ['as' => 'editor.inventorycolor.data', 'uses' => 'InventorycolorController@data']);
	
		//create
	Route::get('/inventorycolor/create', ['middleware' => ['role:inventorycolor|create'], 'as' => 'editor.inventorycolor.create', 'uses' => 'InventorycolorController@create']);
	Route::post('/inventorycolor/create', ['middleware' => ['role:inventorycolor|create'], 'as' => 'editor.inventorycolor.store', 'uses' => 'InventorycolorController@store']);
		//edit
	Route::get('/inventorycolor/edit/{id}', ['middleware' => ['role:inventorycolor|update'], 'as' => 'editor.inventorycolor.edit', 'uses' => 'InventorycolorController@edit']);
	Route::put('/inventorycolor/edit/{id}', ['middleware' => ['role:inventorycolor|update'], 'as' => 'editor.inventorycolor.update', 'uses' => 'InventorycolorController@update']);
		//delete
	Route::delete('/inventorycolor/delete/{id}', ['middleware' => ['role:inventorycolor|delete'], 'as' => 'editor.inventorycolor.delete', 'uses' => 'InventorycolorController@delete']);
	Route::post('/inventorycolor/deletebulk', ['middleware' => ['role:inventorycolor|delete'], 'as' => 'editor.inventorycolor.deletebulk', 'uses' => 'InventorycolorController@deletebulk']);

	//Materialusedtype
		//index
	Route::get('/materialusedtype', ['middleware' => ['role:materialusedtype|read'], 'as' => 'editor.materialusedtype.index', 'uses' => 'MaterialusedtypeController@index']);
	Route::get('/materialusedtype/data', ['as' => 'editor.materialusedtype.data', 'uses' => 'MaterialusedtypeController@data']); 
	//create
	Route::get('/materialusedtype/create', ['middleware' => ['role:materialusedtype|create'], 'as' => 'editor.materialusedtype.create', 'uses' => 'MaterialusedtypeController@create']);
	Route::post('/materialusedtype/create', ['middleware' => ['role:materialusedtype|create'], 'as' => 'editor.materialusedtype.store', 'uses' => 'MaterialusedtypeController@store']); 
		//edit
	Route::get('/materialusedtype/edit/{id}', ['middleware' => ['role:materialusedtype|update'], 'as' => 'editor.materialusedtype.edit', 'uses' => 'MaterialusedtypeController@edit']);
	Route::put('/materialusedtype/edit/{id}', ['middleware' => ['role:materialusedtype|update'], 'as' => 'editor.materialusedtype.update', 'uses' => 'MaterialusedtypeController@update']);

	Route::put('/materialusedtype/edit/{id}', ['middleware' => ['role:materialusedtype|update'], 'as' => 'editor.materialusedtype.update', 'uses' => 'MaterialusedtypeController@update']);
		//delete
	Route::delete('/materialusedtype/delete/{id}', ['middleware' => ['role:materialusedtype|delete'], 'as' => 'editor.materialusedtype.delete', 'uses' => 'MaterialusedtypeController@delete']);
	Route::post('/materialusedtype/deletebulk', ['middleware' => ['role:materialusedtype|delete'], 'as' => 'editor.materialusedtype.deletebulk', 'uses' => 'MaterialusedtypeController@deletebulk']);

	//Department
		//index
	Route::get('/department', ['middleware' => ['role:department|read'], 'as' => 'editor.department.index', 'uses' => 'DepartmentController@index']);
	Route::get('/department/data', ['as' => 'editor.department.data', 'uses' => 'DepartmentController@data']);
	
		//create
	Route::get('/department/create', ['middleware' => ['role:department|create'], 'as' => 'editor.department.create', 'uses' => 'DepartmentController@create']);
	Route::post('/department/create', ['middleware' => ['role:department|create'], 'as' => 'editor.department.store', 'uses' => 'DepartmentController@store']);
		//edit
	Route::get('/department/edit/{id}', ['middleware' => ['role:department|update'], 'as' => 'editor.department.edit', 'uses' => 'DepartmentController@edit']);
	Route::put('/department/edit/{id}', ['middleware' => ['role:department|update'], 'as' => 'editor.department.update', 'uses' => 'DepartmentController@update']);
		//delete
	Route::delete('/department/delete/{id}', ['middleware' => ['role:department|delete'], 'as' => 'editor.department.delete', 'uses' => 'DepartmentController@delete']);
	Route::post('/department/deletebulk', ['middleware' => ['role:department|delete'], 'as' => 'editor.department.deletebulk', 'uses' => 'DepartmentController@deletebulk']);

	//Materialused
		//index
	Route::get('/materialused', ['middleware' => ['role:materialused|read'], 'as' => 'editor.materialused.index', 'uses' => 'MaterialusedController@index']);
	Route::get('/materialused/data', ['as' => 'editor.materialused.data', 'uses' => 'MaterialusedController@data']);
	
	//create
	Route::get('/materialused/create', ['middleware' => ['role:materialused|create'], 'as' => 'editor.materialused.create', 'uses' => 'MaterialusedController@create']);
	Route::post('/materialused/create', ['middleware' => ['role:materialused|create'], 'as' => 'editor.materialused.store', 'uses' => 'MaterialusedController@store']);
		
		//detail
	Route::get('/materialused/detail/{id}', ['middleware' => ['role:materialused|update'], 'as' => 'editor.materialused.detail', 'uses' => 'MaterialusedController@detail']);
	Route::put('/materialused/saveheader/{id}', ['middleware' => ['role:materialused|update'], 'as' => 'editor.materialused.saveheader', 'uses' => 'MaterialusedController@saveheader']);
	Route::put('/materialused/savedetail/{id}', ['middleware' => ['role:materialused|update'], 'as' => 'editor.materialused.savedetail', 'uses' => 'MaterialusedController@savedetail']);

	Route::put('/materialused/detail/{id}', ['middleware' => ['role:materialused|update'], 'as' => 'editor.materialused.update', 'uses' => 'MaterialusedController@update']);
		//delete
	Route::delete('/materialused/deletedet/{id}', ['middleware' => ['role:materialused|delete'], 'as' => 'editor.materialused.deletedet', 'uses' => 'MaterialusedController@deletedet']);
	Route::post('/materialused/deletebulk', ['middleware' => ['role:materialused|delete'], 'as' => 'editor.materialused.deletebulk', 'uses' => 'MaterialusedController@deletebulk']);
	// detailitem 
	Route::get('/materialused/datadetail/{id}', ['as' => 'editor.materialused.datadetail', 'uses' => 'MaterialusedController@datadetail']);

	//Materialused App
		//index
	Route::get('/materialusedapp', ['middleware' => ['role:materialusedapp|read'], 'as' => 'editor.materialusedapp.index', 'uses' => 'MaterialusedappController@index']);
	Route::get('/materialusedapp/detail/{id}', ['middleware' => ['role:materialusedapp|update'], 'as' => 'editor.materialusedapp.detail', 'uses' => 'MaterialusedappController@detail']);
	Route::get('/materialusedapp/data', ['as' => 'editor.materialusedapp.data', 'uses' => 'MaterialusedappController@data']);
	// detailitem 
	Route::get('/materialusedapp/datadetail/{id}', ['as' => 'editor.materialusedapp.datadetail', 'uses' => 'MaterialusedappController@datadetail']);
	Route::put('/materialusedapp/approve/{id}', ['middleware' => ['role:materialusedapp|update'], 'as' => 'editor.materialusedapp.approve', 'uses' => 'MaterialusedappController@approve']);
	Route::put('/materialusedapp/notapprove/{id}', ['middleware' => ['role:materialusedapp|update'], 'as' => 'editor.materialusedapp.notapprove', 'uses' => 'MaterialusedappController@notapprove']);

	//Personal Management
	//Mutation
		//index
	Route::get('/mutation', ['middleware' => ['role:mutation|read'], 'as' => 'editor.mutation.index', 'uses' => 'MutationController@index']);
	Route::get('/mutation/data', ['as' => 'editor.mutation.data', 'uses' => 'MutationController@data']);
	Route::get('/mutation/datahistory', ['as' => 'editor.mutation.datahistory', 'uses' => 'MutationController@datahistory']);

		//create
	Route::get('/mutation/create', ['middleware' => ['role:mutation|create'], 'as' => 'editor.mutation.create', 'uses' => 'MutationController@create']);
	Route::post('/mutation/create', ['middleware' => ['role:mutation|create'], 'as' => 'editor.mutation.store', 'uses' => 'MutationController@store']);
		//edit
	Route::get('/mutation/{id}/edit', ['middleware' => ['role:mutation|update'], 'as' => 'editor.mutation.edit', 'uses' => 'MutationController@edit']);
	Route::put('/mutation/{id}/edit', ['middleware' => ['role:mutation|update'], 'as' => 'editor.mutation.update', 'uses' => 'MutationController@update']);
		//delete
	Route::delete('/mutation/delete/{id}', ['middleware' => ['role:mutation|delete'], 'as' => 'editor.mutation.delete', 'uses' => 'MutationController@delete']);
	Route::post('/mutation/deletebulk', ['middleware' => ['role:mutation|delete'], 'as' => 'editor.mutation.deletebulk', 'uses' => 'MutationController@deletebulk']);
	Route::put('/mutation/cancel/{id}', ['middleware' => ['role:mutation|delete'], 'as' => 'editor.mutation.cancel', 'uses' => 'MutationController@cancel']);
		//slip
	Route::get('/mutation/slip/{id}', ['as' => 'slipor.mutation.slip', 'uses' => 'MutationController@slip']);

	//Promotion
		//index
	Route::get('/promotion', ['middleware' => ['role:promotion|read'], 'as' => 'editor.promotion.index', 'uses' => 'PromotionController@index']);
	Route::get('/promotion/data', ['as' => 'editor.promotion.data', 'uses' => 'PromotionController@data']);
	Route::get('/promotion/datahistory', ['as' => 'editor.promotion.datahistory', 'uses' => 'PromotionController@datahistory']);
	
		//create
	Route::get('/promotion/create', ['middleware' => ['role:promotion|create'], 'as' => 'editor.promotion.create', 'uses' => 'PromotionController@create']);
	Route::post('/promotion/create', ['middleware' => ['role:promotion|create'], 'as' => 'editor.promotion.store', 'uses' => 'PromotionController@store']);
		//edit
	Route::get('/promotion/{id}/edit', ['middleware' => ['role:promotion|update'], 'as' => 'editor.promotion.edit', 'uses' => 'PromotionController@edit']);
	Route::put('/promotion/{id}/edit', ['middleware' => ['role:promotion|update'], 'as' => 'editor.promotion.update', 'uses' => 'PromotionController@update']);
		//delete
	Route::delete('/promotion/delete/{id}', ['middleware' => ['role:promotion|delete'], 'as' => 'editor.promotion.delete', 'uses' => 'PromotionController@delete']);
	Route::post('/promotion/deletebulk', ['middleware' => ['role:promotion|delete'], 'as' => 'editor.promotion.deletebulk', 'uses' => 'PromotionController@deletebulk']);
	Route::put('/promotion/cancel/{id}', ['middleware' => ['role:promotion|delete'], 'as' => 'editor.promotion.cancel', 'uses' => 'PromotionController@cancel']);


	//Punishment
		//index
	Route::get('/punishment', ['middleware' => ['role:punishment|read'], 'as' => 'editor.punishment.index', 'uses' => 'PunishmentController@index']);
	Route::get('/punishment/data', ['as' => 'editor.punishment.data', 'uses' => 'PunishmentController@data']);
	Route::get('/punishment/datahistory', ['as' => 'editor.punishment.datahistory', 'uses' => 'PunishmentController@datahistory']);

		//create
	Route::get('/punishment/create', ['middleware' => ['role:punishment|create'], 'as' => 'editor.punishment.create', 'uses' => 'PunishmentController@create']);
	Route::post('/punishment/create', ['middleware' => ['role:punishment|create'], 'as' => 'editor.punishment.store', 'uses' => 'PunishmentController@store']);
		//edit
	Route::get('/punishment/{id}/edit', ['middleware' => ['role:punishment|update'], 'as' => 'editor.punishment.edit', 'uses' => 'PunishmentController@edit']);
	Route::put('/punishment/{id}/edit', ['middleware' => ['role:punishment|update'], 'as' => 'editor.punishment.update', 'uses' => 'PunishmentController@update']);
		//delete
	Route::delete('/punishment/delete/{id}', ['middleware' => ['role:punishment|delete'], 'as' => 'editor.punishment.delete', 'uses' => 'PunishmentController@delete']);
	Route::post('/punishment/deletebulk', ['middleware' => ['role:punishment|delete'], 'as' => 'editor.punishment.deletebulk', 'uses' => 'PunishmentController@deletebulk']);
	Route::put('/punishment/cancel/{id}', ['middleware' => ['role:punishment|delete'], 'as' => 'editor.punishment.cancel', 'uses' => 'PunishmentController@cancel']);

	//Reward
		//index
	Route::get('/reward', ['middleware' => ['role:reward|read'], 'as' => 'editor.reward.index', 'uses' => 'RewardController@index']);
	Route::get('/reward/data', ['as' => 'editor.reward.data', 'uses' => 'RewardController@data']);
	Route::get('/reward/datahistory', ['as' => 'editor.reward.datahistory', 'uses' => 'RewardController@datahistory']);
	
	
		//create
	Route::get('/reward/create', ['middleware' => ['role:reward|create'], 'as' => 'editor.reward.create', 'uses' => 'RewardController@create']);
	Route::post('/reward/create', ['middleware' => ['role:reward|create'], 'as' => 'editor.reward.store', 'uses' => 'RewardController@store']);
		//edit
	Route::get('/reward/{id}/edit', ['middleware' => ['role:reward|update'], 'as' => 'editor.reward.edit', 'uses' => 'RewardController@edit']);
	Route::put('/reward/{id}/edit', ['middleware' => ['role:reward|update'], 'as' => 'editor.reward.update', 'uses' => 'RewardController@update']);
		//delete
	Route::delete('/reward/delete/{id}', ['middleware' => ['role:reward|delete'], 'as' => 'editor.reward.delete', 'uses' => 'RewardController@delete']);
	Route::post('/reward/deletebulk', ['middleware' => ['role:reward|delete'], 'as' => 'editor.reward.deletebulk', 'uses' => 'RewardController@deletebulk']);
	Route::put('/reward/cancel/{id}', ['middleware' => ['role:reward|delete'], 'as' => 'editor.reward.cancel', 'uses' => 'RewardController@cancel']);


	//Training
		//index
	Route::get('/training', ['middleware' => ['role:training|read'], 'as' => 'editor.training.index', 'uses' => 'TrainingController@index']);
	Route::get('/training/data', ['as' => 'editor.training.data', 'uses' => 'TrainingController@data']);
	Route::get('/training/datahistory', ['as' => 'editor.training.datahistory', 'uses' => 'TrainingController@datahistory']);
	
		//create
	Route::get('/training/create', ['middleware' => ['role:training|create'], 'as' => 'editor.training.create', 'uses' => 'TrainingController@create']);
	Route::post('/training/create', ['middleware' => ['role:training|create'], 'as' => 'editor.training.store', 'uses' => 'TrainingController@store']);
		//edit
	Route::get('/training/{id}/edit', ['middleware' => ['role:training|update'], 'as' => 'editor.training.edit', 'uses' => 'TrainingController@edit']);
	Route::put('/training/{id}/edit', ['middleware' => ['role:training|update'], 'as' => 'editor.training.update', 'uses' => 'TrainingController@update']);
		//delete
	Route::delete('/training/delete/{id}', ['middleware' => ['role:training|delete'], 'as' => 'editor.training.delete', 'uses' => 'TrainingController@delete']);
	Route::post('/training/deletebulk', ['middleware' => ['role:training|delete'], 'as' => 'editor.training.deletebulk', 'uses' => 'TrainingController@deletebulk']);
	Route::put('/training/cancel/{id}', ['middleware' => ['role:training|delete'], 'as' => 'editor.training.cancel', 'uses' => 'TrainingController@cancel']);

	//Training App
		//index
	Route::get('/trainingapp', ['middleware' => ['role:trainingapp|read'], 'as' => 'editor.trainingapp.index', 'uses' => 'TrainingappController@index']);
	Route::get('/trainingapp/detail/{id}', ['middleware' => ['role:trainingapp|update'], 'as' => 'editor.trainingapp.detail', 'uses' => 'TrainingappController@detail']);
	Route::get('/trainingapp/data', ['as' => 'editor.trainingapp.data', 'uses' => 'TrainingappController@data']);
	// detailitem 
	Route::get('/trainingapp/datadetail/{id}', ['as' => 'editor.trainingapp.datadetail', 'uses' => 'TrainingappController@datadetail']);
	Route::put('/trainingapp/approve/{id}', ['middleware' => ['role:trainingapp|update'], 'as' => 'editor.trainingapp.approve', 'uses' => 'TrainingappController@approve']);
	Route::put('/trainingapp/notapprove/{id}', ['middleware' => ['role:trainingapp|update'], 'as' => 'editor.trainingapp.notapprove', 'uses' => 'TrainingappController@notapprove']);

	//Travelling
		//index
	Route::get('/travelling', ['middleware' => ['role:travelling|read'], 'as' => 'editor.travelling.index', 'uses' => 'TravellingController@index']);
	Route::get('/travelling/data', ['as' => 'editor.travelling.data', 'uses' => 'TravellingController@data']);
	Route::get('/travelling/datahistory', ['as' => 'editor.travelling.datahistory', 'uses' => 'TravellingController@datahistory']);
	
		//create
	Route::get('/travelling/create', ['middleware' => ['role:travelling|create'], 'as' => 'editor.travelling.create', 'uses' => 'TravellingController@create']);
	Route::post('/travelling/create', ['middleware' => ['role:travelling|create'], 'as' => 'editor.travelling.store', 'uses' => 'TravellingController@store']);
		//edit
	Route::get('/travelling/{id}/edit', ['middleware' => ['role:travelling|update'], 'as' => 'editor.travelling.edit', 'uses' => 'TravellingController@edit']);
	Route::put('/travelling/{id}/edit', ['middleware' => ['role:travelling|update'], 'as' => 'editor.travelling.update', 'uses' => 'TravellingController@update']);
		//delete
	Route::delete('/travelling/delete/{id}', ['middleware' => ['role:travelling|delete'], 'as' => 'editor.travelling.delete', 'uses' => 'TravellingController@delete']);
	Route::post('/travelling/deletebulk', ['middleware' => ['role:travelling|delete'], 'as' => 'editor.travelling.deletebulk', 'uses' => 'TravellingController@deletebulk']);
	Route::put('/travelling/cancel/{id}', ['middleware' => ['role:travelling|delete'], 'as' => 'editor.travelling.cancel', 'uses' => 'TravellingController@cancel']);


	//Travelling App
		//index
	Route::get('/travellingapp', ['middleware' => ['role:travellingapp|read'], 'as' => 'editor.travellingapp.index', 'uses' => 'TravellingappController@index']);
	Route::get('/travellingapp/detail/{id}', ['middleware' => ['role:travellingapp|update'], 'as' => 'editor.travellingapp.detail', 'uses' => 'TravellingappController@detail']);
	Route::get('/travellingapp/data', ['as' => 'editor.travellingapp.data', 'uses' => 'TravellingappController@data']);
	// detailitem 
	Route::get('/travellingapp/datadetail/{id}', ['as' => 'editor.travellingapp.datadetail', 'uses' => 'TravellingappController@datadetail']);
	Route::put('/travellingapp/approve/{id}', ['middleware' => ['role:travellingapp|update'], 'as' => 'editor.travellingapp.approve', 'uses' => 'TravellingappController@approve']);
	Route::put('/travellingapp/notapprove/{id}', ['middleware' => ['role:travellingapp|update'], 'as' => 'editor.travellingapp.notapprove', 'uses' => 'TravellingappController@notapprove']);

	//Leaving
		//index
	Route::get('/leaving', ['middleware' => ['role:leaving|read'], 'as' => 'editor.leaving.index', 'uses' => 'LeavingController@index']);
	Route::get('/leaving/data', ['as' => 'editor.leaving.data', 'uses' => 'LeavingController@data']);
	Route::get('/leaving/datahistory', ['as' => 'editor.leaving.datahistory', 'uses' => 'LeavingController@datahistory']);

	
		//create
	Route::get('/leaving/create', ['middleware' => ['role:leaving|create'], 'as' => 'editor.leaving.create', 'uses' => 'LeavingController@create']);
	Route::post('/leaving/create', ['middleware' => ['role:leaving|create'], 'as' => 'editor.leaving.store', 'uses' => 'LeavingController@store']);
		//edit
	Route::get('/leaving/{id}/edit', ['middleware' => ['role:leaving|update'], 'as' => 'editor.leaving.edit', 'uses' => 'LeavingController@edit']);
	Route::put('/leaving/{id}/edit', ['middleware' => ['role:leaving|update'], 'as' => 'editor.leaving.update', 'uses' => 'LeavingController@update']);
		//delete
	Route::delete('/leaving/delete/{id}', ['middleware' => ['role:leaving|delete'], 'as' => 'editor.leaving.delete', 'uses' => 'LeavingController@delete']);
	Route::post('/leaving/deletebulk', ['middleware' => ['role:leaving|delete'], 'as' => 'editor.leaving.deletebulk', 'uses' => 'LeavingController@deletebulk']);

	//Leaving App
		//index
	Route::get('/leavingapp', ['middleware' => ['role:leavingapp|read'], 'as' => 'editor.leavingapp.index', 'uses' => 'LeavingappController@index']);
	Route::get('/leavingapp/detail/{id}', ['middleware' => ['role:leavingapp|update'], 'as' => 'editor.leavingapp.detail', 'uses' => 'LeavingappController@detail']);
	Route::get('/leavingapp/data', ['as' => 'editor.leavingapp.data', 'uses' => 'LeavingappController@data']);
	// detailitem 
	Route::get('/leavingapp/datadetail/{id}', ['as' => 'editor.leavingapp.datadetail', 'uses' => 'LeavingappController@datadetail']);
	Route::put('/leavingapp/approve/{id}', ['middleware' => ['role:leavingapp|update'], 'as' => 'editor.leavingapp.approve', 'uses' => 'LeavingappController@approve']);
	Route::put('/leavingapp/notapprove/{id}', ['middleware' => ['role:leavingapp|update'], 'as' => 'editor.leavingapp.notapprove', 'uses' => 'LeavingappController@notapprove']);

	//Leaving App
		//index
	Route::get('/leavingplafond', ['middleware' => ['role:leavingplafond|read'], 'as' => 'editor.leavingplafond.index', 'uses' => 'LeavingplafondController@index']);
	Route::get('/leavingplafond/data', ['as' => 'editor.leavingplafond.data', 'uses' => 'leavingplafondController@data']);
	

	//Time


		//Shiftgroup
		//index
	Route::get('/shiftgroup', ['middleware' => ['role:shiftgroup|read'], 'as' => 'editor.shiftgroup.index', 'uses' => 'ShiftgroupController@index']);
	Route::get('/shiftgroup/data', ['as' => 'editor.shiftgroup.data', 'uses' => 'ShiftgroupController@data']);
	
		//create
	Route::get('/shiftgroup/create', ['middleware' => ['role:shiftgroup|create'], 'as' => 'editor.shiftgroup.create', 'uses' => 'ShiftgroupController@create']);
	Route::post('/shiftgroup/create', ['middleware' => ['role:shiftgroup|create'], 'as' => 'editor.shiftgroup.store', 'uses' => 'ShiftgroupController@store']);
		//edit
	Route::get('/shiftgroup/edit/{id}', ['middleware' => ['role:shiftgroup|update'], 'as' => 'editor.shiftgroup.edit', 'uses' => 'ShiftgroupController@edit']);
	Route::put('/shiftgroup/edit/{id}', ['middleware' => ['role:shiftgroup|update'], 'as' => 'editor.shiftgroup.update', 'uses' => 'ShiftgroupController@update']);
		//delete
	Route::delete('/shiftgroup/delete/{id}', ['middleware' => ['role:shiftgroup|delete'], 'as' => 'editor.shiftgroup.delete', 'uses' => 'ShiftgroupController@delete']);
	Route::post('/shiftgroup/deletebulk', ['middleware' => ['role:shiftgroup|delete'], 'as' => 'editor.shiftgroup.deletebulk', 'uses' => 'ShiftgroupController@deletebulk']);


	//Shift
		//index
	Route::get('/shift', ['middleware' => ['role:shift|read'], 'as' => 'editor.shift.index', 'uses' => 'ShiftController@index']);
	Route::get('/shift/data', ['as' => 'editor.shift.data', 'uses' => 'ShiftController@data']);
	
		//create
	Route::get('/shift/create', ['middleware' => ['role:shift|create'], 'as' => 'editor.shift.create', 'uses' => 'ShiftController@create']);
	Route::post('/shift/create', ['middleware' => ['role:shift|create'], 'as' => 'editor.shift.store', 'uses' => 'ShiftController@store']);
		//edit
	Route::get('/shift/edit/{id}', ['middleware' => ['role:shift|update'], 'as' => 'editor.shift.edit', 'uses' => 'ShiftController@edit']);
	Route::put('/shift/edit/{id}', ['middleware' => ['role:shift|update'], 'as' => 'editor.shift.update', 'uses' => 'ShiftController@update']);
		//delete
	Route::delete('/shift/delete/{id}', ['middleware' => ['role:shift|delete'], 'as' => 'editor.shift.delete', 'uses' => 'ShiftController@delete']);
	Route::post('/shift/deletebulk', ['middleware' => ['role:shift|delete'], 'as' => 'editor.shift.deletebulk', 'uses' => 'ShiftController@deletebulk']);

		//index
	Route::get('/time', ['middleware' => ['role:time|read'], 'as' => 'editor.time.index', 'uses' => 'TimeController@index']);
	Route::get('/time/data', ['as' => 'editor.time.data', 'uses' => 'TimeController@data']);
	Route::get('/time/datadetail/{id}', ['as' => 'editor.time.datadetail', 'uses' => 'TimeController@datadetail']);
	
		//create
	Route::get('/time/create/{id}', ['middleware' => ['role:time|create'], 'as' => 'editor.time.create', 'uses' => 'TimeController@create']);
	Route::post('/time/create/{id}', ['middleware' => ['role:time|create'], 'as' => 'editor.time.store', 'uses' => 'TimeController@store']);

	//edit
	Route::get('/time/{id}/{id2}/edit', ['middleware' => ['role:time|update'], 'as' => 'editor.time.edit', 'uses' => 'TimeController@edit']);
	Route::put('/time/{id}/edit', ['middleware' => ['role:time|update'], 'as' => 'editor.time.update', 'uses' => 'TimeController@update']);
	Route::get('/time/datadetail', ['as' => 'editor.time.datadetail', 'uses' => 'TimeController@datadetail']); 

	//generate
	Route::get('/time/generate/{id}', ['middleware' => ['role:time|create'], 'as' => 'editor.time.generate', 'uses' => 'TimeController@generate']);
	Route::post('/time/generate/{id}', ['middleware' => ['role:time|create'], 'as' => 'editor.time.generate', 'uses' => 'TimeController@generate']);

	//Payroll
		//index
	Route::get('/payroll', ['middleware' => ['role:payroll|read'], 'as' => 'editor.payroll.index', 'uses' => 'PayrollController@index']);
	Route::get('/payroll/data', ['as' => 'editor.payroll.data', 'uses' => 'PayrollController@data']);
	
		//create
	Route::get('/payroll/create/{id}', ['middleware' => ['role:payroll|create'], 'as' => 'editor.payroll.create', 'uses' => 'PayrollController@create']);
	Route::post('/payroll/create/{id}', ['middleware' => ['role:payroll|create'], 'as' => 'editor.payroll.store', 'uses' => 'PayrollController@store']);

	//edit
	Route::get('/payroll/{id}/edit', ['middleware' => ['role:payroll|update'], 'as' => 'editor.payroll.edit', 'uses' => 'PayrollController@edit']);
	Route::put('/payroll/{id}/edit', ['middleware' => ['role:payroll|update'], 'as' => 'editor.payroll.update', 'uses' => 'PayrollController@update']);

	//generate
	Route::get('/payroll/generate/{id}', ['middleware' => ['role:payroll|create'], 'as' => 'editor.payroll.generate', 'uses' => 'PayrollController@generate']);
	Route::post('/payroll/generate/{id}', ['middleware' => ['role:payroll|create'], 'as' => 'editor.payroll.generate', 'uses' => 'PayrollController@generate']);

	//Loan
		//index
	Route::get('/loan', ['middleware' => ['role:loan|read'], 'as' => 'editor.loan.index', 'uses' => 'LoanController@index']);
	Route::get('/loan/data', ['as' => 'editor.loan.data', 'uses' => 'LoanController@data']);
	
	//create
	Route::get('/loan/create', ['middleware' => ['role:loan|create'], 'as' => 'editor.loan.create', 'uses' => 'LoanController@create']);
	Route::post('/loan/create', ['middleware' => ['role:loan|create'], 'as' => 'editor.loan.store', 'uses' => 'LoanController@store']);
		
		//detail
	Route::get('/loan/detail/{id}', ['middleware' => ['role:loan|update'], 'as' => 'editor.loan.detail', 'uses' => 'LoanController@detail']);
	Route::get('/loan/slip/{id}', ['middleware' => ['role:loan|update'], 'as' => 'editor.loan.slip', 'uses' => 'LoanController@slip']);
	Route::put('/loan/saveheader/{id}', ['middleware' => ['role:loan|update'], 'as' => 'editor.loan.saveheader', 'uses' => 'LoanController@saveheader']);
	Route::put('/loan/savedetail/{id}', ['middleware' => ['role:loan|update'], 'as' => 'editor.loan.savedetail', 'uses' => 'LoanController@savedetail']);

	Route::put('/loan/detail/{id}', ['middleware' => ['role:loan|update'], 'as' => 'editor.loan.update', 'uses' => 'LoanController@update']);
		//delete
	Route::delete('/loan/deletedet/{id}', ['middleware' => ['role:loan|delete'], 'as' => 'editor.loan.deletedet', 'uses' => 'LoanController@deletedet']);
	Route::post('/loan/deletebulk', ['middleware' => ['role:loan|delete'], 'as' => 'editor.loan.deletebulk', 'uses' => 'LoanController@deletebulk']);
	// detailitem 
	Route::get('/loan/datadetail/{id}', ['as' => 'editor.loan.datadetail', 'uses' => 'LoanController@datadetail']);

	//generate
	Route::get('/loan/generate/{id}', ['middleware' => ['role:loan|create'], 'as' => 'editor.loan.generate', 'uses' => 'LoanController@generate']);
	Route::post('/loan/generate/{id}', ['middleware' => ['role:loan|create'], 'as' => 'editor.loan.generate', 'uses' => 'LoanController@generate']);

	//edit
	Route::get('/loan/editdetail/{id}', ['middleware' => ['role:loan|update'], 'as' => 'editor.loan.edit', 'uses' => 'LoanController@editdetail']);
	Route::put('/loan/editdetail/{id}', ['middleware' => ['role:loan|update'], 'as' => 'editor.loan.update', 'uses' => 'LoanController@updatedetail']);


	//Reimburse
		//index
	Route::get('/reimburse', ['middleware' => ['role:reimburse|read'], 'as' => 'editor.reimburse.index', 'uses' => 'ReimburseController@index']);
	Route::get('/reimburse/data', ['as' => 'editor.reimburse.data', 'uses' => 'ReimburseController@data']);
	
	//create
	Route::get('/reimburse/create', ['middleware' => ['role:reimburse|create'], 'as' => 'editor.reimburse.create', 'uses' => 'ReimburseController@create']);
	Route::post('/reimburse/create', ['middleware' => ['role:reimburse|create'], 'as' => 'editor.reimburse.store', 'uses' => 'ReimburseController@store']);
		
		//detail
	Route::get('/reimburse/detail/{id}', ['middleware' => ['role:reimburse|update'], 'as' => 'editor.reimburse.detail', 'uses' => 'ReimburseController@detail']);
	Route::get('/reimburse/slip/{id}', ['middleware' => ['role:reimburse|update'], 'as' => 'editor.reimburse.slip', 'uses' => 'ReimburseController@slip']);
	Route::put('/reimburse/saveheader/{id}', ['middleware' => ['role:reimburse|update'], 'as' => 'editor.reimburse.saveheader', 'uses' => 'ReimburseController@saveheader']);
	Route::put('/reimburse/savedetail/{id}', ['middleware' => ['role:reimburse|update'], 'as' => 'editor.reimburse.savedetail', 'uses' => 'ReimburseController@savedetail']);

	Route::put('/reimburse/detail/{id}', ['middleware' => ['role:reimburse|update'], 'as' => 'editor.reimburse.update', 'uses' => 'ReimburseController@update']);
		//delete
	Route::delete('/reimburse/deletedet/{id}', ['middleware' => ['role:reimburse|delete'], 'as' => 'editor.reimburse.deletedet', 'uses' => 'ReimburseController@deletedet']);
	Route::post('/reimburse/deletebulk', ['middleware' => ['role:reimburse|delete'], 'as' => 'editor.reimburse.deletebulk', 'uses' => 'ReimburseController@deletebulk']);
	// detailitem 
	Route::get('/reimburse/datadetail/{id}', ['as' => 'editor.reimburse.datadetail', 'uses' => 'ReimburseController@datadetail']);

	//generate
	Route::get('/reimburse/generate/{id}', ['middleware' => ['role:reimburse|create'], 'as' => 'editor.reimburse.generate', 'uses' => 'ReimburseController@generate']);
	Route::post('/reimburse/generate/{id}', ['middleware' => ['role:reimburse|create'], 'as' => 'editor.reimburse.generate', 'uses' => 'ReimburseController@generate']);

		//Overtime
		//index
	Route::get('/overtime', ['middleware' => ['role:overtime|read'], 'as' => 'editor.overtime.index', 'uses' => 'OvertimeController@index']);
	Route::get('/overtime/data', ['as' => 'editor.overtime.data', 'uses' => 'OvertimeController@data']);
	
		//create
	Route::get('/overtime/create', ['middleware' => ['role:overtime|create'], 'as' => 'editor.overtime.create', 'uses' => 'OvertimeController@create']);
	Route::post('/overtime/create', ['middleware' => ['role:overtime|create'], 'as' => 'editor.overtime.store', 'uses' => 'OvertimeController@store']);
		//edit
	Route::get('/overtime/edit/{id}', ['middleware' => ['role:overtime|update'], 'as' => 'editor.overtime.edit', 'uses' => 'OvertimeController@edit']);
	Route::put('/overtime/edit/{id}', ['middleware' => ['role:overtime|update'], 'as' => 'editor.overtime.update', 'uses' => 'OvertimeController@update']);
		//edit detail
	Route::get('/overtime/{id}/editdetail', ['middleware' => ['role:overtime|update'], 'as' => 'editor.overtime.editdetail', 'uses' => 'OvertimeController@editdetail']);
	Route::put('/overtime/{id}/editdetail', ['middleware' => ['role:overtime|update'], 'as' => 'editor.overtime.updatedetail', 'uses' => 'OvertimeController@updatedetail']);
		//delete
	Route::delete('/overtime/delete/{id}', ['middleware' => ['role:overtime|delete'], 'as' => 'editor.overtime.delete', 'uses' => 'OvertimeController@delete']);
	Route::post('/overtime/deletebulk', ['middleware' => ['role:overtime|delete'], 'as' => 'editor.overtime.deletebulk', 'uses' => 'OvertimeController@deletebulk']);
	 
	//generate
	Route::get('/overtime/generate/{id}', ['middleware' => ['role:overtime|create'], 'as' => 'editor.overtime.generate', 'uses' => 'OvertimeController@generate']);
	Route::post('/overtime/generate/{id}', ['middleware' => ['role:overtime|create'], 'as' => 'editor.overtime.generate', 'uses' => 'OvertimeController@generate']);

		//Bonus
		//index
	Route::get('/bonus', ['middleware' => ['role:bonus|read'], 'as' => 'editor.bonus.index', 'uses' => 'BonusController@index']);
	Route::get('/bonus/data', ['as' => 'editor.bonus.data', 'uses' => 'BonusController@data']);
	
		//create
	Route::get('/bonus/create', ['middleware' => ['role:bonus|create'], 'as' => 'editor.bonus.create', 'uses' => 'BonusController@create']);
	Route::post('/bonus/create', ['middleware' => ['role:bonus|create'], 'as' => 'editor.bonus.store', 'uses' => 'BonusController@store']);
		//edit
	Route::get('/bonus/edit/{id}', ['middleware' => ['role:bonus|update'], 'as' => 'editor.bonus.edit', 'uses' => 'BonusController@edit']);
	Route::put('/bonus/edit/{id}', ['middleware' => ['role:bonus|update'], 'as' => 'editor.bonus.update', 'uses' => 'BonusController@update']);
		//edit detail
	Route::get('/bonus/{id}/{id2}/editdetail', ['middleware' => ['role:bonus|update'], 'as' => 'editor.bonus.editdetail', 'uses' => 'BonusController@editdetail']);
	Route::put('/bonus/{id}/{id2}/editdetail', ['middleware' => ['role:bonus|update'], 'as' => 'editor.bonus.updatedetail', 'uses' => 'BonusController@updatedetail']);
		//delete
	Route::delete('/bonus/delete/{id}', ['middleware' => ['role:bonus|delete'], 'as' => 'editor.bonus.delete', 'uses' => 'BonusController@delete']);
	Route::post('/bonus/deletebulk', ['middleware' => ['role:bonus|delete'], 'as' => 'editor.bonus.deletebulk', 'uses' => 'BonusController@deletebulk']);
	 
	//generate
	Route::get('/bonus/generate/{id}', ['middleware' => ['role:bonus|create'], 'as' => 'editor.bonus.generate', 'uses' => 'BonusController@generate']);
	Route::post('/bonus/generate/{id}', ['middleware' => ['role:bonus|create'], 'as' => 'editor.bonus.generate', 'uses' => 'BonusController@generate']);


		//Mealtran
		//index
	Route::get('/mealtran', ['middleware' => ['role:mealtran|read'], 'as' => 'editor.mealtran.index', 'uses' => 'MealtranController@index']);
	Route::get('/mealtran/data', ['as' => 'editor.mealtran.data', 'uses' => 'MealtranController@data']);
	
		//create
	Route::get('/mealtran/create', ['middleware' => ['role:mealtran|create'], 'as' => 'editor.mealtran.create', 'uses' => 'MealtranController@create']);
	Route::post('/mealtran/create', ['middleware' => ['role:mealtran|create'], 'as' => 'editor.mealtran.store', 'uses' => 'MealtranController@store']);
		//edit
	Route::get('/mealtran/edit/{id}', ['middleware' => ['role:mealtran|update'], 'as' => 'editor.mealtran.edit', 'uses' => 'MealtranController@edit']);
	Route::put('/mealtran/edit/{id}', ['middleware' => ['role:mealtran|update'], 'as' => 'editor.mealtran.update', 'uses' => 'MealtranController@update']);
		//edit detail
	Route::get('/mealtran/{id}/editdetail', ['middleware' => ['role:mealtran|update'], 'as' => 'editor.mealtran.editdetail', 'uses' => 'MealtranController@editdetail']);
	Route::put('/mealtran/{id}/editdetail', ['middleware' => ['role:mealtran|update'], 'as' => 'editor.mealtran.updatedetail', 'uses' => 'MealtranController@updatedetail']);
		//delete
	Route::delete('/mealtran/delete/{id}', ['middleware' => ['role:mealtran|delete'], 'as' => 'editor.mealtran.delete', 'uses' => 'MealtranController@delete']);
	Route::post('/mealtran/deletebulk', ['middleware' => ['role:mealtran|delete'], 'as' => 'editor.mealtran.deletebulk', 'uses' => 'MealtranController@deletebulk']);
	 
	//generate
	Route::get('/mealtran/generate/{id}', ['middleware' => ['role:mealtran|create'], 'as' => 'editor.mealtran.generate', 'uses' => 'MealtranController@generate']);
	Route::post('/mealtran/generate/{id}', ['middleware' => ['role:mealtran|create'], 'as' => 'editor.mealtran.generate', 'uses' => 'MealtranController@generate']);


		//TLK
		//index
	Route::get('/tlk', ['middleware' => ['role:tlk|read'], 'as' => 'editor.tlk.index', 'uses' => 'TlkController@index']);
	Route::get('/tlk/data', ['as' => 'editor.tlk.data', 'uses' => 'TlkController@data']);
	
		//create
	Route::get('/tlk/create', ['middleware' => ['role:tlk|create'], 'as' => 'editor.tlk.create', 'uses' => 'TlkController@create']);
	Route::post('/tlk/create', ['middleware' => ['role:tlk|create'], 'as' => 'editor.tlk.store', 'uses' => 'TlkController@store']);
		//edit
	Route::get('/tlk/edit/{id}', ['middleware' => ['role:tlk|update'], 'as' => 'editor.tlk.edit', 'uses' => 'TlkController@edit']);
	Route::put('/tlk/edit/{id}', ['middleware' => ['role:tlk|update'], 'as' => 'editor.tlk.update', 'uses' => 'TlkController@update']);
		//edit detail
	Route::get('/tlk/{id}/{id2}/editdetail', ['middleware' => ['role:tlk|update'], 'as' => 'editor.tlk.editdetail', 'uses' => 'TlkController@editdetail']);
	Route::put('/tlk/{id}/{id2}/editdetail', ['middleware' => ['role:tlk|update'], 'as' => 'editor.tlk.updatedetail', 'uses' => 'TlkController@updatedetail']);
		//delete
	Route::delete('/tlk/delete/{id}', ['middleware' => ['role:tlk|delete'], 'as' => 'editor.tlk.delete', 'uses' => 'TlkController@delete']);
	Route::post('/tlk/deletebulk', ['middleware' => ['role:tlk|delete'], 'as' => 'editor.tlk.deletebulk', 'uses' => 'TlkController@deletebulk']);
	 
	//generate
	Route::get('/tlk/generate/{id}', ['middleware' => ['role:tlk|create'], 'as' => 'editor.tlk.generate', 'uses' => 'TlkController@generate']);
	Route::post('/tlk/generate/{id}', ['middleware' => ['role:tlk|create'], 'as' => 'editor.tlk.generate', 'uses' => 'TlkController@generate']);

	//Thr
		//index
	Route::get('/thr', ['middleware' => ['role:thr|read'], 'as' => 'editor.thr.index', 'uses' => 'ThrController@index']);
	Route::get('/thr/data', ['as' => 'editor.thr.data', 'uses' => 'ThrController@data']);
	
		//create
	Route::get('/thr/create', ['middleware' => ['role:thr|create'], 'as' => 'editor.thr.create', 'uses' => 'ThrController@create']);
	Route::post('/thr/create', ['middleware' => ['role:thr|create'], 'as' => 'editor.thr.store', 'uses' => 'ThrController@store']);
		//edit
	Route::get('/thr/edit/{id}', ['middleware' => ['role:thr|update'], 'as' => 'editor.thr.edit', 'uses' => 'ThrController@edit']);
	Route::put('/thr/edit/{id}', ['middleware' => ['role:thr|update'], 'as' => 'editor.thr.update', 'uses' => 'ThrController@update']);
		//edit detail
	Route::get('/thr/{id}/{id2}/editdetail', ['middleware' => ['role:thr|update'], 'as' => 'editor.thr.editdetail', 'uses' => 'ThrController@editdetail']);
	Route::put('/thr/{id}/{id2}/editdetail', ['middleware' => ['role:thr|update'], 'as' => 'editor.thr.updatedetail', 'uses' => 'ThrController@updatedetail']);
		//delete
	Route::delete('/thr/delete/{id}', ['middleware' => ['role:thr|delete'], 'as' => 'editor.thr.delete', 'uses' => 'ThrController@delete']);
	Route::post('/thr/deletebulk', ['middleware' => ['role:thr|delete'], 'as' => 'editor.thr.deletebulk', 'uses' => 'ThrController@deletebulk']);
	 
	//generate
	Route::get('/thr/generate/{id}', ['middleware' => ['role:thr|create'], 'as' => 'editor.thr.generate', 'uses' => 'ThrController@generate']);
	Route::post('/thr/generate/{id}', ['middleware' => ['role:thr|create'], 'as' => 'editor.thr.generate', 'uses' => 'ThrController@generate']);

	//FILTER
	Route::post('/periodfilter', ['as' => 'editor.periodfilter', 'uses' => 'UserController@periodfilter']);
	Route::post('/periodfilterthr', ['as' => 'editor.periodfilterthr', 'uses' => 'UserController@periodfilterthr']);
	Route::post('/periodfilteronly', ['as' => 'editor.periodfilteronly', 'uses' => 'UserController@periodfilteronly']);
	Route::post('/periodfilteremp', ['as' => 'editor.periodfilteremp', 'uses' => 'UserController@periodfilteremp']);
	
	//REPORT
	//report payroll
	Route::get('/reportpayroll', ['middleware' => ['role:reportpayroll|read'], 'as' => 'editor.reportpayroll.index', 'uses' => 'ReportpayrollController@index']);
	Route::get('/reportpayroll/data', ['middleware' => ['role:reportpayroll|read'], 'as' => 'editor.reportpayroll.data', 'uses' => 'ReportpayrollController@data']);
	Route::get('/reportpayroll/printreport', ['as' => 'editor.reportpayroll.printreport', 'uses' => 'ReportpayrollController@printreport']);
	Route::get('/reportpayroll/printslip', ['as' => 'editor.reportpayroll.printslip', 'uses' => 'ReportpayrollController@printslip']);

	//report payrollreceive
	Route::get('/reportpayrollreceive', ['middleware' => ['role:reportpayrollreceive|read'], 'as' => 'editor.reportpayrollreceive.index', 'uses' => 'ReportpayrollreceiveController@index']);
	Route::get('/reportpayrollreceive/data', ['middleware' => ['role:reportpayrollreceive|read'], 'as' => 'editor.reportpayrollreceive.data', 'uses' => 'ReportpayrollreceiveController@data']);
	Route::get('/reportpayrollreceive/printreport', ['as' => 'editor.reportpayrollreceive.printreport', 'uses' => 'ReportpayrollreceiveController@printreport']);

	//report jamsostek
	Route::get('/reportjamsostek', ['middleware' => ['role:reportjamsostek|read'], 'as' => 'editor.reportjamsostek.index', 'uses' => 'ReportjamsostekController@index']);
	Route::get('/reportjamsostek/data', ['middleware' => ['role:reportjamsostek|read'], 'as' => 'editor.reportjamsostek.data', 'uses' => 'ReportjamsostekController@data']);
	Route::get('/reportjamsostek/printreport', ['as' => 'editor.reportjamsostek.printreport', 'uses' => 'ReportjamsostekController@printreport']);

	//report loan
	Route::get('/reportloan', ['middleware' => ['role:reportloan|read'], 'as' => 'editor.reportloan.index', 'uses' => 'ReportloanController@index']);
	Route::get('/reportloan/data', ['middleware' => ['role:reportloan|read'], 'as' => 'editor.reportloan.data', 'uses' => 'ReportloanController@data']);
	Route::get('/reportloan/printreport', ['as' => 'editor.reportloan.printreport', 'uses' => 'ReportloanController@printreport']);
	

	//report pph21
	Route::get('/reportpph21', ['middleware' => ['role:reportpph21|read'], 'as' => 'editor.reportpph21.index', 'uses' => 'Reportpph21Controller@index']);
	Route::get('/reportpph21/data', ['middleware' => ['role:reportpph21|read'], 'as' => 'editor.reportpph21.data', 'uses' => 'Reportpph21Controller@data']);

	//report transferbca
	Route::get('/reporttransferbca', ['middleware' => ['role:reporttransferbca|read'], 'as' => 'editor.reporttransferbca.index', 'uses' => 'ReporttransferbcaController@index']);
	Route::get('/reporttransferbca/data', ['middleware' => ['role:reporttransferbca|read'], 'as' => 'editor.reporttransferbca.data', 'uses' => 'ReporttransferbcaController@data']);
	Route::get('/reporttransferbca/printreport', ['as' => 'editor.reporttransferbca.printreport', 'uses' => 'ReporttransferbcaController@printreport']);

	//report payrollslip
	Route::get('/reportpayrollslip', ['middleware' => ['role:reportpayrollslip|read'], 'as' => 'editor.reportpayrollslip.index', 'uses' => 'ReportpayrollslipController@index']);
	Route::get('/reportpayrollslip/data', ['middleware' => ['role:reportpayrollslip|read'], 'as' => 'editor.reportpayrollslip.data', 'uses' => 'ReportpayrollslipController@data']);

	//email payrollslip
	Route::get('/emailpayrollslip', ['middleware' => ['role:emailpayrollslip|read'], 'as' => 'editor.emailpayrollslip.index', 'uses' => 'EmailpayrollslipController@index']);
	Route::post('/emailpayrollslip/sendmail/{id}', ['middleware' => ['role:emailpayrollslip|create'], 'as' => 'editor.emailpayrollslip.sendmail', 'uses' => 'EmailpayrollslipController@sendmail']);


	//report overtime
	Route::get('/reportovertime', ['middleware' => ['role:reportovertime|read'], 'as' => 'editor.reportovertime.index', 'uses' => 'ReportovertimeController@index']);
	Route::get('/reportovertime/data', ['middleware' => ['role:reportovertime|read'], 'as' => 'editor.reportovertime.data', 'uses' => 'ReportovertimeController@data']);
	Route::get('/reportovertime/printreport', ['as' => 'editor.reportovertime.printreport', 'uses' => 'ReportovertimeController@printreport']);

	//report meal
	Route::get('/reportmeal', ['middleware' => ['role:reportmeal|read'], 'as' => 'editor.reportmeal.index', 'uses' => 'ReportmealController@index']);
	Route::get('/reportmeal/data', ['middleware' => ['role:reportmeal|read'], 'as' => 'editor.reportmeal.data', 'uses' => 'ReportmealController@data']);
	Route::get('/reportmeal/printreport', ['as' => 'editor.reportovertime.printreport', 'uses' => 'ReportmealController@printreport']);
	

	//report nightallowance
	Route::get('/reportnightallowance', ['middleware' => ['role:reportnightallowance|read'], 'as' => 'editor.reportnightallowance.index', 'uses' => 'ReportnightallowanceController@index']);
	Route::get('/reportnightallowance/data', ['middleware' => ['role:reportnightallowance|read'], 'as' => 'editor.reportnightallowance.data', 'uses' => 'ReportnightallowanceController@data']);

	//report reportincityllowance
	Route::get('/reportincityallowance', ['middleware' => ['role:reportincityallowance|read'], 'as' => 'editor.reportincityallowance.index', 'uses' => 'ReportincityallowanceController@index']);
	Route::get('/reportincityallowance/data', ['middleware' => ['role:reportincityallowance|read'], 'as' => 'editor.reportincityallowance.data', 'uses' => 'ReportincityallowanceController@data']);

	//report reportoutcityllowance
	Route::get('/reportoutcityallowance', ['middleware' => ['role:reportoutcityallowance|read'], 'as' => 'editor.reportoutcityallowance.index', 'uses' => 'ReportoutcityallowanceController@index']);
	Route::get('/reportoutcityallowance/data', ['middleware' => ['role:reportoutcityallowance|read'], 'as' => 'editor.reportoutcityallowance.data', 'uses' => 'ReportoutcityallowanceController@data']);

	//report absencededuction
	Route::get('/reportabsencededuction', ['middleware' => ['role:reportabsencededuction|read'], 'as' => 'editor.reportabsencededuction.index', 'uses' => 'ReportabsencedeductionController@index']);
	Route::get('/reportabsencededuction/data', ['middleware' => ['role:reportabsencededuction|read'], 'as' => 'editor.reportabsencededuction.data', 'uses' => 'ReportabsencedeductionController@data']);

	//report insentive
	Route::get('/reportinsentive', ['middleware' => ['role:reportinsentive|read'], 'as' => 'editor.reportinsentive.index', 'uses' => 'ReportinsentiveController@index']);
	Route::get('/reportinsentive/data', ['middleware' => ['role:reportinsentive|read'], 'as' => 'editor.reportinsentive.data', 'uses' => 'ReportinsentiveController@data']);
	Route::get('/reportinsentive/printreport', ['as' => 'editor.reportinsentive.printreport', 'uses' => 'ReportinsentiveController@printreport']);
	

	//report reimburse
	Route::get('/reportreimburse', ['middleware' => ['role:reportreimburse|read'], 'as' => 'editor.reportreimburse.index', 'uses' => 'ReportreimburseController@index']);
	Route::get('/reportreimburse/data', ['middleware' => ['role:reportreimburse|read'], 'as' => 'editor.reportreimburse.data', 'uses' => 'ReportreimburseController@data']);
	Route::get('/reportreimburse/printreport', ['as' => 'editor.reportreimburse.printreport', 'uses' => 'ReportreimburseController@printreport']);
	

	//report reimburse remain
	Route::get('/reportreimburseremain', ['middleware' => ['role:reportreimburseremain|read'], 'as' => 'editor.reportreimburseremain.index', 'uses' => 'ReportreimburseremainController@index']);
	Route::get('/reportreimburseremain/data', ['middleware' => ['role:reportreimburseremain|read'], 'as' => 'editor.reportreimburseremain.data', 'uses' => 'ReportreimburseremainController@data']);
	Route::get('/reportreimburseremain/printreport', ['as' => 'editor.reportreimburseremain.printreport', 'uses' => 'ReportreimburseremainController@printreport']);
	

	//report reportmedicalreim
	Route::get('/reportreportmedicalreim', ['middleware' => ['role:reportreportmedicalreim|read'], 'as' => 'editor.reportreportmedicalreim.index', 'uses' => 'ReportreportmedicalreimController@index']);
	Route::get('/reportreportmedicalreim/data', ['middleware' => ['role:reportreportmedicalreim|read'], 'as' => 'editor.reportreportmedicalreim.data', 'uses' => 'ReportreportmedicalreimController@data']);

	//report pph21sum
	Route::get('/reportpph21sum', ['middleware' => ['role:reportpph21sum|read'], 'as' => 'editor.reportpph21sum.index', 'uses' => 'Reportpph21sumController@index']);
	Route::get('/reportpph21sum/data', ['middleware' => ['role:reportpph21sum|read'], 'as' => 'editor.reportpph21sum.data', 'uses' => 'Reportpph21sumController@data']);

	//report exportpph21monthly
	Route::get('/exportpph21monthly', ['middleware' => ['role:exportpph21monthly|read'], 'as' => 'editor.exportpph21monthly.index', 'uses' => 'Exportpph21monthlyController@index']);
	Route::get('/exportpph21monthly/data', ['middleware' => ['role:exportpph21monthly|read'], 'as' => 'editor.exportpph21monthly.data', 'uses' => 'Exportpph21monthlyController@data']);

	//report exportpph21yearly
	Route::get('/exportpph21yearly', ['middleware' => ['role:exportpph21yearly|read'], 'as' => 'editor.exportpph21yearly.index', 'uses' => 'Exportpph21yearlyController@index']);
	Route::get('/exportpph21yearly/data', ['middleware' => ['role:exportpph21yearly|read'], 'as' => 'editor.exportpph21yearly.data', 'uses' => 'Exportpph21yearlyController@data']);

		//report loan
	Route::get('/reportemployeemaster', ['middleware' => ['role:reportemployeemaster|read'], 'as' => 'editor.reportemployeemaster.index', 'uses' => 'ReportemployeemasterController@index']);
	Route::get('/reportemployeemaster/data', ['middleware' => ['role:reportemployeemaster|read'], 'as' => 'editor.reportemployeemaster.data', 'uses' => 'ReportemployeemasterController@data']);
	Route::get('/reportemployeemaster/printreport', ['as' => 'editor.reportemployeemaster.printreport', 'uses' => 'ReportemployeemasterController@printreport']);

	//report bonus thr tax
	Route::get('/reportbonusthrtax', ['middleware' => ['role:reportbonusthrtax|read'], 'as' => 'editor.reportbonusthrtax.index', 'uses' => 'ReportbonusthrtaxController@index']);
	Route::get('/reportbonusthrtax/data', ['middleware' => ['role:reportbonusthrtax|read'], 'as' => 'editor.reportbonusthrtax.data', 'uses' => 'ReportbonusthrtaxController@data']);
	Route::get('/reportbonusthrtax/printreport', ['as' => 'editor.reportbonusthrtax.printreport', 'uses' => 'ReportbonusthrtaxController@printreport']);

	//report bonus thr non tax
	Route::get('/reportbonusthr', ['middleware' => ['role:reportbonusthr|read'], 'as' => 'editor.reportbonusthr.index', 'uses' => 'ReportbonusthrController@index']);
	Route::get('/reportbonusthr/data', ['middleware' => ['role:reportbonusthr|read'], 'as' => 'editor.reportbonusthr.data', 'uses' => 'ReportbonusthrController@data']);
	Route::get('/reportbonusthr/printreport', ['as' => 'editor.reportbonusthr.printreport', 'uses' => 'ReportbonusthrController@printreport']);

	//report bonus tax
	Route::get('/reportbonustax', ['middleware' => ['role:reportbonustax|read'], 'as' => 'editor.reportbonustax.index', 'uses' => 'ReportbonustaxController@index']);
	Route::get('/reportbonustax/data', ['middleware' => ['role:reportbonustax|read'], 'as' => 'editor.reportbonustax.data', 'uses' => 'ReportbonustaxController@data']);
	Route::get('/reportbonustax/printreport', ['as' => 'editor.reportbonustax.printreport', 'uses' => 'ReportbonustaxController@printreport']);
	

	//report bonus 
	Route::get('/reportbonus', ['middleware' => ['role:reportbonus|read'], 'as' => 'editor.reportbonus.index', 'uses' => 'ReportbonusController@index']);
	Route::get('/reportbonus/data', ['middleware' => ['role:reportbonus|read'], 'as' => 'editor.reportbonus.data', 'uses' => 'ReportbonusController@data']);
	Route::get('/reportbonus/printreport', ['as' => 'editor.reportbonus.printreport', 'uses' => 'ReportbonusController@printreport']);
	
	//report bonus transfer
	Route::get('/reportbonustransfer', ['middleware' => ['role:reportbonustransfer|read'], 'as' => 'editor.reportbonustransfer.index', 'uses' => 'ReportbonustransferController@index']);
	Route::get('/reportbonustransfer/data', ['middleware' => ['role:reportbonustransfer|read'], 'as' => 'editor.reportbonustransfer.data', 'uses' => 'ReportbonustransferController@data']);
	Route::get('/reportbonustransfer/printreport', ['as' => 'editor.reportbonustransfer.printreport', 'uses' => 'ReportbonustransferController@printreport']);

	//slip bonus 
	Route::get('/reportbonusslip', ['middleware' => ['role:reportbonusslip|read'], 'as' => 'editor.reportbonusslip.index', 'uses' => 'ReportbonusslipController@index']);
	Route::get('/reportbonusslip/data', ['middleware' => ['role:reportbonusslip|read'], 'as' => 'editor.reportbonusslip.data', 'uses' => 'ReportbonusslipController@data']);

	//Transferrequest
		//index
	Route::get('/reporttransferrequest', ['middleware' => ['role:reporttransferrequest|read'], 'as' => 'editor.reporttransferrequest.index', 'uses' => 'ReporttransferrequestController@index']);
	Route::get('/reporttransferrequest/data', ['as' => 'editor.reporttransferrequest.data', 'uses' => 'ReporttransferrequestController@data']);
	
		//create
	Route::get('/reporttransferrequest/create', ['middleware' => ['role:reporttransferrequest|create'], 'as' => 'editor.reporttransferrequest.create', 'uses' => 'ReporttransferrequestController@create']);
	Route::post('/reporttransferrequest/create', ['middleware' => ['role:reporttransferrequest|create'], 'as' => 'editor.reporttransferrequest.store', 'uses' => 'ReporttransferrequestController@store']);
		//edit
	Route::get('/reporttransferrequest/edit/{id}', ['middleware' => ['role:reporttransferrequest|update'], 'as' => 'editor.reporttransferrequest.edit', 'uses' => 'ReporttransferrequestController@edit']);
	Route::put('/reporttransferrequest/edit/{id}', ['middleware' => ['role:reporttransferrequest|update'], 'as' => 'editor.reporttransferrequest.update', 'uses' => 'ReporttransferrequestController@update']);
		//edit detail
	Route::get('/reporttransferrequest/{id}/print', ['middleware' => ['role:reporttransferrequest|update'], 'as' => 'editor.reporttransferrequest.print', 'uses' => 'ReporttransferrequestController@reportprint']);
	Route::put('/reporttransferrequest/{id}/editdetail', ['middleware' => ['role:reporttransferrequest|update'], 'as' => 'editor.reporttransferrequest.updatedetail', 'uses' => 'ReporttransferrequestController@updatedetail']);
		//delete
	Route::delete('/reporttransferrequest/delete/{id}', ['middleware' => ['role:reporttransferrequest|delete'], 'as' => 'editor.reporttransferrequest.delete', 'uses' => 'ReporttransferrequestController@delete']);
	Route::post('/reporttransferrequest/deletebulk', ['middleware' => ['role:reporttransferrequest|delete'], 'as' => 'editor.reporttransferrequest.deletebulk', 'uses' => 'ReporttransferrequestController@deletebulk']);


	//Popup
		//index
	Route::get('/popup', ['middleware' => ['role:popup|read'], 'as' => 'editor.popup.index', 'uses' => 'PopupController@index']); 
		//edit
	Route::get('/popup/edit/{id}', ['middleware' => ['role:popup|update'], 'as' => 'editor.popup.edit', 'uses' => 'PopupController@edit']);
	Route::put('/popup/edit/{id}', ['middleware' => ['role:popup|update'], 'as' => 'editor.popup.update', 'uses' => 'PopupController@update']); 

	Route::group(['prefix' => '/payroll', 'namespace' => 'Payroll'], function() {
 		Route::group(['prefix' => '/setting',], function() {
 			Route::post('/master-ptkp/destroy-all', 'MasterPtkpController@destroyAll');
 			Route::get('/master-ptkp/data', 'MasterPtkpController@getDataAjax');
 			Route::resource('/master-ptkp', 'MasterPtkpController');
 		});
 
 		Route::group(['prefix' => '/correction',], function() {
 			Route::post('/destroy-all', 'CorrectionController@destroyAll');
 			Route::get('/data', 'CorrectionController@getDataAjax');
 		});
 		Route::resource('/correction', 'CorrectionController');
 
 		Route::group(['prefix' => '/calculation'], function() {
 			Route::group(['prefix' => '/preview'], function() {
 				Route::get('/', 'PayrollCalculationController@preview');
 				Route::get('/data', 'PayrollCalculationController@getDataAjax');
 			});
 			Route::group(['prefix' => '/payslip'], function() {
				Route::get('/result', 'PayslipController@showResult');
				Route::get('/result/data', 'PayslipController@showResultData');
				Route::delete('/result/{id}', 'PayslipController@destroy');
				Route::get('/result/detail/data', 'PayslipController@showResultDetailData');
				Route::get('/result/detail/{id}', 'PayslipController@showResultDetail');
				Route::get('/result/detail-payslip/{id}', 'PayslipController@showPayslip');
				Route::get('/result/detail-payslip/{id}/download', 'PayslipController@downloadPayslips');
				Route::get('/result/detail-payslip/{id}/payslip-download', 'PayslipController@downloadPayslip');
				Route::get('/result/detail-payslip/{id}/send', 'PayslipController@sendPayslip');
				Route::resource('/generate', 'PayslipController');
				Route::get('/tax-minus', 'TaxMinusController@showResult');
				Route::get('/tax-minus/data', 'TaxMinusController@showResultData');
				Route::get('/tax-minus/download', 'TaxMinusController@download');
			 });
			 Route::group(['prefix' => '/report'], function() {
				Route::get('/', 'ReportController@index');
				Route::get('/data', 'ReportController@data');
				Route::get('/pph21-bulanan-pdf', 'ReportController@downloadPph21Pdf');
				Route::get('/pph21-bulanan-csv', 'ReportController@downloadPph21Csv');
				Route::get('/employment-insurance', 'ReportController@downloadEmploymentInsurance');
				Route::get('/employment-insurance-template', 'ReportController@downloadEmploymentInsuranceTemplate');
				Route::get('/format-pph21', 'ReportController@downloadFormatPph21');
				Route::get('/form-a1', 'ReportController@downloadFormA1');
			});
		});
		 
		Route::group(['prefix' => '/thr'], function() {
			Route::group(['prefix' => '/preview',], function() {
				Route::get('/', 'ThrCalculationController@preview');
				Route::get('/data', 'ThrCalculationController@getDataAjax');
			});
			Route::group(['prefix' => '/payslip'], function() {
			   Route::get('/result', 'ThrController@showResult');
			   Route::get('/result/data', 'ThrController@showResultData');
			   Route::get('/result/{id}', 'ThrController@downloadReport');
			   Route::delete('/result/{id}', 'ThrController@destroy');
			   Route::get('/result/detail/data', 'ThrController@showResultDetailData');
			   Route::get('/result/detail/{id}', 'ThrController@showResultDetail');
			   Route::get('/result/detail-payslip/{id}', 'ThrController@showPayslip');
			   Route::get('/result/detail-payslip/{id}/download', 'ThrController@downloadPayslips');
			   Route::get('/result/detail-payslip/{id}/payslip-download', 'ThrController@downloadPayslip');
			   Route::get('/result/detail-payslip/{id}/send', 'ThrController@sendPayslip');
			   Route::resource('/generate', 'ThrController');
			});
		});

		Route::group(['prefix' => '/bonus'], function() {
			Route::group(['prefix' => '/preview',], function() {
				Route::get('/', 'BonusCalculationController@preview');
				Route::get('/data', 'BonusCalculationController@getDataAjax');
			});
			Route::group(['prefix' => '/payslip'], function() {
			   Route::get('/result', 'BonusController@showResult');
			   Route::get('/result/data', 'BonusController@showResultData');
			   Route::get('/result/{id}', 'BonusController@downloadReport');
			   Route::delete('/result/{id}', 'BonusController@destroy');
			   Route::get('/result/detail/data', 'BonusController@showResultDetailData');
			   Route::get('/result/detail/{id}', 'BonusController@showResultDetail');
			   Route::get('/result/detail-payslip/{id}', 'BonusController@showPayslip');
			   Route::get('/result/detail-payslip/{id}/download', 'BonusController@downloadPayslips');
			   Route::get('/result/detail-payslip/{id}/payslip-download', 'BonusController@downloadPayslip');
			   Route::get('/result/detail-payslip/{id}/send', 'BonusController@sendPayslip');
			   Route::resource('/generate', 'BonusController');
			});
		});
 	});
});

Route::get('/editor/payroll/calculation/payslip/download', 'Editor\Payroll\PayslipController@download')->name('payroll.payslip.download');
 	